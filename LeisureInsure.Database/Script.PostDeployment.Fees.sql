﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


USE LeisureInsureDev
GO


--MERGE generated by 'sp_generate_merge' stored procedure, Version 0.93
--Originally by Vyas (http://vyaskn.tripod.com): sp_generate_inserts (build 22)
--Adapted for SQL Server 2008/2012 by Daniel Nolan (http://danere.com)

SET NOCOUNT ON

SET IDENTITY_INSERT [Fees] ON

MERGE INTO [Fees] AS Target
USING (VALUES
  (1,1,1,5.000,10.000)
 ,(2,1,2,5.000,15.000)
 ,(3,20,1,5.000,10.000)
 ,(4,20,2,5.000,15.000)
 ,(5,21,1,5.000,10.000)
 ,(6,21,2,5.000,15.000)
 ,(7,14,1,5.000,1.000)
 ,(8,14,2,5.000,2.000)
 ,(9,16,1,5.000,1.000)
 ,(10,16,2,5.000,2.000)
 ,(11,15,1,5.000,1.000)
 ,(12,15,2,5.000,2.000)
 ,(13,2,1,5.000,10.000)
 ,(14,2,2,5.000,15.000)
 ,(15,4,1,5.000,10.000)
 ,(16,4,2,5.000,15.000)
 ,(17,6,1,5.000,10.000)
 ,(18,6,2,5.000,15.000)
 ,(19,5,1,0.000,1.990)
 ,(20,5,2,0.000,2.990)
) AS Source ([Id],[PolicyId],[LocaleId],[FeeRate],[MinimumFee])
ON (Target.[Id] = Source.[Id])
WHEN MATCHED AND (
	NULLIF(Source.[PolicyId], Target.[PolicyId]) IS NOT NULL OR NULLIF(Target.[PolicyId], Source.[PolicyId]) IS NOT NULL OR 
	NULLIF(Source.[LocaleId], Target.[LocaleId]) IS NOT NULL OR NULLIF(Target.[LocaleId], Source.[LocaleId]) IS NOT NULL OR 
	NULLIF(Source.[FeeRate], Target.[FeeRate]) IS NOT NULL OR NULLIF(Target.[FeeRate], Source.[FeeRate]) IS NOT NULL OR 
	NULLIF(Source.[MinimumFee], Target.[MinimumFee]) IS NOT NULL OR NULLIF(Target.[MinimumFee], Source.[MinimumFee]) IS NOT NULL) THEN
 UPDATE SET
  [PolicyId] = Source.[PolicyId], 
  [LocaleId] = Source.[LocaleId], 
  [FeeRate] = Source.[FeeRate], 
  [MinimumFee] = Source.[MinimumFee]
WHEN NOT MATCHED BY TARGET THEN
 INSERT([Id],[PolicyId],[LocaleId],[FeeRate],[MinimumFee])
 VALUES(Source.[Id],Source.[PolicyId],Source.[LocaleId],Source.[FeeRate],Source.[MinimumFee])
WHEN NOT MATCHED BY SOURCE THEN 
 DELETE
;
GO
DECLARE @mergeError int
 , @mergeCount int
SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT
IF @mergeError != 0
 BEGIN
 PRINT 'ERROR OCCURRED IN MERGE FOR [Fees]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected
 END
ELSE
 BEGIN
 PRINT '[Fees] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));
 END
GO

SET IDENTITY_INSERT [Fees] OFF
GO
SET NOCOUNT OFF
GO


