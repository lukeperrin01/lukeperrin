﻿CREATE TABLE [dbo].[Fees] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [PolicyId]   INT             NOT NULL,
    [LocaleId]   INT             CONSTRAINT [DF_Fees_LocaleId] DEFAULT ((1)) NOT NULL,
    [FeeRate]    DECIMAL (18, 3) CONSTRAINT [DF_Fees_FeeRate] DEFAULT ((0)) NOT NULL,
    [MinimumFee] DECIMAL (18, 3) CONSTRAINT [DF_Fees_MinimumFee] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Fees] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Fees_PolicyLocale]
    ON [dbo].[Fees]([PolicyId] ASC, [LocaleId] ASC);

