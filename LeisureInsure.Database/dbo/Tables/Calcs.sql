﻿CREATE TABLE [dbo].[Calcs] (
    [CalcPK]                 INT            IDENTITY (1, 1) NOT NULL,
    [PolicyFK]               INT            NULL,
    [CoverFK]                INT            NULL,
    [CalcOrdinal]            NUMERIC (6, 2) NULL,
    [CalcOrdinalDescription] VARCHAR (250)  NULL,
    [InputFKOne]             INT            NULL,
    [InputFKTwo]             INT            NULL,
    [FunctionName]           VARCHAR (150)  NULL,
    [InputParam]             VARCHAR (50)   NULL,
    [CalcLevel]              INT            NULL,
    CONSTRAINT [PK_Calcs] PRIMARY KEY CLUSTERED ([CalcPK] ASC)
);

