﻿CREATE TABLE [dbo].[Clauses] (
    [ClausePK]       INT           IDENTITY (1, 1) NOT NULL,
    [ClauseCode]     VARCHAR (5)   NULL,
    [ClauseLink]     VARCHAR (250) NULL,
    [EireClauseLink] VARCHAR (250) NULL,
    CONSTRAINT [PK_Clauses] PRIMARY KEY CLUSTERED ([ClausePK] ASC)
);

