﻿CREATE TABLE [dbo].[Policies] (
    [PolicyPK]         INT             IDENTITY (1, 1) NOT NULL,
    [BitWise]          INT             NULL,
    [CoverBW]          INT             NULL,
    [LocaleBW]         INT             NOT NULL,
    [PolicyName]       VARCHAR (50)    NOT NULL,
    [TamType]          VARCHAR (10)    NOT NULL,
    [Ordinal]          INT             NOT NULL,
    [LeisureInsureFee] DECIMAL (18, 2) CONSTRAINT [DF_Policy_LeisureInsureFee] DEFAULT ((0)) NULL,
    [BrokerCommission] DECIMAL (18, 2) NULL,
    [PeriodYear]       INT             NULL,
    [PeriodDay]        INT             NULL,
    [PolicyType]       CHAR (1)        NULL,
    [PolicyGroupFK]    INT             NULL,
    [PremiseRequired]  INT             NULL,
    [TimeRequired]     INT             NULL,
    [MinDateDays]      INT             NULL,
    [MaxDateMonths]    INT             NULL,
    [TamOccupation]    NVARCHAR (20)   CONSTRAINT [DF_Policies_TamOccupation] DEFAULT ('') NOT NULL,
    [ClauseCode]       VARBINARY (5)   NULL,
    CONSTRAINT [Policy_PK] PRIMARY KEY CLUSTERED ([PolicyPK] ASC)
);





