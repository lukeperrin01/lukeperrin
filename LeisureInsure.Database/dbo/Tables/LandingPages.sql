﻿CREATE TABLE [dbo].[LandingPages] (
    [Id]                     INT            IDENTITY (1, 1) NOT NULL,
    [Url]                    NVARCHAR (255) NOT NULL,
    [Title]                  NVARCHAR (255) NOT NULL,
    [Keywords]               NVARCHAR (MAX) NOT NULL,
    [Description]            NVARCHAR (MAX) NOT NULL,
    [PictureUrl]             NVARCHAR (255) NOT NULL,
    [LandingPageDescription] NVARCHAR (MAX) NOT NULL,
    [PolicyId]               INT            CONSTRAINT [DF_LandingPages_PolicyId] DEFAULT ((0)) NOT NULL,
    [LastModified]           DATETIME2 (7)  CONSTRAINT [DF_LandingPages_LastModified] DEFAULT ('2016-10-01') NOT NULL,
    CONSTRAINT [PK_LandingPages] PRIMARY KEY CLUSTERED ([Id] ASC)
);








GO
CREATE NONCLUSTERED INDEX [IX_LandingPages_Url]
    ON [dbo].[LandingPages]([Url] ASC);

