﻿CREATE TABLE [dbo].[LocaleMultipliers] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [LocaleId]   INT             NOT NULL,
    [PolicyId]   INT             NOT NULL,
    [CoverId]    INT             NOT NULL,
    [Multiplier] DECIMAL (18, 3) CONSTRAINT [DF_LocaleMultipliers_Multiplier] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_LocaleMultipliers] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LocalePolicyCover]
    ON [dbo].[LocaleMultipliers]([LocaleId] ASC, [PolicyId] ASC, [CoverId] ASC);

