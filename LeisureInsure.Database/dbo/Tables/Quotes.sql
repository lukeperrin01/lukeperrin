﻿CREATE TABLE [dbo].[Quotes] (
    [QuotePK]              INT             IDENTITY (1, 1) NOT NULL,
    [CoversBW]             INT             NULL,
    [PolicyFK]             INT             CONSTRAINT [DF_Quotes_PolicyFK] DEFAULT ((0)) NULL,
    [ContactEmail]         VARCHAR (250)   NULL,
    [ContactFK]            INT             CONSTRAINT [DF_Quotes_ContactFK] DEFAULT ((0)) NULL,
    [CustomerFK]           INT             CONSTRAINT [DF_Quotes_CustomerFK] DEFAULT ((0)) NULL,
    [BrokerFK]             INT             CONSTRAINT [DF_Quotes_BrokerFK] DEFAULT ((0)) NULL,
    [AddressFK]            INT             CONSTRAINT [DF_Quotes_AddressFK] DEFAULT ((0)) NULL,
    [Postcode]             VARCHAR (12)    NULL,
    [PremiseFK]            INT             CONSTRAINT [DF_Quotes_PremiseFK] DEFAULT ((0)) NULL,
    [BIHA]                 VARCHAR (50)    NULL,
    [QuoteRef]             NVARCHAR (50)   CONSTRAINT [DF_Quotes_QuoteRef] DEFAULT ('NOQUOTE') NOT NULL,
    [Purchased]            INT             CONSTRAINT [DF_Quotes_Purchased] DEFAULT ((0)) NULL,
    [DateFrom]             DATETIME        NULL,
    [TimeFrom]             VARCHAR (5)     NULL,
    [DateTo]               DATETIME        NULL,
    [TimeTo]               VARCHAR (5)     NULL,
    [DateAdded]            DATETIME        CONSTRAINT [DF_Quotes_DateAdded] DEFAULT (getdate()) NULL,
    [NetPremium]           NUMERIC (18, 3) CONSTRAINT [DF_Quotes_NetPremium] DEFAULT ((0)) NULL,
    [NetLegal]             NUMERIC (18, 3) CONSTRAINT [DF_Quotes_LegalNet] DEFAULT ((0)) NULL,
    [Fee]                  NUMERIC (18, 3) CONSTRAINT [DF_Quotes_Fee] DEFAULT ((0)) NULL,
    [Tax]                  NUMERIC (18, 3) CONSTRAINT [DF_Quotes_Tax] DEFAULT ((0)) NULL,
    [TaxRate]              NUMERIC (10, 3) NULL,
    [BrokerCommission]     NUMERIC (18, 3) CONSTRAINT [DF_Quotes_BrokerCommission] DEFAULT ((0)) NULL,
    [CommissionPercentage] NUMERIC (18, 3) CONSTRAINT [DF_Quotes_CommissionPercentage] DEFAULT ((0)) NOT NULL,
    [CardCharge]           NUMERIC (10, 3) CONSTRAINT [DF_Quotes_CardCharge] DEFAULT ((0)) NULL,
    [PayOption]            INT             NULL,
    [PWord]                VARCHAR (50)    CONSTRAINT [DF_Quotes_PWord] DEFAULT ('NONE') NOT NULL,
    [TamClientCode]        VARCHAR (50)    NULL,
    [TAMPolicyCode]        VARCHAR (12)    CONSTRAINT [DF_Quotes_TAMCode] DEFAULT ('') NULL,
    [TamBrokerCode]        VARCHAR (12)    NULL,
    [Loc]                  VARCHAR (8)     NULL,
    [Lock]                 INT             CONSTRAINT [DF_Quotes_Lock] DEFAULT ((0)) NULL,
    [Reffered]             INT             CONSTRAINT [DF_Quotes_Reffered] DEFAULT ((0)) NULL,
    [Csr]                  VARCHAR (50)    NULL,
    [Progress]             INT             NULL,
    [LegalFeesAdded]       BIT             CONSTRAINT [DF_Quotes_LegalFeesAdded] DEFAULT ('0') NOT NULL,
    [ExcessWaiver]         NUMERIC (18, 3) CONSTRAINT [DF_Quotes_ExcessWaiver] DEFAULT ((0)) NOT NULL,
    [ExcessWaiverAdded]    BIT             CONSTRAINT [DF_Quotes_ExcessWaiverAdded] DEFAULT ('0') NOT NULL,
    [ExcessWaiverAllowed]  BIT             CONSTRAINT [DF_Quotes_ExcessWaiverAllowed] DEFAULT ('0') NOT NULL,
    [Total]                NUMERIC (18, 3) CONSTRAINT [DF_Quotes_Total] DEFAULT ((0)) NOT NULL,
    [IsAgent]              BIT             CONSTRAINT [DF_Quotes_IsAgent] DEFAULT ((0)) NOT NULL,
    [Excess]               NUMERIC (18, 3) CONSTRAINT [DF_Quotes_Excess] DEFAULT ((0)) NOT NULL,
    [LocaleId]             INT             CONSTRAINT [DF_Quotes_LocaleId] DEFAULT ((1)) NOT NULL,
    [PaymentCardId]        INT             CONSTRAINT [DF_Quotes_PaymentCardId] DEFAULT ((0)) NOT NULL,
    [ReferralType]         INT             CONSTRAINT [DF_Quotes_ReferralType] DEFAULT ((0)) NOT NULL,
    [OrderNumber]          INT             CONSTRAINT [DF_Quotes_OrderNumber] DEFAULT ((0)) NOT NULL,
    [LockedForCustomer]    BIT             CONSTRAINT [DF_Quotes_LockedForCustomer] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Quotes] PRIMARY KEY CLUSTERED ([QuotePK] ASC)
);












GO
CREATE NONCLUSTERED INDEX [IX_Quotes_QuoteRef_QuotePassword]
    ON [dbo].[Quotes]([QuoteRef] ASC, [PWord] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Quotes_QuoteRef]
    ON [dbo].[Quotes]([QuoteRef] ASC);

