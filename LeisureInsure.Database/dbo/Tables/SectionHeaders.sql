﻿CREATE TABLE [dbo].[SectionHeaders] (
    [SectionHeaderPK] INT            IDENTITY (1, 1) NOT NULL,
    [SectionNumber]   INT            NULL,
    [Label]           VARCHAR (50)   NULL,
    [Description]     VARCHAR (150)  NULL,
    [ValueUK]         INT            NULL,
    [ValueEire]       INT            NULL,
    [Ordinal]         NUMERIC (6, 3) NULL,
    [LineGroup]       INT            NULL,
    CONSTRAINT [PK_SectionHeaders] PRIMARY KEY CLUSTERED ([SectionHeaderPK] ASC)
);

