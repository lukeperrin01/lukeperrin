﻿CREATE TABLE [dbo].[QuoteDetails] (
    [InputFK]        INT             NULL,
    [RateFK]         INT             NOT NULL,
    [CoverFK]        INT             NOT NULL,
    [QuoteFK]        INT             NOT NULL,
    [QuoteDetailPK]  INT             IDENTITY (1, 1) NOT NULL,
    [LineType]       VARCHAR (250)   NOT NULL,
    [Base]           NUMERIC (12, 2) NULL,
    [Rate]           NUMERIC (8, 2)  NULL,
    [Charge]         NUMERIC (8, 2)  NULL,
    [TamPolicyIndex] VARCHAR (50)    NULL,
    [ListNo]         INT             NULL,
    [ListOrdinal]    INT             NULL,
    [DateAdded]      DATETIME        CONSTRAINT [DF_QuoteDetails_DateAdded] DEFAULT (getdate()) NULL,
    [Lock]           INT             CONSTRAINT [DF_QuoteDetails_Lock] DEFAULT ((0)) NULL,
    [InputValue]     VARCHAR (150)   NULL,
    [SumInsured]     NUMERIC (18)    NULL,
    [NoItems]        INT             NULL,
    [lblSumInsured]  VARCHAR (50)    NULL,
    [lblNoItems]     VARCHAR (50)    NULL,
    [RateTypeFK]     INT             NULL,
    CONSTRAINT [PK_QuoteDetails] PRIMARY KEY CLUSTERED ([QuoteDetailPK] ASC)
);



