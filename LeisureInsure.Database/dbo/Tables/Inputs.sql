﻿CREATE TABLE [dbo].[Inputs] (
    [LocaleBW]       INT            NULL,
    [PolicyBW]       INT            NULL,
    [CoverBW]        INT            NULL,
    [InputPK]        INT            IDENTITY (1, 1) NOT NULL,
    [InputName]      VARCHAR (50)   NULL,
    [InputOrdinal]   NUMERIC (7, 3) NULL,
    [InputType]      INT            NULL,
    [InputTriggerFK] INT            NULL,
    [PageNo]         INT            NULL,
    [ParentPK]       INT            NULL,
    [ngRequired]     INT            NULL,
    [UnitType]       VARCHAR (10)   NULL,
    [CalcTypeFK]     INT            NULL,
    [CalcOrdinal]    NUMERIC (6, 3) NULL,
    [CalcSP]         VARCHAR (50)   NULL,
    [ngClass]        VARCHAR (100)  NULL,
    [ngPattern]      VARCHAR (250)  NULL,
    [ChildCoverPK]   INT            NULL,
    [CertList]       INT            NULL,
    CONSTRAINT [InputPK] PRIMARY KEY CLUSTERED ([InputPK] ASC)
);

