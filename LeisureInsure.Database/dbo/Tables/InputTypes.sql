﻿CREATE TABLE [dbo].[InputTypes] (
    [InputTypePK]      INT           IDENTITY (1, 1) NOT NULL,
    [InputType]        VARCHAR (50)  NULL,
    [InputDescription] VARCHAR (50)  NULL,
    [Regex]            VARCHAR (500) NULL,
    [Required]         VARCHAR (10)  NULL,
    [ValueField]       INT           NULL,
    [InputBW]          INT           NULL,
    CONSTRAINT [PK_InputTypes] PRIMARY KEY CLUSTERED ([InputTypePK] ASC)
);

