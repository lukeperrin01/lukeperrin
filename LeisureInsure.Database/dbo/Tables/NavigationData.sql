﻿CREATE TABLE [dbo].[NavigationData] (
    [Id]            INT IDENTITY (1, 1) NOT NULL,
    [InputId]       INT NOT NULL,
    [RateId]        INT NOT NULL,
    [LandingPageId] INT NOT NULL,
    CONSTRAINT [PK_NavigationData] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_NavigationData_LandingPageId]
    ON [dbo].[NavigationData]([LandingPageId] ASC);

