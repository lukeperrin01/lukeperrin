﻿CREATE TABLE [dbo].[CalcTypes] (
    [CalcTypePK]  INT            IDENTITY (1, 1) NOT NULL,
    [CalcOrdinal] NUMERIC (6, 3) NULL,
    [CalcSP]      VARCHAR (50)   NULL,
    [PolicyBW]    INT            NULL,
    [CoverBW]     INT            NULL
);

