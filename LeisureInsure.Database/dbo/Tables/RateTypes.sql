﻿CREATE TABLE [dbo].[RateTypes] (
    [RateTypePK]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (50)   NULL,
    [Description]      VARCHAR (250)  NULL,
    [Trigger]          INT            NULL,
    [CalcTypeFK]       INT            NULL,
    [CalcOrdinal]      NUMERIC (6, 3) NULL,
    [DestinationTable] VARCHAR (50)   NULL,
    [MagicNumber]      NCHAR (10)     NULL,
    CONSTRAINT [RateTypePK] PRIMARY KEY CLUSTERED ([RateTypePK] ASC)
);

