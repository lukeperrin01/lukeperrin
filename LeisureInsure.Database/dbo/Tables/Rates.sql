﻿CREATE TABLE [dbo].[Rates] (
    [CoverBW]                INT             CONSTRAINT [DF_Rates_CoverBW] DEFAULT ((0)) NOT NULL,
    [PolicyBW]               INT             CONSTRAINT [DF_Rates_PolicyBW] DEFAULT ((0)) NOT NULL,
    [LocaleBW]               INT             CONSTRAINT [DF_Rates_LocaleBW] DEFAULT ((0)) NOT NULL,
    [ChildBW]                INT             NULL,
    [InputFK]                INT             CONSTRAINT [DF_Rates_InputFK] DEFAULT ((122)) NULL,
    [RatePK]                 INT             IDENTITY (1, 1) NOT NULL,
    [RateName]               VARCHAR (500)   NULL,
    [Rate]                   NUMERIC (18, 5) CONSTRAINT [DF_Rates_Rate] DEFAULT ((0)) NULL,
    [Threshold]              NUMERIC (18, 5) CONSTRAINT [DF_Rates_Threshold] DEFAULT ((0)) NULL,
    [Excess]                 INT             CONSTRAINT [DF_Rates_Excess] DEFAULT ((0)) NULL,
    [RateLabel]              VARCHAR (250)   NULL,
    [RateTip]                VARCHAR (250)   NULL,
    [Refer]                  INT             CONSTRAINT [DF_Rates_Refer] DEFAULT ((0)) NOT NULL,
    [DivBy]                  INT             CONSTRAINT [DF_Rates_DivBy] DEFAULT ((0)) NULL,
    [RateOrdinal]            NUMERIC (18, 5) NULL,
    [RateBW]                 INT             NULL,
    [DateTill]               DATETIME        NULL,
    [HazardRating]           INT             NULL,
    [PPK]                    INT             NULL,
    [DiscountFirstRate]      DECIMAL (18, 5) CONSTRAINT [DF_Rates_DiscountRate] DEFAULT ((0)) NOT NULL,
    [DiscountSubsequentRate] DECIMAL (18, 5) CONSTRAINT [DF_Rates_DiscountSubsequentRate] DEFAULT ((0)) NOT NULL,
    [RateIsPercentage]       BIT             CONSTRAINT [DF_Rates_RateIsPercentage] DEFAULT ((0)) NOT NULL,
    [ThresholdIsPercentage]  BIT             CONSTRAINT [DF_Rates_ThresholdIsPercentage] DEFAULT ((0)) NOT NULL,
    [RateType]               INT             CONSTRAINT [DF_Rates_RateType] DEFAULT ((0)) NOT NULL,
    [TableFirstColumn]       INT             NULL,
    [TableChildren]          INT             NULL,
    [InputTypeFK]            INT             NULL,
    [UnitType]               VARCHAR (50)    NULL,
    [ParentInputPK]          INT             NULL,
    [InputOrdinal]           NUMERIC (18, 3) NULL,
    [NewInputPK]             INT             NULL,
    [ParentRatePK]           INT             NULL,
    [NewParentBW]            INT             NULL,
    [NewChildBW]             INT             NULL,
    [ListRatePK]             INT             NULL,
    [SumInsured]             NUMERIC (18)    NULL,
    [NoItems]                INT             NULL,
    [DisplayBW]              INT             NULL,
    [ReturnBW]               INT             NULL,
    [StringBW]               INT             NULL,
    [lblSumInured]           VARCHAR (50)    NULL,
    [lblNumber]              VARCHAR (50)    NULL,
    [RateExclude]            INT             CONSTRAINT [DF_Rates_RateExclude] DEFAULT ((0)) NULL,
    [RateTypeFK]             INT             NULL,
    [RateGroup]              INT             NULL,
    [CalcOrdinal]            NUMERIC (18, 3) NULL,
    [CoverCalcFK]            INT             NULL,
    [TamOccupation]          VARCHAR (50)    NULL,
    [PageNo]                 INT             CONSTRAINT [DF_Rates_PageNo] DEFAULT ((1)) NULL,
    [InputTrigger]           INT             CONSTRAINT [DF_Rates_InputTrigger] DEFAULT ((0)) NULL,
    [ValidUntil]             DATETIME        NULL,
    [ClauseCode]             VARCHAR (20)    NULL,
    CONSTRAINT [RatePK] PRIMARY KEY CLUSTERED ([RatePK] ASC)
);











