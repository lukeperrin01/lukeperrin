﻿CREATE TABLE [dbo].[Instalments] (
    [InstallmentPK]    INT            IDENTITY (1, 1) NOT NULL,
    [QuoteRef]         NUMERIC (18)   NULL,
    [CustomerID]       NUMERIC (18)   NULL,
    [AccountName]      VARCHAR (80)   NULL,
    [AccountNumber]    VARCHAR (50)   NULL,
    [SortCode]         VARCHAR (50)   NULL,
    [BusinessName]     VARCHAR (80)   NULL,
    [Salutation]       VARCHAR (50)   NULL,
    [FirstName]        VARCHAR (50)   NULL,
    [LastName]         VARCHAR (50)   NULL,
    [FullPremium]      DECIMAL (8, 2) NULL,
    [Deposit]          DECIMAL (8, 2) NULL,
    [FlatRate]         DECIMAL (8, 2) NULL,
    [NumberOfPayments] DECIMAL (3)    NULL,
    [InstalmentMonth]  VARCHAR (50)   NULL,
    [Timestamp]        DATETIME       NULL,
    [DownLoaded]       DATETIME       NULL,
    [Lock]             INT            CONSTRAINT [DF_Instalments_Lock] DEFAULT ((0)) NULL,
    CONSTRAINT [Installment_PK] PRIMARY KEY CLUSTERED ([InstallmentPK] ASC)
);

