﻿CREATE TABLE [dbo].[QuoteDocuments] (
    [QuoteDocumentPK] INT           IDENTITY (1, 1) NOT NULL,
    [Description]     VARCHAR (100) NULL,
    [Url]             VARCHAR (MAX) NULL,
    [DateStamp]       DATETIME2 (0) CONSTRAINT [DF_QuoteDocuments_DateStamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_QuoteDocuments] PRIMARY KEY CLUSTERED ([QuoteDocumentPK] ASC)
);

