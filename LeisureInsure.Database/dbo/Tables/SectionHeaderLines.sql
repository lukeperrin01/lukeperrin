﻿CREATE TABLE [dbo].[SectionHeaderLines] (
    [SectionHeaderPK] INT            IDENTITY (1, 1) NOT NULL,
    [SectionNumber]   INT            NULL,
    [Label1]          VARCHAR (150)  NULL,
    [Label2]          VARCHAR (150)  NULL,
    [Description]     VARCHAR (150)  NULL,
    [SpanUK1]         VARCHAR (500)  NULL,
    [SpanEire1]       VARCHAR (500)  NULL,
    [SpanUK2]         VARCHAR (500)  NULL,
    [SpanEire2]       VARCHAR (500)  NULL,
    [LineGroup]       INT            NULL,
    [LineNumber]      NUMERIC (6, 3) NULL,
    [Ordinal]         NUMERIC (6, 3) NULL,
    [ParaAfter]       INT            CONSTRAINT [DF_SectionHeaderLines_ParaAfter] DEFAULT ((0)) NULL,
    [InputFK]         INT            NULL,
    [PolicyBW]        INT            NULL,
    CONSTRAINT [PK_SectionHeaderLine] PRIMARY KEY CLUSTERED ([SectionHeaderPK] ASC)
);



