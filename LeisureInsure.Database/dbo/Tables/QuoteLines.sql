﻿CREATE TABLE [dbo].[QuoteLines] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [LineDescription] NVARCHAR (255)  CONSTRAINT [DF_QuoteLine_LineDescription] DEFAULT ('') NOT NULL,
    [Price]           NUMERIC (18, 2) CONSTRAINT [DF_QuoteLine_Price] DEFAULT ((0)) NOT NULL,
    [Commision]       NUMERIC (18, 2) CONSTRAINT [DF_QuoteLine_Commision] DEFAULT ((0)) NOT NULL,
    [Discriminator]   INT             CONSTRAINT [DF_QuoteLine_Discriminator] DEFAULT ((0)) NOT NULL,
    [QuoteId]         INT             NOT NULL,
    [SumInsured]      MONEY           NULL,
    [Indemnity]       MONEY           NULL,
    [CoverFK]         INT             NULL,
    CONSTRAINT [PK_QuoteLine] PRIMARY KEY CLUSTERED ([Id] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_QuoteLines_QuoteId]
    ON [dbo].[QuoteLines]([QuoteId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_QuoteLines_Discriminator]
    ON [dbo].[QuoteLines]([Discriminator] ASC);

