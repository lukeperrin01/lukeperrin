﻿CREATE TABLE [dbo].[Documents] (
    [DocumentPK] INT           IDENTITY (1, 1) NOT NULL,
    [DocName]    VARCHAR (50)  NULL,
    [DocLink]    VARCHAR (350) NULL,
    [BitWise]    INT           NULL,
    [DocType]    VARCHAR (50)  NULL,
    [LocaleBW]   INT           NULL,
    [PolicyBW]   INT           NULL,
    [CoverBW]    INT           NULL,
    [RateFK]     INT           NULL,
    [Biha]       INT           NULL,
    [DateAdded]  DATETIME      NULL,
    [ValidUntil] DATETIME      NULL,
    [ClauseCode] VARCHAR (20)  NULL,
    CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED ([DocumentPK] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [Doclink_UX]
    ON [dbo].[Documents]([DocLink] ASC);

