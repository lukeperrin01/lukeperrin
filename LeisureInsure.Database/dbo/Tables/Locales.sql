﻿CREATE TABLE [dbo].[Locales] (
    [LocalePK]       INT            IDENTITY (1, 1) NOT NULL,
    [BitWise]        INT            NOT NULL,
    [Country]        VARCHAR (50)   NOT NULL,
    [strLocale]      VARCHAR (10)   NOT NULL,
    [CurrencySymbol] CHAR (1)       NOT NULL,
    [ExchangeRate]   NUMERIC (4, 2) NOT NULL,
    [Multiplier]     NUMERIC (4, 2) NOT NULL,
    [LcID]           INT            NOT NULL,
    [TamAgency]      INT            NOT NULL,
    [imgFlag]        VARCHAR (100)  NOT NULL,
    [Territory]      VARCHAR (250)  NULL,
    [CurrencyCode]   VARCHAR (5)    NULL,
    CONSTRAINT [PK_Locale] PRIMARY KEY CLUSTERED ([LocalePK] ASC)
);

