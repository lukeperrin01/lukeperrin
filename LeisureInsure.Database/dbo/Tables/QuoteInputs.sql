﻿CREATE TABLE [dbo].[QuoteInputs] (
    [QuoteInputPK]  INT            IDENTITY (1, 1) NOT NULL,
    [CoverPK]       INT            NULL,
    [ListNo]        INT            NULL,
    [ListOrdinal]   INT            NULL,
    [InputName]     VARCHAR (150)  NULL,
    [InputFK]       INT            NULL,
    [RatePK]        INT            NULL,
    [RateValue]     VARCHAR (150)  NULL,
    [RateString]    VARCHAR (150)  NULL,
    [InputString]   VARCHAR (1000) NULL,
    [InputPKLock]   INT            NULL,
    [QuoteId]       INT            CONSTRAINT [DF_QuoteInputs_QuoteId] DEFAULT ((0)) NOT NULL,
    [SumInsured]    INT            NULL,
    [NoItems]       INT            NULL,
    [lblSumInsured] VARCHAR (50)   NULL,
    [lblNoItems]    VARCHAR (50)   NULL,
    [RateTypeFK]    INT            NULL
);





