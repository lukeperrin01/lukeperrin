﻿CREATE TABLE [dbo].[Certs] (
    [CertificatePK] INT             IDENTITY (1, 1) NOT NULL,
    [Url]           VARCHAR (50)    NOT NULL,
    [Certificate]   VARBINARY (MAX) NOT NULL,
    [QuoteRef]      INT             NOT NULL,
    [FileName]      VARCHAR (250)   NULL,
    [Lock]          INT             CONSTRAINT [DF_Certs_Lock] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Certificates] PRIMARY KEY CLUSTERED ([CertificatePK] ASC)
);

