﻿CREATE TABLE [dbo].[ErrorLog] (
    [ErrorLogPK] INT            IDENTITY (1, 1) NOT NULL,
    [URL]        VARCHAR (1000) NULL,
    [Msg]        VARCHAR (1000) NULL,
    [StackTrace] VARCHAR (MAX)  NULL,
    [Cause]      VARCHAR (1000) NULL,
    [DateStamp]  DATETIME       CONSTRAINT [DF_ErrorLog_DateStamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_ErrorLog] PRIMARY KEY CLUSTERED ([ErrorLogPK] ASC)
);

