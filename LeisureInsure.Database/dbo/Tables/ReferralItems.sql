﻿CREATE TABLE [dbo].[ReferralItems] (
    [Id]       INT IDENTITY (1, 1) NOT NULL,
    [RateId]   INT NOT NULL,
    [Accepted] BIT NOT NULL,
    [QuoteId]  INT NOT NULL
);

