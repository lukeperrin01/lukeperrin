﻿CREATE TABLE [dbo].[Contacts] (
    [ContactPK]          INT             IDENTITY (1, 1) NOT NULL,
    [FirstName]          VARCHAR (50)    NOT NULL,
    [LastName]           VARCHAR (250)   NOT NULL,
    [AdditionalName]     VARCHAR (50)    CONSTRAINT [DF_Contacts_AdditionalName] DEFAULT ('') NULL,
    [Telephone]          VARCHAR (50)    CONSTRAINT [DF_Contacts_Telephone] DEFAULT ('') NULL,
    [Email]              VARCHAR (50)    CONSTRAINT [DF_Contacts_Email] DEFAULT ('') NULL,
    [ContactType]        INT             CONSTRAINT [DF_Contacts_ContactType] DEFAULT ((0)) NULL,
    [UserName]           VARCHAR (100)   CONSTRAINT [DF_Contacts_UserName] DEFAULT ('') NULL,
    [EntityType]         VARCHAR (50)    CONSTRAINT [DF_Contacts_EntityType] DEFAULT ('C') NULL,
    [EntityName]         VARCHAR (75)    NULL,
    [EntityTypeName]     VARCHAR (50)    NULL,
    [TamClientRef]       VARCHAR (50)    NULL,
    [Lock]               INT             CONSTRAINT [DF_Contacts_Lock] DEFAULT ((0)) NULL,
    [ContactStatus]      INT             NULL,
    [FSANumber]          VARCHAR (20)    NULL,
    [BrokerCommission]   NUMERIC (18, 3) NULL,
    [CommissionPolicyBW] INT             NULL,
    [QuotePK]            INT             NULL,
    [ForAttentionOf]     VARCHAR (50)    CONSTRAINT [DF_Contacts_ForAttentionOf] DEFAULT ('') NULL,
    [TamName]            VARCHAR (50)    CONSTRAINT [DF_Contacts_TamName] DEFAULT ('') NULL,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([ContactPK] ASC)
);








GO
CREATE NONCLUSTERED INDEX [IX_Contacts]
    ON [dbo].[Contacts]([TamClientRef] ASC);

