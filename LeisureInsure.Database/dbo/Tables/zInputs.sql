﻿CREATE TABLE [dbo].[zInputs] (
    [LocaleBW]       INT            NULL,
    [PolicyBW]       INT            NULL,
    [CoverBW]        INT            NULL,
    [InputPK]        INT            NULL,
    [InputName]      VARCHAR (50)   NULL,
    [InputOrdinal]   NUMERIC (7, 3) NULL,
    [InputType]      INT            NULL,
    [InputTriggerFK] INT            NULL,
    [PageNo]         INT            NULL,
    [ParentPK]       INT            NULL,
    [ngRequired]     INT            NULL,
    [UnitType]       VARCHAR (10)   NULL,
    [CalcTypeFK]     INT            NULL,
    [CalcOrdinal]    NUMERIC (6, 3) NULL,
    [CalcSP]         VARCHAR (50)   NULL,
    [ngClass]        VARCHAR (100)  NULL,
    [ChildCoverPK]   INT            NULL,
    [CertList]       INT            NULL
);

