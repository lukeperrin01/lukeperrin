﻿CREATE TABLE [dbo].[PaymentCards] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (255)  NOT NULL,
    [Type]        INT             NOT NULL,
    [AddChargeUk] DECIMAL (18, 5) NOT NULL,
    [AddChargeIe] DECIMAL (18, 5) CONSTRAINT [DF_PaymentCards_AddChargeIe] DEFAULT ((0)) NOT NULL,
    [AddRate]     DECIMAL (18, 5) NOT NULL,
    [ImageUrl]    NVARCHAR (255)  CONSTRAINT [DF_PaymentCards_ImageUrl] DEFAULT ('') NOT NULL
);



