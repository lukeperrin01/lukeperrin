﻿CREATE TABLE [dbo].[Covers] (
    [CoverPK]       INT           IDENTITY (1, 1) NOT NULL,
    [LocaleBW]      INT           NOT NULL,
    [Section]       INT           NOT NULL,
    [BitWise]       INT           NOT NULL,
    [CoverName]     VARCHAR (100) NOT NULL,
    [SectionName]   VARCHAR (100) CONSTRAINT [DF_Covers_SectionName] DEFAULT ('') NULL,
    [Specification] VARCHAR (50)  NULL,
    [CoverTip]      VARCHAR (250) NULL,
    CONSTRAINT [PK_Covers] PRIMARY KEY CLUSTERED ([CoverPK] ASC)
);

