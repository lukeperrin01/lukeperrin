﻿CREATE TABLE [dbo].[Addresses] (
    [ContactFK]   INT          NULL,
    [AddressPK]   INT          IDENTITY (1, 1) NOT NULL,
    [Address1]    VARCHAR (60) NOT NULL,
    [Address2]    VARCHAR (60) CONSTRAINT [DF_Addresses_Address2] DEFAULT ('') NULL,
    [Address3]    VARCHAR (60) CONSTRAINT [DF_Addresses_Address3] DEFAULT ('') NULL,
    [Town]        VARCHAR (50) NOT NULL,
    [County]      VARCHAR (50) NOT NULL,
    [Country]     VARCHAR (30) NULL,
    [Postcode]    VARCHAR (12) CONSTRAINT [DF_Addresses_Postcode] DEFAULT ('') NULL,
    [Locale]      VARCHAR (10) NULL,
    [Timestamp]   DATETIME     CONSTRAINT [DF_Addresses_Timestamp] DEFAULT (getdate()) NOT NULL,
    [Lock]        INT          CONSTRAINT [DF_Addresses_Lock] DEFAULT ((0)) NULL,
    [QuoteFK]     INT          NULL,
    [AddressType] INT          NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([AddressPK] ASC)
);



