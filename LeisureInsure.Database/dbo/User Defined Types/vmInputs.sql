﻿CREATE TYPE [dbo].[vmInputs] AS TABLE (
    [CoverPK]     INT            NULL,
    [ListNo]      INT            NULL,
    [ListOrdinal] INT            NULL,
    [InputPK]     INT            NULL,
    [RatePK]      INT            NULL,
    [RateValue]   VARCHAR (150)  NULL,
    [InputString] VARCHAR (1000) NULL);

