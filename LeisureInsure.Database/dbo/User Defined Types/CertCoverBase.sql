﻿CREATE TYPE [dbo].[CertCoverBase] AS TABLE (
    [QuoteFK]        INT             NULL,
    [RateFK]         INT             NULL,
    [CoverFK]        INT             NULL,
    [QuoteDetailPK]  INT             IDENTITY (1, 1) NOT NULL,
    [LineType]       VARCHAR (250)   NULL,
    [Base]           NUMERIC (12, 2) NULL,
    [Rate]           NUMERIC (8, 2)  NULL,
    [Charge]         NUMERIC (8, 2)  NULL,
    [RunningTotal]   NUMERIC (12, 2) NULL,
    [TamPolicyIndex] VARCHAR (50)    NULL,
    [Trans]          INT             NULL,
    [Lock]           INT             NULL,
    [ListIndex]      INT             NULL,
    [RateValue]      VARCHAR (250)   NULL,
    [InputFK]        INT             NULL,
    [CurrencySymbol] VARCHAR (5)     NULL,
    [Multiplier]     DECIMAL (5, 3)  NULL,
    [Territory]      VARCHAR (250)   NULL,
    [InputPK]        INT             NULL,
    [InputTypeFK]    INT             NULL,
    [InputName]      VARCHAR (250)   NULL);

