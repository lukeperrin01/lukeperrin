﻿CREATE TYPE [dbo].[Charges] AS TABLE (
    [InputPK]  INT             NOT NULL,
    [CoverPK]  INT             NOT NULL,
    [RatePK]   INT             NOT NULL,
    [InvLevel] INT             NULL,
    [LineType] VARCHAR (150)   NOT NULL,
    [Base]     NUMERIC (12, 2) NOT NULL,
    [Rate]     NUMERIC (8, 2)  NOT NULL,
    [Charge]   NUMERIC (12, 2) NOT NULL,
    [ListNo]   INT             NULL);

