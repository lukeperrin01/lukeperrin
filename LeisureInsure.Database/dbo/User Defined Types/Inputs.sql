﻿CREATE TYPE [dbo].[Inputs] AS TABLE (
    [CoverPK]     INT           NULL,
    [ListNo]      INT           NULL,
    [ListOrdinal] INT           NULL,
    [InputName]   VARCHAR (150) NULL,
    [ParentPK]    INT           NULL,
    [InputTypeFK] INT           NULL,
    [InputPK]     INT           NULL,
    [RatePK]      INT           NULL,
    [RateValue]   VARCHAR (150) NULL,
    [RateString]  VARCHAR (150) NULL,
    [InputString] VARCHAR (150) NULL,
    [InputPKLock] INT           NULL);

