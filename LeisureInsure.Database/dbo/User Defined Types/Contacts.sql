﻿CREATE TYPE [dbo].[Contacts] AS TABLE (
    [QuotePK]     INT           NOT NULL,
    [FirstName]   VARCHAR (50)  NOT NULL,
    [LastName]    VARCHAR (50)  NOT NULL,
    [Telephone]   VARCHAR (50)  NULL,
    [Email]       VARCHAR (50)  NULL,
    [ContactType] INT           NOT NULL,
    [EntityType]  CHAR (1)      NULL,
    [EntityName]  VARCHAR (150) NULL);

