﻿CREATE TYPE [dbo].[Address] AS TABLE (
    [QuotePK]  INT          NOT NULL,
    [Address1] VARCHAR (60) NOT NULL,
    [Address2] VARCHAR (60) NULL,
    [Address3] VARCHAR (60) NULL,
    [Town]     VARCHAR (50) NOT NULL,
    [County]   VARCHAR (50) NULL,
    [Postcode] VARCHAR (12) NOT NULL,
    [Locale]   VARCHAR (12) NOT NULL);

