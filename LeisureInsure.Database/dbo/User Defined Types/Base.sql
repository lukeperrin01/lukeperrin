﻿CREATE TYPE [dbo].[Base] AS TABLE (
    [PolicyPK]    INT             NULL,
    [CoverPK]     INT             NULL,
    [Section]     INT             NULL,
    [ParentPK]    INT             NULL,
    [InputPK]     INT             NULL,
    [InputTypeFK] INT             NULL,
    [RatePK]      INT             NULL,
    [RateValue]   VARCHAR (500)   NULL,
    [RateName]    VARCHAR (150)   NULL,
    [Rate]        NUMERIC (18, 3) NULL,
    [DivBy]       INT             NULL,
    [Threshold]   NUMERIC (18, 3) NULL,
    [InputString] VARCHAR (150)   NULL,
    [ListNo]      INT             NULL,
    [ListOrdinal] INT             NULL,
    [CalcTypeFK]  INT             NULL,
    [CalcOrdinal] NUMERIC (6, 3)  NULL,
    [CalcSP]      VARCHAR (50)    NULL,
    [complete]    INT             NULL,
    [Base]        NUMERIC (18, 3) NULL,
    [Charge]      NUMERIC (18, 3) NULL);

