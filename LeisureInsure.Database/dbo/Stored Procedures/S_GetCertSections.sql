﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_GetCertSections]
	@QuotePK INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--DECLARE	@QuotePK int  = 100001

DECLARE @PolicyType char = (select top 1 policytype from Policies p inner join Quotes q on p.PolicyPK = q.PolicyFK AND QuotePK = @QuotePK)
DECLARE @LocalePK int = (select top 1 LocaleId from Quotes where QuotePK = @QuotePK)
;with S1 AS
(
Select
	c.CoverPK,
	Section,
	1 AS insured,
	LocalePK,
	R.CoverCalcFK,
	qd.RatePK,
	r.TableFirstColumn,
	qd.InputFK
	--C.Section
FROM Covers C
INNER JOIN Quotes q
	on q.quotePK = @quotepk
INNER JOIN Locales L
	on q.LocaleId = l.LocalePK
INNER JOIN QuoteInputs QD
	on qd.QuoteId = @quotepk
INNER JOIN Rates R
	ON R.CoverCalcFK = c.coverPK
	and R.RatePK = QD.RatePK

),
freeCovers as
(
Select 
	c.Section
from Rates  r
inner join S1
	on s1.TableFirstColumn = r.TableChildren
	and CoverBW = r.CoverBW
INNER JOIN Covers C
	ON C.CoverPK = R.CoverCalcFK
Group by
	c.Section
)
,
CoverDistinct AS
(
SELECT
	Section 
FROM S1
GROUP BY
	Section 
UNION 
SELECT 
	Section
FROM freeCovers

)
,
SI AS
(
SELECT 
	1 AS HeaderLevel,
	c.Section,
	ISNULL(insured,0) AS Insured, 
	CASE

		WHEN C.SECTX IS NULL 
		THEN c.CoverName + ': Not Insured'
		ELSE c.CoverName + ': Insured'
	END AS Header,
	LocalePK
FROM 
(
select 
	C.CoverName,
	CD.Section AS SECTX,
	C.Section,
	C.CoverPK
from Covers c
LEFT JOIN CoverDistinct CD
	ON C.Section = CD.Section
where 
	c.CoverTip like '%' + @PolicyType + '%'

)C
LEFT JOIN S1
	on S1.section = c.section
	OR S1.CoverCalcFK = C.CoverPK 
WHERE
	c.section > 1 
GROUP BY
	SECTX,
	c.Section,
	s1.Insured,
	c.CoverName,
	LocalePK
)
--SELECT * FROM SI 

,
Lines AS
(
SELECT
	SI.*,
	Label1,

	CASE 
		WHEN @LocalePK = 1
		THEN SpanUK1
		ELSE SpanEire1
	END AS LineValue1,
	CASE 
		WHEN @LocalePK = 1
		THEN SpanUK2
		ELSE SpanEire2
	END AS LineValue2,
	
	LineGroup,
	
	ordinal,
	sh.InputFK,
	paraAfter
FROM SI
Left JOIN SectionHeaderLines SH
	on sh.SectionNumber = si.section


)
--SELECT * FROM Lines 

SELECT
	HeaderLevel,
	l.Section,
	case
		when cd.Section is null 
		then 0
		else 1
	END AS Insured,
	Header,
	@LocalePK AS LocalePK,
	ltrim(Label1) as Label1,
	--LineValue1,
	ISNULL(REPLACE(LineValue1, (select Convert(varchar(50),SUBSTRING(LineValue1, (CHARINDEX('@', LineValue1)) , (CHARINDEX('|', LineValue1) - CHARINDEX('@', LineValue1 )+1)))),(SELECT DBO.F_CertValues(@QuotePK,inputfk))),'') AS LineValue1,
	ISNULL(REPLACE(LineValue2, (select Convert(varchar(50),SUBSTRING(LineValue2, (CHARINDEX('@', LineValue2)) , (CHARINDEX('|', LineValue2) - CHARINDEX('@', LineValue2 )+1)))),(SELECT DBO.F_CertValues(@QuotePK,inputfk))),'') AS LineValue2,
	--LineValue2,
	LineGroup,
	ordinal,
	isnull(paraAfter,0) AS Para
FROM Lines L
LEFT JOIN CoverDistinct CD
	ON cd.Section = l.Section
ORDER BY
	l.Section, 
	ordinal



END