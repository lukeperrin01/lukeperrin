﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_CertLists]
	@QuotePK int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
/*

DECLARE  @QuotePK INT = 100010
*/	
--set @QuotePK  = 100001
DECLARE  @PLWWIDECOVER INT  = ISNULL((SELECT RATEFK FROM QUOTEDETAILS WHERE RATEFK = 920 AND QuoteFK =  @QuotePK),0)

declare @BIIndemnPeriodGrossProfit int = ISNULL((SELECT RATEFK FROM QUOTEDETAILS WHERE inputFK = 64 AND QuoteFK =  @QuotePK),0)

declare @BIIndemnPeriodICW int = ISNULL((SELECT RATEFK FROM QUOTEDETAILS WHERE inputFK = 66 AND QuoteFK =  @QuotePK),0)

DECLARE  @MDCupsWWIDECOVER INT  = ISNULL((SELECT RATEFK FROM QUOTEDETAILS WHERE RATEFK = 920  AND QuoteFK =  @QuotePK),0)


;with Quote AS
(
SELECT TOP 1 
	Q.*,
	l.CurrencySymbol,
	l.Country 
FROM Quotes  q
inner join Locales l
	on  q.LocaleId = l.LocalePK
	
WHERE 
	QuotePK =  @QuotePK
),
detail AS
(
SELECT
	convert(int,ListNo * -1) AS RowNo,
	qd.*,
	c.Section,
	c.Specification,
	--(Select top 1 country from Quote) AS Territory,
	CASE 
		WHEN qd.CoverPK = 2 AND @PLWWIDECOVER > 0
		THEN 'Worldwide'
		else q.country
	END AS Territory,
	RateName,
	CoverName
FROM QuoteInputs QD
INNER JOIN Rates R
	ON qd.RatePK = r.RatePK
INNER JOIN Covers C
	ON r.CoverCalcFK= C.CoverPK
INNER JOIN Quote Q
	on q.QuotePK = qd.QuoteId
WHERE 
	QuoteId =  @QuotePK
)
--select * from detail

,
PL AS
(
select 
	ListNo,
	listordinal,
	CoverpK AS CoverFK,
	Section,
	InputString AS LineType,
	Specification,
	Territory,
	0 AS SumInsured
from detail
WHERE 
	CoverPK = 2
	AND ListNo < 0
),
MD AS
(
select 
	ListNo,
	listordinal,
	CoverpK AS CoverFK,
	Section,
	CASE 
		WHEN listordinal < 0
		THEN (select top 1 RateName from detail dd where dd.listno = d.listordinal AND dd.CoverPK = d.CoverPK)
		ELSE INPUTSTRING
	END  AS LineType,
	Specification,
	Territory,

	CASE 
		WHEN listordinal < 0
		THEN SumInsured
		ELSE DBO.fnRemoveNonNumericCharacters(RateValue)
	END  AS SumInsured
from detail d
WHERE 
	SECTION = 4
),
EL AS
(
select 
	ListNo,
	listordinal,
	CoverpK AS CoverFK,
	Section,
	RateName AS LineType,
	
	Specification,
	Territory,
	SumInsured
from detail d
WHERE 
	SECTION = 3
	and RowNo > 0
),
U AS
(
	SELECT 
		*
	FROM PL 
union all 
	SELECT 
		*
	FROM MD
union all 
	SELECT 
		*
	FROM EL
)
SELECT
	convert(int,ListNo * -1) AS RowNo,
	CoverFK,
	Section,
	LineType,
	Specification,
	Territory,
	CurrencySymbol + replace(CONVERT(varchar(25),CAST((SumInsured) AS MONEY),1),'.00','')  AS SumInsured
FROM U 
INNER JOIN Quote Q
	ON q.QuotePK = @QuotePK
END