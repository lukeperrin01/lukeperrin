﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_MaxMinCharge]
	@InputPK int,
	@LocalePK int,
	@BaseAll dbo.Base Readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
WITH EL AS
(
SELECT
	PolicyPK,
	InputPK,
	CoverPK,
	RatePK,
	1 AS InvLevel,
	InputString AS LineType,
	dbo.fnRemoveNonNumericCharacters(RateValue) AS Base,
	Rate,
	(Rate * dbo.fnRemoveNonNumericCharacters(RateValue))/DivBy AS Charge,
	Threshold,
	ListNo
FROM @BaseALL
where DivBy > 0
),
SumEL AS
(
	SELECT
	CoverPK,

		MIN(InputPK) as InputPK,
		MIN(RatePK) AS RatePK,
		Max(PolicyPK) AS PolicyPK,

		SUM(Charge) AS SumCharges,
		MAX(Threshold) AS MinPremium,
		SUM(Base) AS SumBase,
		CASE
			WHEN MAX(Threshold) > SUM(Charge)
			THEN 1
			ELSE 0
		END AS MinCharge
	FROM EL
	GROUP BY 
		CoverPK
	
	
),
MINPremium AS
(
	SELECT
	InputPK,
	c.CoverPK,
	RatePK,
	1 AS InvLevel,
	'Minimum ' + ISNULL(c.SectionName,CoverName) + ' Liability Premium' AS LineType,
	SumBase AS Base,
	0 AS Rate,
	CASE
		WHEN INPUTPK = 48
		THEN ISNULL(dbo.F_MinRate(SumCharges,@LocalePK,c.bitwise,p.bitwise),MinPremium) 
		ELSE MinPremium
		
	END AS Charge,
	
	--CASE 
	--	WHEN PolicyPK = 1 AND SumBase < 10001 and c.coverpk = 2
	--	THEN 75.00
	--	ELSE MinPremium 
	--END AS Charge,
	MinCharge
	FROM SumEL
	INNER JOIN Covers c
		ON SumEL.CoverPK = c.coverPK
		
	INNER JOIN Policies P
		on p.policyPK = SumEL.PolicyPK
	

),
UnionEL AS
(
SELECT
	InputPK,
	CoverPK,
	RatePK,
	InvLevel,
	LineType,
	Base,
	Rate,
	Charge,
	1 AS ListNo
FROM MINPremium
WHERE 
	MinCharge = 1
UNION ALL
SELECT
	el.InputPK,
	el.CoverPK,
	el.RatePK,
	el.InvLevel,
	el.LineType,
	CASE
		WHEN s.MinCharge = 0
		THEN el.Base
		ELSE 0
	END AS Base,
	CASE
		WHEN s.MinCharge = 0
		THEN el.Rate
		ELSE 0
	END AS Rate,
	CASE
		WHEN s.MinCharge = 0
		THEN el.Charge
		ELSE 0
	END AS Charge,
	el.ListNo
FROM EL
INNER JOIN SumEL S
	ON s.coverPK = el.coverPK
)
SELECT
	InputPK,
	CoverPK,
	RatePK,
	InvLevel,
	LineType,
	ISNULL(Base,0) AS Base,
	Rate,
	ISNULL(Charge,0) AS Charge,
	ListNo
FROM UnionEL
where ISNULL(Charge,0) != 0

END