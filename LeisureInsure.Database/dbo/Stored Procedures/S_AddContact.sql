﻿CREATE PROCEDURE [dbo].[S_AddContact]
	@Contact Contacts ReadOnly
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @ContactPK INT
DECLARE @QuotePK INT = (select TOP 1 QuotePK from @Contact)
INSERT INTO Contacts
(
	QuotePK,
	FirstName,
	LastName,
	Telephone,
	Email,
	ContactType,
	EntityType,
	EntityName
)
Select 
	QuotePK,
	FirstName,
	LastName,
	Telephone,
	Email,
	ContactType,
	EntityType,
	EntityName
FROM @Contact
WHERE 
ContactType = 1
set @ContactPK = (select SCOPE_IDENTITY())
declare @cpk int = @ContactPK

update Quotes
set 
	CustomerFK = @ContactPK
where 
	QuotePK = @QuotePK


INSERT INTO Contacts
(
	QuotePK,
	FirstName,
	LastName,
	Telephone,
	Email,
	ContactType,
	EntityType,
	EntityName
)
Select 
	QuotePK,
	FirstName,
	LastName,
	Telephone,
	Email,
	ContactType,
	EntityType,
	EntityName
FROM @Contact
WHERE 
	ContactType != 1
	
SELECT @cpk
	
END