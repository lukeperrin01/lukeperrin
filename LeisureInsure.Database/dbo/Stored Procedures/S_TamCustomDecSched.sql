﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_TamCustomDecSched]
	-- Add the parameters for the stored procedure here
	@QuotePK AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
/*
DECLARE  @QuotePK INT = 100001
;

*/
	
	
with Base AS
(
    SELECT
		--isnull(X.RATEVALUE,0) AS Indemnity,
		Inputpk,
		RATEFK,
		InputValue,
		InputType,
		--ISNULL((SELECT CAST(InputValue AS MONEY) FROM QuoteDetails WHERE InputFK = INPUTPK AND CoverFK = CoverPK),0) as INDEMNITY,
		CASE
			WHEN Inputpk = 11
			THEN ISNULL((SELECT CAST(InputValue AS MONEY) FROM QuoteDetails WHERE InputFK = INPUTPK AND CoverFK = CoverPK),0) 
			ELSE 0
		END as INDEMNITY,
		charge,
		i.CoverBW,
		q.CoverFK, 
 		LineType, 
		CASE
			WHEN Inputpk in (18,28) AND ISNUMERIC(InputValue) = 1
			THEN ISNULL((SELECT SUM(CAST(InputValue AS MONEY)) FROM QuoteDetails WHERE  CoverFK = CoverPK AND (ListNo * ListOrdinal > -1 OR InputFK = 28)),0) 
			ELSE 0
		END AS SumInsured,
		--0 AS indemnity,
		q.Rate, 
		0 AS Discount,
		Charge AS Premium,
		0 AS Excess,
		c.Specification AS Specification
    FROM QuoteDetails q
  --  INNER JOIN Rates r
		--on r.ratePK = q.rateFK
		--or q.ratefk = -1
	INNER JOIN Inputs i 
		on Q.InputFK = i.inputPK
	INNER JOIN Covers C
		on c.coverpk = Q.coverFK

    WHERE
		QuoteFK = @QuotePK
		and coverPk > 1
		and (charge != 0
		OR ListNo * ListOrdinal > -1)
),
A AS
(
    SELECT

		RATEFK,
		2 as cdkey,
		CoverFK, 
 		LineType, 
		SumInsured,
		indemnity,
		Rate, 
		Discount,
		Premium,
		Excess,
		Specification
    FROM Base

),
B AS 
(
    SELECT
		0 AS RATEPK,
		1 AS cdkey,
		A.CoverFK, 
		CASE 
			WHEN c.SectionName != ''
			THEN c.CoverName + ' - ' + c.SectionName
			ELSE c.CoverName
		END AS LineTypeX,
		Sum(convert(numeric(18,2),a.SumInsured)) AS SumInsured,
		Sum(Indemnity) AS Indemnity,
		0 AS Rate, 
		Sum(convert(numeric(18,2),a.Discount)) AS  Discount,
		Sum(convert(numeric(18,2),a.Premium)) AS Premium,
		0 AS Excess,
		'' AS Specification
    FROM  a
	INNER JOIN Covers C
		on c.coverpk = a.coverfk
	--LEFT JOIN 
	--(
	--SELECT 
	--convert(numeric(18,0),RATEVALUE) AS ratevalue,
	--RATEPK,
	--I.COVERFK
	--FROM Rates r
	--INNER JOIN QuoteDetails q
	--	on r.ratePK = q.rateFK
	--	AND R.PolicyInputFK = 11
	--INNER JOIN Inputs i 
	--	on r.PolicyInputFK = i.inputPK
	--	and InputPK = 11
	--) X ON X.RATEPK = A.RATEFK AND X.COVERFK = C.CoverPK
		--and cdkey =1
	GROUP BY
		A.CoverfK,
		c.SectionName,
		c.CoverName

),
U AS
(
	SELECT * FROM A
	UNION
	SELECT * FROM B
	



)

select * from u
order by coverfk, cdkey





END