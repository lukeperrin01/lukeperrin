﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_ProductPremiumOnNetPLPremium]
	@RatePK int,
	@NetPremium decimal(18,3),
	@CoverPK int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

		SELECT
			InputFK AS InputPK,
			@CoverPK AS CoverPK,
			RatePK,
			1 AS InvLevel,
			RateName,
			@NetPremium AS Base,
			Rate,
			(@NetPremium * Rate)/DivBy AS Charge,
			0 AS ListNo
		FROM Rates R
		WHERE 
			r.RatePK = @RatePK
	

END