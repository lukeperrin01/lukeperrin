﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE S_UpdateInputPolicyBW
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  with a as
  (
  Select 
  inputfk,
  policybw 
  from rates
  group by
    inputfk,
  policybw 
  ),
  b as
  (
  select 
  inputfk,
  SUM(policybw) as pbw
  from a
    group by
    inputfk
  )
  update Inputs 
  set PolicyBW = b.pbw
  --select i.PolicyBW, b.* 
  from b
  inner join Inputs i
  on i.InputPK = b.InputFK 
  where 
  i.PolicyBW != b.pbw
  and b.pbw > 0
END