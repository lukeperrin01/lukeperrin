﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_PL_FirstPrice]
	@InputPK int,
	@LocalePK int,
	@BaseAll dbo.Base Readonly	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
WITH A AS
(
	SELECT
		ROW_NUMBER() OVER(ORDER BY LISTNO * -1) AS RowNo,
		q.CoverPK,
		q.InputString,
		q.ListNo,
		q.InputPK AS InputFK,
		r.RatePK,
		r.Rate,
		r.Threshold,
		x.NoItems
	FROM RATES R
	INNER JOIN @BaseAll Q
		on r.RatePK = q.RatePK
		--AND q.CoverPK = 2
		and q.ListNo < 0
	INNER JOIN 
	(
		SELECT
			CONVERT(INT,RateValue) AS NoItems,
			ListOrdinal
		FROM QuoteInputs
		WHERE
			InputFK = @InputPK
	)x on x.ListOrdinal = q.ListNo
),
B AS
(
	SELECT TOP 1
		RowNo,
		CoverPK,
		InputFK,
		RatePK,
		Threshold AS Rate,
		1 AS NoItems,
		Threshold  AS Charge
	FROM A
	WHERE
		Threshold = (SELECT MAX(Threshold) FROM A)
	GROUP BY
		InputFK,
		RowNo,
		CoverPK,
		RatePK,
		Threshold
),
C AS
(
	SELECT 
		RowNo,
		InputFK,
		CoverPK,
		RatePK,
		Rate,
		NoItems,
		Charge
	FROM B
	
	UNION
	
	SELECT
		a.RowNo,
		a.InputFK,
		a.CoverPK,
		a.RatePK,
		a.Rate,
		a.NoItems - ISNULL(b.NoItems,0) AS NoItems ,
		a.Rate * (a.NoItems - ISNULL(b.NoItems,0)) AS Charge
	FROM A
	LEFT jOIN B 
		ON a.RatePK = b.RatePK
		--ON A.ROWNO = B.ROWNO
		--AND a.RatePK = b.RatePK
	WHERE
		a.NoItems > 1
),
d AS
(
	SELECT
		c.InputFK,
		c.CoverPK,
		c.RatePK,
		1 AS InvLevel,
		a.InputString AS LineType,
		c.NoItems AS Base,
		c.Rate,
		c.Charge,
		a.ListNo
	FROM C 
	INNER JOIN A 
		ON a.ROWNO = c.ROWNO
),
E AS
(
	SELECT
		InputFK,
		CoverPK,
		RatePK,
		InvLevel,
		LineType,
		Base,
		Rate,
		Charge,
		ListNo
	FROM d 

UNION ALL

	SELECT
		a.InputPK,
		a.CoverPK,
		a.RatePK,
		1 AS InvLevel,
		a.InputString AS LineType,
		(SELECT SUM(Charge)FROM D) AS Base,
		r.Rate,
		((SELECT SUM(Charge)FROM D) *r.Rate)/r.DivBy AS Charge,
		0 AS ListNo
	FROM @BaseAll A
	INNER JOIN RATES R
		ON a.RatePK = r.RatePK
		and a.CalcOrdinal = 2
		
		
)
SELECT TOP 1000
	InputFK,
	CoverPK,
	RatePK,
	InvLevel,
	LineType,
	Base,
	Rate,
	Charge,
	ListNo
FROM E
WHERE
	Charge !=0
ORDER BY
	Invlevel, 
	RatePK,
	Base


END