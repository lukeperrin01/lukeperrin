﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_PL_Turnover]
	@InputPK int,
	@LocalePK int,
	@BaseAll dbo.Base Readonly	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
with baseCalc AS
(
SELECT
	InputPK,
	ListNo,
	ListOrdinal,
	CoverPK,
	InputString,
	ParentPK,
	RatePK,
	RateName,
	(Select dbo.fnRemoveNonNumericCharacters(RateValue)  FROM @BaseALL where CalcOrdinal = -1) AS RateValue,
	Rate,
	DivBy,
	Threshold
FROM @BaseALL
WHERE 
	ListNo < 0



),
net AS
(
SELECT
	r.InputPK,
	r.CoverPK,
	r.RatePK,
	1 AS InvLevel,
	r.InputString,
	RateValue AS Base,
	Rate,
	CASE 
	WHEN ((Rate * RateValue)/DivBy ) < Threshold
	THEN Threshold
	ELSE ((Rate * RateValue)/DivBy )
	END AS Charge,
	ListNo
FROM baseCalc R
),
IndemnAdj AS
(
SELECT
	InputPK,
	CoverPK,
	RatePK,
	InvLevel,
	InputString,
	Base,
	Rate,
	Charge,
	ListNo
FROM net

UNION ALL

SELECT
	InputPK,
	CoverPK,
	RatePK,
	1 AS InvLevel,
	InputString,
	(SELECT SUM(Charge) FROM net) AS Base,
	Rate,
	((Rate * (SELECT SUM(Charge) FROM net))/DivBy ) AS Charge,

	ListNo
FROM @BaseALL
WHERE 
	CalcOrdinal = 2

)
SELECT
	InputPK,
	CoverPK,
	RatePK,
	InvLevel,
	InputString,
	Base,
	Rate,
	Charge,
	ListNo
FROM IndemnAdj


END