﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_Reset]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
/*

DELETE CONTACTS WHERE ISNULL(LOCK,0) = 0
DECLARE  @MAXCONTACTPK INT = ISNULL((SELECT MAX(CONTACTPK) FROM CONTACTS),0)
DBCC CHECKIDENT ('CONTACTS', RESEED, @MAXCONTACTPK)

DELETE QUOTES WHERE ISNULL(LOCK,0) = 0
DECLARE  @MAXQUOTEPK INT = ISNULL((SELECT MAX(QUOTEPK) FROM QUOTES),100000)
DBCC CHECKIDENT ('QUOTES', RESEED,@MAXQUOTEPK)

DELETE QUOTEDETAILS WHERE ISNULL(LOCK,0) = 0
DECLARE  @QUOTEDETAILPK INT = ISNULL((SELECT MAX(QUOTEDETAILPK) FROM QUOTEDETAILS),0)
DBCC CHECKIDENT ('QUOTEDETAILS', RESEED, @QUOTEDETAILPK)

DELETE ADDRESSES WHERE ISNULL(LOCK,0) = 0
DECLARE  @ADDRESSPK INT = ISNULL((SELECT MAX(ADDRESSPK) FROM ADDRESSES),0)
DBCC CHECKIDENT ('ADDRESSES', RESEED, @ADDRESSPK)

DELETE CERTS WHERE ISNULL(LOCK,0) = 0
DECLARE  @CertificatePK INT = ISNULL((SELECT MAX(CertificatePK) FROM CERTS),0)
DBCC CHECKIDENT ('CERTS', RESEED, @CertificatePK)

DELETE INSTALMENTS WHERE ISNULL(LOCK,0) = 0
DECLARE  @InstallmentPK INT = ISNULL((SELECT MAX(InstallmentPK) FROM INSTALMENTS),0)
DBCC CHECKIDENT ('INSTALMENTS', RESEED, @InstallmentPK)

  truncate table quoteinputs
  
    truncate table quotelines
*/

/*
DECLARE @maxVal INT
SELECT @maxVal = ISNULL(max(RatePK),0) from Rates 
DBCC CHECKIDENT('Rates', RESEED, @maxVal)

update quotes
set tamclientcode = null

update quotedetails
set tampolicyindex = null,
tamtrans = null,
ico = null

*/
SELECT  
	p.BitWise, 
	PolicyName, 
	PolicyPK, 
	PolicyType,
	CoverName
FROM Policy  P
inner join Ratesx R
	on (r.PolicyFK & p.BitWise ) > 0
INNER JOIN Cover C
	ON C.CoverPK = R.CoverFK
where 
	PolicyFK != -1
group by 
	p.BitWise, 
	PolicyName, 
	PolicyPK, 
	PolicyType,
	CoverName,
	CoverPK
ORDER BY 
	PolicyPK, 
	CoverPK

SELECT * FROM CERTS
SELECT * FROM QUOTES
SELECT * FROM QUOTEDETAILS
SELECT * FROM ADDRESSES
SELECT * FROM CONTACTS




END