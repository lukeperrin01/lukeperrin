﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_CreateSchedule]
	-- Add the parameters for the stored procedure here
	@QuotePK INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SET CONCAT_NULL_YIELDS_NULL OFF
    
    --DECLARE @QuotePK INT = 100001
    
	;with Quote AS
	(
		SELECT
			CASE 
				WHEN ISNULL(q.BIHA,0) = 0 AND l.strlocale = 'en-gb' 
				THEN 'S&LC 0915 - LI UK'
				WHEN ISNULL(q.BIHA,0) = 0 AND l.strlocale = 'ga-IE' 
				THEN 'S&LC 0915 - LI IRE'
				WHEN ISNULL(q.BIHA,0) != 0 AND l.strlocale = 'en-gb' 
				THEN 'S&LC 0915 - LI UK (AFFINITY)'
				WHEN ISNULL(q.BIHA,0) != 0 AND l.strlocale = 'ga-IE' 
				THEN 'S&LC 0915 - LI IRE (AFFINITY)'
		END AS PolicyWording,
		CONVERT(varchar(25),ISNULL(TAMPolicyCode,q.QuotePK)) AS PolicyNumber,
		DateFrom AS StartDate,
		DateTo AS EndDate,
		CONVERT(varchar(25),cast(r.rate AS money),1) as taxRate,
		c.FirstName + ' ' + c.LastName  AS InsuredName,
		CASE
			WHEN l.LocalePK = 1
			THEN 'Insurance Premium Tax (' + CONVERT(varchar(25),cast(r.rate AS money),1) + '%)'
			ELSE 'Irish Government Levy (' + CONVERT(varchar(25),cast(r.rate AS money),1) + '%)'
		End AS TaxType,
		currencySymbol + CONVERT(varchar(25),cast(q.NetPremium AS money),1) AS NetPremium,
		currencySymbol + CONVERT(varchar(25),cast(((r.rate * q.NetPremium)/100)  AS money),1) AS Tax,
		currencySymbol + CONVERT(varchar(25),cast(q.NetPremium + q.fee + ((r.rate * q.NetPremium)/100) AS money),1) AS GrossPremium,
		currencySymbol + CONVERT(varchar(25),cast(q.fee AS money),1) AS Fee,
		p.PolicyName,
		q.AddressFK,
		q.PremiseFK,
		l.Territory,
		q.Purchased
		FROM Quotes Q
		INNER JOIN Locales L
			ON l.LocalePK = q.LocaleId
			AND q.QuotePK = @QuotePK
		INNER JOIN Rates R
			on r.LocaleBW = l.LocalePK
			AND R.CoverBW = -2
			AND RateExclude = 0
			AND ISNULL(r.validUntil,GETDATE()-1)< GETDATE()
		LEFT JOIN Contacts C
			ON c.ContactPK = q.CustomerFK
		INNER JOIN Policies P
			ON p.PolicyPK = q.PolicyFK
	
	),
	A AS
	(
	SELECT
		q.PolicyWording,
		q.policyNumber,
		q.StartDate,
		q.EndDate,
		q.InsuredName,
		case 
			when a.Address1 is not null 
			then a.Address1 + ',' + ' ' +
			(case 
			when a.Address2 is not null 
			then a.Address2 + ',' + ' ' +
			(case 
			when a.Address3 is not null 
			then a.Address3 + ',' +' ' +
			(case 
			when a.Address3 is not null 
			then a.Address3 + ',' + ' ' 
			END)END)END)
		+ a.Town + ',' + ' ' + A.County + ',' + ' ' + UPPER(A.Postcode)
		else 'Test Add'
		END as CorrespondenceAddress,
			case 
				when q.premiseFK > 0
				Then 
					case 
					when a.Address1 is not null 
					then a.Address1 + ',' + ' ' +
					(case 
					when a.Address2 is not null 
					then a.Address2 + ',' + ' ' +
					(case 
					when a.Address3 is not null 
					then a.Address3 + ',' +' ' +
					(case 
					when a.Address3 is not null 
					then a.Address3 + ',' + ' ' 
					END)END)END)END
			+ a.Town + ',' + ' ' + A.County + ',' + ' ' + UPPER(A.Postcode)
			else 'Test Premise' 
		END AS Premises,
		q.PolicyName,
		q.taxrate,
		q.Territory,
		q.TaxType,
		q.Purchased,
		q.NetPremium,
		q.Tax,
		q.GrossPremium,
		q.Fee
		FROM Quote Q
		Left JOIN Addresses A
			ON A.AddressPK = q.AddressFK
	)
SELECT
	PolicyWording,
	PolicyNumber,
	StartDate,
	EndDate,
	InsuredName,
	CorrespondenceAddress,
	Premises,
	PolicyName,
	NetPremium,
	TaxRate,
	Tax,
	GrossPremium,
	Fee,
	Territory,
	TaxType,
	Purchased
FROM A
	
/*
	
	SELECT
	'S&LC 0915 - LI UK' AS PolicyWording,
	'100001' AS PolicyNumber,
	getDate() AS StartDate,
	getDate() AS EndDate,
	'test' AS InsuredName,
	'Test Add' AS CorrespondenceAddress,
	'Test Premise' AS Premises,
	'Showmen' AS PolicyName,
	'£987.66' AS NetPremium,
	'£87.28' AS Tax,
	'£1,074.94' AS GrossPremium,
	'£68.91' AS Fee,
	'United Kingdom of Great Britain & Northern Ireland, the Isle of Man & the Channel Isles' AS Territory,
	'Insurance Premium Tax (9.5%)' AS TaxType
	
	
	FROM A
*/
	
END