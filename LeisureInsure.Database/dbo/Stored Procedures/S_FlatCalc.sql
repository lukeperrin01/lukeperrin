﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_FlatCalc]
	-- Add the parameters for the stored procedure here
	@InputPK int,
	@LocalePK int,
	@BaseAll dbo.Base Readonly	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
with baseCalc AS
(
SELECT
	InputPK,
	ListNo,
	ListOrdinal,
	CoverPK,
	InputString,
	ParentPK,
	RatePK,
	RateName,
	dbo.fnRemoveNonNumericCharacters(RateValue) AS RateValue,
	Rate,
	DivBy,
	Threshold
FROM @BaseALL
WHERE 
	CALCORDINAL <= 1 
	AND CALCTYPEFK =1 


)
SELECT TOP 1000
	r.InputPK,
	r.CoverPK,
	r.RatePK,
	1 AS InvLevel,
	r.InputString,
	Rate AS Base,
	Rate,
	Rate AS Charge,
	ListNo
FROM baseCalc R

END