﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[S_AddAddress]
	@Address Address ReadOnly
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @AddressPK INT
DECLARE @QuotePK INT = (select TOP 1 QuotePK from @Address)
DECLARE @ContactPK INT = (Select CustomerFK from Quotes where QuotePK = @QuotePK)
DECLARE @Loc VARCHAR(12) = (select Loc from Quotes where QuotePK = @QuotePK)

INSERT INTO Addresses
(
	ContactFK,
	Address1,
	Address2,
	Address3,
	Town,
	County,
	Country,
	Locale,
	Postcode
)
SELECT
	@ContactPK,
	Address1,
	Address2,
	Address3,
	Town,
	County,
	(SELECT Country from Locales where strLocale = @Loc),
	Locale,
	Postcode
FROM @Address
set @AddressPK = (select SCOPE_IDENTITY())

update Quotes
set 
	AddressFK = @AddressPK
where 
	QuotePK = @QuotePK

END