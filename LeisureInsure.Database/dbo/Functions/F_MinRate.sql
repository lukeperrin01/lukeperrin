﻿CREATE FUNCTION [dbo].[F_MinRate]
(
	@SumInput int,
	@LocalePK int,
	@CoverBW int,
	@PolicyBW int
)
RETURNS decimal(12,2)
BEGIN
Declare @Return decimal(12,2)
Select @Return = 
(
select
	CASE
		WHEN (l.multiplier * @SumInput) < (l.multiplier * r.threshold)
		THEN r.Rate * l.multiplier
		ELSE null
	end as Threshold
from inputs i
inner join rates r
	on i.inputPK = 48
	and (i.coverbw & @coverbw)>0
	and (i.policyBW & @PolicyBW )>0
	and r.inputfk = i.inputPK
	and (r.policyBW & @PolicyBW )>0
INNER JOIN Locales L
	on l.localePK = @LocalePK
		)
return @Return

END