﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_Calcs]
(	
	@PolicyPK int,
	@CalcLevel int
)
RETURNS TABLE 
AS
RETURN 


(
/*
DECLARE @PolicyPK int = 4
DECLARE @@CalcLevel int = 2
*/

	SELECT
		ROW_NUMBER() OVER(ORDER BY CoverFK,CalcOrdinal,FunctionName) AS RowNo
      ,[PolicyFK]
      ,[CoverFK]
      ,[CalcOrdinal]
      ,[CalcOrdinalDescription]
      ,[InputFKOne]
      ,[InputFKTwo]
      ,[FunctionName]
      ,[inputParam]
      ,[calclevel]
	From Calcs
	WHERE 
		PolicyFK = @PolicyPK
		AND CalcLevel = @CalcLevel
		
)