﻿CREATE FUNCTION [dbo].[F_CertValues]
(
	@QuotePK int,
	@InputPK int
)
RETURNS VARCHAR(50)
BEGIN
Declare @Return VARCHAR(50)
DECLARE @RatePK int = (SELECT top 1 RatePK From QuoteInputs where quoteID = @QuotePK and inputfk = @InputPK)
Select @Return = 
(
select
CASE 
	WHEN @InputPK = 11
	THEN (SELECT top 1 replace(CONVERT(varchar(25),CAST((RateName ) AS MONEY),1),'.00','') FROM Rates WHERE RATEPK = @RatePK)
	ELSE (SELECT '1')
	END
		)
return @Return

END