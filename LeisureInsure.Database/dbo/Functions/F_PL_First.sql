﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_PL_First]
(	
	@InputPK int,
	@BaseAll dbo.Base Readonly
)
RETURNS TABLE 
AS
RETURN 
(
WITH A AS
(
	SELECT
		ROW_NUMBER() OVER(ORDER BY LISTNO * -1) AS RowNo,
		q.CoverPK,
		q.InputString,
		q.ListNo,
		q.InputPK AS InputFK,
		r.RatePK,
		r.Rate,
		r.Threshold,
		x.NoItems
	FROM RATES R
	INNER JOIN @BaseAll Q
		on r.RatePK = q.RatePK
		AND q.CoverPK = 2
		and q.ListNo < 0
	INNER JOIN 
	(
		SELECT
			CONVERT(INT,RateValue) AS NoItems,
			ListOrdinal
		FROM QuoteInputs
		WHERE
			InputFK = 16
	)x on x.ListOrdinal = q.ListNo
),
B AS
(
	SELECT
		RowNo,
		CoverPK,
		InputFK,
		RatePK,
		Threshold AS Rate,
		1 AS NoItems,
		Threshold  AS Charge
	FROM A
	WHERE
		Threshold = (SELECT MAX(Threshold) FROM A)
	GROUP BY
		InputFK,
		RowNo,
		CoverPK,
		RatePK,
		Threshold
),
C AS
(
	SELECT 
		RowNo,
		InputFK,
		CoverPK,
		RatePK,
		Rate,
		NoItems,
		Charge
	FROM B
	
	UNION
	
	SELECT
		a.RowNo,
		a.InputFK,
		a.CoverPK,
		a.RatePK,
		a.Rate,
		a.NoItems - ISNULL(b.NoItems,0) AS NoItems ,
		a.Rate * (a.NoItems - ISNULL(b.NoItems,0)) AS Charge
	FROM A
	LEFT jOIN B 
		ON A.ROWNO = B.ROWNO
		AND a.RatePK = b.RatePK
	WHERE
		a.NoItems > 1
),
d AS
(
	SELECT
		c.InputFK,
		c.CoverPK,
		c.RatePK,
		1 AS InvLevel,
		a.InputString AS LineType,
		c.NoItems AS Base,
		c.Rate,
		c.Charge,
		a.ListNo
	FROM C 
	INNER JOIN A 
		ON a.ROWNO = c.ROWNO
)
SELECT TOP 1000
	InputFK,
	CoverPK,
	RatePK,
	InvLevel,
	LineType,
	Base,
	Rate,
	Charge,
	ListNo
FROM d
ORDER BY
	Invlevel, 
	RatePK,
	Base

)