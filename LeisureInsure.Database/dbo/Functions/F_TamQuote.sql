﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_TamQuote]
(	
	@QuotePK int
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
		CONVERT(varchar(100),QuotePK) AS QuotePK,
		ISNULL(Csr,'WU') AS Csr,
		CONVERT(varchar(100),PWord) AS PWord,
		CASE 
			WHEN Loc = 'en-GB'
			THEN 'CAT'
			ELSE 'DOG'
		END AS TamIco,
		TamClientCode AS TamClientIndex,
		TAMPolicyCode AS TamPolicyIndex,
		(SELECT CONVERT(VARCHAR(15),UPPER(LEFT(PolicyName,15))) FROM Policies WHERE PolicyPK = PolicyFK) AS PolicyType,
		CONVERT(varchar(100),QuotePK) + CONVERT(varchar(10),PWord) AS DocumentID,
		Loc AS Locale,
		DateFrom,
		DateTo
	FROM Quotes
	WHERE 
		QuotePK = @QuotePK
)