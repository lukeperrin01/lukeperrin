﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_CertSectionPKs]
(	
	@QuotePK INT
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT
		@QuotePK AS QuotePK,
		c.Section,
		CoverName,
		CASE
		WHEN q.QuoteFK IS NULL
		THEN ' : NOT INSURED'
		ELSE ' : INSURED'
		END AS Header
	FROM Covers C
	LEFT JOIN 
	(
		Select 
			q.QuoteFK,
			section
	FROM QuoteDetails Q
	INNER JOIN Covers C
		ON C.CoverPK = q.CoverFK
	WHERE 
		q.quotefk = @QuotePK
	) Q ON c.section = q.section

	where 
		c.coverPk > 1
	Group by
		c.Section,
		q.QuoteFK,
		CoverName
)