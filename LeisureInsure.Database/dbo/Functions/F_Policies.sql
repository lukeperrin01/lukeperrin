﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_Policies]
(	
	@localeBW int,
	@policyPK int
)
RETURNS TABLE 
AS
RETURN 
(
SELECT
	PolicyPK,
	Bitwise AS PolicyBW,
	PolicyName,
	PeriodYear,
	PeriodDay,
	PremiseRequired,
	TimeRequired,
	CoverBW,
    CASE
		WHEN PolicyPK = @policyPK
		THEN 1
		ELSE 0
    END AS policyPKSelected,
    CONVERT(char(10), GetDate()+MinDatedays,126) AS MinDate,
    CONVERT(char(10), dateadd(mm, MaxDateMonths, GetDate()+MinDatedays),126) AS MaxDate
FROM Policies P
WHERE
	(p.localeBW & @localeBW)>0
	AND p.coverBW IS NOT NULL
)