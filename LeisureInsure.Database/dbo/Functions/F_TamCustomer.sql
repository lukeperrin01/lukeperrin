﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_TamCustomer]
(	
	-- Add the parameters for the function here
	@QuotePK int
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT
		q.QuotePK as QuotePK,
		convert(varchar(12),q.QuotePK) + convert(varchar(12),q.PWord) AS DocID,
		left(isnull(Left(EntityName,30),FirstName  + '' + LastName ),30)  AS CustomerName,
		EntityType AS CustomerType,
		TamAgency,
		TamType,
		Address1,
		Address2,
		Address3,
		Town,
		County,
		l.Country,
		isnull(q.Postcode,'Eire') as PostCode,
		cu.Telephone,
		cu.Email
	FROM Quotes q
	INNER JOIN Contacts cu
		on q.customerFK = cu.ContactPK
		and q.QuotePK = @QuotePK
	INNER JOIN LocaleS l
		on q.Loc = l.strLocale
	INNER JOIN PolicIES p
		on q.PolicyFK = p.PolicyPK
	INNER JOIN Addresses a
		on q.addressFK = a.AddressPK
)