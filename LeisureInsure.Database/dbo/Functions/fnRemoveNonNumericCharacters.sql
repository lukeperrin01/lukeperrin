﻿CREATE Function [dbo].[fnRemoveNonNumericCharacters](@strText VARCHAR(1000))
RETURNS decimal(18,0)
AS
BEGIN
declare @RText varchar(250)
    BEGIN
        IF(SELECT len(@strText) - len(replace(@strText,'.',''))) = 0
        BEGIN
			WHILE PATINDEX('%[^0-9]%', @strText) > 0
			BEGIN
				SET @strText = STUFF(@strText, PATINDEX('%[^0-9]%', @strText), 1, '')
			END
			SET @RText = ''
        END
        ELSE IF (SELECT len(@strText) - len(replace(@strText,'.',''))) = 1
        BEGIN
			SET @RText = (SELECT(RIGHT(@strText,CHARINDEX('.',REVERSE(@strText))-1)))
			SET @strText =(select LEFT(@strText, charindex('.', @strText) - 1))

			WHILE PATINDEX('%[^0-9]%', @strText) > 0
			BEGIN
				SET @strText = STUFF(@strText, PATINDEX('%[^0-9]%', @strText), 1, '')
			END

			
			--set @RText = (select PARSENAME(@strText,1))
			WHILE PATINDEX('%[^0-9]%', @RText) > 0
			BEGIN
				SET @RText = STUFF(@RText, PATINDEX('%[^0-9]%', @RText), 1, '')
			END
			SET @RText = '.' + @RText
			
        END
        ELSE IF (SELECT len(@strText) - len(replace(@strText,'.',''))) > 1
        BEGIN
			SET @strText = ''
			SET @RText = ''
        END
		
    END 
    IF(@strText + @RText = '')
    BEGIN
    SET @strText = 0 
    SET @RText = 0 
    END
    RETURN convert(decimal(18,0),@strText + @RText)
END