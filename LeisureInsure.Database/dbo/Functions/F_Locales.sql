﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_Locales]
(	
	@strLocale varchar(10)
)
RETURNS TABLE 
AS
RETURN 
(
/*
DECLARE @strLocale varchar(6) = 'en-GB'
*/

	SELECT
        localePK,
        strLocale,
        imgFlag AS flagUrl,
        currencySymbol,
        country,
        territory,
        BitWise AS localeBW,
        CASE
			WHEN strLocale = @strLocale
			THEN LocalePK
			ELSE 0
        END AS localePKselected
	FROM Locales L
)