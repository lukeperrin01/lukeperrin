﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_InputHeaders]
(	
	@LocaleBW int,
	@PolicyPK int,
	@QuotePK int
)
RETURNS TABLE 
AS
RETURN 
(
/*
DECLARE @LocaleBW int = 1
DECLARE @PolicyPK int = 1
DECLARE @QuotePK int = 0
;
*/
SELECT TOP 1000
	c.CoverPK,
	i.InputPK,
	i.InputName,
	i.InputOrdinal,
	i.InputType,
	i.ParentPK,
	ISNULL(i.InputTriggerFK,0) AS InputTriggerFK,
	ISNULL(q.RateFK,0) AS InputSelectedPK,
	ISNULL(q.LineType,'') AS InputString,
	i.PageNo,
	r.ChildBW,
	r.RatePK, 
	r.RateName, 
	r.Rate, 
	r.Threshold, 
	r.Excess, 
	r.RateLabel, 
	r.RateTip, 
	ISNULL(r.Refer,0) AS Refer,
	r.DivBy, 
	r.RateOrdinal, 
	ISNULL(r.RateBW,0) AS RateBW
FROM Inputs I
INNER JOIN Policies P
	ON p.PolicyPK = @PolicyPK
	AND (p.BitWise & i.PolicyBW)>0
INNER JOIN Covers C
	ON (i.CoverBW & c.BitWise)>0
INNER JOIN Rates R
	ON R.InputFK = i.InputPK
LEFT JOIN QuoteDetails Q
	ON q.InputFK = i.InputPK
	AND	q.QuoteFK = @QuotePK

WHERE
	(i.LocaleBW & @LocaleBW)>0
ORDER BY 
	I.InputOrdinal,r.RateOrdinal
)