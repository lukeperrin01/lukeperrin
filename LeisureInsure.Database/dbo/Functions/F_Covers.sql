﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_Covers]
(	

	@LocaleBW int,
	@PolicyPK int,
	@QuotePK int
)
RETURNS TABLE 
AS
RETURN 
(
/*
IF OBJECT_ID('tempdb..#Quotes') IS NOT NULL
    DROP TABLE #Quotes

create table #Quotes
(
	QuotePK int,
    CoversBW int
)
INSERT INTO #Quotes(QuotePK,CoversBW)
SELECT 1,3
DECLARE @QuotePK int = 1
DECLARE @LocaleBW int = 1
DECLARE @PolicyPK int = 1
;


*/
WITH P AS
(
	SELECT 
		CoverBW,
		BitWise AS PBitwise
	FROM Policies
	WHERE 
		PolicyPK = @PolicyPK
		or @PolicyPK = -1

),
Q AS 
(
	SELECT 
		CoversBW
	FROM Quotes
	WHERE 
		QuotePK = @QuotePK
),
C AS
(
	SELECT
		CoverPK,
		section,
		CASE
			WHEN SectionName IS NOT NULL
			THEN CoverName
			ELSE ''
		END AS SectionName,
		
		CASE
			WHEN SectionName IS NOT NULL
			THEN SectionName
			ELSE CoverName
		END AS CoverName,
		ISNULL(CoverTip,'') AS CoverTip,
		CASE
			WHEN ((SELECT CoversBW FROM Q)& c.bitwise)>0 OR CoverPK = 1
			THEN 1
			ELSE 0
		END AS coverPKSelected,
		(SELECT MAX(PageNo)FROM Inputs WHERE (CoverBW & c.bitwise)>0 AND (PBitwise & PolicyBW)>0 AND (@LocaleBW & LocaleBW)>0) AS Pages,
		BitWise
	FROM Covers C
	INNER JOIN P
		ON (p.CoverBW & c.bitwise)>0
		AND (c.localeBW & @localeBW)>0
		OR @PolicyPK = -1
)
SELECT top 100
	CoverPK,
	SectionName,
	Section,
	CoverName,
	CoverTip,
	coverPKSelected,
	Max(Pages) AS Pages,
	bitwise
FROM C
GROUP BY
	CoverPK,
	Section,
	SectionName,
	CoverName,
	CoverTip,
	coverPKSelected,
	bitwise
order by
	Section,
	CoverPK

)