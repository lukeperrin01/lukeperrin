﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_Inputs]
(	
	@LocaleBW int,
	@PolicyPK int,
	@QuotePK int
)
RETURNS TABLE 
AS
RETURN 
(
/*
DECLARE @LocaleBW int = 1
DECLARE @PolicyPK int = 1
DECLARE @QuotePK int = 0
;
*/
With PC AS
(
SELECT
	c.CoverPK,
	i.InputPK,
	i.InputName,
	r.InputOrdinal,

	r.InputTypeFK AS InputType,
	i.ParentPK,
	ISNULL(r.InputTrigger,0) AS InputTriggerFK,
	0 AS InputSelectedPK,
	r.PageNo,
	r.ChildBW,
	r.RatePK,
	CASE 
		WHEN r.UnitType = 'int'AND ISNUMERIC(r.RateName) = 1
		THEN replace(CONVERT(varchar(25),CAST((r.RateName ) AS MONEY)* CAST((ExchangeRate) AS MONEY),1),'.00','') 
		WHEN r.UnitType = '£' AND ISNUMERIC(r.RateName) = 1
		THEN l.CurrencySymbol + replace(CONVERT(varchar(25),CAST((r.RateName ) AS MONEY)* CAST((ExchangeRate) AS MONEY),1),'.00','') 
		WHEN r.UnitType = 'str£' AND r.threshold > 0
		THEN r.RateName + ' ' + l.CurrencySymbol + replace(CONVERT(varchar(25),CAST((r.threshold ) AS MONEY)* CAST((ExchangeRate) AS MONEY),1),'.00','') 
		WHEN r.UnitType = 'str£' AND r.threshold = 0
		THEN l.CurrencySymbol + replace(CONVERT(varchar(25),CAST((r.RateName ) AS MONEY)* CAST((ExchangeRate) AS MONEY),1),'.00','') 
		ELSE CONVERT(varchar(250),R.RateName)
	END AS RateName,
 	r.Rate, 
	r.Threshold, 
	r.Excess, 
	r.RateLabel, 
	r.RateTip, 
	ISNULL(r.Refer,0) AS Refer,
	r.DivBy, 
	r.RateOrdinal, 
	ISNULL(r.RateBW,0) AS RateBW,
	CASE
		WHEN i.ngRequired = 1
		THEN 'true'
		ELSE 'false'
	END AS ngRequired,
	isnull(i.ngClass,'') AS ngClass,
	isnull(i.ngPattern,'') AS ngPattern,
	r.UnitType,
	l.CurrencySymbol,
	l.ExchangeRate,
	r.TableChildren,
	r.TableFirstColumn,
	r.parentRatePK,
	r.listRatePK,
    r.NewParentBW,
    r.NewChildBW,
	R.SumInsured,
	R.NoItems,
	R.DisplayBW,
	R.ReturnBW,
	R.StringBW,
	R.lblSumInured AS lblSumInsured,
	R.lblNumber,
	RateTypeFK,
	r.DiscountFirstRate,
    r.DiscountSubsequentRate,
    r.CoverCalcFK,
    r.RateGroup
FROM Inputs I
INNER JOIN Policies P
	ON p.PolicyPK = @PolicyPK
	--AND (p.BitWise & i.PolicyBW)>0
INNER JOIN Rates R
	ON R.InputFK = i.InputPK
	AND (r.LocaleBW & @LocaleBW)>0
	--AND (r.LocaleBW & 2)>0
	
	AND (r.policyBW & p.bitwise)>0
	AND ISNULL(RateExclude,0)=0
INNER JOIN Covers C
	ON (r.CoverBW & c.BitWise)>0
	and r.coverBW >0
INNER JOIN Locales L
	ON l.BitWise = @LocaleBW

WHERE
	(i.LocaleBW & @LocaleBW)>0
),

Labels AS
(

SELECT	
	RatePK AS HRATEPK,
	listratePK,
	lblSumInsured, 
	lblNumber, 
	displayBW,
	CASE 
		WHEN DisplayBW =1
		THEN RATENAME
		WHEN DisplayBW =2
		THEN RATELABEL
		ELSE NULL 
	END AS SubRateHeading
	
FROM PC 
WHERE LISTRATEPK > -1

)



--select * from Labels 


SELECT TOP 1000
	CoverPK,
	InputPK,
	InputName,
	InputOrdinal,
	InputType,
	ParentPK,
	InputTriggerFK,
	InputSelectedPK,
	'' AS InputString,
	PageNo,
	ChildBW,
	RatePK,

	RateName,
 	Rate, 
	Threshold, 
	Excess, 
	RateLabel, 
	RateTip, 
	Refer,
	DivBy, 
	RateOrdinal, 
	RateBW,
	ngRequired,
	'' AS RateValue,
	ngClass,
	ngPattern,
	0 AS ListNo,
	0 AS ListOrdinal,
	UnitType,
	CASE
		WHEN i.pageno = 2
		THEN 0
		WHEN isnull(i.ParentRatePK,0) = 0
		THEN 1
		WHEN isnull(i.ParentRatePK,0) NOT IN (select RatePK from pc)
		THEN 1
		WHEN InputType = 14
		THEN 1
		ELSE 0
	END AS InputVis,
	CASE
		WHEN i.pageno = 2
		THEN 0
		WHEN isnull(i.ParentRatePK,0) = 0
		THEN 1
		WHEN isnull(i.ParentRatePK,0) NOT IN (select RatePK from pc)
		THEN 1
		WHEN InputType = 14
		THEN 1
		ELSE 0
	END AS RateVis,
	0 AS RateStatus,
	TableChildren,
	TableFirstColumn,
	i.parentRatePK,
	i.listRatePK,
    i.NewParentBW,
    i.NewChildBW,
	i.SumInsured,
	i.NoItems,
	i.DisplayBW,
	i.ReturnBW,
	i.StringBW,
	ISNULL(i.lblSumInsured,l.lblSumInsured) AS lblSumInsured,
	ISNULL(i.lblNumber,l.lblNumber) as lblNumber,
	SubRateHeading,
	isnull(LEN(l.SubRateHeading)
		,isnull(LEN(i.lblNumber),0)
			+isnull(LEN(l.lblSumInsured)
			,isnull(LEN(i.lblSumInsured),0))
				+isnull(LEN(l.lblNumber)
				,isnull(LEN(i.lblNumber),0))) AS TrigSubHeadings,
	RateTypeFK,
	DiscountFirstRate,
    DiscountSubsequentRate,
    CoverCalcFK,
    ISNULL(RateGroup,0) AS RateGroup
FROM PC  I
Left Join Labels l
	on (l.hratePK = i.ListRatePK *-1 or (l.hratePK = i.ratepk and l.ListRatePK = 0))
	and ((i.displayBW & l.displayBW)>0 or i.displayBW = l.displayBW or l.displayBW = 0)
ORDER BY 
	i.coverPK,I.PageNo,i.InputOrdinal,i.RateOrdinal,i.rateName
)