﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_MinPremium]
(
	@SumInput int,
	@LocalePK int,
	@CoverBW int,
	@PolicyBW int
	
)
RETURNS TABLE 
AS
RETURN 
with A AS
(
select
	CASE
		WHEN (l.multiplier * @SumInput) < (l.multiplier * r.threshold)
		THEN r.Rate * l.multiplier
		ELSE null
	end as Threshold,
	@CoverBW as CoverBW
from inputs i
inner join rates r
	on i.inputPK = 48
	and (i.coverbw & @coverbw)>0
	and (i.policyBW & @PolicyBW )>0
	and r.inputfk = i.inputPK
	and (r.policyBW & @PolicyBW )>0
INNER JOIN Locales L
	on l.localePK = @LocalePK



 --where i.inputpk = @InputPK


)
Select * from a