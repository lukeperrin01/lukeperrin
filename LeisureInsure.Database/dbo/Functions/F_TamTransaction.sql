﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
cREATE FUNCTION [dbo].[F_TamTransaction]
(	
	-- Add the parameters for the function here
	@QuotePK INT,
	@Trans Int
)
RETURNS TABLE 
AS
RETURN 
(
with POL AS
(
SELECT
	1 AS Transact,
	'NEW' AS TransType,
	'New Business' AS TransDesc,
	CASE
		WHEN q.Loc = 'en-GB' and p.TamType != 'Events'
		THEN 1
		ELSE 0
	END AS addTax,
	q.TamClientCode,
	q.TAMPolicyCode,
	q.netPremium AS Charge
FROM QUOTES Q
INNER JOIN Policies P
	on q.quotePK = @QuotePK
	AND q.policyFK = p.policyPK
),
AFE AS 
(
SELECT
	2 AS Transact,
	'AFE' AS TransType,
	'Agency Fee' AS TransDesc,
	0 AS addTax,
	q.TamClientCode,
	q.TAMPolicyCode,
	q.Fee AS Charge
FROM QUOTES Q
INNER JOIN Policies P
	on q.quotePK = @QuotePK
	AND q.policyFK = p.policyPK
),
Tax AS
(
SELECT
	3 AS Transact,
	CASE 
		WHEN q.Loc = 'en-GB'
		THEN 'IPT'
		ELSE 'IGL'
	END AS TransType,
	CASE 
		WHEN q.Loc = 'en-GB'
		THEN 'Insurance Premium Tax'
		ELSE 'Irish Government Levy'
	END AS TransDesc,
	0 AS addTax,
	q.TamClientCode,
	q.TAMPolicyCode,
	q.tax AS Charge
FROM QUOTES Q
INNER JOIN Policies P
	on q.quotePK = @QuotePK
	AND q.policyFK = p.policyPK
	--AND p.TamType = 'Events'


),
U as
(
  SELECT * FROM pol
  UNION
  SELECT * FROM afe
  UNION
  SELECT * FROM tax
)
  SELECT * FROM u
  WHERE Transact = @Trans
)