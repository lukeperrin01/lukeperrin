﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_CertHazards]
(	
	@QuotePK INT
)
RETURNS TABLE 
AS
RETURN 
(
/*
DECLARE @QuotePK INT = 100010

*/

SELECT 
	A.RateName AS Question,
	CASE
		WHEN r.UnitType = '£' AND ISNUMERIC(r.RateName) = 1
		THEN L.CurrencySymbol + replace(CONVERT(varchar(25),CAST((r.RateName ) AS MONEY)* CAST((L.ExchangeRate) AS MONEY),1),'.00','') 
		ELSE R.RateName
	END AS Answer
FROM RATES R
INNER JOIN Quotes Q
	ON Q.QuotePK = @QUOTEPK 
INNER JOIN Locales L
ON L.LocalePK = Q.LocaleId
INNER JOIN QUOTEINPUTS QI
	ON R.RatePK = QI.RatePK
	AND R.PageNo = 2
	AND QI.QuoteId = @QUOTEPK
INNER JOIN 
	(
	SELECT 
		ListRatePK * -1 AS ListRatePK,
		RATENAME
	FROM Rates R
	) A ON R.ListRatePK = A.ListRatePK
)