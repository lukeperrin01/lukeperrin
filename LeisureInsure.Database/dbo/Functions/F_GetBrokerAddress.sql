﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_GetBrokerAddress]
(	
	@QuotePK int
)
RETURNS TABLE 
AS
RETURN 
(
Select 
	isnull(ContactPK,0) AS ContactPK,
	EntityName,
	Address1,
	Address2,
	Address3,
	Town,
	County,
	Country,
	a.Postcode,
	Telephone,
	DateFrom
From Quotes q
Left join Contacts c
	on q.BrokerFK = c.ContactPK
Left join addresses a
	on c.ContactPK = a.ContactFK
where q.QuotePK = @QuotePK

)