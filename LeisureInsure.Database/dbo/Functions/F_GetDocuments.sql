﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_GetDocuments]
(	

	@QuotePK int
)
RETURNS TABLE 
AS
RETURN 
(
/*

DECLARE @QuotePK int = 0
;
*/
with q as 
(
SELECT
	q.LocaleId,
	p.BitWise AS PolicyBW,
	ISNULL(BIHA,0) AS Biha
FROM Quotes Q
INNER JOIN Policies P
	ON p.PolicyPK = q.PolicyFK
	and q.QuotePK = @QuotePK
),
qd as
(
SELECT 
	C.BitWise AS CoverBW,
	r.RatePK as RateFK,
	r.clauseCode,
	ValidUntil
FROM QuoteInputs Q
inner join Rates r
	on r.RatePK = q.RatePK
	and q.QuoteId = @QuotePK
INNER JOIN Covers C
	ON C.CoverPK = r.CoverCalcFK
),
d as
(
SELECT 
	[DocumentPK]
      ,[DocName]
      ,[DocLink]
      ,[BitWise]
      ,[DocType]
      ,d.[LocaleBW]
      ,d.[PolicyBW]
      ,d.[CoverBW]
      ,d.[RateFK]
      ,d.[Biha]
      ,d.[DateAdded]
      ,d.[ValidUntil]
  FROM [Documents] d
  inner join q
	on (q.LocaleId & localebw)>0
	and (q.PolicyBW & d.policyBw)>0
	and (q.Biha = d.biha or d.biha = -1)
	AND ISNULL(d.ValidUntil,getdate()-1) <getdate()
	or (d.rateFk in (select RateFK from qd where isnull(qd.ValidUntil,GETDATE()-1) < GETDATE()))

),
RateClauses AS
(
SELECT 
	DocumentPK as o,
	DocType,
	DocLink + '.pdf' AS DocLink 
from d 

UNION

Select 
	200 as o,
	'Clause' AS DocType,
	qd.clauseCode + '.pdf' as DocLink
FROM qd 
WHERE 
	QD.clauseCode IS NOT NULL 
Group by
	qd.clauseCode
)


Select top 1000 
DocType,DocLink from RateClauses order by o 
)