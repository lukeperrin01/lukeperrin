﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[F_QuoteInputs]
(	
	@LocaleBW int,
	@PolicyPK int,
	@QuotePK int
)
RETURNS TABLE 
AS
RETURN 
(
/*
DECLARE @LocaleBW int = 1
DECLARE @PolicyPK int = 1
DECLARE @QuotePK int = 0
;
*/
WITH q AS
(
	SELECT TOP 1
		LocaleId,
		PolicyFK,
		QuotePK
	FROM Quotes
	WHERE 
		QuotePK = @QuotePK
),


Base AS
(
SELECT * FROM F_Inputs((SELECT LocaleId FROM Q),(SELECT PolicyFK FROM Q),@QuotePK)
),
Quote AS
(
SELECT 
	q.QuoteInputPK,
	q.CoverPK,
	q.ListNo,
	q.ListOrdinal,
	q.InputName,
	q.InputFK,
	CASE
		WHEN ListRatePK < 0 and InputTypeFK = 7
		THEN ListRatePK * -1
		ELSE r.RatePK
	END AS J,
	r.RatePK,
	q.RateValue,
	--q.RateValue,
	q.RateString,
	q.InputString,
	q.InputPKLock,
	q.QuoteId,
	q.SumInsured,
	q.NoItems,
	q.lblSumInsured,
	q.lblNoItems,
	q.RateTypeFK,
	r.ListRatePK,
	r.InputTypeFK,
	r.DiscountFirstRate,
	r.DiscountSubsequentRate,
    r.CoverCalcFK,
    r.NewParentBW AS ParentBW,
    case 
		when r.InputTypeFK = 14
		then r.InputFK
		else 0
	end as TableVis
FROM QuoteInputs Q
INNER JOIN Rates R
	ON q.RatePK = r.RatePK

WHERE
	q.QuoteId = @QuotePK
)

--SELECT * FROM Quote

,
VAL AS
(
SELECT
	b.CoverPK,
	b.InputPK,
	b.InputName,
	b.InputOrdinal,
	b.InputType,
	b.ParentPK,
	b.InputTriggerFK,
	b.InputString,
	b.PageNo,
	b.ChildBW,
	b.RatePK,
	CASE 
		WHEN InputTypeFK = 8
		THEN isnull(q.inputString, b.RateName) 
		ELSE b.RateName
	END AS RateName,
 	b.Rate, 
	b.Threshold, 
	b.Excess, 
	b.RateLabel, 
	b.RateTip, 
	b.Refer,
	b.DivBy, 
	b.RateOrdinal, 
	b.RateBW,
	b.ngRequired,
	ISNULL(q.rateValue,b.rateValue) AS RateValue,
	--(SELECT RateValue FROM Quote where RatePK = b.RatePK and isnull(RateValue,'') != '')AS RateValue,
	b.ngClass,
	b.ngPattern,
	b.UnitType,
	b.InputVis,
	b.RateVis,
	b.RateStatus,
	b.TableChildren,
	b.TableFirstColumn,
	b.parentRatePK,
	b.listRatePK,
    b.NewParentBW,
    b.NewChildBW,
	q.SumInsured,
	q.NoItems,
	b.DisplayBW,
	b.ReturnBW,
	b.StringBW,
	b.lblSumInsured,
	b.lblNumber,
	b.SubRateHeading,
	b.TrigSubHeadings,
	b.RateTypeFK,

	ISNULL(q.ListNo,  b.ListNo) AS ListNo,
	ISNULL(q.ListOrdinal,  b.ListOrdinal) AS ListOrdinal,
	CASE 
		WHEN q.ListNo < 0
		THEN b.RatePK
		WHEN InputType = 7 
		THEN ISNULL((SELECT TOP 1 RatePK FROM Quote WHERE ListRatePK *-1 = B.RatePK ),0)
		WHEN InputType = 8 AND q.ListNo <0
		THEN ISNULL(b.RatePK,0)
		WHEN InputType = 14
		THEN ISNULL((SELECT TOP 1 RatePK FROM Quote WHERE RatePK = B.RatePK ),0)
		ELSE 0
	END AS InputSelectedPK,
	b.DiscountFirstRate,
	b.DiscountSubsequentRate,
    b.CoverCalcFK
FROM Base b 
LEFT JOIN Quote Q
	ON (q.RatePK = b.RatePK AND q.ListNo = 0)
	
UNION ALL

SELECT
	b.CoverPK,
	b.InputPK,
	b.InputName,
	b.InputOrdinal,
	b.InputType,
	b.ParentPK,
	b.InputTriggerFK,
	b.InputString,
	b.PageNo,
	b.ChildBW,
	b.RatePK,
	CASE 
		WHEN Q.LISTNO <0
		THEN isnull(q.inputString, b.RateName) 
		ELSE b.RateName
	END AS RateName,
 	b.Rate, 
	b.Threshold, 
	b.Excess, 
	b.RateLabel, 
	b.RateTip, 
	b.Refer,
	b.DivBy, 
	b.RateOrdinal, 
	b.RateBW,
	b.ngRequired,
	ISNULL(q.rateValue,b.rateValue) AS RateValue,
	--(SELECT RateValue FROM Quote where RatePK = b.RatePK and isnull(RateValue,'') != '')AS RateValue,
	b.ngClass,
	b.ngPattern,
	b.UnitType,
	b.InputVis,
	b.RateVis,
	b.RateStatus,
	b.TableChildren,
	b.TableFirstColumn,
	--b.listRatePK * -1 AS parentRatePK,
	b.parentRatePK,
	b.listRatePK,
    b.NewParentBW,
    b.NewChildBW,
	q.SumInsured,
	q.NoItems,
	b.DisplayBW,
	b.ReturnBW,
	b.StringBW,
	b.lblSumInsured,
	b.lblNumber,
	b.SubRateHeading,
	b.TrigSubHeadings,
	b.RateTypeFK,

	ISNULL(q.ListNo,  b.ListNo) AS ListNo,
	ISNULL(q.ListOrdinal,  b.ListOrdinal) AS ListOrdinal,
	CASE 
		WHEN q.ListNo < 0
		THEN b.RatePK
		WHEN InputType = 7 
		THEN ISNULL((sELECT RatePK FROM Quote WHERE ListRatePK *-1 = B.RatePK ),0)
		WHEN InputType = 8 AND q.ListNo <0
		THEN ISNULL(b.RatePK,0)
		ELSE 0
	END AS InputSelectedPK,
	b.DiscountFirstRate,
	b.DiscountSubsequentRate,
    b.CoverCalcFK
FROM Base b 
INNER JOIN Quote Q
	ON (q.RatePK = b.RatePK AND q.ListNo != 0)
	
)


SELECT TOP 1000
	i.CoverPK,
	i.InputPK,
	i.InputName,
	i.InputOrdinal,
	i.InputType,
	i.ParentPK,
	i.InputTriggerFK,
	i.InputString,
	i.PageNo,
	i.ChildBW,
	i.RatePK,
	i.RateName,
 	i.Rate, 
	i.Threshold, 
	i.Excess, 
	i.RateLabel, 
	i.RateTip, 
	i.Refer,
	i.DivBy, 
	i.RateOrdinal, 
	i.RateBW,
	i.ngRequired,
	i.RateValue,
	i.ngClass,
	i.ngPattern,
	i.UnitType,
	i.TableChildren,
	i.TableFirstColumn,
	i.parentRatePK,
	i.listRatePK,
    i.NewParentBW,
    i.NewChildBW,
	i.SumInsured,
	i.NoItems,
	i.DisplayBW,
	i.ReturnBW,
	i.StringBW,
	i.lblSumInsured,
	i.lblNumber,
	i.SubRateHeading,
	i.TrigSubHeadings,
	i.RateTypeFK,
	i.ListNo,
	i.ListOrdinal,
	i.InputSelectedPK,
	CASE
		WHEN i.pageno = 2 AND i.InputSelectedPK = 0
		THEN 0
		WHEN i.pageno = 2 AND i.InputSelectedPK > 0
		THEN 1
		WHEN isnull(i.ParentRatePK,0) = 0
		THEN 1
		WHEN isnull(i.ParentRatePK,0) NOT IN (select RatePK from VAL)
		THEN 1
		WHEN i.InputType = 8 AND i.ListOrdinal < 0
		THEN 1
		ELSE 0
	END AS InputVis,
	CASE
		WHEN InputType = 14
		THEN 1
		WHEN i.pageno = 2 AND i.InputSelectedPK = 0
		THEN 0
		WHEN i.pageno = 2 AND i.InputSelectedPK > 0
		THEN 1
		WHEN i.ChildBW > 0 and ((Select parentbw from quote where ratepk = parentRatePK)& i.ChildBW)>0 and ListOrdinal < 0
		THEN 1
		WHEN InputPK IN (SELECT TABLEVIS FROM Quote)
		THEN 1
		WHEN i.InputSelectedPK > 0 OR isnull(i.NoItems,0)>0 OR ISNULL(i.suminsured,0)>0 OR ISNULL(i.ratevalue,'') != '' and InputType != 14
		THEN 1
		WHEN isnull(i.ParentRatePK,0) = 0
		THEN 1
		--WHEN isnull(i.ParentRatePK,0) NOT IN (select RatePK from VAL)
		--THEN 1
		WHEN i.InputType = 8 AND i.ListOrdinal < 0
		THEN 1
		ELSE 0
	END AS RateVis,
	CASE 
		WHEN InputSelectedPK > 0
		THEN 1
		WHEN RateValue != ''
		THEN 1
		ELSE 0
	END AS RateStatus,
	DiscountFirstRate,
	DiscountSubsequentRate,
    CoverCalcFK
FROM VAL  I
--where RatePK = 1156
ORDER BY 
	i.coverPK,I.PageNo,i.InputOrdinal,i.RateOrdinal,i.rateName
)