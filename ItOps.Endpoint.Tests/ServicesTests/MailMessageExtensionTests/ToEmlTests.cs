﻿using System.Threading.Tasks;
using FluentAssertions;
using ItOps.Endpoint.Services.Emailer;
using Xunit;

namespace ItOps.Endpoint.Tests.ServicesTests.MailMessageExtensionTests
{
    public class ToEmlTests
    {
        [Fact]
        public async Task ItWorks()
        {
            var emailSender = new EmailSender();
            var eml = await emailSender.CreateEml("test@test.com", "to@to.com", "subject", "body", null, "test@test.com");

            eml.Should().NotBeNull();
        }
    }
}
