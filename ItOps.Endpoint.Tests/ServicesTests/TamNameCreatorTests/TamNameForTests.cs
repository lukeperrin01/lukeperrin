﻿using FluentAssertions;
using ItOps.Endpoint.Services;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using Ploeh.AutoFixture.Xunit2;
using Xunit;
using LeisureInsure;

namespace ItOps.Endpoint.Tests.ServicesTests.TamNameCreatorTests
{
    public class TamNameForTests
    {
        [InlineAutoData("You&Me", "YouAndMe")]
        [InlineAutoData("first & second", "firstAndsecond")]
        [InlineAutoData("&& why", "AndAndwhy")]
        [Theory]
        public void AmpersandIsReplacedByAnd(
            string name, 
            string expectedResult, 
            Contact contact,
            TamNameCreator sut)
        {
            contact.EntityName = name;

            //var actual = sut.TamNameFor(contact);

            //actual.Should().Be(expectedResult);
        }

        [InlineAutoData("FirstSecond", "FirstSecond")]
        [InlineAutoData("YouOrMe", "YouOrMe")]
        [InlineAutoData("BusinessName", "BusinessName")]
        [Theory]
        public void ValidNameIsRetained(
            string name,
            string expectedResult,
            Contact contact,
            TamNameCreator sut)
        {
            contact.EntityName = name;

            //var actual = sut.TamNameFor(contact);

            //actual.Should().Be(expectedResult);
        }

        [InlineAutoData("ab£$%^*)cde", "abcde")]
        [InlineAutoData("£$^*^)(*abcde", "abcde")]
        [InlineAutoData("abcde£$%^*()", "abcde")]
        [Theory]
        public void InvalidCharactersRemoved(
            string name,
            string expectedResult,
            Contact contact,
            TamNameCreator sut)
        {
            contact.EntityName = name;

            //var actual = sut.TamNameFor(contact);

            //actual.Should().Be(expectedResult);
        }

        [InlineAutoData("ab£$%^*-)&cde", "abAndcde")]
        [InlineAutoData("£$^*^&)-_(*abcde", "Andabcde")]
        [InlineAutoData("abcde£$&%^*()", "abcdeAnd")]
        [Theory]
        public void InvalidCharactersAndAmpersandAreHandledCorrectly(
            string name,
            string expectedResult,
            Contact contact,
            TamNameCreator sut)
        {
            contact.EntityName = name;

            //var actual = sut.TamNameFor(contact);

            //actual.Should().Be(expectedResult);
        }

        [InlineAutoData("qweÄÂÈËêäñØĕĹŁŌŘ", "qwe")]
        [Theory]
        public void ForeignCharactersRemoved(
            string name,
            string expectedResult,
            Contact contact,
            TamNameCreator sut)
        {
            contact.EntityName = name;

            //var actual = sut.TamNameFor(contact);

            //actual.Should().Be(expectedResult);
        }
    }
}
