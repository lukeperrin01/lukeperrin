﻿using System.Web.Mvc;
using LeisureInsure;
using LeisureInsure.DB;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Practices.Unity;
using NServiceBus;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace LeisureInsure
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => DependencyResolver.Current.GetService<IRepository>());
            app.CreatePerOwinContext(() => DependencyResolver.Current.GetService<LeisureInsureEntities>());

            DataProtectionProvider = app.GetDataProtectionProvider();
            UnityConfig.ConfiguredContainer().RegisterInstance(DataProtectionProvider);

            ConfigureAuth(app);
        }

        public static IDataProtectionProvider DataProtectionProvider { get; private set; }
    }
}