﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using LeisureInsure.DB.ViewModels;
using System.Xml;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Text;

namespace LeisureInsure.Helpers
{
    public class Helper
    {


        private const string payURL = "https://apixml.globaliris.com/";

        public static XmlDocument createHSBCxmlDoc(vmPayment _payment)
        {
            // Create the xml document container
            XmlDocument doc = new XmlDocument();// Create the XML Declaration, and append it to XML document
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(dec);// Create the root element

            XmlElement root = doc.CreateElement("EngineDocList");
            doc.AppendChild(root);

            XmlElement docVersion = doc.CreateElement("DocVersion");
            docVersion.SetAttribute("DataType", "String");
            docVersion.InnerText = "1.0";
            root.AppendChild(docVersion);

            XmlElement engineDoc = doc.CreateElement("EngineDoc");
            root.AppendChild(engineDoc);

            XmlElement contentType = doc.CreateElement("ContentType");
            contentType.SetAttribute("DataType", "String");
            contentType.InnerText = "OrderFormDoc";
            engineDoc.AppendChild(contentType);

            XmlElement user = doc.CreateElement("User");
            engineDoc.AppendChild(user);

            if (_payment.Instalments)
            {
                XmlElement clientID = doc.CreateElement("ClientId");
                clientID.SetAttribute("DataType", "S32");
                clientID.InnerText = "29008"; // client ID
                user.AppendChild(clientID);

                XmlElement name = doc.CreateElement("Name");
                name.SetAttribute("DataType", "String");
                name.InnerText = "lnpk"; // username
                user.AppendChild(name);

                XmlElement password = doc.CreateElement("Password");
                password.SetAttribute("DataType", "String");
                password.InnerText = "150207"; // password
                user.AppendChild(password);
            }
            else if (_payment.Culture == "ga-IE")
            {
                XmlElement clientID = doc.CreateElement("ClientId");
                clientID.SetAttribute("DataType", "S32");
                clientID.InnerText = "19657"; // client ID
                user.AppendChild(clientID);

                XmlElement name = doc.CreateElement("Name");
                name.SetAttribute("DataType", "String");
                name.InnerText = "lnke"; // username
                user.AppendChild(name);

                XmlElement password = doc.CreateElement("Password");
                password.SetAttribute("DataType", "String");
                password.InnerText = "harryr"; // password
                user.AppendChild(password);
            }
            else if (_payment.Culture == "en-GB")
            {
                XmlElement clientID = doc.CreateElement("ClientId");
                clientID.SetAttribute("DataType", "S32");
                clientID.InnerText = "15580"; // client ID
                user.AppendChild(clientID);
                XmlElement name = doc.CreateElement("Name");

                name.SetAttribute("DataType", "String");
                name.InnerText = "lnk"; // username
                user.AppendChild(name);

                XmlElement password = doc.CreateElement("Password");
                password.SetAttribute("DataType", "String");
                password.InnerText = "ducat1"; // password
                user.AppendChild(password);
            }

            XmlElement instructions = doc.CreateElement("Instructions");
            engineDoc.AppendChild(instructions);

            XmlElement pipeline = doc.CreateElement("Pipeline");
            pipeline.SetAttribute("DataType", "String");
            pipeline.InnerText = "Payment";
            instructions.AppendChild(pipeline);

            XmlElement orderFormDoc = doc.CreateElement("OrderFormDoc");
            engineDoc.AppendChild(orderFormDoc);

            XmlElement orderID = doc.CreateElement("Id");
            orderID.SetAttribute("DataType", "String");
            orderID.InnerText = _payment.QuoteRef; // quote ref
            orderID.InnerText = "";
            orderFormDoc.AppendChild(orderID);

            XmlElement mode = doc.CreateElement("Mode");
            mode.SetAttribute("DataType", "String");
            mode.InnerText = "Y"; // P = production / Y = test 
            orderFormDoc.AppendChild(mode);

            XmlElement consumer = doc.CreateElement("Consumer");
            orderFormDoc.AppendChild(consumer);

            XmlElement billTo = doc.CreateElement("BillTo");
            consumer.AppendChild(billTo);

            XmlElement location = doc.CreateElement("Location");
            billTo.AppendChild(location);

            XmlElement address = doc.CreateElement("Address");
            location.AppendChild(address);

            XmlElement firstName = doc.CreateElement("FirstName");
            firstName.SetAttribute("DataType", "String");
            firstName.InnerText = _payment.QuoteRef;
            address.AppendChild(firstName);

            XmlElement paymentMech = doc.CreateElement("PaymentMech");
            consumer.AppendChild(paymentMech);

            XmlElement paymentType = doc.CreateElement("Type");
            paymentType.SetAttribute("DataType", "String");
            paymentType.InnerText = "CreditCard";
            paymentMech.AppendChild(paymentType);

            XmlElement creditcard = doc.CreateElement("CreditCard");
            paymentMech.AppendChild(creditcard);

            XmlElement creditcardSecurityNumberIndicator = doc.CreateElement("Cvv2Indicator");
            creditcardSecurityNumberIndicator.SetAttribute("DataType", "String");
            creditcardSecurityNumberIndicator.InnerText = "1";
            creditcard.AppendChild(creditcardSecurityNumberIndicator);


            XmlElement creditcardSecurityNumber = doc.CreateElement("Cvv2Val");
            creditcardSecurityNumber.SetAttribute("DataType", "String");
            creditcardSecurityNumber.InnerText = _payment.SecurityNumber; // credit card security number
            creditcard.AppendChild(creditcardSecurityNumber);

            XmlElement creditcardExpiryDate = doc.CreateElement("Expires");
            creditcardExpiryDate.SetAttribute("DataType", "ExpirationDate");
            creditcardExpiryDate.SetAttribute("Locale", "826");
            creditcardExpiryDate.InnerText = _payment.ExpiryMM + "/" + _payment.ExpiryYY; // credit card expiration date
            creditcard.AppendChild(creditcardExpiryDate);

            XmlElement creditcardIssueNum = doc.CreateElement("IssueNum");
            creditcardIssueNum.SetAttribute("DataType", "String");
            creditcardIssueNum.InnerText = _payment.IssueNumber.ToString(); // credit card issue number
            creditcard.AppendChild(creditcardIssueNum);

            XmlElement creditcardNumber = doc.CreateElement("Number");
            creditcardNumber.SetAttribute("DataType", "String");
            creditcardNumber.InnerText = _payment.CardNumber; // credit card number
            creditcard.AppendChild(creditcardNumber);


            XmlElement creditcardStartDate = doc.CreateElement("StartDate");
            creditcardStartDate.SetAttribute("DataType", "StartDate");
            creditcardStartDate.SetAttribute("Locale", "826");
            creditcardStartDate.InnerText = _payment.ValidMM + "/" + _payment.ValidYY; // credit card start date
            creditcard.AppendChild(creditcardStartDate);

            XmlElement transaction = doc.CreateElement("Transaction");
            orderFormDoc.AppendChild(transaction);

            XmlElement transactionType = doc.CreateElement("Type");
            transactionType.SetAttribute("DataType", "String");
            transactionType.InnerText = "Auth";
            transaction.AppendChild(transactionType);

            XmlElement currentTotals = doc.CreateElement("CurrentTotals");
            transaction.AppendChild(currentTotals);

            XmlElement totals = doc.CreateElement("Totals");
            currentTotals.AppendChild(totals);

            if (_payment.Culture == "ga-IE")
            {
                XmlElement total = doc.CreateElement("Total");
                total.SetAttribute("DataType", "Money");
                total.SetAttribute("Currency", "978");
                total.InnerText = _payment.TotalPayable.ToString(); // total payable
                totals.AppendChild(total);
            }
            else if (_payment.Culture == "en-GB")
            {
                XmlElement total = doc.CreateElement("Total");
                total.SetAttribute("DataType", "Money");
                total.SetAttribute("Currency", "826");
                total.InnerText = _payment.TotalPayable.ToString(); // total payable
                totals.AppendChild(total);
            }
            return doc;

        }

        public static XmlDocument PostXMLTransaction(XmlDocument v_objXMLDoc)
        {
            //Declare XMLResponse document
            XmlDocument XMLResponse = null;

            //Declare an HTTP-specific implementation of the WebRequest class.
            HttpWebRequest objHttpWebRequest;

            //Declare an HTTP-specific implementation of the WebResponse class
            HttpWebResponse objHttpWebResponse = null;

            //Declare a generic view of a sequence of bytes
            Stream objRequestStream = null;
            Stream objResponseStream = null;

            //Declare XMLReader
            XmlTextReader objXMLReader;

            //Creates an HttpWebRequest for the specified URL.
            objHttpWebRequest = (HttpWebRequest)WebRequest.Create(payURL);

            try
            {
                //---------- Start HttpRequest 

                //Set HttpWebRequest properties
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(v_objXMLDoc.InnerXml);
                objHttpWebRequest.Method = "POST";
                objHttpWebRequest.ContentLength = bytes.Length;
                objHttpWebRequest.ContentType = "text/xml; encoding='utf-8'";

                //Get Stream object 
                objRequestStream = objHttpWebRequest.GetRequestStream();

                //Writes a sequence of bytes to the current stream 
                objRequestStream.Write(bytes, 0, bytes.Length);

                //Close stream
                objRequestStream.Close();

                //---------- End HttpRequest

                //Sends the HttpWebRequest, and waits for a response.
                objHttpWebResponse = (HttpWebResponse)objHttpWebRequest.GetResponse();

                //---------- Start HttpResponse
                if (objHttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    //Get response stream 
                    objResponseStream = objHttpWebResponse.GetResponseStream();

                    //Load response stream into XMLReader
                    objXMLReader = new XmlTextReader(objResponseStream);

                    //Declare XMLDocument
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(objXMLReader);

                    //Set XMLResponse object returned from XMLReader
                    XMLResponse = xmldoc;

                    //Close XMLReader
                    objXMLReader.Close();
                }

                //Close HttpWebResponse
                objHttpWebResponse.Close();
            }
            catch (WebException we)
            {
                //TODO: Add custom exception handling
                throw new WebException(we.Message);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //Close connections
                if (objRequestStream != null) objRequestStream.Close();
                if (objResponseStream != null) objResponseStream.Close();
                if (objHttpWebResponse != null) objHttpWebResponse.Close();

                //Release objects
                objXMLReader = null;
                objRequestStream = null;
                objResponseStream = null;
                objHttpWebResponse = null;
                objHttpWebRequest = null;
            }

            //Return
            return XMLResponse;
        }



        //public static void SetUserLocale(string _lang)
        //{
        //    HttpRequest Request = HttpContext.Current.Request;
        //    if (Request.UserLanguages == null)
        //        return;

        //    string Lang = Request.UserLanguages[0];

        //    if (_lang != null)
        //        Lang = _lang;

        //    if (Lang != "en-GB" && Lang != "ga-IE" && Lang != "en-AU")
        //        Lang = "en-GB";

        //    if (Lang != null)
        //    {
        //        // *** Problems with Turkish Locale and upper/lower case
        //        // *** DataRow/DataTable indexes
        //        if (Lang.StartsWith("tr"))
        //            return;

        //        if (Lang.Length < 3)
        //            Lang = Lang + "-" + Lang.ToUpper();
        //        try
        //        {
        //            Thread.CurrentThread.CurrentCulture = new CultureInfo(Lang);
        //            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Lang);


        //        }
        //        catch
        //        { ;}
        //    }

        //}
        public class XmlResult : ActionResult
        {
            private object _objectToSerialize;

            public XmlResult(object objectToSerialize)
            {
                _objectToSerialize = objectToSerialize;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                if (_objectToSerialize != null)
                {
                    var xs = new XmlSerializer(_objectToSerialize.GetType());
                    context.HttpContext.Response.ContentType = "text/xml";
                    xs.Serialize(context.HttpContext.Response.Output, _objectToSerialize);
                }
            }
        }

        public static string Hash(string input)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("X2"));
                }

                return sb.ToString();
            }
        }


    }




}