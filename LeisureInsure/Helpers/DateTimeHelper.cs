﻿using System;
using LeisureInsure.DB.ViewModels;

namespace LeisureInsure.Helpers
{
    public class DateTimeHelper
    {
        public vmDateCheck DateChecker(vmDateCheck dt)
        {
            var starthours = Convert.ToInt16(dt.startTime.Split(':')[0]);
            var startmins = Convert.ToInt16(dt.startTime.Split(':')[1]);
            var endhours = Convert.ToInt16(dt.endTime.Split(':')[0]);
            var endmins = Convert.ToInt16(dt.endTime.Split(':')[1]);

            dt.startDate = new DateTime(dt.startDate.Year, dt.startDate.Month, dt.startDate.Day, starthours, startmins, 0);
            dt.endDate = new DateTime(dt.endDate.Year, dt.endDate.Month, dt.endDate.Day, endhours, endmins, 0);

            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
            if (dt.PolicyPK == 10) //weddings
            {
                dt.endDate = new DateTime(dt.startDate.Year, dt.startDate.Month, dt.startDate.AddDays(1).Day, 23, 59, 0);
                dt.startTime = dateNow.ToString("HH:mm");
                dt.endTime = "23:59";
            }
            else if (dt.PolicyType == "A") //Annual policies
            {

                //start date is in the past, bring it forward 
                if (DateTime.Compare(dateNow, dt.startDate) > 0)
                {
                    dt.startTime = dateNow.ToString("HH:mm");
                    dt.endTime = "00:00";
                    dt.startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, dateNow.Hour, dateNow.Minute, 0);
                    dt.endDate = new DateTime(dt.startDate.AddYears(1).Year, dt.startDate.Month, dt.startDate.Day, 0, 0, 0);
                }
                //start date is in the future - update times
                else if (DateTime.Compare(dt.startDate.Date, dateNow.Date) > 0)
                {
                    //only setting this to midnight if the actual start date is 
                    //greater than todays date.. ie at least tomorrow
                    dt.startTime = "00:00";
                    dt.endTime = "00:00";
                    dt.startDate = new DateTime(dt.startDate.Year, dt.startDate.Month, dt.startDate.Day, 0, 0, 0);
                    dt.endDate = new DateTime(dt.startDate.AddYears(1).Year, dt.startDate.Month, dt.startDate.Day, 0, 0, 0);
                }
                //start date is today
                else if (DateTime.Compare(dt.startDate.Date, dateNow.Date) == 0)
                {
                    dt.startTime = dateNow.ToString("HH:mm");
                    var starthoursA = Convert.ToInt16(dt.startTime.Split(':')[0]);
                    var startminsA = Convert.ToInt16(dt.startTime.Split(':')[1]);
                    dt.endTime = "00:00";
                    dt.startDate = new DateTime(dt.startDate.Year, dt.startDate.Month, dt.startDate.Day, starthoursA, startminsA, 0);
                    dt.endDate = new DateTime(dt.startDate.AddYears(1).Year, dt.startDate.Month, dt.startDate.Day, 0, 0, 0);
                }

            }
            else if (dt.PolicyType == "E") //Event policies
            {                
                //start date is in the past, bring it forward - update start date to now
                if (DateTime.Compare(dateNow, dt.startDate) > 0)
                {
                    dt.startTime = dateNow.ToString("HH:mm");
                    dt.startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, dateNow.Hour, dateNow.Minute, 0);
                    if (!dt.endTimeRequired)
                    {
                        dt.endTime = "23:59";
                    }
                    if (!dt.dateToRequired)
                    {
                        dt.endDate = new DateTime(dt.startDate.Year, dt.startDate.Month, dt.startDate.Day, 23, 59, 0);
                        if (dt.numberOfDays > 1 && dt.PolicyPK == 6)
                        {
                            dt.endDate = dt.endDate.AddDays(dt.numberOfDays - 1);
                        }
                    }
                }
                //start date is in the future - defaults from 00:00 to 23:59
                else if (DateTime.Compare(dt.startDate, dateNow) > 0)
                {
                    //in the future so make end date same as start date if its not explicit
                    if (!dt.dateToRequired)
                        dt.endDate = new DateTime(dt.startDate.Year, dt.startDate.Month, dt.startDate.Day, 23, 59, 0);
                    if (!dt.endTimeRequired)
                        dt.endTime = "23:59";
                    if(dt.numberOfDays > 1)
                    {
                        dt.endDate = dt.endDate.AddDays(dt.numberOfDays-1);
                    }
                }
                //start date is today
                else if (DateTime.Compare(dt.startDate.Date, dateNow.Date) == 0)
                {
                    if (!dt.startTimeRequired)
                        dt.startTime = dateNow.ToString("HH:mm");
                    if (!dt.endTimeRequired)
                        dt.endTime = "23:59";
                    if (dt.numberOfDays > 1)
                    {
                        dt.endDate = dt.endDate.AddDays(dt.numberOfDays-1);
                    }
                }

            }
            return dt;
        }
    }
}