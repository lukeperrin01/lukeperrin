﻿using System.Net.Mail;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Configuration;

namespace LeisureInsure.Helpers
{
    public class EmailHelper
    {
        public static void SendMailMessage()
        {
            // Instantiate a new instance of MailMessage
            MailMessage mMailMessage = new MailMessage();


            // Set the sender address of the mail message
            mMailMessage.From = new MailAddress("danny@leisureinsure.co.uk");
            // Set the recepient address of the mail message
            mMailMessage.To.Add(new MailAddress("danny@leisureinsure.co.uk"));
            // Check if the bcc value is null or an empty string
            //if ((bcc != null) && (bcc != string.Empty))
            //{
            //    // Set the Bcc address of the mail message
            //    mMailMessage.Bcc.Add(new MailAddress(bcc));
            //}

            // Check if the cc value is null or an empty value
            //if ((cc != null) && (cc != string.Empty))
            //{
            //    // Set the CC address of the mail message
            //    mMailMessage.CC.Add(new MailAddress(cc));
            //}       // Set the subject of the mail message
            mMailMessage.Subject = "test email";
            // Set the body of the mail message
            mMailMessage.Body = "testing";
            // Set the format of the mail message body as HTML
            mMailMessage.IsBodyHtml = false;
            // Set the priority of the mail message to normal
            mMailMessage.Priority = MailPriority.Normal;

            // Instantiate a new instance of SmtpClient
            SmtpClient mSmtpClient = new SmtpClient();
            // Send the mail message
            try
            {
                mSmtpClient.Send(mMailMessage);
            }
            catch (Exception) { }
        }
    }
}