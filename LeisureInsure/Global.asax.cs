﻿using System.Data.Entity;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LeisureInsure.Nsb.Configuration;
using LeisureInsure.Services.Dal.LeisureInsure;
using Microsoft.Practices.Unity;
using NServiceBus;
using StackExchange.Profiling;
using StackExchange.Profiling.EntityFramework6;
using Stripe;
using Microsoft.Azure;
using ItOps.Endpoint.Registrations;

namespace LeisureInsure
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private IEndpointInstance _endpoint;

        public static string BasePath { get; private set; }

        protected void Application_Start()
        {
            ServicePointManager.DefaultConnectionLimit = 100;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.Expect100Continue = false;

            BasePath = Server.MapPath("~");

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            
            // Canelcase json
            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Miniprofiler db 
            MiniProfilerEF6.Initialize();
            Database.SetInitializer<LiContext>(null);

            var container = UnityConfig.ConfiguredContainer();
            
            // Setup NServiceBus bus
            var endpoint = NServiceBusConfig.EndpointName(Constants.Endpoints.Website);

            _endpoint = NServiceBusConfig.ConfigureSendOnlyBus(container, endpoint).GetAwaiter().GetResult();

            //Setup Stripe
            StripeConfiguration.SetApiKey(CloudConfigurationManager.GetSetting("stripeSecretKey"));
            
            //Templates.Initialise();
        }

        public override void Dispose()
        {
            _endpoint?.Stop();
            base.Dispose();
        }

        protected void Application_BeginRequest()
        {
            var ignore = false;
            var path = Request.Path.ToLowerInvariant();
            if (path.StartsWith("/mini-profiler") || path.StartsWith("/__browserlink/"))
            {
                ignore = true;
            }      
            if (Request.IsLocal && !ignore)
            {
                MiniProfiler.Start();
            }
        }

        protected void Application_EndRequest()
        {
            MiniProfiler.Stop();
        }
    }
}
