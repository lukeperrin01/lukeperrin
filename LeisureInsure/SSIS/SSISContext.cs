﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using LeisureInsure.SSIS.Models;



namespace LeisureInsure.SSIS
{
    public class SSISContext : DbContext
    {
        public SSISContext() : base("SSISTest") { }

        private DbSet<Csrs> Csrs { get; set; }
    }
}