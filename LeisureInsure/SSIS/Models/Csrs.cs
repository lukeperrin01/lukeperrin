﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.SSIS.Models
{
    public class Csrs : IEntity
    {
        [Key]
        [Column("CsrsPK")]
        public int Id { get; set; }
        public string CsrName { get; set; }
        public string CsrEmail { get; set; }
        public string CsrInitials { get; set; }
    }
}