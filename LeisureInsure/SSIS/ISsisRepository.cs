﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace LeisureInsure.SSIS
{
    public interface ISSISRepository : IDisposable
    {
        IQueryable<T> StoredProcedure<T>(string functionName, params object[] parameters);
        IQueryable<T> Function<T>(string functionName, params object[] parameters);

        void Add<T>(T entity) where T : class, IEntity;

        void Delete<T>(T entity) where T : class, IEntity;

        IQueryable<T> Q<T>(params Expression<Func<T, object>>[] eagerLoadProperties) where T : class, IEntity;

        T Find<T>(long id) where T : class, IEntity;

        int Commit();
    }
}