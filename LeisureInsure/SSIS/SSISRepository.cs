﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace LeisureInsure.SSIS
{
    public class SSISRepository : ISSISRepository
    {
        private readonly SSISContext _context;
        private bool _disposed;

        public SSISRepository(SSISContext context)
        {
            _context = context;
        }

        public IQueryable<T> Function<T>(string functionName, params object[] parameters)
        {
            var result = ((IObjectContextAdapter)_context).ObjectContext.ExecuteStoreQuery<T>($"SELECT * FROM {functionName}({string.Join(",", parameters)})").ToList();
            return result.AsQueryable();
        }

        public IQueryable<T> StoredProcedure<T>(string functionName, params object[] parameters)
        {


            var result = ((IObjectContextAdapter)_context).ObjectContext.ExecuteStoreQuery<T>($"EXEC {functionName} {string.Join(",", parameters)}").ToList();
            return result.AsQueryable();
        }

        public void Add<T>(T entity) where T : class, IEntity
        {
            _context.Set<T>().Add(entity);
        }

        public void Delete<T>(T entity) where T : class, IEntity
        {
            _context.Set<T>().Remove(entity);
        }

        public IQueryable<T> Q<T>(params Expression<Func<T, object>>[] eagerLoadProperties) where T : class, IEntity
        {
            var result = _context.Set<T>().AsQueryable();
            foreach (var includeRelationship in eagerLoadProperties)
            {
                result = result.Include(includeRelationship);
            }
            return result;
        }

        public T Find<T>(long id) where T : class, IEntity
        {
            return _context.Set<T>().Find(id);
        }

        public int Commit()
        {
            int recordsAffected;

            try
            {
                recordsAffected = _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                foreach (var dbEntityEntry in e.Entries)
                {
                    Console.WriteLine("Error updating DB");
                    Debug.WriteLine("Type: {0} \n\nState: {1}\nEntity: {2}",
                        dbEntityEntry.Entity.GetType(), dbEntityEntry.State, dbEntityEntry.Entity);
                }
                throw;
            }
            catch (DbEntityValidationException e)
            {
                throw;
            }
            return recordsAffected;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }

    }
}