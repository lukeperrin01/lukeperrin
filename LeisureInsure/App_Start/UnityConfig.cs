using System;
using System.Threading;
using System.Web;
using Microsoft.Practices.Unity;
using System.Web.Http;
using System.Web.Mvc;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.Tam;
using LeisureInsure.DB;
using LeisureInsure.DB.Identity;
using LeisureInsure.DB.Models.Identity;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Registrations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ContextFactory = LeisureInsure.DB.ContextFactory;
using ItOps.Endpoint.Services.Interfaces;
using ItOps.Endpoint.Services.Emailer;
using LeisureInsure.Charges;
using LeisureInsure.Charges.Utilities;
using LeisureInsure.Charges.AdditionalCharges;
using LeisureInsure.Charges.Rates;
using LeisureInsure.Charges.Discounts;
using LeisureInsure.Charges.Completion;
using LeisureInsure.Services.Services.Payment;
using LeisureInsure.SSIS;


namespace LeisureInsure
{
    public static class UnityConfig
    {
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(InitialiseContainer, LazyThreadSafetyMode.ExecutionAndPublication); 

        public static IUnityContainer ConfiguredContainer()
        {
            return Container.Value;
        }

        private static IUnityContainer InitialiseContainer()
        {
			var container = new UnityContainer();

            DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

            // Add providers here

            // Auth
            container.RegisterType<IAuthenticationManager>(new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));
            container.RegisterType<ApplicationUserManager>(new InjectionFactory(c => HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>()));
            container.RegisterType<IUserStore<LiApplicationUser>> (new InjectionFactory(c => new UserStore<LiApplicationUser>(new ApplicationIdentityDbContext())));

            // Repository
            container.RegisterType<LeisureInsureEntities>(new InjectionFactory(c => ContextFactory.Context()));
            container.RegisterType<IRepository, Repository>(new PerRequestLifetimeManager());


            container.RegisterType<ISSISRepository, SSISRepository>(new PerRequestLifetimeManager());


            container.RegisterType<LiContext>(new InjectionFactory(c => ContextFactory.LiContext()));
            container.RegisterType<ILiRepository, DefaultRepository>(new PerRequestLifetimeManager());

            // Tam
            container.RegisterType<IInterfaceWithTam, TamClient>(new TransientLifetimeManager());
            container.RegisterType<ICreateTamNames, TamNameCreator>();

            //charges
            container.RegisterType<ICalculateQuoteTotals, CalculateQuoteTotals>(new PerResolveLifetimeManager());
            container.RegisterType<ICalculateAllCoverCharges, AllCoverFeeCalculator>(new PerResolveLifetimeManager());
            container.RegisterType<IFindEmployersLiabilityMinimumPremium, EmployersLiabilityMinimumPremiumFinder>(new PerResolveLifetimeManager());
            container.RegisterType<ICalculateLoiLoading, LoiLoadingCalculator>(new PerResolveLifetimeManager());
            container.RegisterType<ICalculateLoi, LoiCalculator>(new PerResolveLifetimeManager());
            container.RegisterType<ICalculateCardCharges, CardChargeCalculator>(new PerResolveLifetimeManager());

            // Charging
            container.RegisterType<ICalculateTax, TaxCalculator>(new PerResolveLifetimeManager());
            container.RegisterType<ICalculateLegalCareFee, LegalCareFeeCalculator>(new PerResolveLifetimeManager());
            container.RegisterType<ICalculateLeisureInsureFees, LeisureInsureFeeCalculator>(new PerResolveLifetimeManager());
            container.RegisterType<ICalculateAgentCommision, AgentCommisionCalculator>(new PerResolveLifetimeManager());

            container.RegisterType<IProvideChargeEngines, ChargeEngineFactory>(new PerResolveLifetimeManager());
            container.RegisterType<IProvideDiscountEngines, DiscountEngineFactory>(new PerResolveLifetimeManager());
            container.RegisterType<ICreateQuoteDetails, QuoteDetailsCreator>(new PerResolveLifetimeManager());

            // Payments
            container.RegisterType<IAddPaymentOptions, PaymentOptionAdder>(new PerResolveLifetimeManager());

            RegisterServices.WithUnity(container);

            AutoMapperConfig.ConfigureUsing(container);
            
            return container;
        }
    }
}