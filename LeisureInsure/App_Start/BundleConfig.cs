﻿using System.Web.Optimization;

namespace LeisureInsure
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/site")
                //.Include("~/Content/Site.css")
                .Include("~/Content/css/hover-min.css")
                .Include("~/Content/css/slick.css")
                .Include("~/Content/css/slick-theme.css")
                );

            bundles.Add(new StyleBundle("~/bundles/cert").Include(
                      "~/Content/CertificatePdf.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-switch").Include(
                      "~/Scripts/respond.min.js",
                      "~/Scripts/bootstrap-switch-min.js"
                      ));

            bundles.Add(new StyleBundle("~/bundles/bootstrap-switch").Include(
                        "~/Content/bootstrap-switch-min.css"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/libraries").
                Include("~/Scripts/lib/slick.min.js")
                .Include("~/Scripts/template/spin.js")
                .Include("~/Scripts/template/ladda.js")
                .Include("~/Scripts/template/ladda-angular.min.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/oldAngularApp")
                
                );

            bundles.Add(new ScriptBundle("~/bundles/angularApp")
                .Include("~/App/Admin/app.js")

                .Include("~/App/Route.js")
                .Include("~/App/App.js")
                .Include("~/App/Models/UserType.js")

                .Include("~/App/Directives/homeDirectives.js")
                .Include("~/App/Filters/homeFilters.js")
                .Include("~/Scripts/Helper.js")
                );

            bundles.Add(new StyleBundle("~/bundles/templatecss")
                .Include("~/Content/template/owl-carousel/owl.carousel.css")
                .Include("~/Content/template/owl-carousel/owl.transitions.css")
                .Include("~/Content/template/custom.css")
                .Include("~/Content/template/app.css")
                .Include("~/Content/template/blocks.css")
                .Include("~/Content/template/plugins/style-switcher.css")
                .Include("~/Content/template/style.css")
                .Include("~/Content/template/theme-colors/dark-red.css")
                .Include("~/Content/template/headers/header-default.css")
                .Include("~/Content/template/footers/footer-v3.css")
                .Include("~/Content/template/plugins/font-awesome/css/font-awesome.min.css")
                .Include("~/Content/Style.css")
                .Include("~/Content/SiteUpdate.css")
                .Include("~/Content/template/ladda-themeless.min.css")
                );

            bundles.Add(new ScriptBundle("~/bundles/templatescripts")
                .Include("~/Scripts/modernizr-2.6.2.js")
                .Include("~/Content/template/owl-carousel/owl.carousel.js")
                .Include("~/Scripts/template/forms/masking.js")
                .Include("~/Scripts/template/plugins/fancy-box.js")
                .Include("~/Scripts/template/plugins/validation.js")
                .Include("~/Scripts/template/custom.js")
                .Include("~/Scripts/template/app.js")
                .Include("~/Scripts/template/jquery.parallax.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/ie")
                .Include("~/Scripts/template/ie/html5shiv.js")
                .Include("~/Scripts/template/ie/placeholder-IE-fixes.js")
                .Include("~/Scripts/template/ie/respond.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/app")

                ////////////////////////////////////////////////
                ////////         FILTERS                  //////
                ////////////////////////////////////////////////
                .Include("~/App/Filters/NameFilter.js")
                .Include("~/App/Filters/SafeUrl.js")

                ////////////////////////////////////////////////
                ////////         COMPONENTS               //////
                ////////////////////////////////////////////////
                .Include("~/App/Components/CallsToAction/CallsToActionController.js")
                .Include("~/App/Components/PolicyInformation/PolicyInformation.js")

                ////////////////////////////////////////////////
                ////////         DIRECTIVES               //////
                ////////////////////////////////////////////////
                .Include("~/App/Directives/LiDatePicker.js")
                .Include("~/App/Directives/LiDatePickerto.js")
                .Include("~/App/Directives/timePicker.js")
                .Include("~/App/Directives/repeatEnd.js")
                .Include("~/App/Directives/showShortNumber.js")
                ////////////////////////////////////////////////
                ////////         INTERCEPTORS             //////
                ////////////////////////////////////////////////
                .Include("~/App/DataService/Auth/HttpServiceInterceptor.js")

                ////////////////////////////////////////////////
                ////////         SERVICES                  //////
                ////////////////////////////////////////////////
                .Include("~/App/DataService/PolicyUpdateDataService.js")
                .Include("~/App/DataService/CoverService.js")
                .Include("~/App/DataService/PolicyService.js")
                .Include("~/App/DataService/Navigation/CoverNameService.js")
                .Include("~/App/DataService/Navigation/PolicyNameService.js")
                .Include("~/App/DataService/PolicyUpdateDataService.js")
                .Include("~/App/DataService/MetadataService.js")
                .Include("~/App/DataService/CoverInputsService.js")
                .Include("~/App/DataService/Navigation/UrlToNavigationService.js")
                .Include("~/App/DataService/LocationService.js")
                .Include("~/App/DataService/Navigation/UrlToNavigationService.js")
                .Include("~/App/DataService/LocationService.js")
                .Include("~/App/DataService/ChargeService.js")
                .Include("~/App/DataService/CertificateService.js")
                .Include("~/App/DataService/Auth/AuthenticationService.js")
                .Include("~/App/DataService/Auth/AuthorisationService.js")
                .Include("~/App/DataService/Admin/LandingPageService.js")
                .Include("~/App/DataService/PostCodeService.js")
                .Include("~/App/Admin/Services/QuoteStorageService.js")
                .Include("~/App/DataService/PriceService.js")
                .Include("~/App/DataService/CommonDataService.js")
                .Include("~/App/DataService/StripeService.js")
                .Include("~/App/DataService/RateUpdaterService.js")
                ////////////////////////////////////////////////
                ////////         CONTROLLERS              //////
                ////////////////////////////////////////////////
                .Include("~/App/controllers/MetadataController.js")
                .Include("~/App/Homepage/HomepageController.js")
                .Include("~/App/Policiespage/PoliciesController.js")
                .Include("~/App/coverInputspage/CoverInputsController.js")
                .Include("~/App/controllers/UserController.js")
                .Include("~/App/Paymentpage/PaymentController.js")
                .Include("~/App/controllers/LocaleController.js")
                .Include("~/App/CustomerDetailsPage/CustomerDetailsController.js")
                .Include("~/App/controllers/StaticPageLoginController.js")
                .Include("~/App/account/AccountController.js")
                .Include("~/App/PLInputsPage/PLInputsController.js")
                .Include("~/App/Landing/LandingController.js")
                .Include("~/App/NewPayment/NewPaymentController.js")
                .Include("~/App/Purchase/PurchaseController.js")
                .Include("~/App/DetailsPage/DetailsController.js")
                .Include("~/App/Confirmation/ConfirmationController.js")
                .Include("~/App/BrokerPortal/BrokerPortalController.js")
                .Include("~/App/Admin/LandingPages/LandingPageController.js")
                .Include("~/App/Admin/Calculator/CalculatorController.js")
                .Include("~/App/Admin/Menu/MenuController.js")
                .Include("~/App/Admin/Broker/BrokerController.js")
                .Include("~/App/Admin/Renewal/RenewalController.js")
                .Include("~/App/Admin/WebError/WebErrorController.js")
                .Include("~/App/Admin/Underwriter/UnderwriterController.js")
                .Include("~/App/Admin/Underwriter/Quotes/UnderwriterQuotesController.js")
                .Include("~/App/Admin/AddUser/AddUserController.js")
                .Include("~/App/Admin/FailedTAM/FailedTAMController.js")
                .Include("~/App/Admin/RateChecker/RateCheckerController.js")                
             );

            bundles.Add(new ScriptBundle("~/bundles/jquery").
                Include("~/Scripts/jquery-1.12.4.min.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/jquerypayment").
                Include("~/Scripts/jquery-payment.js")
                );
                       

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").
                Include("~/Scripts/jquery-ui-1.12.1.min.js")               
                );

            bundles.Add(new StyleBundle("~/Theme/redmond/css").
               Include("~/Theme/redmond/jquery-ui-1.10.3.custom.min.css")
               );

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                    "~/Content/themes/base/jquery-ui.min.css"));
          
        }
    }
}