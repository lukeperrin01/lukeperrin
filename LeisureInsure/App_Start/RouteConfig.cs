﻿using System.Web.Mvc;
using System.Web.Routing;
using LeisureInsure.Utilities;

namespace LeisureInsure
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("app/{*.}");

            routes.Add(new LegacyRedirectRoute());

            routes.MapRoute(
                name: "Tests",
                url: "test/{action}",
                defaults: new { controller = "Test" });

            routes.MapRoute(
                name: "PaymentResponse",
                url: "payment-response",
                defaults: new { controller = "Payment", action = "Index" });

            routes.MapRoute(
                name: "PaymentFailedEmailQuote",
                url: "payment/email-quote",
                defaults: new {controller = "Payment", action = "Email"});

            routes.MapRoute(
                name: "CertificateGenerator",
                url: "certificates/generate",
                defaults: new { controller = "Pdf", action = "generatecertificate" }
            );
            routes.MapRoute(
                name: "Certificate",
                url: "certificate/{quoteReference}/{passcode}",
                defaults: new { controller = "pdf", action = "certificate" }
            );

            //new website  ***************************
            routes.MapRoute(
                name: "newcertificate",
                url: "NewCertificate/{quoteReference}/{passcode}",
                defaults: new { controller = "pdf", action = "NewCertificate" }
            );

            routes.MapRoute(
               name: "newstatement",
               url: "GetNewStatementofFact/{quoteReference}",
               defaults: new { controller = "pdf", action = "GetNewStatementofFact" }
           );

            // ****************************************

            routes.MapRoute(
                name: "OldCertificates",
                url: "oldCerts/{quotePK}",
                defaults: new { controller = "pdf", action = "OldCertificate" }
            );
            routes.MapRoute(
                name: "CertificateGeneration",
                url: "pdf/{action}",
                defaults: new { controller = "pdf" }
            );
           
            routes.MapRoute(
                name: "statement",
                url: "GetStatementofFact/{quoteReference}",
                defaults: new { controller = "pdf", action = "GetStatementofFact" }
            );
            routes.MapRoute(
                name: "Tam",
                url: "AddTamClient/{quoteReference}",
                defaults: new { controller = "test", action = "AddTamClient"}
            );
            routes.MapRoute(
                name: "Sitemap",
                url: "sitemap.xml",
                defaults: new { controller = "sitemap", action = "index" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{*.}",
                defaults: new { controller = "Home", action = "Index" }
            );
            
            routes.AppendTrailingSlash = true;
        }
    }
}
