﻿using LeisureInsure.DB.Identity;
using LeisureInsure.OwinOptions;
using Microsoft.Owin.Security;
using Owin;

namespace LeisureInsure
{ 
	public partial class Startup
	{
	    private void ConfigureAuth(IAppBuilder app)
	    {
	        app.CreatePerOwinContext(ApplicationIdentityDbContext.Create);
	        app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
	        app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

	        app.UseOAuthAuthorizationServer(new OAuthOptions());
	        app.UseJwtBearerAuthentication(new JwtOptions
	        {
	            AuthenticationMode = AuthenticationMode.Active
	        });
    	}
    }
}