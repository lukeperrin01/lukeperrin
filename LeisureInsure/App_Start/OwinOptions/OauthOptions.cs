﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;

namespace LeisureInsure.OwinOptions
{
    public class OAuthOptions : OAuthAuthorizationServerOptions
    {
        public OAuthOptions()
        {
            TokenEndpointPath = new PathString("/api/token");
            AccessTokenExpireTimeSpan = TimeSpan.FromDays(30);
            AccessTokenFormat = new LiJwtFormat(this);
            Provider = new MyOAuthProvider();
//#if DEBUG
            AllowInsecureHttp = true;
//#endif
        }
    }
}