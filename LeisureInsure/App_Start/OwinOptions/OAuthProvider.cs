﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using LeisureInsure.DB;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.OAuth;

namespace LeisureInsure.OwinOptions
{
    public class MyOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity("otc");
            var username = context.OwinContext.Get<string>("otc:username");
            identity.AddClaim(new Claim(ClaimTypes.Name, username));
            identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
            var type = context.OwinContext.Get<string>("otc:type");
            if (type != null)
            {
                identity.AddClaim(new Claim("user_type", type));
            }
            
            context.Validated(identity);
            return Task.FromResult(0);
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            try
            {
                var authentication = context.OwinContext.Get<ApplicationUserManager>();

                var username = context.Parameters["username"];
                var password = context.Parameters["password"];

                var user = await authentication.FindAsync(username, password);
                if (user != null)
                {
                    var dbContext = context.OwinContext.Get<LeisureInsureEntities>();
                    var liUser = dbContext.Contacts.FirstOrDefault(x => x.TamClientRef == username);
                    if (liUser == null)
                    {
                        context.SetError("Your account has not been activated yet");
                    }
                    else
                    {
                        context.OwinContext.Set("otc:type", liUser.ContactType.ToString());
                    }
                    context.OwinContext.Set("otc:username", username);
                    
                    context.Validated();
                }
                else
                {
                    context.SetError("Invalid credentials");
                }
            }
            catch
            {
                context.SetError("Server error");
            }
        }
    }
}