﻿using System;
using System.Configuration;
using Microsoft.Owin.Security.Jwt;

namespace LeisureInsure.OwinOptions
{
    public class JwtOptions : JwtBearerAuthenticationOptions
    {
        public JwtOptions()
        {
            var issuer = "www.leisureinsure.co.uk";
            var audience = "all";
            var key = Convert.FromBase64String(ConfigurationManager.AppSettings["jwt.key"]); 

            AllowedAudiences = new[] { audience };
            IssuerSecurityTokenProviders = new[] {new SymmetricKeyIssuerSecurityTokenProvider(issuer, key)};
        }
    }
}

