﻿using AutoMapper;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Utilities;
using Microsoft.Practices.Unity;

namespace LeisureInsure
{
    public static class AutoMapperConfig
    {
        public static void ConfigureUsing(IUnityContainer container)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<QuoteLine, QuoteLine>();
                cfg.CreateMap<Discount, Discount>();
                cfg.CreateMap<Answer, Answer>();
                cfg.CreateMap<Address, Address>();
                cfg.CreateMap<Quote, Quote>()
                    .Ignore(x => x.PolicyId)
                    .Ignore(x => x.Customers)
                    .Ignore(x => x.Addresses)
                    .Ignore(x => x.Underwriters);

                cfg.CreateMap<QuoteDocument, QuoteDocument>();

                cfg.CreateMap<vmContact, Agent>().ForMember(x => x.Id, x => x.MapFrom(src => src.ContactPK))
                    .Ignore(x => x.TamClientRef)
                    .Ignore(x => x.FSANumber)
                    .Ignore(x => x.BrokerCommission)
                    .Ignore(x => x.ContactStatus)
                    .Ignore(x => x.CommissionPolicyBW)
                    .Ignore(x => x.Locked)
                    .Ignore(x => x.QuotePK);
                //.Ignore(x => x.ContactType);

                cfg.CreateMap<vmContact, Customer>().ForMember(x => x.Id, x => x.MapFrom(src => src.ContactPK))
                    .Ignore(x => x.TamClientRef)
                    .Ignore(x => x.FSANumber)
                    .Ignore(x => x.BrokerCommission)
                    .Ignore(x => x.ContactStatus)
                    .Ignore(x => x.CommissionPolicyBW)
                    .Ignore(x => x.Locked)
                    .Ignore(x => x.QuotePK);
                //.Ignore(x => x.ContactType);
                cfg.CreateMap<vmContact, Underwriter>().ForMember(x => x.Id, x => x.MapFrom(src => src.ContactPK))
                    .Ignore(x => x.TamClientRef)
                    .Ignore(x => x.FSANumber)
                    .Ignore(x => x.BrokerCommission)
                    .Ignore(x => x.ContactStatus)
                    .Ignore(x => x.CommissionPolicyBW)
                    .Ignore(x => x.Locked)
                    .Ignore(x => x.QuotePK);
                //.Ignore(x => x.ContactType);

                cfg.CreateMap<vmAddress, Address>().ForMember(x => x.Id, x => x.MapFrom(src => src.AddressPK));


            });

            var quoteMapper = new Mapper(mapperConfig);





#if DEBUG
            mapperConfig.AssertConfigurationIsValid();
#endif

            container.RegisterInstance<IMapper>(quoteMapper);
        }
    }
}