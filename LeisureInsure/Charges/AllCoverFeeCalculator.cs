﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeisureInsure.DB;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Charges.AdditionalCharges;
using LeisureInsure.Charges.Discounts;
using LeisureInsure.Services.Services.Information;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.Services.Services.Utilities.Interfaces;
using LeisureInsure.Services.Utilities;
using System.Diagnostics;
using LeisureInsure.DB.Logging;
using System.Text.RegularExpressions;
using CoverTypeHelper = LeisureInsure.Services.Utilities.CoverType;

namespace LeisureInsure.Charges
{
    public class AllCoverFeeCalculator : ICalculateAllCoverCharges
    {
        private readonly IProvideChargeEngines _provider;
        private readonly IGetCoverInformation _cover;
        private readonly ICalculateTax _tax;
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        private readonly IProvideDiscountEngines _discount;
        private readonly IMakeNumbersFriendly _numberAbbreviator;

        public AllCoverFeeCalculator(
            IProvideChargeEngines provider,
            IGetCoverInformation cover,
            ICalculateTax tax,
            IRepository repository,
            LocaleInfo.IGetLocaleInformation locale,
            IProvideDiscountEngines discount,
            IMakeNumbersFriendly numberAbbreviator)
        {
            _provider = provider;
            _cover = cover;
            _tax = tax;
            _repository = repository;
            _locale = locale;
            _discount = discount;
            _numberAbbreviator = numberAbbreviator;
        }

        public Quote Calculate(ChargeCalculationInputs model, IList<ChargeElements> chargeElements)
        {
            try
            {

                //Check if personal assault input has come through, if it has check it has a value - if not remove it
                var personalCover = chargeElements.Where(x => x.RatePK == 949).FirstOrDefault();
                if(personalCover != null)
                {
                    if(string.IsNullOrEmpty(personalCover.RateValue) )
                    {
                        chargeElements.Remove(personalCover);
                    }
                    else
                    {
                        personalCover.SumInsured = chargeElements.Where(x => x.CoverFK == 17).Sum(x => int.Parse(x.RateValue));
                    }
                }

                decimal weddingSumInsured = 0;
                decimal weddingLiability = 0;
                int weddingExcess = 0;
                decimal productLiability = 0;

                var loc = model.PolicyInformation.LocaleId;

                var pcode = chargeElements.Where(x => x.RateTypeFK == 1).FirstOrDefault();

                var summary = new Quote
                {
                    ChargeSummaryItems = new List<QuoteLine>(),
                    Discounts = new List<Discount>(),
                    IsAgent = model.PolicyInformation.ShowFees,
                    TamBrokerCode = model.PolicyInformation.BrokerTamName,
                    PolicyId = model.PolicyInformation.PolicyId,
                    LocaleId = model.PolicyInformation.LocaleId,
                    ReferralType = ReferralType.None
                };

                //this is used for either producer codes or discount codes
                var codename = chargeElements.Where(x => x.RatePK == 5303).FirstOrDefault();
                if (codename != null)
                    summary.CodeName = codename.RateValue;

                var refer = chargeElements.Sum(x => x.Refer);
                if (refer > 0)
                {
                    summary.ReferralType = ReferralType.Referred;
                }

                // The covers to deal with
                var coverListPK = new List<int>();
                var coverList = new List<Covers>();

                coverListPK = (from i in chargeElements
                               select i.CoverFK
                             ).Distinct().ToList();

                coverList = _repository.getCovers().ToList();

                coverList = coverList.Where(c => coverListPK.Contains(c.CoverPK)).ToList();


                IList<ChargeElements> hazards = chargeElements.Where(x => x.PageNo == 2 && x.DivBy != 0 && x.CoverFK > 1).ToList();

                foreach (ChargeElements c in hazards)
                {
                    c.SectionNo = coverList.Where(x => x.CoverPK == c.CoverFK).FirstOrDefault().Section;
                    //c.CoverFK = 0;
                }
                coverListPK = (from i in chargeElements.Where(x => x.PageNo != 2)
                               select i.CoverFK
                             ).Distinct().ToList();

                var localId = model.PolicyInformation.LocaleId;

                var locale = _repository.getLocale(localId);

                //var policyInfo = _repository.Find<Locales>(localId);            

                coverList = _repository.getCovers().ToList();

                coverList = coverList.Where(c => coverListPK.Contains(c.CoverPK)).ToList();

                // Go through all covers and get base prices
                decimal totalHazardValues = 0;
                int plExcess = 0;
                decimal plIndemnity = 0;
                string plPeriodOfCover = "";
                //accumulate contents (exclude buildings, business equip)
                decimal accumContents = 0;

                //show additional information in the quote summary
                var extraInfo = chargeElements.Where(x => x.RateLabel == "Additional");
                foreach (ChargeElements item in extraInfo)
                {
                    //additional info is from dropdown, may need revising in future
                    var desc = item.RateName.Split(':')[0];
                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Price = -10,
                        LineDescription = desc,
                        SumInsured = 0,
                        Excess = 0,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        sectiondescription = "Additional Information",
                        sectionTypeDescription = item.RateValue
                    });
                }

                foreach (var cover in coverList)
                {

                    var chargeEngine = _provider.CoverChargeCalculator(model.PolicyInformation.PolicyId, cover.CoverPK);

                    var elementsToCalculate = chargeElements.Where(x => x.CoverFK == cover.CoverPK && x.PageNo < 2).ToList();

                    var hazardquestions = chargeElements.Where(x => x.RateTypeFK == 18);

                    //refer if combined values for Material Damage and Business Interruption exceeds 3M.
                    if (cover.Section == 4 || cover.Section == 5)
                    {
                        foreach (ChargeElements item in elementsToCalculate)
                        {
                            decimal value = 0, decvalue;
                            bool dectest = false;

                            dectest = decimal.TryParse(item.RateValue, out decvalue);
                            if (dectest)
                                value = decvalue;
                            if (!dectest)
                                value = item.SumInsured;

                            //check if total of BI and MD
                            totalHazardValues += value;

                            //accumulate contents (exclude buildings, business equip)
                            if (cover.Section == 4)
                            {
                                if (cover.CoverPK != 8 && cover.CoverPK != 4 && cover.CoverPK != 5)
                                {
                                    accumContents += value;
                                }
                            }
                        }
                    }
                    if (totalHazardValues >= locale.LocaleRefer)
                        summary.ReferralType = ReferralType.Referred;

                    bool alarmInstalled = false;
                    var alarm = hazardquestions.Where(x => x.RateName.Contains("alarm")).FirstOrDefault();
                    if (alarm != null)
                    {
                        if (!alarm.RateValue.ToLower().Contains("none"))
                            alarmInstalled = true;
                    }
                    //refer if we have no alarm and our cover value exceeds set amount
                    if (cover.ReferValue != null)
                    {
                        //accumulate equipment values - ie CDs, canoes
                        decimal value = 0;
                        foreach (var item in elementsToCalculate)
                        {
                            decimal dectest;
                            var result = decimal.TryParse(item.RateValue, out dectest);
                            value += dectest;
                            if (!result)
                                value += item.SumInsured;

                            if ((value >= cover.ReferValue || accumContents >= cover.ReferValue) && !alarmInstalled)
                                summary.ReferralType = ReferralType.Referred;
                        }
                    }

                    if (cover.CoverPK == CoverTypeHelper.BrandProtection)
                    {
                        var limitOfCover = elementsToCalculate.Where(x => x.ListRatePK == -4231).FirstOrDefault();
                        if (Convert.ToInt32(limitOfCover.Rate) > 2000000)
                            summary.ReferralType = ReferralType.Referred;

                        Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                        var allPrem = rgx.Replace(elementsToCalculate.Where(x => x.RatePK == 4232).FirstOrDefault().RateValue, "");
                        var largestPrem = rgx.Replace(elementsToCalculate.Where(x => x.RatePK == 4234).FirstOrDefault().RateValue, "");

                        var allPremises = Convert.ToInt32(allPrem);
                        var largestPremises = Convert.ToInt32(largestPrem);
                        var noOfPremises = Convert.ToInt32(elementsToCalculate.Where(x => x.RatePK == 4235).FirstOrDefault().RateValue);

                        if (largestPremises > 1000000)
                            summary.ReferralType = ReferralType.Referred;

                        if ((allPremises / noOfPremises) > 1000000)
                            summary.ReferralType = ReferralType.Referred;

                        //refer if we have 4 or more referable questions that answer no
                        var referableQuestions = elementsToCalculate.Where(x => x.RateTypeFK == RateType.ReferableQuestion).ToList();
                        int counter = 0;
                        foreach (var question in referableQuestions)
                        {
                            //these are tickboxes, if ratevalue is "" it is unticked
                            if (question.RateValue.ToLower() == "")
                                counter++;
                        }
                        if (counter >= 4)
                            summary.ReferralType = ReferralType.Referred;
                    }

                    //add our Limit of Cover value to our free covers (Brand Protection)
                    if (elementsToCalculate[0].RateTypeFK == RateType.FoodCovers)
                    {
                        var limitOfCover = chargeElements.Where(x => x.ListRatePK == -4231).FirstOrDefault();
                        elementsToCalculate.Add(limitOfCover);
                    }

                    var chargeInformation = chargeEngine.Calculate(model.PolicyInformation, elementsToCalculate);

                    if (chargeInformation.Refer)
                        summary.ReferralType = ReferralType.Referred;

                    if (chargeInformation.Premium > 0)
                    {
                        foreach (var r in hazards.Where(x => x.SectionNo == cover.Section))
                        {
                            if (r.RateTypeFK != 23)
                            {
                                chargeInformation.Premium += (decimal)(chargeInformation.Premium * r.Rate) / (int)r.DivBy;
                            }
                            else
                            {
                                chargeInformation.Premium += (decimal)(r.Excess * r.Rate) / (int)r.DivBy;
                            }
                        }
                    }
                    if (cover.CoverPK == 2)
                    {
                        productLiability = chargeInformation.Indemnity;
                        plExcess = (int)chargeInformation.Excess;
                        plIndemnity = (int)chargeInformation.Indemnity;
                        plPeriodOfCover = chargeInformation.PeriodOfCover;
                    }
                    if (!chargeInformation.ExcludeFromSummary)
                    {
                        var net = chargeInformation.Premium;

                        if (net > 0)
                        {
                            if (pcode != null)
                            {
                                if (pcode.RateValue.Substring(0, 2).ToUpper() == "BT")
                                {
                                    net += net * (Decimal).50;
                                }
                            }
                            net *= _locale.Multiplier(loc, model.PolicyInformation.PolicyId,
                                cover.CoverPK);
                        }


                        var lineDescription = _cover.Description(cover.CoverPK);
                        var sectionDescription = _cover.SectionDescription(cover.CoverPK);
                        var sectionTypeDescription = _cover.SectionTypeDescription(cover.CoverPK);

                        var remainder = ((int)chargeInformation.Indemnity % locale.ExchangeRate);
                        if (remainder != 0)
                        {
                            chargeInformation.Indemnity = (int)chargeInformation.Indemnity * locale.ExchangeRate;

                        }

                        if (sectionTypeDescription == null)
                            sectionTypeDescription = sectionDescription;

                        if (model.PolicyInformation.PolicyId == 10 || model.PolicyInformation.PolicyId == 11)
                        {
                            if (cover.CoverPK == 2)
                            {
                                lineDescription = "Personal Liability";
                                weddingLiability = chargeInformation.SumInsured;
                                sectionDescription = "Personal Liability";
                                sectionTypeDescription = null;


                            }
                            if (cover.CoverPK == 41)
                            {
                                weddingSumInsured = chargeInformation.SumInsured;
                                weddingExcess = (int)chargeInformation.Excess;
                                sectionDescription = "Standard Cover";
                                sectionTypeDescription = null;
                            }
                            if (cover.CoverPK == 3)
                            {
                                sectionDescription = "Liability to Ceremony Services Suppliers";
                            }
                        }

                        if (lineDescription.Contains("-"))
                        {
                            lineDescription = lineDescription.Split('-')[1];
                        }

                        if (chargeInformation.Indemnity > 0)
                        {
                            lineDescription += " - " + _locale.Symbol(model.PolicyInformation.LocaleId) + _numberAbbreviator.Transform(chargeInformation.Indemnity);
                        }

                        if (cover.CoverPK == 18 || cover.CoverPK == 20)
                        {
                            chargeInformation.Excess = plExcess;
                            if (cover.CoverPK == 18 && chargeInformation.Indemnity == 0)
                            {
                                chargeInformation.Indemnity = plIndemnity;
                                if (locale.LocalePK == 1)
                                {
                                    if (plIndemnity > 5000000)
                                    {
                                        chargeInformation.Indemnity = 5000000;
                                    }

                                }
                                if(locale.LocalePK == 2)
                                {
                                    chargeInformation.Indemnity = plIndemnity;
                                    if (plIndemnity > 6500000)
                                    {
                                        chargeInformation.Indemnity = 6500000;
                                    }

                                }
                            }
                            if (cover.CoverPK == 20 )
                            {

                                chargeInformation.Indemnity = 1000000 * model.PolicyInformation.ExchangeRate;

                            }
                            lineDescription = _cover.Description(cover.CoverPK);
                            lineDescription += " - " + _locale.Symbol(model.PolicyInformation.LocaleId) + _numberAbbreviator.Transform(chargeInformation.Indemnity);
                        }
                        if (model.PolicyInformation.PolicyId != 10 || model.PolicyInformation.PolicyId != 11)
                        {
                            summary.Excess = chargeInformation.Excess;
                        }

                        if (string.IsNullOrWhiteSpace(chargeInformation.PeriodOfCover))
                        {
                            chargeInformation.PeriodOfCover = plPeriodOfCover;
                        }

                        //generally show everything in quote summary
                        bool showinSummary = true;
                        if (model.PolicyInformation.PolicyId == PolicyType.FoodContamination)
                        {
                            showinSummary = false;
                        }
                        if (model.PolicyInformation.PolicyId == 10 || model.PolicyInformation.PolicyId == 11)
                        {
                            //sectionDescription = "";
                            sectionTypeDescription = "";
                            if (cover.CoverPK == 3)
                            {
                                lineDescription = "Liability to Ceremony Services Suppliers - " + _locale.Symbol(model.PolicyInformation.LocaleId) + _numberAbbreviator.Transform(chargeInformation.Indemnity);

                            }
                            else if(cover.CoverPK == 33)
                            {
                                lineDescription = "Ceremony Equipment - " + _locale.Symbol(model.PolicyInformation.LocaleId) + _numberAbbreviator.Transform(chargeInformation.SumInsured);
                                if (chargeElements.Where(x => x.CoverFK == 41 && x.Excess > 0).FirstOrDefault() != null)
                                {
                                    chargeInformation.Excess = chargeElements.Where(x => x.CoverFK == 41 && x.Excess > 0).FirstOrDefault().Excess;
                                }
                                else
                                {
                                    chargeInformation.Excess = 0;
                                }
                            }


                        }

                        summary.ChargeSummaryItems.Add(new QuoteLine
                        {
                            //TODO:RE ADD TAX
                            //Price = price + ((price  * tax)),
                            Net = net,
                            Excess = chargeInformation.Excess,
                            LineDescription = lineDescription,
                            CoverId = cover.CoverPK,
                            coverFK = cover.CoverPK,
                            Territory = chargeInformation.Territory,
                            SumInsured = chargeInformation.SumInsured,
                            indemnity = chargeInformation.Indemnity,
                            PeriodOfCover = chargeInformation.PeriodOfCover,
                            sectiondescription = sectionDescription,
                            sectionTypeDescription = sectionTypeDescription,
                            ShowInSummary = showinSummary
                        });
                    }
                } // end loop through coverlist *******************************************

                //food contamination - these are not based on covers
                if (model.PolicyInformation.PolicyId == PolicyType.FoodContamination)
                {
                    //var maincover = summary.ChargeSummaryItems.Where(x=>x.r)
                    var limitOfCover = chargeElements.Where(x => x.ListRatePK == -4231).FirstOrDefault();
                    var excess = Math.Round(limitOfCover.Rate * 0.01m, 0);
                    //this is instead of excess
                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Net = 0,
                        Price = -1,
                        LineDescription = "Self-Insured Retention",
                        SumInsured = 0,
                        Excess = excess,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        ShowInSummary = true,
                    });

                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Net = 0,
                        Price = -2,
                        LineDescription = "Waiting Period",
                        SumInsured = 0,
                        Excess = 0,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        ShowInSummary = true,
                        ExtraInfo = "3 Days"
                    });
                }

                if (model.PolicyInformation.PolicyId == 10 || model.PolicyInformation.PolicyId == 11)
                {
                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Price = 0,
                        LineDescription = "Rescheduling of the Ceremony" + " - " +
                                          _locale.Symbol(model.PolicyInformation.LocaleId) +
                                          _numberAbbreviator.Transform(weddingSumInsured),
                        SumInsured = weddingSumInsured,
                        Excess = weddingExcess,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        ShowInSummary = false,
                    });
                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Price = 0,
                        LineDescription = "Ceremony Attire and Jewellery" + " - " +
                                          _locale.Symbol(model.PolicyInformation.LocaleId) +
                                          _numberAbbreviator.Transform(weddingSumInsured),
                        SumInsured = weddingSumInsured,
                        Excess = weddingExcess,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        ShowInSummary = false,
                    });
                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Price = 0,
                        LineDescription = "Ceremony Gifts" + " - " +
                                          _locale.Symbol(model.PolicyInformation.LocaleId) +
                                          _numberAbbreviator.Transform(weddingSumInsured),
                        SumInsured = weddingSumInsured,
                        Excess = weddingExcess,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        ShowInSummary = false,
                    });
                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Price = 0,
                        LineDescription = "Failure of Ceremony Services Supplier" + " - " +
                                          _locale.Symbol(model.PolicyInformation.LocaleId) +
                                          _numberAbbreviator.Transform(weddingSumInsured),
                        SumInsured = weddingSumInsured,
                        Excess = weddingExcess,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        ShowInSummary = false,
                    });
                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Price = 0,
                        LineDescription = "Ceremony Transportation, Cakes and Flowers" + " - " +
                                          _locale.Symbol(model.PolicyInformation.LocaleId) +
                                          _numberAbbreviator.Transform(weddingSumInsured),
                        SumInsured = weddingSumInsured,
                        Excess = weddingExcess,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        ShowInSummary = false,
                    });
                    summary.ChargeSummaryItems.Add(new QuoteLine
                    {
                        Price = 0,
                        LineDescription = "Photography and Video" + " - " +
                                          _locale.Symbol(model.PolicyInformation.LocaleId) +
                                          _numberAbbreviator.Transform(weddingSumInsured),
                        SumInsured = weddingSumInsured,
                        Excess = weddingExcess,
                        coverFK = -1,
                        indemnity = 0,
                        CoverId = -1,
                        ShowInSummary = false,
                    });
                }

                var referralRates = chargeElements.Where(x => x.Refer > 0).ToList();
                if (referralRates.Count > 0)
                {
                    if (summary.ReferralItems == null)
                    {
                        summary.ReferralItems = new List<ReferralItem>();
                    }
                    foreach (var referralRate in referralRates)
                    {
                        summary.ReferralItems.Add(new ReferralItem
                        {
                            RateId = referralRate.RatePK,
                        });
                    }
                }

                if (summary.ReferralItems?.Count > 0 && summary.ReferralType == ReferralType.None)
                {
                    summary.ReferralType = ReferralType.Referred;
                }

                // Calculate any discounts 
                _discount.EngineFor(DicountType.MultiParty).Calculate(summary, model.Answers, model.PolicyInformation.PolicyId);


                // div by zero error
                //var DivZero = 0;
                //var xxx = 1 / DivZero;
                return summary;

            }

            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.AllCoverFeeCalculator.Calculate] url:post:api/v1/charges", model.PolicyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}