﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Diagnostics;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Text.RegularExpressions;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class MaterialDamageBuildings : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamageBuildings(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {

                var charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                decimal excess = 0;
                if (!policyInformation.MDExcessAdded)
                {
                    excess = 250;
                    policyInformation.MDExcessAdded = true;
                }

                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                foreach (ChargeElements r in chargeElements.Where(x => x.Rate > 0))
                {
                    decimal Ratecharge = 0;
                    decimal sumInsured = 0;
                    int test;
                    bool isNumeric = int.TryParse(r.RateName, out test);
                    if (isNumeric)
                    {
                        sumInsured = decimal.Parse(Regex.Replace(r.RateName, "[^0-9.]", ""));
                    }
                    else
                    {
                        sumInsured = int.Parse(r.RateValue);
                    }
                    Ratecharge = (r.Rate * sumInsured) / r.DivBy;

                    if (Ratecharge != 0)
                    {
                        charge.Premium += Ratecharge;
                        _repository.AddInputCharge(r.QuoteInputPK, Ratecharge * _localeMultiplier);

                    }

                    charge.SumInsured += sumInsured;
                }
                charge.Indemnity = 0;
                charge.Excess = excess;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.MaterialDamageBuildings] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }

    }
}