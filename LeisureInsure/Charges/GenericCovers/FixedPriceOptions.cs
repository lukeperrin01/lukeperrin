﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Diagnostics;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class FixedPriceOptions : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public FixedPriceOptions(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {

                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                var excess = (decimal)0;
                var indemn = 0;
                foreach (ChargeElements r in chargeElements)
                {

                    decimal itemRate = 0;
                    itemRate = r.Rate;
                    charge.Premium += r.Rate;
                    if (r.RateTypeFK == 7 && r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    if (r.RateTypeFK == 7)
                    {
                        indemn = (int)r.Threshold;

                    }
                    if (r.CoverFK == 3)
                    {
                        excess = 0;
                        indemn = 10000000;
                        if (policyInformation.LocaleId == 2)
                        {
                            indemn = 13000000;
                        }
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }


                }
                charge.Excess = excess;

                charge.Indemnity = indemn;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.FixedPriceOptions] url:post:api/v1/charges" , policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}