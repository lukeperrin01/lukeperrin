﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System.Linq;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class PersonalAssault : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PersonalAssault(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                ChargeInformation charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                charge.Premium = 0;
                decimal excess = (decimal)0;

                foreach (ChargeElements r in chargeElements)
                {
                    decimal Ratecharge = 0;
                    Ratecharge = (decimal)r.Rate;



                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    if (Ratecharge != 0)
                    {
                        charge.SumInsured += (decimal)r.SumInsured;
                        charge.Premium += Ratecharge;
                        _repository.AddInputCharge(r.QuoteInputPK, Ratecharge * _localeMultiplier);
                    }
                }

                charge.Indemnity = 0;
                charge.PeriodOfCover = "";
                charge.Excess = excess;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.PersonalAssault] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
