﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Diagnostics;
using System;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class BusinessInteruptionLossOfGrossRentals : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public BusinessInteruptionLossOfGrossRentals(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                charge.Excess = (decimal)250;
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                decimal minRate = 0;
                decimal excess = 250;
                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;

                    charge.SumInsured += r.SumInsured;
                    if (r.Threshold > minRate)
                    {
                        minRate = (decimal)r.Threshold;
                    }
                    if (r.DivBy != 0)
                    {
                        itemRate = (charge.SumInsured * (decimal)r.Rate) / (int)r.DivBy;
                        if (r.Excess > excess)
                        {
                            excess = (decimal)r.Excess;

                        }
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.SumInsured += r.SumInsured;
                        r.chargeAdded = true;
                        charge.Premium += itemRate;
                    }

                }
                int i = 0;
                if (charge.Premium < minRate)
                {
                    foreach (ChargeElements r in chargeElements.Where(x => x.chargeAdded == true).OrderByDescending(x => x.Rate))
                    {
                        if (i == 0)
                        {
                            _repository.AddInputCharge(r.QuoteInputPK, minRate * _localeMultiplier);
                        }
                        else
                        {
                            _repository.AddInputCharge(r.QuoteInputPK, 0);
                        }
                        i++;
                    }
                }

                charge.Excess = excess;
                charge.Indemnity = 0;
                charge.Premium = Math.Max(charge.Premium, minRate);

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.BusinessInteruptionLossOfGrossRentals.Calculate] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}