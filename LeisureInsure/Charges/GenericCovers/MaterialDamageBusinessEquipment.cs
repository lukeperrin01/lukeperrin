﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class MaterialDamageBusinessEquipment : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamageBusinessEquipment(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public virtual ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charges = new ChargeInformation();
                decimal minRate = 0;
                decimal excess = 0;
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }
                var periodofCover = "";

                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;
                    if (r.RateTypeFK == 17)
                    {
                        periodofCover = r.RateName;

                    }
                    if (r.MinimumRate > minRate)
                    {
                        minRate = r.MinimumRate ?? 0;
                    }
                    if (r.SumInsured > 0)
                    {
                        charges.SumInsured += r.SumInsured;
                    }
                    else
                    {
                        charges.SumInsured += decimal.Parse(Regex.Replace(r.RateValue, "[^0-9.]", ""));

                    }
                    if (!policyInformation.MDExcessAdded)
                    {
                        excess = 250;
                        policyInformation.MDExcessAdded = true;
                    }

                    if (r.DivBy == 100)
                    {
                        itemRate = (decimal)(charges.SumInsured * r.Rate) / (int)r.DivBy;

                    }
                    else if (r.DivBy == 1)
                    {
                        itemRate = (decimal)r.Rate;

                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charges.Premium += itemRate;
                    }

                    //charges.SumInsured += r.SumInsured;
                }

                charges.Excess = excess;
                charges.Indemnity = 0;
                charges.Premium = Math.Max(charges.Premium, minRate);
                charges.Territory = territory;
                charges.PeriodOfCover = periodofCover;

                return charges;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.MaterialDamageBusinessEquipment] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}