﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class EmployersLiability : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public EmployersLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {

                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                IList<ChargeElements> applyminRates = new List<ChargeElements>();

                var charge = new ChargeInformation();

                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }

                var indemn = (int)10000000 * (int)policyInformation.ExchangeRate;
                decimal minRate = 0;
                decimal excess = (decimal)0;

                foreach (ChargeElements r in chargeElements.Where(x => x.RateTypeFK > 0))
                {
                    decimal itemRate = 0;
                    if (r.DivBy != 0)
                    {
                        itemRate = (r.Rate * r.SumInsured) / (int)r.DivBy;
                        if (r.Excess > excess)
                        {
                            excess = r.Excess;
                        }
                    }
                    charge.SumInsured += r.SumInsured;
                    if (itemRate != 0)
                    {
                        applyminRates.Add(r);
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }
                }



                if (charge.Premium > 0)
                {

                    var minRates = chargeElements.Where(x => x.RateTypeFK == 13).OrderBy(x => x.Threshold);

                    foreach (ChargeElements r in minRates)
                    {
                        if (charge.SumInsured < r.Threshold)
                        {
                            int i = 0;
                            minRate = r.Rate;
                            foreach (ChargeElements c in applyminRates)
                            {
                                if (i == 0)
                                {

                                    _repository.AddInputCharge(c.QuoteInputPK, minRate * _localeMultiplier);

                                }
                                else
                                {
                                    _repository.AddInputCharge(c.QuoteInputPK, 0);
                                }
                                i++;

                            }
                            break;
                        }
                    }
                    charge.Premium = Math.Max(charge.Premium, minRate);
                }

                charge.Territory = territory;
                charge.PeriodOfCover = "12 Months";
                charge.Excess = 0;
                charge.Indemnity = indemn;
                //}
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.EmployersLiability.Calculate] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}