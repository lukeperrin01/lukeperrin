﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

using System.Text.RegularExpressions;
using System;
using System.Diagnostics;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;


namespace LeisureInsure.Charges.GenericCovers
{
    class GenericFixedPrice: ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public GenericFixedPrice(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                charge.Premium = 0;
                decimal excess = (decimal)250;
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }
                var periodofCover = "";


                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;
                    if (r.RateTypeFK == 17)
                    {
                        periodofCover = r.RateName;

                    }
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    if (r.Rate > 0)
                    {
                        itemRate = Math.Max((decimal)r.Rate, (decimal)r.MinimumRate);

                    }
                    if (r.RateTypeFK == 22)
                    {

                        charge.SumInsured = 0;
                        var remainder = ((int)r.Threshold % policyInformation.ExchangeRate);
                        if (remainder == 0)
                        {
                            charge.Indemnity = (int)r.Threshold;
                        }
                        else
                        {
                            charge.Indemnity = (int)r.Threshold * policyInformation.ExchangeRate;
                        }
                    }
                    else if (r.SumInsured > 0)
                    {
                        charge.SumInsured += (decimal)r.SumInsured;
                    }
                    else if (r.Threshold > 0)
                    {
                        charge.SumInsured += (decimal)r.Threshold;

                    }
                    else
                    {

                        charge.SumInsured += decimal.Parse(Regex.Replace(r.RateName, "[^0-9.]", ""));
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }
                    if(r.CoverFK == 20)
                    {
                        charge.Indemnity = 1000000 * _localeMultiplier;
                    }

                }
                charge.Excess = excess;
                if (charge.Indemnity == 0)
                {
                    charge.Indemnity = (int)charge.SumInsured;
                }
                charge.Territory = territory;
                charge.PeriodOfCover = periodofCover;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.GenericFixedPrice] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }


    }
}
