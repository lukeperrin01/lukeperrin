using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB;
using System.Diagnostics;
using System;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class BusinessInteruptionIncreasedCostOfWorking : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public BusinessInteruptionIncreasedCostOfWorking(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                charge.Excess = (decimal)250;
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;

                    if (r.RateTypeFK == 5)
                    {
                        charge.Indemnity = (decimal)r.Indemnity;
                    }
                    if (r.RateTypeFK == 17)
                    {
                        itemRate = (r.Rate * r.SumInsured) / r.DivBy;
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);

                        charge.SumInsured += r.SumInsured;
                        charge.Premium += itemRate;
                    }

                }
                return charge;

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.BusinessInteruptionIncreasedCostOfWorking.Calculate] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}