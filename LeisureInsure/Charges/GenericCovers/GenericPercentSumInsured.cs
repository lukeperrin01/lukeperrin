﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Text.RegularExpressions;
using System;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System.Linq;
using System.Diagnostics;
using LeisureInsure.DB;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    class GenericPercentSumInsured : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public GenericPercentSumInsured(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                decimal sumInsured = 0;
                decimal excess = 0;
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }
                //var periodofCover = "12 Months";

                foreach (ChargeElements element in chargeElements)
                {
                    if (element.RateTypeFK == 17)
                    {
                        charge.PeriodOfCover = element.RateValue;

                    }
                    if (element.SumInsured != 0)
                    {
                        sumInsured = element.SumInsured;
                    }
                    else
                    {
                        sumInsured = int.Parse(Regex.Replace(element.RateValue, "[^0-9.]", ""));
                    }
                    if (element.Excess > excess)
                    {
                        excess = element.Excess;
                        charge.Excess = excess;
                    }

                    if (element.DivBy != 0)
                    {
                        decimal itemRate = 0;
                        itemRate = Math.Max((decimal)(element.Rate * sumInsured) / (int)element.DivBy, (decimal)element.MinimumRate);
                        if (itemRate != 0)
                        {

                            charge.Premium += itemRate;
                            _repository.AddInputCharge(element.QuoteInputPK, itemRate * _localeMultiplier);
                        }
                    }
                    charge.SumInsured += sumInsured;
                }
                charge.Territory = territory;
                //charge.PeriodOfCover = periodofCover;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.GenericPercentSumInsured] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
