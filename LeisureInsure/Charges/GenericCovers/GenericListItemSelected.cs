﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Diagnostics;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class GenericListItemSelected : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public GenericListItemSelected(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                var excess = (decimal)250;
                charge.Territory = "United Kingdom";
                var locMultiplier = policyInformation.ExchangeRate;
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }
                var periodofCover = "";
                var indemn = 0;

                var elements = chargeElements.Where(x => x.RateTypeFK == 10).ToList();

                //no premium? check suminsured
                if (elements.Count() == 0)
                {
                    elements = chargeElements.Where(x => x.RateTypeFK == 6).ToList();
                }

                foreach (ChargeElements r in elements)
                {
                    decimal itemRate = 0;
                    if (r.DivBy == 1)
                    {
                        itemRate = (decimal)r.Rate;

                    }
                    else if (r.DivBy == 100)
                    {
                        itemRate = (decimal)(r.Rate * r.SumInsured) / (int)r.DivBy;

                    }
                    else
                    {
                        itemRate = (decimal)r.Rate;
                        //divby could 0 so treat as 1, but make sure we have a rate
                        charge.Premium += (decimal)r.Rate;

                    }

                    //we want to include all rates but exclude turnover
                    if (r.RateName != "Turnover")
                    {
                        if (r.RateTypeFK == 5 || (r.RateTypeFK == 10 && r.Indemnity >= 500000))
                        {
                            if ((int)r.Indemnity % locMultiplier != 0)
                            {
                                indemn = (int)r.Indemnity * (int)locMultiplier;
                            }
                            else indemn = (int)r.Indemnity;
                        }
                    }
                    else
                    {
                        charge.SumInsured += (decimal)r.Threshold;

                    }
                    if (r.RateTypeFK == 17)
                    {
                        periodofCover = r.RateName;

                    }
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    if (r.CoverFK == 3)
                    {
                        excess = 0;
                        indemn = 10000000;
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }

                }
                charge.PeriodOfCover = periodofCover;
                charge.Excess = excess;
                charge.Indemnity = indemn;

                //charge.Indemnity = (int)charge.SumInsured;

                return charge;

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.GenericListItemSelected] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
