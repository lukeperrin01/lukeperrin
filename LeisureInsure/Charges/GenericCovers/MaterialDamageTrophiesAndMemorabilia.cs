﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Diagnostics;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.GenericCovers
{
    public class MaterialDamageTrophiesAndMemorabilia : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamageTrophiesAndMemorabilia(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                var charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                decimal excess = 0;
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                if (!policyInformation.MDExcessAdded)
                {
                    excess = 250;
                    policyInformation.MDExcessAdded = true;
                }

                foreach (ChargeElements c in chargeElements)
                {
                    decimal itemRate = 0;
                    if (c.RateTypeFK == 9)
                    {
                        if (c.RateValue == "Eire")
                        {
                            charge.Territory = "Republic of Ireland";
                        }
                        else
                        {
                            charge.Territory = c.RateValue;
                        }
                        charge.SumInsured += c.SumInsured;

                        itemRate = (c.Rate * c.SumInsured) / c.DivBy;
                        if (itemRate != 0)
                        {
                            charge.Premium += itemRate;
                            _repository.AddInputCharge(c.QuoteInputPK, itemRate * _localeMultiplier);
                        }
                    }

                }
                charge.Excess = excess;
                charge.PeriodOfCover = "12 Months";
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.GenericCovers.MaterialDamageTrophiesAndMemorabilia] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }

    }
}