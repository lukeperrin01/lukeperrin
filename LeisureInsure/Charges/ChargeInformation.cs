﻿namespace LeisureInsure.Charges
{
    public class ChargeInformation
    {

        public ChargeInformation()
        {
            Refer = false;
        }

        public decimal Premium { get; set; }
        public decimal SumInsured { get; set; }
        public decimal Excess { get; set; }
        public string Territory { get; set; }
        public decimal Indemnity { get; set; }
        public string PeriodOfCover { get; set; }
        public bool Refer { get; set; }
        public bool ExcludeFromSummary { get; set; }
    }
}