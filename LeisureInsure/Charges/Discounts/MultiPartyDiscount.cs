﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Services.Utilities;
using System.Diagnostics;
using System;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Discounts
{
    public class MultiPartyDiscount : ICalculateDiscounts
    {
        public Quote Calculate(Quote summary, List<ChargeCalculationAnswer> answers, int policyId)
        {
            try
            {
                var eligibleCover = summary.ChargeSummaryItems.FirstOrDefault(x => x.CoverId == 2);
                if (eligibleCover != null)
                {
                    // Find covers that could have multi party discounts
                    // Number of instructors
                    var numberOfInstructors = answers.FirstOrDefault(x => x.RateTypeFK == 24)?.RateValue.ToSafeInt();
                    var instructorDiscount = 0m;
                    switch (numberOfInstructors)
                    {
                        case 1:
                        case 2:
                        case 3:
                            instructorDiscount = 0;
                            break;
                        case 4:
                        case 5:
                        case 6:
                            instructorDiscount = 10; // %
                            break;
                        case 7:
                        case 8:
                            instructorDiscount = 20; // %
                            break;
                    }
                    if (instructorDiscount > 0)
                    {
                        var discount = eligibleCover.Price * (instructorDiscount / 100);
                        summary.Discounts.Add(new Discount
                        {
                            LineDescription = "Multi-party Discount",
                            Price = discount,
                        });
                    }
                }

                return summary;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Discounts.MultiPartyDiscount.Calculate] url:post:api/v1/charges", summary.QuoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}