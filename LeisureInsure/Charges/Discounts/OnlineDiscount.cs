﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Diagnostics;
using System;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Discounts
{
    public class OnlineDiscount : ICalculateDiscounts
    {
        public Quote Calculate(Quote summary, List<ChargeCalculationAnswer> answers, int policyId)
        {
            try
            {
                // Increase the fee for each cover and apply a discount to bring 
                //      the fee back to what we started with
                var discountRate = 20m; // %

                var discountMultiplier = 1 - (discountRate / 100);
                var increaseMultiplier = 1 / (discountMultiplier);
                var totalPriceBeforeDiscount = 0m;

                foreach (var item in summary.ChargeSummaryItems)
                {
                    item.Price = item.Price * increaseMultiplier;
                    totalPriceBeforeDiscount += item.Price;
                }

                // Remove any old discounts
                var existingDiscount =
                    summary.Discounts.FirstOrDefault(x => x.LineDescription == $"{discountRate}% Online Discount");
                if (existingDiscount != null)
                {
                    summary.Discounts.Remove(existingDiscount);
                }

                var discount = totalPriceBeforeDiscount * (discountRate / 100);
                summary.Discounts.Add(new Discount
                {
                    LineDescription = $"{discountRate}% Online Discount",
                    Price = discount,
                });
                return summary;
            }

            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Discounts.OnlineDiscount.Calculate] url:post:api/v1/charges", summary.QuoteReference);
                throw new ApplicationException($"{errorid}");

            }
        }
    }
}