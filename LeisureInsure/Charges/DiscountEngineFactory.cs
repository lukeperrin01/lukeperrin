﻿using System;
using LeisureInsure.Charges.Discounts;
using Microsoft.Practices.Unity;

namespace LeisureInsure.Charges
{
    public class DiscountEngineFactory : IProvideDiscountEngines
    {
        private readonly IUnityContainer _container;

        public DiscountEngineFactory(IUnityContainer container)
        {
            _container = container;
        }

        public ICalculateDiscounts EngineFor(DicountType discountType)
        {
            switch (discountType)
            {
                case DicountType.MultiParty:
                    return _container.Resolve<MultiPartyDiscount>();
                case DicountType.Online:
                    return _container.Resolve<OnlineDiscount>();

            }
            throw new ApplicationException($"Please include discount engine for {discountType}");
        }
    }
}