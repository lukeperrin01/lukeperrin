﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Charges.AdditionalCharges;


namespace LeisureInsure.Services.Services.Payment
{
    public class PaymentOptionAdder : IAddPaymentOptions
    {
        private readonly IEnumerable<IAmAPaymentOption> _paymentOptions;
        private readonly ICalculateCardCharges _cardCharges;
        
        public PaymentOptionAdder(IEnumerable<IAmAPaymentOption> paymentOptions, 
            ICalculateCardCharges cardCharges)
        {
            _paymentOptions = paymentOptions;
            _cardCharges = cardCharges;
        }

        public void AddPaymentOptionsTo(Quote quote)
        {
            // Check that the collection is initialised
            if (quote.PaymentOptions == null)
            {
                quote.PaymentOptions = new List<PaymentOption>();
            }

            foreach (var paymentOption in _paymentOptions)
            {
                paymentOption.AddIfAllowed(quote);
            }

            foreach (var paymentOption in quote.PaymentOptions)
            {
                var cardCharge = _cardCharges.AddChargeTo(paymentOption.InitialPayment, quote.LocaleId,
                    quote.PaymentCardId);
                                
                    if (paymentOption.Id == quote.PayOption)
                        quote.CardCharge = cardCharge;
                

                paymentOption.InitialPayment += cardCharge;
            }

        }
    }
}