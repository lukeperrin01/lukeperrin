﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.StreetParties
{
    public class PublicLiability : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charges = new ChargeInformation();
                var rate = chargeElements.FirstOrDefault(x => x.RateTypeFK == 5);
                var premium = chargeElements.FirstOrDefault(x => x.RateTypeFK == 10);
                decimal indemnity = 0;
                //var charges = new List<ChargeItem>();

                charges.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charges.Territory = "Republic of Ireland";
                }

                foreach (ChargeElements r in chargeElements.Where(x => x.PageNo == 1))
                {
                    decimal itemRate = 0;
                    if (r.RateTypeFK == 5 && r.Indemnity > indemnity)
                    {
                        indemnity = (decimal)r.Indemnity;

                    }
                    if (r.RateTypeFK == 10)
                    {
                        if (r.Threshold > 0)
                        {
                            itemRate = r.Rate;
                        }
                        else
                        {
                            itemRate = r.Rate * int.Parse(r.RateValue);

                        }
                        if (itemRate != 0)
                        {
                            _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                            charges.Premium += itemRate;
                        }
                    }

                }
                charges.Excess = rate.Excess;

                charges.Indemnity = indemnity;
                charges.PeriodOfCover = "1 Day";
                return charges;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.StreetParties.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
