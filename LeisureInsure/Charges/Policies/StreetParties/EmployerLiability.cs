﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.StreetParties
{
    class EmployerLiability : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public EmployerLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                var premium = chargeElements.FirstOrDefault(x => x.RateTypeFK == 10);
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";

                }
                foreach (ChargeElements c in chargeElements)
                {
                    decimal itemRate = 0;
                    if (c.RateTypeFK == 10)
                    {
                        itemRate = c.Rate;
                    }
                    if (itemRate != 0)
                    {
                        charge.Premium += itemRate;
                        _repository.AddInputCharge(c.QuoteInputPK, itemRate * _localeMultiplier);
                    }
                }

                if (charge.Premium > 0)
                {
                    charge.Excess = 0;
                    charge.Indemnity = 10000000;
                    charge.PeriodOfCover = "1 Day";
                }
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.StreetParties.EmployerLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
