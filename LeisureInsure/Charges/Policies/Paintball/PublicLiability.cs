﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Charges.Policies.Paintball
{
    public class PublicLiability : ICalculateCoverCharges
    {
        private readonly ICalculateLoi _loi;

        public PublicLiability(ICalculateLoi loi)
        {
            _loi = loi;
        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {

            return new ChargeInformation
            {
                //SumInsured = _loi.Calculate(answers, policyInformation, rates),
            };
        }
    }
}