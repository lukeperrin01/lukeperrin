﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Wedding
{
    public class Cancellation : ICalculateCoverCharges
    {
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal excess = 0;
                var indemn = 0;
                var charge = new ChargeInformation();
                foreach (ChargeElements r in chargeElements.Where(x => x.Rate > 0))
                {
                    charge.Premium += (decimal)r.Rate;
                    if (r.RateTypeFK == 10)
                    {
                        charge.SumInsured += (decimal)r.Threshold;
                    }
                    else if (r.RateTypeFK == 5)
                    {
                        indemn = (int)r.Threshold;
                    }
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                }
                charge.Indemnity = indemn;
                charge.Excess = excess;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Wedding.Cancellation] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
