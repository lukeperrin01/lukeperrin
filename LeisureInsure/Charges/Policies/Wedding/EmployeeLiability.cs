﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Wedding
{
    public class EmployeeLiability : ICalculateCoverCharges
    {
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {

                var charge = new ChargeInformation();
                //charge.Excess = 200;
                charge.Premium = (decimal)chargeElements.FirstOrDefault().Rate;
                charge.SumInsured = (decimal)chargeElements.FirstOrDefault().SumInsured;
                charge.Indemnity = (decimal)chargeElements.FirstOrDefault().Indemnity;
                charge.Territory = "United Kingdom";
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Wedding.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }

    }
}