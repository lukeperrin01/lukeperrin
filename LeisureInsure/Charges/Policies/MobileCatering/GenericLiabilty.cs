﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.MobileCatering
{
    public class GenericLiabilty : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public GenericLiabilty(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }


        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                var excess = (decimal)250;
                var territory = "United Kingdom";
                var locMultiplier = policyInformation.ExchangeRate;
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";

                }

                var periodofCover = "12 Months";
                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;

                    itemRate = (decimal)r.Rate;
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }
                    if (r.Indemnity > 0)
                    {
                        charge.Indemnity = ((int)r.Indemnity * locMultiplier);
                        if(r.CoverFK == 20)
                        {
                            charge.Indemnity = 1000000 * _localeMultiplier;
                        }

                    }
                    if (r.SumInsured > 0)
                    {
                        charge.SumInsured += (decimal)r.SumInsured;

                    }
                    if (r.RateTypeFK == 17)
                    {
                        periodofCover = r.RateName;

                    }
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    if (r.CoverFK == 3)
                    {
                        excess = 0;
                    }


                }

                

                charge.PeriodOfCover = periodofCover;
                charge.Excess = excess;
                charge.Territory = territory;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.MobileCatering.GenericLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }


        }
    }
}
