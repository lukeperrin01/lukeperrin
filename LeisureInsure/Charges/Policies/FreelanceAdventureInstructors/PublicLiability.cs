﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.FreelanceAdventureInstructors
{
    public class PublicLiability : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();

                IList<ChargeElements> loadings = new List<ChargeElements>();
                charge.Territory = "United Kingdom";
                charge.Excess = (decimal)250;
                charge.PeriodOfCover = "12 Months";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                var hazRate = chargeElements.Where(x => x.RateTypeFK == 16 && x.DivBy == 1).FirstOrDefault();

                int hazRatedi = 0;
                foreach (ChargeElements c in chargeElements)
                {
                    decimal itemRate = 0;

                    if (c.RateTypeFK == 5)
                    {
                        charge.Indemnity = (decimal)c.Indemnity * policyInformation.ExchangeRate;

                    }
                    if (c.RateTypeFK == 16 && c.DivBy == 0)
                    {

                        if (c.HazardRating == hazRate.HazardRating && hazRatedi == 0)//apply the rating against one item with matching hazardrating
                        {
                            itemRate = hazRate.Rate;
                            hazRatedi++;
                        }

                    }
                    if (c.RateTypeFK == 23)
                    {
                        itemRate = c.Rate;

                    }
                    if (c.RateTypeFK == 6)
                    {
                        charge.SumInsured = decimal.Parse(c.RateValue);
                    }
                    if (c.RateTypeFK == 9) //worldwideCoverLoading
                    {
                        loadings.Add(c);
                    }
                    if (c.RateTypeFK == 24) //numberOfInstructors
                    {
                        loadings.Add(c);
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(c.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }
                }

                var noInstructors = 1;
                var totalPremim = charge.Premium;
                foreach (ChargeElements c in loadings)
                {
                    decimal itemRate = 0;
                    if (c.RateTypeFK == 9 && c.Rate != 0)
                    {
                        itemRate = (c.Rate * charge.Premium) / c.DivBy;
                        charge.Territory = "Worldwide";
                    }
                    if (c.RateTypeFK == 24)
                    {
                        noInstructors = int.Parse(c.RateValue);
                        itemRate = hazRate.Rate * (noInstructors - 1);

                    }
                    if (c.RateTypeFK == 24)
                    {
                        noInstructors = int.Parse(c.RateValue);
                        itemRate = hazRate.Rate * (noInstructors - 1);

                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(c.QuoteInputPK, itemRate * _localeMultiplier);
                        totalPremim += itemRate;
                    }
                }

                charge.Premium = totalPremim;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.FreeLanceAdventureInstructors.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}