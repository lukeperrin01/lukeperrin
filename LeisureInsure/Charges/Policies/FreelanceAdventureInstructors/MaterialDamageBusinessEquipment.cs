﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.FreelanceAdventureInstructors
{
    class MaterialDamageBusinessEquipment : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamageBusinessEquipment(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;
        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charges = new ChargeInformation();
                charges.Territory = "United Kingdom";
                charges.Excess = 250;
                if (policyInformation.LocaleId == 2)
                {
                    charges.Territory = "Republic of Ireland";
                }
                charges.PeriodOfCover = "12 Months";

                foreach (ChargeElements c in chargeElements)
                {
                    decimal itemRate = 0;
                    if (c.RateTypeFK == 10 && c.DivBy == 1)
                    {
                        itemRate = c.Rate;
                        charges.SumInsured += c.SumInsured;
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(c.QuoteInputPK, itemRate * _localeMultiplier);
                        charges.Premium += itemRate;
                    }
                }

                charges.Indemnity = 0;
                return charges;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.FreelanceAdventurerInstructors.MaterialDamageBusinessEquipment] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }
    }
}
