﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.DayCover
{
    public class PublicLiability : ICalculateCoverCharges
    {
        private readonly ICalculateLoi _loi;
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(ICalculateLoi loi, IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _loi = loi;
            _repository = repository;
            _locale = locale;
        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {
                // different rates and covers have different eire loadings - premium must be multiplied by this loading
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                decimal excess = 250;
                charge.Territory = "United Kingdom";
                charge.PeriodOfCover = "1 day";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }
                List<ChargeElements> loadings = new List<ChargeElements>();

                // Indemn charge must be extracted because it is added to each item charge
                var indemn = chargeElements.Where(x => x.Indemnity > 0).FirstOrDefault();

                if (indemn != null)
                {
                    chargeElements.Remove(indemn);
                    charge.Indemnity = indemn.Indemnity * policyInformation.ExchangeRate ?? 0;
                }
                else
                {
                    throw new ApplicationException($"Indemnity line not present");
                }

                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;
                    var noItems = 1;

                    if (r.DivBy == 1)
                    {
                        if (r.NoItems > 1)
                        {
                            noItems = r.NoItems;

                        }
                        itemRate = (decimal)(r.Rate * noItems) + (indemn.Rate * noItems);
                        if (r.Excess > excess)
                        {
                            excess = r.Excess;
                        }
                    }
                    if (r.DivBy > 1)
                    {
                        loadings.Add(r);
                    }

                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }
                }
                decimal loadRate = charge.Premium;
                foreach (ChargeElements r in loadings)
                {
                    decimal itemRate = (decimal)((decimal)charge.Premium * (decimal)r.Rate) / (int)r.DivBy;
                    _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                    loadRate += itemRate;
                }
                charge.Excess = excess;
                charge.Premium = loadRate;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.DayCover.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}