﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System.Diagnostics;
using System;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.EquipmentHirers
{
    public class PublicLiability : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {
                ChargeInformation charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                charge.Excess = 250;
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                // This will contain each of the charges
                var charges = new List<ChargeItem>();
                // Percentage loading to be added after net premium is calculated
                var loadings = new List<ChargeElements>();

                foreach (ChargeElements r in chargeElements.Where(x => x.PageNo == 1 && x.RateTypeFK == 10))
                {
                    // Get the specific charges for the item (varies based on BIHA or Equivalent membership
                    var chargeItem = new ChargeItem { NumberOfItems = r.NoItems };
                    if (policyInformation.MemberOfDiscountOrganisation)
                    {
                        chargeItem.InitialCharge = r.DiscountFirstRate;
                        chargeItem.SubsequentCharge = r.DiscountSubsequentRate;
                        chargeItem.QuoteInputPK = r.QuoteInputPK;
                    }
                    else
                    {
                        chargeItem.InitialCharge = r.FirstRate ?? 0;
                        chargeItem.SubsequentCharge = r.Rate;
                        chargeItem.QuoteInputPK = r.QuoteInputPK;
                    }
                    r.chargeAdded = true;
                    charges.Add(chargeItem);
                }

                // Process the charges. The rule is to take the highest maximum charge 
                //      and then the subsequent charge for each other item
                var highestPricedItem = charges.FirstOrDefault(x => x.InitialCharge == charges.Max(y => y.InitialCharge));
                if (highestPricedItem == null)
                {
                    // No chargable items
                    return new ChargeInformation();
                }

                // Start with the highest charge
                var totalCharge = highestPricedItem.InitialCharge;

                // Reduce the highestPricedItem number of items by 1
                highestPricedItem.NumberOfItems--;

                //if number of highestPricedItem is still greater than 0 charge subsequent items at lower rate
                if (highestPricedItem.NumberOfItems > 0)
                {
                    totalCharge += highestPricedItem.SubsequentCharge * highestPricedItem.NumberOfItems;
                }
                // save charge for highestPricedItem
                _repository.AddInputCharge(highestPricedItem.QuoteInputPK, totalCharge * _localeMultiplier);

                // Remove highestPricedItem from charges and charge everything else at the lower rate
                charges.Remove(highestPricedItem);
                foreach (ChargeItem c in charges.Where(x => x.NumberOfItems > 0))
                {
                    totalCharge += c.SubsequentCharge * c.NumberOfItems;
                    _repository.AddInputCharge(c.QuoteInputPK, (c.SubsequentCharge * c.NumberOfItems) * _localeMultiplier);
                }

                // Loop through chargeElements and apply charges that are NOT applied as a percentage against the totalcharge
                // Where charges are applied against totalcharge add them to 'loadings' array
                var pub = 0;
                foreach (ChargeElements c in chargeElements.Where(x => x.chargeAdded == false))
                {
                    decimal Ratecharge = 0;
                    if (c.RateTypeFK == 18)// nightclub loading -- only apply one of the two
                    {
                        if (pub == 0 && c.Rate > 0)
                        {
                            loadings.Add(c);
                            pub++;
                        }
                    }
                    if (c.RateTypeFK == 7) //Selected Excess
                    {
                        charge.Excess = c.Excess;
                        loadings.Add(c);
                    }
                    if (c.RateTypeFK == 23)//Excess Waiver
                    {
                        loadings.Add(c);
                    }
                    if (c.RateTypeFK == 5)//Idemnity
                    {
                        loadings.Add(c);
                        charge.Indemnity = (decimal)c.Indemnity * policyInformation.ExchangeRate;
                    }
                    if (Ratecharge != 0)
                    {
                        _repository.AddInputCharge(c.QuoteInputPK, Ratecharge * _localeMultiplier);
                        totalCharge += Ratecharge;
                    }
                    c.chargeAdded = true;
                }
                var LoadedTotalCharge = totalCharge;
                foreach (ChargeElements c in loadings)
                {
                    decimal loading = 0;
                    if (c.RateTypeFK == 18) // Nightclub loading 
                    {
                        loading = (totalCharge * c.Rate) / 100;
                    }
                    if (c.RateTypeFK == 5) // Indemnity loading
                    {
                        if (!policyInformation.MemberOfDiscountOrganisation)
                        {
                            loading = totalCharge * (decimal)(c.Rate / (int)c.DivBy);
                        }
                    }
                    if (c.RateTypeFK == 7) // Selected Excess loading
                    {
                        loading = (totalCharge * (decimal)c.Rate) / (int)c.DivBy;
                    }
                    if (c.RateTypeFK == 23) // Nil excess loading
                    {
                        loading = (charge.Excess * (decimal)c.Rate) / (int)c.DivBy;
                    }
                    if (loading != 0)
                    {
                        _repository.AddInputCharge(c.QuoteInputPK, loading * _localeMultiplier);
                        LoadedTotalCharge += loading;
                    }
                }
                

                charge.Premium = LoadedTotalCharge;
                charge.SumInsured = (decimal)0;
                charge.PeriodOfCover = "12 Months";

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.EquipmentHirers.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
