﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using System.Diagnostics;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.EquipmentHirers
{
    public class EmployerLiability : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public EmployerLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                ChargeInformation charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }
                var indemn = (int)10000000 * (int)policyInformation.ExchangeRate;


                decimal minRate = 0;

                foreach (ChargeElements r in chargeElements.Where(x => x.RateTypeFK > 0))
                {
                    decimal itemcharge = 0;

                    if (r.DivBy != 0)
                    {
                        itemcharge = (decimal)(r.Rate * r.SumInsured) / (int)r.DivBy;

                    }
                    charge.SumInsured += r.SumInsured;
                    if (itemcharge != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemcharge * _localeMultiplier);
                        charge.Premium += itemcharge;
                        r.chargeAdded = true;
                    }
                }

                if (charge.Premium > 0)
                {
                    foreach (ChargeElements r in chargeElements.Where(x => x.RateTypeFK == 13).OrderBy(x => x.Threshold))
                    {
                        if (charge.SumInsured < r.Threshold)
                        {
                            minRate = (decimal)r.Rate;
                            int i = 0;
                            foreach (ChargeElements rr in chargeElements.Where(x => x.chargeAdded == true))
                            {
                                if (i == 0)
                                {
                                    _repository.AddInputCharge(r.QuoteInputPK, minRate * _localeMultiplier);
                                }
                                else
                                {
                                    _repository.AddInputCharge(r.QuoteInputPK, 0);
                                }
                            }

                            break;
                        }
                    }


                    charge.Premium = Math.Max(charge.Premium, minRate);
                }
                charge.Territory = territory;
                charge.PeriodOfCover = "";

                charge.Indemnity = indemn;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.EquipmentHirers.EmployerLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
