﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.EquipmentHirers
{
    public class ProfessionalLiability : ICalculateCoverCharges
    {
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                //TODO DATAWORK - CHECK EXCESS 
                var charge = new ChargeInformation();
                var rate = chargeElements.Where(x => x.RateValue != "").FirstOrDefault();
                var indemn = 0;
                if (policyInformation.LocaleId == 1)
                {
                    charge.Territory = "United Kingdom";
                }
                else
                {
                    charge.Territory = "Republic of Ireland";
                }
                if (rate != null)
                {
                    if (rate != null && rate.Indemnity > 0)
                    {
                        indemn = (int)rate.Indemnity;
                        if(rate.CoverFK == 20)
                        {
                            indemn = 1000000;
                            if (policyInformation.LocaleId == 2)
                            {
                                indemn = (int)(indemn * 1.3);
                            }
                        }
                    }
                    else
                    {
                        indemn = (int)rate.SumInsured;
                    }
                    charge.Indemnity = indemn;
                    charge.SumInsured = 0;
                    charge.Excess = rate.Excess;
                    charge.Premium = 0;
                }
                else if(chargeElements.FirstOrDefault().Indemnity > 0)
                {
                    var r = chargeElements.FirstOrDefault();
                    charge.Indemnity = (int)r.Indemnity;
                    charge.Excess = r.Excess;
                    charge.Premium = 0;

                }
                charge.PeriodOfCover = "";

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.EquipmentHirers.ProfessionalLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
