﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using System;
using System.Diagnostics;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.EquipmentHirers
{
    public class MaterialDamage : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamage(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;
        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                var territory = "United Kingdom";
                decimal excess = 250;
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);

                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                    excess = 500;
                }
                // This will contain each of the charges
                var charges = new ChargeInformation();
                decimal minRate = 0;

                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;
                    if (r.MinimumRate > minRate)
                    {
                        minRate = (decimal)r.MinimumRate;
                    }
                    if (r.DivBy != 0)
                    {
                        itemRate = (decimal)(r.SumInsured * r.Rate) / (int)r.DivBy;
                        charges.Premium += itemRate;
                        r.chargeAdded = true;
                    }

                    charges.SumInsured += r.SumInsured;
                    _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                }


                // if minrate > premium then set the first chargeable item to minrate and the rest to zero
                if (minRate > charges.Premium)
                {
                    int i = 0;
                    foreach (ChargeElements c in chargeElements.Where(x => x.chargeAdded == true))
                    {
                        i++;
                        if (i == 1)
                        {
                            _repository.AddInputCharge(c.QuoteInputPK, minRate * _localeMultiplier);
                        }
                        else
                        {
                            _repository.AddInputCharge(c.QuoteInputPK, 0);
                        }

                    }

                }

                charges.Premium = Math.Max(charges.Premium, minRate);

                if (policyInformation.MemberOfDiscountOrganisation == true)
                {
                    var freeCover = chargeElements.Where(x => x.RateTypeFK == 22).FirstOrDefault();
                    if (freeCover != null)
                        excess = freeCover.Excess;
                }

                charges.Excess = excess;
                charges.Territory = territory;
                charges.PeriodOfCover = "12 Months";
                charges.Indemnity = 0;


                return charges;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.EquipmentHirers.MaterialDamage] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
