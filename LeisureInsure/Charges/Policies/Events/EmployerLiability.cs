﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Text.RegularExpressions;
using System;
using System.Diagnostics;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Events
{
    public class EmployerLiability : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public EmployerLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try
            {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                var indemn = 10000000 * _localeMultiplier;
                var period = "12 Months";
                var territory = "United Kingdom";
                var minRate = (decimal)chargeElements.FirstOrDefault().MinimumRate;
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }
                charge.Indemnity = 10000000 * _localeMultiplier;

                foreach (ChargeElements r in chargeElements)
                {

                    decimal itemRate = 0;
                    if (r.DivBy == 100)
                    {
                        itemRate = (decimal)(r.Rate * r.SumInsured) / r.DivBy;
                    }
                    else if (r.DivBy == 1)
                    {
                        itemRate = r.Rate;
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }
                }

                if (charge.Premium > 0)
                {
                    foreach (ChargeElements r in chargeElements)
                    {
                        if (charge.Premium < minRate)
                        {

                            int i = 0;
                            foreach (ChargeElements rr in chargeElements)
                            {
                                if (i == 0)
                                {
                                    _repository.AddInputCharge(r.QuoteInputPK, minRate * _localeMultiplier);
                                }
                                else
                                {
                                    _repository.AddInputCharge(r.QuoteInputPK, 0);
                                }
                            }

                            break;
                        }
                    }


                    charge.Premium = Math.Max(charge.Premium, minRate);
                }

                charge.SumInsured = 0;
                charge.Territory = territory;
                charge.PeriodOfCover = period;
                charge.Excess = 0;
                return charge;

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Events.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}