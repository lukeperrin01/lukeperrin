﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Text.RegularExpressions;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Events
{
    public class Cancellation : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public Cancellation(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                ChargeInformation charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var excess = (decimal)250;
                var premium = chargeElements.FirstOrDefault(x => x.RateTypeFK == 10);
                var excessLine = chargeElements.FirstOrDefault(x => x.RateTypeFK == 7);
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }

                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;
                    decimal suminsured = 0;
                    if (r.RateTypeFK == 10)
                    {
                        if (r.Threshold > 0)
                        {
                            itemRate = r.Rate;
                            suminsured = r.Threshold;
                        }
                        else
                        {
                            suminsured = decimal.Parse(Regex.Replace(r.RateValue, "[^0-9.]", ""));
                            itemRate = (suminsured * r.Rate) / r.DivBy; ;
                        }
                    }

                    if (r.RateTypeFK == 7)
                    {
                        excess = excessLine.Threshold;
                    }

                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                        charge.SumInsured += suminsured;
                    }
                }

                charge.Excess = excess;
                charge.Territory = territory;
                charge.Indemnity = 0;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Events.Cancellation] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }
    }
}
