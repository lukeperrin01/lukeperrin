using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Text.RegularExpressions;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Events
{
    public class BusinessEquipment : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public BusinessEquipment(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                var excess = (decimal)250;
                var premium = chargeElements.FirstOrDefault(x => x.RateTypeFK == 10);
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                if (premium != null)
                {
                    decimal itemRate = 0;
                    if (premium.Threshold > 0)
                    {
                        itemRate = premium.Rate;
                        charge.SumInsured = premium.Threshold;
                    }
                    else
                    {
                        charge.SumInsured += decimal.Parse(Regex.Replace(premium.RateValue, "[^0-9.]", ""));
                        itemRate = (charge.SumInsured * premium.Rate) / premium.DivBy;


                    }
                    if (premium.Excess > excess)
                    {
                        excess = premium.Excess;
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(premium.QuoteInputPK, itemRate * _localeMultiplier);

                        charge.Premium += itemRate;
                    }

                }

                charge.Excess = excess;
                charge.Indemnity = 0;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Events.BusinessEquipment] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}