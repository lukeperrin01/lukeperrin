﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Text.RegularExpressions;
using System;
using System.Diagnostics;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Events
{
    public class PublicLiability : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                int noItems = 1;
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }

                ChargeElements endDateRate = (from i in chargeElements
                                              where i.RateTypeFK == 3
                                              select i).FirstOrDefault();

                if (endDateRate != null)
                {
                    ChargeElements startDateRate = (from i in chargeElements
                                                    where i.RateTypeFK == 2
                                                    select i).FirstOrDefault();

                    DateTime startDate = DateTime.Parse(startDateRate.RateValue);
                    DateTime endDate = DateTime.Parse(endDateRate.RateValue);
                    noItems = (int)(endDate - startDate).TotalDays + 1;
                }


                ChargeElements eventType = (from i in chargeElements
                                            where i.ListRatePK == -1245
                                            select i).FirstOrDefault();


                var indemnity = chargeElements.Where(x => x.RateTypeFK == 5).FirstOrDefault();
                if(indemnity != null)
                {
                    if (indemnity.ListRatePK == -1309)
                    {
                        noItems = indemnity.NoItems;
                    }
                }

                decimal excess = 250;


                foreach (ChargeElements r in chargeElements.Where(x => x.RateTypeFK == 5))
                {

                    decimal itemRate = 0;
                    //if (r.NoItems > noItems)
                    //{
                    //    noItems = r.NoItems;
                    //}
                    //if (indemnity.ListRatePK == -1309)
                    //{
                    //    noItems = 1;
                    //}


                    charge.SumInsured += decimal.Parse(Regex.Replace(r.RateName, "[^0-9.]", ""));
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    itemRate = (decimal)r.Rate * noItems;
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }
                }
                charge.SumInsured = 0;
                charge.Territory = territory;
                if (indemnity != null)
                {
                    charge.Indemnity = int.Parse(Regex.Replace(indemnity.RateValue, "[^0-9.]", ""));
                }
                charge.Excess = excess;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Events.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }
    }
}
