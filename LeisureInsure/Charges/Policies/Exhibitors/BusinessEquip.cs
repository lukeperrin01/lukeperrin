﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Exhibitors
{
    public class BusinessEquip : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public BusinessEquip(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public virtual ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }


                decimal excess = 250;

                var periodofCover = "";

                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;

                    if (r.RateTypeFK == 17)
                    {
                        periodofCover = r.RateName;
                    }
                    charge.SumInsured += (decimal)r.Threshold * policyInformation.ExchangeRate;
                    if (r.Excess > excess)
                    {
                        excess = (decimal)r.Excess;
                    }

                    if (r.DivBy == 100)
                    {
                        itemRate = (decimal)(charge.SumInsured * r.Rate) / (int)r.DivBy;
                    }
                    else if (r.DivBy == 1)
                    {
                        itemRate = (decimal)r.Rate;
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }

                    //charge.SumInsured += r.SumInsured;
                }


                charge.Excess = excess;
                charge.Indemnity = 0;
                charge.PeriodOfCover = periodofCover;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Exhibitors.BusEquip] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }
    }
}
