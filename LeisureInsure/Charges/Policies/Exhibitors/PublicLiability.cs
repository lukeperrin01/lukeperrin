﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Exhibitors
{
    public class PublicLiability : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                decimal indemnity = 0;
                decimal excess = 250;
                string periodOfCover = "1 day";
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }


                foreach (ChargeElements r in chargeElements.Where(x => x.DivBy == 1))
                {
                    int ratePK = r.RatePK;

                    if(ratePK != 0)
                    {
                        var rate = _repository.getRatebyPK(ratePK);
                        if(rate != null && rate.TableParentRatePK == 1091)
                        _repository.AddYear(policyInformation.quoteReference);
                        periodOfCover = "12 Months";
                    }
                    decimal itemRate = 0;

                    if (r.Indemnity > 0)
                    {
                        indemnity = (decimal)r.Indemnity;
                    }
                    if (r.DivBy == 1)
                    {

                        var noItems = 1;
                        if (r.NoItems > 1)
                        {
                            noItems = r.NoItems;

                        }
                        itemRate = (decimal)(r.Rate * noItems);

                        if (r.Excess > excess)
                        {
                            excess = r.Excess;
                        }
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }

                }

                charge.PeriodOfCover = periodOfCover;
                charge.Indemnity = indemnity;
                charge.Excess = excess;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Exhibitors.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }
    }
}
