﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Conference
{
    public class EmployerLiability : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public EmployerLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                var indemn = (int)10000000 * (int)policyInformation.ExchangeRate;
                var excess = (decimal)0;
                ChargeInformation charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);

                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemcharge = (decimal)r.Rate;
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }

                    if (itemcharge != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemcharge * _localeMultiplier);
                        charge.Premium += itemcharge;
                    }
                    charge.Premium += (decimal)r.Rate;
                    //charge.SumInsured += (decimal)r.Threshold;

                }
                charge.Excess = 0;
                charge.Indemnity = indemn;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Conference.EmployerLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }


    }
}
