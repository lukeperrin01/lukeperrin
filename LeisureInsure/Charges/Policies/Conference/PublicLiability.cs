﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Conference
{
    public class PublicLiability : ICalculateCoverCharges
    {
        private readonly ICalculateLoi _loi;
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;

        public PublicLiability(ICalculateLoi loi, IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _loi = loi;
            _repository = repository;
            _locale = locale;
        }


        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                var excess = (decimal)250;
                var indemn = (int)0;
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }
                foreach (ChargeElements r in chargeElements.Where(x => x.Rate > 0))
                {

                    decimal itemRate = 0;
                    itemRate = (decimal)r.Rate;


                    //charge.SumInsured += (decimal)r.Threshold;
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    if (r.RateTypeFK == 10 && r.Indemnity > indemn)
                    {
                        indemn = (int)r.Indemnity;

                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }

                }
                charge.PeriodOfCover = "1 day";
                charge.Excess = excess;
                charge.Indemnity = indemn;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Conference.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
