﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.FreelanceCatering
{
    public class PublicLiability : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();

                charge.Excess = 250;
                charge.PeriodOfCover = "12 Months";

                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                foreach (ChargeElements c in chargeElements)
                {
                    decimal itemRate = 0;
                    if (c.RateTypeFK == 10)
                    {
                        if (c.Indemnity != null)
                        {
                            charge.Indemnity = (decimal)c.Indemnity * policyInformation.ExchangeRate;
                        }
                        itemRate = c.Rate;
                    }
                    if (c.RateTypeFK == 23 && c.DivBy == 100)
                    {
                        itemRate = (charge.Excess / c.DivBy) * c.Rate;
                    }
                    if (itemRate != 0)
                    {
                        //_repository.AddInputCharge(c.QuoteInputPK, itemRate * _localeMultiplier);
                        _repository.AddInputCharge(c.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }

                }

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.FreelanceCatering.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }


        }
    }
}
