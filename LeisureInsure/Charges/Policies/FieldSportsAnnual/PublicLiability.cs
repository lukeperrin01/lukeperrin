﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Text.RegularExpressions;
using System;
using System.Diagnostics;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.FieldSportsAnnual
{
    public class PublicLiability : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }


        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {

                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);

                var charge = new ChargeInformation();
                IList<ChargeElements> loadings = new List<ChargeElements>();
                int noItems;
                var excess = (decimal)250;
                decimal indemn = 0;
                string periodofcover = "";
                charge.Territory = "United Kingdom";

                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                foreach (ChargeElements r in chargeElements)
                {
                    if (r.ListRatePK == -1607 || r.ListRatePK == -1245 || r.ListRatePK == -1635 || r.ListRatePK == -1651)
                    {
                        periodofcover = r.RateValue;
                    }

                    if (r.Indemnity > indemn)
                    {
                        indemn = (decimal)r.Indemnity;
                    }

                    decimal itemRate = 0;
                    if (r.RateTypeFK == 10)
                    {
                        loadings.Add(r);
                    }
                    noItems = 1;
                    if (r.SumInsured == 0)
                    {
                        if (r.NoItems > noItems)
                        {
                            noItems = r.NoItems;
                        }
                        itemRate = Math.Max((decimal)(r.Rate * noItems), (decimal)r.MinimumRate);
                        var si = Regex.Replace(r.RateName, "[^0-9.]", "");
                    }
                    else
                    {
                        itemRate = Math.Max((decimal)((r.Rate * r.SumInsured) / (int)r.DivBy) , (decimal)r.MinimumRate);
                        charge.SumInsured += r.SumInsured;

                    }
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }

                }
                if (string.IsNullOrEmpty(periodofcover))
                {
                    periodofcover = "12 Months";
                }
                charge.PeriodOfCover = periodofcover;
                charge.Indemnity = indemn * policyInformation.ExchangeRate;
                charge.Excess = excess;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.FieldSportsAnnual.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }
    }
}
