﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Text.RegularExpressions;
using System;
using System.Diagnostics;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.FieldSportsAnnual
{
    class MaterialDamage : ICalculateCoverCharges
    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamage(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                decimal sumInsured = 0;
                decimal excess = 0;
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }
                var periodofCover = "12 Months";

                foreach (ChargeElements element in chargeElements)
                {

                    decimal itemRate = 0;
                    if (element.RateTypeFK == 17)
                    {
                        periodofCover = element.RateName;

                    }
                    if (element.SumInsured != 0)
                    {
                        sumInsured = element.SumInsured;
                    }
                    else
                    {
                        sumInsured = int.Parse(Regex.Replace(element.RateValue, "[^0-9.]", ""));
                    }
                    if (element.Excess > excess)
                    {
                        excess = element.Excess;
                        if (!policyInformation.MDExcessAdded)
                            charge.Excess = excess;
                        policyInformation.MDExcessAdded = true;
                    }

                    if (element.DivBy != 0)
                    {
                        itemRate = Math.Max((decimal)(element.Rate * sumInsured) / (int)element.DivBy, (decimal)element.MinimumRate);

                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(element.QuoteInputPK, itemRate * _localeMultiplier);
                        charge.Premium += itemRate;
                    }

                    charge.SumInsured += sumInsured;
                }
                charge.PeriodOfCover = periodofCover;
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.FieldSportsAnnual.MaterialDamage] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }


        }
    }
}
