﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Showmen
{
    public class MaterialDamageBusinessEquipment : ICalculateCoverCharges

    {
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamageBusinessEquipment(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public virtual ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();
                decimal excess = (decimal)250;
                decimal minRate = 0;
                decimal calculatedRate = 0;
                charge.PeriodOfCover = "12 Months";
                charge.Territory = "United Kingdom";

                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                foreach (ChargeElements r in chargeElements.Where(x => x.DivBy > 0))
                {
                    decimal itemRate = 0;

                    itemRate = ((decimal)r.Rate * r.SumInsured) / (int)r.DivBy;
                    if (r.MinimumRate > minRate)
                    {
                        minRate = (decimal)r.MinimumRate;
                    }
                    charge.SumInsured += r.SumInsured;
                    if (r.Excess > excess)
                    {
                        excess = r.Excess;
                    }
                    if (itemRate != 0)
                    {
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                        r.chargeAdded = true;
                        calculatedRate += itemRate;
                    }
                }
                int i = 0;
                if (calculatedRate < minRate)
                {
                    foreach (ChargeElements r in chargeElements.Where(x => x.chargeAdded == true).OrderByDescending(x => x.Rate))
                    {
                        if (i == 0)
                        {
                            _repository.AddInputCharge(r.QuoteInputPK, minRate * _localeMultiplier);
                            i++;
                        }
                        else
                        {
                            _repository.AddInputCharge(r.QuoteInputPK, 0);
                        }

                    }
                    calculatedRate = minRate;
                }

                charge.Premium = calculatedRate;
                charge.Excess = excess;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Showmen.MaterialDamageBusinessEquipment] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }
    }
}