﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Charges.Utilities;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.Showmen
{
    public class PublicLiability : ICalculateCoverCharges
    {
        private readonly ICalculateLoiLoading _loiLoading;
        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(ICalculateLoiLoading loiLoading, IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _loiLoading = loiLoading;
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }


                // This will contain each of the charges
                var charges = new List<ChargeItem>();
                var charge = new ChargeInformation();
                List<ChargeElements> loadings = new List<ChargeElements>();

                decimal excess = 250;

                foreach (ChargeElements r in chargeElements)
                {
                    decimal itemRate = 0;
                    if (r.PageNo == 1 && r.RateTypeFK == 10 && r.Rate != 0)
                    {
                        itemRate = r.Rate;
                    }
                    if (r.RateTypeFK == 5)
                    {
                        charge.Indemnity = (decimal)r.Indemnity;
                        loadings.Add(r);
                    }
                    if (itemRate != 0)
                    {
                        charge.Premium += itemRate;
                        _repository.AddInputCharge(r.QuoteInputPK, itemRate * _localeMultiplier);
                    }

                }

                var LoadedTotalCharge = charge.Premium;
                foreach (ChargeElements c in loadings)
                {
                    decimal loading = 0;
                    if (c.RateTypeFK == 5) // Indemnity loading
                    {

                        loading = charge.Premium * (decimal)(c.Rate / (int)c.DivBy);

                    }
                    if (loading != 0)
                    {
                        _repository.AddInputCharge(c.QuoteInputPK, loading * _localeMultiplier);
                        LoadedTotalCharge += loading;
                    }
                }

                charge.Premium = LoadedTotalCharge;
                charge.Excess = excess;
                charge.SumInsured = 0;
                charge.Territory = territory;
                charge.PeriodOfCover = "12 Months";
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.Showmen.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}