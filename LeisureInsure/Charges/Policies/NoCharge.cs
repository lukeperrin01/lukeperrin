﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Charges.Policies
{
    public class NoCharge : ICalculateCoverCharges
    {
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            return new ChargeInformation
            {
                ExcludeFromSummary = true,
            };
        }
    }
}