﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.BrandProtection
{
    public class FreeCover : ICalculateCoverCharges
    {
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                var charge = new ChargeInformation();
                //var excess = (decimal)250;
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }
                var periodofCover = "12 Months";

                var limitOfCover = chargeElements.Where(x => x.ListRatePK == -4231).FirstOrDefault();

                foreach (ChargeElements r in chargeElements)
                {
                    if (r.DivBy == 1)
                    {

                        charge.Indemnity += (decimal)r.Rate;

                    }
                    else if (r.DivBy == 100) //percentage
                    {
                        //get free cover cost which is % of out limit of cover
                        var limit = Convert.ToDecimal(limitOfCover.Rate);
                        var percIncrease = (decimal)r.Rate / 100;
                        charge.Indemnity += percIncrease * limit;
                        break;

                    }

                }
                charge.PeriodOfCover = periodofCover;
                charge.Excess = 0;

                charge.Territory = territory;
                //charge.Indemnity = (int)charge.SumInsured;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.BrandProtection.FreeCovers] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
