﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.BrandProtection
{
    public class AnnualLoss : ICalculateCoverCharges
    {
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                var charge = new ChargeInformation();
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }
                var periodofCover = "12 Months";

                var minPrem = new List<List<decimal>>();
                ChargeElements noOfSites = chargeElements.Where(x => x.RatePK == 4235).FirstOrDefault();
                ChargeElements limitOfCover = chargeElements.Where(x => x.ListRatePK == -4231).FirstOrDefault();
                int coverLimit = Convert.ToInt32(limitOfCover.Rate);
                int numOfSites = Convert.ToInt32(noOfSites.RateValue);

                if (numOfSites <= 10)
                {

                    var minPremiums = chargeElements.Where(x => x.RateTypeFK == 13).ToList();

                    foreach (var item in minPremiums)
                    {
                        var items = item.LongRate.Split(',');
                        var prems = Array.ConvertAll(items, decimal.Parse).ToList();
                        prems.Insert(0, (decimal)item.Rate);
                        minPrem.Add(prems);
                    }

                    int length = minPrem.Count();
                    decimal compare = 0, limit = 0;
                    decimal minCover = 0, min_b = 0, limit_b = 0;

                    for (int i = 0; i < length; i++)
                    {
                        compare = limit;
                        limit = minPrem[i][0];
                        var diff = limit - compare;
                        if (coverLimit <= limit)
                        {
                            decimal min_a = minPrem[i][numOfSites];
                            decimal limit_a = minPrem[i][0];
                            if (i > 0)
                            {
                                min_b = minPrem[i - 1][numOfSites];
                                limit_b = minPrem[i - 1][0];
                                minCover = (coverLimit - limit_b) / (limit_a - limit_b) * (min_a - min_b) + min_b;
                            }
                            else
                                minCover = min_a;

                            break;
                        }
                    }

                    minCover = minCover * numOfSites;

                    if (minCover > 0)
                    {
                        charge.Premium += (decimal)minCover;
                        charge.Indemnity += (decimal)coverLimit;
                    }


                    foreach (ChargeElements r in chargeElements)
                    {
                        if (r.DivBy == 1)
                        {

                            charge.Premium += (decimal)r.Rate;
                            //we want to include all rates but exclude turnover
                            if (r.RateName != "Turnover")
                            {
                                charge.Indemnity += (int)r.Threshold;
                            }
                            else
                            {
                                charge.SumInsured += (decimal)r.Threshold;

                            }
                            //indemnity perdiod
                            if (r.RateTypeFK == 17)
                            {
                                periodofCover = r.RateName;

                            }
                        }
                        else if (r.DivBy == 100)
                        {
                            var percIncrease = (decimal)r.Rate / 100;
                            charge.Premium += percIncrease * charge.Premium;
                        }

                    }
                }
                else
                    charge.Refer = true;

                charge.PeriodOfCover = periodofCover;
                charge.Excess = 0;

                charge.Territory = territory;
                //charge.Indemnity = (int)charge.SumInsured;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.BrandProtection.AnnualLoss] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
