﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.OtherActivities
{
    public class Material_DamageRentPayable : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public Material_DamageRentPayable(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                ChargeInformation charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }

                foreach (ChargeElements c in chargeElements)
                {
                    if (c.RateTypeFK == 10)
                    {
                        decimal Ratecharge = 0;
                        decimal sumInsured = 0;
                        sumInsured = decimal.Parse(Regex.Replace(c.RateValue, "[^0-9.]", ""));
                        Ratecharge = sumInsured * c.Rate / c.DivBy;
                        if (Ratecharge != 0)
                        {
                            charge.Excess = c.Excess;
                            charge.SumInsured += sumInsured;
                            charge.Premium += Ratecharge;
                            _repository.AddInputCharge(c.QuoteInputPK, Ratecharge * _localeMultiplier);
                        }
                    }
                }
                var IndemnPeriodRate = chargeElements.Where(x => x.RateTypeFK == 17).FirstOrDefault();
                charge.PeriodOfCover = "12 Months";
                charge.Indemnity = 0;

                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.OtherActivities.MaterialDamageRentPayable] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
