﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System.Linq;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.OtherActivities
{
    public class MaterialDamageBusinessEquipment : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamageBusinessEquipment(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public virtual ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                var charge = new ChargeInformation();
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                decimal excess = 0;
                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }
                if (!policyInformation.MDExcessAdded)
                {
                    excess = 250;
                    policyInformation.MDExcessAdded = true;
                }

                foreach (ChargeElements r in chargeElements.Where(x => x.DivBy > 0))
                {
                    decimal Ratecharge = 0;
                    Ratecharge = (r.Rate * r.SumInsured) / r.DivBy;

                    if (Ratecharge != 0)
                    {
                        charge.Premium += (r.Rate * r.SumInsured) / r.DivBy;
                        _repository.AddInputCharge(r.QuoteInputPK, Ratecharge * _localeMultiplier);
                        r.chargeAdded = true;
                    }
                    charge.SumInsured += r.SumInsured;

                }
                int i = 0;
                if (charge.Premium < 50)
                {
                    charge.Premium = 50;
                    foreach (ChargeElements r in chargeElements.Where(x => x.chargeAdded == true).OrderByDescending(x => x.Rate))
                    {
                        if (i == 0)
                        {
                            _repository.AddInputCharge(r.QuoteInputPK, charge.Premium * _localeMultiplier);
                            i++;
                        }
                        else
                        {
                            _repository.AddInputCharge(r.QuoteInputPK, 0);
                        }
                    }
                }

                charge.Excess = excess;
                charge.Indemnity = 0;
                charge.PeriodOfCover = "12 Months";
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.OtherActivities.MaterialDamageBusinessEquipment] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }


        }
    }
}