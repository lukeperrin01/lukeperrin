﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.OtherActivities
{
    
    public class MaterialDamageBuildings : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public MaterialDamageBuildings(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }

        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var charge = new ChargeInformation();

                charge.Territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    charge.Territory = "Republic of Ireland";
                }
                charge.PeriodOfCover = "12 Months";

                foreach (ChargeElements r in chargeElements)
                {
                    decimal Ratecharge = 0;
                    charge.SumInsured += int.Parse(r.RateValue);
                    Ratecharge += (r.Rate * int.Parse(r.RateValue)) / r.DivBy;

                    if (!policyInformation.MDExcessAdded)
                    {
                        charge.Excess = 250;
                        policyInformation.MDExcessAdded = true;
                    }
                    if (Ratecharge != 0)
                    {
                        charge.Premium += Ratecharge;
                        _repository.AddInputCharge(r.QuoteInputPK, Ratecharge * _localeMultiplier);
                    }
                }


                return charge;

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.OtherActivities.MaterialDamageBuildings] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }
        }
    }
}
