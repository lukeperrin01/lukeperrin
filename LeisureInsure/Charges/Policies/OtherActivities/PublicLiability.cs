﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.DB;
using LocaleInfo = LeisureInsure.Services.Services.Locales;
using System.Diagnostics;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Charges.Policies.OtherActivities
{
    public class PublicLiability : ICalculateCoverCharges
    {

        private readonly IRepository _repository;
        private readonly LocaleInfo.IGetLocaleInformation _locale;
        public PublicLiability(IRepository repository, LocaleInfo.IGetLocaleInformation locale)
        {
            _repository = repository;
            _locale = locale;

        }


        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            try {
                decimal _localeMultiplier = _locale.Multiplier(policyInformation.LocaleId, policyInformation.PolicyId, chargeElements.FirstOrDefault().CoverFK);
                var territory = "United Kingdom";
                if (policyInformation.LocaleId == 2)
                {
                    territory = "Republic of Ireland";
                }
                // Percentage loading to be added after net premium is calculated
                var loadings = new List<ChargeElements>();


                var indemnity = chargeElements.Where(x => x.RateTypeFK == 5).FirstOrDefault();
                decimal excess = 250;
                var sumInsured = 0m;

                var charge = new ChargeInformation();

                foreach (ChargeElements r in chargeElements.Where(x => x.DivBy > 0))
                {
                    decimal Ratecharge = 0;
                    if (r.RateTypeFK == 5)
                    {
                        charge.Indemnity = (decimal)r.Indemnity * policyInformation.ExchangeRate;
                        loadings.Add(r);
                    }
                    else
                    {
                        sumInsured += r.SumInsured;
                        Ratecharge = Math.Max((decimal)(r.Rate * r.SumInsured) / (int)r.DivBy, (int)r.MinimumRate);

                        if (r.Excess > excess)
                        {
                            excess = r.Excess;
                        }
                    }

                    if (Ratecharge != 0)
                    {
                        charge.Premium += Ratecharge;
                        _repository.AddInputCharge(r.QuoteInputPK, Ratecharge * _localeMultiplier);

                    }
                }
                decimal LoadedTotalCharge = 0;
                foreach (ChargeElements r in loadings)
                {
                    LoadedTotalCharge = (charge.Premium * r.Rate) / 100;

                    if (LoadedTotalCharge != 0)
                    {
                        charge.Premium += LoadedTotalCharge;
                        _repository.AddInputCharge(r.QuoteInputPK, LoadedTotalCharge * _localeMultiplier);
                    }
                }
                charge.SumInsured = sumInsured;
                charge.Excess = excess;
                charge.Territory = territory;
                charge.PeriodOfCover = "12 Months";
                return charge;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.Policies.OtherActivities.PublicLiability] url:post:api/v1/charges", policyInformation.quoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }
    }
}