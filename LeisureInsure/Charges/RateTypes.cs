﻿namespace LeisureInsure.Charges
{
    public enum RateTypes
    {
        Standard = 0,
        EmployersLiabilityBand = 13,//minimum charge
        LeisureInsureFee = 19,
        AgentCommision = 31,
    }
}