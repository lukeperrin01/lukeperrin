﻿using System;
using System.Linq;
using LeisureInsure.DB;
using LeisureInsure.Services.Dal.LeisureInsure.Information;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Services.Utilities;

namespace LeisureInsure.Charges.Rates
{
    public class EmployersLiabilityMinimumPremiumFinder : IFindEmployersLiabilityMinimumPremium
    {
        private readonly IRepository _repository;
        private readonly IClock _clock;

        public EmployersLiabilityMinimumPremiumFinder(
            IRepository repository, 
            IClock clock)
        {
            _repository = repository;
            _clock = clock;
        }

        public decimal? Find(int policyId, int coverId, int localeId, decimal wageroll, DateTime? date = null)
        {           
            var policy = _repository.GetPolicy(policyId);
            var cover = _repository.GetCover(coverId);            
           
            var baseQuery = _repository.GetRateByBitWise(cover.BitWise,policy.BitWise, 13, localeId);            
                              

            if (date == null)
            {
                date = _clock.UtcNow;
            }

            var now = _clock.UtcNow.AddMinutes(1);

            var rates = baseQuery
                // Filter
                .Where(x => x.DiscountFirstRate <= wageroll && x.DiscountSubsequentRate >= wageroll)
                .ToList() // to transform...
                .Select(x => new EmployersLiabilityBand
                {
                    DateTill = x.ValidUntil ?? now,
                    Rate = x.Rate,
                    Id = x.RatePK,
                    WageRollFrom = x.DiscountFirstRate,
                    WageRollTo = x.DiscountSubsequentRate
                });
            var rate = rates
                .Where(x => x.DateTill > date)
                .OrderByDescending(x => x.DateTill)
                // Get the maxiumum date less than the date entered
                .FirstOrDefault();

            if (rate == null)
            {
                throw new ApplicationException($"Please enter a band for employers liability for cover: {coverId}, policy: {policyId}, and amount: {wageroll}");
            }
            return rate.Rate;
        }
    }
}