﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Charges.Utilities
{
    public class LoiCalculator : ICalculateLoi
    {
        public decimal Calculate(List<ChargeCalculationAnswer> answers, PolicyInformation policyInformation, List<Rate> rates)
        {
            var loiAnswer = answers.FirstOrDefault(x => x.RateTypeFK == 5);
            if (loiAnswer != null)
            {
                var rate = rates.FirstOrDefault(x => x.Id == loiAnswer.RateId);
                if (rate != null)
                {
                    return rate.DiscountFirstRate;
                }
            }
            return 0m;
        }
    }
}
