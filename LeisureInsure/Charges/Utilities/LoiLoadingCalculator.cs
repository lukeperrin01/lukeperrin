﻿using System.Collections.Generic;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Charges.Utilities
{
    public class LoiLoadingCalculator : ICalculateLoiLoading
    {
        public ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements)
        {
            // Retrieve the limit of indemnity loading rate
            var indemnity = chargeElements.Where(x => x.RateTypeFK == 5).FirstOrDefault();
            var loiLoading = 0m;
            if (indemnity != null)
            {
                loiLoading = (decimal)indemnity.Rate;
            }


            return new ChargeInformation
            {
                Premium = loiLoading,

            };
        }
    }
}