﻿using System;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Charges.Utilities
{
    public static class RateExtensions
    {
        public static decimal PremiumFor(this Rate rate, decimal amount = 0)
        {
            if (rate.DivBy == 100)
            {
                return amount * (rate.Value/100);
            }
            return rate.Value;
        }

        public static decimal PremiumWithMinimum(this Rate rate, decimal amount = 0)
        {
            decimal item;
            if (rate.DivBy == 100)
            {
                item = amount * (rate.Value/100);
            }
            else
            {
                item = rate.Value;
            }

            return Math.Max(item, rate.MinimumRate??0);
        }
    }
}