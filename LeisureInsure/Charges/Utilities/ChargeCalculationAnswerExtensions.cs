﻿using System.Linq;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Services.Utilities;

namespace LeisureInsure.Charges.Utilities
{
    public static class ChargeCalculationAnswerExtensions
    {
        public static int NumberOfItems(this ChargeCalculationAnswer answer)
        {
            var numberOfItemsChild = answer.Children.FirstOrDefault(x => x.NoItems > 0);
            if (numberOfItemsChild == null)
            {
                return 0;
            }
            return numberOfItemsChild.RateValue.ToSafeInt();
        }

        public static decimal MaterialDamageValue(this ChargeCalculationAnswer answer)
        {
            var materialDamageChild = answer.Children.FirstOrDefault(x =>
                x.SumInsured > 0);

            if (materialDamageChild == null)
            {
                return 0;
            }
            return materialDamageChild.RateValue.ToSafeDecimal();
        }
    }
}