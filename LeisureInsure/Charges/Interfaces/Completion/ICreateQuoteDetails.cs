﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Charges.Completion
{
    public interface ICreateQuoteDetails
    {
        void Create(Quote quote);
    }
}
