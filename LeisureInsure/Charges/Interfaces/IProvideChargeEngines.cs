﻿namespace LeisureInsure.Charges
{
    public interface IProvideChargeEngines
    {
        ICalculateCoverCharges CoverChargeCalculator(int policyId, int coverId);
    }
}
