﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Charges
{
    public interface ICalculateCoverCharges
    {
        ChargeInformation Calculate(PolicyInformation policyInformation, List<ChargeElements> chargeElements);                
    }
}
