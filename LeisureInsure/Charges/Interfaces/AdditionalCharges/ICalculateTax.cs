﻿using System;

namespace LeisureInsure.Charges.AdditionalCharges
{
    public interface ICalculateTax
    {
        decimal Calculate(decimal premiumAmount, int localeId, DateTime? date = null, string quoteRef = "");
        decimal Rate(int localeId, DateTime? date = null, string quoteRef = "");
    }
}
