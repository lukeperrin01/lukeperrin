﻿namespace LeisureInsure.Charges.AdditionalCharges
{
    public interface ICalculateCardCharges
    {
        decimal AddChargeTo(decimal charge, int localeId, int paymentCardId);
    }
}
