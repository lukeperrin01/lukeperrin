﻿using System;
namespace LeisureInsure.Charges.AdditionalCharges
{
    public interface ICalculateLegalCareFee
    {
        decimal Calculate(decimal totalPremiumWithoutTax, int policyId, int localeId, int quotePK ,DateTime? dateEffective = null);
    }
}
