﻿using System;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Charges.AdditionalCharges
{
    public interface ICalculateLeisureInsureFees
    {
       // decimal Calculate(decimal premium, int policyId, int localeId, int coverid);

        /// <summary>
        /// Return the rate for the policy and the locale at <paramref name="date"/>
        /// </summary>
        Rate Rate(int policyId, int localeId, DateTime? date = null,string quoteRef = "");

        void Calculate(Quote quote);
    }
}
