﻿using System;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Dal.LeisureInsure.Information;

namespace LeisureInsure.Charges.AdditionalCharges
{
    public interface ICalculateAgentCommision
    {
        decimal Rate(int policyId, int localeId, DateTime? date = null);
    }
}
