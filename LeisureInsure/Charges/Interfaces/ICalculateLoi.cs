using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Charges
{
    public interface ICalculateLoi
    {
        decimal Calculate(List<ChargeCalculationAnswer> answers, PolicyInformation policyInformation, List<Rate> rates);
    }
}