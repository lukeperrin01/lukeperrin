﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using System.Collections.Generic;
using System.Linq;
namespace LeisureInsure.Charges
{
    public interface ICalculateAllCoverCharges
    {
        Quote Calculate(ChargeCalculationInputs model, IList<ChargeElements> chargeElements);
      
    }
}
