﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Charges.Discounts
{
    public interface ICalculateDiscounts
    {
        Quote Calculate(Quote summary, List<ChargeCalculationAnswer> answers, int policyId);
    }
}
