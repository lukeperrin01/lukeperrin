﻿namespace LeisureInsure.Charges.Discounts
{
    public interface IProvideDiscountEngines
    {
        ICalculateDiscounts EngineFor(DicountType discountType);
    }
}
