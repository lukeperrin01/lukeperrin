﻿using System;

namespace LeisureInsure.Charges
{
    public interface IFindEmployersLiabilityMinimumPremium
    {
        decimal? Find(int policyId, int coverId, int localeId, decimal wageroll, DateTime? date = null);
    }
}
