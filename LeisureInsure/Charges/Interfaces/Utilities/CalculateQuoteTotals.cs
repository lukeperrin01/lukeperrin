﻿using System.Linq;
using System;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Charges.AdditionalCharges;
using LeisureInsure.DB;

namespace LeisureInsure.Charges.Utilities
{
    class CalculateQuoteTotals : ICalculateQuoteTotals
    {
        private readonly ICalculateLegalCareFee _legal;
        private readonly ICalculateLeisureInsureFees _leisureInsureFee;
        private readonly ICalculateAgentCommision _commision;

        public CalculateQuoteTotals(ICalculateLegalCareFee legal,
            ICalculateLeisureInsureFees leisureInsureFee,
            ICalculateAgentCommision commision)
        {
            _legal = legal;
            _leisureInsureFee = leisureInsureFee;
            _commision = commision;
        }

        public void Calculate(Quote quote, decimal taxRate)
        {


            var netPremium = quote.ChargeSummaryItems.Sum(x => x.Net) - quote.Discounts.Sum(x => x.Net);

            //do we have a discount?
            if(!string.IsNullOrEmpty(quote.CodeName))
            {
                var rep = new Repository();
                var code = rep.GetCode(quote.CodeName);
                if(code.CodeType == "DISC")
                {
                    var multiplier = code.Discount / 100;
                    var newPremium = netPremium - (multiplier * netPremium);
                    netPremium = (decimal)newPremium;
                }
            }


            var legal = _legal.Calculate(netPremium, quote.PolicyId, quote.LocaleId,quote.Id, quote.DateFrom);
            quote.LegalFees = legal;

            //Apply taxes

            //work out tax on net price               
            foreach (var item in quote.ChargeSummaryItems)
            {
                if (item.Net > 0)
                {
                    var tax = item.Net * taxRate;
                    item.Tax = tax;
                    //preserve the net to later calculate the fee
                    item.Price = item.Net + tax;
                }
            }
            quote.Tax = netPremium * taxRate;
            quote.TaxRate = taxRate;
            
            if (quote.IsAgent)
            {
                // Calculate commision including discounts
                var commision = _commision.Rate(quote.PolicyId, quote.LocaleId);

                var rate = new Rate();
                rate.PremiumWithMinimum();
                
                quote.CommissionPercentage = commision;
                quote.Commission = (commision / 100) * netPremium;                
            }

            //Apply fees...

            quote.NetPremium = netPremium;

            _leisureInsureFee.Calculate(quote);           

            quote.Total = netPremium + quote.Tax + quote.LeisureInsureFees;
            quote.Total = Math.Round(quote.Total,2);

            if (quote.LegalFeesAdded)
            {
                quote.Total += quote.LegalFees;
            }
        }
    }
}
