﻿namespace LeisureInsure.Charges.Utilities
{
    public interface IGetCoverBitwiseIds
    {
        int BitwiseId(int coverId);
    }
}
