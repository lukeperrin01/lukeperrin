﻿namespace LeisureInsure.Charges.Utilities
{
    interface IGetPolicyBitwiseIds
    {
        int BitwiseId(int policyId);
    }
}
