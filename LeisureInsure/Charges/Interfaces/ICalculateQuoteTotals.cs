﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Charges.AdditionalCharges;

namespace LeisureInsure.Charges
{
    public interface ICalculateQuoteTotals
    {
        void Calculate(Quote quote, decimal taxrate = 0);
    }
}
