﻿using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Charges.Completion
{
    public class QuoteDetailsCreator : ICreateQuoteDetails
    {
        private readonly ILiRepository _repository;
        
        public QuoteDetailsCreator(ILiRepository repository)
        {
            _repository = repository;
        }

        public void Create(Quote quote)
        {
            var rateIds = quote.Answers.Select(x => x.RatePk).Distinct().ToList();

            var rates = _repository.Q<Rate>().Where(x => rateIds.Contains(x.Id));

            foreach (var answer in quote.Answers)
            {
                var rate = rates.FirstOrDefault(x => x.Id == answer.RatePk);
                var detail = new QuoteDetail
                {
                    CoverId = answer.CoverPk,
                    DateAdded = quote.DateCreated,
                    InputValue = answer.RateValue,
                    LineType = answer.InputString,
                    ListNumber = answer.ListNo,
                    ListOrdinal = answer.ListOrdinal,
                    RateId = answer.RatePk,
                    QuoteId = quote.Id,
                };
                if (rate != null)
                {
                    detail.Rate = rate.Value;
                }
                _repository.Add(detail);
            }
            _repository.Commit();
        }
    }
}

