﻿using System;
using LeisureInsure.Services.Models;
using LeisureInsure.Charges.Policies;
using Microsoft.Practices.Unity;

namespace LeisureInsure.Charges
{
    public class ChargeEngineFactory : IProvideChargeEngines
    {
        private readonly IUnityContainer _container;

        public ChargeEngineFactory(IUnityContainer container)
        {
            _container = container;
        }

        public ICalculateCoverCharges CoverChargeCalculator(int policyId, int coverId)
        {
            // Generic covers
            switch (coverId)
            {
                case 0:
                    return _container.Resolve<NoCharge>();
                case 1:
                    return _container.Resolve<NoCharge>();
                case CoverId.EmployersLiability: // Employers' Liability
                    {
                        switch (policyId)
                        {
                            case 1://Equipment hirers
                                return _container.Resolve<GenericCovers.EmployersLiability>();
                            case 16://Freelance Sports Instructor
                                return _container.Resolve<GenericCovers.EmployersLiability>();
                            case 3://Conference & Meetings
                                return _container.Resolve<Policies.Conference.EmployerLiability>();
                            case 6://Events
                                return _container.Resolve<Policies.Events.EmployerLiability>();
                            case 7://FieldSports Annual
                                return _container.Resolve<GenericCovers.EmployersLiability>();
                            case 10:// 10 - Weddings
                                return _container.Resolve<Policies.Wedding.EmployeeLiability>();
                            case 11:// 11 - bar mitzvah
                                return _container.Resolve<Policies.Wedding.EmployeeLiability>();
                            case 25:// Street Parties
                                return _container.Resolve<Policies.StreetParties.EmployerLiability>();
                            case 18:// 18 - exhibitors
                                return _container.Resolve<GenericCovers.FixedPriceOptions>();
                            case 26:// 26 - mobilecatering
                                return _container.Resolve<Policies.MobileCatering.GenericLiabilty>();

                            default:
                                return _container.Resolve<GenericCovers.EmployersLiability>();
                        }
                    }
                case CoverId.MaterialDamageBuildings:
                    switch (policyId)
                    {
                        case 6:
                            return _container.Resolve<GenericCovers.GenericListItemSelected>();
                        case 4:
                            return _container.Resolve<Policies.OtherActivities.MaterialDamageBuildings>();
                        default:
                            return _container.Resolve<GenericCovers.MaterialDamageBuildings>();
                    }

                case 5: //Ancillary Buildings
                    switch (policyId)
                    {
                        case 7:
                            //return _container.Resolve<GenericCovers.GenericFixedPrice>();
                            return _container.Resolve<Policies.FieldSportsAnnual.MaterialDamage>();
                        default:
                            return _container.Resolve<GenericCovers.MaterialDamageBuildings>();
                    }
                case 6:
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                case 7:
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                case CoverId.MaterialDamageBusinessEquipment: //8
                    switch (policyId)
                    {
                        case 1:
                            return _container.Resolve<Policies.EquipmentHirers.MaterialDamage>();
                        case 2:
                            return _container.Resolve<Policies.Showmen.MaterialDamageBusinessEquipment>();
                        case 3:
                            return _container.Resolve<Policies.Conference.MaterialDamageEquipment>();
                        case 4:
                            return _container.Resolve<Policies.OtherActivities.MaterialDamageBusinessEquipment>();
                        case 5:
                            return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                        case 6:
                            return _container.Resolve<Policies.Events.BusinessEquipment>();
                        case 7:
                            //return _container.Resolve<GenericCovers.GenericFixedPrice>();
                            return _container.Resolve<Policies.FieldSportsAnnual.MaterialDamage>();
                        case 10:

                        case 11:
                            return _container.Resolve<GenericCovers.FixedPriceOptions>();
                        case 15:
                            return _container.Resolve<Policies.FreelanceAdventureInstructors.MaterialDamageBusinessEquipment>();
                        case 18:
                            return _container.Resolve<Policies.Exhibitors.BusinessEquip>();
                        case 24:
                            return _container.Resolve<GenericCovers.GenericListItemSelected>();
                        case 25:
                            return _container.Resolve<GenericCovers.GenericListItemSelected>();
                        case 26:
                            return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                        default:
                            return _container.Resolve<GenericCovers.MaterialDamageBusinessEquipment>();
                    }
                case CoverId.MaterialDamageTrophiesAndMemorabilia:
                    return _container.Resolve<GenericCovers.MaterialDamageTrophiesAndMemorabilia>();
                case 10://Machinery And Plant
                    switch (policyId)
                    {
                        case 7:
                            //return _container.Resolve<GenericCovers.GenericFixedPrice>();
                            return _container.Resolve<Policies.FieldSportsAnnual.MaterialDamage>();
                        default:
                            return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                    }                    
                case 11: // Stock
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                case 12:
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                case 13: //business equipment
                    switch (policyId)
                    {
                        case 1:
                            return _container.Resolve<Policies.EquipmentHirers.MaterialDamage>();
                        default:
                            return _container.Resolve<GenericCovers.MaterialDamageBusinessEquipment>();
                    }
                case 14:
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();

                case 15:
                case 16:
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                case 17:
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                case 18:
                    switch (policyId)
                    {
                        case 1:
                            return _container.Resolve<Policies.EquipmentHirers.ProfessionalLiability>();
                        case 7:
                            return _container.Resolve<Policies.EquipmentHirers.ProfessionalLiability>();
                        case 26:
                            return _container.Resolve<Policies.MobileCatering.GenericLiabilty>();
                        default:
                            return _container.Resolve<GenericCovers.GenericFixedPrice>();
                    }
                case 20:
                    switch (policyId)
                    {
                        case 1:
                            return _container.Resolve<Policies.EquipmentHirers.ProfessionalLiability>();
                        case 7:
                            return _container.Resolve<Policies.EquipmentHirers.ProfessionalLiability>();
                        default:
                            return _container.Resolve<GenericCovers.GenericFixedPrice>();
                    }
                case 26://book debts
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();

                case CoverId.PersonalAssault: // 23
                    return _container.Resolve<GenericCovers.PersonalAssault>();
                case 25:
                    switch (policyId)
                    {
                        case 4:
                            return _container.Resolve<GenericCovers.BusinessInteruptionLossOfGrossRentals>();
                        default:
                            return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                    }
                case CoverId.BusinessInterruptionIncreasedCostOfWorking:
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                case CoverId.Cancellation: //30
                    switch (policyId)
                    {
                        case 3:
                            return _container.Resolve<Policies.Conference.Cancellation>();
                        case 6:
                            return _container.Resolve<Policies.Events.Cancellation>();
                        case 10:
                            return _container.Resolve<Policies.Wedding.Cancellation>();
                        case 11:
                            return _container.Resolve<Policies.Wedding.Cancellation>();
                        case 18:
                            return _container.Resolve<Policies.Exhibitors.Cancellation>();
                        default:
                            return _container.Resolve<GenericCovers.FixedPriceOptions>();
                    }
                case 33: //Liability to Ceremony Services Suppliers
                    switch (policyId)
                    {
                        case 10:
                            return _container.Resolve<Policies.Wedding.Cancellation>();
                        case 11:
                            return _container.Resolve<Policies.Wedding.Cancellation>();
                        default:
                            return _container.Resolve<GenericCovers.FixedPriceOptions>();
                    }
                case 35:
                    return _container.Resolve<GenericCovers.GenericListItemSelected>();

                case 37:
                    return _container.Resolve<Policies.OtherActivities.Material_DamageRentPayable>();
                case 24:
                    return _container.Resolve<GenericCovers.GenericPercentSumInsured>();
                case 41: //Cancellation
                    switch (policyId)
                    {
                        case 3:
                            return _container.Resolve<Policies.Conference.Cancellation>();
                        case 6:
                            return _container.Resolve<Policies.Events.Cancellation>();
                        case 10:
                            return _container.Resolve<Policies.Wedding.Cancellation>();
                        case 11:
                            return _container.Resolve<Policies.Wedding.Cancellation>();
                        default:
                            return _container.Resolve<GenericCovers.FixedPriceOptions>();
                    }

            }


            // Policy Specific Covers
            switch (policyId)
            {
                case 1://Equipment Hirers
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.EquipmentHirers.PublicLiability>();
                    }
                    break;
                case 2://Showmen
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.EquipmentHirers.PublicLiability>();
                    }
                    break;
                case 3://Conference & Meetings
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.Conference.PublicLiability>();
                    }
                    break;
                case 4: //Other Activities
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.OtherActivities.PublicLiability>();
                    }
                    break;
                case 5: //Day Cover for Inflatables
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.DayCover.PublicLiability>();
                    }
                    break;
                case 6: //Events
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.Events.PublicLiability>();
                    }
                    break;
                case 7://Field Sports Annual
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.FieldSportsAnnual.PublicLiability>();
                    }
                    break;
                case 8://Field Sports Event
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.FieldSportsAnnual.PublicLiability>();
                    }
                    break;

                case 10:    // Weddings
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.Wedding.PublicLiability>();
                    }
                    break;

                case 11:    // Bar Mitvah
                    switch (coverId)
                    {
                        case CoverId.PublicLiability: // Wedding included cover
                            return _container.Resolve<Policies.Wedding.PublicLiability>();
                            //return _container.Resolve<NoCharge>();
                    }
                    break;
                case 13: //airsoft
                case 20:// paintball
                case 14://Freelance Catering
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.FreelanceCatering.PublicLiability>();
                    }
                    break;
                case 15://Freelance Activity Instructor
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.FreelanceAdventureInstructors.PublicLiability>();
                    }
                    break;

                case 16://Freelance Sports Instructor
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.FreelanceAdventureInstructors.PublicLiability>();
                    }
                    break;
                case 18:
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.Exhibitors.PublicLiability>();
                    }
                    break;
                case 24: //Stallholders
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<GenericCovers.GenericListItemSelected>();

                    }
                    break;
                case 25:
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            return _container.Resolve<Policies.StreetParties.PublicLiability>();

                    }
                    break;
                case 26:
                    switch (coverId)
                    {
                        case CoverId.PublicLiability:
                            //return _container.Resolve<GenericCovers.GenericListItemSelected>();
                            return _container.Resolve<Policies.MobileCatering.GenericLiabilty>();

                    }
                    break;
                case 27:
                    switch (coverId)
                    {
                        case CoverId.BrandProtection:
                            return _container.Resolve<Policies.BrandProtection.AnnualLoss>();
                        case CoverId.RehabilitationExpenses:
                            return _container.Resolve<Policies.BrandProtection.FreeCover>();
                        case CoverId.RecallCosts:
                            return _container.Resolve<Policies.BrandProtection.FreeCover>();
                        case CoverId.SuppContamination:
                            return _container.Resolve<Policies.BrandProtection.FreeCover>();
                        case CoverId.WorkplaceViolence:
                            return _container.Resolve<Policies.BrandProtection.FreeCover>();
                        case CoverId.PubHealthAuthorityAnnouncement:
                            return _container.Resolve<Policies.BrandProtection.FreeCover>();
                        case CoverId.MalicousTampering:
                            return _container.Resolve<Policies.BrandProtection.FreeCover>();
                        case CoverId.Extortion:
                            return _container.Resolve<Policies.BrandProtection.FreeCover>();
                    }
                    break;
            }
            throw new ApplicationException($"Please add charge calculator for policy id: {policyId} and cover id: {coverId}");
        }
    }
}