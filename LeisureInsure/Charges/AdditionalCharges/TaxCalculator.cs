﻿using System;
using System.Linq;
using LeisureInsure.DB;
using System.Diagnostics;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Charges.AdditionalCharges
{
    public class TaxCalculator : ICalculateTax
    {
        private readonly IRepository _repository;
        
        public TaxCalculator(
            IRepository repository)
        {
            _repository = repository;
        }

        public decimal Calculate(decimal premiumAmount, int localeId, DateTime? dateEffective = null, string quoteRef = "")
        {
            var taxRate = Rate(localeId, dateEffective, quoteRef);
            return premiumAmount*taxRate;
        }

        public decimal Rate(int localeId, DateTime? dateEffective = null, string quoteRef = "")
        {
            var rates = _repository.getTaxRates(localeId);


            var rate = rates.Where(x => x.ValidUntil > dateEffective)
                .OrderByDescending(x => x.ValidUntil)
                .FirstOrDefault();

            if (rate == null)
            {
                rate = rates.Where(x => x.ValidUntil == null).FirstOrDefault();
            }

            if (rate == null)
            {
                var errorDetails = "LeisureInsure.Charges.AdditionalCharges.TaxCalculator.Rate | Exception logged: (TaxRate is null)";
                throw new Exception($"QuoteRef: {quoteRef} | {errorDetails}");
            }
            return rate.Value/100;
        }
    }
}

