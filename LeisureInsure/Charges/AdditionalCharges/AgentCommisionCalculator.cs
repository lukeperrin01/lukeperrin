﻿using System;
using System.Linq;
using LeisureInsure.DB;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Services.Information;
using LeisureInsure.Services.Dal.LeisureInsure.Information;

namespace LeisureInsure.Charges.AdditionalCharges
{
    public class AgentCommisionCalculator : ICalculateAgentCommision
    {
        private readonly IRepository _repository;
        private readonly IGetPolicyInformation _policy;

        public AgentCommisionCalculator(
            IRepository repository, 
            IGetPolicyInformation policy)
        {
            _repository = repository;
            _policy = policy;
        }

        public decimal Rate(int policyId, int localeId, DateTime? date = null)
        {
            var policyBitwiseId = _policy.BitwiseId(policyId);

            var policy = _repository.GetPolicy(policyId);

            if (policy != null)
                return (decimal)policy.BrokerCommission;

            //no rates have commision on them          
            //var rate = _repository.Q<Rate>().Where(x => (x.PolicyBw & policyBitwiseId) > 0 &&
            //                                            (x.LocaleBw & localeId) > 0 &&
            //                                            x.RateType == RateTypes.AgentCommision).FirstOrDefault();           

            return 0;

            //throw new ApplicationException($"Please ensure that there is a commision for policy id: {policyId} and locale id: {localeId}");
        }
    }
}