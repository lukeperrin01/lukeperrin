﻿using LeisureInsure.DB;


namespace LeisureInsure.Charges.AdditionalCharges
{
    public class CardChargeCalculator : ICalculateCardCharges
    {
        private readonly IRepository _repository;

        public CardChargeCalculator(IRepository repository)
        {
            _repository = repository;
        }

        public decimal AddChargeTo(decimal charge, int localeId, int paymentCardId)
        {
            if (paymentCardId == 0)
            {
                return 0;
            }
            var card = _repository.getPaymentCardById(paymentCardId);
            var cardFee = 0m;

            if (localeId == 1) // UK
            {
                cardFee += card.AddChargeUk;
            }
            else if (localeId == 2) // IE
            {
                cardFee += card.AddChargeIe;
            }

            //if our 3% is greater then use that
            if (charge * card.AddRate > cardFee)
                cardFee = charge * card.AddRate;            

            return cardFee;
        }
    }
}
