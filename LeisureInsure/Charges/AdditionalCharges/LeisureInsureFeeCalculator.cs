﻿using System;
using System.Linq;
using LeisureInsure.DB;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Services.Information;
using System.Data;
using System.Collections.Generic;
using System.Diagnostics;
using LeisureInsure.DB.Logging;
using System.Web.Http;

namespace LeisureInsure.Charges.AdditionalCharges
{
    public class LeisureInsureFeeCalculator : ICalculateLeisureInsureFees
    {
        private readonly IRepository _repository;
        private readonly IGetPolicyInformation _policy;

        public LeisureInsureFeeCalculator(
            IRepository repository,
            IGetPolicyInformation policy)
        {
            _repository = repository;
            _policy = policy;
        }

        public void Calculate(Quote quote)
        {
            try
            {
                decimal totalfee = 0;
                decimal minimumfee = 0;
                //get fee at quote level
                Rate rate = Rate(quote.PolicyId, quote.LocaleId, null, quote.QuoteReference);


                var eventSupplierHackRates = _repository.getQuoteInputsbyQuotePK(quote.Id);
                var eventSupplierHack = eventSupplierHackRates.Where(x => x.RateTypeFK == 5 && x.CoverPK == 2).FirstOrDefault();

                // This hack is necessary (hopefully temporarily) because event supplier is a FlatCharge of £1.99 whereas for event organisers it's 5% with min rate of £10
                //var eventSupplierHack = _repository.Q<Answer>().Where(x => (x.QuoteId == quote.Id && x.RateTypeFK == 5 && x.CoverPk == 2)).FirstOrDefault();
                //TODO DATAWORK
                if (eventSupplierHack != null && quote.PolicyId == 6)
                {
                    //var hackrate = _repository.Q<Rate>().Where(x => x.Id == eventSupplierHack.RatePk).FirstOrDefault();
                    var hackrate = _repository.getRatebyPK(eventSupplierHack.RatePK ?? 0);
                    if (hackrate.ListRatePK == -1309)
                    {
                        if (quote.LocaleId == 1)
                        {
                            totalfee = (decimal)1.99;
                            minimumfee = (decimal)0;
                        }
                        else
                        {
                            totalfee = (decimal)2.99;
                            minimumfee = (decimal)0;
                        }
                        //rate.DivBy = 1;
                    }
                    else if (rate != null)
                    {
                        if (rate.DivBy == 1)
                        {
                            totalfee = rate.Value;
                            //we dont need a minimum rate on a flat fee
                            minimumfee = 0;
                        }
                        else if (rate != null)
                        {
                            totalfee = rate.Value * quote.NetPremium / (int)rate.DivBy;
                            minimumfee = (decimal)rate.MinimumRate;
                        }
                    }
                }


                else if (rate != null)
                {
                    if (rate.DivBy == 1)
                    {
                        totalfee = rate.Value;
                        //we dont need a minimum rate on a flat fee
                        minimumfee = 0;
                    }
                    else if (rate != null)
                    {
                        totalfee = rate.Value * quote.NetPremium / (int)rate.DivBy;
                        minimumfee = (decimal)rate.MinimumRate;
                    }
                }
                if (quote.Buy != -1)
                {
                    totalfee = Math.Max(totalfee, minimumfee);
                    if (totalfee == 0)
                    {
                        totalfee = quote.LeisureInsureFees;
                    }
                    quote.LeisureInsureFees = totalfee;
                }

                int premiumCovers = quote.ChargeSummaryItems.Where(x => x.Net > 0).Count();

                foreach (var line in quote.ChargeSummaryItems)
                {
                    //only add fee to chargable premiums
                    if (line.Net > 0)
                    {
                        if (quote.Buy == -1)
                        {
                            if (line.coverFK == 2 || line.coverFK == 42)
                            {
                                line.Price = line.Price + quote.LeisureInsureFees;
                                line.Total = line.Price;
                                line.Fees = quote.LeisureInsureFees;
                            }
                            else
                            {
                                line.Fees = 0;
                            }
                        }
                        else
                        {
                            if (rate == null || rate.DivBy == 1)
                            {
                                //if flat fee only apply to PL or Annual Loss (Brand Impact)
                                if (line.coverFK == 2 || line.coverFK == 42)
                                {
                                    //by this stage we already have tax
                                    line.Price = line.Price + totalfee;
                                    line.Total = line.Price;
                                    line.Fees = totalfee;
                                }
                            }
                            else
                            {
                                //get % this net is of the total net and use that to get the fee % from total fee                       
                                decimal percOfTotal = line.Net / quote.NetPremium;
                                decimal coverfee = totalfee * percOfTotal;
                                line.Price = line.Price + coverfee;
                                line.Total = line.Price;
                                line.Fees = coverfee;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.AllCoverFeeCalculator.Calculate] url:post:api/v1/charges", quote.QuoteReference);
                throw new ApplicationException($"{errorid}");
            }

        }

        public Rate Rate(int policyId, int localeId, DateTime? date = null, string quoteRef = "")
        {
            try
            {
                var policyBitwiseId = _policy.BitwiseId(policyId);
                IList<Rate> rateList = new List<Rate>();
                var rates = _repository.getPolicyRatesbyRateTypeFK(policyBitwiseId, localeId, 19);

                foreach (var r in rates)
                {
                    rateList.Add(new Rate()
                    {
                        CoverBw = r.CoverBw,
                        DiscountFirstRate = r.DiscountFirstRate,
                        DiscountSubsequentRate = r.DiscountSubsequentRate,
                        DivBy = r.DivBy,
                        Excess = r.Excess,
                        FirstRate = r.FirstRate,
                        HazardRating = r.HazardRating,
                        Id = r.Id,
                        Indemnity = r.Indemnity,
                        ListRatePK = r.ListRatePK,
                        LocaleBw = r.LocaleBw,
                        MinimumRate = r.MinimumRate,
                        MustRefer = r.MustRefer,
                        PolicyBw = r.PolicyBw,
                        RateExclude = r.RateExclude,
                        RateTypeFK = r.RateTypeFK,
                        SumInsured = r.SumInsured,
                        TableParentRatePK = r.TableParentRatePK,
                        TamOccupation = r.TamOccupation,
                        Threshold = r.Threshold,
                        ValidUntil = r.ValidUntil,
                        Value = r.Value
                    });
                }

                if (date == null)
                {
                    return rateList.FirstOrDefault();
                }
                var rate = rateList.Where(x => x.ValidUntil > date).OrderByDescending(x => x.ValidUntil).FirstOrDefault();
                if (rate != null)
                {
                    return rate;
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[LeisureInsure.Charges.AdditionalCharges.LeisureInsureFee.Rate] url:post:api/v1/charges", quoteRef);
                throw new ApplicationException($"{errorid}");
            }

            throw new ApplicationException($"QuoteRef: {quoteRef} |Please ensure that there is a fee for policy id: {policyId} and locale id: {localeId}");
        }
    }
}