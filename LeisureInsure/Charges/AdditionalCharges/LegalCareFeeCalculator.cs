﻿using System;
using System.Configuration;
using LeisureInsure.DB;

namespace LeisureInsure.Charges.AdditionalCharges
{
    public class LegalCareFeeCalculator : ICalculateLegalCareFee
    {
        private readonly ICalculateTax _tax;
        private readonly IRepository _rep;

        public LegalCareFeeCalculator(ICalculateTax tax, IRepository rep)
        {
            _tax = tax;
            _rep = rep;
        }

        public decimal Calculate(decimal totalPremiumWithoutTax, int policyId, int localeId, int quotePK, DateTime? dateEffective = null)
        {
            var legalCareRate = 6m;
            var q = _rep.GetQuotebyQuotePK(quotePK);
            var legalCare = 0m;


            var rate = ConfigurationManager.AppSettings["legalcare.rate"];
            try
            {
                legalCareRate = decimal.Parse(rate);
            }
            catch (FormatException)
            {}

            var legalCareRateAsAMultiplier = (legalCareRate/100);
            var netLegalCare = totalPremiumWithoutTax*legalCareRateAsAMultiplier;
            if (q.TaxRate != 0)
            {
                legalCare = netLegalCare + _tax.Calculate(netLegalCare, localeId, dateEffective);
            }
            else
            {
                legalCare = netLegalCare;
            }

            return legalCare;
        }
    }
}