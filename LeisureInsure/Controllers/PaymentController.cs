﻿using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Services.Payment;
using LeisureInsure.Services.Utilities;
using LeisureInsure.ViewModels;
using Microsoft.Azure;
using NServiceBus;
using ItOps.Commands;
using Website.Events;
using System;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Controllers
{
    public class PaymentController : Controller
    {
        private readonly ILiRepository _repository;
        private readonly IEndpointInstance _endpointInstance;
        private readonly IValidateGlobalIrisResponse _response;
        private readonly IAddPaymentOptions _paymentOptions;
        private readonly IAddGlobalIrisInformation _globalIris;


        public PaymentController(
            ILiRepository repository, 
            IEndpointInstance endpointInstance,
            IValidateGlobalIrisResponse response, 
            IAddPaymentOptions paymentOptions,
            IAddGlobalIrisInformation globalIris)
        {
            _repository = repository;
            _endpointInstance = endpointInstance;
            _response = response;
            _paymentOptions = paymentOptions;
            _globalIris = globalIris;
        }

        // ReSharper disable InconsistentNaming
        public ActionResult Index(string merchant_Id, string order_Id, string amount,
            string timestamp, string sha1Hash, string result, string message, string account,
            string authCode, string pasref, string comment2, string passcode)
        {
            // ReSharper restore InconsistentNaming

            var model = new PaymentResponseVm();
            string quoteref = "";

            // Ensure that we are dealing with Global Iris
            var validated = _response.Validate(timestamp, merchant_Id, order_Id, result, message, pasref, authCode, sha1Hash);
            if (!validated)
            {
                AppInsightLog.LogInfo($"[PaymentController.Index] PaymentController:Could not validate global iris response", comment2);
                throw new HttpException(400, "Could not validate global iris response");
            }

            try
            {
                var quoteReference = comment2;
                var homePage = CloudConfigurationManager.GetSetting("website.address");
                var quote = _repository.Q<Quote>().First(x => x.QuoteReference == quoteReference);
                quoteref = quote.QuoteReference;
                var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

                _paymentOptions.AddPaymentOptionsTo(quote);
                _globalIris.AddInformation(quote);

                if (result.Equals("00"))
                {
                    //at this point we mark quote as paid for
                    quote.Purchased = 1;
                    quote.DatePurchased = dateNow;
                    quote.LockedForCustomer = true;

                    _repository.Commit();                    

                    // Success... send message to continue the purchase process
                    _endpointInstance.Send<PaymentConfirmed>(m =>
                    {
                        m.QuoteReference = quoteReference;
                    });

                    return View("Confirmed", new PaymentResponseVm
                    {
                        HomepageUrl = homePage,
                    });
                }

                // Declined
                _endpointInstance.Publish<PaymentDeclined>(m =>
                {
                    m.QuoteReference = quoteReference;
                });

                quote.OrderNumber++;
                _repository.Commit();


                var paymentMethod = quote.PaymentOptions.First(x => x.GlobalIrisFields.Account.Equals(account));

                model = new PaymentResponseVm
                {
                    Amount = paymentMethod.GlobalIrisFields.Amount,
                    Account = paymentMethod.GlobalIrisFields.Account,
                    Timestamp = paymentMethod.GlobalIrisFields.Timestamp,
                    OrderId = paymentMethod.GlobalIrisFields.OrderId,
                    MerchantId = paymentMethod.GlobalIrisFields.MerchantId,
                    QuoteReference = quote.QuoteReference,
                    Passcode = quote.Password,
                    AuthCode = authCode,
                    Message = message,
                    PasRef = pasref,
                    Result = result,
                    Currency = paymentMethod.GlobalIrisFields.Currency,
                    Hash = paymentMethod.GlobalIrisFields.Hash,
                    HomepageUrl = homePage,
                    ProductDescription = quote.PolicyType
                };

                model.EmailQuoteUrl = CloudConfigurationManager.GetSetting("website.address") + "/payment/email-quote";

                model.AutoSettleFlag = CloudConfigurationManager.GetSetting("global-iris.auto-settle-flag");
                var useTest = CloudConfigurationManager.GetSetting("global-iris.use-test").ToSafeInt();
                if (useTest == 0) // The default
                {
                    model.Url = "https://hpp.globaliris.com/pay";
                }
                else
                {
                    model.Url = "https://hpp.sandbox.globaliris.com/pay";
                }
                model.ResponseUrl = CloudConfigurationManager.GetSetting("global-iris.responseUrl");

                AppInsightLog.LogInfo($"[PaymentController.Index] PaymentController:Index Declined_result:{result}QuoteRef:{quoteref}",quoteref);

                return View("Declined", model);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex,"[PaymentController.Index] "+quoteref,quoteref);

                return View("Declined", model);
            }           
            
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Email([FromBody]string email, [FromBody]string quoteReference, [FromBody]string passcode)
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            _endpointInstance.Send<ItOps.Commands.Email.Quote>(m =>
            {
                m.QuoteReference = quoteReference;
                m.Email = email;
                m.Passcode = passcode;
                m.BaseLink = url.Scheme + "://" + url.Host + ":" + url.Port;
            });
            return RedirectToAction("Index", "Home");
        }
    }
}