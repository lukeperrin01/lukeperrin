﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LeisureInsure.Helpers;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.DB;
using System.Xml;
using System.IO;
using Rotativa;
using LeisureInsure.DB.Logging;
using System.Diagnostics;

namespace LeisureInsure.Controllers
{
    public class HomeController : JsonController
    {        

        public HomeController()
        {

        }      
       

        #region Login
        //public ActionResult Login(string u, string p)
        //{
        //    vmLogin _vmRV = _repository.login(u, p);
        //    Session.Add("contactPK", _vmRV.ContactPK);
        //    Session.Add("contactType", _vmRV.ContactType);
        //    return Json(_vmRV, JsonRequestBehavior.AllowGet);

        //}
        #endregion

        #region culture
        protected vmLocales _Locales(string strLocale = "")
        {
            HttpCookie locale = new HttpCookie("locale");
            if (strLocale == "")
            {
                if (!this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("locale"))
                {

                    string _culture = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                    if (_culture == "en-GB" || _culture == "ga-IE")
                    {
                        locale.Value = _culture;
                    }
                    else
                    {
                        locale.Value = "en-GB";
                    }
                    locale.Expires = DateTime.Now.AddMonths(12);
                    HttpContext.Response.Cookies.Add(locale);
                    strLocale = locale.Value;
                }
                else
                {
                    strLocale = Request.Cookies["locale"].Value;
                }
            }

            else
            {
                locale.Value = strLocale;
                locale.Expires = DateTime.Now.AddMonths(12);
                HttpContext.Response.Cookies.Add(locale);
                strLocale = locale.Value;
            }

            var _locales = new vmLocales();

            using (var rep = new PoliciesRepository())
            {
                _locales = rep.GetLocales(strLocale,1);
            }

            
            return _locales;
        }

        #endregion

        public ActionResult Index(string strLocale = "", int _quotePK = 0, int _policyPK = -1)
        {            

            return View("Index");
        }

        public void SetLocale(string _locale)
        {
            HttpCookie locale = new HttpCookie("locale");
            locale.Value = _locale;
            locale.Expires = DateTime.Now.AddMonths(12);
            HttpContext.Response.Cookies.Add(locale);
        }

        [Route("")]
        public ActionResult GetHome(string strLocale = "", int _quotePK = 0, int _policyPK = -1)
        {
            try
            {
                vmLocales _locales = _Locales(strLocale);
                var _vmNav = new vmNavigation();

                using (var rep = new Repository())
                {
                    _vmNav = rep._getHome(_locales, _quotePK, _policyPK);
                }
                
                return Json(_vmNav, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string reference = "GetHome";
                if (_policyPK != 0)
                    reference += $"Policy:{_policyPK.ToString()}";
                if (_quotePK != 0)
                    reference += $"Quote:{_quotePK.ToString()}";

                var errorid = AppInsightLog.LogError(ex, $"[HomeController.GetHome] url:post:api/v1/charges", reference);
                throw new ApplicationException($"{errorid}");
            }

        }
        public ActionResult GetInputs(int _policyPK, int _quotePK)
        {
            try
            {
                vmLocales _locales = _Locales();
                var _vmInputs = new List<vmInputHeader>();

                using (var rep = new Repository())
                {
                   _vmInputs = rep._getInputs(_locales.localeSelected.localeBW, _policyPK, _quotePK);
                }
                
                return Json(_vmInputs, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string reference = "GetInputs";
                if (_policyPK != 0)
                {
                    reference += $"Policy:{_policyPK.ToString()}";
                }
                if (_quotePK != 0)
                {
                    reference += $"Quote:{_quotePK.ToString()}";
                }
                var errorid = AppInsightLog.LogError(ex, $"[HomeController.GetInputs]", _quotePK.ToString());
                throw new ApplicationException($"{errorid}");

            }

        }
        public ActionResult GetCovers(int _policyPK, int _quotePK)
        {
            try
            {
                vmLocales _locales = _Locales();
                var _cover = new vmCovers();
                using (var repo = new Repository())
                {
                     _cover = repo._getCovers(_locales.localeSelected.localePK, _policyPK, _quotePK);
                }
                
                return Json(_cover, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string reference = "GetCovers";
                if (_policyPK != 0)
                    reference += $"Policy:{_policyPK.ToString()}";
                if (_quotePK != 0)
                    reference += $"Quote:{_quotePK.ToString()}";

                var errorid = AppInsightLog.LogError(ex, $"[HomeController.GetCovers]", _quotePK.ToString());
                throw new ApplicationException($"{errorid}");
            }
        }

    }
}
