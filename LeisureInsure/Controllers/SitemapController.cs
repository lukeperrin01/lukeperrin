﻿using System;
using System.Linq;
using System.Web.Mvc;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.LandingPages;
using LeisureInsure.ViewModels;
using Microsoft.Azure;

namespace LeisureInsure.Controllers
{
    public class SitemapController : Controller
    {
        readonly ILiRepository _repository;

        public SitemapController(ILiRepository repository)
        {
            _repository = repository;
        }

        // GET: Sitemap
        public ActionResult Index()
        {
            var baseUrl = CloudConfigurationManager.GetSetting(Constants.Configuration.WebsiteBaseAddress);

            var landingPages = _repository.Q<LandingPage>().Select(x => new 
            {
                Location = x.Url,
                ChangeFrequency = "monthly",
                LastModified = x.LastModified,
            }).ToList().Select(x => new SitePage
            {
                Location = baseUrl + "/quote/" + x.Location,
                ChangeFrequency = x.ChangeFrequency,
                LastModified = x.LastModified.ToString("yyyy-MM-dd"),
            }).ToList();

            var model = new SitemapVm
            {
                Pages = landingPages,
                BaseUrl = baseUrl,
                LastChanged = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time").ToString("yyyy-MM-dd"), // TODO: Change this
            };
            return View(model);
        }
    }
}