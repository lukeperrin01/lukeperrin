﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LeisureInsure.DB;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.LandingPages;
using LeisureInsure.ViewModels;
using StackExchange.Profiling;

namespace LeisureInsure.Controllers
{
    public class TestController : Controller
    {
        private readonly ILiRepository _repository;

        public TestController(ILiRepository repository)
        {
            _repository = repository;
        }

       

        public ActionResult Search()
        {
            List<SearchViewModel> a = null;
            for (var i = 0; i < 50; i++)
            {
                a = _repository.Q<LandingPage>().Select(x => new SearchViewModel
                {
                    Url = x.Url,
                    Keywords = x.Keywords + ", " + x.Title,
                    Title = x.Title
                }).ToList();
            }
            return View(a);
        }
    }
}