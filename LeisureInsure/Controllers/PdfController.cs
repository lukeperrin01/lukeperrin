﻿using System.Web.Mvc;
using LeisureInsure.DB;
using Rotativa;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using System;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB.ViewModels;
using System.ComponentModel;
using LeisureInsure.DB.Business;
using iText.Kernel.Pdf;
using iText.Kernel.Geom;
using iText.Layout.Element;
using iText.IO.Image;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Xobject;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using iText.Kernel.Pdf.Canvas.Parser.Data;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Font;

//using System.Web.Http;


namespace LeisureInsure.Controllers
{
    public class PdfController : Controller
    {
        private readonly IRepository _repository;
        private readonly ILiRepository _liRepository;

        public PdfController(IRepository repository,
            ILiRepository liRepository)
        {
            _repository = repository;
            _liRepository = liRepository;
        }

        public void WriteTsv<T>(IEnumerable<T> data, TextWriter output)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            foreach (PropertyDescriptor prop in props)
            {
                output.Write(prop.DisplayName); // header
                output.Write("\t");
            }
            output.WriteLine();
            foreach (T item in data)
            {
                foreach (PropertyDescriptor prop in props)
                {
                    output.Write(prop.Converter.ConvertToString(
                         prop.GetValue(item)));
                    output.Write("\t");
                }
                output.WriteLine();
            }
        }

        public void GetInstallments()
        {
            try
            {
                var dt = DateTime.Now.ToString("yyyy-MM-dd");

                var installments = _repository.GetInstallments();
                if (installments.Count() > 0)
                {
                    HttpContext.Response.ClearContent();
                    HttpContext.Response.AddHeader("content-disposition", "attachment;filename=installments " + dt + ".xls");
                    HttpContext.Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    WriteTsv(installments, Response.Output);
                    Response.End();
                    _repository.SetInstalmentsDownloaded();
                }
                else
                {
                    var data = new[]{
                               new{ Name="No new installments to report"+DateTime.Now }
                      };
                    HttpContext.Response.ClearContent();
                    HttpContext.Response.AddHeader("content-disposition", "attachment;filename=installments " + dt + ".xls");
                    HttpContext.Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    WriteTsv(data, Response.Output);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PdfController.GetInstallments]", "Installments");
            }
        }

        [HttpGet]
        public ActionResult OldCertificate(int quotePK)
        {
            try
            {
                var quote = _repository.GetOldcert(quotePK);

                var pdfData = quote.Data;
                //FileContentResult System.Web.Mvc
                return File(pdfData, "application/pdf");
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PdfController.OldCertificate]", quotePK.ToString());
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        [HttpGet]
        public ActionResult NewCertificate(string quoteReference, string passcode)
        {
            try
            {
                //return as html for testing
                //return NewCertificateBody(quoteReference, passcode);
                //return NewHazards(quoteReference, passcode);


                var quote = new Quotes();
                var certificate = new Certs();

                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == quoteReference && x.PWord == passcode);

                    if (quote != null && quote.PolicyFK == 10 || quote.PolicyFK == 11)
                    {
                        var dateFrom = (DateTime)quote.DateFrom;
                        quote.DateTo = new DateTime(dateFrom.Year, dateFrom.Month, dateFrom.AddDays(1).Day, 23, 59, 0);
                    }
                    //remove old certificates if the are unlocked (not purchased)
                    var certs = repo.Certs.Where(x => x.QuoteRef == quote.QuoteRef && x.Lock == 0).ToList();
                    if (certs != null)
                    {
                        repo.Certs.RemoveRange(certs);
                        repo.SaveChanges();
                    }

                    certificate = repo.Certs.FirstOrDefault(x => x.QuoteRef == quote.QuoteRef);

                    if (certificate == null)
                    {
                        GenerateNewCertificate(quoteReference, passcode);
                        certificate = repo.Certs.FirstOrDefault(x => x.QuoteRef == quote.QuoteRef);
                    }


                }

                var pdfData = certificate.Certificate;
                return File(pdfData, "application/pdf");

            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[LeisureInsure.Controllers.PdfController.Certificate]", quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        [HttpGet]
        public ActionResult GetNewStatementofFact(string quoteReference)
        {
            try
            {
                var quote = new Quotes();

                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == quoteReference);
                }
                string CertRef = "";
                if (quote.RenewalTamPolicyNo != null)
                {
                    CertRef = quote.RenewalTamPolicyNo;
                }
                else
                {
                    CertRef = quote.QuoteRef;
                }
                if (!string.IsNullOrEmpty(quote.TamClientCode))
                {
                    CertRef = quote.TamClientCode;
                }

                var schedule = new vmCertSchedule();

                using (var repo = new PoliciesRepository())
                {
                    schedule = repo.GetCertSchedule(quote.QuotePK);
                }

                schedule.PolicyNumber = CertRef;


                return new Rotativa.ViewAsPdf("StatementOfFact", schedule);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[PdfController.GetStatementofFact] get:GetStatementofFact" + quoteReference, quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        [HttpPost]
        public ActionResult GenerateNewCertificate(string quoteReference, string passcode)
        {
            try
            {

                var quote = new Quotes();
                var docs = new List<PolicyDocuments>();
                QuoteLineNew employersLiab = null;
                var pdf = new List<byte[]>();
                var clausepdf = new List<byte[]>();
                var customClauses = new List<QuoteClause>();
                byte[] all;
                var addedTerms = false;

                var pdfPageNo = 1;

                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == quoteReference && x.PWord == passcode);

                    var quotelines = repo.QuoteLineNew.Where(x => x.QuoteId == quote.QuotePK).ToList();

                    employersLiab = quotelines.FirstOrDefault(x => x.CoversNew.CoverType1.Id == 2);

                    customClauses = repo.QuoteClause.Where(x => x.QuoteId == quote.QuotePK && x.CustomClauseName != null).ToList();

                }

                var path = System.IO.Path.Combine(Server.MapPath("~"), @"Content\CertificateDocuments\");

                //do we have nill excess? have we added inflatables?
                using (var repo = new PoliciesRepository())
                {
                    docs = repo.GetClausesForCert(quote.QuotePK);
                }

                var bodyByes = CreateCertificatePart(quoteReference, passcode, CertificatePart.NewCertificateBody);

                pdf.Add(bodyByes);


                //add all website clauses
                foreach (var i in docs)
                {
                    var link = path + i.DocLink;

                    byte[] buffer;
                    using (Stream stream = new FileStream(@link, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        buffer = new byte[stream.Length - 1];
                        stream.Read(buffer, 0, buffer.Length);
                    }

                    if (!addedTerms)
                        buffer = AddTermsHeader(buffer);
                    addedTerms = true;

                    clausepdf.Add(buffer);
                }

                //add all custom clauses
                foreach (var customClause in customClauses)
                {
                    var clauseBytes = customClause.CustomClause;

                    if (!addedTerms)
                        clauseBytes = AddTermsHeader(customClause.CustomClause);

                    addedTerms = true;
                    clausepdf.Add(clauseBytes);
                }

                var nUpped = CombineIntoPage(clausepdf);
                pdf.Add(nUpped);                


                //define endorsements here
                var endorsements = _repository.getQuoteEndorsements(quote.QuotePK).Endorsements.Count();
                if (endorsements != 0)
                {
                    pdf.Add(CreateCertificatePart(quoteReference, passcode, CertificatePart.Endorsements));
                }

                pdf.Add(CreateCertificatePart(quoteReference, passcode, CertificatePart.NewStatementOfFact));
                if (quote.Purchased == 1)
                {
                    pdf.Add(CreateCertificatePart(quoteReference, passcode, CertificatePart.NewSignature));
                }

                if (employersLiab != null)
                {
                    //do we have employers liability - only showing these for purchased and UK?
                    if (employersLiab.Required && quote.LocaleId == 1 && quote.Purchased == 1)
                    {
                        pdf.Add(CreateCertificatePart(quoteReference, passcode, CertificatePart.NewELCertificate));
                    }
                }

                var haz = new vmCertHazardList();

                using (var repo = new PoliciesRepository())
                {
                    haz = repo.GetHazardsForCert(quote.QuotePK);
                }

                if (haz.Hazards.Any())
                {
                    pdf.Add(CreateCertificatePart(quoteReference, passcode, CertificatePart.NewHazards));
                }

                if (quote.LegalFeesAdded && quote.LocaleId == 1 && quote.Purchased == 1)
                {
                    pdf.Add(CreateCertificatePart(quoteReference, passcode, CertificatePart.NewLegalCertificate));
                }
                

                all = CombineIntoDocument(pdf);
                
                all = SetPageNo(all);

                if (quote.Purchased == 0)
                {
                    var imagePath = Server.MapPath(@"~\Content\WaterMarkQuoteOnly.png");

                    var tranState = new iText.Kernel.Pdf.Extgstate.PdfExtGState();
                    tranState.SetFillOpacity(0.1f);
                    tranState.SetStrokeOpacity(0.1f);

                    ImageData myImageData = ImageDataFactory.Create(imagePath, true);
                    Image img = new Image(myImageData);
                    var reader = new PdfReader(new MemoryStream(all));

                    using (var ms = new MemoryStream())
                    {
                        var writer = new PdfWriter(ms);
                        var doc = new PdfDocument(reader, writer);
                        int pages = doc.GetNumberOfPages();
                        for (int i = 1; i <= pages; i++)
                        {
                            //get existing page
                            PdfPage page = doc.GetPage(i);
                            var canvas = new PdfCanvas(page);
                            canvas.SaveState().SetExtGState(tranState);
                            canvas.AddImage(myImageData, 0, 300, false);
                            canvas.RestoreState();

                        }
                        doc.Close();
                        all = ms.GetBuffer();
                        ms.Flush();
                    }

                }
                var fileType = "Certificate";

                if (quote.Purchased == 0)
                {
                    fileType = "Quote";
                }
                var certificatesModel = new CertificatesModel
                {
                    Url = "test",
                    Certificate = File(all, "application/pdf").FileContents,
                    QuoteRef = quote.QuoteRef,
                    Lock = quote.Purchased,
                    FileName = fileType
                };

                _repository.addCertificate(certificatesModel);             

                return new HttpStatusCodeResult(200, "Generated Certificate");
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[PdfController.GenerateCertificate]", quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        private static byte[] CombineIntoDocument(List<byte[]> pdf)
        {
            byte[] all;
            //get each section in list and combine into one document (byte[])                 
            using (var ms = new MemoryStream())
            {
                var writer = new PdfWriter(ms);
                var newdoc = new PdfDocument(writer);

                foreach (var p in pdf)
                {
                    var pdfDoc =
                             new PdfDocument(new PdfReader(new MemoryStream(p)));

                    var pdfReader = new PdfReader(new MemoryStream(p));
                    var pos = pdfReader.GetLastXref();
                    var length = pdfReader.GetFileLength();

                    var pages = pdfDoc.GetNumberOfPages();

                    for (var i = 1; i <= pages; i++)
                    {
                        var page = pdfDoc.GetPage(i);
                        newdoc.AddPage(page.CopyTo(newdoc)); ;
                    }
                }

                newdoc.Close();
                all = ms.GetBuffer();
                ms.Flush();
            }

            return all;
        }

        // <summary>
        // Stamp "Terms And Conditions" 
        // </summary>
        // <param name = "customClause" ></ param >
        // < param name= "pageNo" ></ param >
        // < param name= "addedTerms" ></ param >
        // < returns ></ returns >
        private byte[] AddTermsHeader(byte[] customClause)
        {

            var imagePath = Server.MapPath(@"~\Content\TermsAndConditions.png");
            ImageData imageData = ImageDataFactory.Create(imagePath, true);
            Image img = new Image(imageData);

            var reader = new PdfReader(new MemoryStream(customClause));

            using (MemoryStream ms = new MemoryStream())
            {
                var writer = new PdfWriter(ms);
                var doc = new PdfDocument(reader, writer);
                var page = doc.GetPage(1);
                var canvas = new PdfCanvas(page);

                canvas.SaveState();
                //35:x,805:y,520f:width.35f:height
                canvas.AddImage(imageData, 520f, 0, 0, 35f, 35, 805, false);
                canvas.RestoreState();

                doc.Close();
                ms.Flush();
                customClause = ms.GetBuffer();
            }


            return customClause;
        }

        /// <summary>
        /// Combine clauses into one page 
        /// </summary>
        /// <param name="clauses"></param>
        /// <returns></returns>
        public byte[] CombineIntoPage(List<byte[]> clauses)
        {
            byte[] result;

            try
            {
                using (var ms = new MemoryStream())
                {
                    PdfDocument newpdf = new PdfDocument(new PdfWriter(ms));
                    PageSize nUpPageSize = PageSize.A4;

                    float pageSpace = 0f;
                    float clauseSpace = 0f;
                    float prevSpace = 0f;

                    PdfPage page = newpdf.AddNewPage(nUpPageSize);
                    PdfCanvas canvas = new PdfCanvas(page);

                    foreach (var clause in clauses)
                    {

                        var reader = new PdfReader(new MemoryStream(clause));
                        PdfDocument sourcePdf = new PdfDocument(reader);
                        var pages = sourcePdf.GetNumberOfPages();

                        bool newPage = false;

                        if (pageSpace == 0f)
                            newPage = true;

                        //if pagespace exceeds 85k start a new page                        

                        if (pageSpace > 80500)
                        {
                            canvas.Release();
                            page = newpdf.AddNewPage(nUpPageSize);
                            canvas = new PdfCanvas(page);
                            newPage = true;
                            pageSpace = 0;
                        }

                        for (int i = 1; i <= pages; i++)
                        {
                            PdfPage currentPage = sourcePdf.GetPage(i);                                                                                               

                            Rectangle orig = currentPage.GetPageSize();
                            var width = 0.99f;
                            var height = 0.99f;
                            AffineTransform transformationMatrix = AffineTransform.GetScaleInstance(
                            width, height);
                            canvas.ConcatMatrix(transformationMatrix);

                            PdfFormXObject pageCopy = currentPage.CopyAsFormXObject(newpdf);

                            var strategy = new TextMeasurementListener();
                            var parser = new PdfCanvasProcessor(strategy);
                            parser.ProcessPageContent(currentPage);

                            clauseSpace = strategy.GetReservedSpaceInPoints();
                            //pageSpace += clauseSpace;

                            if (clauseSpace > 80500)
                            {
                                if (!newPage || i > 1)
                                {
                                    canvas.Release();
                                    page = newpdf.AddNewPage(nUpPageSize);
                                    canvas = new PdfCanvas(page);
                                    newPage = true;
                                    pageSpace = 0;                               
                                }
                            }

                            //look at room of previous page
                            if (newPage)
                                canvas.AddXObject(pageCopy, 0, 0);

                            //write next clause on the same page
                            if (!newPage)
                            {
                                //-400 is in the middle, -800 shifts to the bottom 
                                if (pageSpace > 70000f && clauseSpace < 20000f)
                                    canvas.AddXObject(pageCopy, 0, -450);//clause above is larger so shift down a bit
                                else if (pageSpace < 50000f && clauseSpace < 30000f)
                                    canvas.AddXObject(pageCopy, 0, -350); //clause above is smaller so shift up a bit
                                else if (pageSpace < 15000f && clauseSpace < 85000f)
                                    canvas.AddXObject(pageCopy, 0, -150); //clause above is much smaller so shift up even more
                                else if (pageSpace < 76000 && clauseSpace < 71000f)
                                    canvas.AddXObject(pageCopy, 0, -380); //clause above is fairly big, next clause is also little big shift up slightly
                                else
                                    canvas.AddXObject(pageCopy, 0, -400);
                            }

                            pageSpace += clauseSpace;
                            prevSpace = clauseSpace;                            
                        }
                        sourcePdf.Close();
                    }                    

                    newpdf.Close();

                    result = ms.GetBuffer();
                }


                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        /// <summary>
        /// Write page no to a pdf document
        /// </summary>
        /// <param name="customClause"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        private static byte[] SetPageNo(byte[] data)
        {

            byte[] customClause;

            try
            {
                var reader = new PdfReader(new MemoryStream(data));

                using (MemoryStream ms = new MemoryStream())
                {
                    var writer = new PdfWriter(ms);
                    var doc = new PdfDocument(reader, writer);
                    var pages = doc.GetNumberOfPages();

                    for (int i = 1; i <= pages; i++)
                    {
                        var page = doc.GetPage(i);
                        var canvas = new PdfCanvas(page);

                        var size = page.GetPageSize();

                        canvas.SaveState();
                        
                        var height = size.GetHeight();

                        canvas.ConcatMatrix(1, 0, 0, 1, -30,70);
                        canvas.BeginText()
                            .SetFontAndSize(PdfFontFactory.CreateFont(iText.IO.Font.Constants.StandardFonts.HELVETICA), 9)
                            .SetLeading(14 * 1.2f)
                            .MoveText(70, -40);

                        canvas.NewlineShowText($"Page {i} of {pages}");
                        canvas.EndText();

                        canvas.RestoreState();

                        
                        // ms.Flush();                    
                    }
                    doc.Close();
                    customClause = ms.GetBuffer();
                }

                return customClause;

            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        private enum CertificatePart
        {

            NewCertificateBody,
            NewSignature,
            NewStatementOfFact,
            NewELCertificate,
            NewLegalCertificate,
            NewHazards,
            Endorsements,
            CustomClause
        }

        private byte[] CreateCertificatePart(string quoteReference, string passcode, CertificatePart part)
        {
            var customSwitches = string.Format(
                "--print-media-type " +
                "--margin-top 10mm " +
                "--margin-bottom 10mm " +
                "--margin-left 10mm " +
                "--margin-right 10mm " +
                "--encoding utf-8 " +
                "--minimum-font-size 11 " +
                "--zoom 1.0 " +
                "--disable-smart-shrinking"
            );

            var pdfResult = new ActionAsPdf(part.ToString(), new { quoteReference, passcode }) { CustomSwitches = customSwitches };
            var pdfBytes = pdfResult.BuildPdf(ControllerContext);

            //pdfBytes = SetPageNo(pdfBytes, 15);

            return pdfBytes;
        }

        public ActionResult Endorsements(string quoteReference, string passcode)
        {
            try
            {
                var quote = _liRepository.Q<Quote>()
                        .FirstOrDefault(x => x.QuoteReference == quoteReference && x.Password == passcode);
                var endorsements = _repository.getQuoteEndorsements(quote.Id);

                return View("_Endorsements", endorsements);
            }
            catch (Exception ex)
            {

                AppInsightLog.LogError(ex, "[PdfController.Endorsements]", quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        public ActionResult NewCertificateBody(string quoteReference, string passcode)
        {
            try
            {
                string CertRef = "";
                var quote = new Quotes();
                var policy = new Products();
                var certModel = new vmCert();

                using (var repo = new PoliciesRepository())
                {
                    quote = repo.GetQuote(quoteReference, passcode);
                    policy = repo.GetPolicy((int)quote.PolicyFK);

                    if (quote.RenewalTamPolicyNo != null)
                    {
                        CertRef = quote.RenewalTamPolicyNo;
                    }
                    else
                    {
                        CertRef = quote.QuoteRef;
                    }

                    if (quote.OverrideDatesVal == null)
                    {
                        quote.OverrideDatesVal = 0;
                    }

                    //note there is nothing to do here, we are setting dates when 
                    //user clicks 'View Quote' or 'Purchase' for either client or broker            
                    if (quote.OverrideDatesVal == 0)
                    {


                    }

                    certModel.CertificatePK = CertRef;
                    certModel.Schedule = repo.GetCertSchedule(quote.QuotePK);
                    certModel.Sections = repo.GetCertSectionsAndHeaders(quote.QuotePK);
                }


                return View(policy.CertificateBody, certModel);
            }
            catch (Exception ex)
            {

                AppInsightLog.LogError(ex, "[PdfController.CertificateBody]", quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        public ActionResult NewSignature(string quoteReference, string passcode)
        {
            try
            {
                var quote = new Quotes();
                var brokerAddy = new vmBrokerAddress();

                using (var repo = new PoliciesRepository())
                {
                    quote = repo.GetQuote(quoteReference, passcode);
                    brokerAddy = repo.GetBrokerAddress(quote.QuotePK);
                }

                return View("Signature", brokerAddy);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[PdfController.Signature]", quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        public ActionResult ELCertificate(string quoteReference, string passcode)
        {
            try
            {
                var quote = _liRepository.Q<Quote>()
                        .FirstOrDefault(x => x.QuoteReference == quoteReference && x.Password == passcode);

                var schedule = _repository._getCertScheduleByQuotePK(quote.Id);
                string polNo = schedule.PolicyNumber;

                if (!string.IsNullOrEmpty(quote.TamClientCode))
                {
                    schedule.PolicyNumber = quote.TamClientCode;
                }


                return View("_ELCertificate", schedule);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[PdfController.ELCertificate]" + quoteReference, quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        public ActionResult NewELCertificate(string quoteReference, string passcode)
        {
            try
            {
                var schedule = new vmCertSchedule();
                var quote = new Quotes();

                using (var repo = new PoliciesRepository())
                {
                    quote = repo.GetQuote(quoteReference, passcode);
                    schedule = repo.GetCertSchedule(quote.QuotePK);
                }

                string polNo = schedule.PolicyNumber;

                if (!string.IsNullOrEmpty(quote.TamClientCode))
                {
                    schedule.PolicyNumber = quote.TamClientCode;
                }


                return View("_ELCertificate", schedule);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[PdfController.ELCertificate]" + quoteReference, quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }


        public ActionResult NewLegalCertificate(string quoteReference, string passcode)
        {
            try
            {
                var schedule = new vmCertSchedule();
                var quote = new Quotes();

                using (var repo = new PoliciesRepository())
                {
                    quote = repo.GetQuote(quoteReference, passcode);
                    schedule = repo.GetCertSchedule(quote.QuotePK);
                }

                return View("_LegalCertificate", schedule);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[PdfController.LegalCertificate]", quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }

        public ActionResult NewHazards(string quoteReference, string passcode)
        {

            try
            {
                var haz = new vmCertHazardList();

                using (var repo = new PoliciesRepository())
                {
                    var quote = repo.GetQuote(quoteReference, passcode);
                    haz = repo.GetHazardsForCert(quote.QuotePK);
                }

                return View("_CertificateHazardNew", haz);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[PdfController.Hazards]" + quoteReference, quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }


        public ActionResult NewStatementOfFact(string quoteReference, string passcode)
        {
            try
            {

                var schedule = new vmCertSchedule();
                var quote = new Quotes();

                using (var repo = new PoliciesRepository())
                {
                    quote = repo.GetQuote(quoteReference, passcode);
                    schedule = repo.GetCertSchedule(quote.QuotePK);
                }

                string CertRef = "";

                if (quote.RenewalTamPolicyNo != null)
                {
                    CertRef = quote.RenewalTamPolicyNo;
                }
                else
                {
                    CertRef = quote.QuoteRef;
                }

                if (!string.IsNullOrEmpty(quote.TamClientCode))
                {
                    CertRef = quote.TamClientCode;
                }

                schedule.PolicyNumber = CertRef;

                return View("StatementOfFact", schedule);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogError(ex, "[PdfController.StatementOfFact]", quoteReference);
                return new HttpStatusCodeResult(400, ex.Message);
            }
        }


    }

    public class TextMeasurementListener : IEventListener
    {
        private float space = 0.0f;

        public void EventOccurred(IEventData data, EventType type)
        {
            if (type != EventType.RENDER_TEXT)
                return;

            TextRenderInfo textRenderInfo = (TextRenderInfo)data;

            var renderInfos = textRenderInfo.GetCharacterRenderInfos();

            foreach (var charInfo in renderInfos)
            {
                CharacterRenderInfo characterRenderInfo = new CharacterRenderInfo(charInfo);
                space += characterRenderInfo.GetBoundingBox().GetWidth() * characterRenderInfo.GetBoundingBox().GetHeight();
            }
        }

        public float GetReservedSpaceInPoints()
        {
            return space;
        }

        ICollection<EventType> IEventListener.GetSupportedEvents()
        {
            //throw new NotImplementedException();
            return null;
        }
    }

}