﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Net;

namespace LeisureInsure.Infrastructure
{
    public class ApiClient 
    {

        public string BaseUrl { get; private set; }
        public Dictionary<String, String> Headers { get; private set; }

        public ApiClient(string url, Dictionary<String, String> headers)
        {
            Headers = headers;
            BaseUrl = url;
        }

        public ApiClient(string url)
        {
            BaseUrl = url;
            Headers = new Dictionary<string, string>();
        }

        public string PostForm(string url, String data)
        {
            return PostFormData(BaseUrl + url, data);
        }
        

        public string GetOneAsJson<T>(String url, Dictionary<String, String> queryData)
        {
            url = SetQueryData(url, queryData);
            var jsonResponse = GetRequest(this.BaseUrl + url);
            return jsonResponse;
        }

        /// <summary>
        /// generic post for URL only, no json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        public T Post<T>(String url)
        {
            var jsonResponse = PostRequest(BaseUrl + url);
            return JsonConvert.DeserializeObject<T>(jsonResponse);
        }

        public T Update<T>(String url, T data)
        {
            String jsonRequest = JsonConvert.SerializeObject(data);
            var jsonResponse = PutRequest(BaseUrl + url, jsonRequest);
            return JsonConvert.DeserializeObject<T>(jsonResponse);
        }

        public string UpdateAsJson<T>(String url, T data)
        {
            String jsonRequest = JsonConvert.SerializeObject(data);
            var jsonResponse = PutRequest(BaseUrl + url, jsonRequest);
            return jsonResponse;

        }

        public T Create<T>(String url, T data)
        {
            String jsonRequest = JsonConvert.SerializeObject(data);
            var jsonResponse = PostRequest(BaseUrl + url, jsonRequest);
            return JsonConvert.DeserializeObject<T>(jsonResponse);
        }

        public string CreateAsJson<T>(String url, T data)
        {
            String jsonRequest = JsonConvert.SerializeObject(data);
            var jsonResponse = PostRequest(BaseUrl + url, jsonRequest);
            return jsonResponse;
        }

        protected string PostFormData(string url, string formData)
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = formData.Length;

            StreamWriter stOut = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            stOut.Write(formData);
            stOut.Close();
            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }
        }

        protected String DeleteRequest(String url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "DELETE";
            request.Headers.Add("username", "fitnessGenes");
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();


            var response = request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }

        }

        protected String GetRequest(String url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            request.Headers.Add("username", "fitnessGenes");

            if (Headers.Count() > 0)
            {
                foreach (var header in Headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            //calling thread should await this call and continue processing
            WebResponse response = request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }
        }

        protected String PostRequest(String url, String jsonPostData = null)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("username", "fitnessGenes");

            if (Headers.Count() > 0)
            {
                foreach (var header in Headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            request.ContentType = @"application/json";
            UTF8Encoding encoding = new UTF8Encoding();
            if (!String.IsNullOrWhiteSpace(jsonPostData))
            {
                Byte[] byteArray = encoding.GetBytes(jsonPostData);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
            }
            else
            {
                request.ContentLength = 0;
            }


            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }

        }

        protected String PutRequest(String url, String jsonPostData)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "PUT";
            request.Headers.Add("username", "fitnessGenes");
            if (Headers.Count() > 0)
            {
                foreach (var header in Headers)
                {
                    request.Headers.Add(header.Key, header.Value);
                }
            }

            if (!string.IsNullOrEmpty(jsonPostData))
            {
                UTF8Encoding encoding = new UTF8Encoding();
                Byte[] byteArray = encoding.GetBytes(jsonPostData);
                request.ContentLength = byteArray.Length;
                request.ContentType = @"application/json";
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
            }

            var response = request.GetResponse();
            using (var responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                return reader.ReadToEnd();
            }

        }

        public String SetQueryData(string url, Dictionary<String, String> queryData)
        {
            if (queryData != null && queryData.Any())
            {
                String queryParams = "";
                foreach (var queryKey in queryData.Keys)
                {
                    if (queryParams == "")
                    {
                        queryParams += "?";
                    }
                    else
                    {
                        queryParams += "&";
                    }
                    queryParams += queryKey + "=" + queryData[queryKey];
                }
                url += queryParams;
            }

            return url;
        }


    }
}
