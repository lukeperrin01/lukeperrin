﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LeisureInsure.DAL.LeisureInsure.Quotes;
using LeisureInsure.Models.Quotes;
using LeisureInsure.Utilities;
using LeisureInsure.Services.Charges.Utilities;

namespace LeisureInsure.Services.Charges.Policies.OtherActivities
{
    public class PublicLiability : ICalculateCoverCharges
    {
        public decimal Calculate(List<ChargeCalculationAnswer> answers, PolicyInformation policyInformation, List<Rate> rates)
        {
            var indemnity = answers.FirstOrDefault(x => x.InputId == Inputs.LevelOfIndemnity);



            if (indemnity == null)
            {
                throw new ApplicationException("Please ensure that there is an indemnity selected for this cover");
            }


            var indemnityRate = rates.FirstOrDefault(x => x.Id == indemnity.RateId);

            var annualTurnoverAnswer = answers.FirstOrDefault(x => x.InputId == Inputs.AnnualTurnover);
            if (annualTurnoverAnswer == null)
            {
                throw new ApplicationException("Please ensure that there is an annual turnover input for this cover");
            }
            var annualTurnover = annualTurnoverAnswer.RateValue.ToSafeDecimal();

            var professionalLiability = answers.FirstOrDefault(x => x.InputId == Inputs.ProfessionalLiability);



            if (professionalLiability == null)
            {
                throw new ApplicationException("Please ensure that there is an professional Liability selected for this cover");
            }
            var professionalLiabilityRate = rates.FirstOrDefault(x => x.Id == professionalLiability.RateId);

            var premium = professionalLiabilityRate.PremiumWithMinimum(annualTurnover);

            var activities = answers.Where(x => x.InputId == Inputs.HazardQuestions).ToList();

            foreach(var activity in activities)
            {
                var rate = rates.FirstOrDefault(x => x.Id == activity.RateId);
                premium += rate.PremiumWithMinimum(annualTurnover);
            }

            premium += indemnityRate.PremiumWithMinimum(premium);

            return premium;
        }





    }
}