﻿
var navScreen = function (s) {
    navScreen = s;
}
//$(document).ready(function () {
//    alert('hit');
//    $('[data-toggle="popover"]').popover();
//});
//function save(s) {
//    document.getElementById("topLevel").setAttribute("data-save", s)
//}
function setLocale(l) {
    $.get("/home/SetLocale", {_locale:l});
};

function getDate(e, y) {
    var date = new Date(e);
    var jdate = parseInt(date.getFullYear() + parseInt(y) + "" + (date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1)
        + ("" + date.getDate() < 10 ? "0" + date.getDate() : date.getDate()));
    return (jdate);
};
function getDateExp(e) {

    var date = new Date(e);
    //alert(date.getFullYear);
    date.setFullYear(parseInt(e.getFullYear + 1));
    return (date);
};

function tam() {
    var q = 1; //qpk();
    $.get("/TAM/TAM", { _qPK: q });
};
function testURL() {

    $.get("/TAM/TestAttach");
}

function cert(q) {
    //var q = qpk();
    //var q = 1;
    //alert(q);
    //document.getElementById("pdfCert").setAttribute("href", "/pdf/GetCertificate/?_quotePK=" + q);
    $.get("/pdf/CreateCert", { _quotePK: q });
};
function getcert() {
    var q = qpk();
    $.get("/pdf/GetCertificate", { _quotePK: 1 });
};
function l(u, p) {
    $.getJSON("/home/Login", { u: u, p: p }, function (data) {
        document.getElementById("topLevel").setAttribute("data-contact", data.contactPK)

    });
};
function TestEmail() {
    $.get("/home/TestEmail");
}
function SendEmail(e) {
    $.get("/home/SendEmail", {_quotePK:e});
}

function quoteCharges() {
    var qti = getQuoteTypeInput();
    var qi = getQuoteInput();
    var charges = [];
    $.ajax({
        url: 'Home/CreateQuoteDetails',
        data: JSON.stringify({ _QTI: qti, _QI: qi }),
        contentType: 'application/json',
        type: 'POST',
        success: function (data) {
            $.each(data, function (key, val) {
                charges.push({ quoteFK: val.quoteFK, coverFK: val.coverFK, coverName: val.coverName, lineType: val.lineType, base: val.base, rate: val.rate, charge: val.charge, runningTotal: val.runningTotal });
            });
        }
    })
    return charges;
};

function editCover(e) {
    var x = e.getAttribute("data-coverPK");
    var e = '#' + x + '-collapse';
    $(e).collapse('toggle');
}

function getQuoteTypeInput(s) {
    var sa = s;
    var s = document.getElementById("topLevel").getAttribute("data-quotepk");
    var loc = document.getElementById("flagx").getAttribute("data-loc");
    var contact = document.getElementById("topLevel").getAttribute("data-contact")
    var policyPK = document.getElementById("policyPK").getAttribute("data-policyPK");
    var postCode = document.getElementById("postCode").getAttribute("data-postcode");
    var dateEff = document.getElementById("dateEff").getAttribute("data-dateeff");
    var dateExp = document.getElementById("dateExp").getAttribute("data-dateexp");
    var qti = [];
    qti.push({ SaveLevel: sa, ContactPK: contact, PolicyFK: policyPK, loc: loc, PCode: postCode, DateEff: dateEff, DateExp: dateExp });
    return qti;
};

function getQuoteInput() {
    var qi = [];
    var premiums = $('#premiums');
    premiums.find('.coverHeader').each(function (i, element) {
        coverpk = $(this).data("coverheaderpk");
        cover = $(this);
        cover.find('.selectRate').each(function (i, element) {

            if ($('.selectRate').length > 0 && $('option:selected', this).val() > 0) {
                var selectRate = $('option:selected', this).val();
                cover.find('.suminsured').each(function (i, element) {
                    if ($(this).val() > 0) {
                        var selectval = +$(this).val();
                        qi.push({ coverFK: coverpk, rateFK: selectRate, rateValue: selectval, rateString: 'x' });
                    };
                })
            };
        });

        cover.find('.selectindemn').each(function (i, element) {
            var coverpk = $(this).data("coverpk");
            if ($('.selectindemn').length > 0 && $('option:selected', this).val() > 0) {
                var selectRate = $('option:selected', this).val();
                qi.push({ coverFK: coverpk, rateFK: selectRate, rateValue: 0, rateString: 'x' });
            }
        });
        cover.find('.selectHaz').each(function (i, element) {
            var coverpk = $(this).data("coverpk");
            if ($('.selectRate').length > 0 && $('option:selected', this).val() > 0) {
                var selectRate = $('option:selected', this).val();
                qi.push({ coverFK: coverpk, rateFK: selectRate, rateValue: 0, rateString: 'x' });
            }
        });
        cover.find('.suminsured').each(function (i, element) {
            var coverpk = $(this).data("coverpk");
            var ratepk = $(this).data("ratepk");
            var val = $(this).val();
            var strv = "x";
            var intv = 0;
            if (val > 0 && ratepk != 111) {
                if ($.isNumeric(val)) {
                    intv = parseInt(val);
                }
                else {
                    strv = val;
                };
                qi.push({ coverFK: coverpk, rateFK: ratepk, rateValue: intv, rateString: strv });
            }
        });
    });
    return qi;
};

function ZgetQuoteInput() {

    var qi = [];
    var premiums = $('#premiums');

    premiums.find('.coverHeader').each(function (i, element) {
        coverpk = $(this).data("coverheaderpk");
        //alert(coverpk);
        cover = $(this);
        cover.find('.selectRate').each(function (i, element) {

            if ($('.selectRate').length > 0 && $('option:selected', this).val() > 0) {
                var selectRate = $('option:selected', this).val();
                cover.find('.suminsured').each(function (i, element) {
                    if ($(this).val() > 0) {
                        var selectval = +$(this).val();
                        qi.push({ coverFK: coverpk, rateFK: selectRate, rateValue: selectval, rateString: 'x' });
                    };
                })
            };
        });

        cover.find('.selectindemn').each(function (i, element) {
            var coverpk = $(this).data("coverpk");
            if ($('.selectindemn').length > 0 && $('option:selected', this).val() > 0) {
                var selectRate = $('option:selected', this).val();
                qi.push({ coverFK: coverpk, rateFK: selectRate, rateValue: 0, rateString: 'x' });
            }
        });
        cover.find('.selectHaz').each(function (i, element) {
            var coverpk = $(this).data("coverpk");
            if ($('.selectRate').length > 0 && $('option:selected', this).val() > 0) {
                var selectRate = $('option:selected', this).val();
                qi.push({ coverFK: coverpk, rateFK: selectRate, rateValue: 0, rateString: 'x' });
            }
        });
        cover.find('.suminsured').each(function (i, element) {
            var coverpk = $(this).data("coverpk");
            var ratepk = $(this).data("ratepk");
            var val = $(this).val();
            var strv = "x";
            var intv = 0;
            if (val > 0 && ratepk != 111) {
                if ($.isNumeric(val)) {
                    intv = parseInt(val);
                }
                else {
                    strv = val;
                };
                qi.push({ coverFK: coverpk, rateFK: ratepk, rateValue: intv, rateString: strv });

            }
        });
    });

    return qi;
};

function getQuoteSummary(cpk) {
    var covers = [];
    var ratepk = "";
    var rateName = "";
    var ratevalue = "";
    var indemnratepk = 0;
    var indemnrateValue = 0;
    var valsum = 0;
    var selectRatepk = 0;
    var selectRateName = "";
    var indemnrateValue = "";
    var coverpk = 0;
    var cover = "";
    var allRates = "";
    var premiums = $('#premiums');
    premiums.find('.coverHeader').each(function (i, element) {

        coverpk = $(this).data("coverheaderpk");
        if (cpk == coverpk || cpk == -1) {
            cover = $(this);
            cover.find('.selectindemn').each(function (i, element) {
                indemnratepk = $('option:selected', this).val();
                indemnrateValue = $('option:selected', this).text();
            });
            cover.find('.indemn').each(function (i, element) {
                indemnratepk = $(this).data("ratepk");
                indemnrateValue = $(this).data("ratevalue")
            });
            cover.find('.selectRate option:selected').each(function (i, element) {
                ratepk = $(this).val();
                rateName = $(this).text();
                allRates += rateName + ": ";


                cover.find('.suminsured').each(function (i, element) {
                    val = $(this).val();
                    if (val > 0) {

                        valsum = val;
                        covers.push({ coverPK: coverpk, ratePK: ratepk, rateName: rateName, rateVal: val, IndemnPK: indemnratepk, indemnVal: indemnrateValue, allRates: allRates, valsum: valsum });

                    };

                });


            });
            cover.find('.suminsured').each(function (i, element) {

                ratepk = $(this).data("ratepk");
                rateName = $(this).data("ratename");

                val = $(this).val();
                if (val > 0 && coverpk != 1) {

                    allRates += rateName + ": ";

                    valsum = valsum + parseInt(val);
                    covers.push({ coverPK: coverpk, ratePK: ratepk, rateName: rateName, rateVal: val, IndemnPK: indemnratepk, indemnVal: indemnrateValue, allRates: allRates, valsum: valsum });

                    indemnratepk = 0;
                    indemnrateValue = "";
                    val = 0;
                }

            });
        }
    });
    return covers;
};
function addContact(c) {
    //var c = $('#_Customer');
    //var serform = c.serialize();
    _contact = c;
    //alert("hit helper");
    $.get("/Home/SaveCustomer", { _contact: _contact },
        function () {
        });
};

function GetAddress() {
    var p = $('#PCode').val();
    //alert(p);
    $.getJSON("/Home/GetAddress", { _pCode: p }, function (data) {
        var ddl = $('#_adds');
        ddl.empty();
        $(data).each(function () {
            ddl.append(
                $('<option/>', {
                    value: this.id,
                    selected: false
                }).html(this.name)
            );
        });
    });
    return false;
};

function getAdd() {
    event.preventDefault();

    var p = $('#postCodex').val();

    $.getJSON("/Home/GetAddress", { _pCode: p }, function (data) {
        var ddl = $('#_adds');
        ddl.empty();
        $(data).each(function () {
            ddl.append(
                $('<option/>', {
                    value: this.id,
                    selected: false
                }).html(this.name)
            );
        });
    });
};

function popAdd() {
    var p = $('#_adds :Selected').val();
    $.getJSON("/Home/_updateAdd", { _pcodeID: p }, function (data) {
        $('#Address_Address1').val(data.Address1)
        $('#Address_Address2').val(data.Address2)
        $('#Address_Address3').val(data.Address3)
        $('#Address_Town').val(data.Town)
        $('#Address_County').val(data.County)
    });
    function testAtt(e) {
        //alert(e);
        return e;
    }


}

function addPayment() {
    //alert("pay");
    //var c = $('#_payment');
    var serform = c.serialize();
    $.post("/Payment/SendPayment", serform,
        function () {
        });
};
function getPaymentDetails() {
    //alert("pay");
    //var c = $('#_payment');
    //var serform = c.serialize();
    $.get("/Payment/PaymentDetails");
    
};
