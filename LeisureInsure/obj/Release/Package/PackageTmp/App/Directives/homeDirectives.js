﻿/// <reference path="../html/Certificate/certCoverPages.html" />

angular.module("App").directive("toggle", function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            if (attrs.toggle === "tooltip") {
                $(element).tooltip();
            }
            if (attrs.toggle === "popover") {
                $(element).popover();
            }
        }
    };
});

angular.module("App").directive("ngPdf", function () {
    return {
        restrict: "AEC",
        require: "ngModel",
        templateUrl: "app/html/Certificate/certCoverPages.html",
        controller: "pdfCtrl"
    }
});

app.directive('ngAccHeaderOne', function () {
    return {
        templateUrl: function (element, attribute) {
            return '/App/StaticPages/accordion-group.html';
        }
    };
});
app.directive('ngAccHeaderOne', function () {
    return {
        templateUrl: function (element, attribute) {
            return '/App/StaticPages/accordion-group.html';
        }
    };
});


angular.module("App").directive("datepickerPopup", ["dateFilter", "uibDatepickerPopupConfig", "uibDatepickerConfig",
    function (dateFilter, uibDatepickerPopupConfig, uibDatepickerConfig) {
        uibDatepickerPopupConfig.showButtonBar = true;
        uibDatepickerConfig.showWeeks = false;
        
        return {
            restrict: "AEC",
            priority: 1,
            require: "ngModel",
            isOpen: true,
            open: function() {
                this.isOpen = true;
            },
            link: function (scope, element, attr, ngModel) {
                ngModel.$formatters.push(function (value) {
                    return dateFilter(value, "yyyy-MM-dd");
                });
            }
        };
    }]);




