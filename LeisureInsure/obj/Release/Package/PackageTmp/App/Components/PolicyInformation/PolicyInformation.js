/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var Components;
    (function (Components) {
        var PolicyInformationController = /** @class */ (function () {
            function PolicyInformationController(coverService, policyService, policyName, coverName) {
                this.coverService = coverService;
                this.policyService = policyService;
                this.policyName = policyName;
                this.coverName = coverName;
            }
            PolicyInformationController.prototype.policyInformationTemplate = function () {
                var policyId = this.policyService.selectedPolicyId;
                var policyName = this.policyName.nameForId(policyId);
                var url = "/app/components/policyinformation/html/policies/" + policyName + "/policy.html";
                return url;
            };
            PolicyInformationController.prototype.coverInformationTemplate = function () {
                var coverId = this.coverService.selectedCoverId;
                var policyId = this.policyService.selectedPolicyId;
                var policyName = this.policyName.nameForId(policyId);
                var coverName = this.coverName.nameForId(coverId);
                var url = "/app/components/policyinformation/html/policies/" + policyName + "/" + coverName + ".html";
                return url;
            };
            PolicyInformationController.$inject = ["CoverService", "PolicyService", "PolicyNameService", "CoverNameService"];
            return PolicyInformationController;
        }());
        Components.PolicyInformationController = PolicyInformationController;
        angular.module("App").controller("PolicyInformationController", PolicyInformationController);
    })(Components = LeisureInsure.Components || (LeisureInsure.Components = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=PolicyInformation.js.map