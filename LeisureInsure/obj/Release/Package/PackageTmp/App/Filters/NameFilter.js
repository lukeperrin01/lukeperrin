/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    function nameFilter() {
        return function (input) {
            if (!input) {
                return input;
            }
            var lowerCase = input.toLowerCase();
            var words = lowerCase.split(" ", undefined);
            var capitalizedWords = new Array();
            _.each(words, function (word) {
                var newWord = word.substr(0, 1).toUpperCase() + word.substr(1);
                capitalizedWords.push(newWord);
            });
            return capitalizedWords.join(" ");
        };
    }
    LeisureInsure.nameFilter = nameFilter;
    angular.module("App").filter("name", nameFilter);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=NameFilter.js.map