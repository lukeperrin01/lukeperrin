/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    function safeUrl($sce) {
        return function (input) {
            return $sce.trustAsResourceUrl(input);
        };
    }
    LeisureInsure.safeUrl = safeUrl;
    angular.module("App").filter("safeUrl", ["$sce", safeUrl]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=SafeUrl.js.map