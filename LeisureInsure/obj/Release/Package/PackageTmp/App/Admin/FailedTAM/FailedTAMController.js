var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var FailedTAMController = /** @class */ (function () {
            function FailedTAMController($http, $q, $scope, $rootScope, $window, quoteStorageService) {
                this.http = $http;
                this.q = $q;
                this.scope = $scope;
                this.scope.alerts = [];
                this.window = $window;
                this.rootScope = $rootScope;
                this.getFailedTAM();
                this.quoteStorageService = quoteStorageService;
            }
            FailedTAMController.prototype.getFailedTAM = function () {
                var _this = this;
                this.http({
                    url: "/api/v1/admin/failedTAM",
                    method: "POST"
                })
                    .success(function (data) {
                    _this.quotes = data;
                });
            };
            FailedTAMController.prototype.addToTam = function (quotePK) {
                this.quoteStorageService.addToTam(quotePK);
            };
            FailedTAMController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/failedTAM", {
                    controller: "FailedTAMController",
                    templateUrl: "/app/admin/failedTAM/FailedTAM.html",
                    controllerAs: "failedTAM",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            };
            FailedTAMController.$inject = ["$http", "$q", "$scope", "$rootScope", "$window", "QuoteStorageService"];
            return FailedTAMController;
        }());
        Admin.FailedTAMController = FailedTAMController;
        angular.module("LeisureInsure.Admin")
            .controller("FailedTAMController", FailedTAMController)
            .config(["$routeProvider", FailedTAMController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=FailedTAMController.js.map