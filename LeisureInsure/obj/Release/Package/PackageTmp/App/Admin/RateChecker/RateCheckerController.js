var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var RateCheckerController = /** @class */ (function () {
            function RateCheckerController($http, $q, $scope, $rootScope, $window, rateUpdaterService) {
                var _this = this;
                this.http = $http;
                this.q = $q;
                this.scope = $scope;
                this.scope.alerts = [];
                this.window = $window;
                this.rootScope = $rootScope;
                this.rateUpdater = rateUpdaterService;
                this.updated = false;
                this.updatedLocale = false;
                this.rateMessage = "";
                this.localeMessage = "";
                $("#allGifs").css("display", "block");
                this.rateUpdater.GetPoliciesCoversRates().then(function (data) {
                    _this.policies = data;
                });
                this.rateUpdater.GetLocales().then(function (data) {
                    _this.locales = data;
                });
            }
            RateCheckerController.prototype.Debug = function () {
                console.log(this.selectedPolicy, "selectedpolicy");
                console.log(this.selectedCover, "selectedcover");
                console.log(this.selectedRate, "selectedrate");
            };
            RateCheckerController.prototype.updateRate = function () {
                var _this = this;
                if (this.selectedRate != null) {
                    this.rateUpdater.UpdateRate(this.selectedRate).then(function (data) {
                        _this.updated = true;
                        _this.updatedRate = data.rateName;
                        _this.rateUpdater.GetPoliciesCoversRates();
                    });
                }
                else {
                    this.rateMessage = "Please select a rate!";
                }
            };
            RateCheckerController.prototype.updateLocale = function () {
                var _this = this;
                if (this.selectedLocale != null) {
                    this.rateUpdater.UpdateLocale(this.selectedLocale).then(function (data) {
                        _this.updatedLocale = true;
                        _this.updatedLocaleName = data.country;
                        _this.rateUpdater.GetPoliciesCoversRates();
                    });
                }
                else {
                    this.localeMessage = "Please select a locale!";
                }
            };
            RateCheckerController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/ratechecker", {
                    controller: "RateCheckerController",
                    templateUrl: "/app/admin/ratechecker/ratechecker.html",
                    controllerAs: "RateCheckerController",
                    metadata: {
                        title: "Leisure Insure - RateChecker"
                    }
                });
            };
            RateCheckerController.$inject = ["$http", "$q", "$scope", "$rootScope", "$window", "RateUpdaterService"];
            return RateCheckerController;
        }());
        Admin.RateCheckerController = RateCheckerController;
        angular.module("LeisureInsure.Admin")
            .controller("RateCheckerController", RateCheckerController)
            .config(["$routeProvider", RateCheckerController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//function openTab(evt, tabName) {
//    // Declare all variables
//    var i, tabcontent, tablinks;
//    // Get all elements with class="tabcontent" and hide them
//    tabcontent = document.getElementsByClassName("tabcontent");
//    for (i = 0; i < tabcontent.length; i++) {
//        tabcontent[i].style.display = "none";
//    }
//    // Get all elements with class="tablinks" and remove the class "active"
//    tablinks = document.getElementsByClassName("tablinks");
//    for (i = 0; i < tablinks.length; i++) {
//        tablinks[i].className = tablinks[i].className.replace(" active", "");
//    }
//    // Show the current tab, and add an "active" class to the button that opened the tab
//    document.getElementById(tabName).style.display = "block";
//    evt.currentTarget.className += " active";
//} 
//# sourceMappingURL=RateCheckerController.js.map