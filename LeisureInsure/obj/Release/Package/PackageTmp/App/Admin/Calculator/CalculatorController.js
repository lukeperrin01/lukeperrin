/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../../scripts/typings/underscore/underscore.d.ts" />
/// <reference path="../../../scripts/typings/angularjs/angular-route.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var CalculatorController = /** @class */ (function () {
            function CalculatorController($http) {
                var _this = this;
                this.http = $http;
                this.http.get("api/v1/calculators/policies").then(function (response) {
                    _this.policies = angular.copy(response.data);
                });
                this.http.get("api/v1/calculators/covers").then(function (response) {
                    _this.covers = angular.copy(response.data);
                });
            }
            CalculatorController.prototype.policyFlag = function () {
                return _.reduce(this.selectedPolicies, function (memo, iteratee) { return (memo | iteratee.bitwiseId); }, 0);
            };
            CalculatorController.prototype.coverFlag = function () {
                return _.reduce(this.selectedCovers, function (memo, iteratee) { return (memo | iteratee); }, 0);
            };
            ;
            CalculatorController.prototype.copyPolicy = function () {
                var value = document.getElementById("policyCopy");
                this.copyToClipboard(value);
            };
            CalculatorController.prototype.copyCover = function () {
                var value = document.getElementById("coverCopy");
                this.copyToClipboard(value);
            };
            CalculatorController.prototype.copyToClipboard = function (target) {
                // select the content
                target.focus();
                target.setSelectionRange(0, target.value.length);
                // copy the selection
                var succeed;
                try {
                    succeed = document.execCommand("copy");
                }
                catch (e) {
                    succeed = false;
                }
                return succeed;
            };
            CalculatorController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/calculator", {
                    controller: "CalculatorController",
                    templateUrl: "/app/admin/calculator/calculator.html",
                    controllerAs: "calculator",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            };
            CalculatorController.$inject = ["$http"];
            return CalculatorController;
        }());
        Admin.CalculatorController = CalculatorController;
        angular.module("LeisureInsure.Admin")
            .controller("CalculatorController", CalculatorController)
            .config(["$routeProvider", CalculatorController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=CalculatorController.js.map