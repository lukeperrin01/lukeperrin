/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
"use strict";
var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        angular.module("LeisureInsure.Admin", ["ui.tinymce"]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=app.js.map