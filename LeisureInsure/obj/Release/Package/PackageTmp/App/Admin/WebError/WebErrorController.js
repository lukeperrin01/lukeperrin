var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var WebErrorController = /** @class */ (function () {
            function WebErrorController($http, $q, $scope, $rootScope, $window, QuoteStorageService) {
                this.http = $http;
                this.q = $q;
                this.scope = $scope;
                this.scope.alerts = [];
                this.window = $window;
                this.rootScope = $rootScope;
                this.quoteStorageService = QuoteStorageService;
                this.getWebErrors();
            }
            WebErrorController.prototype.getWebErrors = function () {
                var _this = this;
                this.http({
                    url: "/api/v1/admin/getErrors",
                    method: "GET"
                })
                    .success(function (data) {
                    _this.webErrors = data;
                });
            };
            WebErrorController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/weberror", {
                    controller: "WebErrorController",
                    templateUrl: "/app/admin/weberror/errorlog.html",
                    controllerAs: "weberrorController",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            };
            WebErrorController.$inject = ["$http", "$q", "$scope", "$rootScope", "$window", "QuoteStorageService"];
            return WebErrorController;
        }());
        Admin.WebErrorController = WebErrorController;
        angular.module("LeisureInsure.Admin")
            .controller("WebErrorController", WebErrorController)
            .config(["$routeProvider", WebErrorController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=WebErrorController.js.map