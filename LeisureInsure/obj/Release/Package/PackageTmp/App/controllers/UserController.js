/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/angular-ui-bootstrap/angular-ui-bootstrap.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var UserController = /** @class */ (function () {
        function UserController($rootScope, authenticationService, $uibModal, $location) {
            var _this = this;
            this.modalService = $uibModal;
            this.location = $location;
            this.authenticationService = authenticationService;
            this.authenticationService.onLoginStatusChanged($rootScope, function () {
                _this.update();
            });
            this.update();
        }
        UserController.prototype.showLogin = function () {
            var _this = this;
            this.modalInstance = this.modalService.open({
                animation: true,
                templateUrl: "login-dialog.html",
                controller: "LoginController",
                controllerAs: "modal"
            });
            this.modalInstance.result.then(function () {
                _this.update();
            });
        };
        UserController.prototype.logout = function () {
            this.authenticationService.logout();
            this.update();
            if (this.location.path().indexOf('/brokerQuotes') > -1) {
                this.location.path('https://leisureinsure.co.uk/');
            }
            if (this.location.path().indexOf('/brokerDetails') > -1) {
                this.location.path('https://leisureinsure.co.uk/');
            }
            if (this.location.path().indexOf('/brokerPortal') > -1) {
                this.location.path('https://leisureinsure.co.uk/');
            }
        };
        UserController.prototype.update = function () {
            this.name = this.authenticationService.name;
            this.isAuthenticated = this.authenticationService.isAuthenticated();
        };
        UserController.$inject = ["$rootScope", "AuthenticationService", "$uibModal", "$location"];
        return UserController;
    }());
    LeisureInsure.UserController = UserController;
    var LoginController = /** @class */ (function () {
        function LoginController($location, $rootScope, authenticationService, $uibModalInstance) {
            this.authenticationService = authenticationService;
            this.modalInstance = $uibModalInstance;
            this.rootScope = $rootScope;
            this.location = $location;
            $("#allGifs").css("display", "block");
        }
        LoginController.prototype.login = function () {
            var _this = this;
            this.rootScope.loggingin = true;
            this.authenticationService.login(this.username, this.password).then(function (response) {
            }).then(function () {
                _this.modalInstance.close();
                _this.rootScope.loggingin = false;
            }, function () {
                _this.errorMessage = "We could not validate the username and password entered. Please try again or contact us on 01993 700 761";
                _this.rootScope.loggingin = false;
            });
        };
        LoginController.prototype.cancel = function () {
            this.modalInstance.close();
        };
        LoginController.$inject = ["$location", "$rootScope", "AuthenticationService", "$uibModalInstance"];
        return LoginController;
    }());
    LeisureInsure.LoginController = LoginController;
    angular.module("App")
        .controller("LoginController", LoginController)
        .controller("UserController", UserController);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=UserController.js.map