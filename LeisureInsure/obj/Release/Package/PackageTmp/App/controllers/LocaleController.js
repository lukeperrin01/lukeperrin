/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var LocaleController = /** @class */ (function () {
        function LocaleController($rootScope, locationService, policyUpdateDataService, $location) {
            var _this = this;
            this.location = $location;
            this.localeService = locationService;
            locationService.initialised.then(function (service) {
                //service.getlocals("");
                //this.locales = this.localeService.locales;
                //this.selectedLocale = this.localeService.selectedLocale;
                _this.locales = service.locales;
                _this.selectedLocale = service.selectedLocale;
            });
            this.localeService.onSelectedLocaleChanged($rootScope, function (locale) {
                _this.selectedLocale = _this.localeService.selectedLocale;
            });
            this.policyUpdateDataService = policyUpdateDataService;
            this.irelandMessage = false;
        }
        LocaleController.prototype.brokerPortal = function () {
            alert("HIT");
            this.location.path("\brokerPortal");
        };
        LocaleController.prototype.selectLocale = function (locale) {
            this.selectedLocale = locale;
            this.localeService.getlocales(locale.strLocale, 1);
            //this.policyUpdateDataService.initialise(this.selectedLocale.strLocale, 0, 0);
        };
        LocaleController.prototype.ClickedFlag = function () {
            if (this.selectedLocale.showFlag)
                ShowDialog();
        };
        LocaleController.prototype.SelectUK = function () {
            this.irelandMessage = false;
            var uklocale = this.locales[0];
            this.selectedLocale = uklocale;
            this.localeService.getlocales(uklocale.strLocale, 1);
            //this.policyUpdateDataService.initialise(uklocale.strLocale, 0, 0);
            HideDialog();
        };
        LocaleController.prototype.SelectIreland = function () {
            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                var eirlocale = this.locales[1];
                this.selectedLocale = eirlocale;
                this.localeService.getlocales(eirlocale.strLocale, 1);
                HideDialog();
            }
            else
                this.irelandMessage = true;
        };
        LocaleController.routing = function ($routeProvider) {
            $routeProvider.when("/brokerPortal", {
                controller: "BrokerPortalController",
                templateUrl: "/app/brokerportal/brokerPortal.html",
                controllerAs: "BrokerPortalController",
                metadata: {
                    title: "Broker Portal",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        LocaleController.$inject = ["$rootScope", "LocationService", "PolicyUpdateDataService", "$location"];
        return LocaleController;
    }());
    LeisureInsure.LocaleController = LocaleController;
    angular.module("App").controller("LocaleController", LocaleController);
})(LeisureInsure || (LeisureInsure = {}));
function ShowDialog() {
    $("#countryDialog").css("display", "block");
}
function HideDialog() {
    $("#countryDialog").css("display", "none");
}
//# sourceMappingURL=LocaleController.js.map