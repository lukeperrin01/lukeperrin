var LeisureInsure;
(function (LeisureInsure) {
    var StaticPageLoginController = /** @class */ (function () {
        function StaticPageLoginController($location, $rootScope, authenticationService) {
            this.authenticationService = authenticationService;
            this.rootScope = $rootScope;
            this.location = $location;
        }
        StaticPageLoginController.prototype.login = function () {
            var _this = this;
            this.authenticationService.login(this.username, this.password).then(function (response) {
                var redirectUrl = _this.rootScope.redirectUrl;
                if (redirectUrl) {
                    _this.location.path(redirectUrl);
                }
                else {
                    _this.location.path("/");
                }
            }, function () {
                _this.errorMessage = "We could not validate the username and password entered. Please try again or contact us on 01993 700 761";
            });
        };
        StaticPageLoginController.$inject = ["$location", "$rootScope", "AuthenticationService"];
        return StaticPageLoginController;
    }());
    LeisureInsure.StaticPageLoginController = StaticPageLoginController;
    angular.module("App")
        .controller("StaticPageLoginController", StaticPageLoginController);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=StaticPageLoginController.js.map