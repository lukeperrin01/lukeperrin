/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var MetadataController = /** @class */ (function () {
        function MetadataController(metadataService) {
            this.metadataService = metadataService;
        }
        MetadataController.prototype.title = function () { return this.metadataService.title; };
        MetadataController.prototype.description = function () { return this.metadataService.description; };
        MetadataController.prototype.keywords = function () { return this.metadataService.keywords; };
        MetadataController.prototype.ogUrl = function () { return this.metadataService.ogUrl; };
        MetadataController.prototype.ogTitle = function () { return this.metadataService.ogTitle; };
        MetadataController.prototype.ogDescription = function () { return this.metadataService.ogDescription; };
        MetadataController.prototype.ogType = function () { return this.metadataService.ogType; };
        MetadataController.prototype.ofImage = function () { return this.metadataService.ofImage; };
        MetadataController.prototype.twitterCard = function () { return this.metadataService.twitterCard; };
        MetadataController.prototype.twitterTitle = function () { return this.metadataService.twitterTitle; };
        MetadataController.prototype.twitterDescription = function () { return this.metadataService.twitterDescription; };
        MetadataController.prototype.twitterSite = function () { return this.metadataService.twitterSite; };
        MetadataController.prototype.twitterImage = function () { return this.metadataService.twitterImage; };
        MetadataController.$inject = ["MetadataService"];
        return MetadataController;
    }());
    LeisureInsure.MetadataController = MetadataController;
    angular.module("App").controller("MetadataController", MetadataController);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=MetadataController.js.map