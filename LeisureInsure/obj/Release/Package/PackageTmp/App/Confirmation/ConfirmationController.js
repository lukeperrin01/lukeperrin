/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var ConfirmationController = /** @class */ (function () {
        function ConfirmationController($http, $q, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies, locationService, chargeService, urlToNavigationService, metadataService, $timeout, commonDataService) {
            var _this = this;
            var initialisationPromises = [];
            var navigationData;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.commonDataService = commonDataService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            locationService.onSelectedLocaleChanged($scope, function (locale) {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            });
            //load existing quote *******************************************************
            if (commonDataService.policyData != null) {
                this.policyData = commonDataService.policyData;
                this.InitMinDetails();
            }
            else {
                if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                    var quoteRef = $routeParams["QUOTEREFERENCE"];
                    var password = $routeParams["PASSCODE"];
                    urlToNavigationService.GetLandingPageNew(quoteRef, password)
                        .then(function (data) {
                        _this.navigationData = data;
                        if (_this.navigationData.policyId > 0) {
                            metadataService.setMetadata(_this.navigationData.title, _this.navigationData.description, _this.navigationData.keywords);
                            if (_this.locationService.selectedLocale != null) {
                            }
                            _this.policyData = {};
                            _this.InitMinDetails();
                        }
                    });
                    this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then(function (data) {
                        _this.policyData = data;
                        _this.InitMinDetails();
                        if (_this.locationService.selectedLocale != null) {
                            _this.locationService.selectedLocale.showFlag = false;
                        }
                    });
                }
            } //****************************************** End Constructor *************************************  
        }
        //the minimum required to show the page, picture, policyname, locale       
        ConfirmationController.prototype.InitMinDetails = function () {
            if (this.navigationData != null) {
                this.policyData.policyImage = this.navigationData.pictureUrl;
                this.policyData.policyName = this.navigationData.title;
            }
            this.policyData.locale = this.locationService.selectedLocale;
            if (this.policyData.addresses == null)
                this.policyData.addresses = [];
            if (this.policyData.covers != null)
                this.policyData.covers = this.filter("orderBy")(this.policyData.covers, ["coverOrdinal"]);
        };
        ConfirmationController.prototype.Debug = function () {
            console.log("policyData", this.policyData);
        };
        ConfirmationController.routing = function ($routeProvider) {
            $routeProvider.when("/confirmation", {
                controller: "ConfirmationController",
                templateUrl: "/app/confirmation/confirmation.html",
                controllerAs: "ConfirmationController",
                metadata: {
                    title: "Leisure Insure Personal Details",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/confirmation/:QUOTEREFERENCE/:PASSCODE", {
                controller: "ConfirmationController",
                templateUrl: "/app/confirmation/confirmation.html",
                controllerAs: "ConfirmationController",
                metadata: {
                    title: "Leisure Insure Personal Details",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        ConfirmationController.$inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "CommonDataService", "PriceService", "StripeService"];
        return ConfirmationController;
    }());
    LeisureInsure.ConfirmationController = ConfirmationController;
    angular.module("App")
        .controller("ConfirmationController", ConfirmationController)
        .config(["$routeProvider", ConfirmationController.routing]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=ConfirmationController.js.map