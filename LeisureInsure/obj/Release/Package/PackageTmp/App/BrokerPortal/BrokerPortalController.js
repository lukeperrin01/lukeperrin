var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var BrokerPortalController = /** @class */ (function () {
            function BrokerPortalController($http, $scope, authenticationService, $rootScope, $location, $routeParams, PostCodeService, chargeService, $q) {
                this.authenticationService = authenticationService;
                this.rootScope = $rootScope;
                this.postCodeService = PostCodeService;
                this.chargeService = chargeService;
                this.http = $http;
                this.q = $q;
                this.scope = $scope;
                $("#allGifs").css("display", "block");
                this.scope.alerts = [];
                this.brokerName = authenticationService.name;
                this.location = $location;
                var link = "";
                if (window.location.href.indexOf("localhost") > -1) {
                    this.certlinkbase = "http://" + window.location.hostname + ":" + window.location.port;
                    this.quotelinkbase = "http://" + window.location.hostname + ":" + window.location.port;
                    this.checkoutlinkbase = "http://" + window.location.hostname + ":" + window.location.port;
                }
                else {
                    this.certlinkbase = "http://" + window.location.hostname;
                    this.quotelinkbase = "https://" + window.location.hostname;
                    this.checkoutlinkbase = "https://" + window.location.hostname;
                }
                this.getpolicies();
                if ($routeParams["quotePK"] !== undefined) {
                    var quotePK = $routeParams["quotePK"];
                    this.getQuote(quotePK);
                }
                if (this.location.path().indexOf('/brokerQuotes') > -1) {
                    this.getBrokerQuotes();
                }
                if (this.location.path().indexOf('/brokerDetails') > -1) {
                    this.getBrokerDetails();
                }
            }
            BrokerPortalController.prototype.getpolicies = function () {
                var _this = this;
                this.http({
                    url: "/api/v1/data/getPolicies",
                    method: "GET",
                    params: { strLocale: 0 }
                })
                    .success(function (data) {
                    _this.policies = data;
                })
                    .catch(function (data) {
                    // Error
                });
            };
            BrokerPortalController.prototype.submitCustomer = function () {
                var _this = this;
                var deferred = this.q.defer();
                var qr = this.quoteRef;
                this.quotePK = this.quote.id;
                var addx = [];
                var custx = [];
                this.customers[0].contactType = 1;
                custx.push(this.customers[0]);
                //var eType = this.customer[0].entityType;
                if (this.customers[0].entityType != "Customer" &&
                    (this.customers[0].entityName === undefined || this.customers[0].entityName === "")) {
                    deferred.reject("Please ensure that the entity name is completed");
                }
                else {
                    //if (this.customers.length > 1) {
                    //this.customer.splice(1, this.customer.length - 1);
                    var i = 0;
                    _.each(this.customers, function (cust) {
                        if (i == 0 && _this.contactEmail == '') {
                            _this.contactEmail = cust.email;
                        }
                        if (i == 0 && _this.contactName == '') {
                            _this.contactName = cust.firstName;
                        }
                        if (i > 0) {
                            //alert("MORE THAN ONE CUSTOMER");
                            cust.contactType = 1;
                            cust.quotePK = _this.customers[0].quotePK;
                            cust.entityType = "Customer";
                            custx.push(cust);
                        }
                        i++;
                        _this.instructors = i;
                    });
                    if (this.addresses.length > 1) {
                        //this.address.splice(1, this.address.length - 1);
                    }
                    var loc = 'en-GB';
                    if (this.quote.localeId == 2) {
                        loc = 'ga-IE';
                    }
                    this.addresses[0].quoteFK = this.quotePK;
                    this.addresses[0].addressType = 1;
                    this.addresses[0].locale = loc;
                    addx.push(this.addresses[0]);
                    this.contactDetail = {
                        contact: custx,
                        address: addx,
                        quoteReference: this.quote.quoteReference,
                        passCode: this.quote.password,
                        contactName: this.quote.contactName,
                        contactEmail: this.quote.contactEmail
                    };
                    this.chargeService.addCustomerDetails(this.contactDetail)
                        .then(function () {
                        deferred.resolve();
                    }, function (response) {
                        _this.scope.alerts.push({
                            type: "danger",
                            msg: response
                        });
                        deferred.reject(response);
                    });
                }
                document.cookie = "customers=" + JSON.stringify(this.customers);
                document.cookie = "addresses=" + JSON.stringify(this.addresses);
                var x = document.cookie;
                return deferred.promise;
            };
            BrokerPortalController.prototype.getQuote = function (quotePK) {
                var _this = this;
                this.http({
                    url: 'api/v1/brokerPortal/getQuote/{quotePK}',
                    method: "GET",
                    params: { quotePK: quotePK }
                })
                    .success(function (data) {
                    _this.quote = data;
                    if (_this.quote.addresses != null) {
                        _this.addresses = _this.quote.addresses;
                    }
                    else {
                        _this.addresses = [];
                    }
                    if (_this.quote.customers != null) {
                        _this.customers = _this.quote.customers;
                    }
                    else {
                        _this.customers = [];
                    }
                    _this.selectedPolicy = _.findWhere(_this.policies, {
                        id: _this.quote.policyId
                    });
                    _this.password = _this.quote.password;
                    _this.contactEmail = _this.quote.contactEmail;
                    _this.contactName = _this.quote.contactName;
                    _this.instructors = _this.quote.noInstructors;
                    _this.certlink = _this.certlinkbase + "/certificate/" + _this.quote.quoteReference + "/" + _this.quote.password;
                    _this.quotelink = _this.quotelinkbase + "/quote/" + _this.quote.quoteReference + "/" + _this.quote.password;
                    _this.checkoutlink = _this.checkoutlinkbase + "/checkout/" + _this.quote.quoteReference + "/" + _this.quote.password;
                })
                    .catch(function (data) {
                    // Error
                });
            };
            BrokerPortalController.prototype.getAddress = function () {
                var _this = this;
                if (this.addresses[0] != null) {
                    if (this.addresses[0].postcode != null) {
                        this.postCodeService.GetAddresses(this.addresses[0].postcode)
                            .then(function (addresses) {
                            _this.addressLookups = [];
                            _.each(addresses, function (address) {
                                var addy = {};
                                addy.addressID = address.addressID;
                                addy.address1 = address.addressLine1;
                                addy.address2 = address.addressLine2;
                                addy.town = address.town;
                                addy.county = address.county;
                                addy.postcode = address.postCode;
                                _this.addressLookups.push(addy);
                            });
                            //get first address and assign to our select, also update form
                            _this.selectedAddress = _this.addressLookups[0];
                            _this.addresses[0] = _this.selectedAddress;
                        });
                    }
                }
            };
            BrokerPortalController.prototype.getBrokerQuotes = function () {
                var _this = this;
                this.rootScope.searchingQuote = true;
                this.http({
                    url: "/api/v1/brokerPortal/GetBrokerQuotes/",
                    method: "GET",
                    params: { broker: this.brokerName, quoteRef: this.quoteRef }
                })
                    .success(function (data) {
                    _this.brokerQuotes = [];
                    _this.brokerQuotes = data;
                    _this.rootScope.searchingQuote = false;
                })
                    .catch(function (data) {
                    // Error
                    _this.rootScope.searchingQuote = false;
                });
            };
            BrokerPortalController.prototype.getBrokerDetails = function () {
                var _this = this;
                //alert(this.loggedIn);
                this.http.get("api/v1/brokerPortal/" + this.brokerName)
                    .then(function (response) {
                    _this.brokerDetails = response.data;
                }, function (response) {
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: "Could not find the broker"
                    });
                });
            };
            BrokerPortalController.prototype.updateBroker = function () {
                var _this = this;
                this.http.post("api/v1/brokerLive", {
                    locked: false,
                    entityName: this.brokerDetails.entityName,
                    firstname: this.brokerDetails.firstname,
                    surname: this.brokerDetails.lastname,
                    telephone: this.brokerDetails.telephone,
                    email: this.brokerDetails.email,
                    fsaNumber: this.brokerDetails.fsaNumber,
                    address1: this.brokerDetails.address1,
                    address2: this.brokerDetails.address2,
                    city: this.brokerDetails.city,
                    county: this.brokerDetails.county,
                    postcode: this.brokerDetails.postcode,
                    tamCode: this.brokerName
                })
                    .then(function () {
                    _this.scope.alerts.push({
                        type: "success",
                        msg: "Broker updated"
                    });
                    eval("ShowUIDialog('Details updated');");
                }, function (response) {
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: "There was a problem updating the broker: " + response.statusText
                    });
                });
            };
            BrokerPortalController.prototype.ShowQuote = function () {
                window.open(this.quotelink);
            };
            BrokerPortalController.prototype.ShowCert = function () {
                window.open(this.certlink);
            };
            BrokerPortalController.prototype.showCheckout = function () {
                window.open(this.checkoutlink);
            };
            BrokerPortalController.routing = function ($routeProvider) {
                $routeProvider.when("/brokerPortal", {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/brokerPortal.html",
                    controllerAs: "brokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
                $routeProvider.when("/brokerQuotes", {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/brokerQuotes.html",
                    controllerAs: "brokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
                $routeProvider.when("/brokerDetails", {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/brokerDetails.html",
                    controllerAs: "brokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
                $routeProvider.when("/getQuote/:quotePK", {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/quoteDetails.html",
                    controllerAs: "brokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            };
            BrokerPortalController.$inject = ["$http", "$scope", "AuthenticationService", "$rootScope", "$location", "$routeParams", "PostCodeService", "ChargeService", "$q"];
            return BrokerPortalController;
        }());
        Admin.BrokerPortalController = BrokerPortalController;
        angular.module("App")
            .controller("BrokerPortalController", BrokerPortalController)
            .config(["$routeProvider", BrokerPortalController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=BrokerPortalController.js.map