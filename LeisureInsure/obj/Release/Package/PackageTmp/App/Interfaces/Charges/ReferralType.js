var LeisureInsure;
(function (LeisureInsure) {
    var ReferralType;
    (function (ReferralType) {
        ReferralType[ReferralType["None"] = 0] = "None";
        ReferralType[ReferralType["Referred"] = 1] = "Referred";
        ReferralType[ReferralType["Overridden"] = 2] = "Overridden";
    })(ReferralType = LeisureInsure.ReferralType || (LeisureInsure.ReferralType = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=ReferralType.js.map