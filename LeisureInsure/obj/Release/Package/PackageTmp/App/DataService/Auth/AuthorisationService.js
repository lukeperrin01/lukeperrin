/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var AuthorisationService = /** @class */ (function () {
        function AuthorisationService() {
        }
        AuthorisationService.$inject = [];
        return AuthorisationService;
    }());
    LeisureInsure.AuthorisationService = AuthorisationService;
    angular.module("App").service("AuthorisationService", AuthorisationService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=AuthorisationService.js.map