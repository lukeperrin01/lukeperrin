/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var HttpServiceInterceptor = /** @class */ (function () {
        function HttpServiceInterceptor() {
        }
        // automatically attach Authorization header
        HttpServiceInterceptor.prototype.request = function (config) {
            var token = HttpServiceInterceptor.authenticationService.token;
            if (token) {
                config.headers.Authorization = "Bearer " + token;
            }
            return config;
        };
        HttpServiceInterceptor.factory = function (authenticationService) {
            HttpServiceInterceptor.authenticationService = authenticationService;
            return new HttpServiceInterceptor();
        };
        // If a token was sent back, save it
        HttpServiceInterceptor.prototype.response = function (res) {
            if (res.status === 401) {
                HttpServiceInterceptor.authenticationService.logout();
            }
            return res;
        };
        return HttpServiceInterceptor;
    }());
    LeisureInsure.HttpServiceInterceptor = HttpServiceInterceptor;
    angular.module("App")
        .factory("HttpServiceInterceptor", ["AuthenticationService", HttpServiceInterceptor.factory])
        .config(["$httpProvider", function ($httpProvider) {
            $httpProvider.interceptors.push("HttpServiceInterceptor");
        }]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=HttpServiceInterceptor.js.map