/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var LandingPageService = /** @class */ (function () {
            function LandingPageService($http, $q) {
                var _this = this;
                var defer = $q.defer();
                this.q = $q;
                this.http = $http;
                this.initialised = defer.promise;
                $http.get("api/v1/landingpages").then(function (response) {
                    _this.landingPages = angular.copy(response.data);
                    defer.resolve(_this);
                });
            }
            LandingPageService.prototype.landingPageForId = function (landingPageId) {
                return _.find(this.landingPages, function (landingPage) {
                    return (landingPage.id === landingPageId);
                });
            };
            LandingPageService.prototype.addLandingPage = function () {
                var _this = this;
                var defer = this.q.defer();
                this.http.post("api/v1/landingpages", null).then(function (response) {
                    var landingPage = response.data;
                    _this.landingPages.push(landingPage);
                    defer.resolve(landingPage);
                });
                return defer.promise;
            };
            LandingPageService.prototype.deleteLandingPage = function (landingPageId) {
                var _this = this;
                this.http.delete("api/v1/landingpages/" + landingPageId).then(function (response) {
                    if (response.status === 200) {
                        var landingPage = _.find(_this.landingPages, function (landingPage) {
                            return (landingPage.id == landingPageId);
                        });
                        if (landingPage) {
                            var index = _this.landingPages.indexOf(landingPage);
                            if (index > -1) {
                                _this.landingPages.splice(index, 1);
                            }
                        }
                    }
                });
            };
            LandingPageService.prototype.updateLandingPage = function (landingPage) {
                var _this = this;
                this.http.put("api/v1/landingpages/" + landingPage.id, landingPage).then(function (response) {
                    if (response.status === 200) {
                        var existingObject = _this.landingPageForId(landingPage.id);
                        angular.copy(landingPage, existingObject);
                    }
                });
            };
            LandingPageService.$inject = ["$http", "$q"];
            return LandingPageService;
        }());
        Admin.LandingPageService = LandingPageService;
        angular.module("LeisureInsure.Admin").service("LandingPageService", LandingPageService);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=LandingPageService.js.map