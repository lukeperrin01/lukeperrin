/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var RateUpdaterService = /** @class */ (function () {
        function RateUpdaterService($http, $q, $rootScope) {
            this.http = $http;
            this.q = $q;
            this.rootScope = $rootScope;
            //this.initialised = this.q.promise;
        }
        RateUpdaterService.prototype.UpdateRate = function (ratemodel) {
            var _this = this;
            var deferred = this.q.defer();
            var json = JSON.stringify(ratemodel);
            this.rootScope.loading = true;
            this.http({
                url: "/api/v1/ratechecker/updaterate",
                method: "PUT",
                data: json
            })
                .success(function (data) {
                _this.rootScope.loading = false;
                deferred.resolve(data);
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        RateUpdaterService.prototype.UpdateLocale = function (localemodel) {
            var _this = this;
            var deferred = this.q.defer();
            var json = JSON.stringify(localemodel);
            this.rootScope.loading = true;
            this.http({
                url: "/api/v1/ratechecker/updatelocale",
                method: "PUT",
                data: json
            })
                .success(function (data) {
                _this.rootScope.loading = false;
                deferred.resolve(data);
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        RateUpdaterService.prototype.GetPoliciesCoversRates = function () {
            var _this = this;
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/ratechecker/getpolicies",
                method: "GET"
            })
                .success(function (data) {
                deferred.resolve(data);
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        RateUpdaterService.prototype.GetLocales = function () {
            var _this = this;
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/ratechecker/getlocales",
                method: "GET"
            })
                .success(function (data) {
                deferred.resolve(data);
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        RateUpdaterService.prototype.ShowError = function (err, defer) {
            this.rootScope.loading = false;
            eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + err.data.message + "');");
            defer.reject(err.data.message);
        };
        RateUpdaterService.$inject = ["$http", "$q", "$rootScope"];
        return RateUpdaterService;
    }());
    LeisureInsure.RateUpdaterService = RateUpdaterService;
    angular.module("App").service("RateUpdaterService", RateUpdaterService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=RateUpdaterService.js.map