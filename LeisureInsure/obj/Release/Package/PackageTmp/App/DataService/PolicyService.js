/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var PolicyService = /** @class */ (function () {
        function PolicyService($http, $q) {
            this.selectedPolicyId = 0;
            this.policies = [];
            this.http = $http;
            this.q = $q;
            //this.initialised = this.q.promise;
        }
        PolicyService.prototype.addPolicies = function (policies) {
            angular.copy(policies, this.policies);
            //this.q.resolve(this);
        };
        PolicyService.prototype.GetPolicies = function (localeID) {
            var _this = this;
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/data/getallpolicies",
                method: "GET",
                params: { localeID: localeID }
            })
                .success(function (data) {
                deferred.resolve();
                _this.addPolicies(data);
            })
                .catch(function (data) {
                deferred.reject(data.data.message);
                //console.log("error", data);
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        PolicyService.prototype.updateSelectedPolicy = function (selectedPolicy) {
            if (selectedPolicy != null) {
                this.selectedPolicyId = selectedPolicy.policyPK;
            }
        };
        PolicyService.prototype.selectPolicy = function (policy) {
            if (policy != null) {
                this.selectedPolicy = policy;
                this.selectedPolicyId = policy.policyPK;
            }
        };
        PolicyService.prototype.selectPolicyById = function (policyId) {
            var policy = _.findWhere(this.policies, { policyPK: policyId });
            if (policy) {
                this.selectPolicy(policy);
            }
        };
        PolicyService.prototype.refreshPolicies = function (policyList) {
            this.policies = policyList;
        };
        PolicyService.$inject = ["$http", "$q"];
        return PolicyService;
    }());
    LeisureInsure.PolicyService = PolicyService;
    angular.module("App").service("PolicyService", PolicyService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=PolicyService.js.map