/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var PolicyUpdateDataService = /** @class */ (function () {
        function PolicyUpdateDataService($http, $q, policyService, coverService, coverInputsService, locationService, $rootScope) {
            this.http = $http;
            this.q = $q;
            this.policyService = policyService;
            this.coverService = coverService;
            this.coverInputsService = coverInputsService;
            this.locationService = locationService;
            this.rootScope = $rootScope;
        }
        PolicyUpdateDataService.prototype.initialiseNew = function (locale, quoteKey, policyKey) {
            var _this = this;
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/data/newpolicy",
                method: "GET",
                params: { strLocale: locale, quotePK: quoteKey, policyPK: policyKey }
            })
                .success(function (data) {
                if (data.locale.localePK != _this.locationService.selectedLocale.localePK) {
                    _this.locationService.getlocales(data.locales.localeSelected.strLocale, policyKey);
                }
                deferred.resolve(data);
            })
                .catch(function (data) {
                // Error
                //this.rootScope.loadingGif = false; 
                deferred.reject(data.data.message);
                //console.log("error", data);
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        PolicyUpdateDataService.$inject = ["$http", "$q", "PolicyService", "CoverService", "CoverInputsService", "LocationService", "$rootScope"];
        return PolicyUpdateDataService;
    }());
    LeisureInsure.PolicyUpdateDataService = PolicyUpdateDataService;
    angular.module("App").service("PolicyUpdateDataService", PolicyUpdateDataService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=PolicyUpdateDataService.js.map