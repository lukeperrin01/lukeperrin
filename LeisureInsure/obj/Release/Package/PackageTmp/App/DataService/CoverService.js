/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var CoverService = /** @class */ (function () {
        function CoverService($q, $filter) {
            this.filter = $filter;
            this.selectedCoverId = 0;
            this.covers = Array();
            this.policyCovers = Array();
            var deferred = $q.defer();
            this.q = deferred;
            this.initialised = deferred.promise;
        }
        CoverService.prototype.addCovers = function (covers) {
            angular.copy(covers, this.covers);
            this.q.resolve(this);
        };
        CoverService.prototype.updateSelectedCover = function (selectedCover) {
            this.selectedCover = selectedCover;
            this.selectedCoverId = selectedCover.coverPK;
        };
        CoverService.prototype.refreshCovers = function (coverList) {
            this.covers = coverList;
        };
        CoverService.prototype.coversForPolicy = function (coverFilter) {
            var selectedCovers = new Array();
            if (coverFilter > 0) {
                _.each(this.covers, function (cover) {
                    selectedCovers.push(cover);
                });
                this.policyCovers = selectedCovers;
            }
            else {
                this.policyCovers = this.covers;
            }
            ;
        };
        CoverService.$inject = ["$q", "$filter"];
        return CoverService;
    }());
    LeisureInsure.CoverService = CoverService;
    angular.module("App").service("CoverService", CoverService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=CoverService.js.map