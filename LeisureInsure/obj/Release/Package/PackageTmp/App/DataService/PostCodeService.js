/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var PostCodeService = /** @class */ (function () {
        function PostCodeService($http, $q, $rootScope) {
            this.http = $http;
            this.q = $q;
            this.rootScope = $rootScope;
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
        }
        PostCodeService.prototype.GetAddresses = function (postcode) {
            var _this = this;
            this.rootScope.searchingQuote = true;
            var deferred = this.q.defer();
            this.http({
                url: "api/v1/postcode",
                method: "GET",
                params: { postcode: postcode }
            })
                .success(function (data) {
                // Successful                                       
                deferred.resolve(data);
                _this.rootScope.searchingQuote = false;
            })
                .catch(function (data) {
                // Error                    
                deferred.reject();
                _this.rootScope.searchingQuote = false;
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        PostCodeService.$inject = ["$http", "$q", "$rootScope"];
        return PostCodeService;
    }());
    LeisureInsure.PostCodeService = PostCodeService;
    angular.module("App").service("PostCodeService", PostCodeService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=PostCodeService.js.map