/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../../scripts/typings/angularjs/angular-route.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var UrlToNavigationService = /** @class */ (function () {
        function UrlToNavigationService($routeParams, $http, $q) {
            this.routeParams = $routeParams;
            this.http = $http;
            this.q = $q;
        }
        UrlToNavigationService.prototype.translateUrlNew = function (segmentName) {
            var policy = this.routeParams[segmentName];
            var defer = this.q.defer();
            this.http.get("api/v1/data/getlandingUrl/" + policy).then(function (response) {
                if (response.status === 200) {
                    var landingPage = response.data;
                    defer.resolve(landingPage);
                }
                defer.reject(response.statusText);
            });
            return defer.promise;
        };
        UrlToNavigationService.prototype.GetLandingPageNew = function (quoteRef, password) {
            var _this = this;
            var defer = this.q.defer();
            this.http.get("api/v1/data/getlandingQuote/" + quoteRef + "/" + password).success(function (response) {
                var landingPage = response;
                defer.resolve(landingPage);
            }).catch(function (err) { return _this.ShowError(err, defer); });
            return defer.promise;
        };
        UrlToNavigationService.prototype.ShowError = function (err, defer) {
            eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + err.data.message + "');");
            defer.reject(err.data.message);
        };
        UrlToNavigationService.$inject = ["$routeParams", "$http", "$q"];
        return UrlToNavigationService;
    }());
    LeisureInsure.UrlToNavigationService = UrlToNavigationService;
    angular.module("App").service("UrlToNavigationService", UrlToNavigationService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=UrlToNavigationService.js.map