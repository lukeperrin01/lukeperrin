/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var CoverNameService = /** @class */ (function () {
        function CoverNameService() {
        }
        CoverNameService.prototype.idForName = function (name) {
            switch (name) {
                case "no-cover":
                    return 0;
                case "basic-info":
                    return 1;
                case "public-liability":
                    return 2;
                case "employer-liability":
                    return 3;
                case "material-damage-buildings":
                    return 4;
                case "material-damage-ancillary-buildings":
                    return 5;
                case "material-damage-fixtures-and-fittings":
                    return 6;
                case "material-damage-playing-surfaces":
                    return 7;
                case "material-damage-business-equipment":
                    return 8;
                case "material-damage-trophies-and-memorabilia":
                    return 9;
                case "material-damage-machinery-and-plant":
                    return 10;
                case "material-damage-stock":
                    return 11;
                case "business-interruption-loss-of-gross-profit":
                    return 12;
                case "material-damage-equipment-list":
                    return 13;
                case "material-damage-refrigerated-goods":
                    return 14;
                case "material-damage-miscellaneous-contents":
                    return 15;
                case "material-damage-property-in-transit":
                    return 16;
                case "material-damage-money":
                    return 17;
                case "product-liabilty":
                    return 18;
                case "trustee-liability":
                    return 19;
                case "professional-liability":
                    return 20;
                case "personal-accident":
                    return 21;
                case "personal-assault":
                    return 23;
                case "business-interruption-loss-of-gross-revenue":
                    return 24;
                case "business-interruption-loss-of-gross-rentals":
                    return 25;
                case "business-interruption-book-debts":
                    return 26;
                case "business-interruption-denial-of-access":
                    return 27;
                case "business-interruption-increased-cost-of-working":
                    return 28;
            }
            return 0;
        };
        CoverNameService.prototype.nameForId = function (id) {
            switch (id) {
                case 0:
                    return "no-cover";
                case 1:
                    return "basic-info";
                case 2:
                    return "public-liability";
                case 3:
                    return "employer-liability";
                case 4:
                    return "material-damage-buildings";
                case 5:
                    return "material-damage-ancillary-buildings";
                case 6:
                    return "material-damage-fixtures-and-fittings";
                case 7:
                    return "material-damage-playing-surfaces";
                case 8:
                    return "material-damage-business-equipment";
                case 9:
                    return "material-damage-trophies-and-memorabilia";
                case 10:
                    return "material-damage-machinery-and-plant";
                case 11:
                    return "material-damage-stock";
                case 12:
                    return "business-interruption-loss-of-gross-profit";
                case 13:
                    return "material-damage-equipment-list";
                case 14:
                    return "material-damage-refrigerated-goods";
                case 15:
                    return "material-damage-miscellaneous-contents";
                case 16:
                    return "material-damage-property-in-transit";
                case 17:
                    return "material-damage-money";
                case 18:
                    return "product-liabilty";
                case 19:
                    return "trustee-liability";
                case 20:
                    return "professional-liability";
                case 21:
                    return "personal-accident";
                case 23:
                    return "personal-assault";
                case 24:
                    return "business-interruption-loss-of-gross-revenue";
                case 25:
                    return "business-interruption-loss-of-gross-rentals";
                case 26:
                    return "business-interruption-book-debts";
                case 27:
                    return "business-interruption-denial-of-access";
                case 28:
                    return "business-interruption-increased-cost-of-working";
            }
            return "";
        };
        CoverNameService.$inject = [];
        return CoverNameService;
    }());
    LeisureInsure.CoverNameService = CoverNameService;
    angular.module("App").service("CoverNameService", CoverNameService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=CoverNameService.js.map