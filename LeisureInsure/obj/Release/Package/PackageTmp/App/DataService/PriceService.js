/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
/*Note, We are only applying irish multiplier against keyed in values for liability covers, eg Employers liability, Public Liability
If PL is based on items being added not keyed in values then this is already set in the backend first, see GetPolicyData in PoliciesRepository
that is also where minimum rates are applying multiplier. Idea being set as much as we can before the form receives rates*/
var LeisureInsure;
(function (LeisureInsure) {
    var PriceService = /** @class */ (function () {
        function PriceService($rootScope, $http, $q, $filter, localeService, commonDataService) {
            this.http = $http;
            this.q = $q;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.filter = $filter;
            this.commonDataService = commonDataService;
        }
        PriceService.prototype.GetPrice = function (policyData) {
            var _this = this;
            var covers = policyData.covers;
            var manCover = policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            var feeRate = manCover.rates.filter(function (x) { return x.rateTypeId == 19; })[0];
            var totalFee = 0;
            var minFee = 0;
            var finalFee = 0;
            var legalCare = 0;
            this.commonDataService.priceChanged = true;
            policyData.refer = false;
            var totalnet;
            totalnet = 0;
            _.each(covers, function (cover) {
                //this will get us indemnity/net/suminsured/exess
                switch (cover.id) {
                    case CoverEnum.EquipPL:
                        _this.GetEquipPL(cover, policyData);
                        break;
                    case CoverEnum.EquipEL:
                        _this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.EquipMD:
                        _this.GetEquipMD(cover, policyData);
                        break;
                    case CoverEnum.EquipProd:
                        _this.GetProdL(cover, policyData);
                        break;
                    case CoverEnum.EquipProf:
                        _this.GetProfL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallPL:
                        _this.GetPaintballPL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallEL:
                        _this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallBuildings:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallAncillaryBuildings:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallFixtures:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallPlayingSurfaces:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallBusinessEquip:
                        _this.GetBusinessEquipment(cover, policyData);
                        break;
                    case CoverEnum.PaintBallTrophies:
                        _this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.PaintBallMachinery:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallStock:
                        _this.TwoInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallRefrigerated:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallPropertyInTransit:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallMoney:
                        _this.GetMoney(cover, policyData);
                        break;
                    case CoverEnum.PaintBallRentPayable:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallProdLiability:
                        _this.GetProdL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallProfLiability:
                        _this.GetProfL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallLossOfGrossProfit:
                        _this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.PaintBallLossOfGrossRevenue:
                        _this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.PaintBallLossOfGrossRentals:
                        _this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.PaintBallBookDebts:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallCostOfWorking:
                        _this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.AirsoftPL:
                        _this.GetPaintballPL(cover, policyData);
                        break;
                    case CoverEnum.AirsoftEL:
                        _this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.AirsoftProdLiability:
                        _this.GetProdL(cover, policyData);
                        break;
                    case CoverEnum.AirsoftProfLiability:
                        _this.GetProfL(cover, policyData);
                        break;
                    case CoverEnum.MobileCateringPL:
                        _this.GetMobileCateringPL(cover, policyData);
                        break;
                    case CoverEnum.MobileCateringMD:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.MobileCateringStockGeneral:
                        _this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.FreelanceCateringPL:
                        _this.GetFreelanceCateringPL(cover, policyData);
                        break;
                    case CoverEnum.SportsInstructorPL:
                        _this.GetSportsInstructorPL(cover, policyData);
                        break;
                    case CoverEnum.SportsInstructorEL:
                        _this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.SportsInstructorMD:
                        _this.GetSportsInstructorMD(cover, policyData);
                        break;
                    case CoverEnum.ShowmenPL:
                        _this.GetShowmenPL(cover, policyData);
                        break;
                    case CoverEnum.ShowmenEL:
                        _this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.ShowmenMD:
                        _this.GetShowmenMD(cover, policyData);
                        break;
                    case CoverEnum.ShowmenProducts:
                        _this.GetProdL(cover, policyData);
                        break;
                    case CoverEnum.ShowmenBusinessEquip:
                        _this.GetBusinessEquipment(cover, policyData);
                        break;
                    default:
                }
                //add on tax - cover level             
                if (cover.net > 0) {
                    var tax = cover.net * policyData.taxRate;
                    cover.tax = tax;
                    cover.total = cover.net + tax;
                    totalnet += cover.net;
                }
            });
            //calculate fees at policy level
            if (totalnet > 0) {
                if (feeRate.divBy == 1) {
                    finalFee = feeRate.rate;
                    minFee = 0;
                }
                else {
                    totalFee = feeRate.rate * totalnet / feeRate.divBy;
                    minFee = feeRate.minimumRate;
                    finalFee = Math.max(totalFee, minFee);
                }
            }
            //calculate fees at cover level
            _.each(covers, function (cover) {
                if (cover.net > 0) {
                    //we are only applying flat rates to mandatory covers eg PL,foodborne illness
                    if (feeRate.divBy == 1) {
                        if (cover.coverType == "Mandatory") {
                            cover.total = cover.total + finalFee;
                            cover.leisureInsureFee = finalFee;
                        }
                    }
                    else {
                        //get % this net is of the total net and use that to get the fee % from total fee                       
                        var percOfTotal = cover.net / totalnet;
                        var coverfee = finalFee * percOfTotal;
                        cover.total = cover.total + coverfee;
                        cover.leisureInsureFee = coverfee;
                    }
                }
            });
            if (policyData.brokerName != null) {
                var commisionRate = policyData.commissionPercentage / 100;
                var commision = (commisionRate) * totalnet;
                policyData.brokerCommission = commision;
            }
            //net and tax
            policyData.tax = totalnet * policyData.taxRate;
            policyData.net = totalnet;
            //excess
            policyData.excess = manCover.excess;
            //legal care with tax
            var legalCareRate = (policyData.legalRate / 100);
            var netLegalCare = 0;
            netLegalCare = totalnet * legalCareRate;
            var legalCareTax = netLegalCare * policyData.taxRate;
            legalCare = netLegalCare + legalCareTax;
            policyData.legalFees = legalCare;
            policyData.legalNet = netLegalCare;
            policyData.leisureInsureFee = finalFee;
            if (policyData.legalFeesAdded)
                policyData.total = policyData.net + policyData.tax + finalFee + legalCare;
            else
                policyData.total = policyData.net + policyData.tax + finalFee;
            return policyData;
        };
        // ---------------------------------- Equipment Hirers --------------------------------
        PriceService.prototype.GetEquipPL = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var excessVal;
            excessVal = 0;
            _.each(cover.itemsAdded, function (item) {
                if (policyData.tradeMember) {
                    item.initialCharge = item.discountFirstRate;
                    item.subsequentCharge = item.discountsubsequentRate;
                }
                else {
                    item.initialCharge = item.firstRate;
                    item.subsequentCharge = item.rate;
                }
                item.firstCharge = false;
                //if we have multiple quantites add as many as we need 
                for (var x = 0; x < item.quantity; x++) {
                    var newitem = void 0;
                    newitem = angular.copy(item, newitem);
                    charges.push(newitem);
                }
                var rate = cover.rates.filter(function (x) { return x.id == item.rateId; })[0];
                if (rate.refer)
                    policyData.refer = true;
            });
            // take the highest maximum charge in the items    
            if (charges.length > 0) {
                //let maxCharge = charges.reduce((l, e) => e.firstCharge > l.firstCharge ? e : l);
                var sortedcharges = void 0;
                sortedcharges = angular.copy(charges, sortedcharges);
                sortedcharges = _.sortBy(sortedcharges, "subsequentCharge");
                sortedcharges = _.sortBy(sortedcharges, "initialCharge");
                var maxCharge = sortedcharges[sortedcharges.length - 1];
                maxCharge.firstCharge = true;
                coverprice += maxCharge.initialCharge;
                //add up subsequent charges for each other item
                var subsequentCharges = sortedcharges.filter(function (x) { return x.firstCharge == false; });
                _.each(subsequentCharges, function (item) {
                    coverprice += item.subsequentCharge;
                });
                _.each(charges, function (item) {
                    console.log("charges initialCharge", item.initialCharge);
                    console.log("charges subsequentCharge", item.subsequentCharge);
                });
                _.each(sortedcharges, function (item) {
                    console.log("sortedcharges initialCharge", item.initialCharge);
                    console.log("sortedcharges subsequentCharge", item.subsequentCharge);
                });
            }
            //add on any loading - eg indemnity from our table
            if (cover.table != null) {
                //if trademember then get the rate for 1m (first row)
                //otherwise get whatever is selected
                var tradeRow;
                var selectedRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });
                if (policyData.tradeMember)
                    tradeRow = _.findWhere(cover.table.rows, { rowid: 1 });
                else
                    tradeRow = selectedRow;
                if (selectedRow == null)
                    return 0;
                //get column for this cover
                var selectedcell = _.findWhere(selectedRow.cells, { coverid: cover.id });
                var selectedRate = cover.rates.filter(function (x) { return x.id == selectedcell.rateid; })[0];
                var tradeCell = _.findWhere(tradeRow.cells, { coverid: cover.id });
                var tradeRate = cover.rates.filter(function (x) { return x.id == tradeCell.rateid; })[0];
                if (selectedRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = selectedRate.indemnity;
                coverprice += coverprice * tradeCell.rate / tradeCell.divby;
            }
            //excess - dropdown
            var excessPrice;
            var excess = _.findWhere(cover.rates, { rateTypeId: 7 });
            if (excess.inputSelected != null) {
                var option = excess.inputSelected;
                excessPrice = (coverprice * option.rate) / option.divBy;
                coverprice += excessPrice;
                excessVal = excess.inputSelected.excess;
                //nill excess - yes/no this is a flat charge depending on the excess selected
                var nillexcess = _.findWhere(cover.rates, { rateTypeId: 23 });
                if (nillexcess.rateValue == "yes") {
                    //add our nil excess flat charge
                    var nillExcessVal = option.nilExcessRate;
                    coverprice += nillExcessVal;
                    //take off the excess discount
                    coverprice -= excessPrice;
                }
            }
            //hazard questions - eg nightclub, flooding, are you a licencee etc
            //some of these questions have loadings some dont, but the ones that
            //dont still need to show in certificate which should be grouped together
            var genericLoadings = cover.rates.filter(function (x) { return x.rateTypeId == 18; });
            _.each(genericLoadings, function (loading) {
                //is the loading a yes / no?
                if (loading.inputTypeId == 19) {
                    if (loading.rateValue == "yes") {
                        if (loading.rate > 0) {
                            var loadingVal = (coverprice * loading.rate) / loading.divBy;
                            coverprice += loadingVal;
                        }
                    }
                }
            });
            if (policyData.tradeMember) {
                excessVal = 50 * policyData.locale.multiplier;
            }
            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;
            return coverprice;
        };
        PriceService.prototype.GetEL = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var totalWageRoll;
            totalWageRoll = 0;
            var excess;
            excess = 0;
            var indemnity;
            indemnity = 0;
            indemnity = indemnity * policyData.locale.exchangeRate;
            if (cover.coverRequired) {
                _.each(cover.itemsAdded, function (item) {
                    var wagerate = _.findWhere(cover.rates, { id: item.rateId });
                    coverprice += wagerate.rate * (item.rateValueNum / wagerate.divBy) * policyData.locale.multiplier;
                    totalWageRoll += item.rateValueNum;
                });
            }
            var minCharge;
            minCharge = 0;
            var sorted;
            sorted = [];
            //min charges - order by threshold
            var minCharges = cover.rates.filter(function (x) { return x.rateTypeId == 13; });
            sorted = this.filter("orderBy")(minCharges, ["threshold"]);
            if (totalWageRoll > 0) {
                for (var x = 0; x < sorted.length; x++) {
                    if (totalWageRoll < sorted[x].threshold) {
                        minCharge = sorted[x].rate;
                        break;
                    }
                }
            }
            if (minCharge > coverprice)
                coverprice = minCharge;
            if (coverprice > 0)
                indemnity = 10000000;
            cover.indemnity = indemnity;
            cover.excess = excess;
            cover.net = coverprice;
            cover.sumInsured = 0;
            cover.total = 0;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            return coverprice;
        };
        PriceService.prototype.GetEquipMD = function (cover, policyData) {
            var coverprice;
            coverprice = 0;
            var suminsured;
            suminsured = 0;
            //get mandatory cover as thats where we put in the equipment value
            var mancover = policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            var addingMD = false;
            //note we are storing equipment cover with the dimensions           
            var rate = cover.rates.filter(function (x) { return x.rateTypeId == 6; })[0];
            cover.excess = rate.excess * policyData.locale.multiplier;
            if (policyData.tradeMember) {
                cover.excess = 100 * policyData.locale.multiplier;
            }
            cover.itemsAdded = [];
            cover.itemsAdded = mancover.itemsAdded;
            _.each(cover.itemsAdded, function (item) {
                //go through all items where we added sumInsured (equipment cover)                            
                if (item.sumInsured > 0) {
                    coverprice += (item.sumInsured * rate.rate) / rate.divBy;
                    suminsured += item.sumInsured;
                    addingMD = true;
                }
            });
            if (addingMD) {
                if (rate.minimumRate > coverprice)
                    coverprice = rate.minimumRate;
            }
            else
                cover.coverRequired = false;
            cover.indemnity = 0;
            cover.net = coverprice;
            cover.sumInsured = suminsured;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            cover.total = 0;
            return coverprice;
        };
        PriceService.prototype.GetProdL = function (cover, policyData) {
            var mancover = policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            var coverprice;
            coverprice = 0;
            cover.excess = mancover.excess;
            if (policyData.tradeMember) {
                cover.excess = 50 * policyData.locale.multiplier;
            }
            cover.indemnity = mancover.indemnity;
            cover.net = coverprice;
            cover.sumInsured = 0;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            cover.total = 0;
            return coverprice;
        };
        PriceService.prototype.GetProfL = function (cover, policyData) {
            var mancover = policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            if (policyData.localeId == 1)
                indemnity = 1000000;
            else if (policyData.localeId == 2)
                indemnity = 1300000;
            cover.excess = mancover.excess;
            if (policyData.tradeMember) {
                cover.excess = 50 * policyData.locale.multiplier;
                cover.coverRequired = true;
            }
            cover.indemnity = indemnity;
            cover.net = coverprice;
            cover.sumInsured = 0;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            cover.total = 0;
            return coverprice;
        };
        // ---------------------------------- Paintball --------------------------------
        PriceService.prototype.GetPaintballPL = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var excess;
            excess = 500;
            if (policyData.localeId == 2)
                excess = excess * 2;
            //get premium - turnover
            var turnover = _.findWhere(cover.rates, { rateTypeId: 10 });
            //get rid of non numeric characters
            var input = turnover.rateValue.replace(/\D/g, '');
            var inputAsNum = parseInt(input);
            var turnoverVal = (inputAsNum * turnover.rate) / turnover.divBy * policyData.locale.multiplier;
            if (turnoverVal < turnover.minimumRate)
                turnoverVal = turnover.minimumRate;
            coverprice += turnoverVal;
            //add on any loading - eg indemnity from our table
            if (cover.table != null) {
                //get selected row
                var row = _.findWhere(cover.table.rows, { selected: true });
                if (row == null)
                    return 0;
                //get column for this cover
                var cell = _.findWhere(row.cells, { coverid: cover.id });
                var rate = cover.rates.filter(function (x) { return x.id == cell.rateid; })[0];
                if (rate.refer == 1)
                    policyData.refer = true;
                indemnity = rate.indemnity;
                coverprice += coverprice * cell.rate / cell.divby;
            }
            if (indemnity > 0)
                cover.indemnity = indemnity;
            cover.net = coverprice;
            cover.sumInsured = 0;
            cover.excess = excess;
            return coverprice;
        };
        //select item, enter value, add item to list
        PriceService.prototype.GetBusinessEquipment = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var suminsured;
            suminsured = 0;
            var excess;
            excess = 250;
            if (cover.coverRequired) {
                _.each(cover.itemsAdded, function (item) {
                    var equiprate = _.findWhere(cover.rates, { id: item.rateId });
                    coverprice += equiprate.rate * (item.rateValueNum / equiprate.divBy);
                    suminsured = item.rateValueNum;
                });
            }
            var minCharge;
            minCharge = 0;
            var sorted;
            sorted = [];
            cover.indemnity = 0;
            cover.excess = excess;
            cover.net = coverprice;
            cover.sumInsured = suminsured;
            cover.total = 0;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            return coverprice;
        };
        //select item, enter value 
        PriceService.prototype.GetValueFromSelect = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var suminsured;
            suminsured = 0;
            var excess = 250;
            if (cover.coverRequired) {
                //ie EU, UK, Worldwide, get selected option
                var selectedType = _.findWhere(cover.rates, { inputTypeId: 7 });
                var rateType = selectedType.inputSelected;
                if (rateType != null) {
                    //get value entered from currency field
                    var rateValue = _.findWhere(cover.rates, { inputTypeId: 18 });
                    //get rid of non numeric characters
                    var input = rateValue.rateValue.replace(/\D/g, '');
                    var inputAsNum = parseInt(input);
                    suminsured = inputAsNum;
                    var val = (inputAsNum * rateType.rate) / rateType.divBy;
                    coverprice += val;
                }
            }
            cover.net = coverprice;
            cover.excess = excess; //? ireland
            cover.sumInsured = suminsured;
            cover.indemnity = 0;
            return coverprice;
        };
        //enter value only (2 inputs)
        PriceService.prototype.TwoInputLoading = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var suminsured;
            suminsured = 0;
            var excess = 250;
            if (cover.coverRequired) {
                //get premium - turnover
                var rateValues = cover.rates.filter(function (x) { return x.rateTypeId == 10; });
                _.each(rateValues, function (value) {
                    //get rid of non numeric characters
                    var input = value.rateValue.replace(/\D/g, '');
                    var inputAsNum = parseInt(input);
                    suminsured += inputAsNum;
                    var stockVal = (inputAsNum * value.rate) / value.divBy;
                    coverprice += stockVal;
                });
            }
            cover.net = coverprice;
            cover.excess = excess; //? ireland
            cover.sumInsured = suminsured;
            cover.indemnity = 0;
            return coverprice;
        };
        //enter value only
        PriceService.prototype.OneInputLoading = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var suminsured;
            suminsured = 0;
            var excess = 250 * policyData.locale.multiplier;
            if (cover.coverRequired) {
                //get premium - turnover
                var inputRate = _.findWhere(cover.rates, { rateTypeId: 10 });
                //get rid of non numeric characters
                var keyedinput = inputRate.rateValue.replace(/\D/g, '');
                var inputAsNum = parseInt(keyedinput);
                suminsured = inputAsNum;
                var mdVal = (inputAsNum * inputRate.rate) / inputRate.divBy;
                if (inputRate.minimumRate > mdVal && inputRate.minimumRate > 0)
                    mdVal = inputRate.minimumRate;
                coverprice += mdVal;
            }
            cover.net = coverprice;
            cover.excess = excess; //? ireland
            cover.sumInsured = suminsured;
            cover.indemnity = 0;
            return coverprice;
        };
        //enter value only (multiple inputs)
        PriceService.prototype.GetMoney = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var suminsured;
            suminsured = 0;
            var excess = 250;
            if (cover.coverRequired) {
                //get premium - turnover
                var rateValues = cover.rates.filter(function (x) { return x.inputTypeId == 18; });
                _.each(rateValues, function (value) {
                    //get rid of non numeric characters
                    if (value.rateValue != null) {
                        var input = value.rateValue.replace(/\D/g, '');
                        var inputAsNum = parseInt(input);
                        suminsured += inputAsNum;
                        var moneyVal = (inputAsNum * value.rate) / value.divBy;
                        coverprice += moneyVal;
                    }
                });
                //have we added personal assault?
                var assault = cover.rates.filter(function (x) { return x.extraCoverId != null; })[0];
                var addedCover = policyData.covers.filter(function (x) { return x.id == assault.extraCoverId; })[0];
                if (assault != null) {
                    if (assault.rateValue == "true") {
                        //add 25%
                        var assaultVal = (coverprice * assault.rate) / assault.divBy;
                        coverprice += assaultVal;
                        //make sure cover is added
                        addedCover.total = 0;
                        addedCover.net = 0;
                        addedCover.excess = 250;
                        addedCover.indemnity = 0;
                        addedCover.sumInsured = 0;
                        addedCover.coverRequired = true;
                    }
                    else {
                        addedCover.coverRequired = false;
                    }
                }
            }
            cover.net = coverprice;
            cover.excess = excess; //? ireland
            cover.sumInsured = suminsured;
            cover.indemnity = 0;
            return coverprice;
        };
        // ***************************** Mobile Catering **********************************************************************************
        PriceService.prototype.GetMobileCateringPL = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var excessVal;
            excessVal = 250 * policyData.locale.multiplier;
            //add on any loading - eg indemnity from our table
            if (cover.table != null) {
                //if trademember then get the rate for 1m (first row)
                //otherwise get whatever is selected
                var tradeRow;
                var selectedRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });
                tradeRow = selectedRow;
                if (selectedRow == null)
                    return 0;
                //get turnover column which has our rate
                var turnovercell = selectedRow.cells[4];
                var plcell = selectedRow.cells[1];
                var turnoverRate = cover.rates.filter(function (x) { return x.id == turnovercell.rateid; })[0];
                cover.turnover = turnovercell.turnover;
                if (turnoverRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = plcell.indemnity;
                coverprice += turnoverRate.rate / turnovercell.divby;
            }
            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;
            //we can add in bundled covers this way when we add them on the PL table
            //no need for another method
            _.each(selectedRow.cells, function (cell, ICell) {
                if (cell.coverid > 0 && cell.coverid != cover.id) {
                    var addcover = policyData.covers.filter(function (x) { return x.id == cell.coverid; })[0];
                    addcover.coverRequired = true;
                    addcover.excess = cover.excess;
                    addcover.indemnity = cell.indemnity;
                    addcover.total = 0;
                    addcover.net = 0;
                }
            });
            return coverprice;
        };
        // ***************************** Freelance Catering **********************************************************************************
        PriceService.prototype.GetFreelanceCateringPL = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var excessVal;
            excessVal = 0;
            var nilexcess;
            nilexcess = 0;
            //add on any loading - eg indemnity from our table
            if (cover.table != null) {
                var selectedRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });
                if (selectedRow == null)
                    return 0;
                var plcell = selectedRow.cells[1];
                var plRate = cover.rates.filter(function (x) { return x.id == plcell.rateid; })[0];
                if (plRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = plcell.indemnity;
                coverprice += plRate.rate / plRate.divBy;
                excessVal = plRate.excess * policyData.locale.multiplier;
                //nill excess - yes/no this is always based on the rate for 250
                var nilexcessRate = _.findWhere(cover.rates, { rateTypeId: 23 });
                if (nilexcessRate.rateValue == "yes") {
                    if (nilexcessRate.divBy == 100)
                        nilexcess = (coverprice * nilexcessRate.rate) / nilexcessRate.divBy;
                    else
                        nilexcess = nilexcessRate.rate;
                    coverprice += nilexcess;
                }
            }
            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;
            return coverprice;
        };
        // ***************************** Sports Instructor **********************************************************************************
        PriceService.prototype.GetSportsInstructorPL = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var excessVal;
            excessVal = 0;
            var nilexcess;
            nilexcess = 0;
            //add on any loading - eg indemnity from our table
            if (cover.table != null) {
                var selectedRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });
                if (selectedRow == null)
                    return 0;
                var plcell = selectedRow.cells[1];
                var plRate = cover.rates.filter(function (x) { return x.id == plcell.rateid; })[0];
                if (plRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = plcell.indemnity;
                _.each(cover.itemsAdded, function (item) {
                    //get highest price
                    var activity = cover.rates.filter(function (x) { return x.id == item.rateId; })[0];
                    var rate = cover.rates.filter(function (x) { return x.catRating == activity.catRating && plcell.indemnity == x.indemnity; })[0];
                    if (rate.rate > coverprice)
                        coverprice = rate.rate;
                });
                excessVal = plRate.excess * policyData.locale.multiplier;
                //nill excess - yes/no this is always based on the rate for 250
                var nilexcessRate = _.findWhere(cover.rates, { rateTypeId: 23 });
                if (nilexcessRate.rateValue == "yes") {
                    if (nilexcessRate.divBy == 100)
                        nilexcess = (coverprice * nilexcessRate.rate) / nilexcessRate.divBy;
                    else
                        nilexcess = nilexcessRate.rate;
                    coverprice += nilexcess;
                }
            }
            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;
            return coverprice;
        };
        PriceService.prototype.GetSportsInstructorMD = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var sumInsured;
            sumInsured = 0;
            var excess;
            excess = 0;
            var nilexcess;
            nilexcess = 0;
            //get select option
            if (cover.coverRequired) {
                var selected = cover.rates.filter(function (x) { return x.inputTypeId == 7; })[0];
                if (selected.inputSelected != null) {
                    var option = selected.inputSelected;
                    excess = option.excess * policyData.locale.multiplier;
                    if (option.divBy == 1) {
                        coverprice = option.rate;
                        sumInsured = option.rateValueNum;
                    }
                    else {
                        //get free type value
                        var input = cover.rates.filter(function (x) { return x.inputTypeId == 18; })[0];
                        var rateMultiplier = (input.rate / 100);
                        coverprice = input.rateValueNum * rateMultiplier;
                        sumInsured = input.rateValueNum;
                        excess = input.excess * policyData.locale.multiplier;
                    }
                }
            }
            cover.net = coverprice;
            cover.sumInsured = sumInsured;
            cover.excess = excess;
            return coverprice;
        };
        PriceService.prototype.GetShowmenPL = function (cover, policyData) {
            var charges;
            charges = [];
            var coverprice;
            coverprice = 0;
            var indemnity;
            indemnity = 0;
            var excessVal;
            excessVal = 0;
            _.each(cover.itemsAdded, function (item) {
                item.initialCharge = item.firstRate;
                item.subsequentCharge = item.rate;
                item.firstCharge = false;
                //if we have multiple quantites add as many as we need 
                for (var x = 0; x < item.quantity; x++) {
                    var newitem = void 0;
                    newitem = angular.copy(item, newitem);
                    charges.push(newitem);
                }
                var rate = cover.rates.filter(function (x) { return x.id == item.rateId; })[0];
                if (rate.refer)
                    policyData.refer = true;
            });
            // take the highest maximum charge in the items    
            if (charges.length > 0) {
                //let maxCharge = charges.reduce((l, e) => e.firstCharge > l.firstCharge ? e : l);
                var sortedcharges = void 0;
                sortedcharges = angular.copy(charges, sortedcharges);
                sortedcharges = _.sortBy(sortedcharges, "subsequentCharge");
                sortedcharges = _.sortBy(sortedcharges, "initialCharge");
                var maxCharge = sortedcharges[sortedcharges.length - 1];
                maxCharge.firstCharge = true;
                coverprice += maxCharge.initialCharge;
                //add up subsequent charges for each other item
                var subsequentCharges = sortedcharges.filter(function (x) { return x.firstCharge == false; });
                _.each(subsequentCharges, function (item) {
                    coverprice += item.subsequentCharge;
                });
                _.each(charges, function (item) {
                    console.log("charges initialCharge", item.initialCharge);
                    console.log("charges subsequentCharge", item.subsequentCharge);
                });
                _.each(sortedcharges, function (item) {
                    console.log("sortedcharges initialCharge", item.initialCharge);
                    console.log("sortedcharges subsequentCharge", item.subsequentCharge);
                });
            }
            //add on any loading - eg indemnity from our table
            if (cover.table != null) {
                //if trademember then get the rate for 1m (first row)
                //otherwise get whatever is selected
                var tradeRow;
                var selectedRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });
                if (policyData.tradeMember)
                    tradeRow = _.findWhere(cover.table.rows, { rowid: 1 });
                else
                    tradeRow = selectedRow;
                if (selectedRow == null)
                    return 0;
                //get column for this cover
                var selectedcell = _.findWhere(selectedRow.cells, { coverid: cover.id });
                var selectedRate = cover.rates.filter(function (x) { return x.id == selectedcell.rateid; })[0];
                if (selectedRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = selectedRate.indemnity;
                coverprice += coverprice * selectedcell.rate / selectedcell.divby;
            }
            //refer questions
            var yesnoquestions = cover.rates.filter(function (x) { return x.inputTypeId == 19; });
            _.each(yesnoquestions, function (question) {
                if (question.refer) {
                    if (question.invertReferral && question.rateValue == "no") {
                        policyData.refer = true;
                    }
                    if (!question.invertReferral && question.rateValue == "yes") {
                        policyData.refer = true;
                    }
                }
            });
            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;
            return coverprice;
        };
        PriceService.prototype.GetShowmenMD = function (cover, policyData) {
            var coverprice;
            coverprice = 0;
            var suminsured;
            suminsured = 0;
            //get mandatory cover as thats where we put in the equipment value
            var mancover = policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            var addingMD = false;
            //note we are storing equipment cover with the dimensions           
            var rate = cover.rates.filter(function (x) { return x.rateTypeId == 6; })[0];
            cover.excess = rate.excess * policyData.locale.multiplier;
            cover.itemsAdded = [];
            cover.itemsAdded = mancover.itemsAdded;
            _.each(cover.itemsAdded, function (item) {
                //go through all items where we added sumInsured (equipment cover)                            
                if (item.sumInsured > 0) {
                    coverprice += (item.sumInsured * rate.rate) / rate.divBy;
                    suminsured += item.sumInsured;
                    addingMD = true;
                }
            });
            if (addingMD) {
                if (rate.minimumRate > coverprice)
                    coverprice = rate.minimumRate;
            }
            else
                cover.coverRequired = false;
            cover.indemnity = 0;
            cover.net = coverprice;
            cover.sumInsured = suminsured;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            cover.total = 0;
            return coverprice;
        };
        PriceService.prototype.CalculateInstalments = function (data, policy) {
            //£100 -£1, 000       18.00%
            //£1, 000 -£2, 000    12.50%
            //£2, 000 -£3, 000    10.00%
            //£3, 000 +           8.50 %            
            var newtotal;
            var total = policy.total;
            if (total <= 1000) {
                newtotal = total * 1.18;
            }
            else if (total > 1000 && total < 2000) {
                newtotal = total * 1.125;
            }
            else if (total >= 2000 && total < 3000) {
                newtotal = total * 1.10;
            }
            else if (total >= 3000) {
                newtotal = total * 1.085;
            }
            data.numberOfPayments = 8;
            var instalment = newtotal / 10;
            //deposit is 2 monthly instalments
            data.deposit = instalment * 2;
            //remaining left after deposit
            var remaining = newtotal - data.deposit;
            //8 payments in total        
            var monthlyPayment = remaining / 8;
            data.eachPayment = monthlyPayment;
            data.totalToPay = newtotal;
        };
        PriceService.$inject = ["$rootScope", "$http", "$q", "$filter", "LocationService", "CommonDataService"];
        return PriceService;
    }());
    LeisureInsure.PriceService = PriceService;
    angular.module("App").service("PriceService", PriceService);
    var CoverEnum;
    (function (CoverEnum) {
        CoverEnum[CoverEnum["EquipPL"] = 1] = "EquipPL";
        CoverEnum[CoverEnum["EquipEL"] = 2] = "EquipEL";
        CoverEnum[CoverEnum["EquipMD"] = 4] = "EquipMD";
        CoverEnum[CoverEnum["EquipProd"] = 5] = "EquipProd";
        CoverEnum[CoverEnum["EquipProf"] = 28] = "EquipProf";
        CoverEnum[CoverEnum["PaintBallPL"] = 6] = "PaintBallPL";
        CoverEnum[CoverEnum["PaintBallEL"] = 7] = "PaintBallEL";
        CoverEnum[CoverEnum["PaintBallBuildings"] = 20] = "PaintBallBuildings";
        CoverEnum[CoverEnum["PaintBallAncillaryBuildings"] = 8] = "PaintBallAncillaryBuildings";
        CoverEnum[CoverEnum["PaintBallFixtures"] = 9] = "PaintBallFixtures";
        CoverEnum[CoverEnum["PaintBallPlayingSurfaces"] = 10] = "PaintBallPlayingSurfaces";
        CoverEnum[CoverEnum["PaintBallBusinessEquip"] = 11] = "PaintBallBusinessEquip";
        CoverEnum[CoverEnum["PaintBallTrophies"] = 12] = "PaintBallTrophies";
        CoverEnum[CoverEnum["PaintBallMachinery"] = 13] = "PaintBallMachinery";
        CoverEnum[CoverEnum["PaintBallStock"] = 14] = "PaintBallStock";
        CoverEnum[CoverEnum["PaintBallRefrigerated"] = 15] = "PaintBallRefrigerated";
        CoverEnum[CoverEnum["PaintBallPropertyInTransit"] = 16] = "PaintBallPropertyInTransit";
        CoverEnum[CoverEnum["PaintBallMoney"] = 17] = "PaintBallMoney";
        CoverEnum[CoverEnum["PaintBallRentPayable"] = 18] = "PaintBallRentPayable";
        CoverEnum[CoverEnum["PaintBallLossOfGrossProfit"] = 19] = "PaintBallLossOfGrossProfit";
        CoverEnum[CoverEnum["PaintBallPersonalAssault"] = 21] = "PaintBallPersonalAssault";
        CoverEnum[CoverEnum["PaintBallProdLiability"] = 22] = "PaintBallProdLiability";
        CoverEnum[CoverEnum["PaintBallProfLiability"] = 23] = "PaintBallProfLiability";
        CoverEnum[CoverEnum["PaintBallLossOfGrossRevenue"] = 24] = "PaintBallLossOfGrossRevenue";
        CoverEnum[CoverEnum["PaintBallLossOfGrossRentals"] = 25] = "PaintBallLossOfGrossRentals";
        CoverEnum[CoverEnum["PaintBallBookDebts"] = 26] = "PaintBallBookDebts";
        CoverEnum[CoverEnum["PaintBallCostOfWorking"] = 27] = "PaintBallCostOfWorking";
        CoverEnum[CoverEnum["AirsoftPL"] = 29] = "AirsoftPL";
        CoverEnum[CoverEnum["AirsoftEL"] = 30] = "AirsoftEL";
        CoverEnum[CoverEnum["AirsoftBuildings"] = 43] = "AirsoftBuildings";
        CoverEnum[CoverEnum["AirsoftAncillaryBuildings"] = 31] = "AirsoftAncillaryBuildings";
        CoverEnum[CoverEnum["AirsoftFixtures"] = 32] = "AirsoftFixtures";
        CoverEnum[CoverEnum["AirsoftPlayingSurfaces"] = 33] = "AirsoftPlayingSurfaces";
        CoverEnum[CoverEnum["AirsoftBusinessEquip"] = 34] = "AirsoftBusinessEquip";
        CoverEnum[CoverEnum["AirsoftTrophies"] = 35] = "AirsoftTrophies";
        CoverEnum[CoverEnum["AirsoftMachinery"] = 36] = "AirsoftMachinery";
        CoverEnum[CoverEnum["AirsoftStock"] = 37] = "AirsoftStock";
        CoverEnum[CoverEnum["AirsoftRefrigerated"] = 38] = "AirsoftRefrigerated";
        CoverEnum[CoverEnum["AirsoftPropertyInTransit"] = 39] = "AirsoftPropertyInTransit";
        CoverEnum[CoverEnum["AirsoftMoney"] = 40] = "AirsoftMoney";
        CoverEnum[CoverEnum["AirsoftRentPayable"] = 41] = "AirsoftRentPayable";
        CoverEnum[CoverEnum["AirsoftLossOfGrossProfit"] = 42] = "AirsoftLossOfGrossProfit";
        CoverEnum[CoverEnum["AirsoftPersonalAssault"] = 44] = "AirsoftPersonalAssault";
        CoverEnum[CoverEnum["AirsoftProdLiability"] = 45] = "AirsoftProdLiability";
        CoverEnum[CoverEnum["AirsoftProfLiability"] = 46] = "AirsoftProfLiability";
        CoverEnum[CoverEnum["AirsoftLossOfGrossRevenue"] = 47] = "AirsoftLossOfGrossRevenue";
        CoverEnum[CoverEnum["AirsoftLossOfGrossRentals"] = 48] = "AirsoftLossOfGrossRentals";
        CoverEnum[CoverEnum["AirsoftBookDebts"] = 49] = "AirsoftBookDebts";
        CoverEnum[CoverEnum["AirsoftCostOfWorking"] = 50] = "AirsoftCostOfWorking";
        CoverEnum[CoverEnum["MobileCateringPL"] = 51] = "MobileCateringPL";
        CoverEnum[CoverEnum["MobileCateringMD"] = 52] = "MobileCateringMD";
        CoverEnum[CoverEnum["MobileCateringEL"] = 53] = "MobileCateringEL";
        CoverEnum[CoverEnum["MobileCateringProd"] = 54] = "MobileCateringProd";
        CoverEnum[CoverEnum["MobileCateringStockGeneral"] = 55] = "MobileCateringStockGeneral";
        CoverEnum[CoverEnum["FreelanceCateringPL"] = 56] = "FreelanceCateringPL";
        CoverEnum[CoverEnum["SportsInstructorPL"] = 57] = "SportsInstructorPL";
        CoverEnum[CoverEnum["SportsInstructorEL"] = 70] = "SportsInstructorEL";
        CoverEnum[CoverEnum["SportsInstructorMD"] = 71] = "SportsInstructorMD";
        CoverEnum[CoverEnum["ShowmenPL"] = 72] = "ShowmenPL";
        CoverEnum[CoverEnum["ShowmenEL"] = 73] = "ShowmenEL";
        CoverEnum[CoverEnum["ShowmenMD"] = 74] = "ShowmenMD";
        CoverEnum[CoverEnum["ShowmenProducts"] = 75] = "ShowmenProducts";
        CoverEnum[CoverEnum["ShowmenBusinessEquip"] = 76] = "ShowmenBusinessEquip";
    })(CoverEnum = LeisureInsure.CoverEnum || (LeisureInsure.CoverEnum = {}));
})(LeisureInsure || (LeisureInsure = {}));
//function ShortSumInsured(number: number): string {
//    if (number >= 1000000) {
//        number = Math.round(number / 1000000);
//    }
//    return number.toString() + "M";
//} 
//# sourceMappingURL=PriceService.js.map