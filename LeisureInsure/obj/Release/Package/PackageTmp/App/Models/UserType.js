var LeisureInsure;
(function (LeisureInsure) {
    var UserType;
    (function (UserType) {
        UserType[UserType["Admin"] = 0] = "Admin";
        UserType[UserType["Underwriter"] = 1] = "Underwriter";
        UserType[UserType["Agent"] = 2] = "Agent";
        UserType[UserType["Customer"] = 3] = "Customer";
    })(UserType = LeisureInsure.UserType || (LeisureInsure.UserType = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=UserType.js.map