/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
var LeisureInsure;
(function (LeisureInsure) {
    var DetailsController = /** @class */ (function () {
        function DetailsController($http, $q, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies, locationService, chargeService, urlToNavigationService, metadataService, $timeout, priceService, commonDataService, postCodeService) {
            var _this = this;
            var initialisationPromises = [];
            var navigationData;
            this.heroImageSrc = "";
            this.landingPageDescription = "";
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.commonDataService = commonDataService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            this.postCodeService = postCodeService;
            locationService.onSelectedLocaleChanged($scope, function (locale) {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            });
            //load existing quote *******************************************************
            if (commonDataService.policyData != null) {
                this.policyData = commonDataService.policyData;
                this.InitMinDetails();
            }
            else {
                if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                    var quoteRef = $routeParams["QUOTEREFERENCE"];
                    var password = $routeParams["PASSCODE"];
                    urlToNavigationService.GetLandingPageNew(quoteRef, password)
                        .then(function (data) {
                        _this.navigationData = data;
                        if (_this.navigationData.policyId > 0) {
                            metadataService.setMetadata(_this.navigationData.title, _this.navigationData.description, _this.navigationData.keywords);
                            if (_this.locationService.selectedLocale != null) {
                                _this.countryCode = _this.locationService.selectedLocale.strLocale;
                                _this.currencySymbol = _this.locationService.selectedLocale.currencySymbol;
                            }
                            _this.policyData = {};
                            _this.InitMinDetails();
                        }
                    });
                    this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then(function (data) {
                        _this.policyData = data;
                        _this.InitMinDetails();
                        //existing quote so put in common service
                        _this.commonDataService.SetPolicyData(_this.policyData);
                        if (_this.locationService.selectedLocale != null) {
                            _this.locationService.selectedLocale.showFlag = false;
                        }
                    }).catch(function (errorMessage) {
                        eval("ShowUIDialog('An error has occured loading your quote and been logged.<br> Please retain your quote reference " + $routeParams['QUOTEREFERENCE'] + "');");
                    });
                }
            } //****************************************** End Constructor *************************************  
        }
        //the minimum required to show the page, picture, policyname, locale
        //this needs to show before the quote/questions reload
        DetailsController.prototype.InitMinDetails = function () {
            if (this.navigationData != null) {
                this.policyData.policyImage = this.navigationData.pictureUrl;
                this.policyData.policyName = this.navigationData.title;
            }
            if (this.policyData.addresses != null)
                this.selectedAddress = this.policyData.addresses[0];
        };
        DetailsController.prototype.Debug = function () {
            console.log("policyData", this.policyData);
            console.log("selectedAddress", this.selectedAddress);
            console.log("postCodeAddress", this.postCodeLookupAddress);
            console.log("listAddress", this.addressLookups);
        };
        DetailsController.prototype.getAddress = function () {
            var _this = this;
            this.postCodeService.GetAddresses(this.selectedAddress.postcode)
                .then(function (addresses) {
                _this.addressLookups = [];
                _.each(addresses, function (address) {
                    var addy = {};
                    addy.id = address.addressID;
                    addy.address1 = address.addressLine1;
                    addy.address2 = address.addressLine2;
                    addy.town = address.town;
                    addy.county = address.county;
                    addy.postcode = address.postCode;
                    _this.addressLookups.push(addy);
                });
            });
        };
        DetailsController.prototype.populateAddress = function () {
            var address1 = this.postCodeLookupAddress.address1;
            var address2 = this.postCodeLookupAddress.address2;
            var town = this.postCodeLookupAddress.town;
            var county = this.postCodeLookupAddress.county;
            var newAddress = {};
            if (this.policyData.addresses == null)
                this.policyData.addresses = [];
            if (this.policyData.addresses[0] == null)
                this.policyData.addresses[0] = newAddress;
            this.policyData.addresses[0].address1 = address1;
            this.policyData.addresses[0].address2 = address2;
            this.policyData.addresses[0].town = town;
            this.policyData.addresses[0].county = county;
            this.selectedAddress.address1 = address1;
            this.selectedAddress.address2 = address2;
            this.selectedAddress.town = town;
            this.selectedAddress.county = county;
        };
        DetailsController.prototype.PreviousPage = function () {
            this.location.path("/quotepage/" + this.policyData.quoteReference + "/" + this.policyData.password);
        };
        DetailsController.prototype.submitDetailsForm = function (form, valid) {
            var _this = this;
            if (valid) {
                //any old address need to be replaced with new ones
                this.policyData.addresses = [];
                this.policyData.addresses.push(this.selectedAddress);
                console.log("addressBeforeSave", this.policyData.addresses);
                //save address/contact then proceed to payment    
                if (form.$dirty) {
                    this.chargeService.SaveAddress(this.policyData)
                        .then(function (response) {
                        _this.policyData = response;
                        _this.commonDataService.SetPolicyData(_this.policyData);
                        _this.location.path("/payment/" + response.quoteReference + "/" + response.password);
                    });
                }
                else
                    this.location.path("/payment/" + this.policyData.quoteReference + "/" + this.policyData.password);
            }
        };
        DetailsController.routing = function ($routeProvider) {
            $routeProvider.when("/details", {
                controller: "DetailsController",
                templateUrl: "/app/detailspage/details.html",
                controllerAs: "DetailsController",
                metadata: {
                    title: "Leisure Insure Personal Details",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/details/:QUOTEREFERENCE/:PASSCODE", {
                controller: "DetailsController",
                templateUrl: "/app/detailspage/details.html",
                controllerAs: "DetailsController",
                metadata: {
                    title: "Leisure Insure Personal Details",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        DetailsController.$inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "PriceService", "CommonDataService", "PostCodeService"];
        return DetailsController;
    }());
    LeisureInsure.DetailsController = DetailsController;
    angular.module("App")
        .controller("DetailsController", DetailsController)
        .config(["$routeProvider", DetailsController.routing]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=DetailsController.js.map