var LeisureInsure;
(function (LeisureInsure) {
    var LookupAddress = /** @class */ (function () {
        function LookupAddress(id, address1, address2, town, county, postcode) {
            this.addressID = id;
            this.address1 = address1;
            this.address2 = address2;
            this.town = town;
            this.county = county;
            this.postcode = postcode;
        }
        return LookupAddress;
    }());
    LeisureInsure.LookupAddress = LookupAddress;
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=LookupAddress.js.map