﻿// Bitwise NOT
angular.module("App").filter("bitwiseNOT", function () {
    return function (number) {
        return (~number);
    };
});


angular.module("App").filter("unique", function() {
    return function(arr, field) {
        var o = {}, i, l = arr.length, r = [];
        for (i = 0; i < l; i += 1) {
            o[arr[i][field]] = arr[i];
        }
        for (i in o) {
            r.push(o[i]);
        }
        return r;
    };
});

angular.module("App").filter("orderObjectBy", function () {
    return function (input, attribute) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for (var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function (a, b) {
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            return b - a;
        });
        return array;
    }
});

angular.module("App").filter("max", function () {
    return function (input) {
        var out;
        if (input) {
            for (var i in input) {
                if (input[i] > out || out === undefined || out === null) {
                    out = input[i];
                }
            }
        }
        return out;
    };
});

angular.module("App").filter("unique", function () {
    return function (items, filterOn) {
        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});

angular.module("App").filter("slice", function () {
    return function (arr, start, end) {
        return arr.slice(start, end);
    };
});

// Bitwise AND
angular.module("App").filter("bitwiseANDInputs", function () {
    return function (parentPK, inputs) {
        var filteredinputs = [];
        filteredinputs = $filter("bitwiseAND")(inputs, { childBW: parentPK });
        return (filteredinputs);
    };
});

// Bitwise AND
angular.module("App").filter("bitwiseAND", function () {
    return function (firstNumber, secondNumber) {
        return (firstNumber & secondNumber);
    };
});

// Bitwise OR
angular.module("App").filter("bitwiseOR", function () {
    return function (firstNumber, secondNumber) {
        return (firstNumber | secondNumber);
    };
});

// Bitwise XOR
angular.module("App").filter("bitwiseXOR", function () {
    return function (firstNumber, secondNumber) {
        return (firstNumber ^ secondNumber);
    };
});

// Bitwise RIGHT SHIFT
angular.module("App").filter("bitwiseRS", function () {
    return function (firstNumber, secondNumber) {
        return (firstNumber >> secondNumber);
    };
});

// Bitwise LEFT SHIFT
angular.module("App").filter("bitwiseLS", function () {
    return function (firstNumber, secondNumber) {
        return (firstNumber << secondNumber);
    };
});

// Bitwise ZERO FILLED RIGHT SHIFT
angular.module("App").filter("bitwiseZFRS", function () {
    return function (firstNumber, secondNumber) {
        return (firstNumber >>> secondNumber);
    };
});
