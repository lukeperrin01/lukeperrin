﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export function safeUrl($sce) {
        return (input: string) => {
            return $sce.trustAsResourceUrl(input);
        }
    }

    angular.module("App").filter("safeUrl", ["$sce", safeUrl]);
}