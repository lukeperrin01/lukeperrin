﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export function nameFilter() {
        return (input: string) => {
            if (!input) {
                return input;
            }
            var lowerCase = input.toLowerCase();
            var words = lowerCase.split(" ", undefined);
            var capitalizedWords = new Array<string>();
            _.each(words, word => {
                var newWord = word.substr(0, 1).toUpperCase() + word.substr(1);
                capitalizedWords.push(newWord);
            });
            return capitalizedWords.join(" ");
        }
    }

    angular.module("App").filter("name", nameFilter);
}