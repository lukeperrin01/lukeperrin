﻿var ga;
var app = angular.module("App", ["ngRoute", "ngResource", "ui.bootstrap", "ngCookies", "ngAnimate", "ngSanitize",
    "angularMoment", "ngMessages", "slick", "LeisureInsure.Admin", "ngLocationUpdate", "ladda"])
    .config(LeisureInsure.Routes.configureRoutes)
    //.run(["PolicyUpdateDataService", (policyUpdateDataService) => {
    //    policyUpdateDataService.initialise("", 0, 0);
    //}])
    .run(["$rootScope", "$location", "AuthenticationService", ($rootScope, $location, authenticationService) => {

        $rootScope.loadingquestions = false;
        $rootScope.loadingquote = false;
        $rootScope.savingquote = false;
        $rootScope.loadingdocuments = false;
        $rootScope.loadingpurchase = false;
        $rootScope.searchingQuote = false;    
        $rootScope.passwordreset = false; 
        $rootScope.loggingin = false;                   
        $rootScope.loading = false;   

        //$rootScope.$on("$routeChangeSuccess", () => {
        //    debugger;
        //    setTimeout(() => {
        //        var location = $location.url();
        //        if (location.indexOf("/checkout/")) {
        //            location = "/checkout/quote";
        //        }
        //        if (location.indexOf("/quote/LEI00")) {
        //            location = "/quote/enteringdetails";
        //        }
        //        location.replace("", "");
        //        ga("send",
        //        {
        //            hittype: "pageview",
        //            page: location,
        //            title: window.document.title
        //            });
        //    }, 200);
        //});

        $rootScope.$on("$routeChangeStart", () => {
           
            var adminPage = ($location.url().substr(0, 7) === "/admin/" || $location.url() === "/admin");
            if (adminPage) {
                var userAuthenticated = authenticationService.isAuthenticated &&
                    (authenticationService.isAdmin() === true);
                if (!userAuthenticated) {
                    $rootScope.redirectUrl = $location.url();
                    $location.path("/login");
                }
            }
        });
    }]);