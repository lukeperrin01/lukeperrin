﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class DetailsController {
        static $inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "PriceService", "CommonDataService", "PostCodeService"];
        filter: any; // ng.IRootScopeService;
        window: ng.IWindowService;
        location: any;
        scope: any;
        rootScope: any;
        timeout: any;
        http: ng.IHttpService;
        q: ng.IQService;
        locationService: LocationService;
        locales: Array<ILocale>;
        chargeService: ChargeService;
        title: string;
        heroImageSrc: string;
        landingPageDescription: string;
        loadingPrice: boolean;
        priceService: PriceService;
        policyData: IPolicyView;
        commonDataService: CommonDataService;
        descriptionSrc: string;
        //have we attempted to get a price? used for validation        
        countryCode: string;
        navigationData: INavigationObject;
        currencySymbol: string;
        //used with postcode service
        postCodeService: PostCodeService;
        addressLookups: Array<IAddressView>;
        postCodeLookupAddress: IAddressView;
        selectedAddress: IAddressView;

        constructor($http, $q: ng.IQService, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies,
            locationService, chargeService, urlToNavigationService: UrlToNavigationService, metadataService: MetadataService,
            $timeout, priceService: PriceService, commonDataService: CommonDataService, postCodeService: PostCodeService) {
            var initialisationPromises = [];
            var navigationData: INavigationObject;
            this.heroImageSrc = "";
            this.landingPageDescription = "";
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.commonDataService = commonDataService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            this.postCodeService = postCodeService;


            locationService.onSelectedLocaleChanged($scope, (locale: ILocale) => {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            })

            //load existing quote *******************************************************
            if (commonDataService.policyData != null) {
                this.policyData = commonDataService.policyData;
                this.navigationData = this.policyData.landingPage;
                this.InitMinDetails();
            }
            else {
                if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                    var quoteRef = $routeParams["QUOTEREFERENCE"];
                    var password = $routeParams["PASSCODE"];                   

                    this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then((data: IPolicyView) => {

                            this.policyData = data;

                            this.navigationData = data.landingPage;

                            this.InitMinDetails();
                            //existing quote so put in common service
                            this.commonDataService.SetPolicyData(this.policyData);

                            if (this.locationService.selectedLocale != null) {
                                this.locationService.selectedLocale.showFlag = false;
                            }

                        }).catch((errorMessage: any) => {
                            eval("ShowUIDialog('An error has occured loading your quote and been logged.<br> Please retain your quote reference " + $routeParams['QUOTEREFERENCE'] + "');");
                        })
                }



            }//****************************************** End Constructor *************************************  
        }

        //the minimum required to show the page, picture, policyname, locale
        //this needs to show before the quote/questions reload
        InitMinDetails(): void {
           
            if (this.policyData.addresses != null)
                this.selectedAddress = this.policyData.addresses[0];
        }

        Debug(): void {
            console.log("policyData", this.policyData);
            console.log("selectedAddress", this.selectedAddress);
            console.log("postCodeAddress", this.postCodeLookupAddress);
            console.log("listAddress", this.addressLookups);
        }

        getAddress(): void {

            this.postCodeService.GetAddresses(this.selectedAddress.postcode)
                .then((addresses) => {
                    this.addressLookups = [];
                    _.each(addresses, (address: any) => {
                        let addy = <IAddressView>{};
                        addy.id = address.addressID;
                        addy.address1 = address.addressLine1;
                        addy.address2 = address.addressLine2;
                        addy.town = address.town;
                        addy.county = address.county;
                        addy.postcode = address.postCode;

                        this.addressLookups.push(addy);
                    });
                });
        }

        populateAddress(): void {

            var address1 = this.postCodeLookupAddress.address1;
            var address2 = this.postCodeLookupAddress.address2;
            var town = this.postCodeLookupAddress.town;
            var county = this.postCodeLookupAddress.county;
           
            var newAddress = <IAddressView>{};

            if (this.policyData.addresses == null)
                this.policyData.addresses = []

            if (this.policyData.addresses[0] == null)
                this.policyData.addresses[0] = newAddress;

            this.policyData.addresses[0].address1 = address1;
            this.policyData.addresses[0].address2 = address2;
            this.policyData.addresses[0].town = town;
            this.policyData.addresses[0].county = county;

            this.selectedAddress.address1 = address1;
            this.selectedAddress.address2 = address2;
            this.selectedAddress.town = town;
            this.selectedAddress.county = county;

            

        }

        PreviousPage(): void {

            this.location.path("/quotepage/" + this.policyData.quoteReference + "/" + this.policyData.password);
        }

        submitDetailsForm(form: any, valid: boolean): void {

            if (valid) {

                //any old address need to be replaced with new ones
                this.policyData.addresses = [];
                this.policyData.addresses.push(this.selectedAddress);
                console.log("addressBeforeSave", this.policyData.addresses);

                //save address/contact then proceed to payment    
                if (form.$dirty) {
                    this.chargeService.SaveAddress(this.policyData)
                        .then((response: IPolicyView) => {
                            this.policyData = response;
                            this.commonDataService.SetPolicyData(this.policyData);
                            this.location.path("/payment/" + response.quoteReference + "/" + response.password);
                        });
                }
                else
                    this.location.path("/payment/" + this.policyData.quoteReference + "/" + this.policyData.password);
            }
        }

        static routing($routeProvider) {
            $routeProvider.when("/details",
                {
                    controller: "DetailsController",
                    templateUrl: "/app/detailspage/details.html",
                    controllerAs: "DetailsController",
                    metadata: {
                        title: "Leisure Insure Personal Details",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/details/:QUOTEREFERENCE/:PASSCODE",
                {
                    controller: "DetailsController",
                    templateUrl: "/app/detailspage/details.html",
                    controllerAs: "DetailsController",
                    metadata: {
                        title: "Leisure Insure Personal Details",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });

        }

    }

    angular.module("App")
        .controller("DetailsController", DetailsController)
        .config(["$routeProvider", DetailsController.routing]);
}




