var LeisureInsure;
(function (LeisureInsure) {
    var FinalPaymentController = (function () {
        function FinalPaymentController($q, $http, $scope, $rootScope, $window, $location, $routeParams, chargeService, policyUpdateDataService, locationService, certificateService, $filter, $timeout, PostCodeService, PolicyService) {
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.certificateService = certificateService;
            this.postCodeService = PostCodeService;
            this.policyService = PolicyService;
            this.quotePk = this.chargeService.quoteId;
            this.password = this.chargeService.password;
            this.quoteReference = this.chargeService.quoteReference;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.q = $q;
            this.creditcards = [];
            this.addressLookups = [];
            this.selectedAddress = [];
            this.scope.alerts = [];
            this.scope.agentAlerts = [];
            this.instructors = 1;
            this.purchase = false;
        }
        FinalPaymentController.routing = function ($routeProvider) {
            $routeProvider.when("/payment", {
                controller: "FinalPaymentController",
                templateUrl: "/app/finalpayment/finalpayment.html",
                controllerAs: "finalpaymentController",
                metadata: {
                    title: "Leisure Insure - Shopping Basket",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/payment/:QUOTEREFERENCE/:PASSCODE", {
                controller: "FinalPaymentController",
                templateUrl: "/app/finalpayment/finalpayment.html",
                controllerAs: "finalpaymentController",
                metadata: {
                    title: "Leisure Insure - Shopping Basket",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        FinalPaymentController.prototype.Pay = function () {
            alert("submitted payment form!");
        };
        FinalPaymentController.$inject = [
            "$q", "$http", "$scope", "$rootScope", "$window", "$location", "$routeParams", "ChargeService", "PolicyUpdateDataService",
            "LocationService", "CertificateService", "$filter", "$timeout", "PostCodeService", "PolicyService"];
        return FinalPaymentController;
    }());
    LeisureInsure.FinalPaymentController = FinalPaymentController;
    angular.module("App")
        .controller("FinalPaymentController", FinalPaymentController)
        .config(["$routeProvider", FinalPaymentController.routing]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=FinalPaymentController.js.map