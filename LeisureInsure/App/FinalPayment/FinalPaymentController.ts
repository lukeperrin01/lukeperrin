﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
////$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$route"

module LeisureInsure {
    export class FinalPaymentController {
        static $inject = [
            "$q", "$http", "$scope", "$rootScope", "$window", "$location", "$routeParams", "ChargeService", "PolicyUpdateDataService",
            "LocationService", "CertificateService", "$filter", "$timeout", "PostCodeService", "PolicyService"];
        window: ng.IWindowService;
        location: any;
        scope: any;
        http: any;
        q: ng.IQService;
        filter: any;
        rootScope: any;
        creditcards: Array<IPaymentCard>;
        addressLookups: Array<IAddress>;
        selectedAddress: IAddress;
        locationService: LocationService;
        certificateService: CertificateService;
        postCodeService: PostCodeService;
        policyService: PolicyService;
        customers: Array<ICustomer>;
        addresses: Array<IAddress>;
        contactDetail: IContactDetail;
        selectedLocale: ILocale;
        quotePk: number;
        entityTypes: Array<IEntityTypes>;
        paymentCards: Array<IPaymentCard>;
        documents: Array<IDocument>;
        selectedPaymentCard: IPaymentCard;
        selectedPaymentOption: IPaymentOption;
        termsAccepted: boolean;
        Range: number;
        //referral details
        name: string;
        email: string;
        phone: string;
        clientName: string;
        purchase: boolean;
        entityTypeSelected: IEntityTypes;
        quoteReference: string;
        password: string;
        sortCode: string;
        account: string;
        accountType: string;
        businessName: string;
        title: string;
        dayOfMonth: string;
        accountHolder: boolean;
        accountConfirm: boolean;
        instructors: number;
        entityNamePlaceHolder: string;
        inputInstructors: Array<number>;
        contactName: string;
        contactEmail: string;
        certHref: string;
        renewal: number;
        dateFromDt: Date;
        dateToDt: Date;
        dateRequiresUpdating: boolean;
        dates: IDates;
        chargeService: ChargeService;


        constructor($q, $http, $scope, $rootScope, $window, $location, $routeParams, chargeService, policyUpdateDataService: PolicyUpdateDataService, locationService,
            certificateService, $filter, $timeout, PostCodeService, PolicyService) {
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.certificateService = certificateService;
            this.postCodeService = PostCodeService;
            this.policyService = PolicyService;
            this.quotePk = this.chargeService.quoteId;
            this.password = this.chargeService.password;
            this.quoteReference = this.chargeService.quoteReference;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.q = $q;
            this.creditcards = [];
            this.addressLookups = [];
            this.selectedAddress = [];
            //this.instructors = 5;
            this.scope.alerts = [];
            this.scope.agentAlerts = [];
            this.instructors = 1;
            this.purchase = false;                                                                     

        } //end constructor  *********************************************              

        static routing($routeProvider) {
            $routeProvider.when("/checkout",
                {
                    controller: "PaymentController",
                    templateUrl: "/app/paymentpage/payment.html",
                    controllerAs: "paymentController",
                    metadata: {
                        title: "Leisure Insure - Shopping Basket",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/checkout/:QUOTEREFERENCE/:PASSCODE",
                {
                    controller: "PaymentController",
                    templateUrl: "/app/paymentpage/payment.html",
                    controllerAs: "paymentController",
                    metadata: {
                        title: "Leisure Insure - Shopping Basket",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
        }

    } //end controller            

    angular.module("App")
        .controller("FinalPaymentController", FinalPaymentController)
        .config(["$routeProvider", FinalPaymentController.routing])

}











