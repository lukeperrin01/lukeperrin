var LeisureInsure;
(function (LeisureInsure) {
    var HomepageController = (function () {
        function HomepageController($cookies, $q, $window, $location, policyUpdateDataService, chargeService, locationService) {
            this.localeService = locationService;
            this.annualProducts = new Array();
            this.eventProducts = new Array();
            chargeService.resetChargeService();
            var strLocal = "";
            if (this.localeService.selectedLocale != null) {
                strLocal = this.localeService.selectedLocale.strLocale;
                if (this.localeService.selectedLocale != null) {
                    this.localeService.selectedLocale.showFlag = true;
                }
            }
            if ($cookies.get("cookiesAccepted")) {
                this.cookiesAccepted = $cookies.get("cookiesAccepted");
            }
            this.populateProduts(strLocal);
            $(".parallaxBg").parallax("50%", 0.2);
            $(".parallaxBg1").parallax("50%", 0.4);
        }
        HomepageController.prototype.showMore = function () {
            $("div.service-block-v7").css("height", "inherit");
            $("div.service-block-v7").css("min-height", "325px");
            $("div.service-block-v7>p").show();
            $("div.service-block-v7>h5").hide();
            var height = Math.max($("div.service-block-v7").innerHeight());
            $("div.service-block-v7").height(height);
        };
        HomepageController.prototype.LoadPolicy = function (link) {
            var flag = $("#flagImg").attr("data-country");
            var valid = true;
            if (link == "/quote/wedding-insurance") {
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }
            }
            if (link == "/quote/civil-ceremony-insurance") {
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }
            }
            if (link == "/quote/bar-and-bat-mitzvah-insurance") {
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }
            }
            if (link == "/quote/catering-brand-protection") {
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }
            }
            if (link == "/quote/showmen-and-fairground-insurance") {
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }
            }
            if (valid)
                window.location.href = link;
        };
        HomepageController.prototype.populateProdutsCountry = function (locale) {
            this.populateProduts(locale.strLocale);
        };
        HomepageController.prototype.populateProduts = function (locale) {
            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/equipment-hirers-insurance",
                    image: "/Content/Images/policies/leisure-equipment.jpg",
                    name: "New Leisure Equipment",
                    divname: "leisureequip"
                });
            }
            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/paintball-insurance",
                    image: "/Content/Images/policies/paintball-sm.png",
                    name: "New Paintball Site Operators",
                    divname: "paintsite"
                });
            }
            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/airsoft-insurance",
                    image: "/Content/Images/policies/airsoft.jpg",
                    name: "New Airsoft Site Operators",
                    divname: "airsite"
                });
            }
            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/mobile-catering-trailer-insurance",
                    image: "/Content/Images/policies/catering-trailer.jpg",
                    name: "New Mobile Catering Trailers",
                    divname: "mobilecatering"
                });
            }
            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/freelance-catering-insurance",
                    image: "/Content/Images/policies/freelance-caterer.jpg",
                    name: "New Freelance Catering",
                    divname: "freecatering"
                });
            }
            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/freelance-sports-instructor-insurance",
                    image: "/Content/Images/policies/sport-instructor.jpg",
                    name: "New Freelance Sports Instructors",
                    divname: "freesports"
                });
            }
            this.annualProducts.push({
                link: "/landingpage/showmen-and-fairground-insurance",
                image: "/Content/Images/policies/showmen.jpg",
                name: "New Showmen and Fairgrounds",
                divname: "showmen"
            });
        };
        HomepageController.routing = function ($routeProvider) {
            $routeProvider.when("/", {
                controller: "HomepageController",
                templateUrl: "/app/homepage/homepage.html",
                controllerAs: "homepageController",
                metadata: {
                    title: "Leisure Insure - Home Page",
                    description: "Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        HomepageController.$inject = ["$cookies", "$q", "$window", "$location", "PolicyUpdateDataService", "ChargeService", "LocationService"];
        return HomepageController;
    }());
    LeisureInsure.HomepageController = HomepageController;
    angular.module("App")
        .controller("HomepageController", HomepageController)
        .config(["$routeProvider", HomepageController.routing])
        .directive("owlCarousel", function () {
        return {
            restrict: "E",
            transclude: false,
            link: function (scope) {
                scope.initCarousel = function (element) {
                    var defaultOptions = {};
                    var customOptions = scope.$eval($(element).attr("data-options"));
                    for (var key in customOptions) {
                        defaultOptions[key] = customOptions[key];
                    }
                    $(element).owlCarousel(defaultOptions);
                };
            }
        };
    })
        .directive("owlCarouselItem", [function () {
            return {
                restrict: "A",
                transclude: false,
                link: function (scope, element) {
                    if (scope.$last) {
                        scope.initCarousel(element.parent());
                    }
                }
            };
        }]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=HomepageController.js.map