﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/angularjs/angular-route.d.ts" />



module LeisureInsure {
    export interface IHomePageProduct {
        link: string;
        image: string;
        name: string;
        divname: string;
    }

    export class HomepageController {
        static $inject = ["$cookies", "$q", "$window", "$location", "PolicyUpdateDataService", "ChargeService", "LocationService"];

        localeService: LocationService;
        scope: any;

        constructor($cookies, $q, $window, $location, policyUpdateDataService: PolicyUpdateDataService, chargeService: ChargeService, locationService) {

            //we are no longer loading https this way but doing it in web.config instead.. leaving this here just in case!
            //if (window.location.href.indexOf("azure") > -1 || window.location.href.indexOf("leisureinsure") > -1) {
            //    var ForceSSL = function () {
            //        if ($location.protocol() !== 'https') {
            //            $window.location.href = $location.absUrl().replace('http', 'https');
            //        }
            //    };
            //    ForceSSL();
            //}

            this.localeService = locationService;
            //this.localeService.initialised();
            //this.localeService.getlocals("");

            this.annualProducts = new Array();
            this.eventProducts = new Array();

            chargeService.resetChargeService();

            //make sure we update country from any previous selections
            var strLocal = "";
            if (this.localeService.selectedLocale != null) {
                strLocal = this.localeService.selectedLocale.strLocale;
                if (this.localeService.selectedLocale != null) {
                    this.localeService.selectedLocale.showFlag = true;
                }
            }           

            if ($cookies.get("cookiesAccepted")) {
                this.cookiesAccepted = $cookies.get("cookiesAccepted");
            }

            this.populateProduts(strLocal);

            ($(".parallaxBg") as any).parallax("50%", 0.2);
            ($(".parallaxBg1") as any).parallax("50%", 0.4);

        }

        annualProducts: Array<IHomePageProduct>;
        eventProducts: Array<IHomePageProduct>;

        cookiesAccepted: boolean;

        showMore() {
            $("div.service-block-v7").css("height", "inherit");
            $("div.service-block-v7").css("min-height", "325px");

            $("div.service-block-v7>p").show();
            $("div.service-block-v7>h5").hide();

            var height = Math.max($("div.service-block-v7").innerHeight());
            $("div.service-block-v7").height(height);
        }

        LoadPolicy(link: string) {

            var flag = $("#flagImg").attr("data-country");
            var valid = true;            

            if (link == "/quote/wedding-insurance") {
                //eval("ShowUIDialog('This policy is not availble online yet. Please call us: 01993 700 761');");
                //valid = false;
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }            
            }

            if (link == "/quote/civil-ceremony-insurance") {
                //eval("ShowUIDialog('This policy is not availble online yet. Please call us: 01993 700 761');");
                //valid = false;
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }             
            }

            if (link == "/quote/bar-and-bat-mitzvah-insurance") {
                //eval("ShowUIDialog('This policy is not availble online yet. Please call us: 01993 700 761');");
                //valid = false;
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }
            }

            if (link == "/quote/catering-brand-protection")
            {
                if (flag == "ga-IE") {
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");
                    valid = false;
                }
            }

            if (link == "/quote/showmen-and-fairground-insurance") {
                if (flag == "ga-IE") {                  
                    eval("ShowUIDialog('This policy is not availble in your country. Please call us: 01993 700 761');");                  
                    valid = false;
                }
            }
            if (valid)
                window.location.href = link;
        }

        populateProdutsCountry(locale: ILocale) {
            this.populateProduts(locale.strLocale);            
        }


        populateProduts(locale: string) {               


            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/equipment-hirers-insurance",
                    image: "/Content/Images/policies/leisure-equipment.jpg",
                    name: "New Leisure Equipment",
                    divname: "leisureequip"
                });
            }



            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/paintball-insurance",
                    image: "/Content/Images/policies/paintball-sm.png",
                    name: "New Paintball Site Operators",
                    divname: "paintsite"
                });
            }


            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/airsoft-insurance",
                    image: "/Content/Images/policies/airsoft.jpg",
                    name: "New Airsoft Site Operators",
                    divname: "airsite"
                });
            }
            

            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/mobile-catering-trailer-insurance",
                    image: "/Content/Images/policies/catering-trailer.jpg",
                    name: "New Mobile Catering Trailers",
                    divname: "mobilecatering"
                });
            }

            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/freelance-catering-insurance",
                    image: "/Content/Images/policies/freelance-caterer.jpg",
                    name: "New Freelance Catering",
                    divname: "freecatering"
                });
            }            

            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                this.annualProducts.push({
                    link: "/landingpage/freelance-sports-instructor-insurance",
                    image: "/Content/Images/policies/sport-instructor.jpg",
                    name: "New Freelance Sports Instructors",
                    divname: "freesports"
                });
            }                              


            //this.annualProducts.push({
            //    link: "/quote/freelance-activity-instructor-insurance",
            //    image: "/Content/Images/policies/instructors.jpg",
            //    name: "Freelance Adventure Instructors",
            //    divname: "freeadventure"
            //});

            this.annualProducts.push({
                link: "/landingpage/showmen-and-fairground-insurance",
                image: "/Content/Images/policies/showmen.jpg",
                name: "New Showmen and Fairgrounds",
                divname: "showmen"
            });

            


            //this.annualProducts.push({
            //    link: "/quote/showmen-and-fairground-insurance",
            //    image: "/Content/Images/policies/showmen.jpg",
            //    name: "Showmen and Fairgrounds",
            //    divname: "showmen"
            //});


            //this.annualProducts.push({
            //    link: "/quote/catering-brand-protection",
            //    image: "/Content/Images/policies/brand-protect.png",
            //    name: "Catering Brand Protection",
            //    divname: "cateringbrand"
            //});


            //this.annualProducts.push({
            //    link: "/quote/other-activities-insurance",
            //    image: "/Content/Images/policies/Karting.jpg",
            //    name: "Other Activities",
            //    divname: "otheractivities"
            //});
            //this.annualProducts.push({
            //    link: "/quote/fieldsports-insurance",
            //    image: "/Content/Images/policies/Fieldsports-Annual.jpg",
            //    name: "Fieldsports",
            //    divname: "fieldsports"
            //});
            //this.annualProducts.push({
            //    link: "/quote/stallholder-insurance",
            //    image: "/Content/Images/policies/stallholder.jpg",
            //    name: "Stallholders",
            //    divname: "stallholders"
            //});

            //this.eventProducts.push({
            //    link: "/quote/events-insurance",
            //    image: "/Content/Images/policies/event-organiser.jpg",
            //    name: "Event Organisers / Suppliers",
            //    divname: "eventorg"
            //});

            //this.eventProducts.push({
            //    link: "/quote/day-cover-for-inflatables-insurance",
            //    image: "/Content/Images/policies/day-cover-for-inflatables.jpg",
            //    name: "Day Cover for Inflatables",
            //    divname: "daycoverInf"
            //});

            //this.eventProducts.push({
            //    link: "/quote/street-party-insurance",
            //    image: "/Content/Images/policies/street-party.jpg",
            //    name: "Street Parties",
            //    divname: "streetparties"
            //});

            //this.eventProducts.push({
            //    link: "/quote/exhibitors-insurance",
            //    image: "/Content/Images/policies/exhibitor.jpg",
            //    name: "Exhibitors",
            //    divname: "exhibitors"
            //});
            //this.eventProducts.push({
            //    link: "/quote/bar-and-bat-mitzvah-insurance",
            //    image: "/Content/Images/policies/bar-mitzvah.jpg",
            //    name: "Bar and Bat Mitzvahs",
            //    divname: "barandbat"
            //});

           
            //this.eventProducts.push({
            //    link: "/quote/civil-ceremony-insurance",
            //    image: "/Content/Images/policies/civil-ceremony.jpg",
            //    name: "Civil Ceremonies",
            //    divname: "civilceromonies"
            //});

            //this.eventProducts.push({
            //    link: "/quote/conference-and-meetings-insurance",
            //    image: "/Content/Images/policies/conference.jpg",
            //    name: "Conferences and Meetings",
            //    divname: "confandmeetings"
            //});

          
            //this.eventProducts.push({
            //    link: "/quote/wedding-insurance",
            //    image: "/Content/Images/policies/wedding.jpg",
            //    name: "Weddings",
            //    divname: "weddings"
            //});

            //this.eventProducts.push({
            //    link: "/quote/fieldsports-event-insurance",
            //    image: "/Content/Images/policies/Fieldsports-Annual.jpg",
            //    name: "Fieldsports Event",
            //    divname: "fieldsportsevent"
            //});
        }


        static routing($routeProvider) {
            $routeProvider.when("/",
                {
                    controller: "HomepageController",
                    templateUrl: "/app/homepage/homepage.html",
                    controllerAs: "homepageController",
                    metadata: {
                        title: "Leisure Insure - Home Page",
                        description: "Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
        }
    }

    angular.module("App")
        .controller("HomepageController", HomepageController)
        .config(["$routeProvider", HomepageController.routing])
        .directive("owlCarousel", () => {
            return {
                restrict: "E",
                transclude: false,
                link: (scope: any) => {
                    scope.initCarousel = (element) => {
                        // provide any default options you want
                        var defaultOptions = {
                        };
                        var customOptions = scope.$eval($(element).attr("data-options"));
                        // combine the two options objects
                        for (var key in customOptions) {
                            defaultOptions[key] = customOptions[key];
                        }
                        // init carousel
                        (<any>$(element)).owlCarousel(defaultOptions);
                    };
                }
            };
        })
        .directive("owlCarouselItem", [() => {
            return {
                restrict: "A",
                transclude: false,
                link: (scope, element) => {
                    // wait for the last item in the ng-repeat then call init
                    if (scope.$last) {
                        scope.initCarousel(element.parent());
                    }
                }
            };
        }]);
}


