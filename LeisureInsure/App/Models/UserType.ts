﻿module LeisureInsure {
    export enum UserType {
        Admin,
        Underwriter,
        Agent,
        Customer
    }
}