var LeisureInsure;
(function (LeisureInsure) {
    var AccountController = (function () {
        function AccountController($http, $routeParams, $location, authenticationService, $rootScope) {
            this.showForm = true;
            this.http = $http;
            this.authenticationService = authenticationService;
            if ($routeParams["USERID"]) {
                this.userId = $routeParams["USERID"];
                this.token = $location.search().code;
            }
            this.rootScope = $rootScope;
            $("#allGifs").css("display", "block");
        }
        AccountController.prototype.retrievePassword = function () {
            var _this = this;
            this.rootScope.passwordreset = true;
            this.errorMessage = undefined;
            this.http.get("api/v1/account/forgottenpassword?username=" + this.username)
                .then(function () { _this.showForm = false; _this.rootScope.passwordreset = false; }, function (error) { _this.errorMessage = error.statusText; _this.rootScope.passwordreset = false; });
        };
        AccountController.prototype.resetPassword = function () {
            var _this = this;
            this.errorMessage = undefined;
            this.http.post("api/v1/account/resetpassword", {
                userId: this.userId,
                code: this.token,
                newPassword: this.password
            })
                .then(function () {
                _this.authenticationService.logout();
                _this.showForm = false;
            }, function (error) {
                _this.errorMessage = error.statusText;
                _this.rootScope.passwordreset = false;
            });
        };
        AccountController.routing = function ($routeProvider) {
            $routeProvider.when("/account/forgotten-password", {
                controller: "AccountController",
                templateUrl: "/app/account/account.html",
                controllerAs: "accountController",
                metadata: {
                    title: "Leisure Insure - Forgotten Password",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/account/reset-password/:USERID", {
                controller: "AccountController",
                templateUrl: "/app/account/reset.html",
                controllerAs: "accountController",
                metadata: {
                    title: "Leisure Insure - Forgotten Password",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        AccountController.$inject = ["$http", "$routeParams", "$location", "AuthenticationService", "$rootScope"];
        return AccountController;
    }());
    LeisureInsure.AccountController = AccountController;
    angular.module("App")
        .controller("AccountController", AccountController)
        .config(["$routeProvider", AccountController.routing]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=AccountController.js.map