var LeisureInsure;
(function (LeisureInsure) {
    var PLInputsController = (function () {
        function PLInputsController($http, $q, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies, policyUpdateDataService, locationService, chargeService, urlToNavigationService, metadataService, $timeout, priceService, commonDataService) {
            var _this = this;
            var initialisationPromises = [];
            var navigationData;
            this.heroImageSrc = "";
            this.landingPageDescription = "";
            this.policyUpdateDataService = policyUpdateDataService;
            this.locationService = locationService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.loadingPrice = false;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.chargeService = chargeService;
            this.priceService = priceService;
            this.commonDataService = commonDataService;
            this.commonDataService.priceChanged = false;
            this.rootScope.loading = true;
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            this.showExtraInfo = false;
            this.showLengthWidth = false;
            this.validOpCover = true;
            console.log("PLconstructor called");
            if (commonDataService.policyData == null) {
                if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                    var quoteRef = $routeParams["QUOTEREFERENCE"];
                    var password = $routeParams["PASSCODE"];
                    this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then(function (data) {
                        _this.policyData = data;
                        _this.navigationData = data.landingPage;
                        _this.InitMinDetails();
                        _this.BuildTable();
                        _this.InitCovers(false);
                        _this.commonDataService.SetPolicyData(_this.policyData);
                        var countryCode = "en-GB";
                        if (data.localeId == 2) {
                            countryCode = "ga-IE";
                        }
                    });
                }
                else {
                    urlToNavigationService.translateUrlNew("POLICYDESCRIPTION")
                        .then(function (data) {
                        _this.navigationData = data;
                        if (_this.navigationData.newPolicyId > 0) {
                            _this.policyData = {};
                            _this.InitMinDetails();
                            metadataService.setMetadata(_this.navigationData.title, _this.navigationData.description, _this.navigationData.keywords);
                        }
                        policyUpdateDataService.initialiseNew(_this.locationService.selectedLocale.strLocale, 0, _this.navigationData.newPolicyId)
                            .then(function (policyData) {
                            _this.policyData = policyData;
                            _this.InitMinDetails();
                            _this.BuildTable();
                            _this.InitCovers(true);
                        });
                    });
                }
            }
            else {
                this.navigationData = commonDataService.policyData.landingPage;
                this.policyData = commonDataService.policyData;
                this.BuildTable();
                this.InitCovers(false);
                this.InitMinDetails();
            }
        }
        PLInputsController.prototype.ShowCoverSection = function (coverTypeId, coverid) {
            var covers = this.policyData.covers.filter(function (x) { return x.coverTypeId == coverTypeId; });
            _.each(covers, function (cover) {
                cover.showCover = true;
            });
            if (coverTypeId == 3)
                this.policyData.mdSectionAdded = true;
            if (coverTypeId == 22)
                this.policyData.biSectionAdded = true;
            this.GetPrice();
        };
        PLInputsController.prototype.HideCoverSection = function (coverTypeId, coverid) {
            var covers = this.policyData.covers.filter(function (x) { return x.coverTypeId == coverTypeId; });
            _.each(covers, function (cover) {
                cover.coverRequired = false;
                cover.showCover = false;
            });
            if (coverTypeId == 3)
                this.policyData.mdSectionAdded = false;
            if (coverTypeId == 22)
                this.policyData.biSectionAdded = false;
            this.GetPrice();
        };
        PLInputsController.prototype.InitMinDetails = function () {
            var _this = this;
            _.each(this.policyData.covers, function (cover) {
                if (cover.coverType == "Optional") {
                    cover.question = "Do you require " + cover.coverHeader + "?";
                    if (cover.sectionType)
                        cover.question = cover.coverHeader;
                    if (cover.requiredParent != null) {
                        cover.question = cover.coverHeader + " *";
                    }
                    if (cover.requiredChild != null) {
                        if (cover.requiredChild < 0) {
                            var id = Math.abs(cover.requiredChild);
                            var parentCover = _this.policyData.covers.filter(function (x) { return x.id == id; })[0];
                            cover.requiredInfo = "Only available with " + parentCover.coverHeader;
                        }
                        else {
                            cover.requiredInfo = "Only available with a cover marked with an *";
                        }
                    }
                }
            });
        };
        PLInputsController.prototype.InitCovers = function (newquote) {
            var _this = this;
            this.policyData.localeId = this.locationService.selectedLocale.localePK;
            this.validManCover = true;
            var manCover = this.policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            if (!newquote) {
                _.each(manCover.table.rows, function (row) {
                    _.each(row.cells, function (cell) {
                        if (manCover.turnover != null) {
                            if (cell.turnover == manCover.turnover)
                                row.selected = true;
                        }
                        else {
                            if (cell.indemnity == manCover.indemnity)
                                row.selected = true;
                        }
                    });
                });
                var mdCovers = this.policyData.covers.filter(function (x) { return x.coverTypeId == 3 && x.coverRequired == true; });
                if (mdCovers.length > 0)
                    this.policyData.mdSectionAdded = true;
                var biCovers = this.policyData.covers.filter(function (x) { return x.coverTypeId == 22 && x.coverRequired == true; });
                if (biCovers.length > 0)
                    this.policyData.biSectionAdded = true;
            }
            _.each(this.policyData.covers, function (cover) {
                if (newquote) {
                    var addlistItem = cover.rates.filter(function (x) { return x.rateTypeId == 45; })[0];
                    if (addlistItem != null)
                        cover.itemsAdded = [];
                    cover.ernExempt = false;
                    cover.net = 0;
                    _this.policyData.refer = false;
                    _this.policyData.mdSectionAdded = false;
                    _this.policyData.biSectionAdded = false;
                }
            });
            var tradeMember = manCover.rates.filter(function (x) { return x.rateTypeId == 4; })[0];
            if (tradeMember != null) {
                if (tradeMember.inputSelected != null) {
                    if (tradeMember.inputSelected.rateName == "No") {
                        this.RemoveTradeMember();
                    }
                    else
                        this.AddTradeMember();
                }
            }
        };
        PLInputsController.prototype.AddTradeMember = function () {
            var manCover = this.policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            this.policyData.tradeMember = true;
            var excessRates = manCover.rates.filter(function (x) { return x.rateTypeId == 7; });
            _.each(excessRates, function (rate) {
                rate.display = 0;
            });
            var row = _.findWhere(manCover.table.rows, { rowid: 3 });
            row.selected = true;
            var oneMrow = _.findWhere(manCover.table.rows, { rowid: 1 });
            var oneMcell = _.findWhere(oneMrow.cells, { coverid: manCover.id });
            var profLiab = this.policyData.covers.filter(function (x) { return x.coverTypeId == 24; })[0];
            profLiab.indemnity = oneMcell.indemnity;
            this.SelectIndemnity(row.rowid, manCover.id, this.policyForm);
        };
        PLInputsController.prototype.RemoveTradeMember = function () {
            var manCover = this.policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            this.policyData.tradeMember = false;
            var excessRates = manCover.rates.filter(function (x) { return x.rateTypeId == 7; });
            _.each(excessRates, function (rate) {
                rate.display = 1;
            });
            var profLiab = this.policyData.covers.filter(function (x) { return x.coverTypeId == 24; })[0];
            profLiab.indemnity = 0;
            this.GetPrice();
        };
        PLInputsController.prototype.ShowCover = function (coverid) {
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            cover.coverRequired = true;
        };
        PLInputsController.prototype.HideCover = function (coverid) {
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            cover.coverRequired = false;
        };
        PLInputsController.prototype.CheckRequiredCover = function (cover) {
            if (cover.requiredParent != null) {
            }
            return true;
        };
        PLInputsController.prototype.AddOptionalCover = function (form, coverid) {
            var cover = this.policyData.covers.filter(function (x) { return x.id == coverid; })[0];
            var validParentCover = false;
            var addcover = this.policyData.covers.filter(function (x) { return x.otherCoverAdded != null && x.id != coverid && x.coverRequired == true; })[0];
            if (addcover != null) {
                if (addcover.otherCoverAdded == cover.id)
                    return;
            }
            if (cover.requiredChild != null) {
                if (cover.requiredChild < 0) {
                    var id = Math.abs(cover.requiredChild);
                    var parentCover = this.policyData.covers.filter(function (x) { return x.id == id; })[0];
                    if (parentCover.coverRequired)
                        validParentCover = true;
                }
                else {
                    var parentCovers = this.policyData.covers.filter(function (x) { return x.requiredParent == cover.requiredChild; });
                    _.each(this.policyData.covers, function (parentcover) {
                        if (parentcover.coverRequired && parentcover.requiredParent == cover.requiredChild)
                            validParentCover = true;
                    });
                }
            }
            else
                validParentCover = true;
            this.validManCover = this.ValidateMandatoryCover(form, coverid);
            if (this.validManCover && validParentCover) {
                cover.coverRequired = true;
                this.GetPrice();
            }
            form.$dirty = true;
        };
        PLInputsController.prototype.RemoveOptionalCover = function (form, coverid) {
            var _this = this;
            var cover = this.policyData.covers.filter(function (x) { return x.id == coverid; })[0];
            cover.coverRequired = false;
            var childid = (cover.id * -1);
            var childCover = this.policyData.covers.filter(function (x) { return x.requiredChild == childid; })[0];
            if (childCover != null)
                childCover.coverRequired = false;
            if (cover.requiredParent == 1) {
                var otherParentCovers = this.policyData.covers.filter(function (x) { return x.requiredParent == 1 && x.id != cover.id && x.coverRequired == true; });
                if (otherParentCovers.length == 0) {
                    var childCovers = this.policyData.covers.filter(function (x) { return x.requiredChild == 1 && x.coverRequired == true; });
                    _.each(childCovers, function (childcover) {
                        childcover.coverRequired = false;
                        var addedCoversInSection = _this.policyData.covers.filter(function (x) { return x.coverTypeId == childcover.coverTypeId && x.coverRequired == true; });
                        if (addedCoversInSection.length == 0) {
                            var allCoversInSection = _this.policyData.covers.filter(function (x) { return x.coverTypeId == childcover.coverTypeId; });
                            if (childcover.coverTypeId == 3)
                                _this.policyData.mdSectionAdded = false;
                            if (childcover.coverTypeId == 22)
                                _this.policyData.biSectionAdded = false;
                        }
                    });
                }
            }
            var coverRate = cover.rates.filter(function (x) { return x.extraCoverId != null; })[0];
            if (coverRate != null) {
                var addedCover = this.policyData.covers.filter(function (x) { return x.id == coverRate.extraCoverId; })[0];
                addedCover.coverRequired = false;
            }
            this.GetPrice();
            form.$dirty = true;
        };
        PLInputsController.prototype.Debug = function () {
            console.log(this.policyData, "policydata");
            _.each(this.policyData.covers, function (cover) {
                console.log(cover.coverName + " net:" + cover.net + "excess:" + cover.excess + " suminsured:" + cover.sumInsured + "indemnity" + cover.indemnity);
            });
            console.log(this.validManCover, "validManCover");
            console.log(this.locationService, "location service");
        };
        PLInputsController.prototype.BuildTable = function () {
            _.each(this.policyData.covers, function (cover) {
                var tablerates = cover.rates.filter(function (x) { return x.tableParentRatePK > 0 || x.tableChildren > 0; });
                if (tablerates.length > 0) {
                    var columnRates = tablerates.filter(function (x) { return x.tableFirstColumn > 0; });
                    var dataColIndex = 0;
                    var dataCols = {};
                    var colname = columnRates[0].rateName;
                    var datacolumn;
                    datacolumn = [];
                    var allDataCols = [];
                    _.each(columnRates, function (col) {
                        if (colname != col.rateName) {
                            colname = col.rateName;
                            allDataCols[dataColIndex] = [];
                            allDataCols[dataColIndex] = datacolumn;
                            dataColIndex++;
                            datacolumn = [];
                        }
                        datacolumn.push(col);
                    });
                    allDataCols[dataColIndex] = [];
                    allDataCols[dataColIndex] = datacolumn;
                    var rows = allDataCols[0].length + 1;
                    var cellDetails;
                    cellDetails = [];
                    var selectColumn;
                    selectColumn = [];
                    var description = "";
                    for (var x = 0; x < rows; x++) {
                        if (x == 0) {
                            var cellItem;
                            cellItem = {
                                description: "Select",
                                cellid: x,
                                rateid: 0,
                                header: true,
                                divby: 0,
                                rate: 0,
                                coverid: 0,
                                indemnity: 0,
                                turnover: 0
                            };
                            selectColumn.push(cellItem);
                        }
                        else {
                            var cellItem;
                            cellItem = {
                                description: "Radio",
                                cellid: x,
                                rateid: 0,
                                header: false,
                                divby: 0,
                                rate: 0,
                                coverid: 0,
                                indemnity: 0,
                                turnover: 0
                            };
                            selectColumn.push(cellItem);
                        }
                    }
                    cellDetails[0] = selectColumn;
                    var colname = "";
                    var dataColumn;
                    for (var a = 0; a < allDataCols.length; a++) {
                        dataColumn = [];
                        for (var x = 0; x < rows; x++) {
                            if (x == 0) {
                                var cellItem;
                                var header = allDataCols[a][x];
                                cellItem = {
                                    description: header.rateName,
                                    cellid: x,
                                    rateid: 0,
                                    header: true,
                                    divby: 0,
                                    rate: 0,
                                    coverid: header.coverId,
                                    indemnity: 0,
                                    turnover: 0
                                };
                                dataColumn.push(cellItem);
                            }
                            else {
                                var item = void 0;
                                item = allDataCols[a][x - 1];
                                if (item.extraCoverId != null)
                                    item.coverId = item.extraCoverId;
                                var cellItem;
                                cellItem = {
                                    description: item.rateValue,
                                    cellid: x,
                                    rateid: item.id,
                                    header: false,
                                    divby: item.divBy,
                                    rate: item.rate,
                                    coverid: item.coverId,
                                    indemnity: item.indemnity,
                                    turnover: item.turnover
                                };
                                dataColumn.push(cellItem);
                            }
                        }
                        cellDetails[a + 1] = dataColumn;
                    }
                    var newRow;
                    var cellDetailsNew;
                    cellDetailsNew = [];
                    var cells;
                    var tablerows;
                    tablerows = [];
                    for (var x = 0; x < rows; x++) {
                        cells = [];
                        for (var y = 0; y < cellDetails.length; y++) {
                            var cell = cellDetails[y][x];
                            cells.push(cell);
                        }
                        newRow =
                            {
                                selected: false,
                                cells: cells,
                                rowid: x
                            };
                        tablerows[x] = newRow;
                    }
                    cover.table =
                        {
                            rowCount: rows,
                            columnCount: selectColumn.length,
                            rows: tablerows
                        };
                }
            });
        };
        PLInputsController.prototype.SelectIndemnity = function (rowid, coverid, form) {
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            _.each(cover.table.rows, function (row) {
                if (row.rowid != rowid)
                    row.selected = false;
            });
            if (form != null) {
                form.indemTable.$setValidity('required', true);
                form.indemTable.$validate();
            }
            this.GetPrice();
        };
        PLInputsController.prototype.optionSelected = function (rate) {
            var cover = _.findWhere(this.policyData.covers, { id: rate.coverId });
            $("#" + rate.inputName + "").removeClass("has-error");
            if (rate.inputSelected != null) {
                if (rate.inputSelected.selectParent != null) {
                    var itemTypeId = rate.inputSelected.itemTypeId;
                    var itemRates = _.where(cover.rateItems, { itemTypeId: itemTypeId });
                    var options = Array();
                    _.each(itemRates, function (rateitem) {
                        var childrate = _.findWhere(cover.rates, { id: rateitem.rateId });
                        options.push(childrate);
                    });
                    var childSelect = _.findWhere(cover.rates, { selectChild: rate.inputSelected.selectParent });
                    childSelect.options = options;
                }
                if (rate.rateTypeId == 4) {
                    if (rate.inputSelected.rateName == "No")
                        this.RemoveTradeMember();
                    else
                        this.AddTradeMember();
                }
                if (rate.rateTypeId == 7) {
                    this.GetPrice();
                }
                if (rate.rateTypeId == 6) {
                    this.GetPrice();
                }
                this.ShowHideQuestions(rate, null);
            }
        };
        PLInputsController.prototype.ShowHideQuestions = function (rate, show) {
            var cover = _.findWhere(this.policyData.covers, { id: rate.coverId });
            var rate;
            if (rate.inputSelected != null)
                rate = rate.inputSelected;
            else
                rate = rate;
            if (rate.visibleParent != null) {
                var display = 0;
                var visibleid = rate.visibleParent;
                if (show != null) {
                    if (show)
                        display = 1;
                    else
                        display = 0;
                }
                else {
                    if (visibleid > 0) {
                        display = 1;
                    }
                    if (visibleid < 0) {
                        var visibleid = Math.abs(visibleid);
                        display = 0;
                    }
                }
                var visibleChildRates = _.where(cover.rates, { visibleChild: visibleid });
                _.each(visibleChildRates, function (rate) {
                    rate.display = display;
                });
            }
            if (rate.inputTypeId == 20) {
                if (rate.heightwidth == true)
                    this.showLengthWidth = true;
                else
                    this.showLengthWidth = false;
            }
        };
        PLInputsController.prototype.CheckRate = function (rate) {
            var addedCover;
            if (rate.extraCoverId != null) {
                addedCover = this.policyData.covers.filter(function (x) { return x.id == rate.extraCoverId; })[0];
                if (rate.rateValue == "true") {
                    addedCover.coverRequired = true;
                }
                else
                    addedCover.coverRequired = false;
                this.priceService.GetPrice(this.policyData);
            }
        };
        PLInputsController.prototype.FormatCurrency = function (coverid, rateid) {
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            var itemSpecs = _.findWhere(cover.rates, { id: rateid });
            var input = "";
            var currency = this.locationService.selectedLocale.currencySymbol;
            if (itemSpecs.extraCover != null)
                input = itemSpecs.extraCover;
            else
                input = itemSpecs.rateValue;
            if (input == currency || input == "") {
                input = null;
                itemSpecs.sumInsured = null;
                itemSpecs.rateValue = "";
            }
            if (input != null) {
                input = input.replace(/\D/g, '');
                var inputAsNum = parseInt(input);
                input = input.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                if (input != "")
                    input = currency + input;
                if (itemSpecs.extraCover != null) {
                    itemSpecs.extraCover = input;
                    itemSpecs.sumInsured = inputAsNum;
                }
                else {
                    itemSpecs.rateValue = input;
                    itemSpecs.rateValueNum = inputAsNum;
                }
                if (itemSpecs.extraCoverId != null) {
                    if (input != "") {
                        var extracover = _.findWhere(this.policyData.covers, { id: itemSpecs.extraCoverId });
                        extracover.coverRequired = true;
                    }
                }
                var show;
                if (input != "")
                    show = true;
                else
                    show = false;
                if (itemSpecs.visibleParent != null)
                    this.ShowHideQuestions(itemSpecs, show);
                if (itemSpecs.rateTypeId == 10 || itemSpecs.rateTypeId == 6)
                    this.GetPrice();
            }
        };
        PLInputsController.prototype.FormComplete = function (form) {
            this.policyForm = form;
            this.rootScope.loading = false;
        };
        PLInputsController.prototype.ValidateNoItems = function (form, noItems) {
            if (noItems != null)
                form.noItems.$setValidity('required', true);
            else
                form.noItems.$setValidity('required', false);
        };
        PLInputsController.prototype.ValidateWidth = function (form, width) {
            if (width != null)
                form.width.$setValidity('required', true);
            else
                form.width.$setValidity('required', false);
        };
        PLInputsController.prototype.ValidateLength = function (form, length) {
            if (length != null)
                form.length.$setValidity('required', true);
            else
                form.length.$setValidity('required', false);
        };
        PLInputsController.prototype.ValidateDimensions = function (form, coverid) {
            var validDimensions = true;
            var mancover = this.policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            if (mancover.itemsAdded != null) {
                if (mancover.itemsAdded.length > 0 && mancover.id != coverid)
                    validDimensions = true;
                else {
                    var dimensionList = mancover.rates.filter(function (x) { return x.inputTypeId == 21 || x.inputTypeId == 23 || x.inputTypeId == 28; });
                    var dimensions = dimensionList[0];
                    if (dimensions.inputTypeId == 21) {
                        if (dimensions.noItems == null)
                            form.noItems.$setValidity('required', false);
                        else
                            form.noItems.$setValidity('required', true);
                        if (this.showLengthWidth) {
                            if (dimensions.length == null)
                                form.length.$setValidity('required', false);
                            else
                                form.length.$setValidity('required', true);
                            if (dimensions.width == null)
                                form.width.$setValidity('required', false);
                            else
                                form.width.$setValidity('required', true);
                        }
                        if (this.showLengthWidth)
                            validDimensions = form.noItems.$valid && form.length.$valid && form.width.$valid;
                        else
                            validDimensions = form.noItems.$valid;
                    }
                }
            }
            return validDimensions;
        };
        PLInputsController.prototype.ValidateMandatoryCover = function (form, coverid) {
            var validTable = false;
            var validTradeMember = true;
            var valid = false;
            var mancover = this.policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            if (mancover.table != null) {
                validTable = this.ValidateTable(mancover.table, form);
            }
            var tradeMember = mancover.rates.filter(function (x) { return x.rateTypeId == 4; })[0];
            if (tradeMember != null) {
                if (tradeMember.inputSelected == null) {
                    validTradeMember = false;
                    $("#" + tradeMember.inputName + "").addClass("has-error");
                }
            }
            valid = validTable && validTradeMember;
            this.validManCover = valid;
            return valid;
        };
        PLInputsController.prototype.ValidateOptionalCover = function (cover) {
            var valid = true;
            var selectList = cover.rates.filter(function (x) { return x.rateTypeId == 44; })[0];
            if (selectList != null) {
                if (selectList.inputSelected != null)
                    valid = valid && true;
                else {
                    valid = valid && false;
                    $("#" + selectList.inputName + "").addClass("has-error");
                }
            }
            var itemValue = cover.rates.filter(function (x) { return x.rateTypeId == 43; })[0];
            if (itemValue != null) {
                var validItem = false;
                if (itemValue.rateValue != null)
                    validItem = true;
                valid = valid && validItem;
            }
            return valid;
        };
        PLInputsController.prototype.AddListItem = function (rate, form) {
            var cover = _.findWhere(this.policyData.covers, { id: rate.coverId });
            var mancover = this.policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            var dimensionList = cover.rates.filter(function (x) { return x.inputTypeId == 21 || x.inputTypeId == 23 || x.inputTypeId == 27 || x.inputTypeId == 28; });
            var dimensions = dimensionList[0];
            var validOptCover = true;
            var validDimensions = true;
            var valid = true;
            if (cover.id != mancover.id) {
                this.validManCover = this.ValidateMandatoryCover(form, rate.coverId);
                validOptCover = this.ValidateOptionalCover(cover);
                if (mancover.net == 0)
                    this.validManCover = false;
                if (mancover.itemsAdded != null) {
                    if (mancover.itemsAdded.length == 0)
                        valid = false;
                }
                this.validOpCover = true;
            }
            else {
                this.validManCover = this.ValidateMandatoryCover(form, rate.coverId);
                validDimensions = this.ValidateDimensions(form, rate.coverId);
            }
            valid = this.validManCover && validOptCover && validDimensions;
            if (valid) {
                var item;
                var itemadded = _.findWhere(cover.rates, { id: rate.itemListId });
                var valueOfItem = _.findWhere(cover.rates, { rateTypeId: 43 });
                var valueAsString = "";
                if (valueOfItem != null)
                    valueAsString = valueOfItem.rateValue;
                var itemdescription = itemadded.inputSelected.rateName;
                var noItems, length, width, extrainfo, extraCoverId, extraCover, sumInsured;
                noItems = dimensions.noItems;
                length = dimensions.length;
                width = dimensions.width;
                extrainfo = dimensions.itemExtra;
                extraCoverId = dimensions.extraCoverId;
                extraCover = dimensions.extraCover;
                sumInsured = dimensions.sumInsured;
                var input = valueAsString.replace(/\D/g, '');
                var valueAsNum = parseInt(input);
                if (cover.coverTypeId == 3)
                    sumInsured = valueAsNum;
                if (noItems == null)
                    noItems = 1;
                var price;
                if (itemadded.inputSelected.rate != null)
                    price = itemadded.inputSelected.rate;
                item = {
                    quantity: noItems,
                    extraInfo: extrainfo,
                    length: length,
                    rateId: itemadded.inputSelected.id,
                    width: width,
                    premium: price,
                    itemDescription: itemdescription,
                    indemnity: 0,
                    rateValue: valueAsString,
                    rateValueNum: valueAsNum,
                    extraCover: extraCover,
                    extraCoverId: extraCoverId,
                    rate: itemadded.inputSelected.rate,
                    firstRate: itemadded.inputSelected.firstRate,
                    discountFirstRate: itemadded.inputSelected.discountFirstRate,
                    discountsubsequentRate: itemadded.inputSelected.discountSubsequentRate,
                    initialCharge: 0,
                    subsequentCharge: 0,
                    firstCharge: false,
                    sumInsured: sumInsured,
                    turnover: 0,
                    excess: 0,
                    selected: false,
                    activity: "",
                    wageRoll: 0,
                    indemnityPeriod: ""
                };
                dimensions.width = null;
                dimensions.length = null;
                dimensions.noItems = null;
                dimensions.extraCover = null;
                dimensions.sumInsured = null;
                dimensions.itemExtra = null;
                if (form.noItems != null)
                    form.noItems.$setValidity('required', true);
                if (this.showLengthWidth) {
                    form.length.$setValidity('required', true);
                    form.width.$setValidity('required', true);
                }
                cover.itemsAdded.push(item);
            }
            if (valid) {
                this.GetPrice();
            }
        };
        PLInputsController.prototype.YesNoSelected = function (rate, form) {
            var priceReady = this.ValidateMandatoryCover(form, rate.coverId);
            if (priceReady) {
                if (rate.rateTypeId == 23 || rate.rateTypeId == 18)
                    this.GetPrice();
            }
            form.$dirty = true;
        };
        PLInputsController.prototype.GetPrice = function () {
            this.policyData = this.priceService.GetPrice(this.policyData);
            if (this.policyForm != null)
                this.policyForm.$dirty = true;
            if (this.policyData.refer)
                window.scrollTo(0, document.body.scrollHeight);
        };
        PLInputsController.prototype.RemoveListItem = function (index, coverid) {
            var mancover = this.policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            cover.itemsAdded.splice(index, 1);
            this.GetPrice();
            this.ValidateMandatoryCover(this.policyForm, mancover.id);
        };
        PLInputsController.prototype.ValidateTable = function (table, form) {
            var valid = true;
            var row = table.rows.filter(function (x) { return x.selected == true; })[0];
            if (row == null) {
                form.indemTable.$setValidity('required', false);
                valid = false;
            }
            else {
                form.indemTable.$setValidity('required', true);
                valid = true;
            }
            return valid;
        };
        PLInputsController.prototype.submitPolicyForm = function (form, valid) {
            var _this = this;
            var mancover = this.policyData.covers.filter(function (x) { return x.coverType == "Mandatory"; })[0];
            var validtable = this.ValidateTable(mancover.table, form);
            if (this.policyData.refer)
                valid = false;
            if (mancover.net == 0)
                this.validManCover = false;
            var optionalCovers = this.policyData.covers.filter(function (x) { return x.coverType == "Optional" && x.coverRequired == true; });
            _.each(optionalCovers, function (opcover) {
                var button = opcover.rates.filter(function (x) { return x.inputTypeId == 10; })[0];
                if (button != null) {
                    if (opcover.itemsAdded.length == 0) {
                        _this.validOpCover = false;
                        valid = false;
                    }
                    else
                        _this.validOpCover = true;
                }
            });
            if (valid && validtable && this.validManCover) {
                if (form.$dirty == true) {
                    this.chargeService.SaveQuote(this.policyData)
                        .then(function (response) {
                        _this.policyData = response;
                        _this.commonDataService.SetPolicyData(_this.policyData);
                        _this.location.path("/details/" + response.quoteReference + "/" + response.password);
                    });
                }
                else
                    this.location.path("/details/" + this.policyData.quoteReference + "/" + this.policyData.password);
            }
        };
        PLInputsController.routing = function ($routeProvider) {
            $routeProvider.when("/quotepage/:POLICYDESCRIPTION", {
                controller: "PLInputsController",
                templateUrl: "/app/plinputspage/plinputs.html",
                controllerAs: "PLInputsController",
                metadata: {
                    title: "Leisure Insure - Public Liability",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/quotepage/:QUOTEREFERENCE/:PASSCODE", {
                controller: "PLInputsController",
                templateUrl: "/app/plinputspage/plinputs.html",
                controllerAs: "PLInputsController",
                metadata: {
                    title: "Leisure Insure - Public Liability",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        PLInputsController.$inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "PolicyUpdateDataService", "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "PriceService", "CommonDataService"];
        return PLInputsController;
    }());
    LeisureInsure.PLInputsController = PLInputsController;
    angular.module("App")
        .controller("PLInputsController", PLInputsController)
        .config(["$routeProvider", PLInputsController.routing]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=PLInputsController.js.map