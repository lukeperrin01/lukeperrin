﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class PLInputsController {
        static $inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "PolicyUpdateDataService", "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "PriceService", "CommonDataService"];
        filter: any; // ng.IRootScopeService;
        window: ng.IWindowService;
        location: any;
        scope: any;
        rootScope: any;
        timeout: any;
        http: ng.IHttpService;
        q: ng.IQService;
        policyUpdateDataService: PolicyUpdateDataService;
        locationService: LocationService;
        chargeService: ChargeService;
        title: string;
        heroImageSrc: string;
        landingPageDescription: string;        
        loadingPrice: boolean;
        priceService: PriceService;
        policyData: IPolicyView;
        descriptionSrc: string;
        //have we attempted to get a price? used for validation
        validManCover: boolean;
        validOpCover: boolean;
        commonDataService: CommonDataService;
        navigationData: INavigationObject;
        policyForm: any;
        showLengthWidth: boolean;
        showExtraInfo: boolean;

        invalidSelect: boolean;

        sections: number;

        constructor($http, $q: ng.IQService, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies,
            policyUpdateDataService: PolicyUpdateDataService, locationService, chargeService,
            urlToNavigationService: UrlToNavigationService, metadataService: MetadataService, $timeout, priceService: PriceService, commonDataService: CommonDataService) {
            var initialisationPromises = [];
            var navigationData: INavigationObject;
            this.heroImageSrc = "";
            this.landingPageDescription = "";
            this.policyUpdateDataService = policyUpdateDataService;
            this.locationService = locationService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.loadingPrice = false;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.chargeService = chargeService;
            this.priceService = priceService;
            this.commonDataService = commonDataService;
            this.commonDataService.priceChanged = false;
            this.rootScope.loading = true;
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            //var defer = $q.defer();
            //var initialiseDataPromise = defer.promise;
            this.showExtraInfo = false;
            this.showLengthWidth = false;
            this.validOpCover = true;


            console.log("PLconstructor called");

            //locationService.onSelectedLocaleChanged($scope, (locale: ILocale) => {
            //    $cookies.put("locale", locale.strLocale);
            //    $route.reload();
            //})

            //if (this.locationService.selectedLocale != null) {
            //    this.locationService.selectedLocale.showFlag = false;
            //}

            if (commonDataService.policyData == null) {

                if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                    var quoteRef = $routeParams["QUOTEREFERENCE"];
                    var password = $routeParams["PASSCODE"];                   

                    this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then((data: IPolicyView) => {
                            this.policyData = data;
                            this.navigationData = data.landingPage;

                            this.InitMinDetails();
                            this.BuildTable();
                            this.InitCovers(false);
                            //existing quote so put in common service
                            this.commonDataService.SetPolicyData(this.policyData);
                            var countryCode = "en-GB";
                            if (data.localeId == 2) {
                                countryCode = "ga-IE";
                            }
                        });

                }
                else {
                    //new quote ***********************************************************************************

                    urlToNavigationService.translateUrlNew("POLICYDESCRIPTION")
                        .then((data: INavigationObject) => {
                            this.navigationData = data;
                            if (this.navigationData.newPolicyId > 0) {
                                this.policyData = <IPolicyView>{};
                                this.InitMinDetails();

                                metadataService.setMetadata(this.navigationData.title,
                                    this.navigationData.description,
                                    this.navigationData.keywords);
                            }
                            //we need location service for a new quote, but when loading existing quote we have everything we need
                            policyUpdateDataService.initialiseNew(this.locationService.selectedLocale.strLocale, 0, this.navigationData.newPolicyId)
                                .then((policyData: IPolicyView) => {
                                    this.policyData = policyData;
                                    this.InitMinDetails();

                                    this.BuildTable();
                                    this.InitCovers(true);
                                    //defer.resolve();
                                });
                        });
                }
            }
            else {
                this.navigationData = commonDataService.policyData.landingPage;

                this.policyData = commonDataService.policyData;
                this.BuildTable();
                this.InitCovers(false);
                this.InitMinDetails();
            }


        }//****************************************** End Constructor *************************************  

        ShowCoverSection(coverTypeId: number, coverid: number): void {

            var covers = this.policyData.covers.filter(x => x.coverTypeId == coverTypeId);
            _.each(covers, (cover: ICoverView) => {                
                cover.showCover = true;
            });

            if (coverTypeId == 3)
                this.policyData.mdSectionAdded = true;
            if (coverTypeId == 22)
                this.policyData.biSectionAdded = true;

            this.GetPrice();
        }

        HideCoverSection(coverTypeId: number, coverid: number): void {

            var covers = this.policyData.covers.filter(x => x.coverTypeId == coverTypeId);

            _.each(covers, (cover: ICoverView) => {
                cover.coverRequired = false;
                cover.showCover = false;
            });

            if (coverTypeId == 3)
                this.policyData.mdSectionAdded = false;
            if (coverTypeId == 22)
                this.policyData.biSectionAdded = false;

            this.GetPrice();
        }


        //the minimum required to show the page, picture, policyname, locale
        //this needs to show before the quote/questions reload
        InitMinDetails(): void {
           

            _.each(this.policyData.covers, (cover: ICoverView) => {
                if (cover.coverType == "Optional") {
                    //if this has dependent child covers mark with asterix
                    cover.question = "Do you require " + cover.coverHeader + "?";
                    if (cover.sectionType)
                        cover.question = cover.coverHeader;
                    if (cover.requiredParent != null) {
                        cover.question = cover.coverHeader + " *";
                    }
                    if (cover.requiredChild != null) {
                        if (cover.requiredChild < 0) {
                            //requires specific cover
                            var id = Math.abs(cover.requiredChild);
                            var parentCover = this.policyData.covers.filter(x => x.id == id)[0];
                            cover.requiredInfo = "Only available with " + parentCover.coverHeader;
                        }
                        else {
                            cover.requiredInfo = "Only available with a cover marked with an *";
                        }
                    }
                }
            });
        }



        InitCovers(newquote: boolean): void {

            this.policyData.localeId = this.locationService.selectedLocale.localePK;
            this.validManCover = true;
            var manCover = this.policyData.covers.filter(x => x.coverType == "Mandatory")[0];

            if (!newquote) {
                //select row with our indemnity
                _.each(manCover.table.rows, (row: IRow) => {
                    _.each(row.cells, (cell: ICell) => {
                        if (manCover.turnover != null) {
                            if (cell.turnover == manCover.turnover)
                                row.selected = true;
                        }
                        else {
                            if (cell.indemnity == manCover.indemnity)
                                row.selected = true;
                        }
                    });
                });

                //set cover sections
                var mdCovers = this.policyData.covers.filter(x => x.coverTypeId == 3 && x.coverRequired == true);
                if (mdCovers.length > 0)
                    this.policyData.mdSectionAdded = true;

                var biCovers = this.policyData.covers.filter(x => x.coverTypeId == 22 && x.coverRequired == true);
                if (biCovers.length > 0)
                    this.policyData.biSectionAdded = true;


            }

            _.each(this.policyData.covers, (cover: ICoverView) => {
                if (newquote) {
                    //if we have an add items button then initialise the array for items
                    let addlistItem = cover.rates.filter(x => x.rateTypeId == 45)[0];
                    if (addlistItem != null)
                        cover.itemsAdded = [];
                    cover.ernExempt = false;
                    cover.net = 0;
                    this.policyData.refer = false;
                    this.policyData.mdSectionAdded = false;
                    this.policyData.biSectionAdded = false;
                }
            });

            //trade member?
            var tradeMember = manCover.rates.filter(x => x.rateTypeId == 4)[0];
            if (tradeMember != null) {
                if (tradeMember.inputSelected != null) {
                    if (tradeMember.inputSelected.rateName == "No") {
                        this.RemoveTradeMember();

                    }
                    else
                        this.AddTradeMember();
                }
            }
        }

        //£5 Million Public Liability for the price of £1 Million
        //Free Professional Indemnity cover with a limit of £1 Million
        AddTradeMember(): void {

            var manCover = this.policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            this.policyData.tradeMember = true;
            //hide excess question
            var excessRates = manCover.rates.filter(x => x.rateTypeId == 7);
            _.each(excessRates, (rate: IRateView) => {
                rate.display = 0;
            });

            //rowid 3 will always be 5M UK or 6.5M for Ireland
            var row = _.findWhere(manCover.table.rows, { rowid: 3 });
            row.selected = true;
            //get 1M from table, apply to professional liability
            var oneMrow = _.findWhere(manCover.table.rows, { rowid: 1 });
            var oneMcell = _.findWhere(oneMrow.cells, { coverid: manCover.id });
            var profLiab = this.policyData.covers.filter(x => x.coverTypeId == 24)[0];

            profLiab.indemnity = oneMcell.indemnity;
            this.SelectIndemnity(row.rowid, manCover.id, this.policyForm);
        }

        RemoveTradeMember(): void {
            var manCover = this.policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            this.policyData.tradeMember = false;
            //show excess question
            var excessRates = manCover.rates.filter(x => x.rateTypeId == 7);
            _.each(excessRates, (rate: IRateView) => {
                rate.display = 1;
            });
            var profLiab = this.policyData.covers.filter(x => x.coverTypeId == 24)[0];
            profLiab.indemnity = 0;
            this.GetPrice();
        }


        ShowCover(coverid: number) {
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            cover.coverRequired = true;

        }

        HideCover(coverid: number) {
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            cover.coverRequired = false;
        }

        CheckRequiredCover(cover: ICoverView): boolean {

            if (cover.requiredParent != null) {

            }
            return true;
        }

        AddOptionalCover(form: any, coverid: number): void {

            var cover = this.policyData.covers.filter(x => x.id == coverid)[0];
            var validParentCover = false;

            //check this cover can only be added if another cover is not added
            var addcover = this.policyData.covers.filter(x => x.otherCoverAdded != null && x.id != coverid && x.coverRequired == true)[0];
            if (addcover != null) {
                if (addcover.otherCoverAdded == cover.id)
                    return;
            }


            //check this cover doesnt have a dependent cover that needs to be added first
            //negative childId means a specific parent cover id needs to be added, otherwise they can refer to more than one 
            //required cover
            if (cover.requiredChild != null) {
                if (cover.requiredChild < 0) {
                    var id = Math.abs(cover.requiredChild);
                    var parentCover = this.policyData.covers.filter(x => x.id == id)[0];
                    if (parentCover.coverRequired)
                        validParentCover = true;
                }
                else {
                    var parentCovers = this.policyData.covers.filter(x => x.requiredParent == cover.requiredChild);
                    _.each(this.policyData.covers, (parentcover: ICoverView) => {
                        if (parentcover.coverRequired && parentcover.requiredParent == cover.requiredChild)
                            validParentCover = true;
                    });
                }
            }
            else
                validParentCover = true;

            this.validManCover = this.ValidateMandatoryCover(form, coverid);
            if (this.validManCover && validParentCover) {
                cover.coverRequired = true;
                this.GetPrice();
            }

            form.$dirty = true;
        }

        RemoveOptionalCover(form: any, coverid: number): void {

            var cover = this.policyData.covers.filter(x => x.id == coverid)[0];
            cover.coverRequired = false;

            //Also remove dependant (child) covers if this a parent
            //get negative number to find child
            var childid = (cover.id * -1)
            var childCover = this.policyData.covers.filter(x => x.requiredChild == childid)[0];
            if (childCover != null)
                childCover.coverRequired = false;
            //get all general parent covers (*)
            if (cover.requiredParent == 1) {
                //get all other added parent covers except this one
                var otherParentCovers = this.policyData.covers.filter(x => x.requiredParent == 1 && x.id != cover.id && x.coverRequired == true);
                if (otherParentCovers.length == 0) {
                    //remove child covers if this is the only parent
                    var childCovers = this.policyData.covers.filter(x => x.requiredChild == 1 && x.coverRequired == true);
                    _.each(childCovers, (childcover: ICoverView) => {
                        childcover.coverRequired = false;
                        //check if we have removed all covers in this section
                        var addedCoversInSection = this.policyData.covers.filter(x => x.coverTypeId == childcover.coverTypeId && x.coverRequired == true);
                        if (addedCoversInSection.length == 0) {
                            //close all covers
                            var allCoversInSection = this.policyData.covers.filter(x => x.coverTypeId == childcover.coverTypeId);

                            if (childcover.coverTypeId == 3)
                                this.policyData.mdSectionAdded = false;
                            if (childcover.coverTypeId == 22)
                                this.policyData.biSectionAdded = false;
                        }
                    });
                }
            }

            //Some covers also off the option of adding further covers.. (personal assault ) so remove these as well
            var coverRate = cover.rates.filter(x => x.extraCoverId != null)[0];
            if (coverRate != null) {
                var addedCover = this.policyData.covers.filter(x => x.id == coverRate.extraCoverId)[0];
                addedCover.coverRequired = false;
            }



            this.GetPrice();

            form.$dirty = true;
        }

        Debug(): void {

            //equip hirer PL cover           

            //var cover = _.findWhere(this.policyData.covers, { id: 1 });
            //var cover2 = _.findWhere(this.policyData.covers, { id: 2 });

            console.log(this.policyData, "policydata");
            //console.log(cover.table, "table");

            //var visibleRates: IRateView[]; visibleRates = [];
            ////PL for equipment hirer
            //var cover = _.findWhere(this.policyData.covers, { id: 1 });

            ////make sure the radio buttons update yes/no
            //_.each(cover.rates, (rate: IRateView) => {
            //    //get radio buttons
            //    if (rate.display == 1 && rate.inputTypeId == 19)
            //        visibleRates.push(rate);
            //});

            //check net /excess after getting a price
            _.each(this.policyData.covers, (cover: ICoverView) => {
                console.log(cover.coverName + " net:" + cover.net + "excess:" + cover.excess + " suminsured:" + cover.sumInsured + "indemnity" + cover.indemnity);
            });

            console.log(this.validManCover, "validManCover");

            //console.log(visibleRates, "radio buttons");
            console.log(this.locationService, "location service");
        }



        BuildTable(): void {
            //get all covers
            _.each(this.policyData.covers, (cover: ICoverView) => {

                //check if we have a table
                var tablerates = cover.rates.filter(x => x.tableParentRatePK > 0 || x.tableChildren > 0);
                if (tablerates.length > 0) {

                    var columnRates = tablerates.filter(x => x.tableFirstColumn > 0);
                    //break up columnrates into multiple arrays based on ratename
                    //first, get number of columns
                    var dataColIndex = 0;
                    var dataCols = {};

                    var colname = columnRates[0].rateName;

                    var datacolumn: IRateView[]; datacolumn = [];
                    var allDataCols = [];

                    //get all columns for each indemnities
                    _.each(columnRates, (col: IRateView) => {

                        if (colname != col.rateName) {
                            colname = col.rateName;
                            allDataCols[dataColIndex] = [];
                            allDataCols[dataColIndex] = datacolumn;
                            dataColIndex++;
                            datacolumn = [];
                        }
                        datacolumn.push(col);
                    });

                    allDataCols[dataColIndex] = [];
                    allDataCols[dataColIndex] = datacolumn;

                    var rows = allDataCols[0].length + 1;
                    var cellDetails: ICell[][]; cellDetails = [];
                    var selectColumn: ICell[]; selectColumn = [];


                    var description = "";

                    //populate first column
                    for (var x = 0; x < rows; x++) {
                        if (x == 0) {
                            var cellItem: ICell;
                            cellItem = {
                                description: "Select",
                                cellid: x,
                                rateid: 0,
                                header: true,
                                divby: 0,
                                rate: 0,
                                coverid: 0,
                                indemnity: 0,
                                turnover: 0

                            };
                            selectColumn.push(cellItem);
                        }
                        else {
                            var cellItem: ICell;
                            cellItem = {
                                description: "Radio",
                                cellid: x,
                                rateid: 0,
                                header: false,
                                divby: 0,
                                rate: 0,
                                coverid: 0,
                                indemnity: 0,
                                turnover: 0
                            };
                            selectColumn.push(cellItem);
                        }
                    }

                    cellDetails[0] = selectColumn;


                    var colname = "";

                    var dataColumn: ICell[];

                    for (var a = 0; a < allDataCols.length; a++) {

                        dataColumn = [];

                        for (var x = 0; x < rows; x++) {
                            if (x == 0) {
                                var cellItem: ICell;
                                var header = allDataCols[a][x];
                                cellItem = {
                                    description: header.rateName,
                                    cellid: x,
                                    rateid: 0,
                                    header: true,
                                    divby: 0,
                                    rate: 0,
                                    coverid: header.coverId,
                                    indemnity: 0,
                                    turnover: 0
                                };
                                dataColumn.push(cellItem);
                            }
                            else {
                                let item: IRateView;
                                item = allDataCols[a][x - 1];
                                //some rows will be for bundled in covers
                                if (item.extraCoverId != null)
                                    item.coverId = item.extraCoverId;

                                var cellItem: ICell;
                                cellItem = {
                                    description: item.rateValue,
                                    cellid: x,
                                    rateid: item.id,
                                    header: false,
                                    divby: item.divBy,
                                    rate: item.rate,
                                    coverid: item.coverId,
                                    indemnity: item.indemnity,
                                    turnover: item.turnover
                                };

                                dataColumn.push(cellItem);
                            }
                        }

                        cellDetails[a + 1] = dataColumn;
                    }



                    var newRow: IRow;

                    var cellDetailsNew: ICell[][]; cellDetailsNew = [];
                    var cells: ICell[];
                    var tablerows: IRow[]; tablerows = [];

                    //we need columns arranged in rows for our table
                    for (var x = 0; x < rows; x++) {
                        cells = [];
                        for (var y = 0; y < cellDetails.length; y++) {
                            var cell = cellDetails[y][x];
                            cells.push(cell);
                        }
                        newRow =
                            {
                                selected: false,
                                cells: cells,
                                rowid: x
                            }
                        tablerows[x] = newRow;
                    }

                    cover.table =
                        {
                            rowCount: rows,
                            columnCount: selectColumn.length,
                            rows: tablerows
                        };

                }
            });
        }

        SelectIndemnity(rowid: number, coverid: number, form: any): void {
            //reset all rows to unselected
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            _.each(cover.table.rows, (row: IRow) => {
                if (row.rowid != rowid)
                    row.selected = false;
            });

            //mark table as valid as we selected a row
            if (form != null) {
                form.indemTable.$setValidity('required', true);
                form.indemTable.$validate();
            }

            //keep our price dynamically updated
            this.GetPrice();
        }

        optionSelected(rate: IRateView): void {

            var cover = _.findWhere(this.policyData.covers, { id: rate.coverId });

            //remove error class 
            $("#" + rate.inputName + "").removeClass("has-error");

            if (rate.inputSelected != null) {
                //populate a secondary (child) select based on our category
                if (rate.inputSelected.selectParent != null) {
                    var itemTypeId = rate.inputSelected.itemTypeId;
                    //get current cover                    
                    var itemRates = _.where(cover.rateItems, { itemTypeId: itemTypeId });
                    var options: IRateView[] = Array();
                    //get all rates for the select we want to populate                
                    _.each(itemRates, (rateitem: IRateItemView) => {
                        var childrate = _.findWhere(cover.rates, { id: rateitem.rateId });
                        options.push(childrate);
                    });

                    //this is the select we want to populate
                    var childSelect = _.findWhere(cover.rates, { selectChild: rate.inputSelected.selectParent });
                    childSelect.options = options;
                }

                //BIHA                 
                if (rate.rateTypeId == 4) {
                    if (rate.inputSelected.rateName == "No")
                        this.RemoveTradeMember();
                    else
                        this.AddTradeMember();

                }

                //is the selected question a loading? update price
                //excess
                if (rate.rateTypeId == 7) {
                    this.GetPrice();
                }

                //MD (sumInsured)
                if (rate.rateTypeId == 6) {
                    this.GetPrice();
                    //get rowid
                    //this.SelectIndemnity(
                }


                this.ShowHideQuestions(rate, null);


            }
        }

        ShowHideQuestions(rate: IRateView, show: boolean): void {

            //if we have a show boolean then use that to show hide the child elements,
            //otherwise use the selected option, if visible parent is set to minus then hide, otherwise show

            var cover = _.findWhere(this.policyData.covers, { id: rate.coverId });

            var rate: IRateView;

            //are we dealing with a selected option or a regular input?
            if (rate.inputSelected != null)
                rate = rate.inputSelected;
            else
                rate = rate;

            if (rate.visibleParent != null) {
                var display = 0;
                var visibleid = rate.visibleParent;
                if (show != null) {
                    if (show)
                        display = 1;
                    else
                        display = 0;
                }
                else {
                    //showing
                    if (visibleid > 0) {
                        display = 1;
                    }
                    //hiding
                    if (visibleid < 0) {
                        var visibleid = Math.abs(visibleid);
                        display = 0;
                    }
                }

                var visibleChildRates = _.where(cover.rates, { visibleChild: visibleid });

                _.each(visibleChildRates, (rate: IRateView) => {
                    rate.display = display;
                });



                //if visibleid is 2 then hide 5, if 5 then hide all 2s, 
                //this is to show different item specification ie some show the spec with height and width and some dont
                //may need a better way of doing this

                //if (visibleid == 5)
                //{
                //    var visibleChildRates = _.where(cover.rates, { visibleChild: 2 });
                //    _.each(visibleChildRates, (rate: IRateView) => {
                //        rate.display = 0;
                //    });
                //}

                //if (visibleid == 2) {
                //    var visibleChildRates = _.where(cover.rates, { visibleChild: 5 });
                //    _.each(visibleChildRates, (rate: IRateView) => {
                //        rate.display = 0;
                //    });
                //}
            }


            if (rate.inputTypeId == 20) {
                if (rate.heightwidth == true)
                    this.showLengthWidth = true;
                else
                    this.showLengthWidth = false;
            }


        }

        CheckRate(rate: IRateView): void {
            var addedCover: ICoverView;

            //var cover = _.findWhere(this.policyData.covers, { id: rate.coverId });
            if (rate.extraCoverId != null) {
                addedCover = this.policyData.covers.filter(x => x.id == rate.extraCoverId)[0];

                if (rate.rateValue == "true") {
                    addedCover.coverRequired = true;
                }
                else
                    addedCover.coverRequired = false;

                this.priceService.GetPrice(this.policyData);
            }
        }

        FormatCurrency(coverid, rateid): void {

            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            var itemSpecs = _.findWhere(cover.rates, { id: rateid });
            var input = "";
            var currency = this.locationService.selectedLocale.currencySymbol;

            if (itemSpecs.extraCover != null)
                input = itemSpecs.extraCover;
            else
                input = itemSpecs.rateValue;

            //if nothing is entered make sure update model
            if (input == currency || input == "") {
                input = null;
                itemSpecs.sumInsured = null;
                itemSpecs.rateValue = "";
            }

            //show number as currency
            if (input != null) {
                //get rid of non numeric characters
                input = input.replace(/\D/g, '');
                var inputAsNum = parseInt(input);
                input = input.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                if (input != "")
                    input = currency + input;

                //rebind our formatted value back to model - extra cover so far means MD so use suminsured
                if (itemSpecs.extraCover != null) {
                    itemSpecs.extraCover = input;
                    itemSpecs.sumInsured = inputAsNum;
                }
                else {
                    itemSpecs.rateValue = input;
                    itemSpecs.rateValueNum = inputAsNum;
                }


                //does this currency value add a cover?
                if (itemSpecs.extraCoverId != null) {
                    if (input != "") {
                        var extracover = _.findWhere(this.policyData.covers, { id: itemSpecs.extraCoverId });
                        extracover.coverRequired = true;
                    }
                }

                //also check to see if entering currency is supposed to show something                
                var show: boolean;
                if (input != "")
                    show = true;
                else
                    show = false;

                if (itemSpecs.visibleParent != null)
                    this.ShowHideQuestions(itemSpecs, show);

                //is this currency a for a premium or suminsured?
                if (itemSpecs.rateTypeId == 10 || itemSpecs.rateTypeId == 6)
                    this.GetPrice();
            }

        }

        FormComplete(form: any): void {

            this.policyForm = form;
            this.rootScope.loading = false;
        }

        //validation for some fields must not be triggered by a submit 
        ValidateNoItems(form: any, noItems: number): void {
            if (noItems != null)
                form.noItems.$setValidity('required', true);
            else
                form.noItems.$setValidity('required', false);
        }

        ValidateWidth(form: any, width: number): void {
            if (width != null)
                form.width.$setValidity('required', true);
            else
                form.width.$setValidity('required', false);
        }

        ValidateLength(form: any, length: number): void {
            if (length != null)
                form.length.$setValidity('required', true);
            else
                form.length.$setValidity('required', false);
        }

        ValidateDimensions(form: any, coverid: number): boolean {

            var validDimensions = true;
            //this will more than likely be PL but maybe not 
            var mancover = this.policyData.covers.filter(x => x.coverType == "Mandatory")[0];

            //make sure we have items added
            if (mancover.itemsAdded != null) {

                //only validate dimensions if adding on main cover
                if (mancover.itemsAdded.length > 0 && mancover.id != coverid)
                    validDimensions = true;
                //no items?
                else {
                    var dimensionList = mancover.rates.filter(x => x.inputTypeId == 21 || x.inputTypeId == 23 || x.inputTypeId == 28);
                    var dimensions = dimensionList[0];
                    if (dimensions.inputTypeId == 21) {
                        //no of items
                        if (dimensions.noItems == null)
                            form.noItems.$setValidity('required', false);
                        else
                            form.noItems.$setValidity('required', true);
                        if (this.showLengthWidth) {
                            //length
                            if (dimensions.length == null)
                                form.length.$setValidity('required', false);
                            else
                                form.length.$setValidity('required', true);
                            //width
                            if (dimensions.width == null)
                                form.width.$setValidity('required', false);
                            else
                                form.width.$setValidity('required', true);
                        }

                        if (this.showLengthWidth)
                            validDimensions = form.noItems.$valid && form.length.$valid && form.width.$valid;
                        else
                            validDimensions = form.noItems.$valid;
                    }
                }
            }

            return validDimensions;
        }


        ValidateMandatoryCover(form: any, coverid: number): boolean {

            //make sure our mandatory cover has the minimum fields to get a price
            var validTable = false;
            //these may not be applicable for every policy

            var validTradeMember = true;
            var valid = false;

            //this will more than likely be PL but maybe not 
            var mancover = this.policyData.covers.filter(x => x.coverType == "Mandatory")[0];

            //if (mancover.itemsAdded != null) {
            //    if (mancover.itemsAdded.length == 0)
            //        return false;            
            //}

            //make sure we have selected indemnity
            if (mancover.table != null) {
                validTable = this.ValidateTable(mancover.table, form);
            }
            //if we have trade member question make sure we have an option selected
            var tradeMember = mancover.rates.filter(x => x.rateTypeId == 4)[0];
            if (tradeMember != null) {
                if (tradeMember.inputSelected == null) {
                    validTradeMember = false;
                    $("#" + tradeMember.inputName + "").addClass("has-error");
                }
            }

            valid = validTable && validTradeMember;

            //no need to highlight fields if we can get a price
            this.validManCover = valid;


            return valid;

        }

        ValidateOptionalCover(cover: ICoverView): boolean {

            var valid = true;
            //employee list
            var selectList = cover.rates.filter(x => x.rateTypeId == 44)[0];
            if (selectList != null) {
                if (selectList.inputSelected != null)
                    valid = valid && true;
                else {
                    valid = valid && false;
                    $("#" + selectList.inputName + "").addClass("has-error");
                }
            }

            //annual wage roll/value of equipment
            var itemValue = cover.rates.filter(x => x.rateTypeId == 43)[0];
            if (itemValue != null) {
                var validItem = false;
                if (itemValue.rateValue != null)
                    validItem = true;
                valid = valid && validItem;
            }

            return valid;

        }

        AddListItem(rate: IRateView, form: any): void {

            var cover = _.findWhere(this.policyData.covers, { id: rate.coverId });
            var mancover = this.policyData.covers.filter(x => x.coverType == "Mandatory")[0];

            //this will hold our items as we add them - see html <div data-ng-switch-when="27"
            var dimensionList = cover.rates.filter(x => x.inputTypeId == 21 || x.inputTypeId == 23 || x.inputTypeId == 27 || x.inputTypeId == 28);
            var dimensions = dimensionList[0];

            var validOptCover = true;
            var validDimensions = true;
            var valid = true;

            //adding an item on EL or similar
            if (cover.id != mancover.id) {

                this.validManCover = this.ValidateMandatoryCover(form, rate.coverId);
                validOptCover = this.ValidateOptionalCover(cover);
                if (mancover.net == 0)
                    this.validManCover = false;
                if (mancover.itemsAdded != null) {
                    if (mancover.itemsAdded.length == 0)
                        valid = false;
                }

                //we are about to add an item to optional 
                //cover so all good
                this.validOpCover = true;

            }
            else {
                this.validManCover = this.ValidateMandatoryCover(form, rate.coverId);
                validDimensions = this.ValidateDimensions(form, rate.coverId);
            }

            valid = this.validManCover && validOptCover && validDimensions;

            //add item
            if (valid) {

                var item: IListItem;

                var itemadded = _.findWhere(cover.rates, { id: rate.itemListId });
                var valueOfItem = _.findWhere(cover.rates, { rateTypeId: 43 });
                var valueAsString = "";

                if (valueOfItem != null)
                    valueAsString = valueOfItem.rateValue;

                //we may be trying to add an item but nothing has been selected
                //if (itemadded.inputSelected == null) {
                //    form.nme112.$setValidity('required', false);
                //    this.invalidSelect = true;
                //    return;
                //}
                //else {
                //    form.nme112.$setValidity('required', true);
                //    this.invalidSelect = false;
                //}

                var itemdescription = itemadded.inputSelected.rateName;

                var noItems, length, width, extrainfo, extraCoverId, extraCover, sumInsured;

                noItems = dimensions.noItems;
                length = dimensions.length;
                width = dimensions.width;
                extrainfo = dimensions.itemExtra;
                extraCoverId = dimensions.extraCoverId;
                extraCover = dimensions.extraCover;
                sumInsured = dimensions.sumInsured;

                //generic rateValues
                var input = valueAsString.replace(/\D/g, '');
                var valueAsNum = parseInt(input);
                //specify rateValues 
                if (cover.coverTypeId == 3)
                    sumInsured = valueAsNum;
                if (noItems == null)
                    noItems = 1;

                var price: number;
                //show a whole price
                if (itemadded.inputSelected.rate != null)
                    price = itemadded.inputSelected.rate

                item = {
                    quantity: noItems,
                    extraInfo: extrainfo,
                    length: length,
                    rateId: itemadded.inputSelected.id,
                    width: width,
                    premium: price,
                    itemDescription: itemdescription,
                    indemnity: 0,
                    rateValue: valueAsString,
                    rateValueNum: valueAsNum,
                    extraCover: extraCover,
                    extraCoverId: extraCoverId,
                    rate: itemadded.inputSelected.rate,
                    firstRate: itemadded.inputSelected.firstRate,
                    discountFirstRate: itemadded.inputSelected.discountFirstRate,
                    discountsubsequentRate: itemadded.inputSelected.discountSubsequentRate,
                    initialCharge: 0,
                    subsequentCharge: 0,
                    firstCharge: false,
                    sumInsured: sumInsured,
                    turnover: 0,
                    excess: 0,
                    selected: false,
                    activity: "",
                    wageRoll: 0,
                    indemnityPeriod: ""
                };

                //clear dimensions after addings
                dimensions.width = null;
                dimensions.length = null;
                dimensions.noItems = null;
                dimensions.extraCover = null;
                dimensions.sumInsured = null;
                dimensions.itemExtra = null;

                //also reset the select - not doing this for sports 
                //actually.. we are not doing this at all! we need to save the option
                //so certain things become visible again
                //if (dimensions.inputTypeId != 28)
                //    itemadded.inputSelected = null;

                //the select we have added an item from is now valid
                // this.policyForm[itemadded.inputName].$setValidity('required', true);                

                //these are no longer required
                if (form.noItems != null)
                    form.noItems.$setValidity('required', true);
                if (this.showLengthWidth) {
                    form.length.$setValidity('required', true);
                    form.width.$setValidity('required', true);
                }


                cover.itemsAdded.push(item);
            }


            if (valid) {
                this.GetPrice();
            }

        }

        YesNoSelected(rate: IRateView, form: any): void {

            //if we selected a loading update price
            //23:nill excess 18 hazard questions
            var priceReady = this.ValidateMandatoryCover(form, rate.coverId);
            if (priceReady) {
                if (rate.rateTypeId == 23 || rate.rateTypeId == 18)
                    this.GetPrice();
            }

            form.$dirty = true;

        }

        GetPrice(): void {
            this.policyData = this.priceService.GetPrice(this.policyData);
            if (this.policyForm != null)
                this.policyForm.$dirty = true;

            if (this.policyData.refer)
                window.scrollTo(0, document.body.scrollHeight);

        }

        RemoveListItem(index: number, coverid: number): void {

            var mancover = this.policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            var cover = _.findWhere(this.policyData.covers, { id: coverid });
            cover.itemsAdded.splice(index, 1);
            this.GetPrice();
            this.ValidateMandatoryCover(this.policyForm, mancover.id);
        }

        ValidateTable(table: ITable, form: any): boolean {
            var valid = true;

            //have we selected indemnity?
            var row = table.rows.filter(x => x.selected == true)[0];
            //set table to invalid if no row selected
            if (row == null) {
                form.indemTable.$setValidity('required', false);
                valid = false;
            }
            else {
                form.indemTable.$setValidity('required', true);
                valid = true;
            }
            return valid;
        }

        submitPolicyForm(form: any, valid: boolean): void {

            var mancover = this.policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            var validtable = this.ValidateTable(mancover.table, form);
            if (this.policyData.refer)
                valid = false;

            if (mancover.net == 0)
                this.validManCover = false;

            //make sure any added covers which can have items have had items added
            var optionalCovers = this.policyData.covers.filter(x => x.coverType == "Optional" && x.coverRequired == true);
            _.each(optionalCovers, (opcover: ICoverView) => {
                var button = opcover.rates.filter(x => x.inputTypeId == 10)[0];
                if (button != null) {
                    if (opcover.itemsAdded.length == 0) {
                        this.validOpCover = false;
                        valid = false;
                    }
                    else
                        this.validOpCover = true;

                }
            });

            if (valid && validtable && this.validManCover) {

                if (form.$dirty == true) {
                    this.chargeService.SaveQuote(this.policyData)
                        .then((response: IPolicyView) => {
                            this.policyData = response;
                            this.commonDataService.SetPolicyData(this.policyData);
                            this.location.path("/details/" + response.quoteReference + "/" + response.password);

                        });
                }
                else//if the form is clean and valid we already have what we need 
                    this.location.path("/details/" + this.policyData.quoteReference + "/" + this.policyData.password);

            }
        }


        static routing($routeProvider) {
            $routeProvider.when("/quotepage/:POLICYDESCRIPTION",
                {
                    controller: "PLInputsController",
                    templateUrl: "/app/plinputspage/plinputs.html",
                    controllerAs: "PLInputsController",
                    metadata: {
                        title: "Leisure Insure - Public Liability",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/quotepage/:QUOTEREFERENCE/:PASSCODE",
                {
                    controller: "PLInputsController",
                    templateUrl: "/app/plinputspage/plinputs.html",
                    controllerAs: "PLInputsController",
                    metadata: {
                        title: "Leisure Insure - Public Liability",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });

        }

    }

    angular.module("App")
        .controller("PLInputsController", PLInputsController)
        .config(["$routeProvider", PLInputsController.routing]);
}



