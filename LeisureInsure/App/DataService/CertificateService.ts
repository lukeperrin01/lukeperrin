﻿module LeisureInsure {
    export class CertificateService {
        static $inject = ["$rootScope", "$http", "$q", "CoverInputsService", "LocationService", "ChargeService"];
        http: ng.IHttpService;
        q: ng.IQService;
        rootScope: ng.IRootScopeService;
        chargeSummaryChanged: string;

        constructor($rootScope, $http, $q, coverInputsService, localeService, chargeService) {
            this.http = $http;
            this.q = $q;
            this.coverInputsService = coverInputsService;
            this.chargeService = chargeService;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.quoteReference = this.chargeService.quoteReference;
            this.password = this.chargeService.password;
        }
        localeService: LocationService;

        chargeSummary: IChargeSummary;
        coverInputsService: CoverInputsService;
        quoteReference: string;
        quoteId: number;
        password: string;
        customerID: number;
        chargeService: ChargeService;

        public GenerateCertificate(quoteReference: string, password: string) {
            this.quoteReference = quoteReference;
            var deferred = this.q.defer();
            this.http({
                url: "certificates/generate/",
                method: "POST",
                params: { quoteReference: quoteReference, passcode: password }
            })
                .success(() => {
                    // Successful
                    //this.copyData(data);
                    deferred.resolve();
                })
                .catch(() => {
                    // Error
                    deferred.reject();
                });
            return deferred.promise;
        }
        public GetStatementofFact(quoteReference: string) {

            this.quoteReference = this.chargeService.quoteReference;
            this.password = this.chargeService.password;
            var urlString = "GetStatementofFact/" + this.quoteReference + "/";
            var deferred = this.q.defer();
            this.http({
                url: urlString,
                method: "GET"
                //,
                //params: { quoteReference: quoteReference, passcode: passcode }
            })
                .success(() => {
                    alert("HIT SUCCESS");
                    // Successful
                    //this.copyData(data);
                    deferred.resolve();
                })
                .catch(() => {
                    // Error
                    alert("HIT FAIL");
                    deferred.reject();
                });
            return deferred.promise;

        }
    
        public getCertificate(quoteReference: string) {
            this.quoteReference = this.chargeService.quoteReference;
            this.password = this.chargeService.password;
            var urlString = "certificate/" + this.quoteReference + "/" + this.password+"/";
            var deferred = this.q.defer();
            this.http({
                url: urlString,
                method: "GET"
                //,
                //params: { quoteReference: quoteReference, passcode: passcode }
            })
                .success(() => {
                    // Successful
                    //this.copyData(data);
                    deferred.resolve();
                })
                .catch(() => {
                    // Error
                    deferred.reject();
                });
            return deferred.promise;

        }
    }

    angular.module("App").service("CertificateService", CertificateService);
}