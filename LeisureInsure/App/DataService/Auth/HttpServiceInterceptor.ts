﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />



module LeisureInsure {
    export class HttpServiceInterceptor {
        static authenticationService: AuthenticationService;

        // automatically attach Authorization header
        request(config) {
            var token = HttpServiceInterceptor.authenticationService.token;
            if (token) {
                config.headers.Authorization = "Bearer " + token;
            }

            return config;
        }

        static factory(authenticationService): HttpServiceInterceptor {
            HttpServiceInterceptor.authenticationService = authenticationService;
            return new HttpServiceInterceptor();
        }

        // If a token was sent back, save it
        response(res) {
            if (res.status === 401) {
                HttpServiceInterceptor.authenticationService.logout();
            }
            return res;
        }
    }

    angular.module("App")
        .factory("HttpServiceInterceptor", ["AuthenticationService", HttpServiceInterceptor.factory])
        .config(["$httpProvider", $httpProvider => {
            $httpProvider.interceptors.push("HttpServiceInterceptor");
        }]);
}