﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />



module LeisureInsure {
    export class AuthenticationService {
        static $inject = ["$q", "$window", "$rootScope", "$location"];
        
        constructor($q, $window, $rootScope, $location) {
            this.loginStatusChanged = "AUTHENTICATION_SERVICE.LOGIN_STATUS_CHANGED";
            this.q = $q;
            this.window = $window;
            this.rootScope = $rootScope;
            this.location = $location;

            var token = this.getToken();
            if (this.isAuthenticated()) {
                this.name = token.unique_name;
                if (token.user_type === 4) {
                    this.userType = UserType.Admin;
                } else if (token.user_type === 3) {
                    this.userType = UserType.Underwriter;
                } else if (token.user_type === 2) {
                    this.userType = UserType.Agent;
                } else {
                    this.userType = UserType.Customer;
                }
            } else {
                this.userType = UserType.Customer;
            }

            this.rootScope.$broadcast(this.loginStatusChanged);
        }

        q: ng.IQService;
        window: ng.IWindowService;
        rootScope: ng.IRootScopeService;
        location: ng.ILocationService;

        token: string;
        name: string;
        userType: UserType;
        loginStatusChanged: string;

        onLoginStatusChanged(scope: ng.IScope, handler: () => void) {
            scope.$on(this.loginStatusChanged, () => {
                handler();
            });
        }

        checkAdminAccess() {
            var adminPage = (this.location.url().substr(0, 7) === "/admin/" || this.location.url() === "/admin");
            if (adminPage) {
                var userAuthenticated = this.isAuthenticated &&
                    (this.isAdmin());
                if (!userAuthenticated) {
                    (<any>this.rootScope).redirectUrl = this.location.url();
                    this.location.path("/login");
                }
            }
        }

        login(username: string, password: string): ng.IPromise<ITokenResponse> {

            var deferred = this.q.defer<ITokenResponse>();
            var $ = angular.element;
            $.post("/api/token", {
                username: username,
                password: password,
                grant_type: "password"
            }, data => {
                var tokenResponse = <ITokenResponse>data;
                this.saveJwtToken(tokenResponse.access_token);
                var parsedToken = this.getToken();
                this.name = parsedToken.unique_name;
                deferred.resolve(tokenResponse);
            }).fail(error => {deferred.reject(error)});

            var promise = deferred.promise;
            promise.then(() => {
                this.rootScope.$broadcast(this.loginStatusChanged);
            });

            return deferred.promise;
        }

        logout(): void {
            this.window.localStorage["jwtToken"] = "";
            this.rootScope.$broadcast(this.loginStatusChanged);
            if (!this.isAuthenticated()) {
                this.checkAdminAccess();
            }
        }

        isAdmin(): boolean {
            var token = this.getToken();
            if (token) {
                return token.user_type >= 4;
            }
            return false;
        }

        isAuthenticated(): boolean {
            var token = this.getToken();
            if (token) {
                return Math.round(new Date().getTime() / 1000) <= token.exp;
            }
            return false;
        }
        
        private parseJwt(token: string): IToken {
            if (!token) {
                return undefined;
            }
            var base64Url = token.split(".")[1];
            var base64 = base64Url.replace("-", "+").replace("_", "/");
            var parsedJwt = JSON.parse(this.window.atob(base64));
            return parsedJwt;
        }

        private saveJwtToken(token: string) {
            this.window.localStorage["jwtToken"] = token;
        }

        private getToken(): IToken {
            var rawToken = <string>this.window.localStorage["jwtToken"];
            this.token = rawToken;
            return this.parseJwt(rawToken);
        }
    }

    angular.module("App").service("AuthenticationService", AuthenticationService);
}