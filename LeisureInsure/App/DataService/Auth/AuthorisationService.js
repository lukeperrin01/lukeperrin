var LeisureInsure;
(function (LeisureInsure) {
    var AuthorisationService = (function () {
        function AuthorisationService() {
        }
        AuthorisationService.$inject = [];
        return AuthorisationService;
    }());
    LeisureInsure.AuthorisationService = AuthorisationService;
    angular.module("App").service("AuthorisationService", AuthorisationService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=AuthorisationService.js.map