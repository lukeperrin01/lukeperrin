var LeisureInsure;
(function (LeisureInsure) {
    var AuthenticationService = (function () {
        function AuthenticationService($q, $window, $rootScope, $location) {
            this.loginStatusChanged = "AUTHENTICATION_SERVICE.LOGIN_STATUS_CHANGED";
            this.q = $q;
            this.window = $window;
            this.rootScope = $rootScope;
            this.location = $location;
            var token = this.getToken();
            if (this.isAuthenticated()) {
                this.name = token.unique_name;
                if (token.user_type === 4) {
                    this.userType = LeisureInsure.UserType.Admin;
                }
                else if (token.user_type === 3) {
                    this.userType = LeisureInsure.UserType.Underwriter;
                }
                else if (token.user_type === 2) {
                    this.userType = LeisureInsure.UserType.Agent;
                }
                else {
                    this.userType = LeisureInsure.UserType.Customer;
                }
            }
            else {
                this.userType = LeisureInsure.UserType.Customer;
            }
            this.rootScope.$broadcast(this.loginStatusChanged);
        }
        AuthenticationService.prototype.onLoginStatusChanged = function (scope, handler) {
            scope.$on(this.loginStatusChanged, function () {
                handler();
            });
        };
        AuthenticationService.prototype.checkAdminAccess = function () {
            var adminPage = (this.location.url().substr(0, 7) === "/admin/" || this.location.url() === "/admin");
            if (adminPage) {
                var userAuthenticated = this.isAuthenticated &&
                    (this.isAdmin());
                if (!userAuthenticated) {
                    this.rootScope.redirectUrl = this.location.url();
                    this.location.path("/login");
                }
            }
        };
        AuthenticationService.prototype.login = function (username, password) {
            var _this = this;
            var deferred = this.q.defer();
            var $ = angular.element;
            $.post("/api/token", {
                username: username,
                password: password,
                grant_type: "password"
            }, function (data) {
                var tokenResponse = data;
                _this.saveJwtToken(tokenResponse.access_token);
                var parsedToken = _this.getToken();
                _this.name = parsedToken.unique_name;
                deferred.resolve(tokenResponse);
            }).fail(function (error) { deferred.reject(error); });
            var promise = deferred.promise;
            promise.then(function () {
                _this.rootScope.$broadcast(_this.loginStatusChanged);
            });
            return deferred.promise;
        };
        AuthenticationService.prototype.logout = function () {
            this.window.localStorage["jwtToken"] = "";
            this.rootScope.$broadcast(this.loginStatusChanged);
            if (!this.isAuthenticated()) {
                this.checkAdminAccess();
            }
        };
        AuthenticationService.prototype.isAdmin = function () {
            var token = this.getToken();
            if (token) {
                return token.user_type >= 4;
            }
            return false;
        };
        AuthenticationService.prototype.isAuthenticated = function () {
            var token = this.getToken();
            if (token) {
                return Math.round(new Date().getTime() / 1000) <= token.exp;
            }
            return false;
        };
        AuthenticationService.prototype.parseJwt = function (token) {
            if (!token) {
                return undefined;
            }
            var base64Url = token.split(".")[1];
            var base64 = base64Url.replace("-", "+").replace("_", "/");
            var parsedJwt = JSON.parse(this.window.atob(base64));
            return parsedJwt;
        };
        AuthenticationService.prototype.saveJwtToken = function (token) {
            this.window.localStorage["jwtToken"] = token;
        };
        AuthenticationService.prototype.getToken = function () {
            var rawToken = this.window.localStorage["jwtToken"];
            this.token = rawToken;
            return this.parseJwt(rawToken);
        };
        AuthenticationService.$inject = ["$q", "$window", "$rootScope", "$location"];
        return AuthenticationService;
    }());
    LeisureInsure.AuthenticationService = AuthenticationService;
    angular.module("App").service("AuthenticationService", AuthenticationService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=AuthenticationService.js.map