﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />



module LeisureInsure {
    export class AuthorisationService {
        static $inject = [];
        
        http: ng.IHttpService;
    }

    angular.module("App").service("AuthorisationService", AuthorisationService);
}