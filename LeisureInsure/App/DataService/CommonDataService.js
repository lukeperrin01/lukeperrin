var LeisureInsure;
(function (LeisureInsure) {
    var CommonDataService = (function () {
        function CommonDataService($rootScope, $http, $q, $filter, localeService) {
            this.http = $http;
            this.q = $q;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.filter = $filter;
        }
        CommonDataService.prototype.SetPolicyData = function (policydata) {
            this.policyData = policydata;
        };
        CommonDataService.prototype.GetPolicyData = function (policydata) {
            return this.policyData;
        };
        CommonDataService.$inject = ["$rootScope", "$http", "$q", "$filter", "LocationService"];
        return CommonDataService;
    }());
    LeisureInsure.CommonDataService = CommonDataService;
    angular.module("App").service("CommonDataService", CommonDataService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=CommonDataService.js.map