﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class CommonDataService {
        static $inject = ["$rootScope", "$http", "$q", "$filter", "LocationService"];
        http: ng.IHttpService;
        q: ng.IQService;
        //rootScope: ng.IRootScopeService;
        rootScope: any;       
        filter: any;
        localeService: LocationService;
        policyData: IPolicyView;
        priceChanged: boolean;        
        
        constructor($rootScope, $http, $q, $filter, localeService) {

            this.http = $http;
            this.q = $q;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.filter = $filter;
        }

        SetPolicyData(policydata: IPolicyView): void
        {
            this.policyData = policydata;
        }

        GetPolicyData(policydata: IPolicyView): IPolicyView {

            return this.policyData;
        }

        
        
    }

    angular.module("App").service("CommonDataService", CommonDataService);
}



