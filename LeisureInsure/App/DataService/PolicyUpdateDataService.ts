﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />



module LeisureInsure {
    export class PolicyUpdateDataService {
        static $inject = ["$http", "$q", "PolicyService", "CoverService", "CoverInputsService", "LocationService","$rootScope"];
        http: ng.IHttpService;
        q: ng.IQService;
        constructor($http, $q, policyService, coverService, coverInputsService, locationService, $rootScope) {
            this.http = $http;
            this.q = $q;
            this.policyService = policyService;
            this.coverService = coverService;
            this.coverInputsService = coverInputsService;
            this.locationService = locationService;
            this.rootScope = $rootScope;
        }

        policyService: PolicyService;
        coverService: CoverService;
        coverInputsService: CoverInputsService;
        locationService: LocationService;
        rootScope: any;

        initialiseNew(locale: string, quoteKey: number, policyKey: number): ng.IPromise<any> {
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/data/newpolicy",
                method: "GET",
                params: { strLocale: locale, quotePK: quoteKey, policyPK: policyKey }
            })
                .success((data: IPolicyView) => {                                      

                    if (data.locale.localePK != this.locationService.selectedLocale.localePK) {
                        this.locationService.getlocales(data.locales.localeSelected.strLocale,policyKey);
                    }                                       

                    deferred.resolve(data);
                })
                .catch((data: any) => {
                    // Error
                    //this.rootScope.loadingGif = false; 
                    deferred.reject(data.data.message);
                    //console.log("error", data);
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
            return deferred.promise;
        }        
    }

    angular.module("App").service("PolicyUpdateDataService", PolicyUpdateDataService);
}


