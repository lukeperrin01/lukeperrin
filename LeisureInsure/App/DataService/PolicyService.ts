﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />



module LeisureInsure {
    export class PolicyService {
        static $inject = ["$http", "$q"];
        http: ng.IHttpService;
        q: ng.IQService;

        constructor($http,$q) {
            this.selectedPolicyId = 0;
            this.policies = [];
            this.http = $http;
            this.q = $q;            
            //this.initialised = this.q.promise;
        }

        policies: Array<IPolicy>;
        selectedPolicyId: number;
        selectedPolicy: IPolicy;       
        initialised: ng.IPromise<PolicyService>;

        addPolicies(policies: Array<any>): void {
            angular.copy(policies, this.policies);
            //this.q.resolve(this);
        }

        GetPolicies(localeID: number): ng.IPromise<any> {            
            var deferred = this.q.defer();       
            
            this.http({
                url: "/api/v1/data/getallpolicies",
                method: "GET",
                params: { localeID: localeID }
            })
                .success((data: any) => {
                    deferred.resolve();
                 
                    this.addPolicies(data);
                })
                .catch((data: any) => {                    
                    deferred.reject(data.data.message);
                   
                    //console.log("error", data);
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });

            return deferred.promise;
        }

        updateSelectedPolicy(selectedPolicy: any): void {
            if (selectedPolicy != null) {
                this.selectedPolicyId = selectedPolicy.policyPK;
            }
        }
        selectPolicy(policy: IPolicy): void {
            if (policy != null) {
                this.selectedPolicy = policy;
                this.selectedPolicyId = policy.policyPK;
            }
        }

        selectPolicyById(policyId: number): void {
            var policy = _.findWhere(this.policies, { policyPK: policyId });
            if (policy) {
                this.selectPolicy(policy);
            }
        }

        refreshPolicies(policyList:Array<any>): void {
            this.policies = policyList;
        }        

    }

    angular.module("App").service("PolicyService", PolicyService);
}
