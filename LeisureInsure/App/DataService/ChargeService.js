var LeisureInsure;
(function (LeisureInsure) {
    var ChargeService = (function () {
        function ChargeService($rootScope, $http, $q, $filter, coverInputsService, localeService) {
            var _this = this;
            this.chargeSummaryChanged = "CHARGE_SERVICE.CHARGE_SUMMARY_CHANGED";
            this.http = $http;
            this.q = $q;
            this.coverInputsService = coverInputsService;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.filter = $filter;
            this.localeService.onSelectedLocaleChanged($rootScope, function () {
                if (_this.chargeSummary) {
                    _this.updateQuote();
                }
            });
        }
        ChargeService.prototype.onChargeSummaryChanged = function (scope, handler) {
            scope.$on(this.chargeSummaryChanged, function (event, message) {
                handler(message);
            });
        };
        ChargeService.prototype.copyData = function (data) {
            this.chargeSummary = angular.copy(data);
            this.quoteReference = this.chargeSummary.quoteReference;
            this.quoteId = this.chargeSummary.id;
            this.password = this.chargeSummary.password;
            this.landingPageId = this.chargeSummary.landingPageId;
            this.renewal = this.chargeSummary.renewal;
            this.dateFromDt = new Date(data.dateFrom);
            this.dateToDt = new Date(data.dateTo);
            this.rootScope.$broadcast(this.chargeSummaryChanged, this.chargeSummary);
        };
        ChargeService.prototype.resetChargeService = function () {
            this.chargeSummary = null;
            this.quoteReference = "";
            this.quoteId = 0;
            this.password = "";
            this.landingPageId = 0;
        };
        ChargeService.prototype.removeCover = function (coverId) {
            var _this = this;
            var defer = this.q.defer();
            this.http({
                url: "api/v1/payment/removecover/" + coverId,
                method: "POST",
                data: {
                    quoteReference: this.quoteReference,
                    passcode: this.password
                }
            })
                .success(function (data) {
                _this.copyData(data);
                defer.resolve(_this.chargeSummary);
            })
                .catch(function (data) {
                defer.reject(data.data.message);
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return defer.promise;
        };
        ChargeService.prototype.toggleLegalCare = function () {
            this.chargeSummary.legalFeesAdded = !this.chargeSummary.legalFeesAdded;
            return this.updateQuote();
        };
        ChargeService.prototype.toggleExcessWaiver = function () {
            this.chargeSummary.excessWaiverAdded = !this.chargeSummary.excessWaiverAdded;
            return this.updateQuote();
        };
        ChargeService.prototype.selectCard = function (cardId) {
            this.chargeSummary.paymentCardId = cardId;
            return this.updateQuote();
        };
        ChargeService.prototype.updateQuotePeriod = function () {
            this.updateQuote();
        };
        ChargeService.prototype.updateQuote = function () {
            var _this = this;
            var deferred = this.q.defer();
            var dateTo = GetDateTime(this.dateToDt);
            var dateFrom = GetDateTime(this.dateFromDt);
            this.http({
                url: "/api/v1/updateQuote/" + this.chargeSummary.quoteReference,
                method: "PUT",
                data: {
                    localeId: this.localeService.selectedLocale.localePK,
                    password: this.chargeSummary.password,
                    legalCare: this.chargeSummary.legalFeesAdded,
                    excessWaiver: this.chargeSummary.excessWaiverAdded,
                    cardId: this.chargeSummary.paymentCardId,
                    payOption: this.paymentTypeId,
                    dateFrom: dateFrom,
                    dateTo: dateTo
                }
            })
                .success(function (data) {
                _this.copyData(data);
                deferred.resolve(data);
            })
                .catch(function (data) {
                deferred.reject(data.data.message);
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        ChargeService.prototype.updatePaymentCard = function (paymentCardId) {
            this.chargeSummary.paymentCardId = paymentCardId;
            return this.updateQuote();
        };
        ChargeService.prototype.sortHazardGroups = function () {
            var hazards = Array();
            var items = angular.copy(this.chargeSummary.chargeSummaryItems);
            var temp = "";
            if (this.chargeSummary != null) {
                $.each(items, function (index, item) {
                    if (item.sectionTypeDescription != null) {
                        if (item.showInSummary == true)
                            hazards.push(item);
                    }
                });
                var sorted = this.filter('orderBy')(hazards, 'price', true);
                this.sortedHazards = [];
                var result = Array();
                $.each(sorted, function (index, item) {
                    if (temp != item.sectiondescription) {
                        temp = item.sectiondescription;
                    }
                    else
                        item.sectiondescription = "";
                    result.push(item);
                });
                this.sortedHazards = result;
            }
        };
        ChargeService.prototype.loadQuote = function (quoteReference, password) {
            var _this = this;
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/quotes/" + quoteReference + "?password=" + password,
                method: "GET",
                data: {
                    password: password
                }
            })
                .success(function (data) {
                _this.quoteId = data.id;
                _this.copyData(data);
                _this.sortHazardGroups();
                deferred.resolve(data);
            })
                .catch(function (data) {
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        ChargeService.prototype.CreateQuote = function (inputs, policyId) {
            var _this = this;
            var deferred = this.q.defer();
            this.rootScope.loadingquote = true;
            var browser = eval("GetBrowserDetails()");
            var currentDate = new Date();
            var currentTime = currentDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
            this.http({
                url: "/api/v1/charges",
                method: "POST",
                data: {
                    localeId: this.localeService.selectedLocale.localePK,
                    policyId: policyId,
                    inputs: inputs,
                    quoteReference: this.quoteReference,
                    quoteId: this.quoteId,
                    password: this.password,
                    landingPageId: this.landingPageId,
                    browserTime: currentTime,
                    browser: browser
                }
            })
                .success(function (data) {
                _this.copyData(data);
                _this.sortHazardGroups();
                deferred.resolve();
                _this.rootScope.loadingquote = false;
            })
                .catch(function (data) {
                _this.rootScope.loadingquote = false;
                deferred.reject(data.data.message);
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        ChargeService.prototype.emailQuote = function (email) {
            var deferred = this.q.defer();
            return this.http({
                url: "/api/v1/communications/email-quote",
                method: "POST",
                data: {
                    email: email,
                    quoteReference: this.chargeSummary.quoteReference,
                    quoteId: this.chargeSummary.quoteId,
                    passcode: this.chargeSummary.password
                }
            }).success(function (data) {
                deferred.resolve();
            }).error(function (data) {
                deferred.resolve(data.message);
            });
        };
        ChargeService.prototype.lockQuote = function (quoteReference, lockQuote) {
            return this.http({
                url: "/api/v1/payment/lockQuotexx",
                method: "POST",
                data: {
                    quoteReference: quoteReference,
                    lockQuotex: lockQuote
                }
            });
        };
        ChargeService.prototype.updateQuoteDates = function (dates) {
            var _this = this;
            if (dates.dateFrom != null) {
                var dateFrom = GetDateTime(dates.dateFrom);
                var dateTo = dateFrom;
                if (dates.dateTo != null)
                    dateTo = GetDateTime(dates.dateTo);
                var timefrom = dates.startingHour + ":" + dates.startingMinute;
                var timeto = dates.endingHour + ":" + dates.endingMinute;
                var deferred = this.q.defer();
                this.http({
                    url: "/api/v1/updateQuoteDates/" + this.chargeSummary.quoteReference,
                    method: "POST",
                    data: {
                        startDate: dateFrom,
                        endDate: dateTo,
                        timeFrom: timefrom,
                        timeTo: timeto,
                        dateToRequired: dates.dateToRequired,
                        startTimeRequired: dates.startTimeRequired,
                        endTimeRequired: dates.endTimeRequired,
                        numberOfDays: dates.coverDays
                    }
                })
                    .success(function (data) {
                    deferred.resolve(data);
                })
                    .catch(function (data) {
                    deferred.reject(data.data.message);
                    _this.rootScope.loadingpurchase = false;
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
                return deferred.promise;
            }
        };
        ChargeService.prototype.completeAsAgent = function (dates) {
            var _this = this;
            var dateFrom = GetDateTime(dates.dateFrom);
            var dateTo = dateFrom;
            if (dates.dateTo != null)
                dateTo = GetDateTime(dates.dateTo);
            var timefrom = dates.startingHour + ":" + dates.startingMinute;
            var timeto = dates.endingHour + ":" + dates.endingMinute;
            var deferred = this.q.defer();
            this.rootScope.loadingpurchase = true;
            this.http({
                url: "/api/v1/completeAgent/" + this.chargeSummary.quoteReference,
                method: "POST",
                data: {
                    startDate: dateFrom,
                    endDate: dateTo,
                    timeFrom: timefrom,
                    timeTo: timeto,
                    dateToRequired: dates.dateToRequired,
                    startTimeRequired: dates.startTimeRequired,
                    endTimeRequired: dates.endTimeRequired,
                    numberOfDays: dates.coverDays
                }
            })
                .success(function (data) {
                deferred.resolve(data);
                _this.rootScope.loadingpurchase = false;
            })
                .catch(function (data) {
                deferred.reject(data.data.message);
                _this.rootScope.loadingpurchase = false;
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        ChargeService.prototype.addCustomerDetails = function (contactDetails) {
            var _this = this;
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/addCustomerDetails/",
                method: "POST",
                data: contactDetails
            })
                .then(function () {
                _this.rootScope.loadingpurchase = false;
                deferred.resolve();
            }, function (response) {
                deferred.reject(response.data.exceptionMessage);
            })
                .catch(function (data) {
                deferred.reject(data.data.message);
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        ChargeService.prototype.updateQuoteDatesNew = function (policyData) {
            var _this = this;
            var json = JSON.stringify(policyData);
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/updateQuoteDatesNew",
                method: "POST",
                data: json
            })
                .success(function (data) {
                deferred.resolve(data);
            })
                .catch(function (data) {
                deferred.reject(data.data.message);
                _this.rootScope.loadingpurchase = false;
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        ChargeService.prototype.loadPolicyData = function (quoteRef, password) {
            var _this = this;
            var deferred = this.q.defer();
            this.rootScope.loading = true;
            this.http({
                url: "/api/v1/loadquote?quoteRef=" + quoteRef + "&password=" + password,
                method: "GET"
            })
                .success(function (data) {
                _this.quoteId = data.id;
                _this.rootScope.loading = false;
                deferred.resolve(data);
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        ChargeService.prototype.SaveQuote = function (policyData) {
            var _this = this;
            var deferred = this.q.defer();
            this.rootScope.loading = true;
            var browser = eval("GetBrowserDetails()");
            var currentDate = new Date();
            var currentTime = currentDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
            var json = JSON.stringify(policyData);
            this.http({
                url: "/api/v1/savequote",
                data: json,
                method: "POST"
            })
                .success(function (data) {
                _this.rootScope.loading = false;
                deferred.resolve(data);
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        ChargeService.prototype.SaveAddress = function (policyData) {
            var _this = this;
            var deferred = this.q.defer();
            this.rootScope.loading = true;
            var browser = eval("GetBrowserDetails()");
            var currentDate = new Date();
            var currentTime = currentDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
            var json = JSON.stringify(policyData);
            this.http({
                url: "/api/v1/saveaddress",
                data: json,
                method: "POST"
            })
                .success(function (data) {
                _this.rootScope.loading = false;
                deferred.resolve(data);
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        ChargeService.prototype.completeAsAgentNew = function () {
            var _this = this;
            var deferred = this.q.defer();
            this.rootScope.loading = true;
            this.http({
                url: "/api/v1/agentpayment",
                method: "POST",
                data: {
                    StripeToken: null,
                    QuoteRef: PaymentData.quoteRef,
                    QuotePass: PaymentData.quotePass,
                    CurrencyId: PaymentData.currencyId,
                    InstalmentData: null,
                    PolicyDates: Dates
                }
            })
                .success(function (data) {
                deferred.resolve(data);
                _this.rootScope.loadingpurchase = false;
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        ChargeService.prototype.ShowError = function (err, defer) {
            this.rootScope.loading = false;
            this.rootScope.savingquote = false;
            defer.reject(err.data.message);
        };
        ChargeService.$inject = ["$rootScope", "$http", "$q", "$filter", "CoverInputsService", "LocationService"];
        return ChargeService;
    }());
    LeisureInsure.ChargeService = ChargeService;
    angular.module("App").service("ChargeService", ChargeService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=ChargeService.js.map