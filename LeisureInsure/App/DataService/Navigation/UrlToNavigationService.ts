﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../../scripts/typings/angularjs/angular-route.d.ts" />

module LeisureInsure {
    export class UrlToNavigationService {
        static $inject = ["$routeParams", "$http", "$q"];
        constructor($routeParams, $http, $q) {
            this.routeParams = $routeParams;
            this.http = $http;
            this.q = $q;
        }

        routeParams: ng.route.IRouteParamsService;
        http: ng.IHttpService;
        q: ng.IQService;       

        

        translateUrlNew(segmentName: string): ng.IPromise<INavigationObject> {
            var policy = this.routeParams[segmentName];
            var defer = this.q.defer<INavigationObject>();

            this.http.get("api/v1/data/getlandingUrl/" + policy).then(response => {
                if (response.status === 200) {
                    var landingPage = <INavigationObject>response.data;                    
                    defer.resolve(landingPage);
                }
                defer.reject(response.statusText);
            });


            return defer.promise;
        }

        GetLandingPageNew(quoteRef: string, password: string): ng.IPromise<INavigationObject> {

            var defer = this.q.defer<INavigationObject>();

            this.http.get("api/v1/data/getlandingQuote/" + quoteRef + "/" + password).success((response: INavigationObject) => {

                var landingPage = response;
                defer.resolve(landingPage);                                           
            }).catch(err => this.ShowError(err, defer));

            return defer.promise;
        }

        ShowError(err: any, defer: any): any {           

            eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + err.data.message + "');");
            defer.reject(err.data.message);
        }


    }

    angular.module("App").service("UrlToNavigationService", UrlToNavigationService);
}


