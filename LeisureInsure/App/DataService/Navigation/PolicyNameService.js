var LeisureInsure;
(function (LeisureInsure) {
    var PolicyNameService = (function () {
        function PolicyNameService() {
        }
        PolicyNameService.prototype.idForName = function (name) {
            switch (name) {
                case "equipment-hirers":
                    return 1;
                case "showmen":
                    return 2;
                case "Carting":
                    return 3;
                case "other-activities":
                    return 4;
                case "day-cover-for-inflatables":
                    return 5;
                case "event-supplier":
                    return 6;
                case "bowles-club":
                    return 7;
                case "fitness-club":
                    return 8;
                case "celebrations-and-parties":
                    return 9;
                case "weddings-and-civil-ceremonies":
                    return 10;
                case "bar-and-bat-mitzvahs":
                    return 11;
                case "off-roading":
                    return 12;
                case "paintball-and-airsoft":
                    return 13;
                case "freelance-catering":
                    return 14;
                case "freelance-activity-instructor":
                    return 15;
                case "freelance-sports-instructor":
                    return 16;
                case "christmas-lights":
                    return 17;
                case "conference-and-meetings":
                    return 18;
                case "outdoor-activities":
                    return 19;
                case "paintball":
                    return 20;
                case "airsoft":
                    return 21;
            }
            return 0;
        };
        PolicyNameService.prototype.nameForId = function (id) {
            switch (id) {
                case 0:
                    return "no-policy";
                case 1:
                    return "equipment-hirers";
                case 2:
                    return "showmen";
                case 3:
                    return "carting";
                case 4:
                    return "other-activities";
                case 5:
                    return "day-cover-for-inflatables";
                case 6:
                    return "event-supplier";
                case 7:
                    return "bowles-club";
                case 8:
                    return "fitness-club";
                case 9:
                    return "celebrations-and-parties";
                case 10:
                    return "weddings-and-civil-ceremonies";
                case 11:
                    return "bar-and-bat-mitzvahs";
                case 12:
                    return "off-roading";
                case 13:
                    return "paintball-and-airsoft";
                case 14:
                    return "freelance-catering";
                case 15:
                    return "freelance-activity-instructor";
                case 16:
                    return "freelance-sports-instructor";
                case 17:
                    return "christmas-lights";
                case 18:
                    return "conference-and-meetings";
                case 19:
                    return "outdoor-activities";
                case 20:
                    return "paintball";
                case 21:
                    return "airsoft";
            }
            return "";
        };
        return PolicyNameService;
    }());
    LeisureInsure.PolicyNameService = PolicyNameService;
    angular.module("App").service("PolicyNameService", PolicyNameService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=PolicyNameService.js.map