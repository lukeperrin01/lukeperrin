﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../../scripts/typings/underscore/underscore.d.ts" />



module LeisureInsure.Admin {
    export class LandingPageService {
        static $inject = ["$http", "$q"];
        http: ng.IHttpService;
        q: ng.IQService;
        constructor($http, $q) {
            var defer = $q.defer();
            this.q = $q;
            this.http = $http;
            this.initialised = defer.promise;

            $http.get("api/v1/landingpages").then(response => {
                this.landingPages = <Array<ILandingPageDefinition>>angular.copy(response.data);
                defer.resolve(this);
            });
        }

        initialised: ng.IPromise<LandingPageService>;
        landingPages: Array<ILandingPageDefinition>;

        landingPageForId(landingPageId: number): ILandingPageDefinition {
            return _.find(this.landingPages, landingPage => {
                return (landingPage.id === landingPageId);
            });
        }

        addLandingPage(): ng.IPromise<ILandingPageDefinition> {
            var defer = this.q.defer<ILandingPageDefinition>();
            this.http.post("api/v1/landingpages", null).then(response => {
                var landingPage = <ILandingPageDefinition>response.data;
                this.landingPages.push(landingPage);
                defer.resolve(landingPage);
            });
            return defer.promise;
        }

        deleteLandingPage(landingPageId: number): void {
            this.http.delete("api/v1/landingpages/" + landingPageId).then(response => {
                if (response.status === 200) {
                    var landingPage:ILandingPageDefinition = _.find(this.landingPages, (landingPage:ILandingPageDefinition) => {
                        return (landingPage.id == landingPageId); 
                    }); 
                    if (landingPage) {
                        var index = this.landingPages.indexOf(landingPage);
                        if (index > -1) {
                            this.landingPages.splice(index, 1);
                        }
                    }
                }
            });
        }

        updateLandingPage(landingPage:ILandingPageDefinition) {
            this.http.put("api/v1/landingpages/" + landingPage.id, landingPage).then(response => {
                if (response.status === 200) {
                    var existingObject = this.landingPageForId(landingPage.id);
                    angular.copy(landingPage, existingObject);
                }
            });
        }
    }

    angular.module("LeisureInsure.Admin").service("LandingPageService", LandingPageService);
}