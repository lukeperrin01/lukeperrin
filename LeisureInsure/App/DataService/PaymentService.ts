﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />


module LeisureInsure {
    export class PaymentService {
        static $inject = ["$http", "$q","ChargeService"];
        http: ng.IHttpService;

        constructor($http, $q, chargeService) {
            this.http = $http;
            this.q = $q;
            this.chargeService = chargeService;
            this.chargeSummary = Array<IChargeSummaryItem>();
            this.chargeInputs = Array<IChargeInputs>();
        }

        chargeSummary: Array<IChargeSummaryItem>;
        chargeInputs: Array<IChargeInputs>;
        chargeService: ChargeService;
        chargeLine: IChargeLine;
        q: ng.IQService;
        
        summaryForCover(inputs: Array<IChargeInputs> ): ng.IPromise<Array<IChargeSummaryItem>> {
            var deferred = this.q.defer<Array<IChargeSummaryItem>>();
            this.http({
                url: "/api/v1/data/getCharges",
                method: "POST",
                data: inputs
            })
                .success((data: Array<IChargeSummaryItem>) => {
                    // Successful
                    angular.copy(data, this.chargeSummary);
                    deferred.resolve();
                })
                .catch((data:any) => {
                    // Error
                    deferred.reject();
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
            return deferred.promise;
        }
    }

    angular.module("App").service("PaymentService", PaymentService);
}