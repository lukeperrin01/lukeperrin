﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />



module LeisureInsure {
    export class CoverInputsService {
        static $inject = ["$q", "$filter"];
        filter: any; //ng.IRootScopeService;
        constructor($q, $filter) {
            this.filter = $filter;
            this.selectedCoverId = 0;
            //this.coverInputs = Array<ICoverInputs>();
            this.inputs = Array<IFlatInput>();
            this.covers = Array<ICover>();

            var deferred = $q.defer();
            this.q = deferred;
            this.initialised = deferred.promise;
        }

        //coverInputs: Array<ICoverInputs>;
        selectedCoverId: number;
        selectedCover: ICover;
        covers: Array<ICover>;
        inputs: Array<IFlatInput>;

        q: ng.IQService;
        initialised: ng.IPromise<CoverInputsService>;
        addFlatInputs(flatInputs: Array<IFlatInput>): void {
            angular.copy(flatInputs, this.inputs);
            this.q.resolve(this);
        }
    }

    angular.module("App").service("CoverInputsService", CoverInputsService);
}