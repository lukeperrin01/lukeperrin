var LeisureInsure;
(function (LeisureInsure) {
    var StripeService = (function () {
        function StripeService($rootScope, $http, $q, $filter, localeService) {
            this.http = $http;
            this.q = $q;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.filter = $filter;
        }
        StripeService.prototype.GetStripeData = function () {
            var _this = this;
            var deferred = this.q.defer();
            this.rootScope.loading = true;
            this.http({
                url: "/api/v1/getstripedata",
                method: "GET"
            })
                .success(function (data) {
                _this.rootScope.loading = false;
                deferred.resolve(data);
            })
                .catch(function (err) { return _this.ShowError(err, deferred); });
            return deferred.promise;
        };
        StripeService.prototype.ShowError = function (err, defer) {
            this.rootScope.loading = false;
            eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + err.data.message + "');");
            defer.reject(err.data.message);
        };
        StripeService.$inject = ["$rootScope", "$http", "$q", "$filter", "LocationService"];
        return StripeService;
    }());
    LeisureInsure.StripeService = StripeService;
    angular.module("App").service("StripeService", StripeService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=StripeService.js.map