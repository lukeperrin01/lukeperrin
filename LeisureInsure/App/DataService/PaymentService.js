var LeisureInsure;
(function (LeisureInsure) {
    var PaymentService = (function () {
        function PaymentService($http, $q, chargeService) {
            this.http = $http;
            this.q = $q;
            this.chargeService = chargeService;
            this.chargeSummary = Array();
            this.chargeInputs = Array();
        }
        PaymentService.prototype.summaryForCover = function (inputs) {
            var _this = this;
            var deferred = this.q.defer();
            this.http({
                url: "/api/v1/data/getCharges",
                method: "POST",
                data: inputs
            })
                .success(function (data) {
                angular.copy(data, _this.chargeSummary);
                deferred.resolve();
            })
                .catch(function (data) {
                deferred.reject();
                eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
            });
            return deferred.promise;
        };
        PaymentService.$inject = ["$http", "$q", "ChargeService"];
        return PaymentService;
    }());
    LeisureInsure.PaymentService = PaymentService;
    angular.module("App").service("PaymentService", PaymentService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=PaymentService.js.map