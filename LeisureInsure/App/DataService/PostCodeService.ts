﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />


module LeisureInsure {
    export class PostCodeService {
        static $inject = ["$http", "$q", "$rootScope"];
        http: ng.IHttpService;   
        rootScope: any;    

        constructor($http, $q ,$rootScope) {
            this.http = $http;
            this.q = $q;          
            this.rootScope = $rootScope;

            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");

        }
        
        q: ng.IQService;

        GetAddresses(postcode: String): any {
            
            this.rootScope.searchingQuote = true;

            var deferred = this.q.defer();
            this.http({
                url: "api/v1/postcode",
                method: "GET",
                params: { postcode: postcode }                               
            })
                .success((data: any) => {
                    // Successful                                       
                    deferred.resolve(data);
                    this.rootScope.searchingQuote = false;
                })
                .catch((data:any) => {
                    // Error                    
                    deferred.reject();
                    this.rootScope.searchingQuote = false;
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });

                return deferred.promise;
        }
    }

    angular.module("App").service("PostCodeService", PostCodeService);
}