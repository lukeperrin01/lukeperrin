﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />

module LeisureInsure {
    export class LocationService {
        rootScope: ng.IRootScopeService;
        http: ng.IHttpService;
        static $inject = ["$http", "$q", "$rootScope"];
        constructor($http, $q, $rootScope) {
            this.changeEvent = "LOCALE_CHANGED_EVENT";
            this.rootScope = $rootScope;
            this.http = $http;
            this.locales = Array<ILocale>();
            this.btnDisabled = false;
            var deferred = $q.defer();
            this.q = deferred;
            this.qx = $q;
            this.getlocales("", 1);
            this.initialised = deferred.promise;

        }
        btnDisabled: boolean;
        selectedLocale: ILocale;
        locales: Array<ILocale>;
        changeEvent: string;
        q: ng.IQService;
        qx: ng.IQService;
        initialised: ng.IPromise<LocationService>;

        getlocales(locale: string, policyid: number): ng.IPromise<any> {

            var deferredx = this.qx.defer<any>();
            this.http({
                url: "/api/v1/data/getLocales",
                method: "GET",
                params: { strLocale: locale, policyid: policyid }
            })
                .success((data: any) => {
                    // Successful
                    if (locale == "") {
                        this.locales = data.localeList;
                    }
                    if (!this.selectedLocale || this.selectedLocale.strLocale != data.localeSelected.strLocale) {
                        this.selectedLocale = data.localeSelected;
                    }
                    this.updateSelectedLocale(data.localeSelected);
                })
                .catch((data: any) => {
                    // Error
                    deferredx.reject(data.exceptionMessage);
                });

            return deferredx.promise;

        }

        updateSelectedLocale(locale: ILocale): void {

            this.selectedLocale = locale;
            this.rootScope.$broadcast(this.changeEvent, locale);           
          
            this.q.resolve(this);
        }

        disableLocaleBtn(trueFalse: boolean): void {
            if (this.selectedLocale != null) {
                this.selectedLocale.showFlag = trueFalse;
            }           

        };

        onSelectedLocaleChanged(scope: ng.IScope, handler: (locale: ILocale) => void): void {
            scope.$on(this.changeEvent, (event: any) => {
                handler(this.selectedLocale);
            });
        }
    }

    angular.module("App").service("LocationService", LocationService);
}