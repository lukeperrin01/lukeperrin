﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

/*Note, We are only applying irish multiplier against keyed in values for liability covers, eg Employers liability, Public Liability
If PL is based on items being added not keyed in values then this is already set in the backend first, see GetPolicyData in PoliciesRepository
that is also where minimum rates are applying multiplier. Idea being set as much as we can before the form receives rates*/

module LeisureInsure {
    export class PriceService {
        static $inject = ["$rootScope", "$http", "$q", "$filter", "LocationService", "CommonDataService"];
        http: ng.IHttpService;
        q: ng.IQService;
        //rootScope: ng.IRootScopeService;
        rootScope: any;
        filter: any;
        localeService: LocationService;
        commonDataService: CommonDataService;

        constructor($rootScope, $http, $q, $filter, localeService, commonDataService: CommonDataService) {

            this.http = $http;
            this.q = $q;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.filter = $filter;
            this.commonDataService = commonDataService;
        }

        GetPrice(policyData: IPolicyView): IPolicyView {

            var covers = policyData.covers;
            var manCover = policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            var feeRate = manCover.rates.filter(x => x.rateTypeId == 19)[0];
            var totalFee = 0;
            var minFee = 0;
            var finalFee = 0;
            var legalCare = 0;

            this.commonDataService.priceChanged = true;

            policyData.refer = false;

            var totalnet: number; totalnet = 0;

            _.each(covers, (cover: ICoverView) => {

                //this will get us indemnity/net/suminsured/exess
                switch (cover.id) {
                    case CoverEnum.EquipPL:
                        this.GetEquipPL(cover, policyData);
                        break;
                    case CoverEnum.EquipEL:
                        this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.EquipMD:
                        this.GetEquipMD(cover, policyData);
                        break;
                    case CoverEnum.EquipProd:
                        this.GetProdL(cover, policyData);
                        break;
                    case CoverEnum.EquipProf:
                        this.GetProfL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallPL:
                        this.GetPaintballPL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallEL:
                        this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallBuildings:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallAncillaryBuildings:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallFixtures:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallPlayingSurfaces:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallBusinessEquip:
                        this.GetBusinessEquipment(cover, policyData);
                        break;
                    case CoverEnum.PaintBallTrophies:
                        this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.PaintBallMachinery:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallStock:
                        this.TwoInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallRefrigerated:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallPropertyInTransit:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallMoney:
                        this.GetMoney(cover, policyData);
                        break;
                    case CoverEnum.PaintBallRentPayable:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallProdLiability:
                        this.GetProdL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallProfLiability:
                        this.GetProfL(cover, policyData);
                        break;
                    case CoverEnum.PaintBallLossOfGrossProfit:
                        this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.PaintBallLossOfGrossRevenue:
                        this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.PaintBallLossOfGrossRentals:
                        this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.PaintBallBookDebts:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.PaintBallCostOfWorking:
                        this.GetValueFromSelect(cover, policyData);
                        break;
                    case CoverEnum.AirsoftPL:
                        this.GetPaintballPL(cover, policyData);
                        break;
                    case CoverEnum.AirsoftEL:
                        this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.AirsoftProdLiability:
                        this.GetProdL(cover, policyData);
                        break;
                    case CoverEnum.AirsoftProfLiability:
                        this.GetProfL(cover, policyData);
                        break;
                    case CoverEnum.MobileCateringPL:
                        this.GetMobileCateringPL(cover, policyData);
                        break;
                    case CoverEnum.MobileCateringMD:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.MobileCateringStockGeneral:
                        this.OneInputLoading(cover, policyData);
                        break;
                    case CoverEnum.FreelanceCateringPL:
                        this.GetFreelanceCateringPL(cover, policyData);
                        break;
                    case CoverEnum.SportsInstructorPL:
                        this.GetSportsInstructorPL(cover, policyData);
                        break;
                    case CoverEnum.SportsInstructorEL:
                        this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.SportsInstructorMD:
                        this.GetSportsInstructorMD(cover, policyData);
                        break;
                    case CoverEnum.ShowmenPL:
                        this.GetShowmenPL(cover, policyData);
                        break;
                    case CoverEnum.ShowmenEL:
                        this.GetEL(cover, policyData);
                        break;
                    case CoverEnum.ShowmenMD:
                        this.GetShowmenMD(cover, policyData);
                        break;
                    case CoverEnum.ShowmenProducts:
                        this.GetProdL(cover, policyData);
                        break;
                    case CoverEnum.ShowmenBusinessEquip:
                        this.GetBusinessEquipment(cover, policyData);
                        break;
                    default:
                }

                //add on tax - cover level             
                if (cover.net > 0) {
                    var tax = cover.net * policyData.taxRate;
                    cover.tax = tax;
                    cover.total = cover.net + tax;
                    totalnet += cover.net;
                }
            });

            //calculate fees at policy level
            if (totalnet > 0) {
                if (feeRate.divBy == 1) {
                    finalFee = feeRate.rate;
                    minFee = 0;
                }
                else {
                    totalFee = feeRate.rate * totalnet / feeRate.divBy;
                    minFee = feeRate.minimumRate;
                    finalFee = Math.max(totalFee, minFee);
                }
            }

            //calculate fees at cover level
            _.each(covers, (cover: ICoverView) => {

                if (cover.net > 0) {
                    //we are only applying flat rates to mandatory covers eg PL,foodborne illness
                    if (feeRate.divBy == 1) {
                        if (cover.coverType == "Mandatory") {
                            cover.total = cover.total + finalFee;
                            cover.leisureInsureFee = finalFee;
                        }
                    }
                    else {
                        //get % this net is of the total net and use that to get the fee % from total fee                       
                        var percOfTotal = cover.net / totalnet;
                        var coverfee = finalFee * percOfTotal;
                        cover.total = cover.total + coverfee;
                        cover.leisureInsureFee = coverfee;
                    }
                }
            });

            if (policyData.brokerName != null) {
                var commisionRate = policyData.commissionPercentage / 100;
                var commision = (commisionRate) * totalnet;
                policyData.brokerCommission = commision;
            }

            //net and tax
            policyData.tax = totalnet * policyData.taxRate;
            policyData.net = totalnet;
            //excess
            policyData.excess = manCover.excess;

            //legal care with tax

            var legalCareRate = (policyData.legalRate / 100);
            var netLegalCare = 0;
            netLegalCare = totalnet * legalCareRate;
            var legalCareTax = netLegalCare * policyData.taxRate;
            legalCare = netLegalCare + legalCareTax;
            policyData.legalFees = legalCare;
            policyData.legalNet = netLegalCare;


            policyData.leisureInsureFee = finalFee;
            if (policyData.legalFeesAdded)
                policyData.total = policyData.net + policyData.tax + finalFee + legalCare;
            else
                policyData.total = policyData.net + policyData.tax + finalFee;



            return policyData;
        }


        // ---------------------------------- Equipment Hirers --------------------------------
        GetEquipPL(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let excessVal: number; excessVal = 0;

            _.each(cover.itemsAdded, (item: IListItem) => {

                if (policyData.tradeMember) {
                    item.initialCharge = item.discountFirstRate;
                    item.subsequentCharge = item.discountsubsequentRate;
                }
                else {
                    item.initialCharge = item.firstRate;
                    item.subsequentCharge = item.rate;
                }
                item.firstCharge = false;

                //if we have multiple quantites add as many as we need 
                for (var x = 0; x < item.quantity; x++) {
                    let newitem: IListItem;
                    newitem = angular.copy(item, newitem);
                    charges.push(newitem);
                }
                var rate = cover.rates.filter(x => x.id == item.rateId)[0];
                if (rate.refer)
                    policyData.refer = true

            });

            // take the highest maximum charge in the items    
            if (charges.length > 0) {

                //let maxCharge = charges.reduce((l, e) => e.firstCharge > l.firstCharge ? e : l);
                let sortedcharges: Array<IListItem>;

                sortedcharges = angular.copy(charges, sortedcharges);

                sortedcharges = _.sortBy(sortedcharges, "subsequentCharge");
                sortedcharges = _.sortBy(sortedcharges, "initialCharge");

                var maxCharge = sortedcharges[sortedcharges.length - 1];

                maxCharge.firstCharge = true;
                coverprice += maxCharge.initialCharge;
                //add up subsequent charges for each other item
                let subsequentCharges = sortedcharges.filter(x => x.firstCharge == false);

                _.each(subsequentCharges, (item: IListItem) => {
                    coverprice += item.subsequentCharge;
                });

                _.each(charges, (item: IListItem) => {
                    console.log("charges initialCharge", item.initialCharge);
                    console.log("charges subsequentCharge", item.subsequentCharge);
                });

                _.each(sortedcharges, (item: IListItem) => {
                    console.log("sortedcharges initialCharge", item.initialCharge);
                    console.log("sortedcharges subsequentCharge", item.subsequentCharge);
                });
            }

            //add on any loading - eg indemnity from our table
            if (cover.table != null) {

                //if trademember then get the rate for 1m (first row)
                //otherwise get whatever is selected
                var tradeRow: IRow;
                var selectedRow: IRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });

                if (policyData.tradeMember)
                    tradeRow = _.findWhere(cover.table.rows, { rowid: 1 });
                else
                    tradeRow = selectedRow;

                if (selectedRow == null)
                    return 0;

                //get column for this cover
                var selectedcell = _.findWhere(selectedRow.cells, { coverid: cover.id });
                var selectedRate = cover.rates.filter(x => x.id == selectedcell.rateid)[0];

                var tradeCell = _.findWhere(tradeRow.cells, { coverid: cover.id });
                var tradeRate = cover.rates.filter(x => x.id == tradeCell.rateid)[0];

                if (selectedRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = selectedRate.indemnity;
                coverprice += coverprice * tradeCell.rate / tradeCell.divby;
            }


            //excess - dropdown
            var excessPrice: number;
            var excess = _.findWhere(cover.rates, { rateTypeId: 7 });
            if (excess.inputSelected != null) {
                var option = excess.inputSelected;
                excessPrice = (coverprice * option.rate) / option.divBy;
                coverprice += excessPrice;
                excessVal = excess.inputSelected.excess;

                //nill excess - yes/no this is a flat charge depending on the excess selected
                var nillexcess = _.findWhere(cover.rates, { rateTypeId: 23 });
                if (nillexcess.rateValue == "yes") {
                    //add our nil excess flat charge
                    var nillExcessVal = option.nilExcessRate;
                    coverprice += nillExcessVal;
                    //take off the excess discount
                    coverprice -= excessPrice;
                }
            }



            //hazard questions - eg nightclub, flooding, are you a licencee etc
            //some of these questions have loadings some dont, but the ones that
            //dont still need to show in certificate which should be grouped together
            var genericLoadings = cover.rates.filter(x => x.rateTypeId == 18);
            _.each(genericLoadings, (loading: IRateView) => {
                //is the loading a yes / no?
                if (loading.inputTypeId == 19) {
                    if (loading.rateValue == "yes") {
                        if (loading.rate > 0) {
                            var loadingVal = (coverprice * loading.rate) / loading.divBy;
                            coverprice += loadingVal;
                        }
                    }
                }
            });

            if (policyData.tradeMember) {
                excessVal = 50 * policyData.locale.multiplier;
            }

            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;

            return coverprice;
        }

        GetEL(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let totalWageRoll: number; totalWageRoll = 0;
            let excess: number; excess = 0;
            let indemnity: number; indemnity = 0;

            indemnity = indemnity * policyData.locale.exchangeRate;

            if (cover.coverRequired) {
                _.each(cover.itemsAdded, (item: IListItem) => {

                    var wagerate = _.findWhere(cover.rates, { id: item.rateId });
                    coverprice += wagerate.rate * (item.rateValueNum / wagerate.divBy) * policyData.locale.multiplier;
                    totalWageRoll += item.rateValueNum;
                });
            }

            let minCharge: number; minCharge = 0;
            let sorted: IRateView[]; sorted = [];

            //min charges - order by threshold
            var minCharges = cover.rates.filter(x => x.rateTypeId == 13);
            sorted = this.filter("orderBy")(minCharges, ["threshold"]);

            if (totalWageRoll > 0) {
                for (var x = 0; x < sorted.length; x++) {
                    if (totalWageRoll < sorted[x].threshold) {
                        minCharge = sorted[x].rate;
                        break;
                    }
                }
            }

            if (minCharge > coverprice)
                coverprice = minCharge;

            if (coverprice > 0)
                indemnity = 10000000;

            cover.indemnity = indemnity;
            cover.excess = excess;
            cover.net = coverprice;
            cover.sumInsured = 0;
            cover.total = 0;
            cover.tax = 0;
            cover.leisureInsureFee = 0;

            return coverprice;

        }

        GetEquipMD(cover: ICoverView, policyData: IPolicyView): number {
            let coverprice: number; coverprice = 0;
            let suminsured: number; suminsured = 0;
            //get mandatory cover as thats where we put in the equipment value
            var mancover = policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            var addingMD = false;
            //note we are storing equipment cover with the dimensions           
            var rate = cover.rates.filter(x => x.rateTypeId == 6)[0];
            cover.excess = rate.excess * policyData.locale.multiplier;

            if (policyData.tradeMember) {
                cover.excess = 100 * policyData.locale.multiplier;
            }

            cover.itemsAdded = [];
            cover.itemsAdded = mancover.itemsAdded;

            _.each(cover.itemsAdded, (item: IListItem) => {
                //go through all items where we added sumInsured (equipment cover)                            
                if (item.sumInsured > 0) {
                    coverprice += (item.sumInsured * rate.rate) / rate.divBy;
                    suminsured += item.sumInsured;
                    addingMD = true;
                }
            });

            if (addingMD) {
                if (rate.minimumRate > coverprice)
                    coverprice = rate.minimumRate;
            } else
                cover.coverRequired = false;


            cover.indemnity = 0;
            cover.net = coverprice;
            cover.sumInsured = suminsured;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            cover.total = 0;



            return coverprice
        }

        GetProdL(cover: ICoverView, policyData: IPolicyView): number {
            var mancover = policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            let coverprice: number; coverprice = 0;

            cover.excess = mancover.excess;

            if (policyData.tradeMember) {
                cover.excess = 50 * policyData.locale.multiplier;
            }

            cover.indemnity = mancover.indemnity;
            cover.net = coverprice;
            cover.sumInsured = 0;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            cover.total = 0;

            return coverprice;

        }

        GetProfL(cover: ICoverView, policyData: IPolicyView): number {
            var mancover = policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;

            if (policyData.localeId == 1)
                indemnity = 1000000;
            else if (policyData.localeId == 2)
                indemnity = 1300000;

            cover.excess = mancover.excess;

            if (policyData.tradeMember) {
                cover.excess = 50 * policyData.locale.multiplier;
                cover.coverRequired = true;
            }

            cover.indemnity = indemnity
            cover.net = coverprice;
            cover.sumInsured = 0;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            cover.total = 0;

            return coverprice;

        }

        // ---------------------------------- Paintball --------------------------------
        GetPaintballPL(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let excess: number; excess = 500;

            if (policyData.localeId == 2)
                excess = excess * 2;

            //get premium - turnover
            var turnover = _.findWhere(cover.rates, { rateTypeId: 10 });
            //get rid of non numeric characters
            var input = turnover.rateValue.replace(/\D/g, '');
            var inputAsNum = parseInt(input);

            var turnoverVal = (inputAsNum * turnover.rate) / turnover.divBy * policyData.locale.multiplier;
            if (turnoverVal < turnover.minimumRate)
                turnoverVal = turnover.minimumRate;

            coverprice += turnoverVal;

            //add on any loading - eg indemnity from our table
            if (cover.table != null) {
                //get selected row

                var row = _.findWhere(cover.table.rows, { selected: true });
                if (row == null)
                    return 0;
                //get column for this cover
                var cell = _.findWhere(row.cells, { coverid: cover.id });
                var rate = cover.rates.filter(x => x.id == cell.rateid)[0];
                if (rate.refer == 1)
                    policyData.refer = true;
                indemnity = rate.indemnity;
                coverprice += coverprice * cell.rate / cell.divby;
            }

            if (indemnity > 0)
                cover.indemnity = indemnity;
            cover.net = coverprice;
            cover.sumInsured = 0;
            cover.excess = excess;

            return coverprice;
        }

        //select item, enter value, add item to list
        GetBusinessEquipment(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let suminsured: number; suminsured = 0;
            let excess: number; excess = 250;

            if (cover.coverRequired) {
                _.each(cover.itemsAdded, (item: IListItem) => {

                    var equiprate = _.findWhere(cover.rates, { id: item.rateId });
                    coverprice += equiprate.rate * (item.rateValueNum / equiprate.divBy);
                    suminsured = item.rateValueNum;
                });
            }

            let minCharge: number; minCharge = 0;
            let sorted: IRateView[]; sorted = [];

            cover.indemnity = 0;
            cover.excess = excess;
            cover.net = coverprice;
            cover.sumInsured = suminsured;
            cover.total = 0;
            cover.tax = 0;
            cover.leisureInsureFee = 0;

            return coverprice;

        }

        //select item, enter value 
        GetValueFromSelect(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let suminsured: number; suminsured = 0;
            var excess = 250;

            if (cover.coverRequired) {

                //ie EU, UK, Worldwide, get selected option
                var selectedType = _.findWhere(cover.rates, { inputTypeId: 7 });
                var rateType = selectedType.inputSelected;
                if (rateType != null) {

                    //get value entered from currency field
                    var rateValue = _.findWhere(cover.rates, { inputTypeId: 18 });
                    //get rid of non numeric characters
                    var input = rateValue.rateValue.replace(/\D/g, '');
                    var inputAsNum = parseInt(input);
                    suminsured = inputAsNum;
                    var val = (inputAsNum * rateType.rate) / rateType.divBy;

                    coverprice += val;
                }
            }

            cover.net = coverprice;
            cover.excess = excess; //? ireland
            cover.sumInsured = suminsured;
            cover.indemnity = 0;

            return coverprice;
        }

        //enter value only (2 inputs)
        TwoInputLoading(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let suminsured: number; suminsured = 0;
            var excess = 250;

            if (cover.coverRequired) {

                //get premium - turnover
                var rateValues = cover.rates.filter(x => x.rateTypeId == 10);

                _.each(rateValues, (value: IRateView) => {
                    //get rid of non numeric characters
                    var input = value.rateValue.replace(/\D/g, '');
                    var inputAsNum = parseInt(input);
                    suminsured += inputAsNum;
                    var stockVal = (inputAsNum * value.rate) / value.divBy;
                    coverprice += stockVal;
                });
            }

            cover.net = coverprice;
            cover.excess = excess; //? ireland
            cover.sumInsured = suminsured;
            cover.indemnity = 0;

            return coverprice;
        }

        //enter value only
        OneInputLoading(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let suminsured: number; suminsured = 0;
            var excess = 250 * policyData.locale.multiplier;

            if (cover.coverRequired) {

                //get premium - turnover
                var inputRate = _.findWhere(cover.rates, { rateTypeId: 10 });

                //get rid of non numeric characters
                var keyedinput = inputRate.rateValue.replace(/\D/g, '');
                var inputAsNum = parseInt(keyedinput);
                suminsured = inputAsNum;
                var mdVal = (inputAsNum * inputRate.rate) / inputRate.divBy;

                if (inputRate.minimumRate > mdVal && inputRate.minimumRate > 0)
                    mdVal = inputRate.minimumRate;

                coverprice += mdVal;
            }

            cover.net = coverprice;
            cover.excess = excess; //? ireland
            cover.sumInsured = suminsured;
            cover.indemnity = 0;

            return coverprice;
        }

        //enter value only (multiple inputs)
        GetMoney(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let suminsured: number; suminsured = 0;
            var excess = 250;

            if (cover.coverRequired) {

                //get premium - turnover
                var rateValues = cover.rates.filter(x => x.inputTypeId == 18);

                _.each(rateValues, (value: IRateView) => {
                    //get rid of non numeric characters
                    if (value.rateValue != null) {
                        var input = value.rateValue.replace(/\D/g, '');
                        var inputAsNum = parseInt(input);
                        suminsured += inputAsNum;
                        var moneyVal = (inputAsNum * value.rate) / value.divBy;
                        coverprice += moneyVal;
                    }
                });


                //have we added personal assault?
                var assault = cover.rates.filter(x => x.extraCoverId != null)[0];
                var addedCover = policyData.covers.filter(x => x.id == assault.extraCoverId)[0];
                if (assault != null) {
                    if (assault.rateValue == "true") {
                        //add 25%
                        var assaultVal = (coverprice * assault.rate) / assault.divBy;
                        coverprice += assaultVal;
                        //make sure cover is added

                        addedCover.total = 0;
                        addedCover.net = 0;
                        addedCover.excess = 250;
                        addedCover.indemnity = 0;
                        addedCover.sumInsured = 0;
                        addedCover.coverRequired = true;

                    }
                    else {
                        addedCover.coverRequired = false;
                    }
                }

            }


            cover.net = coverprice;
            cover.excess = excess; //? ireland
            cover.sumInsured = suminsured;
            cover.indemnity = 0;

            return coverprice;
        }

        // ***************************** Mobile Catering **********************************************************************************

        GetMobileCateringPL(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let excessVal: number; excessVal = 250 * policyData.locale.multiplier;

            //add on any loading - eg indemnity from our table
            if (cover.table != null) {

                //if trademember then get the rate for 1m (first row)
                //otherwise get whatever is selected
                var tradeRow: IRow;
                var selectedRow: IRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });

                tradeRow = selectedRow;

                if (selectedRow == null)
                    return 0;

                //get turnover column which has our rate
                var turnovercell = selectedRow.cells[4];
                var plcell = selectedRow.cells[1];
                var turnoverRate = cover.rates.filter(x => x.id == turnovercell.rateid)[0];
                cover.turnover = turnovercell.turnover;

                if (turnoverRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = plcell.indemnity;
                coverprice += turnoverRate.rate / turnovercell.divby;
            }

            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;

            //we can add in bundled covers this way when we add them on the PL table
            //no need for another method
            _.each(selectedRow.cells, (cell, ICell) => {
                if (cell.coverid > 0 && cell.coverid != cover.id) {
                    var addcover = policyData.covers.filter(x => x.id == cell.coverid)[0];
                    addcover.coverRequired = true;
                    addcover.excess = cover.excess;
                    addcover.indemnity = cell.indemnity;
                    addcover.total = 0;
                    addcover.net = 0;
                }
            });

            return coverprice;
        }

        // ***************************** Freelance Catering **********************************************************************************

        GetFreelanceCateringPL(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let excessVal: number; excessVal = 0;
            let nilexcess: number; nilexcess = 0;

            //add on any loading - eg indemnity from our table
            if (cover.table != null) {

                var selectedRow: IRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });

                if (selectedRow == null)
                    return 0;

                var plcell = selectedRow.cells[1];
                var plRate = cover.rates.filter(x => x.id == plcell.rateid)[0];

                if (plRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = plcell.indemnity;
                coverprice += plRate.rate / plRate.divBy;
                excessVal = plRate.excess * policyData.locale.multiplier;

                //nill excess - yes/no this is always based on the rate for 250
                var nilexcessRate = _.findWhere(cover.rates, { rateTypeId: 23 });
                if (nilexcessRate.rateValue == "yes") {
                    if (nilexcessRate.divBy == 100)
                        nilexcess = (coverprice * nilexcessRate.rate) / nilexcessRate.divBy;
                    else
                        nilexcess = nilexcessRate.rate;
                    coverprice += nilexcess;
                }
            }

            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;

            return coverprice;
        }


        // ***************************** Sports Instructor **********************************************************************************

        GetSportsInstructorPL(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let excessVal: number; excessVal = 0;
            let nilexcess: number; nilexcess = 0;

            //add on any loading - eg indemnity from our table
            if (cover.table != null) {

                var selectedRow: IRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });

                if (selectedRow == null)
                    return 0;

                var plcell = selectedRow.cells[1];
                var plRate = cover.rates.filter(x => x.id == plcell.rateid)[0];

                if (plRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = plcell.indemnity;

                _.each(cover.itemsAdded, (item: IListItem) => {
                    //get highest price

                    var activity = cover.rates.filter(x => x.id == item.rateId)[0];
                    var rate = cover.rates.filter(x => x.catRating == activity.catRating && plcell.indemnity == x.indemnity)[0];
                    if (rate.rate > coverprice)
                        coverprice = rate.rate;

                });

                excessVal = plRate.excess * policyData.locale.multiplier;

                //nill excess - yes/no this is always based on the rate for 250
                var nilexcessRate = _.findWhere(cover.rates, { rateTypeId: 23 });
                if (nilexcessRate.rateValue == "yes") {
                    if (nilexcessRate.divBy == 100)
                        nilexcess = (coverprice * nilexcessRate.rate) / nilexcessRate.divBy;
                    else
                        nilexcess = nilexcessRate.rate;
                    coverprice += nilexcess;
                }
            }

            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;

            return coverprice;
        }

        GetSportsInstructorMD(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let sumInsured: number; sumInsured = 0;
            let excess: number; excess = 0;
            let nilexcess: number; nilexcess = 0;

            //get select option
            if (cover.coverRequired) {
                var selected = cover.rates.filter(x => x.inputTypeId == 7)[0];

                if (selected.inputSelected != null) {
                    var option = selected.inputSelected;
                    excess = option.excess * policyData.locale.multiplier;

                    if (option.divBy == 1) {
                        coverprice = option.rate;
                        sumInsured = option.rateValueNum;
                    }
                    else {
                        //get free type value
                        var input = cover.rates.filter(x => x.inputTypeId == 18)[0];
                        var rateMultiplier = (input.rate / 100);
                        coverprice = input.rateValueNum * rateMultiplier;
                        sumInsured = input.rateValueNum;
                        excess = input.excess * policyData.locale.multiplier;
                    }
                }
            }

            cover.net = coverprice;
            cover.sumInsured = sumInsured;
            cover.excess = excess;

            return coverprice;
        }

        GetShowmenPL(cover: ICoverView, policyData: IPolicyView): number {
            var charges: IListItem[]; charges = [];
            let coverprice: number; coverprice = 0;
            let indemnity: number; indemnity = 0;
            let excessVal: number; excessVal = 0;

            _.each(cover.itemsAdded, (item: IListItem) => {


                item.initialCharge = item.firstRate;
                item.subsequentCharge = item.rate;

                item.firstCharge = false;

                //if we have multiple quantites add as many as we need 
                for (var x = 0; x < item.quantity; x++) {
                    let newitem: IListItem;
                    newitem = angular.copy(item, newitem);
                    charges.push(newitem);
                }
                var rate = cover.rates.filter(x => x.id == item.rateId)[0];
                if (rate.refer)
                    policyData.refer = true

            });

            // take the highest maximum charge in the items    
            if (charges.length > 0) {

                //let maxCharge = charges.reduce((l, e) => e.firstCharge > l.firstCharge ? e : l);
                let sortedcharges: Array<IListItem>;

                sortedcharges = angular.copy(charges, sortedcharges);

                sortedcharges = _.sortBy(sortedcharges, "subsequentCharge");
                sortedcharges = _.sortBy(sortedcharges, "initialCharge");

                var maxCharge = sortedcharges[sortedcharges.length - 1];

                maxCharge.firstCharge = true;
                coverprice += maxCharge.initialCharge;
                //add up subsequent charges for each other item
                let subsequentCharges = sortedcharges.filter(x => x.firstCharge == false);

                _.each(subsequentCharges, (item: IListItem) => {
                    coverprice += item.subsequentCharge;
                });

                _.each(charges, (item: IListItem) => {
                    console.log("charges initialCharge", item.initialCharge);
                    console.log("charges subsequentCharge", item.subsequentCharge);
                });

                _.each(sortedcharges, (item: IListItem) => {
                    console.log("sortedcharges initialCharge", item.initialCharge);
                    console.log("sortedcharges subsequentCharge", item.subsequentCharge);
                });
            }

            //add on any loading - eg indemnity from our table
            if (cover.table != null) {

                //if trademember then get the rate for 1m (first row)
                //otherwise get whatever is selected
                var tradeRow: IRow;
                var selectedRow: IRow;
                selectedRow = _.findWhere(cover.table.rows, { selected: true });

                if (policyData.tradeMember)
                    tradeRow = _.findWhere(cover.table.rows, { rowid: 1 });
                else
                    tradeRow = selectedRow;

                if (selectedRow == null)
                    return 0;

                //get column for this cover
                var selectedcell = _.findWhere(selectedRow.cells, { coverid: cover.id });
                var selectedRate = cover.rates.filter(x => x.id == selectedcell.rateid)[0];

                if (selectedRate.refer == 1)
                    policyData.refer = true;
                //indemnity is whatever is selected, but the price may be set from a cheaper one if trade member
                indemnity = selectedRate.indemnity;
                coverprice += coverprice * selectedcell.rate / selectedcell.divby;
            }

            //refer questions
            var yesnoquestions = cover.rates.filter(x => x.inputTypeId == 19);

            _.each(yesnoquestions, (question: IRateView) => {
                if (question.refer) {
                    if (question.invertReferral && question.rateValue == "no") {
                        policyData.refer = true;
                    }
                    if (!question.invertReferral && question.rateValue == "yes") {
                        policyData.refer = true;
                    }
                }
            }
            );



            if (indemnity > 0)
                cover.indemnity = indemnity;
            if (excessVal > 0)
                cover.excess = excessVal;
            cover.net = coverprice;
            cover.sumInsured = 0;

            return coverprice;
        }

        GetShowmenMD(cover: ICoverView, policyData: IPolicyView): number {
            let coverprice: number; coverprice = 0;
            let suminsured: number; suminsured = 0;
            //get mandatory cover as thats where we put in the equipment value
            var mancover = policyData.covers.filter(x => x.coverType == "Mandatory")[0];
            var addingMD = false;
            //note we are storing equipment cover with the dimensions           
            var rate = cover.rates.filter(x => x.rateTypeId == 6)[0];
            cover.excess = rate.excess * policyData.locale.multiplier;


            cover.itemsAdded = [];
            cover.itemsAdded = mancover.itemsAdded;

            _.each(cover.itemsAdded, (item: IListItem) => {
                //go through all items where we added sumInsured (equipment cover)                            
                if (item.sumInsured > 0) {
                    coverprice += (item.sumInsured * rate.rate) / rate.divBy;
                    suminsured += item.sumInsured;
                    addingMD = true;
                }
            });

            if (addingMD) {
                if (rate.minimumRate > coverprice)
                    coverprice = rate.minimumRate;
            } else
                cover.coverRequired = false;

            cover.indemnity = 0;
            cover.net = coverprice;
            cover.sumInsured = suminsured;
            cover.tax = 0;
            cover.leisureInsureFee = 0;
            cover.total = 0;



            return coverprice
        }

        CalculateInstalments(data: IInstalments, policy: IPolicyView) {

            //£100 -£1, 000       18.00%
            //£1, 000 -£2, 000    12.50%
            //£2, 000 -£3, 000    10.00%
            //£3, 000 +           8.50 %            

            var newtotal;
            var total = policy.total;

            if (total <= 1000) {
                newtotal = total * 1.18;
            }
            else if (total > 1000 && total < 2000) {
                newtotal = total * 1.125;
            }
            else if (total >= 2000 && total < 3000) {
                newtotal = total * 1.10;
            }
            else if (total >= 3000) {
                newtotal = total * 1.085;
            }

            data.numberOfPayments = 8;
            var instalment = newtotal / 10;
            //deposit is 2 monthly instalments
            data.deposit = instalment * 2;
            //remaining left after deposit
            var remaining = newtotal - data.deposit;
            //8 payments in total        
            var monthlyPayment = remaining / 8;
            data.eachPayment = monthlyPayment;
            data.totalToPay = newtotal;
        }

    }

    angular.module("App").service("PriceService", PriceService);

    export enum CoverEnum {
        EquipPL = 1,
        EquipEL = 2,
        EquipMD = 4,
        EquipProd = 5,
        EquipProf = 28,
        PaintBallPL = 6,
        PaintBallEL = 7,
        PaintBallBuildings = 20,
        PaintBallAncillaryBuildings = 8,
        PaintBallFixtures = 9,
        PaintBallPlayingSurfaces = 10,
        PaintBallBusinessEquip = 11,
        PaintBallTrophies = 12,
        PaintBallMachinery = 13,
        PaintBallStock = 14,
        PaintBallRefrigerated = 15,
        PaintBallPropertyInTransit = 16,
        PaintBallMoney = 17,
        PaintBallRentPayable = 18,
        PaintBallLossOfGrossProfit = 19,
        PaintBallPersonalAssault = 21,
        PaintBallProdLiability = 22,
        PaintBallProfLiability = 23,
        PaintBallLossOfGrossRevenue = 24,
        PaintBallLossOfGrossRentals = 25,
        PaintBallBookDebts = 26,
        PaintBallCostOfWorking = 27,
        AirsoftPL = 29,
        AirsoftEL = 30,
        AirsoftBuildings = 43,
        AirsoftAncillaryBuildings = 31,
        AirsoftFixtures = 32,
        AirsoftPlayingSurfaces = 33,
        AirsoftBusinessEquip = 34,
        AirsoftTrophies = 35,
        AirsoftMachinery = 36,
        AirsoftStock = 37,
        AirsoftRefrigerated = 38,
        AirsoftPropertyInTransit = 39,
        AirsoftMoney = 40,
        AirsoftRentPayable = 41,
        AirsoftLossOfGrossProfit = 42,
        AirsoftPersonalAssault = 44,
        AirsoftProdLiability = 45,
        AirsoftProfLiability = 46,
        AirsoftLossOfGrossRevenue = 47,
        AirsoftLossOfGrossRentals = 48,
        AirsoftBookDebts = 49,
        AirsoftCostOfWorking = 50,
        MobileCateringPL = 51,
        MobileCateringMD = 52,
        MobileCateringEL = 53,
        MobileCateringProd = 54,
        MobileCateringStockGeneral = 55,
        FreelanceCateringPL = 56,
        SportsInstructorPL = 57,
        SportsInstructorEL = 70,
        SportsInstructorMD = 71,
        ShowmenPL = 72,
        ShowmenEL = 73,
        ShowmenMD = 74,
        ShowmenProducts = 75,
        ShowmenBusinessEquip = 76,
    }
}



//function ShortSumInsured(number: number): string {
//    if (number >= 1000000) {
//        number = Math.round(number / 1000000);
//    }
//    return number.toString() + "M";
//}