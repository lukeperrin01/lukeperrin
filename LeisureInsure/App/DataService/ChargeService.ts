﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class ChargeService {
        static $inject = ["$rootScope", "$http", "$q", "$filter", "CoverInputsService", "LocationService"];
        http: ng.IHttpService;
        q: ng.IQService;
        //rootScope: ng.IRootScopeService;
        rootScope: any;
        chargeSummaryChanged: string;
        filter: any;

        constructor($rootScope, $http, $q, $filter, coverInputsService, localeService) {
            this.chargeSummaryChanged = "CHARGE_SERVICE.CHARGE_SUMMARY_CHANGED";

            this.http = $http;
            this.q = $q;
            this.coverInputsService = coverInputsService;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.filter = $filter;

            this.localeService.onSelectedLocaleChanged($rootScope, () => {
                if (this.chargeSummary) {
                    this.updateQuote();
                }
            });
        }
        localeService: LocationService;

        chargeSummary: IChargeSummary;
        coverInputsService: CoverInputsService;
        quoteReference: string;
        quoteId: number;
        password: string;
        customerID: number;
        landingPageId: number;
        noInstructors: number;
        refer: boolean;
        sortedHazards: Array<IChargeLine>;
        additionalItems: Array<IChargeLine>;
        renewal: number;
        paymentTypeId: number;//one off or finance payment                     
        dateFromDt: Date;
        dateToDt: Date;
        onChargeSummaryChanged(scope: ng.IScope, handler: (chargeSummary: IChargeSummary) => void) {
            scope.$on(this.chargeSummaryChanged, (event, message) => {
                handler(message);
            });
        }

        private copyData(data: IChargeSummary): void {
            this.chargeSummary = angular.copy(data);
            this.quoteReference = this.chargeSummary.quoteReference;
            this.quoteId = this.chargeSummary.id;
            this.password = this.chargeSummary.password;
            this.landingPageId = this.chargeSummary.landingPageId;
            this.renewal = this.chargeSummary.renewal;
            this.dateFromDt = new Date(data.dateFrom);
            this.dateToDt = new Date(data.dateTo);
            this.rootScope.$broadcast(this.chargeSummaryChanged, this.chargeSummary);
        }

        resetChargeService() {
            this.chargeSummary = null;
            this.quoteReference = "";
            this.quoteId = 0;
            this.password = "";
            this.landingPageId = 0;
        }

        removeCover(coverId: number): ng.IPromise<any> {
            var defer = this.q.defer();

            this.http({
                url: "api/v1/payment/removecover/" + coverId,
                method: "POST",
                data: {
                    quoteReference: this.quoteReference,
                    passcode: this.password
                }
            })
                .success((data: IChargeSummary) => {
                    // Successful
                    this.copyData(data);
                    defer.resolve(this.chargeSummary);
                })
                .catch((data: any) => {                    // Error

                    defer.reject(data.data.message);
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });

            return defer.promise;
        }

        toggleLegalCare(): ng.IPromise<IChargeSummary> {

            this.chargeSummary.legalFeesAdded = !this.chargeSummary.legalFeesAdded;
            return this.updateQuote();
        }

        toggleExcessWaiver(): ng.IPromise<IChargeSummary> {
            this.chargeSummary.excessWaiverAdded = !this.chargeSummary.excessWaiverAdded;
            return this.updateQuote();
        }

        selectCard(cardId: number): ng.IPromise<IChargeSummary> {
            this.chargeSummary.paymentCardId = cardId;
            return this.updateQuote();
        }

        //this sends the UpdateQuoteVm model
        updateQuotePeriod() {

            this.updateQuote();
        }

        private updateQuote(): ng.IPromise<IChargeSummary> {
            var deferred = this.q.defer<IChargeSummary>();

            var dateTo = GetDateTime(this.dateToDt);
            var dateFrom = GetDateTime(this.dateFromDt);

            this.http({
                url: "/api/v1/updateQuote/" + this.chargeSummary.quoteReference,
                method: "PUT",
                data: {
                    localeId: this.localeService.selectedLocale.localePK,
                    password: this.chargeSummary.password,
                    legalCare: this.chargeSummary.legalFeesAdded,
                    excessWaiver: this.chargeSummary.excessWaiverAdded,
                    cardId: this.chargeSummary.paymentCardId,
                    payOption: this.paymentTypeId,
                    dateFrom: dateFrom,
                    dateTo: dateTo

                }
            })
                .success((data: IChargeSummary) => {
                    // Successful                    
                    this.copyData(data);
                    deferred.resolve(data);
                })
                .catch((data: any) => {
                    // Error                   
                    deferred.reject(data.data.message);
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
            return deferred.promise;
        }

        updatePaymentCard(paymentCardId: number) {
            this.chargeSummary.paymentCardId = paymentCardId;
            return this.updateQuote();
        }

        //only show hazard sections (Material damage etc)  once in the quote summary 
        sortHazardGroups(): void {

            var hazards: IChargeLine[] = Array();

            var items = angular.copy(this.chargeSummary.chargeSummaryItems);

            var temp = "";
            if (this.chargeSummary != null) {

                //get our hazard prices
                $.each(items, function (index, item) {
                    if (item.sectionTypeDescription != null) {

                        if (item.showInSummary == true)
                            hazards.push(item);
                    }
                });

                //reverse so chargeable items are at the top
                var sorted: IChargeLine[] = this.filter('orderBy')(hazards, 'price', true)

                this.sortedHazards = [];
                var result: IChargeLine[] = Array();

                $.each(sorted, function (index, item) {

                    if (temp != item.sectiondescription) {
                        temp = item.sectiondescription;
                    }
                    else//if next cover is in same section dont show description again
                        item.sectiondescription = "";

                    result.push(item);
                });

                this.sortedHazards = result;
            }
        }

        loadQuote(quoteReference: string, password: string): ng.IPromise<IChargeSummary> {
            var deferred = this.q.defer<IChargeSummary>();
            //this.rootScope.loadingquote = true; 

            this.http({
                url: "/api/v1/quotes/" + quoteReference + "?password=" + password,
                method: "GET",
                data: {
                    password: password
                }
            })
                .success((data: IChargeSummary) => {
                    // Successful
                    this.quoteId = data.id;
                    this.copyData(data);
                    this.sortHazardGroups();
                    deferred.resolve(data);
                })
                .catch((data: any) => {
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
            return deferred.promise;
        }

        CreateQuote(inputs: Array<IChargeInputs>, policyId: number): ng.IPromise<Array<IChargeSummaryItem>> {
            var deferred = this.q.defer<Array<IChargeSummaryItem>>();
            this.rootScope.loadingquote = true;

            var browser = eval("GetBrowserDetails()");
            var currentDate = new Date();
            var currentTime = currentDate.toLocaleTimeString([],
                { hour: '2-digit', minute: '2-digit' });

            this.http({
                url: "/api/v1/charges",
                method: "POST",
                data: {
                    localeId: this.localeService.selectedLocale.localePK,
                    policyId: policyId,
                    inputs: inputs,
                    quoteReference: this.quoteReference,
                    quoteId: this.quoteId,
                    password: this.password,
                    landingPageId: this.landingPageId,
                    browserTime: currentTime,
                    browser: browser

                }
            })
                .success((data: IChargeSummary) => {
                    // Successful                    
                    this.copyData(data);
                    this.sortHazardGroups();
                    //console.log("chargeSummary", data);
                    deferred.resolve();
                    this.rootScope.loadingquote = false;
                })
                .catch((data: any) => {
                    // Error
                    this.rootScope.loadingquote = false;
                    deferred.reject(data.data.message);
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
            return deferred.promise;
        }

        emailQuote(email: string): ng.IHttpPromise<any> {
            var deferred = this.q.defer();
            return this.http({
                url: "/api/v1/communications/email-quote",
                method: "POST",
                data: {
                    email: email,
                    quoteReference: this.chargeSummary.quoteReference,
                    quoteId: this.chargeSummary.quoteId,
                    passcode: this.chargeSummary.password
                }
            }).success((data: any) => {
                deferred.resolve();
            }).error((data: any) => {
                deferred.resolve(data.message);
            });
        }


        lockQuote(quoteReference: string, lockQuote: number) {
            return this.http({
                url: "/api/v1/payment/lockQuotexx",
                method: "POST",
                data: {
                    quoteReference: quoteReference,
                    lockQuotex: lockQuote
                }
            });
        }

        updateQuoteDates(dates: IDates): ng.IPromise<any> {

            if (dates.dateFrom != null) {
                var dateFrom = GetDateTime(dates.dateFrom);
                var dateTo = dateFrom;
                if (dates.dateTo != null)
                    dateTo = GetDateTime(dates.dateTo);
                var timefrom = dates.startingHour + ":" + dates.startingMinute;
                var timeto = dates.endingHour + ":" + dates.endingMinute;

                var deferred = this.q.defer();

                this.http({
                    url: "/api/v1/updateQuoteDates/" + this.chargeSummary.quoteReference,
                    method: "POST",
                    data: {
                        startDate: dateFrom,
                        endDate: dateTo,
                        timeFrom: timefrom,
                        timeTo: timeto,
                        dateToRequired: dates.dateToRequired,
                        startTimeRequired: dates.startTimeRequired,
                        endTimeRequired: dates.endTimeRequired,
                        numberOfDays: dates.coverDays
                    }
                })
                    .success((data: any) => {
                        deferred.resolve(data);
                    })
                    .catch((data: any) => {
                        // Error
                        deferred.reject(data.data.message);
                        this.rootScope.loadingpurchase = false;
                        eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                    });
                return deferred.promise;
            }

        }

        completeAsAgent(dates: IDates): ng.IPromise<any> {

            var dateFrom = GetDateTime(dates.dateFrom);
            var dateTo = dateFrom;
            if (dates.dateTo != null)
                dateTo = GetDateTime(dates.dateTo);
            var timefrom = dates.startingHour + ":" + dates.startingMinute;
            var timeto = dates.endingHour + ":" + dates.endingMinute;

            var deferred = this.q.defer();
            this.rootScope.loadingpurchase = true;
            this.http({
                url: "/api/v1/completeAgent/" + this.chargeSummary.quoteReference,
                method: "POST",
                data: {
                    startDate: dateFrom,
                    endDate: dateTo,
                    timeFrom: timefrom,
                    timeTo: timeto,
                    dateToRequired: dates.dateToRequired,
                    startTimeRequired: dates.startTimeRequired,
                    endTimeRequired: dates.endTimeRequired,
                    numberOfDays: dates.coverDays
                }
            })
                .success((data: any) => {
                    deferred.resolve(data);
                    this.rootScope.loadingpurchase = false;
                })
                .catch((data: any) => {
                    // Error
                    deferred.reject(data.data.message);
                    this.rootScope.loadingpurchase = false;
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
            return deferred.promise;
        }

        addCustomerDetails(contactDetails): ng.IPromise<any> {
            var deferred = this.q.defer();
            //this.rootScope.loadingpurchase = true;
            this.http({
                url: "/api/v1/addCustomerDetails/",
                method: "POST",
                data: contactDetails
            })
                .then(() => {
                    this.rootScope.loadingpurchase = false;
                    deferred.resolve();
                },
                response => {
                    deferred.reject(response.data.exceptionMessage);
                })
                .catch((data: any) => {
                    // Error
                    deferred.reject(data.data.message);
                    //this.rootScope.loadingpurchase = false;
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });

            return deferred.promise;
        }


        //these methods are for the new website 
        updateQuoteDatesNew(policyData: IPolicyView): ng.IPromise<any> {

            //var dateFrom = GetDateTime(policyData.dates.dateFrom);
            //var dateTo = dateFrom;
            //if (policyData.dates.dateTo != null)
            //    dateTo = GetDateTime(policyData.dates.dateTo);
            //var timefrom = policyData.dates.startingHour + ":" + policyData.dates.startingMinute;
            //var timeto = policyData.dates.endingHour + ":" + policyData.dates.endingMinute;
            var json = JSON.stringify(policyData);

            var deferred = this.q.defer();

            this.http({
                url: "/api/v1/updateQuoteDatesNew",
                method: "POST",
                data: json
            })
                .success((data: any) => {
                    deferred.resolve(data);
                })
                .catch((data: any) => {
                    // Error
                    deferred.reject(data.data.message);
                    this.rootScope.loadingpurchase = false;
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
            return deferred.promise;

        }

        loadPolicyData(quoteRef: string, password: string): ng.IPromise<IPolicyView> {
            var deferred = this.q.defer<IPolicyView>();
            this.rootScope.loading = true;

            this.http({
                url: "/api/v1/loadquote?quoteRef=" + quoteRef + "&password=" + password,
                method: "GET"
            })
                .success((data: IPolicyView) => {
                    this.quoteId = data.id;                                       
                    this.rootScope.loading = false;
                    deferred.resolve(data);
                })
                .catch(err => this.ShowError(err, deferred));
            return deferred.promise;
        }

        SaveQuote(policyData: IPolicyView): ng.IPromise<IPolicyView> {
            var deferred = this.q.defer<IPolicyView>();
            this.rootScope.loading = true;

            var browser = eval("GetBrowserDetails()");
            var currentDate = new Date();
            var currentTime = currentDate.toLocaleTimeString([],
                { hour: '2-digit', minute: '2-digit' });

            var json = JSON.stringify(policyData);

            this.http({
                url: "/api/v1/savequote",
                data: json,
                method: "POST"
            })
                .success((data: IPolicyView) => {
                    this.rootScope.loading = false;

                    deferred.resolve(data);

                })
                .catch(err => this.ShowError(err, deferred));
            return deferred.promise;
        }

        SaveAddress(policyData: IPolicyView): ng.IPromise<IPolicyView> {
            var deferred = this.q.defer<IPolicyView>();
            this.rootScope.loading = true;

            var browser = eval("GetBrowserDetails()");

            var currentDate = new Date();
            var currentTime = currentDate.toLocaleTimeString([],
                { hour: '2-digit', minute: '2-digit' });

            var json = JSON.stringify(policyData);

            this.http({
                url: "/api/v1/saveaddress",
                data: json,
                method: "POST"
            })
                .success((data: IPolicyView) => {
                    this.rootScope.loading = false;

                    deferred.resolve(data);

                })
                .catch(err => this.ShowError(err, deferred));
            return deferred.promise;
        }

        completeAsAgentNew(): ng.IPromise<any> {

            var deferred = this.q.defer();
            this.rootScope.loading = true;
            this.http({
                url: "/api/v1/agentpayment",
                method: "POST",
                data: {
                    StripeToken: null,
                    QuoteRef: PaymentData.quoteRef,
                    QuotePass: PaymentData.quotePass,
                    CurrencyId: PaymentData.currencyId,
                    InstalmentData: null,
                    PolicyDates: Dates
                }
            })
                .success((data: any) => {
                    deferred.resolve(data);
                    this.rootScope.loadingpurchase = false;
                })
                .catch(err => this.ShowError(err, deferred));
            return deferred.promise;
        }

        ShowError(err: any, defer: any): any {
            this.rootScope.loading = false;
            this.rootScope.savingquote = false;
           
            defer.reject(err.data.message);
        }
    }

    angular.module("App").service("ChargeService", ChargeService);
}
