var LeisureInsure;
(function (LeisureInsure) {
    var CoverInputsService = (function () {
        function CoverInputsService($q, $filter) {
            this.filter = $filter;
            this.selectedCoverId = 0;
            this.inputs = Array();
            this.covers = Array();
            var deferred = $q.defer();
            this.q = deferred;
            this.initialised = deferred.promise;
        }
        CoverInputsService.prototype.addFlatInputs = function (flatInputs) {
            angular.copy(flatInputs, this.inputs);
            this.q.resolve(this);
        };
        CoverInputsService.$inject = ["$q", "$filter"];
        return CoverInputsService;
    }());
    LeisureInsure.CoverInputsService = CoverInputsService;
    angular.module("App").service("CoverInputsService", CoverInputsService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=CoverInputsService.js.map