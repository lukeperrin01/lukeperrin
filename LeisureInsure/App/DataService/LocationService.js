var LeisureInsure;
(function (LeisureInsure) {
    var LocationService = (function () {
        function LocationService($http, $q, $rootScope) {
            this.changeEvent = "LOCALE_CHANGED_EVENT";
            this.rootScope = $rootScope;
            this.http = $http;
            this.locales = Array();
            this.btnDisabled = false;
            var deferred = $q.defer();
            this.q = deferred;
            this.qx = $q;
            this.getlocales("", 1);
            this.initialised = deferred.promise;
        }
        LocationService.prototype.getlocales = function (locale, policyid) {
            var _this = this;
            var deferredx = this.qx.defer();
            this.http({
                url: "/api/v1/data/getLocales",
                method: "GET",
                params: { strLocale: locale, policyid: policyid }
            })
                .success(function (data) {
                if (locale == "") {
                    _this.locales = data.localeList;
                }
                if (!_this.selectedLocale || _this.selectedLocale.strLocale != data.localeSelected.strLocale) {
                    _this.selectedLocale = data.localeSelected;
                }
                _this.updateSelectedLocale(data.localeSelected);
            })
                .catch(function (data) {
                deferredx.reject(data.exceptionMessage);
            });
            return deferredx.promise;
        };
        LocationService.prototype.updateSelectedLocale = function (locale) {
            this.selectedLocale = locale;
            this.rootScope.$broadcast(this.changeEvent, locale);
            this.q.resolve(this);
        };
        LocationService.prototype.disableLocaleBtn = function (trueFalse) {
            if (this.selectedLocale != null) {
                this.selectedLocale.showFlag = trueFalse;
            }
        };
        ;
        LocationService.prototype.onSelectedLocaleChanged = function (scope, handler) {
            var _this = this;
            scope.$on(this.changeEvent, function (event) {
                handler(_this.selectedLocale);
            });
        };
        LocationService.$inject = ["$http", "$q", "$rootScope"];
        return LocationService;
    }());
    LeisureInsure.LocationService = LocationService;
    angular.module("App").service("LocationService", LocationService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=LocationService.js.map