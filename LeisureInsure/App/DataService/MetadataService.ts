﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/angularjs/angular-route.d.ts" />



module LeisureInsure {
    export class MetadataService {
        static $inject = ["$rootScope", "$routeParams"];
        routeParams: ng.route.IRouteParamsService;
        constructor($rootScope: ng.IRootScopeService, $routeParams: ng.route.IRouteParamsService) {
            this.routeParams = $routeParams;
            this.setMetadata(
                "Leisure Insure",
                "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure."
            );

            // Route change handler, sets the route's defined metadata
            $rootScope.$on('$routeChangeSuccess', (event, newRoute) => {
                if (newRoute.metadata) {
                    this.setRouteMetadata(newRoute.metadata);
                }
            });
        }

        title: string;
        description: string;
        keywords: string;

        ogUrl: string;
        ogTitle: string;
        ogDescription: string;
        ogType: string;
        ofImage: string;

        twitterCard: string;
        twitterTitle: string;
        twitterDescription: string;
        twitterSite: string;
        twitterImage: string;

        setRouteMetadata(metadata: IMetadata): void {
            var title: string = metadata.title;
            while (/\[.*\]/i.test(title)) {
                var urlSegment = title.substring(title.indexOf("[") + 1, title.indexOf("]"));
                var swappedOutSegment = this.routeParams[urlSegment];
                if (!swappedOutSegment) {
                    break;
                }
                var replacement = swappedOutSegment.replace(/([^ ])(-)([^ ])/g, "$1 $3");
                title = title.replace("[" + urlSegment + "]", replacement);
            }
            this.setMetadata(title, metadata.description, metadata.keywords);
        }

        setMetadata(title: string, description: string, keywords?: string): void {
            if (keywords) {
                this.keywords = keywords;
            }
            
            this.title = title;
            this.twitterTitle = title;
            this.ogTitle = title;

            this.description = description;
            this.ogDescription = description;
            this.twitterDescription = description;
        }
    }

    angular.module("App").service("MetadataService", MetadataService);
}
