var LeisureInsure;
(function (LeisureInsure) {
    var CertificateService = (function () {
        function CertificateService($rootScope, $http, $q, coverInputsService, localeService, chargeService) {
            this.http = $http;
            this.q = $q;
            this.coverInputsService = coverInputsService;
            this.chargeService = chargeService;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.quoteReference = this.chargeService.quoteReference;
            this.password = this.chargeService.password;
        }
        CertificateService.prototype.GenerateCertificate = function (quoteReference, password) {
            this.quoteReference = quoteReference;
            var deferred = this.q.defer();
            this.http({
                url: "certificates/generate/",
                method: "POST",
                params: { quoteReference: quoteReference, passcode: password }
            })
                .success(function () {
                deferred.resolve();
            })
                .catch(function () {
                deferred.reject();
            });
            return deferred.promise;
        };
        CertificateService.prototype.GetStatementofFact = function (quoteReference) {
            this.quoteReference = this.chargeService.quoteReference;
            this.password = this.chargeService.password;
            var urlString = "GetStatementofFact/" + this.quoteReference + "/";
            var deferred = this.q.defer();
            this.http({
                url: urlString,
                method: "GET"
            })
                .success(function () {
                alert("HIT SUCCESS");
                deferred.resolve();
            })
                .catch(function () {
                alert("HIT FAIL");
                deferred.reject();
            });
            return deferred.promise;
        };
        CertificateService.prototype.getCertificate = function (quoteReference) {
            this.quoteReference = this.chargeService.quoteReference;
            this.password = this.chargeService.password;
            var urlString = "certificate/" + this.quoteReference + "/" + this.password + "/";
            var deferred = this.q.defer();
            this.http({
                url: urlString,
                method: "GET"
            })
                .success(function () {
                deferred.resolve();
            })
                .catch(function () {
                deferred.reject();
            });
            return deferred.promise;
        };
        CertificateService.$inject = ["$rootScope", "$http", "$q", "CoverInputsService", "LocationService", "ChargeService"];
        return CertificateService;
    }());
    LeisureInsure.CertificateService = CertificateService;
    angular.module("App").service("CertificateService", CertificateService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=CertificateService.js.map