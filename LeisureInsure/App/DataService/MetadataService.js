var LeisureInsure;
(function (LeisureInsure) {
    var MetadataService = (function () {
        function MetadataService($rootScope, $routeParams) {
            var _this = this;
            this.routeParams = $routeParams;
            this.setMetadata("Leisure Insure", "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure.");
            $rootScope.$on('$routeChangeSuccess', function (event, newRoute) {
                if (newRoute.metadata) {
                    _this.setRouteMetadata(newRoute.metadata);
                }
            });
        }
        MetadataService.prototype.setRouteMetadata = function (metadata) {
            var title = metadata.title;
            while (/\[.*\]/i.test(title)) {
                var urlSegment = title.substring(title.indexOf("[") + 1, title.indexOf("]"));
                var swappedOutSegment = this.routeParams[urlSegment];
                if (!swappedOutSegment) {
                    break;
                }
                var replacement = swappedOutSegment.replace(/([^ ])(-)([^ ])/g, "$1 $3");
                title = title.replace("[" + urlSegment + "]", replacement);
            }
            this.setMetadata(title, metadata.description, metadata.keywords);
        };
        MetadataService.prototype.setMetadata = function (title, description, keywords) {
            if (keywords) {
                this.keywords = keywords;
            }
            this.title = title;
            this.twitterTitle = title;
            this.ogTitle = title;
            this.description = description;
            this.ogDescription = description;
            this.twitterDescription = description;
        };
        MetadataService.$inject = ["$rootScope", "$routeParams"];
        return MetadataService;
    }());
    LeisureInsure.MetadataService = MetadataService;
    angular.module("App").service("MetadataService", MetadataService);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=MetadataService.js.map