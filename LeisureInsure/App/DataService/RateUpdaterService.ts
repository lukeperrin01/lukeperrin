﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />



module LeisureInsure {
    export class RateUpdaterService {
        static $inject = ["$http", "$q","$rootScope"];
        http: ng.IHttpService;
        q: ng.IQService;
        rootScope: any;

        constructor($http, $q, $rootScope) {
           
            this.http = $http;
            this.q = $q;     
            this.rootScope = $rootScope;       
            //this.initialised = this.q.promise;
        }   

        UpdateRate(ratemodel:IRateView): ng.IPromise<IRateView> {
            var deferred = this.q.defer<IRateView>();
            var json = JSON.stringify(ratemodel);
            this.rootScope.loading = true;

            this.http({
                url: "/api/v1/ratechecker/updaterate",
                method: "PUT",
                data:json
            })
                .success((data: IRateView) => {
                    this.rootScope.loading = false;
                    deferred.resolve(data);
                })
                .catch(err => this.ShowError(err,deferred));

            return deferred.promise;
        }        

        UpdateLocale(localemodel: ILocale): ng.IPromise<ILocale> {
            var deferred = this.q.defer<ILocale>();
            var json = JSON.stringify(localemodel);
            this.rootScope.loading = true;

            this.http({
                url: "/api/v1/ratechecker/updatelocale",
                method: "PUT",
                data: json
            })
                .success((data: ILocale) => {
                    this.rootScope.loading = false;
                    deferred.resolve(data);
                })
                .catch(err => this.ShowError(err, deferred));

            return deferred.promise;
        }        

        GetPoliciesCoversRates(): ng.IPromise<Array<IPolicyView>> {
            var deferred = this.q.defer<Array<IPolicyView>>();
            this.http({
                url: "/api/v1/ratechecker/getpolicies",
                method: "GET"                
            })
                .success((data: Array<IPolicyView>) => {                    

                    deferred.resolve(data);
                })
                .catch(err => this.ShowError(err,deferred));
            return deferred.promise;
        }

        GetLocales(): ng.IPromise<Array<ILocale>> {
            var deferred = this.q.defer<Array<ILocale>>();
            this.http({
                url: "/api/v1/ratechecker/getlocales",
                method: "GET"
            })
                .success((data: Array<ILocale>) => {

                    deferred.resolve(data);
                })
                .catch(err => this.ShowError(err, deferred));
            return deferred.promise;
        }

        ShowError(err: any, defer: any): any {         
            this.rootScope.loading = false;   
            eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + err.data.message + "');");
            defer.reject(err.data.message);
        }
        

    }

    angular.module("App").service("RateUpdaterService", RateUpdaterService);
}
