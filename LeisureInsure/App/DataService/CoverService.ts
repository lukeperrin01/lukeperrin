﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />



module LeisureInsure {
    export class CoverService {
        static $inject = ["$q", "$filter"];
        filter: any; //ng.IRootScopeService;

        constructor($q, $filter) {

            this.filter = $filter;
            this.selectedCoverId = 0;
            this.covers = Array<ICover>();
            this.policyCovers = Array<ICover>();
            var deferred = $q.defer();
            this.q = deferred;
            this.initialised = deferred.promise;
        }

        covers: Array<ICover>;
        selectedCoverId: number;
        selectedCover: ICover;
        selectedCovers: Array<ICover>;
        policyCovers: Array<ICover>;
        q: ng.IQService;
        initialised: ng.IPromise<CoverService>;

        addCovers(covers: Array<any>): void {
            angular.copy(covers, this.covers);
            this.q.resolve(this);
        }
        updateSelectedCover(selectedCover: ICover): void {
            this.selectedCover = selectedCover;
            this.selectedCoverId = selectedCover.coverPK;
        }
        refreshCovers(coverList: Array<any>): void {
            this.covers = coverList;
        }

        coversForPolicy(coverFilter: Number): void {
            var selectedCovers = new Array<ICover>();
            if (coverFilter > 0) {
                _.each(this.covers, (cover) => {
                        selectedCovers.push(cover);
                });
                this.policyCovers = selectedCovers;
            }
            else {
                this.policyCovers = this.covers;
            };
        }
    }

    angular.module("App").service("CoverService", CoverService);
}
