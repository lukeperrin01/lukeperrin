﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class StripeService {
        static $inject = ["$rootScope", "$http", "$q", "$filter", "LocationService"];
        http: ng.IHttpService;
        q: ng.IQService;
        //rootScope: ng.IRootScopeService;
        rootScope: any;
        filter: any;
        localeService: LocationService;       
        

        constructor($rootScope, $http, $q, $filter, localeService) {

            this.http = $http;
            this.q = $q;
            this.localeService = localeService;
            this.rootScope = $rootScope;
            this.filter = $filter;            
        }

        GetStripeData(): ng.IPromise<IStripe> {
            var deferred = this.q.defer<IStripe>();
            this.rootScope.loading = true;

            this.http({
                url: "/api/v1/getstripedata",
                method: "GET"
            })
                .success((data: IStripe) => {
                    this.rootScope.loading = false;                    
                    deferred.resolve(data);
                })
                .catch(err => this.ShowError(err, deferred));
            return deferred.promise;
        }

        ShowError(err: any, defer: any): any {
            this.rootScope.loading = false;
            eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + err.data.message + "');");
            defer.reject(err.data.message);
        }


    }

    angular.module("App").service("StripeService", StripeService);
}



