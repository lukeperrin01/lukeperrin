﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/angular-ui-bootstrap/angular-ui-bootstrap.d.ts" />



module LeisureInsure {
    export function liDatePickerto(uibDateParser): ng.IDirective {
        function pad(number, size) {
            number = number.toString();
            while (number.length < size) number = `0${number}`;
            return number;
        }
        return {
            restrict: "A",
            replace: true,
            template: '<div><input type="text" required name="datepickerto" class="form-control" data-uib-datepicker-popup="dd/MM/yyyy" data-is-open="isOpen" data-ng-click="open()" ng-model="endDate" data-datepicker-options="datePickerOptions" readonly="readonly" style="background-color:white !important"></input></div>',
            link: (scope: any) => {
                scope.isOpen = false;
                scope.open = () => {
                    scope.isOpen = true;
                }
                                
                scope.processedDate = scope.endDate;
                var minimumDate = uibDateParser.parse(scope.minDate, "yyyy-MM-dd");
                var maximumDate = uibDateParser.parse(scope.maxDate, "yyyy-MM-dd");


                scope.datePickerOptions = {
                    "minDate": minimumDate,
                    "maxDate": maximumDate
                }

                scope.$watch("endDate", newendDate => {

                    if (newendDate != null) {
                       
                        var minDate = uibDateParser.parse(scope.minDate, "yyyy-MM-dd");
                        var maxDate = uibDateParser.parse(scope.maxDate, "yyyy-MM-dd");
                        if (scope.endDate > uibDateParser.parse(scope.maxDate, "yyyy-MM-dd")) {
                            scope.endDate = null;
                        }                        
                    }
                });
                
                scope.$watch("minExpDate", newminimumDate => {

                    
                    if (newminimumDate != null) {
                        //var minDate = uibDateParser.parse(scope.minDate, "yyyy-MM-dd");
                        //var maxDate = uibDateParser.parse(scope.maxDate, "yyyy-MM-dd");
                        //if (uibDateParser.parse(scope.processedDate, "yyyy-MM-dd") < uibDateParser.parse(scope.minDate, "yyyy-MM-dd") || uibDateParser.parse(scope.processedDate, "yyyy-MM-dd") > uibDateParser.parse(scope.maxDate, "yyyy-MM-dd")) {
                        //    scope.processedDate = null;
                        //}
                        scope.datePickerOptions = {
                            "minDate": scope.minExpDate,
                            "maxDate": scope.maxExpDate
                        }
                    }
                });
                
            },
            scope: {
                date: "=",
                minExpDate: "=",
                maxExpDate: "=",
                minDate: "=",
                maxDate: "=",
                watchdate: "=",
                endDate: "=",
            }
        }
    }

    angular.module("App").directive("liDatePickerto", ["uibDateParser", liDatePickerto]);
}