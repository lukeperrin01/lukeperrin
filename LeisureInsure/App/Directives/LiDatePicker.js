var LeisureInsure;
(function (LeisureInsure) {
    function liDatePicker(uibDateParser) {
        function pad(number, size) {
            number = number.toString();
            while (number.length < size)
                number = "0" + number;
            return number;
        }
        return {
            restrict: "A",
            replace: true,
            template: '<div><input type="text" required name="datepicker" class="form-control" data-uib-datepicker-popup="dd/MM/yyyy" data-is-open="isOpen" data-ng-click="open()" ng-model="startDate" data-datepicker-options="datePickerOptions" readonly="readonly" style="background-color:white !important"></input></div>',
            link: function (scope) {
                scope.isOpen = false;
                scope.open = function () {
                    scope.isOpen = true;
                };
                var minimumDate = uibDateParser.parse(scope.minDate, "yyyy-MM-dd");
                var maximumDate = uibDateParser.parse(scope.maxDate, "yyyy-MM-dd");
                scope.datePickerOptions = {
                    "minDate": minimumDate,
                    "maxDate": maximumDate
                };
                scope.$watch("minDate", function (newminimumDate) {
                    var minDate = uibDateParser.parse(scope.minDate, "yyyy-MM-dd");
                    var maxDate = uibDateParser.parse(scope.maxDate, "yyyy-MM-dd");
                    if (scope.startDate < uibDateParser.parse(scope.minDate, "yyyy-MM-dd") || scope.startDate > uibDateParser.parse(scope.maxDate, "yyyy-MM-dd")) {
                        scope.startDate = null;
                    }
                    scope.datePickerOptions = {
                        "minDate": minDate,
                        "maxDate": maxDate
                    };
                });
                scope.$watch("startDate", function (newDate) {
                    if (newDate != null && scope.numberOfDays != null) {
                        var dateAsString = scope.GetDateAsString(newDate);
                        var newdateTo = scope.AddDaysToDate(newDate, scope.numberOfDays);
                        var newdateToAsString = scope.GetDateAsString(newdateTo);
                        if (scope.numberOfDays != null) {
                            scope.minExpDate = dateAsString;
                            scope.maxExpDate = newdateToAsString;
                        }
                    }
                });
                scope.AddDaysToDate = function (date1, days) {
                    var ONE_DAY = 1000 * 60 * 60 * 24;
                    var date1_ms = date1.getTime();
                    var days_ms = ONE_DAY * days;
                    var new_ms = Math.abs(date1_ms + days_ms);
                    var newDate = new Date(new_ms);
                    return newDate;
                };
                scope.GetDateAsString = function (date) {
                    var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
                    if (month.length < 2)
                        month = '0' + month;
                    if (day.length < 2)
                        day = '0' + day;
                    return [year, month, day].join('-');
                };
            },
            scope: {
                date: "=",
                minDate: "=",
                maxDate: "=",
                startDate: "=",
                minExpDate: "=",
                maxExpDate: "=",
                numberOfDays: "="
            }
        };
    }
    LeisureInsure.liDatePicker = liDatePicker;
    angular.module("App").directive("liDatePicker", ["uibDateParser", liDatePicker]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=LiDatePicker.js.map