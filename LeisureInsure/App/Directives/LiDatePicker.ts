﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/angular-ui-bootstrap/angular-ui-bootstrap.d.ts" />

module LeisureInsure {
    export function liDatePicker(uibDateParser): ng.IDirective {
        function pad(number, size) {
            number = number.toString();
            while (number.length < size) number = `0${number}`;
            return number;
        }
        return {
            restrict: "A",
            replace: true,
            template: '<div><input type="text" required name="datepicker" class="form-control" data-uib-datepicker-popup="dd/MM/yyyy" data-is-open="isOpen" data-ng-click="open()" ng-model="startDate" data-datepicker-options="datePickerOptions" readonly="readonly" style="background-color:white !important"></input></div>',
            link: (scope: any) => {
                scope.isOpen = false;
                scope.open = () => {
                    scope.isOpen = true;
                }

                var minimumDate = uibDateParser.parse(scope.minDate, "yyyy-MM-dd");
                var maximumDate = uibDateParser.parse(scope.maxDate, "yyyy-MM-dd");
                scope.datePickerOptions = {
                    "minDate": minimumDate,
                    "maxDate": maximumDate
                }

                scope.$watch("minDate", newminimumDate => {

                    var minDate = uibDateParser.parse(scope.minDate, "yyyy-MM-dd");
                    var maxDate = uibDateParser.parse(scope.maxDate, "yyyy-MM-dd");
                    if (scope.startDate < uibDateParser.parse(scope.minDate, "yyyy-MM-dd") || scope.startDate > uibDateParser.parse(scope.maxDate, "yyyy-MM-dd")) {
                        scope.startDate = null;                       
                    }
                    scope.datePickerOptions = {
                        "minDate": minDate,
                        "maxDate": maxDate
                    }
                });

                scope.$watch("startDate", newDate => {

                    if (newDate != null && scope.numberOfDays != null) {
                       
                        var dateAsString = scope.GetDateAsString(newDate)
                        var newdateTo = scope.AddDaysToDate(newDate, scope.numberOfDays);
                        var newdateToAsString = scope.GetDateAsString(newdateTo);

                        if (scope.numberOfDays != null) {

                            //update min and max on the dateTo
                            //minDate is start date
                            scope.minExpDate = dateAsString;
                            scope.maxExpDate = newdateToAsString;
                        }
                    }
                });

                scope.AddDaysToDate = function(date1, days) {
                    // The number of milliseconds in one day
                    var ONE_DAY = 1000 * 60 * 60 * 24

                    // Convert date to milliseconds
                    var date1_ms = date1.getTime()

                    //get milliseconds from days
                    var days_ms = ONE_DAY * days;

                    //total milliseconds
                    var new_ms = Math.abs(date1_ms + days_ms)

                    // Convert milliseconds to date
                    var newDate = new Date(new_ms);

                    return newDate;
                }

                //we want YYYY-mm-dd for the options
                scope.GetDateAsString = function(date) {
                    var d = new Date(date),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;

                    return [year, month, day].join('-');
                }
            },
            scope: {
                date: "=",
                minDate: "=",                
                maxDate: "=",
                startDate: "=",                
                minExpDate: "=",
                maxExpDate: "=",
                numberOfDays: "="               
            }
        }//end return
    }

    angular.module("App").directive("liDatePicker", ["uibDateParser", liDatePicker]);
}

