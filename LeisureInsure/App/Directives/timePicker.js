var LeisureInsure;
(function (LeisureInsure) {
    function timePicker() {
        return {
            restrict: 'E',
            replace: true,
            template: '<div>' +
                '<div class="form-group">' +
                '<label class="control-label col-sm-2">Event Start Time</label> ' +
                '<div class="col-sm-10" style="padding-top:5px">' +
                '<select ng-model="startingHour">' +
                '<option ng-repeat="option in startingTimeHoursRange" ng-disabled="option.disabled" value="{{option.value}}">{{option.name}}</option>' +
                '</select>' +
                ':' +
                '<select ng-model="startingMinute">' +
                '<option ng-repeat="option in startingTimeHMinutesRange" ng-disabled="option.disabled" value="{{option.value}}">{{option.name}}</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group" ng-show="endTimeRequired" >' +
                '<label class="control-label col-sm-2" >Event End Time</label> ' +
                '<div class="col-sm-10" style="padding-top:5px">' +
                '<select ng-model="endingHour">' +
                '<option ng-repeat="option in endingTimeHoursRange" ng-disabled="option.disabled" value="{{option.value}}">{{option.name}}</option>' +
                '</select>' +
                ':' +
                '<select ng-model="endingMinute">' +
                '<option ng-repeat="option in endingTimeHMinutesRange" ng-disabled="option.disabled" value="{{option.value}}">{{option.name}}</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '</div>',
            scope: {
                timeSettings: '=',
                startingHour: '=',
                startingMinute: '=',
                endingHour: '=',
                endingMinute: '=',
                endTimeRequired: '=',
                startDate: '=',
                endDate: '=',
                applyCallback: '&',
                clearCallback: '&',
                timeEvent: '&'
            },
            link: function (scope) {
                var i;
                var timeHoursRange = [], timeMinutesRange = [];
                scope.startingTimeHoursRange = [];
                scope.endingTimeHoursRange = [];
                scope.startingTimeHMinutesRange = [];
                scope.endingTimeHMinutesRange = [];
                scope.timeDropDownToggleState = false;
                scope.disableOptions = true;
                for (i = 0; i < 24; i++) {
                    timeHoursRange.push({
                        name: (i < 10) ? ('0' + i) : i + '',
                        value: (i < 10) ? ('0' + i) : i + ''
                    });
                }
                for (i = 0; i < 60; i++) {
                    timeMinutesRange.push({
                        name: (i < 10) ? ('0' + i) : i + '',
                        value: (i < 10) ? ('0' + i) : i + ''
                    });
                }
                angular.copy(timeHoursRange, scope.startingTimeHoursRange);
                angular.copy(timeHoursRange, scope.endingTimeHoursRange);
                angular.copy(timeMinutesRange, scope.startingTimeHMinutesRange);
                angular.copy(timeMinutesRange, scope.endingTimeHMinutesRange);
                scope.TimeToTest = function () {
                    alert(scope.endTimeRequired);
                };
                scope.RefreshHoursMins = function () {
                    var startdate = new Date(scope.startDate);
                    var enddate = new Date(scope.endDate);
                    var enddateCompare = enddate.getFullYear() + enddate.getMonth() + enddate.getDate();
                    var startdateCompare = startdate.getFullYear() + startdate.getMonth() + startdate.getDate();
                    if (enddateCompare > startdateCompare) {
                        scope.disableOptions = false;
                        for (i = 0; i < timeMinutesRange.length; i++) {
                            scope.startingTimeHMinutesRange[i].disabled = false;
                            scope.endingTimeHMinutesRange[i].disabled = false;
                        }
                        for (i = 0; i < timeHoursRange.length; i++) {
                            scope.startingTimeHoursRange[i].disabled = false;
                            scope.endingTimeHoursRange[i].disabled = false;
                        }
                    }
                    else {
                        scope.disableOptions = true;
                        var starthour = parseInt(scope.startingHour);
                        var endhour = parseInt(scope.endingHour);
                        if (starthour > endhour) {
                            scope.startingHour = scope.endingHour;
                            scope.startingMinute = "00";
                            scope.endingHourWatch(scope.endingHour, scope.endingHour);
                        }
                    }
                };
                scope.$watch('startDate', function (newValue, oldValue) {
                    scope.RefreshHoursMins();
                });
                scope.$watch('endDate', function (newValue, oldValue) {
                    scope.RefreshHoursMins();
                });
                scope.updateTimeRangeFilter = function () {
                    scope.timeSettings.fromHour = scope.startingHour;
                    scope.timeSettings.toHour = scope.endingHour;
                    scope.timeSettings.fromMinute = scope.startingMinute;
                    scope.timeSettings.toMinute = scope.endingMinute;
                };
                scope.setInitialTimeRange = function () {
                    if (scope.startingHour == null) {
                        scope.startingHour = scope.startingTimeHoursRange[0].value;
                        scope.startingMinute = scope.endingTimeHMinutesRange[0].value;
                    }
                    if (scope.endingHour == null) {
                        scope.endingHour = scope.startingTimeHoursRange[23].value;
                        scope.endingMinute = scope.endingTimeHMinutesRange[59].value;
                    }
                };
                scope.setInitialTimeRange();
                scope.clearTimeRange = function () {
                    scope.isCustomTimeFilter = false;
                    scope.clearCallback({
                        data: {
                            isCustomTimeFilter: scope.isCustomTimeFilter
                        }
                    });
                    scope.closeTimeFilterDropdown();
                };
                scope.applyTimeRangeFilter = function () {
                    scope.isCustomTimeFilter = true;
                    scope.updateTimeRangeFilter();
                    scope.applyCallback({
                        data: {
                            isCustomTimeFilter: scope.isCustomTimeFilter
                        }
                    });
                    scope.closeTimeFilterDropdown();
                };
                scope.closeTimeFilterDropdown = function () {
                    scope.timeDropDownToggleState = false;
                    scope.startingHour = scope.timeSettings.fromHour;
                    scope.startingMinute = scope.timeSettings.fromMinute;
                    scope.endingHour = scope.timeSettings.toHour;
                    scope.endingMinute = scope.timeSettings.toMinute;
                };
                scope.updateHour = function () {
                    if (scope.startingHour !== scope.endingHour) {
                        for (i = 0; i < timeMinutesRange.length; i++) {
                            scope.startingTimeHMinutesRange[i].disabled = false;
                            scope.endingTimeHMinutesRange[i].disabled = false;
                        }
                    }
                    else if (scope.startingMinute > scope.endingMinute) {
                        scope.startingMinute = scope.endingMinute - 1;
                        if (scope.endingMinute === '00') {
                            scope.endingMinute = '01';
                        }
                        else {
                            scope.updateStartingMinuteTime();
                        }
                    }
                    else if (scope.startingHour === scope.endingHour) {
                        scope.updateStartingMinuteTime();
                        scope.updateEndingMinuteTime();
                    }
                };
                scope.updateStartingMinuteTime = function () {
                    for (var i = 0; i < timeMinutesRange.length; i++) {
                        if (i > (parseInt(scope.endingMinute, 10) - 1) && i < timeMinutesRange.length) {
                            scope.startingTimeHMinutesRange[i].disabled = true;
                        }
                        else {
                            scope.startingTimeHMinutesRange[i].disabled = false;
                        }
                    }
                };
                scope.updateEndingMinuteTime = function () {
                    for (var i = 0; i < timeMinutesRange.length; i++) {
                        if (i >= 0 && i < (parseInt(scope.startingMinute, 10) + 1)) {
                            scope.endingTimeHMinutesRange[i].disabled = true;
                        }
                        else {
                            scope.endingTimeHMinutesRange[i].disabled = false;
                        }
                    }
                };
                scope.$watch('startingHour', function (newValue, oldValue) {
                    if (!newValue || newValue === oldValue) {
                        return;
                    }
                    if (scope.disableOptions) {
                        var i;
                        for (i = 0; i < timeHoursRange.length; i++) {
                            if (i >= 0 && i < parseInt(scope.startingHour, 10)) {
                                scope.endingTimeHoursRange[i].disabled = true;
                            }
                            else {
                                scope.endingTimeHoursRange[i].disabled = false;
                            }
                        }
                        scope.updateHour(scope.startingHour, scope.endingTimeHoursRange);
                    }
                });
                scope.endingHourWatch = function (newValue, oldValue) {
                    if (scope.disableOptions) {
                        var i;
                        for (i = 0; i < timeHoursRange.length; i++) {
                            if (i > parseInt(scope.endingHour, 10) && i < timeHoursRange.length) {
                                scope.startingTimeHoursRange[i].disabled = true;
                            }
                            else {
                                scope.startingTimeHoursRange[i].disabled = false;
                            }
                        }
                        scope.updateHour(scope.endingHour, scope.startingTimeHoursRange);
                    }
                };
                scope.$watch('endingHour', function (newValue, oldValue) {
                    scope.endingHourWatch(newValue, oldValue);
                });
                scope.$watch('startingMinute', function (newValue, oldValue) {
                    if (!newValue || newValue === oldValue || scope.startingHour !== scope.endingHour) {
                        return;
                    }
                    if (scope.disableOptions) {
                        scope.updateEndingMinuteTime();
                    }
                });
                scope.$watch('endingMinute', function (newValue, oldValue) {
                    if (!newValue || newValue === oldValue || scope.startingHour !== scope.endingHour) {
                        return;
                    }
                    if (scope.disableOptions) {
                        scope.updateStartingMinuteTime();
                    }
                });
                scope.$watch('isCustomTimeFilter', function (newValue) {
                    if (!newValue) {
                        scope.setInitialTimeRange();
                    }
                });
            }
        };
    }
    LeisureInsure.timePicker = timePicker;
    angular.module("App").directive("timePicker", timePicker);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=timePicker.js.map