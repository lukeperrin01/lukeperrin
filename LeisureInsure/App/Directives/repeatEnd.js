var LeisureInsure;
(function (LeisureInsure) {
    function repeatEnd() {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                if (scope.$last) {
                    scope.$eval(attrs.repeatEnd);
                }
            }
        };
    }
    LeisureInsure.repeatEnd = repeatEnd;
    angular.module("App").directive("repeatEnd", repeatEnd);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=repeatEnd.js.map