﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/angular-ui-bootstrap/angular-ui-bootstrap.d.ts" />

module LeisureInsure {
    export function repeatEnd(): ng.IDirective {
        return {
        restrict: "A",
        link: function (scope: any, element, attrs) {
            if (scope.$last) {
                scope.$eval(attrs.repeatEnd);
            }
        }
    };      
    }
    angular.module("App").directive("repeatEnd", repeatEnd);
}


