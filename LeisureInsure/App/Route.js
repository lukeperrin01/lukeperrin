var LeisureInsure;
(function (LeisureInsure) {
    var Routes = (function () {
        function Routes() {
        }
        Routes.configureRoutes = function ($routeProvider, $locationProvider) {
            $locationProvider.html5Mode(true);
            $routeProvider.otherwise({ redirectTo: "/" });
            $routeProvider.when("/login", {
                controller: "StaticPageLoginController",
                templateUrl: "/app/staticpages/login.html",
                controllerAs: "loginController"
            });
            $routeProvider.when("/policies", {
                controller: "PoliciesController",
                templateUrl: "/app/policiespage/policies.html",
                controllerAs: "policiesController"
            });
            $routeProvider.when("/customerDetails", {
                controller: "CustomerDetailsPageController",
                templateUrl: "/app/customerDetailspage/customerDetails.html",
                controllerAs: "customerDetailscontroller"
            });
            $routeProvider.when("/claims", {
                templateUrl: "/app/staticpages/claims.html",
                metadata: {
                    title: "Leisure Insure - Claims",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/complaints", {
                templateUrl: "/app/staticpages/complaints.html",
                metadata: {
                    title: "Leisure Insure - Complaints",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/about-us", {
                templateUrl: "/app/staticpages/about-us.html",
                metadata: {
                    title: "Leisure Insure - About Us",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/contact-us/thank-you", {
                templateUrl: "/app/staticpages/contact-us-thank-you.html",
                metadata: {
                    title: "Leisure Insure - Contact Us",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/contact-us", {
                templateUrl: "/app/staticpages/contact-us.html",
                controller: "CallsToActionController",
                controllerAs: "contact",
                metadata: {
                    title: "Leisure Insure - Contact Us",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/call-me-back", {
                templateUrl: "/app/staticpages/call-me-back.html",
                controller: "CallsToActionController",
                controllerAs: "contact",
                metadata: {
                    title: "Leisure Insure - Call Me Back",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/faq", {
                templateUrl: "/app/staticpages/faq.html",
                metadata: {
                    title: "Leisure Insure - Frequently Asked Questions",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        Routes.$inject = ["$routeProvider", "$locationProvider"];
        return Routes;
    }());
    LeisureInsure.Routes = Routes;
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=Route.js.map