﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class ConfirmationController {
        static $inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "CommonDataService", "PriceService", "StripeService"];
        filter: any; // ng.IRootScopeService;
        window: ng.IWindowService;
        location: any;
        scope: any;
        rootScope: any;
        timeout: any;
        http: ng.IHttpService;
        q: ng.IQService;
        locationService: LocationService;
        locales: Array<ILocale>;
        chargeService: ChargeService;
        policyData: IPolicyView;
        commonDataService: CommonDataService;       
        descriptionSrc: string;
        //have we attempted to get a price? used for validation                
        navigationData: INavigationObject;
        dates: IDates;
        //stripeData: IStripe;        
        paymentCard: StripeTokenData;
        certHref: string;
        cardMessage: string;
        validCard: boolean;
        instalment: boolean;
        instalmentData: IInstalments;


        constructor($http, $q: ng.IQService, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies,
            locationService, chargeService, urlToNavigationService: UrlToNavigationService, metadataService: MetadataService,
            $timeout, commonDataService: CommonDataService) {
            var initialisationPromises = [];
            var navigationData: INavigationObject;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.commonDataService = commonDataService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;           
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;            

            locationService.onSelectedLocaleChanged($scope, (locale: ILocale) => {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            })                       

            //load existing quote *******************************************************
            if (commonDataService.policyData != null) {
                this.policyData = commonDataService.policyData;
                this.InitMinDetails();               
            }
            else {
                if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                    var quoteRef = $routeParams["QUOTEREFERENCE"];
                    var password = $routeParams["PASSCODE"];

                    urlToNavigationService.GetLandingPageNew(quoteRef, password)
                        .then((data: INavigationObject) => {
                            this.navigationData = data;

                            if (this.navigationData.policyId > 0) {
                                metadataService.setMetadata(this.navigationData.title,
                                    this.navigationData.description,
                                    this.navigationData.keywords);
                                if (this.locationService.selectedLocale != null) {

                                }

                                this.policyData = <IPolicyView>{};
                                this.InitMinDetails();
                            }
                        });

                    this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then((data: IPolicyView) => {
                            this.policyData = data;
                            this.InitMinDetails();                            

                            if (this.locationService.selectedLocale != null) {
                                this.locationService.selectedLocale.showFlag = false;
                            }

                        })
                }
            }//****************************************** End Constructor *************************************  
        }

        //the minimum required to show the page, picture, policyname, locale       
        InitMinDetails(): void {
            
            this.policyData.locale = this.locationService.selectedLocale;
            if (this.policyData.addresses == null)
                this.policyData.addresses = [];

            if (this.policyData.covers != null)
                this.policyData.covers = this.filter("orderBy")(this.policyData.covers, ["coverOrdinal"]);            

        }       

        

        Debug(): void {
            console.log("policyData", this.policyData);           
        }

       

        static routing($routeProvider) {
            $routeProvider.when("/confirmation",
                {
                    controller: "ConfirmationController",
                    templateUrl: "/app/confirmation/confirmation.html",
                    controllerAs: "ConfirmationController",
                    metadata: {
                        title: "Leisure Insure Personal Details",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/confirmation/:QUOTEREFERENCE/:PASSCODE",
                {
                    controller: "ConfirmationController",
                    templateUrl: "/app/confirmation/confirmation.html",
                    controllerAs: "ConfirmationController",
                    metadata: {
                        title: "Leisure Insure Personal Details",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });

        }

    }

    angular.module("App")
        .controller("ConfirmationController", ConfirmationController)
        .config(["$routeProvider", ConfirmationController.routing]);
}









