﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class NewPaymentController {
        static $inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "CommonDataService", "PriceService", "StripeService"];
        filter: any; // ng.IRootScopeService;
        window: ng.IWindowService;
        location: any;
        scope: any;
        rootScope: any;
        timeout: any;
        http: ng.IHttpService;
        q: ng.IQService;
        locationService: LocationService;
        locales: Array<ILocale>;
        chargeService: ChargeService;
        policyData: IPolicyView;
        commonDataService: CommonDataService;
        priceService: PriceService;
        stripeService: StripeService;
        descriptionSrc: string;
        //have we attempted to get a price? used for validation                
        navigationData: INavigationObject;
        dates: IDates;
        //stripeData: IStripe;        
        paymentCard: StripeTokenData;
        certHref: string;
        cardMessage: string;
        validCard: boolean;
        instalment: boolean;
        instalmentData: IInstalments;


        constructor($http, $q: ng.IQService, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies,
            locationService, chargeService, urlToNavigationService: UrlToNavigationService, metadataService: MetadataService,
            $timeout, commonDataService: CommonDataService, priceService: PriceService, stripeService: StripeService) {
            var initialisationPromises = [];
            var navigationData: INavigationObject;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.commonDataService = commonDataService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.priceService = priceService;
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            this.stripeService = stripeService;
            this.certHref = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("payment")) + 'newcertificate/' + $routeParams["QUOTEREFERENCE"] + '/' + $routeParams["PASSCODE"];
            $("#cardLabel").css("display", "none");            

            locationService.onSelectedLocaleChanged($scope, (locale: ILocale) => {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            })

            this.dates = {
                dateFrom: null,
                dateTo: null,
                minDate: "",
                maxDate: "",
                startingHour: "00",
                startingMinute: "00",
                endingHour: "23",
                endingMinute: "59",
                dateToRequired: false,
                startTimeRequired: false,
                endTimeRequired: false,
                minToMaxDays: null,
                coverDays: null,
                minExpDate: "",
                maxExpDate: "",
            };

            if (this.instalment == null)
                this.instalment = false;

            if (this.instalmentData == null)
                this.instalmentData = <IInstalments>{};

            //load existing quote *******************************************************
            if (commonDataService.policyData != null) {
                this.navigationData = commonDataService.policyData.landingPage;
                this.policyData = commonDataService.policyData;
                this.InitMinDetails();
                this.SetDates(365, this.policyData.policyType);
            }
            else {
                if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                    var quoteRef = $routeParams["QUOTEREFERENCE"];
                    var password = $routeParams["PASSCODE"];                  

                    this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then((data: IPolicyView) => {

                            this.navigationData = data.landingPage;

                            this.policyData = data;
                            this.InitMinDetails();
                            this.SetDates(365, this.policyData.policyType);

                            if (this.locationService.selectedLocale != null) {
                                this.locationService.selectedLocale.showFlag = false;
                            }

                        })
                }
            }//****************************************** End Constructor *************************************  
        }

        //the minimum required to show the page, picture, policyname, locale       
        InitMinDetails(): void {           

            if (this.policyData.addresses == null)
                this.policyData.addresses = [];

            if (this.policyData.covers != null)
                this.policyData.covers = this.filter("orderBy")(this.policyData.covers, ["coverOrdinal"]);

            $('.cc-number').payment('formatCardNumber');
            $('.cc-exp').payment('formatCardExpiry');
            $('.cc-cvc').payment('formatCardCVC');

        }

        SetDates(numdays: number, policyType: string) {

            var isEventSupplier = false;
            this.dates.minDate = new Date(this.policyData.minStartDate).toISOString().slice(0, 10);
            this.dates.maxDate = new Date(this.policyData.maxStartDate).toISOString().slice(0, 10);

            var min = new Date(this.dates.minDate);
            var max = new Date(this.dates.maxDate);
            var days = daysBetween(max, min);

            this.dates.minToMaxDays = days;
            var policyID = this.policyData.id;

            ////fieldsports - number of days determines our min/max range
            //if (policyID == 8) {
            //    this.dates.minToMaxDays = numdays;
            //}
            ////exhibitors - number of days determines our end date 7 or 1
            //if (policyID == 18) {
            //    this.dates.coverDays = numdays;
            //}
            //if (policyID == 25) { //stallholders
            //    var cancellation = _.filter(this.chargeSummary.chargeSummaryItems, (c: any) => {
            //        return c.coverFK == 30 && c.price != null && c.price > 0;
            //    });
            //    // if cancellation cover has been selected then min date = today + 14 days
            //    if (cancellation.length > 0) {
            //        var dt = new Date(min.setDate(min.getDate() + 14));
            //        min = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
            //        max = new Date(min.getFullYear() + 1, dt.getMonth(), dt.getDate())
            //        var m = "";
            //        if (min.getMonth() < 10) {
            //            m = '0' + (min.getMonth() + 1).toString();
            //        }
            //        else {
            //            m = (dt.getMonth() + 1).toString();
            //        }

            //        this.dates.minDate = min.getFullYear() + '-' + m + '-' + min.getDate();
            //        this.dates.maxDate = max.getFullYear() + '-' + m + '-' + min.getDate();
            //        days = daysBetween(max, min);
            //        this.dates.minToMaxDays = days;
            //    }

            //}
            ////events - number of days determines our min/max range
            //if (policyID == 6 && policyType == "E") {
            //    this.dates.minToMaxDays = numdays;

            //    if (this.chargeSummary.businessDescription.indexOf("Supplier") != -1) {
            //        this.dates.coverDays = numdays;
            //        isEventSupplier = true;
            //    }
            //}

            //set whether we are showing dateTo/times
            if (policyType == "E") {
                if (policyID == PolicyEnum.FieldSportsEvent || policyID == PolicyEnum.Events && !isEventSupplier)
                    this.policyData.dates.dateToRequired = true;
                if (policyID == PolicyEnum.DayCoverForInflatables || policyID == PolicyEnum.StreetParties || policyID == PolicyEnum.Events || policyID == PolicyEnum.Exhibitors)
                    this.policyData.dates.startTimeRequired = true;
                if (policyID == PolicyEnum.Events)
                    this.policyData.dates.endTimeRequired = true;
            }
        }

        TermsClicked(form: any) {
            var display = $("#termsTick").css("display");

            if (display == "none") {
                $("#termsTick").css("display", "block");
                form.termsAndCond.$setValidity('required', true);
                this.policyData.termsAccepted = true;
            }
            else {
                $("#termsTick").css("display", "none");
                form.termsAndCond.$setValidity('required', false);
                this.policyData.termsAccepted = false;
            }
        }

        ViewCert(form: any) {

            if (this.dates.dateFrom != null) {

                this.policyData.dates = this.dates;

                var win = window.open();

                // this.updateEndDate();
                this.chargeService.updateQuoteDatesNew(this.policyData)
                    .then((data) => {
                        win.location.href = this.certHref;
                    }).catch(function (data: any) { this.rootScope.loadingdocuments = false; });
            }
            else
                $("#cardInfo").text("Please select a start date before viewing a quote");
                

        }

        ShowCardImage(): void {

            $(".card-images img").removeClass("disabled");

            var cardType = $.payment.cardType($('.cc-number').val());
            if (!cardType) {
                return;
            }

            $(".cc-number").css("background", "url(/Content/Images/CreditCards/" + cardType + ".png) no-repeat");
            $(".cc-number").css("background-position", "98% 50%");

            var selector = ".card-images img[alt!='cardType']";
            selector = selector.replace("cardType", cardType);
            $(selector).addClass("disabled");
        }

        Debug(): void {
            console.log("policyData", this.policyData);
            console.log("dates", this.dates);
            console.log("certRef", this.certHref);
            //console.log("stripeData", this.stripeData);
            console.log("card", this.paymentCard);
            console.log("valid card", this.validCard);
            console.log("card message", this.cardMessage);
            console.log("instalmentData", this.instalmentData);
            console.log("instalment", this.instalment);
            console.log("navigationData", this.navigationData);

            _.each(this.policyData.covers, (cover: ICoverView) => {
                if (cover.net > 0)
                    console.log("covers", cover);
            });

        }

        PreviousPage(): void {

            this.location.path("/details/" + this.policyData.quoteReference + "/" + this.policyData.password);
        }

        AddLegal(): void {

            this.policyData.legalFeesAdded = true;
            this.priceService.GetPrice(this.policyData);
            this.commonDataService.SetPolicyData(this.policyData);

            //update instalment if we have added
            if (this.instalment)
                this.priceService.CalculateInstalments(this.instalmentData, this.policyData);

            //update quote on backend
            this.chargeService.SaveQuote(this.policyData)
                .then((response: IPolicyView) => {
                    this.policyData = response;
                    this.commonDataService.SetPolicyData(this.policyData);
                });
        }

        RemoveLegal(): void {

            this.policyData.legalFeesAdded = false;
            this.priceService.GetPrice(this.policyData);
            this.commonDataService.SetPolicyData(this.policyData);

            //update instalment if we have added
            if (this.instalment)
                this.priceService.CalculateInstalments(this.instalmentData, this.policyData);

            //update quote on backend
            this.chargeService.SaveQuote(this.policyData)
                .then((response: IPolicyView) => {
                    this.policyData = response;
                    this.commonDataService.SetPolicyData(this.policyData);
                });
        }

        PayByInstalment(): void {
            //recalculate total for instalment
            //this.priceService.GetPrice(this.P
            this.priceService.CalculateInstalments(this.instalmentData, this.policyData);
            //this.policyData.instalmentData = this.instalmentData;
            //this.commonDataService.SetPolicyData(this.policyData);
        }

        RemoveInstalment(): void {

            this.instalmentData = <IInstalments>{};
            ResetInstalmentData();
            //this.policyData.instalmentData = null;
            //this.commonDataService.SetPolicyData(this.policyData);
        }

        StripeResponse(status: number, response: StripeTokenResponse) {


            //we do not have the context of angular with a stripe callback 
            //so use jquery/javascript
            if (response.id) {
                $("#cardInfo").css("display", "none");
                this.validCard = true;
                this.cardMessage = "";

                PaymentData.stripeToken = response.id;

                //$("#purchaseBtn").attr("disabled", "disabled");

                $.ajax({
                    type: "POST",
                    url: "api/v1/stripepayment",
                    data: {
                        StripeToken: PaymentData.stripeToken,
                        QuoteRef: PaymentData.quoteRef,
                        QuotePass: PaymentData.quotePass,
                        CurrencyId: PaymentData.currencyId,
                        InstalmentData: InstalmentData,
                        PolicyDates: Dates
                    },
                    beforeSend: function () {
                        $("#loadingAnim").css("display", "block");
                    },
                    success: function (data, test) {
                        //proceed to confirmation page
                        window.location.href = confirmLink;

                    },
                    complete: function () {
                        $("#loadingAnim").css("display", "none");
                    },
                    error: function (error, test) {
                        $("#cardInfo").css("display", "block");
                        $("#cardInfo").html(error.responseJSON.message);
                    },
                    dataType: "json"
                });

            }
            else {
                $("#cardInfo").css("display", "block");
                $("#cardInfo").html(response.error.message);
            }
        }

        CompleteAsAgent(): void {
            //even though we are not using sripe here we are keeping our 
            //payment data for agents and customers consistent
            this.GetDates();

            this.chargeService.completeAsAgentNew().then((data: any) => {
                window.location.href = confirmLink;
            }).catch((data: any) => {
                $("#cardInfo").css("display", "block");
                $("#cardInfo").html(data);
            });
          
        }

        GetDates(): void {
            Dates.dateFrom = GetDateTime(this.dates.dateFrom);
            if (this.dates.dateTo != null)
                Dates.dateTo = GetDateTime(this.dates.dateTo);
            Dates.maxDate = this.dates.maxDate;
            Dates.minDate = this.dates.minDate;
            Dates.startingHour = this.dates.startingHour;
            Dates.startingMinute = this.dates.startingMinute;
            Dates.endingHour = this.dates.endingHour;
            Dates.endingMinute = this.dates.endingMinute;
            Dates.dateToRequired = this.dates.dateToRequired;
            Dates.startTimeRequired = this.dates.startTimeRequired;
            Dates.endTimeRequired = this.dates.endTimeRequired;
            Dates.minToMaxDays = this.dates.minToMaxDays;
            Dates.coverDays = this.dates.coverDays;
            Dates.minExpDate = this.dates.minExpDate;
            Dates.maxExpDate = this.dates.maxExpDate;
        }

        submitPaymentForm(form: any, valid: boolean): void {

            if (valid) {

                PaymentData.quoteRef = this.policyData.quoteReference;
                PaymentData.quotePass = this.policyData.password;
                PaymentData.currencyId = this.policyData.locale.currencySymbol;

                confirmLink = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("payment")) + 'confirmation/' + PaymentData.quoteRef + '/' + PaymentData.quotePass;

                if (this.policyData.brokerName != null) {
                    this.CompleteAsAgent();
                }
                else {

                    let card: StripeTokenData;
                    card = angular.copy(this.paymentCard, card);
                    let addy = this.policyData.addresses[0];
                    //update card data with customer address
                    card.address_country = addy.country;
                    card.address_line1 = addy.address1;
                    card.address_line2 = addy.address2;
                    card.address_zip = addy.postcode;
                    card.name = addy.contactName;
                    //send card data to stripe so we get back a token                   

                    //update instalment if we added it
                    if (this.instalmentData.totalToPay != null) {
                        InstalmentData.totalToPay = this.instalmentData.totalToPay;
                        InstalmentData.deposit = this.instalmentData.deposit;
                        InstalmentData.numberOfPayments = this.instalmentData.numberOfPayments;
                        InstalmentData.eachPayment = this.instalmentData.eachPayment;
                        InstalmentData.sortCode = this.instalmentData.sortCode;
                        InstalmentData.title = this.instalmentData.title;
                        InstalmentData.businessName = this.instalmentData.businessName;
                        InstalmentData.name = this.instalmentData.name;
                        InstalmentData.accountHolder = this.instalmentData.accountHolder;
                        InstalmentData.accountType = this.instalmentData.accountType;
                        InstalmentData.accountConfirm = this.instalmentData.accountConfirm;
                        InstalmentData.accountNumber = this.instalmentData.accountNumber;
                    }

                    this.GetDates();

                    Stripe.setPublishableKey(this.policyData.stripePublishableKey);
                    Stripe.card.createToken(card, this.StripeResponse);
                }

            }

        }

        static routing($routeProvider) {
            $routeProvider.when("/payment",
                {
                    controller: "NewPaymentController",
                    templateUrl: "/app/newpayment/newpayment.html",
                    controllerAs: "NewPaymentController",
                    metadata: {
                        title: "Leisure Insure Personal Details",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/payment/:QUOTEREFERENCE/:PASSCODE",
                {
                    controller: "NewPaymentController",
                    templateUrl: "/app/newpayment/newpayment.html",
                    controllerAs: "NewPaymentController",
                    metadata: {
                        title: "Leisure Insure Personal Details",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });

        }

    }

    angular.module("App")
        .controller("NewPaymentController", NewPaymentController)
        .config(["$routeProvider", NewPaymentController.routing]);
}
//stripe response does not recognise angular 
var PaymentData = { stripeToken: "", quoteRef: "", quotePass: "", currencyId: "" };

var Dates = {
    dateFrom: "",
    dateTo: "",
    maxDate: "",
    minDate: "",
    startingHour: "",
    startingMinute: "",
    endingHour: "",
    endingMinute: "",
    dateToRequired: false,
    startTimeRequired: false,
    endTimeRequired: false,
    minToMaxDays: 0,
    coverDays: 0,
    maxExpDate: "",
    minExpDate: ""
};

var InstalmentData = {
    totalToPay: 0, deposit: 0, numberOfPayments: 0, eachPayment: 0, accountNumber: 0, sortCode: "",
    accountType: "", title: "", businessName: "", name: "", accountHolder: false, accountConfirm: false
};

function ResetInstalmentData() {
    InstalmentData = {
        totalToPay: 0, deposit: 0, numberOfPayments: 0, eachPayment: 0, accountNumber: 0, sortCode: "",
        accountType: "", title: "", businessName: "", name: "", accountHolder: false, accountConfirm: false
    };
}

var confirmLink = "";


//return DateTime friendly string from javascript - frontend to backend
function GetDateTime(jsDate: Date): string {

    //make sure we have 2 digits 
    var day = ("0" + jsDate.getDate()).slice(-2);
    var month = ("0" + (jsDate.getMonth() + 1)).slice(-2);
    var year = jsDate.getFullYear();
    var hour = ("0" + jsDate.getHours()).slice(-2);
    var minute = ("0" + jsDate.getMinutes()).slice(-2);
    var second = ("0" + jsDate.getSeconds()).slice(-2);
    //make compatible with dateTime
    var backendDate = year + "-" + month + "-" + day + "T" + hour + ":" + minute;

    return backendDate;
}

//convert a "15:00" to js date - backend to frontend 
function GetJSDateFromTime(fulltime: string): Date {

    //make sure we have 2 digits 
    var time = fulltime.split(":");
    var hour = parseInt(time[0]);
    var mins = parseInt(time[1]);

    var date =  new Date(0, 0, 0, hour, mins, 0, 0)
    return date;
}

function daysBetween(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime()
    var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms)

    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY)

}

//note this is in datepicker directive.. 
function addhoursToDate(date1, days) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert date to milliseconds
    var date1_ms = date1.getTime()

    //get milliseconds from days
    var days_ms = ONE_DAY * days;

    //total milliseconds
    var new_ms = Math.abs(date1_ms + days_ms)

    // Convert milliseconds to date
    var newDate = new Date(new_ms);

    return newDate;

}








