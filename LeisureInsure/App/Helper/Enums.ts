﻿module LeisureInsure {
    export enum PolicyEnum {
        EquipmentHirer = 1,
        FieldSportsEvent = 5,
        Events = 5,
        DayCoverForInflatables = 5,
        StreetParties = 5,
        Exhibitors = 5
    }

    
}
