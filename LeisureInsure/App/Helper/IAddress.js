var LeisureInsure;
(function (LeisureInsure) {
    (function (PolicyEnum) {
        PolicyEnum[PolicyEnum["EquipmentHirer"] = 1] = "EquipmentHirer";
        PolicyEnum[PolicyEnum["FieldSportsEvent"] = 5] = "FieldSportsEvent";
        PolicyEnum[PolicyEnum["Events"] = 5] = "Events";
        PolicyEnum[PolicyEnum["DayCoverForInflatables"] = 5] = "DayCoverForInflatables";
        PolicyEnum[PolicyEnum["StreetParties"] = 5] = "StreetParties";
        PolicyEnum[PolicyEnum["Exhibitors"] = 5] = "Exhibitors";
    })(LeisureInsure.PolicyEnum || (LeisureInsure.PolicyEnum = {}));
    var PolicyEnum = LeisureInsure.PolicyEnum;
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=IAddress.js.map