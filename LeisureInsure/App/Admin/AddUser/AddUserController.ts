﻿namespace LeisureInsure.Admin {
    export class AddUserController {

        static $inject = ["$http", "$scope", "$window"];
        constructor($http, $scope, $window) {
            this.http = $http;
            this.scope = $scope;
            this.window = $window;
        }
        userName: string;
        password: string;
        email; string;
        http: ng.IHttpService;
        scope: any;
        window: ng.IWindowService;


        addCsr() {
            alert("hit");
            this.http.post(
                "api/v1/account/create",
                {
                    Username: this.userName,
                    email: this.email,
                    password: this.password,
                }).then(() => {
                    this.window.scrollTo();
                    this.scope.alerts.push({
                        type: "success",
                        msg: "Broker successfully added"
                    });
                }, response => {
                    this.scope.alerts.push({
                        type: "danger",
                        msg: `There was an issue adding the broker: ${response.statusText}`
                    });
                });

        }


        static routing($routeProvider) {
            $routeProvider.when("/admin/addUser",
                {
                    controller: "AddUserController",
                    templateUrl: "/app/admin/addUser/addUser.html",
                    controllerAs: "csr",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
        }
    }
    angular.module("LeisureInsure.Admin")
        .controller("AddUserController", AddUserController)
        .config(["$routeProvider", AddUserController.routing]);
}