var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var AddUserController = (function () {
            function AddUserController($http, $scope, $window) {
                this.http = $http;
                this.scope = $scope;
                this.window = $window;
            }
            AddUserController.prototype.addCsr = function () {
                var _this = this;
                alert("hit");
                this.http.post("api/v1/account/create", {
                    Username: this.userName,
                    email: this.email,
                    password: this.password,
                }).then(function () {
                    _this.window.scrollTo();
                    _this.scope.alerts.push({
                        type: "success",
                        msg: "Broker successfully added"
                    });
                }, function (response) {
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: "There was an issue adding the broker: " + response.statusText
                    });
                });
            };
            AddUserController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/addUser", {
                    controller: "AddUserController",
                    templateUrl: "/app/admin/addUser/addUser.html",
                    controllerAs: "csr",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            };
            AddUserController.$inject = ["$http", "$scope", "$window"];
            return AddUserController;
        }());
        Admin.AddUserController = AddUserController;
        angular.module("LeisureInsure.Admin")
            .controller("AddUserController", AddUserController)
            .config(["$routeProvider", AddUserController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=AddUserController.js.map