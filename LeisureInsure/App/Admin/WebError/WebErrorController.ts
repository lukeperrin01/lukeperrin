﻿namespace LeisureInsure.Admin {
    export class WebErrorController {
        static $inject = ["$http", "$q", "$scope", "$rootScope", "$window", "QuoteStorageService"];
        q: ng.IQService;
        constructor($http, $q, $scope, $rootScope, $window, QuoteStorageService) {
            this.http = $http;
            this.q = $q;
            this.scope = $scope;
            this.scope.alerts = [];
            this.window = $window;
            this.rootScope = $rootScope;
            this.quoteStorageService = QuoteStorageService;
            this.getWebErrors()
        }
        http: ng.IHttpService;
        scope: any;
        rootScope: any;
        window: ng.IWindowService;
        quoteStorageService: QuoteStorageService;
        webErrors: Array<IWebErrors>;


        getWebErrors() {

            this.http({
                url: "/api/v1/admin/getErrors",
                method: "GET"
            })
                .success((data: Array<any>) => {
                    this.webErrors = data;
                });
        }


        static routing($routeProvider) {
            $routeProvider.when("/admin/weberror",
                {
                    controller: "WebErrorController",
                    templateUrl: "/app/admin/weberror/errorlog.html",
                    controllerAs: "weberrorController",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
        }

    }
    angular.module("LeisureInsure.Admin")
        .controller("WebErrorController", WebErrorController)
        .config(["$routeProvider", WebErrorController.routing]);
}