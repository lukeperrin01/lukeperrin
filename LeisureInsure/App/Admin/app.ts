﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />

"use strict";

module LeisureInsure.Admin {
    angular.module("LeisureInsure.Admin", ["ui.tinymce"]);
}