﻿namespace LeisureInsure.Admin {
    export class UnderwriterQuotesController {
        static $inject = ["$http", "$scope", "$routeParams", "QuoteStorageService", "$rootScope", "$filter",];
        http: ng.IHttpService;
        scope: any;
        rootScope: any;
        certlink: string;
        quotelink: string;
        checkoutlink: string;
        filter: any; // ng.IRootScopeService;
        constructor($http, $scope: ng.IScope, $routeParams, quoteStorageService, $rootScope, policyUpdateService, $filter) {
            this.http = $http;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.scope.alerts = [];
            this.quoteStorageService = quoteStorageService;
            this.filter = $filter;
            $("#allGifs").css("display", "block");
            this.coverVis = 0;
            this.showCovers = false;
            this.showNotes = true;
            this.showEndorsements = true;
            $scope.$watch("quotes.override",
                () => {
                    if (this.selectedQuote !== undefined) {
                        if (this.override) {
                            this.selectedQuote.referralType = 2;
                        } else {
                            this.selectedQuote.referralType = 1;
                        }
                    }
                });
            this.getpolicies();
            this.getBrokers();
            this.btnLabel = 'Hide';
            this.answerFilter = "All";
            this.territoryCovers = [];
            this.territoryCovers.push("");
            this.territoryCovers.push("Europe");
            this.territoryCovers.push("Republic of Ireland");
            this.territoryCovers.push("United Kingdom");
            this.territoryCovers.push("Worldwide");
            this.policyId = 0;
            this.renewalType = 0;

            if ($routeParams["QUOTEREFERENCE"] !== undefined) {
                if (this.quoteStorageService.quotesLoaded) {
                    
                    this.selectedQuote = _.findWhere(this.quoteStorageService.quotes, {
                        quoteReference: $routeParams["QUOTEREFERENCE"]
                    });
                } else {
                    this.quoteStorageService
                        .searchQuotes({ quoteReference: $routeParams["QUOTEREFERENCE"] })
                        .then(quotes => {
                            this.quotes = angular.copy(quotes);
                            this.selectedQuote = _.findWhere(this.quoteStorageService.quotes,
                                { quoteReference: $routeParams["QUOTEREFERENCE"] });

                            var link = "";
                            if (window.location.href.indexOf("localhost") > -1) {
                                var certlinkbase = "http://" + window.location.hostname + ":" + window.location.port;
                                var quotelinkbase = "http://" + window.location.hostname + ":" + window.location.port;
                                var checkoutlinkbase = "http://" + window.location.hostname + ":" + window.location.port;
                            }
                            else {
                                var certlinkbase = "http://" + window.location.hostname;
                                var quotelinkbase = "https://" + window.location.hostname;
                                var checkoutlinkbase = "https://" + window.location.hostname;
                            }

                            this.certlink = certlinkbase + "/certificate/" + this.selectedQuote.quoteReference + "/" + this.selectedQuote.password;
                            this.quotelink = quotelinkbase + "/quote/" + this.selectedQuote.quoteReference + "/" + this.selectedQuote.password;
                            this.checkoutlink = checkoutlinkbase + "/checkout/" + this.selectedQuote.quoteReference + "/" + this.selectedQuote.password;
                            this.getCovers();
                            this.taxRate = this.selectedQuote.taxRate * 100;
                            if (this.selectedQuote.policyId != -1) {

                                this.policySelected = _.findWhere(this.policies, {
                                    id: this.selectedQuote.policyId
                                });
                            }
                            else {
                                alert("Please select policy type from the dropdown box");
                            }
                            if (this.selectedQuote.brokerName != null) {
                                this.brokerSelected = _.findWhere(this.brokers, {
                                    tamClientRef: this.selectedQuote.brokerName
                                });
                            }

                            var renType = _.filter(this.selectedQuote.answers, (a: IAnswer) => {
                                return a.rateTypeFK == 35
                            });

                            if (renType.length > 0) {
                                this.renewalType = 1;
                            }
                        });
                }
            }

        }
        showEndorsements: boolean;
        showNotes: boolean;
        quoteReference: string;
        lastName: string;
        tamReference: string;
        postcode: string;
        brokerCode: string;
        email: string;
        quotes: Array<IQuote>;
        newQuoteNote: IQuoteNote;
        quoteStorageService: QuoteStorageService;
        policies: Array<IPolicy>;
        covers: Array<any>;
        addCoverList: Array<any>;
        newCover: any;
        sectionCovers: Array<any>;
        coverSections: Array<ICoverSections>;
        coverVis: number;
        groupedSections: Array<any>;
        selectedQuote: IQuote;
        override: boolean;
        additionalDocument: number;
        endorsement: IEndorsement;
        coverDetailVis: number;
        coverDetailName: string;
        quotePK: number;
        btnLabel: string;
        policyId: number;
        policySelected: IPolicy;
        answerFilter: string;
        dateFromString: string;
        dateFromDt: Date;
        dateToDt: Date;
        showCovers: boolean;
        dateRequiresUpdating: boolean;
        brokers: Array<IBroker>;
        brokerSelected: IBroker;
        taxRate: number;
        territoryCovers: Array<string>;
        renewalType: number;

        getpolicies() {
            this.http({
                url: "/api/v1/data/getPolicies",
                method: "GET",
                params: { strLocale: 0 }
            })
                .success((data: Array<IPolicy>) => {
                    this.policies = data;

                })
                .catch((data: any) => {
                    // Error
                });
        }
        addToTam() {
            this.quoteStorageService.addToTam(this.selectedQuote.id);


        }

        newNote() {
            if (this.selectedQuote.notes == null) {
                this.selectedQuote.notes = [];
            }
            this.selectedQuote.notes.push(this.newQuoteNote);
        }
        deleteNote() {

            this.selectedQuote.notes.pop();

            //console.log('this.selectedQuote.notes before', this.selectedQuote.notes);
            ////this.selectedQuote.notes.slice(note);
            //console.log('this.selectedQuote.notes after', this.selectedQuote.notes);

        }
        addNote(note) {
            this.scope.updatebutton = true;
            this.quoteStorageService.addNote(note, this.selectedQuote.id)
                .then(response => {
                    this.selectedQuote = response;
                    this.scope.updatebutton = false;
                });


        }

        updatePolicyPK(policy) {
            if (policy != null) {
                this.selectedQuote.policyId = policy.id;
                this.policyId = policy.id;
            }
            else {
                this.selectedQuote.policyId = 0;
            }
            this.updateQuote();
        }

        getBrokers() {
            this.http({
                url: "api/v1/admin/getBrokers",
                method: "GET",
            })
                .success((data: Array<IBroker>) => {
                    this.brokers = data;
                })
                .catch((data: any) => {
                    // Error
                });
        }

        updateBrokerFK(brokerSelected) {
            if (brokerSelected != null) {
                this.selectedQuote.brokerName = brokerSelected.tamClientRef;
                this.selectedQuote.isAgent = true;
            }
            else {
                this.selectedQuote.brokerName = 'Remove';
                this.selectedQuote.isAgent = false;
            }
        }

        getCovers() {
            // Put this in the quoteStorageService
            this.http({
                url: "/api/v1/data/getCovers",
                method: "GET",
                params: { strLocale: 0 }
            })
                .success((data: Array<any>) => {
                    //build groupedSections so we can display coverSections and covers 
                    this.covers = data;
                    this.covers = _.sortBy(this.covers, 'coverOrdinal');
                    this.covers = _.filter(this.covers, (c: any) => {
                        return c.coverPK != 1;
                    });

                    this.addCoverList = angular.copy(this.covers);

                    _.each(this.addCoverList, (cover: any) => {
                        if (cover.sectionName == null || cover.sectionName == '') {
                            cover.sectionName = cover.coverName;
                        }
                        else {
                            cover.sectionName = cover.coverName + '-' + cover.sectionName;
                        }
                    });

                    if (this.selectedQuote != null) {
                        this.dateToDt = new Date(this.selectedQuote.dateTo);
                        this.dateFromDt = new Date(this.selectedQuote.dateFrom);
                        this.dateUpdate();


                        // embellish quote answers with cover info to all filtering by sectionNo later
                        _.each(this.selectedQuote.answers, (answer: IAnswer) => {
                            var c = _.findWhere(this.covers, {
                                coverPK: answer.coverPk
                            });
                            if (c != null) {
                                answer.cover = c;
                                answer.coverSection = c.section;
                                answer.coverName = c.coverName;
                            }
                        });
                        // embellish quote chargeSummaryItems with cover info to all filtering by sectionNo later
                        _.each(this.selectedQuote.chargeSummaryItems, (chargeItem: IChargeSummaryItem) => {
                            var c1 = _.findWhere(this.covers, {
                                coverPK: chargeItem.coverFK
                            });
                            if (c1 != null) {
                                chargeItem.coverSection = c1.section;
                                chargeItem.ordinal = c1.coverOrdinal;
                            }
                            this.addCoverList = _.without(this.addCoverList, _.findWhere(this.addCoverList, {
                                coverPK: c1.coverPK
                            }));



                        });

                        this.selectedQuote.chargeSummaryItems = _.sortBy(this.selectedQuote.chargeSummaryItems, 'ordinal');
                    }
                })
                .catch((data: any) => {
                    // Error
                });

        }



        btnDetailLabel(section) {
            // Show/hide cover answers
            if (this.coverVis == section.coverFK) {
                return 'Hide';
            }
            else {
                return 'View';
            }
        }

        dateUpdate() {
            if (this.dateToDt.getTime() < new Date().getTime()) {
                this.dateRequiresUpdating = true;
            }
            else {
                this.dateRequiresUpdating = false;
            }
        }

        // region Answers = quoteInputs table
        showAnwers(section) {
            var renType = _.filter(this.selectedQuote.answers, (a: IAnswer) => {
                return a.rateTypeFK == 35
            });
            if (renType.length > 0) {
                this.renewalType = 1;
            }
            else {
                this.renewalType = 0;
            }

            //if (this.renewalType == 1) {
            // show hide selectedQuote.answers
            var sectioncoversList = _.filter(this.covers, (c: any) => {
                return c.coverPK == section.coverFK
            });
            this.sectionCovers = sectioncoversList;
            if (this.coverVis == section.coverFK) {
                this.coverVis = 0;
                this.coverDetailName = "";
                _.each(this.selectedQuote.answers, (answer: IAnswer) => {
                    answer.rateVis = 0;
                });
            }
            else {
                this.coverVis = section.coverFK;
                this.coverDetailName = section.lineDescription;
                _.each(this.selectedQuote.answers, (answer: IAnswer) => {
                    if (answer.coverPk == section.coverFK) {
                        answer.rateVis = 1;
                    }
                    else {
                        answer.rateVis = 0;
                    }
                });
            }
            //}
            //else {
            //    alert("Edit quote details by clicking on the quoteLink at the bottom of this page");
            //}
        }

        saveAnswer(answer) {
            answer.id = -1;
            if (answer.rateTypeFK == 35) {
                answer.listOrdinal = answer.listNo;
            }
            this.calcSumInsured(answer);
        }

        addBlankAnswer() {
            var answerList = _.filter(this.selectedQuote.answers, (a: IAnswer) => {
                return a.coverPk == this.coverVis && a.listNo < 0;
            });

            var c = _.findWhere(this.covers, {
                coverPK: this.coverVis
            });

            var listNo = -1;
            if (answerList.length > 0) {
                listNo = (answerList.length + 1) * -1;
            }
            var x = {
                coverPk: this.coverVis,
                id: 0,
                rateTypeFK: 35,
                refer: 0,
                coverSection: parseInt(c.sectionNumber),
                inputPk: -1,
                inputString: null,
                inputName: '',
                listNo: listNo,
                listOrdinal: listNo,
                ratePk: -1,
                rateValue: null,
                rateVis: 1,
                coverName: c.coverName.toString(),
                sumInsured: 0,
                cover: c,
                territoryCover: null,
                certListItem: 0
            };

            this.selectedQuote.answers.push(x);
        }

        deleteAnswer(answer) {
            this.selectedQuote.answers = _.filter(this.selectedQuote.answers, (a: IAnswer) => {
                return a.id != answer.id;
            });
            var reNum = _.filter(this.selectedQuote.answers, (a: IAnswer) => {
                return a.coverPk == answer.coverPk;
            });
            var num = 1;
            _.each(reNum, (a: IAnswer) => {
                a.listNo = num * -1;
                a.listOrdinal = num * -1;
                num += 1;
            });
            this.calcSumInsured(answer);
        }

        calcSumInsured(answer) {
            var c = _.findWhere(this.covers, {
                coverPK: this.coverVis
            });
            var coverPK = c.coverPK;
            var section = c.section;
            var si = 0;
            //calc suminsured where covers can have a sum insured
            if (section == 2 || section == 3 || section == 4 || section == 5 || section == 11 || section == 12 || section == 13 || section == 14) {
                if (coverPK == 2) {
                    var busEquip = _.filter(this.selectedQuote.answers, (ans: IAnswer) => {
                        return ans.coverPk == coverPK && ans.rateTypeFK == 35 && ans.listNo < 0;
                    });
                    if (busEquip.length > 0) {
                        var busLine = _.findWhere(this.selectedQuote.chargeSummaryItems, {
                            coverFK: 8
                        });
                        if (!angular.isObject(busLine)) {
                            this.coverVis = 8;
                            this.newCover = _.findWhere(this.addCoverList, {
                                coverPK: this.coverVis
                            });
                            this.addQuoteLine(this.newCover);
                            this.coverVis = 2;
                        }
                        var busAns = _.filter(this.selectedQuote.answers, (ans: IAnswer) => {
                            return ans.coverPk == 8 || (ans.coverPk == coverPK && ans.rateTypeFK == 35 && ans.listNo < 0);
                        });
                        si = 0;
                        _.each(busAns, (a: IAnswer) => {
                            si += a.sumInsured;
                        });
                        var busLine = _.findWhere(this.selectedQuote.chargeSummaryItems, {
                            coverFK: 8
                        });
                        busLine.sumInsured = si;

                        this.coverVis = coverPK;
                    }
                    var plAns = _.filter(this.selectedQuote.answers, (ans: IAnswer) => {
                        return ans.coverPk == 2 && ans.listNo == 0;
                    });
                    si = 0;
                    _.each(plAns, (a: IAnswer) => {
                        si += a.sumInsured;
                    });
                    var plLine = _.findWhere(this.selectedQuote.chargeSummaryItems, {
                        coverFK: 2
                    });
                    plLine.sumInsured = si;
                }

                if (section == 3 || section == 4 || section == 5 || section == 11 || section == 12 || section == 13 || section == 14) {
                    var ax = _.filter(this.selectedQuote.answers, (ans: IAnswer) => {
                        return ans.coverPk == answer.coverPk;
                    });
                    si = 0;
                    _.each(ax, (a: IAnswer) => {
                        si += a.sumInsured;
                    });

                    var chargeSI = _.findWhere(this.selectedQuote.chargeSummaryItems, {
                        coverFK: this.coverVis
                    });

                    chargeSI.sumInsured = si;
                }
            }

        }

        answerFormat(answer) {

            var format;
            if (answer.refer == 1 && answer.certListItem == 1) {
                format = {
                    "background-color": "pink",
                    "color": "blue"
                }
            }
            else if (answer.refer == 1) {
                format = {
                    "background-color": "pink",
                }
            }
            else if (answer.certListItem == 1) {
                format = {
                    "color": "blue"
                }
            }
            return format;
        }

        filterAnswers() {
            var filtered = _.filter(this.selectedQuote.answers, (ans: IAnswer) => {
                return ans.coverPk == this.coverVis;
            });
            if (this.answerFilter == "All") {
                _.each(filtered, (answer: IAnswer) => {
                    answer.rateVis = 1;
                });
            }
            else if (this.answerFilter == "CertListItems")
                _.each(filtered, (answer: IAnswer) => {
                    if (answer.certListItem == 1) {
                        answer.rateVis = 1;
                    }
                    else {
                        answer.rateVis = 0;
                    }
                });
            else if (this.answerFilter == "Reffered")
                _.each(filtered, (answer: IAnswer) => {
                    if (answer.refer == 1) {
                        answer.rateVis = 1;
                    }
                    else {
                        answer.rateVis = 0;
                    }
                });
        }

        // endregion Answers

        // region quotelines

        addQuoteLine(cover) {
            if (cover != null) {
                this.coverVis = cover.coverPK;
                this.coverDetailName = cover.sectionName;
                var csi =
                    {
                        coverFK: cover.coverPK,
                        coverSection: cover.section,
                        invLevel: 1,
                        lineType: '',
                        charge: 0,
                        net: 0,
                        rateValue: '',
                        section: cover.section,
                        rate: -1,
                        ordinal: cover.coverOrdinal,
                        sectionTypeDescription: cover.sectionName,
                        sectiondescription: cover.coverName,
                        sumInsured: 0,
                        quoteId: this.selectedQuote.id,
                        indemnity: 0,
                        id: -1,
                        lineDescription: cover.sectionName
                    }
                this.selectedQuote.chargeSummaryItems.push(csi);

                var ax = _.filter(this.selectedQuote.answers, (ans: IAnswer) => {
                    return ans.coverPk == cover.coverPK;
                });
                if (ax.length == 0) {
                    this.addBlankAnswer()
                }
                this.addCoverList = _.filter(this.addCoverList, (c: any) => {
                    return c.coverPK != cover.coverPK;
                });

                this.selectedQuote.chargeSummaryItems = _.sortBy(this.selectedQuote.chargeSummaryItems, 'ordinal');
            }

        }

        deleteQuoteLine(section) {
            this.selectedQuote.chargeSummaryItems = _.filter(this.selectedQuote.chargeSummaryItems, (a: IChargeSummaryItem) => {
                return a.coverFK !== section.coverFK;
            });

            this.selectedQuote.answers = _.filter(this.selectedQuote.answers, (a: IAnswer) => {
                return a.coverPk !== section.coverFK;
            });

            var c = _.findWhere(this.addCoverList, (c: any) => {
                return c.coverPK == section.coverFK;
            });

            if (c.sectionName == null || c.sectionName == '') {
                c.sectionName = c.coverName;
            }
            else {
                c.sectionName = c.coverName + '-' + c.sectionName;
            }
            this.addCoverList.push(c);

            this.addCoverList = _.sortBy(this.addCoverList, 'coverOrdinal');
        }
        // endregion quotelines


        getOldCertificate() {
            var url = window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("admin")) + 'OldCerts/' + this.quotePK;
            window.open(url);
        }

        ShowQuote() {
            window.open(this.quotelink);
        }

        ShowCert() {
            window.open(this.certlink);
        }

        showCheckout() {
            window.open(this.checkoutlink);
        }

        recalc() {
            this.selectedQuote.tax = (this.selectedQuote.netPremium / 100) * this.taxRate;
            this.selectedQuote.legalFees = (this.selectedQuote.netPremium / 100) * 6;
            this.selectedQuote.legalFees += (this.selectedQuote.legalFees / 100) * this.taxRate;
            this.selectedQuote.commission = (this.selectedQuote.netPremium / 100) * this.selectedQuote.commissionPercentage;

            if (this.selectedQuote.legalFeesAdded == 1) {

                this.selectedQuote.total = this.selectedQuote.netPremium + this.selectedQuote.leisureInsureFees + this.selectedQuote.tax + this.selectedQuote.legalFees;
            }
            else {

                this.selectedQuote.total = this.selectedQuote.netPremium + this.selectedQuote.leisureInsureFees + this.selectedQuote.tax;
            }
            this.selectedQuote.total = parseFloat(parseFloat(this.selectedQuote.total.toString()).toFixed(2));
            this.selectedQuote.leisureInsureFees = parseFloat(parseFloat(this.selectedQuote.leisureInsureFees.toString()).toFixed(2));
            this.selectedQuote.netPremium = parseFloat(parseFloat(this.selectedQuote.netPremium.toString()).toFixed(2));
            this.selectedQuote.tax = parseFloat(parseFloat(this.selectedQuote.tax.toString()).toFixed(2));
            this.selectedQuote.legalFees = parseFloat(parseFloat(this.selectedQuote.legalFees.toString()).toFixed(2));
            this.selectedQuote.commission = parseFloat(parseFloat(this.selectedQuote.commission.toString()).toFixed(2));
        }

        addLegal(quote, addRemove) {
            if (addRemove == 1) {
                quote.legalFeesAdded = 1;
                quote.total = quote.netPremium + quote.leisureInsureFees + quote.tax + quote.legalFees;
                quote.total = parseFloat((quote.total).toFixed(2));

            }
            else {
                quote.legalFeesAdded = 0;
                quote.total = quote.netPremium + quote.leisureInsureFees + quote.tax;
                quote.total = parseFloat((quote.total).toFixed(2));
            }
        }

        editEndorsement(endorsement) {

            this.quoteStorageService.updateEndorsement(this.selectedQuote.id, this.endorsement.text, this.endorsement.id)
                .then(response => {
                    this.selectedQuote = response;
                    this.scope.updatebutton = false;
                });


        }

        addEndorsement() {
            this.scope.updatebutton = true;
            this.quoteStorageService.addEndorsement(this.endorsement.text, this.selectedQuote.id)
                .then(response => {
                    this.selectedQuote = response;
                    this.endorsement.text = "";
                    this.scope.updatebutton = false;
                });
        }
        removeEndorsement(endorsementId: number) {
            this.scope.updatebutton = true;
            this.quoteStorageService.removeEndorsement(this.endorsement.id, this.selectedQuote.id)
                .then(response => {
                    this.selectedQuote = response;
                    this.endorsement = null;
                    this.scope.updatebutton = false;
                });
        }
        addNewEndorsement() {
            this.endorsement = null;
        }

        addDocument() {
            this.scope.updatebutton = true;
            this.quoteStorageService.addDocument(this.additionalDocument, this.selectedQuote.id)
                .then(response => {
                    this.selectedQuote = response;
                    this.scope.updatebutton = false;
                });
        }
        removeDocument(documentId: number) {
            this.scope.updatebutton = true;
            this.quoteStorageService.removeDocument(documentId, this.selectedQuote.id)
                .then(response => {
                    this.selectedQuote = response;
                    this.scope.updatebutton = false;
                });
        }
        getPolicyDocuments() {
            if (this.policySelected != null) {
                this.policyId = this.policySelected.id;
            }

            if (this.policyId != 0) {
                this.scope.updatebutton = true;
                this.quoteStorageService.getPolicyDocuments(this.selectedQuote.id)
                    .then(response => {
                        this.selectedQuote = response;
                        this.scope.updatebutton = false;
                    });
            }
            else {
                alert("Select Policy");
            }
        }


        updateQuote() {
            this.scope.updatebutton = true;
            this.selectedQuote.taxRate = this.taxRate / 100;
            this.selectedQuote.dateFrom = this.dateFromDt.toISOString();
            this.selectedQuote.dateTo = this.dateToDt.toISOString();
            this.quoteStorageService.updateQuote(this.selectedQuote)
                .then(response => {
                    this.selectedQuote = response;
                    this.scope.updatebutton = false;
                    // embellish quote chargeSummaryItems with cover info to all filtering by sectionNo later
                    _.each(this.selectedQuote.chargeSummaryItems, (chargeItem: IChargeSummaryItem) => {
                        var c1 = _.findWhere(this.covers, {
                            coverPK: chargeItem.coverFK
                        });
                        chargeItem.coverSection = c1.section;
                        chargeItem.ordinal = c1.coverOrdinal;

                        this.addCoverList = _.without(this.addCoverList, _.findWhere(this.addCoverList, {
                            coverPK: c1.coverPK
                        }));
                    });

                    this.selectedQuote.chargeSummaryItems = _.sortBy(this.selectedQuote.chargeSummaryItems, 'ordinal');
                });
            this.coverVis = 0;
        }

        search() {

            this.quoteStorageService.searchQuotes({
                quoteReference: this.quoteReference,
                lastName: this.lastName,
                tamReference: this.tamReference,
                postcode: this.postcode,
                brokerCode: this.brokerCode,
                email: this.email
            }).then((response: Array<IQuote>) => {
                this.quotes = angular.copy(response);
            });
        }

        emailCustomer() {
            this.scope.updatebutton = true;
            this.quoteStorageService.emailQuote(this.selectedQuote.id)
                .then(() => {
                    alert("Customer has been emailed");
                    this.scope.updatebutton = false;
                });
        }

        emailCustomerRenewal() {
            this.scope.updatebutton = true;

            //this.quoteStorageService.updateQuote(this.selectedQuote);


            if (this.selectedQuote.documents.length < 4) {
                alert("check policy documents");
            }
            else if (this.selectedQuote.policyId == -1) {
                alert("Select Policy Type");
            }
            else {
                this.quoteStorageService.emailRenewal(this.selectedQuote.id)
                    .then(() => {
                        alert("Customer has been emailed");
                        this.scope.updatebutton = false;
                    });
            }
            this.scope.updatebutton = false;
        }

        emailNewBusiness() {
            this.scope.updatebutton = true;
            this.quoteStorageService.emailNewBusinessQuote(this.selectedQuote.id)
                .then(() => {
                    alert("Customer has been emailed");
                    this.scope.updatebutton = false;
                });
            this.scope.updatebutton = false;
        }



        setPolicy(policyFK) {
            // ToDo: to assign policyFK to renewal quote where this is not known

        }

        static routing($routeProvider) {
            $routeProvider.when("/admin/underwriter/quotes",
                {
                    controller: "UnderwriterQuotesController",
                    templateUrl: "/app/admin/underwriter/quotes/search.html",
                    controllerAs: "quotes",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            $routeProvider.when("/admin/underwriter/quotes/search",
                {
                    controller: "UnderwriterQuotesController",
                    templateUrl: "/app/admin/underwriter/quotes/search.html",
                    controllerAs: "quotes",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            $routeProvider.when("/admin/underwriter/quotes/edit/:QUOTEREFERENCE",
                {
                    controller: "UnderwriterQuotesController",
                    templateUrl: "/app/admin/underwriter/quotes/edit.html",
                    controllerAs: "quotes",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
        }
    }

    angular.module("LeisureInsure.Admin")
        .controller("UnderwriterQuotesController", UnderwriterQuotesController)
        .config(["$routeProvider", UnderwriterQuotesController.routing]);
}
