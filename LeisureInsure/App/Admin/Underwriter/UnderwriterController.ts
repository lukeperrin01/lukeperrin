﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../../scripts/typings/underscore/underscore.d.ts" />
/// <reference path="../../../scripts/typings/angularjs/angular-route.d.ts" />

module LeisureInsure.Admin {
    export class UnderwriterController {

        static $inject = ["$http", "$scope"];
        http: ng.IHttpService;
        scope: any;
        constructor($http, $scope) {
            this.http = $http;
            this.scope = $scope;
            this.scope.alerts = [];
        }

        confirmAgentPolicy() {
            this.scope.laddaStartProcess = true;
            this.http.post("api/v1/underwriter/confirmAgentPolicy",
                {
                    agentCode: this.agentCode,
                    quoteReference: this.quoteReference
                }).then(_ => {
                    this.scope.laddaStartProcess = false;
                }).catch((data: any) => {                   
                    
                    eval("ShowUIDialog('" + data.data.message + "');");
                });
        }

        confirmClientPayment() {
            this.scope.laddaStartProcess = true;
            this.http.post("api/v1/underwriter/confirmClientPayment",
                {
                    agentCode: this.agentCode,
                    quoteReference: this.quoteReference
                }).then(_ => {
                    this.scope.laddaStartProcess = false;
                }).catch((data: any) => {

                    eval("ShowUIDialog('" + data.data.message + "');");
                });
        }

        addUnderwriter() {
            this.http.post(
                "api/v1/underwriters",
                {
                    tamCode: this.tamCode,
                    firstname: this.firstname,
                    surname: this.lastname,
                    telephone: this.telephone,
                    email: this.email,
                    password: this.password
                }).then(() => {
                    this.scope.alerts.push({
                        type: "success",
                        msg: "Underwriter successfully added"
                    });
                    this.tamCode = "";
                    this.firstname = "";
                    this.lastname = "";
                    this.telephone = "";
                    this.email = "";
                    this.password = "";
                    //this.scope.addBroker.$setPristine();
                    this.scope.addBroker.$setUntouched();
                }, response => {
                    this.scope.alerts.push({
                        type: "danger",
                        msg: "There was a problem adding the underwriter: " + response.statusText
                    });
                });
        }


        agentCode: string;
        quoteReference: string;
        tamCode: string;
        firstname: string;
        lastname: string;
        telephone: string;
        email: string;
        password: string;

        static routing($routeProvider) {
            $routeProvider.when("/admin/underwriter",
                {
                    controller: "UnderwriterController",
                    templateUrl: "/app/admin/underwriter/index.html",
                    controllerAs: "underwriter",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            $routeProvider.when("/admin/underwriter/add",
                {
                    controller: "UnderwriterController",
                    templateUrl: "/app/admin/underwriter/add.html",
                    controllerAs: "underwriter",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
        }
    }

    angular.module("LeisureInsure.Admin")
        .controller("UnderwriterController", UnderwriterController)
        .config(["$routeProvider", UnderwriterController.routing]);
}