var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var UnderwriterController = (function () {
            function UnderwriterController($http, $scope) {
                this.http = $http;
                this.scope = $scope;
                this.scope.alerts = [];
            }
            UnderwriterController.prototype.confirmAgentPolicy = function () {
                var _this = this;
                this.scope.laddaStartProcess = true;
                this.http.post("api/v1/underwriter/confirmAgentPolicy", {
                    agentCode: this.agentCode,
                    quoteReference: this.quoteReference
                }).then(function (_) {
                    _this.scope.laddaStartProcess = false;
                }).catch(function (data) {
                    eval("ShowUIDialog('" + data.data.message + "');");
                });
            };
            UnderwriterController.prototype.confirmClientPayment = function () {
                var _this = this;
                this.scope.laddaStartProcess = true;
                this.http.post("api/v1/underwriter/confirmClientPayment", {
                    agentCode: this.agentCode,
                    quoteReference: this.quoteReference
                }).then(function (_) {
                    _this.scope.laddaStartProcess = false;
                }).catch(function (data) {
                    eval("ShowUIDialog('" + data.data.message + "');");
                });
            };
            UnderwriterController.prototype.addUnderwriter = function () {
                var _this = this;
                this.http.post("api/v1/underwriters", {
                    tamCode: this.tamCode,
                    firstname: this.firstname,
                    surname: this.lastname,
                    telephone: this.telephone,
                    email: this.email,
                    password: this.password
                }).then(function () {
                    _this.scope.alerts.push({
                        type: "success",
                        msg: "Underwriter successfully added"
                    });
                    _this.tamCode = "";
                    _this.firstname = "";
                    _this.lastname = "";
                    _this.telephone = "";
                    _this.email = "";
                    _this.password = "";
                    _this.scope.addBroker.$setUntouched();
                }, function (response) {
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: "There was a problem adding the underwriter: " + response.statusText
                    });
                });
            };
            UnderwriterController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/underwriter", {
                    controller: "UnderwriterController",
                    templateUrl: "/app/admin/underwriter/index.html",
                    controllerAs: "underwriter",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
                $routeProvider.when("/admin/underwriter/add", {
                    controller: "UnderwriterController",
                    templateUrl: "/app/admin/underwriter/add.html",
                    controllerAs: "underwriter",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            };
            UnderwriterController.$inject = ["$http", "$scope"];
            return UnderwriterController;
        }());
        Admin.UnderwriterController = UnderwriterController;
        angular.module("LeisureInsure.Admin")
            .controller("UnderwriterController", UnderwriterController)
            .config(["$routeProvider", UnderwriterController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=UnderwriterController.js.map