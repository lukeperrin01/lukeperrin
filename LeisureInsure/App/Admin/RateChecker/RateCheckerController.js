var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var RateCheckerController = (function () {
            function RateCheckerController($http, $q, $scope, $rootScope, $window, rateUpdaterService) {
                var _this = this;
                this.http = $http;
                this.q = $q;
                this.scope = $scope;
                this.scope.alerts = [];
                this.window = $window;
                this.rootScope = $rootScope;
                this.rateUpdater = rateUpdaterService;
                this.updated = false;
                this.updatedLocale = false;
                this.rateMessage = "";
                this.localeMessage = "";
                $("#allGifs").css("display", "block");
                this.rateUpdater.GetPoliciesCoversRates().then(function (data) {
                    _this.policies = data;
                });
                this.rateUpdater.GetLocales().then(function (data) {
                    _this.locales = data;
                });
            }
            RateCheckerController.prototype.Debug = function () {
                console.log(this.selectedPolicy, "selectedpolicy");
                console.log(this.selectedCover, "selectedcover");
                console.log(this.selectedRate, "selectedrate");
            };
            RateCheckerController.prototype.updateRate = function () {
                var _this = this;
                if (this.selectedRate != null) {
                    this.rateUpdater.UpdateRate(this.selectedRate).then(function (data) {
                        _this.updated = true;
                        _this.updatedRate = data.rateName;
                        _this.rateUpdater.GetPoliciesCoversRates();
                    });
                }
                else {
                    this.rateMessage = "Please select a rate!";
                }
            };
            RateCheckerController.prototype.updateLocale = function () {
                var _this = this;
                if (this.selectedLocale != null) {
                    this.rateUpdater.UpdateLocale(this.selectedLocale).then(function (data) {
                        _this.updatedLocale = true;
                        _this.updatedLocaleName = data.country;
                        _this.rateUpdater.GetPoliciesCoversRates();
                    });
                }
                else {
                    this.localeMessage = "Please select a locale!";
                }
            };
            RateCheckerController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/ratechecker", {
                    controller: "RateCheckerController",
                    templateUrl: "/app/admin/ratechecker/ratechecker.html",
                    controllerAs: "RateCheckerController",
                    metadata: {
                        title: "Leisure Insure - RateChecker"
                    }
                });
            };
            RateCheckerController.$inject = ["$http", "$q", "$scope", "$rootScope", "$window", "RateUpdaterService"];
            return RateCheckerController;
        }());
        Admin.RateCheckerController = RateCheckerController;
        angular.module("LeisureInsure.Admin")
            .controller("RateCheckerController", RateCheckerController)
            .config(["$routeProvider", RateCheckerController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=RateCheckerController.js.map