﻿namespace LeisureInsure.Admin {
    export class RateCheckerController {

        static $inject = ["$http", "$q", "$scope", "$rootScope", "$window", "RateUpdaterService"];
        q: ng.IQService;
        http: ng.IHttpService;
        scope: any;
        rootScope: any;
        window: ng.IWindowService;      

        rateUpdater: RateUpdaterService;
        policies: Array<IPolicyView>;               
        locales: Array<ILocale>;    
        selectedLocale: ILocale;
        selectedPolicy: IPolicyView;
        selectedCover: ICoverView;
        selectedRate: IRateView;
        updated: boolean;
        updatedRate: string;
        updatedLocale: boolean;
        updatedLocaleName: string;
        rateMessage: string;
        localeMessage: string;

        constructor($http, $q, $scope, $rootScope, $window, rateUpdaterService : RateUpdaterService) {
            this.http = $http;
            this.q = $q;
            this.scope = $scope;
            this.scope.alerts = [];
            this.window = $window;
            this.rootScope = $rootScope;           
            this.rateUpdater = rateUpdaterService;
            this.updated = false;
            this.updatedLocale = false;
            this.rateMessage = "";
            this.localeMessage = "";

            $("#allGifs").css("display", "block");

            this.rateUpdater.GetPoliciesCoversRates().then((data:Array<IPolicyView>) => {               
                this.policies = data;
            });

            this.rateUpdater.GetLocales().then((data: Array<ILocale>) => {
                this.locales = data;
            });
            
        }       

        Debug(): void {
            console.log(this.selectedPolicy, "selectedpolicy");
            console.log(this.selectedCover, "selectedcover");
            console.log(this.selectedRate, "selectedrate");
        }


        updateRate(): void
        {
            if (this.selectedRate != null) {

                this.rateUpdater.UpdateRate(this.selectedRate).then((data: IRateView) => {
                    this.updated = true;
                    this.updatedRate = data.rateName;

                    this.rateUpdater.GetPoliciesCoversRates();
                });
            }
            else
            {
                this.rateMessage = "Please select a rate!";
            }
        }

        updateLocale(): void {

            if (this.selectedLocale != null) {
                this.rateUpdater.UpdateLocale(this.selectedLocale).then((data: ILocale) => {
                    this.updatedLocale = true;
                    this.updatedLocaleName = data.country;

                    this.rateUpdater.GetPoliciesCoversRates();
                });
            }
            else {
                this.localeMessage = "Please select a locale!";
            }
        }

        static routing($routeProvider) {
            $routeProvider.when("/admin/ratechecker",
                {
                    controller: "RateCheckerController",
                    templateUrl: "/app/admin/ratechecker/ratechecker.html",
                    controllerAs: "RateCheckerController",
                    metadata: {
                        title: "Leisure Insure - RateChecker"
                    }
                });           
        }
    }
    angular.module("LeisureInsure.Admin")
        .controller("RateCheckerController", RateCheckerController)
        .config(["$routeProvider", RateCheckerController.routing]);
}


//function openTab(evt, tabName) {
//    // Declare all variables
//    var i, tabcontent, tablinks;

//    // Get all elements with class="tabcontent" and hide them
//    tabcontent = document.getElementsByClassName("tabcontent");
//    for (i = 0; i < tabcontent.length; i++) {
//        tabcontent[i].style.display = "none";
//    }

//    // Get all elements with class="tablinks" and remove the class "active"
//    tablinks = document.getElementsByClassName("tablinks");
//    for (i = 0; i < tablinks.length; i++) {
//        tablinks[i].className = tablinks[i].className.replace(" active", "");
//    }

//    // Show the current tab, and add an "active" class to the button that opened the tab
//    document.getElementById(tabName).style.display = "block";
//    evt.currentTarget.className += " active";
//}