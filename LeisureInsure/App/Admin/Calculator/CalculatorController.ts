﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../../scripts/typings/underscore/underscore.d.ts" />
/// <reference path="../../../scripts/typings/angularjs/angular-route.d.ts" />

module LeisureInsure.Admin {
    export class CalculatorController {

        static $inject = ["$http"];
        http: ng.IHttpService;
        constructor($http) {
            this.http = $http;
            this.http.get("api/v1/calculators/policies").then(response => {
                this.policies = angular.copy(<Array<IPolicy>>response.data);
            });
            this.http.get("api/v1/calculators/covers").then(response => {
                this.covers = angular.copy(<Array<ICover>>response.data);
            });
        }

        policies: Array<IPolicy>;
        covers: Array<ICover>;

        selectedPolicies: Array<IPolicy>;
        selectedCovers: Array<ICover>;

        policyFlag(): number {
            return _.reduce(this.selectedPolicies, (memo, iteratee: IPolicy) => (memo | iteratee.bitwiseId), 0);
        }
        coverFlag(): number {
            return _.reduce(this.selectedCovers, (memo, iteratee: number) => (memo | iteratee), 0);
        };

        copyPolicy(): void {
            var value = document.getElementById("policyCopy");
            this.copyToClipboard(value);
        }

        copyCover(): void {
            var value = document.getElementById("coverCopy");
            this.copyToClipboard(value);
        }

        copyToClipboard(target) {
            // select the content
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch (e) {
                succeed = false;
            }

            return succeed;
        }



        static routing($routeProvider) {
            $routeProvider.when("/admin/calculator",
                {
                    controller: "CalculatorController",
                    templateUrl: "/app/admin/calculator/calculator.html",
                    controllerAs: "calculator",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
        }
    }

    angular.module("LeisureInsure.Admin")
        .controller("CalculatorController", CalculatorController)
        .config(["$routeProvider", CalculatorController.routing]);
}