var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var LandingPageController = (function () {
            function LandingPageController($location, $routeParams, landingPageService, scope) {
                var _this = this;
                this.landingPages = this.allLandingPages;
                this.save = this.saveLandingPage;
                this.cancel = this.cancelEdit;
                this.edit = this.editLandingPage;
                this.delete = this.deleteLandingPage;
                this.addLandingPage = this.addNewLandingPage;
                this.location = $location;
                this.landingPageService = landingPageService;
                this.scope = scope;
                this.newNavigationData = {};
                var selectedLandingPageId = parseInt($routeParams["LANDINGPAGEID"]);
                if (selectedLandingPageId) {
                    this.landingPageService.initialised.then(function () {
                        var landingPage = _this.landingPageService.landingPageForId(selectedLandingPageId);
                        _this.selectedLandingPage = angular.copy(landingPage);
                        _this.scope.tinymceModel = landingPage.landingPageDescription;
                    });
                }
            }
            LandingPageController.prototype.addNewLandingPage = function () {
                var _this = this;
                this.landingPageService.addLandingPage().then(function (landingPage) {
                    _this.editLandingPage(landingPage.id);
                });
            };
            LandingPageController.prototype.deleteLandingPage = function (landingPageId) {
                this.landingPageService.deleteLandingPage(landingPageId);
            };
            LandingPageController.prototype.allLandingPages = function () {
                return this.landingPageService.landingPages;
            };
            LandingPageController.prototype.editLandingPage = function (landingPageId) {
                if (landingPageId) {
                    var newUrl = "/admin/landingpages/edit/" + landingPageId;
                    this.location.path(newUrl);
                }
            };
            LandingPageController.prototype.saveLandingPage = function () {
                this.landingPageService.updateLandingPage(this.selectedLandingPage);
                this.location.path("/admin/landingpages");
            };
            LandingPageController.prototype.cancelEdit = function () {
                this.location.path("/admin/landingpages");
            };
            LandingPageController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/landingpages", {
                    controller: "LandingPageController",
                    templateUrl: "/app/admin/landingpages/index.html",
                    controllerAs: "landingPageController",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
                $routeProvider.when("/admin/landingpages/edit/:LANDINGPAGEID", {
                    controller: "LandingPageController",
                    templateUrl: "/app/admin/landingpages/edit.html",
                    controllerAs: "landingPageController",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            };
            LandingPageController.$inject = ["$location", "$routeParams", "LandingPageService", "$scope"];
            return LandingPageController;
        }());
        Admin.LandingPageController = LandingPageController;
        angular.module("LeisureInsure.Admin")
            .controller("LandingPageController", LandingPageController)
            .config(["$routeProvider", LandingPageController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=LandingPageController.js.map