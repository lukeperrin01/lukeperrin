﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../../scripts/typings/underscore/underscore.d.ts" />
/// <reference path="../../../scripts/typings/angularjs/angular-route.d.ts" />

module LeisureInsure.Admin {
    export class LandingPageController {

        static $inject = ["$location", "$routeParams", "LandingPageService", "$scope"];
        location: ng.ILocationService;
        constructor($location: ng.ILocationService,
            $routeParams: ng.route.IRouteParamsService,
            landingPageService: LandingPageService,
            scope: ng.IScope) {

            this.location = $location;
            this.landingPageService = landingPageService;
            this.scope = scope;

            this.newNavigationData = <INavigationData>{};

            var selectedLandingPageId = parseInt(<string>$routeParams["LANDINGPAGEID"]);
            if (selectedLandingPageId) {
                this.landingPageService.initialised.then(() => {
                    var landingPage = this.landingPageService.landingPageForId(selectedLandingPageId);
                    this.selectedLandingPage = angular.copy(landingPage);
                    this.scope.tinymceModel = landingPage.landingPageDescription;
                });
            }
        }

        landingPages = this.allLandingPages;
        save = this.saveLandingPage;
        cancel = this.cancelEdit;
        edit = this.editLandingPage;
        delete = this.deleteLandingPage;        
        addLandingPage = this.addNewLandingPage;

        landingPageService: LandingPageService;
        selectedLandingPage: ILandingPageDefinition;
        newNavigationData: INavigationData;
        scope: any;

        private addNewLandingPage() {
            this.landingPageService.addLandingPage().then(landingPage => {
                this.editLandingPage(landingPage.id);
            });
        }

        private deleteLandingPage(landingPageId: number) {
            this.landingPageService.deleteLandingPage(landingPageId);    
        }
        

        private allLandingPages(): Array<ILandingPageDefinition> {
            return this.landingPageService.landingPages;
        }

        private editLandingPage(landingPageId: number): void {
            if (landingPageId) {
                var newUrl = "/admin/landingpages/edit/" + landingPageId;
                this.location.path(newUrl);
            }
        }

        private saveLandingPage() {
            this.landingPageService.updateLandingPage(this.selectedLandingPage);
            this.location.path("/admin/landingpages");
        }

        private cancelEdit() {
            this.location.path("/admin/landingpages");
        }

        static routing($routeProvider) {
            $routeProvider.when("/admin/landingpages",
            {
                controller: "LandingPageController",
                templateUrl: "/app/admin/landingpages/index.html",
                controllerAs: "landingPageController",
                metadata: {
                    title: "Leisure Insure - Admin"
                }
            });
            $routeProvider.when("/admin/landingpages/edit/:LANDINGPAGEID",
            {
                controller: "LandingPageController",
                templateUrl: "/app/admin/landingpages/edit.html",
                controllerAs: "landingPageController",
                metadata: {
                    title: "Leisure Insure - Admin"
                }
            });
        }
    }

    angular.module("LeisureInsure.Admin")
        .controller("LandingPageController", LandingPageController)
        .config(["$routeProvider", LandingPageController.routing]);
}