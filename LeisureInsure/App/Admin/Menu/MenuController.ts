﻿namespace LeisureInsure.Admin {
    export class MenuController {
        static $inject = ["$http"];
        constructor($http) {
            this.http = $http;
        }
        http: ng.IHttpService;
        testSSIS() {
            alert("hit");
            this.http({
                url: "/api/v1/admin/testSSIS",
                method: "POST"
            })
                .success((data: any) => {

                });
        }
        static routing($routeProvider) {
            $routeProvider.when("/admin",
                {
                    controller: "MenuController",
                    templateUrl: "/app/admin/menu/menu.html",
                    controllerAs: "menu",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
        }
    }

    angular.module("LeisureInsure.Admin")
        .controller("MenuController", MenuController)
        .config(["$routeProvider", MenuController.routing]);
}