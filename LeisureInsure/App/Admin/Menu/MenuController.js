var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var MenuController = (function () {
            function MenuController($http) {
                this.http = $http;
            }
            MenuController.prototype.testSSIS = function () {
                alert("hit");
                this.http({
                    url: "/api/v1/admin/testSSIS",
                    method: "POST"
                })
                    .success(function (data) {
                });
            };
            MenuController.routing = function ($routeProvider) {
                $routeProvider.when("/admin", {
                    controller: "MenuController",
                    templateUrl: "/app/admin/menu/menu.html",
                    controllerAs: "menu",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            };
            MenuController.$inject = ["$http"];
            return MenuController;
        }());
        Admin.MenuController = MenuController;
        angular.module("LeisureInsure.Admin")
            .controller("MenuController", MenuController)
            .config(["$routeProvider", MenuController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=MenuController.js.map