﻿namespace LeisureInsure.Admin {
    export class FailedTAMController {
        static $inject = ["$http", "$q", "$scope", "$rootScope", "$window", "QuoteStorageService"];
        q: ng.IQService;
        constructor($http, $q, $scope, $rootScope, $window, quoteStorageService) {
            this.http = $http;
            this.q = $q;
            this.scope = $scope;
            this.scope.alerts = [];
            this.window = $window;
            this.rootScope = $rootScope;
            this.getFailedTAM();
            this.quoteStorageService = quoteStorageService;
        }
        http: ng.IHttpService;
        scope: any;
        rootScope: any;
        window: ng.IWindowService;
        quotes: Array<IQuote>;
        quoteStorageService: QuoteStorageService;

        getFailedTAM() {
            this.http({
                url: "/api/v1/admin/failedTAM",
                method: "POST"
            })
                .success((data: Array<IQuote>) => {
                    this.quotes = data;
                });
        }
        addToTam(quotePK) {
            this.quoteStorageService.addToTam(quotePK);
        }

        static routing($routeProvider) {
            $routeProvider.when("/admin/failedTAM",
                {
                    controller: "FailedTAMController",
                    templateUrl: "/app/admin/failedTAM/FailedTAM.html",
                    controllerAs: "failedTAM",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
        }

    }
    angular.module("LeisureInsure.Admin")
        .controller("FailedTAMController", FailedTAMController)
        .config(["$routeProvider", FailedTAMController.routing]);
}