﻿namespace LeisureInsure.Admin {
    export class RenewalController {

        static $inject = ["$http", "$q", "$scope", "$rootScope", "$window", "QuoteStorageService", "LocationService"];
        q: ng.IQService;
        locationService: LocationService;
        constructor($http, $q, $scope, $rootScope, $window, QuoteStorageService, LocationService) {
            this.http = $http;
            this.q = $q;
            this.scope = $scope;
            this.scope.alerts = [];
            this.window = $window;
            this.rootScope = $rootScope;
            this.quoteStorageService = QuoteStorageService;
            this.locationService = LocationService;

            //show first search tab as default
            document.getElementById("policySearchBtn").click();

            this.quoteRef = "";
            this.error = "";

            this.LoadDefaults();



        } //end constructor

        http: ng.IHttpService;
        scope: any;
        rootScope: any;
        window: ng.IWindowService;
        quoteStorageService: QuoteStorageService;
        instructors: Array<string>;
        //show hide certain fields       

        instructorName: string;
        emailedQuote: boolean;

        view: IDefaultEngine;

        //policydetail
        policyEngine: IPolicyEngine;
        clientEngine: IClientEngine;
        quoteEngine: IQuoteEngine;
        quoteRef: string;
        error: string;
        policyData: IPolicyView;

        //search fields
        clients: Array<IClientEngine>;

        //items to add to tables for various covers
        plItem: IListItem;
        elItem: IListItem;
        mdItem: IListItem;
        biItem: IListItem;
        cancelItem: IListItem;

        elIndemnities: Array<IIndemnity>;

        //MD Cover Tab
        selectedMdCover: ICoverView;
        //input fields for Material Damage cover
        mdCover: ICoverView;

        testQuoteAddress: string;

        Debug() {

            console.log("ELItem", this.elItem);
            console.log("MDItem", this.mdItem);
            console.log("BIItem", this.biItem);
            console.log("policyEngine", this.policyEngine);
            console.log("clientEngine", this.clientEngine);
            console.log("clients", this.clients);
            console.log("views", this.view);
            console.log("selected MD", this.selectedMdCover);



        }

        LoadDefaults() {

            //if for some reason this hadnt loaded in home page then set to default (UK)
            if (this.locationService.selectedLocale == null) {
                //this.locationService.getlocales("", 1).then((data: any) => {
                this.GetClientsAndQuotes(1);
                //});;
            }
            else
                this.GetClientsAndQuotes(this.locationService.selectedLocale.localePK);
        }

        /* ------   Public Liability Tab   -------- */

        AddItemPL() {

            //only add items if we have selected PL cover and have filled out the tab
            if (this.policyEngine.plCover != null) {
                if (this.policyEngine.plIndemnity != null) {

                    //add this item to MD as well which means creating a default MD cover
                    if (this.plItem.sumInsured != null) {

                        if (this.policyEngine.mdCover == null) {

                            var defaultCover = <ICoverView>{};

                            defaultCover.total = 0;
                            defaultCover.mdAddress = "";
                            defaultCover.mdPostCode = "";
                            defaultCover.sumInsured = 0;
                            defaultCover.excess = 0;

                            this.policyEngine.mdCover = defaultCover;
                        }

                        if (this.policyEngine.mdCover.itemsAdded == null)
                            this.policyEngine.mdCover.itemsAdded = [];

                        var item: IListItem;
                        if (this.plItem != null) {
                            var item = angular.copy(this.plItem, item);
                            this.policyEngine.mdCover.itemsAdded.push(item);
                        }


                    }

                    if (this.policyEngine.plCover.itemsAdded == null)
                        this.policyEngine.plCover.itemsAdded = [];

                    var item: IListItem;
                    if (this.plItem != null) {
                        var item = angular.copy(this.plItem, item);
                        this.policyEngine.plCover.itemsAdded.push(item);
                    }
                }
            }
        }

        SelectItemPL(index: number) {
            var item = this.policyEngine.plCover.itemsAdded[index];

            _.each(this.policyEngine.plCover.itemsAdded, (item: IListItem) => {
                item.selected = false;
            })

            item.selected = true;

            var updatedItem: IListItem;
            updatedItem = angular.copy(item, updatedItem);
            this.plItem = updatedItem;
        }

        UpdatePLItem() {
            var selected = this.policyEngine.plCover.itemsAdded.filter(x => x.selected == true)[0];
            var index = this.policyEngine.plCover.itemsAdded.indexOf(selected);
            this.policyEngine.plCover.itemsAdded[index] = this.plItem;
        }

        RemovePLItem() {
            var selected = this.policyEngine.plCover.itemsAdded.filter(x => x.selected == true)[0];
            var index = this.policyEngine.plCover.itemsAdded.indexOf(selected);
            this.policyEngine.plCover.itemsAdded.splice(index, 1);
        }

        /* ------   Employer Liability Tab   -------- */

        AddItemEL() {

            //only add items if we have selected PL cover and have filled out the tab
            if (this.policyEngine.elCover != null) {
                if (this.policyEngine.elIndemnity != null) {

                    if (this.policyEngine.elCover.itemsAdded == null)
                        this.policyEngine.elCover.itemsAdded = [];

                    var item: IListItem;
                    if (this.elItem != null) {
                        var item = angular.copy(this.elItem, item);

                        this.policyEngine.elCover.itemsAdded.push(item);
                    }
                }
            }
        }

        SelectItemEL(index: number) {
            var item = this.policyEngine.elCover.itemsAdded[index];

            _.each(this.policyEngine.elCover.itemsAdded, (item: IListItem) => {
                item.selected = false;
            })

            item.selected = true;

            var updatedItem: IListItem;
            updatedItem = angular.copy(item, updatedItem);
            this.elItem = updatedItem;
        }

        UpdateELItem() {
            var selected = this.policyEngine.elCover.itemsAdded.filter(x => x.selected == true)[0];
            var index = this.policyEngine.elCover.itemsAdded.indexOf(selected);
            this.policyEngine.elCover.itemsAdded[index] = this.elItem;
        }

        RemoveELItem() {
            var selected = this.policyEngine.elCover.itemsAdded.filter(x => x.selected == true)[0];
            var index = this.policyEngine.elCover.itemsAdded.indexOf(selected);
            this.policyEngine.elCover.itemsAdded.splice(index, 1);
        }


        /* ------   Material Damage Tab   -------- */

        AddItemMD() {

            //only add items if we have selected PL cover and have filled out the tab
            if (this.policyEngine.mdCover != null) {

                if (this.policyEngine.mdCover.itemsAdded == null)
                    this.policyEngine.mdCover.itemsAdded = [];

                var item: IListItem;
                if (this.mdItem != null) {
                    var item = angular.copy(this.mdItem, item);

                    //we are taking the excess from the main cover to use for items
                    item.excess = this.policyEngine.mdCover.excess;

                    this.policyEngine.mdCover.itemsAdded.push(item);
                }

            }
        }

        SelectItemMD(index: number) {

            var item = this.policyEngine.mdCover.itemsAdded[index];

            _.each(this.policyEngine.mdCover.itemsAdded, (item: IListItem) => {
                item.selected = false;
            })

            item.selected = true;

            var updatedItem: IListItem;
            updatedItem = angular.copy(item, updatedItem);
            this.mdItem = updatedItem;
        }

        UpdateMDItem() {
            var selected = this.policyEngine.mdCover.itemsAdded.filter(x => x.selected == true)[0];
            var index = this.policyEngine.mdCover.itemsAdded.indexOf(selected);
            this.policyEngine.mdCover.itemsAdded[index] = this.mdItem;
        }

        RemoveMDItem() {
            var selected = this.policyEngine.mdCover.itemsAdded.filter(x => x.selected == true)[0];
            var index = this.policyEngine.mdCover.itemsAdded.indexOf(selected);
            this.policyEngine.mdCover.itemsAdded.splice(index, 1);
        }

        AddMdCover() {
            //get selected cover, add it to our list
            if (this.policyEngine.mdCovers == null)
                this.policyEngine.mdCovers = [];

            var cover: ICoverView;
            var cover = angular.copy(this.mdCover, cover);
            //we are taking the excess from the main MD input to use for cover
            cover.excess = this.policyEngine.mdCover.excess;
            this.policyEngine.mdCovers.push(cover);
        }

        SelectMDRow(index:number) {

            var cover = this.policyEngine.mdCovers[index];

            _.each(this.policyEngine.mdCovers, (item: ICoverView) => {
                item.selected = false;
            })

            cover.selected = true;
            //update input fields with selected cover
            var updatedCover: ICoverView;
            updatedCover = angular.copy(cover, updatedCover);
            this.mdCover = updatedCover;
        }


        RemoveMDCover() {
            var selected = this.policyEngine.mdCovers.filter(x => x.selected == true)[0];
            var index = this.policyEngine.mdCovers.indexOf(selected);

            this.policyEngine.mdCovers.splice(index, 1);
        }

        UpdateMdCover() {

            var selected = this.policyEngine.mdCovers.filter(x => x.selected == true)[0];
            var index = this.policyEngine.mdCovers.indexOf(selected);
            this.policyEngine.mdCovers[index] = this.mdCover;

        }

        SelectMDCover()
        {
           this.mdCover = this.selectedMdCover;
        }


        /* ------   Business Interruption Tab   -------- */

        AddItemBI() {

            //only add items if we have selected PL cover and have filled out the tab
            if (this.policyEngine.biCover != null) {

                if (this.policyEngine.biCover.itemsAdded == null)
                    this.policyEngine.biCover.itemsAdded = [];

                var item: IListItem;
                if (this.biItem != null) {
                    var item = angular.copy(this.biItem, item);

                    this.policyEngine.biCover.itemsAdded.push(item);
                }

            }
        }

        SelectItemBI(index: number) {
            var item = this.policyEngine.biCover.itemsAdded[index];

            _.each(this.policyEngine.biCover.itemsAdded, (item: IListItem) => {
                item.selected = false;
            })

            item.selected = true;

            var updatedItem: IListItem;
            var updatedItem = angular.copy(item, updatedItem);
            this.biItem = updatedItem;
        }

        UpdateBIItem() {
            var selected = this.policyEngine.biCover.itemsAdded.filter(x => x.selected == true)[0];
            var index = this.policyEngine.biCover.itemsAdded.indexOf(selected);
            this.policyEngine.biCover.itemsAdded[index] = this.biItem;
        }

        RemoveBIItem() {
            var selected = this.policyEngine.biCover.itemsAdded.filter(x => x.selected == true)[0];
            var index = this.policyEngine.biCover.itemsAdded.indexOf(selected);
            this.policyEngine.biCover.itemsAdded.splice(index, 1);
        }

        GetClientsAndQuotes(localid: number) {
            this.quoteStorageService.GetClients(localid)
                .then((view: IDefaultEngine) => {

                    this.clients = view.clients;
                    this.view = view;
                    this.error = "";

                    this.elIndemnities = this.view.indemnities.filter(x => x.indemnity == 5000000 || x.indemnity == 10000000);



                }).catch((data: any) => {
                    this.error = data.message;
                });

        }

        EmailQuote()
        {
            //email client links to view quote and pay 
            this.quoteStorageService.EmailQuote(this.policyEngine.quoteRef, this.policyEngine.pword,"").then((result: string) => {
                this.error = "";
                this.emailedQuote = true;
                
            }).catch((data: any) => {
                this.error = data.message;
            });

        }

        EmailTestQuote() {
            //email client links to view quote and pay 
            this.quoteStorageService.EmailQuote(this.policyEngine.quoteRef, this.policyEngine.pword,this.testQuoteAddress).then((result: string) => {
                this.error = "";
                this.emailedQuote = true;

            }).catch((data: any) => {
                this.error = data.message;
            });
        }

        SavePDF() {

            if (this.policyEngine.quoteRef != null) {

                this.policyEngine.customClauseName = uploadFilename;

                if (this.policyEngine.customClauses == null)
                    this.policyEngine.customClauses = [];

                this.policyEngine.customClauses.push(uploadFilename);

                this.quoteStorageService.SavePdf(uploadedFile, this.policyEngine.id, uploadFilename).then((result: IPolicyView) => {

                    this.error = "";
                }).catch((data: any) => {
                    this.error = data.message;
                });
            }
        }

        DeletePDF(index: number) {

            var customClauseName = this.policyEngine.customClauses[index];
            this.policyEngine.customClauses.splice(index, 1);
            this.quoteStorageService.DeletePdf(this.policyEngine.id, customClauseName);
        }

        SubmitForm(valid: boolean) {


            if (this.locationService.selectedLocale == null) {
                this.error = "Please select a location";
                valid = false;
            }

            if (valid) {

                //frontend to backend - convert JS Date to DateTime
                this.policyEngine.cover.startDate = this.policyEngine.cover.startDateDt.toDateString();
                this.policyEngine.cover.endDate = this.policyEngine.cover.endDateDt.toDateString();
                this.policyEngine.cover.purchasedDate = this.policyEngine.cover.purchasedDateDt.toDateString();
                this.policyEngine.cover.issuedDate = this.policyEngine.cover.issuedDateDt.toDateString();
                //we lose the time with toDateString so just convert this using our method
                this.policyEngine.cover.startTime = GetDateTime(this.policyEngine.cover.startTimeDt);
                this.policyEngine.cover.endTime = GetDateTime(this.policyEngine.cover.endTimeDt);
                this.policyEngine.cover.purchasedTime = GetDateTime(this.policyEngine.cover.purchasedTimeDt);                

                this.quoteEngine =
                    {
                        clientDetail: this.clientEngine,
                        coverDetail: this.policyEngine.cover,
                        policyDetail: this.policyEngine,
                        localeId: this.locationService.selectedLocale.localePK
                    }


                this.quoteStorageService.CreateUpdatePolicy(this.quoteEngine).then((policyData: IPolicyView) => {

                    this.policyData = policyData;
                    this.quoteRef = policyData.quoteReference;

                    if (this.policyEngine.quoteRef == null) {
                        //new quote
                        this.policyEngine.pword = policyData.password;
                        this.policyEngine.quoteRef = policyData.quoteReference;
                    }


                    this.LoadDefaults();

                    this.error = "";
                }).catch((data: any) => {
                    this.error = data.message;
                });
            }

        }
        

        RemoveInstructor(index: number) {

            this.policyEngine.cover.instructors.splice(index, 1);
        }

        AddInstructor() {

            if (this.policyEngine.cover.instructors == null)
                this.policyEngine.cover.instructors = [];

            this.policyEngine.cover.instructors.push(this.instructorName);
        }

        AddClause() {

            if (this.policyEngine.clauses == null)
                this.policyEngine.clauses = [];

            this.policyEngine.clauses.push(this.policyEngine.policyClause);
        }

        RemoveClause(index: number) {

            this.policyEngine.clauses.splice(index, 1);
        }

        SelectPolicy() {
            //backend to frontend - convert DateTime to JS Date
            if (this.policyEngine != null) {
                this.policyEngine.cover.startDateDt = new Date(this.policyEngine.cover.startDate);
                this.policyEngine.cover.endDateDt = new Date(this.policyEngine.cover.endDate);
                this.policyEngine.cover.issuedDateDt = new Date(this.policyEngine.cover.issuedDate);
                this.policyEngine.cover.purchasedDateDt = new Date(this.policyEngine.cover.purchasedDate);

                this.policyEngine.cover.endTimeDt = GetJSDateFromTime(this.policyEngine.cover.endTime);
                this.policyEngine.cover.startTimeDt = GetJSDateFromTime(this.policyEngine.cover.startTime);
                this.policyEngine.cover.purchasedTimeDt = GetJSDateFromTime(this.policyEngine.cover.purchasedTime);
            }
        }

        SelectClient() {
            this.policyEngine = null;
        }

        ViewQuote() {

            var win = window.open();

            var certRef = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("admin/renewal")) +
                'newcertificate/' + this.policyEngine.quoteRef + '/' + this.policyEngine.pword;

            win.location.href = certRef;

            $("#cardLabel").css("display", "none");
        }

        static routing($routeProvider) {
            $routeProvider.when("/admin/renewal",
                {
                    controller: "RenewalController",
                    templateUrl: "/App/Admin/Renewal/Renewal.html",
                    controllerAs: "RenewalController",
                    metadata: {
                        title: "Leisure Insure - Renewal"
                    }
                });
            $routeProvider.when("/admin/underwriter/quotes/edit/:QUOTEREFERENCE",
                {
                    controller: "UnderwriterQuotesController",
                    templateUrl: "/app/admin/underwriter/quotes/edit.html",
                    controllerAs: "quotes",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
        }
    }
    angular.module("LeisureInsure.Admin")
        .controller("RenewalController", RenewalController)
        .config(["$routeProvider", RenewalController.routing]);
}

var uploadedFile;
var uploadFilename;
var filecontentByte;

function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

function FileSelected(fileuploader: any) {


    debugger;
    var reader = new FileReader();
    var file = fileuploader.files[0];

    reader.readAsBinaryString(file);
    uploadFilename = file.name;

    reader.onload = function (evt: any) {

        uploadedFile = evt.target.result;
    }
}

