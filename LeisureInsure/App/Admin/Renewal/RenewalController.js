var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var RenewalController = (function () {
            function RenewalController($http, $q, $scope, $rootScope, $window, QuoteStorageService, LocationService) {
                this.http = $http;
                this.q = $q;
                this.scope = $scope;
                this.scope.alerts = [];
                this.window = $window;
                this.rootScope = $rootScope;
                this.quoteStorageService = QuoteStorageService;
                this.locationService = LocationService;
                document.getElementById("policySearchBtn").click();
                this.quoteRef = "";
                this.error = "";
                this.LoadDefaults();
            }
            RenewalController.prototype.Debug = function () {
                console.log("ELItem", this.elItem);
                console.log("MDItem", this.mdItem);
                console.log("BIItem", this.biItem);
                console.log("policyEngine", this.policyEngine);
                console.log("clientEngine", this.clientEngine);
                console.log("clients", this.clients);
                console.log("views", this.view);
                console.log("selected MD", this.selectedMdCover);
            };
            RenewalController.prototype.LoadDefaults = function () {
                if (this.locationService.selectedLocale == null) {
                    this.GetClientsAndQuotes(1);
                }
                else
                    this.GetClientsAndQuotes(this.locationService.selectedLocale.localePK);
            };
            RenewalController.prototype.AddItemPL = function () {
                if (this.policyEngine.plCover != null) {
                    if (this.policyEngine.plIndemnity != null) {
                        if (this.plItem.sumInsured != null) {
                            if (this.policyEngine.mdCover == null) {
                                var defaultCover = {};
                                defaultCover.total = 0;
                                defaultCover.mdAddress = "";
                                defaultCover.mdPostCode = "";
                                defaultCover.sumInsured = 0;
                                defaultCover.excess = 0;
                                this.policyEngine.mdCover = defaultCover;
                            }
                            if (this.policyEngine.mdCover.itemsAdded == null)
                                this.policyEngine.mdCover.itemsAdded = [];
                            var item;
                            if (this.plItem != null) {
                                var item = angular.copy(this.plItem, item);
                                this.policyEngine.mdCover.itemsAdded.push(item);
                            }
                        }
                        if (this.policyEngine.plCover.itemsAdded == null)
                            this.policyEngine.plCover.itemsAdded = [];
                        var item;
                        if (this.plItem != null) {
                            var item = angular.copy(this.plItem, item);
                            this.policyEngine.plCover.itemsAdded.push(item);
                        }
                    }
                }
            };
            RenewalController.prototype.SelectItemPL = function (index) {
                var item = this.policyEngine.plCover.itemsAdded[index];
                _.each(this.policyEngine.plCover.itemsAdded, function (item) {
                    item.selected = false;
                });
                item.selected = true;
                var updatedItem;
                updatedItem = angular.copy(item, updatedItem);
                this.plItem = updatedItem;
            };
            RenewalController.prototype.UpdatePLItem = function () {
                var selected = this.policyEngine.plCover.itemsAdded.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.plCover.itemsAdded.indexOf(selected);
                this.policyEngine.plCover.itemsAdded[index] = this.plItem;
            };
            RenewalController.prototype.RemovePLItem = function () {
                var selected = this.policyEngine.plCover.itemsAdded.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.plCover.itemsAdded.indexOf(selected);
                this.policyEngine.plCover.itemsAdded.splice(index, 1);
            };
            RenewalController.prototype.AddItemEL = function () {
                if (this.policyEngine.elCover != null) {
                    if (this.policyEngine.elIndemnity != null) {
                        if (this.policyEngine.elCover.itemsAdded == null)
                            this.policyEngine.elCover.itemsAdded = [];
                        var item;
                        if (this.elItem != null) {
                            var item = angular.copy(this.elItem, item);
                            this.policyEngine.elCover.itemsAdded.push(item);
                        }
                    }
                }
            };
            RenewalController.prototype.SelectItemEL = function (index) {
                var item = this.policyEngine.elCover.itemsAdded[index];
                _.each(this.policyEngine.elCover.itemsAdded, function (item) {
                    item.selected = false;
                });
                item.selected = true;
                var updatedItem;
                updatedItem = angular.copy(item, updatedItem);
                this.elItem = updatedItem;
            };
            RenewalController.prototype.UpdateELItem = function () {
                var selected = this.policyEngine.elCover.itemsAdded.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.elCover.itemsAdded.indexOf(selected);
                this.policyEngine.elCover.itemsAdded[index] = this.elItem;
            };
            RenewalController.prototype.RemoveELItem = function () {
                var selected = this.policyEngine.elCover.itemsAdded.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.elCover.itemsAdded.indexOf(selected);
                this.policyEngine.elCover.itemsAdded.splice(index, 1);
            };
            RenewalController.prototype.AddItemMD = function () {
                if (this.policyEngine.mdCover != null) {
                    if (this.policyEngine.mdCover.itemsAdded == null)
                        this.policyEngine.mdCover.itemsAdded = [];
                    var item;
                    if (this.mdItem != null) {
                        var item = angular.copy(this.mdItem, item);
                        item.excess = this.policyEngine.mdCover.excess;
                        this.policyEngine.mdCover.itemsAdded.push(item);
                    }
                }
            };
            RenewalController.prototype.SelectItemMD = function (index) {
                var item = this.policyEngine.mdCover.itemsAdded[index];
                _.each(this.policyEngine.mdCover.itemsAdded, function (item) {
                    item.selected = false;
                });
                item.selected = true;
                var updatedItem;
                updatedItem = angular.copy(item, updatedItem);
                this.mdItem = updatedItem;
            };
            RenewalController.prototype.UpdateMDItem = function () {
                var selected = this.policyEngine.mdCover.itemsAdded.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.mdCover.itemsAdded.indexOf(selected);
                this.policyEngine.mdCover.itemsAdded[index] = this.mdItem;
            };
            RenewalController.prototype.RemoveMDItem = function () {
                var selected = this.policyEngine.mdCover.itemsAdded.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.mdCover.itemsAdded.indexOf(selected);
                this.policyEngine.mdCover.itemsAdded.splice(index, 1);
            };
            RenewalController.prototype.AddMdCover = function () {
                if (this.policyEngine.mdCovers == null)
                    this.policyEngine.mdCovers = [];
                var cover;
                var cover = angular.copy(this.mdCover, cover);
                cover.excess = this.policyEngine.mdCover.excess;
                this.policyEngine.mdCovers.push(cover);
            };
            RenewalController.prototype.SelectMDRow = function (index) {
                var cover = this.policyEngine.mdCovers[index];
                _.each(this.policyEngine.mdCovers, function (item) {
                    item.selected = false;
                });
                cover.selected = true;
                var updatedCover;
                updatedCover = angular.copy(cover, updatedCover);
                this.mdCover = updatedCover;
            };
            RenewalController.prototype.RemoveMDCover = function () {
                var selected = this.policyEngine.mdCovers.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.mdCovers.indexOf(selected);
                this.policyEngine.mdCovers.splice(index, 1);
            };
            RenewalController.prototype.UpdateMdCover = function () {
                var selected = this.policyEngine.mdCovers.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.mdCovers.indexOf(selected);
                this.policyEngine.mdCovers[index] = this.mdCover;
            };
            RenewalController.prototype.SelectMDCover = function () {
                this.mdCover = this.selectedMdCover;
            };
            RenewalController.prototype.AddItemBI = function () {
                if (this.policyEngine.biCover != null) {
                    if (this.policyEngine.biCover.itemsAdded == null)
                        this.policyEngine.biCover.itemsAdded = [];
                    var item;
                    if (this.biItem != null) {
                        var item = angular.copy(this.biItem, item);
                        this.policyEngine.biCover.itemsAdded.push(item);
                    }
                }
            };
            RenewalController.prototype.SelectItemBI = function (index) {
                var item = this.policyEngine.biCover.itemsAdded[index];
                _.each(this.policyEngine.biCover.itemsAdded, function (item) {
                    item.selected = false;
                });
                item.selected = true;
                var updatedItem;
                var updatedItem = angular.copy(item, updatedItem);
                this.biItem = updatedItem;
            };
            RenewalController.prototype.UpdateBIItem = function () {
                var selected = this.policyEngine.biCover.itemsAdded.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.biCover.itemsAdded.indexOf(selected);
                this.policyEngine.biCover.itemsAdded[index] = this.biItem;
            };
            RenewalController.prototype.RemoveBIItem = function () {
                var selected = this.policyEngine.biCover.itemsAdded.filter(function (x) { return x.selected == true; })[0];
                var index = this.policyEngine.biCover.itemsAdded.indexOf(selected);
                this.policyEngine.biCover.itemsAdded.splice(index, 1);
            };
            RenewalController.prototype.GetClientsAndQuotes = function (localid) {
                var _this = this;
                this.quoteStorageService.GetClients(localid)
                    .then(function (view) {
                    _this.clients = view.clients;
                    _this.view = view;
                    _this.error = "";
                    _this.elIndemnities = _this.view.indemnities.filter(function (x) { return x.indemnity == 5000000 || x.indemnity == 10000000; });
                }).catch(function (data) {
                    _this.error = data.message;
                });
            };
            RenewalController.prototype.EmailQuote = function () {
                var _this = this;
                this.quoteStorageService.EmailQuote(this.policyEngine.quoteRef, this.policyEngine.pword, "").then(function (result) {
                    _this.error = "";
                    _this.emailedQuote = true;
                }).catch(function (data) {
                    _this.error = data.message;
                });
            };
            RenewalController.prototype.EmailTestQuote = function () {
                var _this = this;
                this.quoteStorageService.EmailQuote(this.policyEngine.quoteRef, this.policyEngine.pword, this.testQuoteAddress).then(function (result) {
                    _this.error = "";
                    _this.emailedQuote = true;
                }).catch(function (data) {
                    _this.error = data.message;
                });
            };
            RenewalController.prototype.SavePDF = function () {
                var _this = this;
                if (this.policyEngine.quoteRef != null) {
                    this.policyEngine.customClauseName = uploadFilename;
                    if (this.policyEngine.customClauses == null)
                        this.policyEngine.customClauses = [];
                    this.policyEngine.customClauses.push(uploadFilename);
                    this.quoteStorageService.SavePdf(uploadedFile, this.policyEngine.id, uploadFilename).then(function (result) {
                        _this.error = "";
                    }).catch(function (data) {
                        _this.error = data.message;
                    });
                }
            };
            RenewalController.prototype.DeletePDF = function (index) {
                var customClauseName = this.policyEngine.customClauses[index];
                this.policyEngine.customClauses.splice(index, 1);
                this.quoteStorageService.DeletePdf(this.policyEngine.id, customClauseName);
            };
            RenewalController.prototype.SubmitForm = function (valid) {
                var _this = this;
                if (this.locationService.selectedLocale == null) {
                    this.error = "Please select a location";
                    valid = false;
                }
                if (valid) {
                    this.policyEngine.cover.startDate = this.policyEngine.cover.startDateDt.toDateString();
                    this.policyEngine.cover.endDate = this.policyEngine.cover.endDateDt.toDateString();
                    this.policyEngine.cover.purchasedDate = this.policyEngine.cover.purchasedDateDt.toDateString();
                    this.policyEngine.cover.issuedDate = this.policyEngine.cover.issuedDateDt.toDateString();
                    this.policyEngine.cover.startTime = GetDateTime(this.policyEngine.cover.startTimeDt);
                    this.policyEngine.cover.endTime = GetDateTime(this.policyEngine.cover.endTimeDt);
                    this.policyEngine.cover.purchasedTime = GetDateTime(this.policyEngine.cover.purchasedTimeDt);
                    this.quoteEngine =
                        {
                            clientDetail: this.clientEngine,
                            coverDetail: this.policyEngine.cover,
                            policyDetail: this.policyEngine,
                            localeId: this.locationService.selectedLocale.localePK
                        };
                    this.quoteStorageService.CreateUpdatePolicy(this.quoteEngine).then(function (policyData) {
                        _this.policyData = policyData;
                        _this.quoteRef = policyData.quoteReference;
                        if (_this.policyEngine.quoteRef == null) {
                            _this.policyEngine.pword = policyData.password;
                            _this.policyEngine.quoteRef = policyData.quoteReference;
                        }
                        _this.LoadDefaults();
                        _this.error = "";
                    }).catch(function (data) {
                        _this.error = data.message;
                    });
                }
            };
            RenewalController.prototype.RemoveInstructor = function (index) {
                this.policyEngine.cover.instructors.splice(index, 1);
            };
            RenewalController.prototype.AddInstructor = function () {
                if (this.policyEngine.cover.instructors == null)
                    this.policyEngine.cover.instructors = [];
                this.policyEngine.cover.instructors.push(this.instructorName);
            };
            RenewalController.prototype.AddClause = function () {
                if (this.policyEngine.clauses == null)
                    this.policyEngine.clauses = [];
                this.policyEngine.clauses.push(this.policyEngine.policyClause);
            };
            RenewalController.prototype.RemoveClause = function (index) {
                this.policyEngine.clauses.splice(index, 1);
            };
            RenewalController.prototype.SelectPolicy = function () {
                if (this.policyEngine != null) {
                    this.policyEngine.cover.startDateDt = new Date(this.policyEngine.cover.startDate);
                    this.policyEngine.cover.endDateDt = new Date(this.policyEngine.cover.endDate);
                    this.policyEngine.cover.issuedDateDt = new Date(this.policyEngine.cover.issuedDate);
                    this.policyEngine.cover.purchasedDateDt = new Date(this.policyEngine.cover.purchasedDate);
                    this.policyEngine.cover.endTimeDt = GetJSDateFromTime(this.policyEngine.cover.endTime);
                    this.policyEngine.cover.startTimeDt = GetJSDateFromTime(this.policyEngine.cover.startTime);
                    this.policyEngine.cover.purchasedTimeDt = GetJSDateFromTime(this.policyEngine.cover.purchasedTime);
                }
            };
            RenewalController.prototype.SelectClient = function () {
                this.policyEngine = null;
            };
            RenewalController.prototype.ViewQuote = function () {
                var win = window.open();
                var certRef = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("admin/renewal")) +
                    'newcertificate/' + this.policyEngine.quoteRef + '/' + this.policyEngine.pword;
                win.location.href = certRef;
                $("#cardLabel").css("display", "none");
            };
            RenewalController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/renewal", {
                    controller: "RenewalController",
                    templateUrl: "/App/Admin/Renewal/Renewal.html",
                    controllerAs: "RenewalController",
                    metadata: {
                        title: "Leisure Insure - Renewal"
                    }
                });
                $routeProvider.when("/admin/underwriter/quotes/edit/:QUOTEREFERENCE", {
                    controller: "UnderwriterQuotesController",
                    templateUrl: "/app/admin/underwriter/quotes/edit.html",
                    controllerAs: "quotes",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            };
            RenewalController.$inject = ["$http", "$q", "$scope", "$rootScope", "$window", "QuoteStorageService", "LocationService"];
            return RenewalController;
        }());
        Admin.RenewalController = RenewalController;
        angular.module("LeisureInsure.Admin")
            .controller("RenewalController", RenewalController)
            .config(["$routeProvider", RenewalController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
var uploadedFile;
var uploadFilename;
var filecontentByte;
function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}
function FileSelected(fileuploader) {
    debugger;
    var reader = new FileReader();
    var file = fileuploader.files[0];
    reader.readAsBinaryString(file);
    uploadFilename = file.name;
    reader.onload = function (evt) {
        uploadedFile = evt.target.result;
    };
}
//# sourceMappingURL=RenewalController.js.map