var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var BrokerController = (function () {
            function BrokerController($http, $scope, $window) {
                this.http = $http;
                this.scope = $scope;
                this.scope.alerts = [];
                this.window = $window;
            }
            BrokerController.prototype.addBroker = function () {
                var _this = this;
                if (!this.agreed) {
                    this.scope.alerts.push({
                        type: "danger",
                        msg: "Please read and agree our Terms of Business Agreement before applying"
                    });
                }
                else {
                    this.http.post("api/v1/brokers", {
                        tamCode: this.tamCode,
                        telephone: this.telephone,
                        firstName: this.firstName,
                        surname: this.surname,
                        email: this.email,
                        password: this.password,
                        brokerName: this.brokerName,
                        fsaNumber: this.fsaNumber,
                        address1: this.address1,
                        address2: this.address2,
                        city: this.city,
                        county: this.county,
                        postcode: this.postcode,
                        location: this.location
                    }).then(function () {
                        _this.window.scrollTo(0, 0);
                        _this.scope.alerts.push({
                            type: "success",
                            msg: "Thank you, your application has been received"
                        });
                        _this.tamCode = "";
                        _this.firstName = "";
                        _this.surname = "";
                        _this.telephone = "";
                        _this.email = "";
                        _this.password = "";
                        _this.brokerName = "";
                        _this.fsaNumber = "";
                        _this.address1 = "";
                        _this.address2 = "";
                        _this.city = "";
                        _this.postcode = "";
                        _this.scope.addBroker.$setPristine();
                        _this.scope.addBroker.$setUntouched();
                    }, function (response) {
                        _this.scope.alerts.push({
                            type: "danger",
                            msg: "There was a problem adding your information: " + response.statusText
                        });
                    });
                }
            };
            BrokerController.prototype.addBrokerLive = function () {
                var _this = this;
                this.http.post("api/v1/brokerLive", {
                    tamCode: this.tamCode,
                    telephone: this.telephone,
                    firstName: this.firstName,
                    surname: this.surname,
                    email: this.email,
                    password: this.password,
                    brokerName: this.brokerName,
                    fsaNumber: this.fsaNumber,
                    address1: this.address1,
                    address2: this.address2,
                    city: this.city,
                    county: this.county,
                    postcode: this.postcode,
                    location: this.location,
                    locked: this.locked,
                    contactId: this.contactId
                }).then(function (response) {
                    _this.generatedTamCode = response.data.tamCode;
                    _this.window.scrollTo(0, 0);
                    _this.scope.alerts.push({
                        type: "success",
                        msg: "Broker Added"
                    });
                }, function (response) {
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: "There was a problem adding your information: " + response.statusText
                    });
                });
            };
            BrokerController.prototype.updateBroker = function () {
                var _this = this;
                this.http.post("api/v1/brokers/" + this.tamCode + "/update", {
                    locked: this.locked,
                    entityName: this.brokerName,
                    firstname: this.comments,
                    telephone: this.telephone,
                    email: this.email,
                    fsaNumber: this.fsaNumber,
                    address1: this.address1,
                    address2: this.address2,
                    city: this.city,
                    county: this.county,
                    postcode: this.postcode
                })
                    .then(function () {
                    _this.window.scrollTo(0, 0);
                    _this.scope.alerts.push({
                        type: "success",
                        msg: "Broker updated"
                    });
                }, function (response) {
                    _this.window.scrollTo(0, 0);
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: "There was a problem updating the broker: " + response.statusText
                    });
                });
            };
            BrokerController.prototype.findBroker = function () {
                var _this = this;
                this.http.get("api/v1/brokers/" + this.tamCode)
                    .then(function (response) {
                    _this.window.scrollTo(0, 0);
                    _this.tamCode = response.data.tamCode;
                    _this.brokerName = response.data.entityName;
                    _this.comments = response.data.firstname;
                    _this.telephone = response.data.telephone;
                    _this.email = response.data.email;
                    _this.fsaNumber = response.data.fsaNumber;
                    _this.address1 = response.data.address1;
                    _this.address2 = response.data.address2;
                    _this.city = response.data.city;
                    _this.county = response.data.county,
                        _this.postcode = response.data.postcode;
                    _this.locked = response.data.locked;
                    _this.generatedTamCode = response.data.tamCode;
                }, function (response) {
                    _this.window.scrollTo(0, 0);
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: "Could not find the broker"
                    });
                });
            };
            BrokerController.prototype.clearBroker = function () {
                this.tamCode = "";
                this.firstName = "";
                this.surname = "";
                this.telephone = "";
                this.email = "";
                this.password = "";
                this.brokerName = "";
                this.fsaNumber = "";
                this.address1 = "";
                this.address2 = "";
                this.city = "";
                this.county = "";
                this.postcode = "";
                this.comments = "";
                this.tamCode = "";
                this.generatedTamCode = "";
                this.locked = false;
                this.scope.addBroker.$setPristine();
                this.scope.addBroker.$setUntouched();
            };
            BrokerController.prototype.addBrokerAdmin = function () {
                var _this = this;
                this.http.post("api/v1/brokers/admin", {
                    tamCode: this.tamCode,
                    email: this.email,
                    password: this.password,
                }).then(function () {
                    _this.window.scrollTo();
                    _this.scope.alerts.push({
                        type: "success",
                        msg: "Broker successfully added"
                    });
                    _this.tamCode = "";
                    _this.firstName = "";
                    _this.surname = "";
                    _this.telephone = "";
                    _this.email = "";
                    _this.password = "";
                    _this.brokerName = "";
                    _this.fsaNumber = "";
                    _this.address1 = "";
                    _this.address2 = "";
                    _this.city = "";
                    _this.postcode = "";
                    _this.scope.addBroker.$setPristine();
                    _this.scope.addBroker.$setUntouched();
                }, function (response) {
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: "There was an issue adding the broker: " + response.statusText
                    });
                });
            };
            BrokerController.prototype.closeAlert = function (index) {
                this.scope.alerts.splice(index, 1);
            };
            ;
            BrokerController.routing = function ($routeProvider) {
                $routeProvider.when("/admin/broker/add", {
                    controller: "BrokerController",
                    templateUrl: "/app/admin/broker/add.html",
                    controllerAs: "broker",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
                $routeProvider.when("/admin/broker/review/:BROKERID", {
                    controller: "BrokerController",
                    templateUrl: "/app/admin/broker/review.html",
                    controllerAs: "broker",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
                $routeProvider.when("/broker-application", {
                    controller: "BrokerController",
                    templateUrl: "/app/admin/broker/apply.html",
                    controllerAs: "broker",
                    metadata: {
                        title: "Leisure Insure - New Broker Application"
                    }
                });
                $routeProvider.when("/admin/broker/review", {
                    controller: "BrokerController",
                    templateUrl: "/app/admin/broker/review.html",
                    controllerAs: "broker",
                    metadata: {
                        title: "Leisure Insure - New Broker Application"
                    }
                });
            };
            BrokerController.$inject = ["$http", "$scope", "$window"];
            return BrokerController;
        }());
        Admin.BrokerController = BrokerController;
        angular.module("LeisureInsure.Admin")
            .controller("BrokerController", BrokerController)
            .config(["$routeProvider", BrokerController.routing]);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=BrokerController.js.map