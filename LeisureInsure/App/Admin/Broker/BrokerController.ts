﻿namespace LeisureInsure.Admin {
    export class BrokerController {

        static $inject = ["$http", "$scope", "$window"];
        constructor($http, $scope, $window) {
            this.http = $http;
            this.scope = $scope;
            this.scope.alerts = [];
            this.window = $window;
        }

        http: ng.IHttpService;
        scope: any;
        window: ng.IWindowService;

        tamCode: string;
        firstName: string;
        surname: string;
        telephone: string;
        email: string;
        password: string;
        brokerName: string;
        fsaNumber: string;
        address1: string;
        address2: string;
        city: string;
        county: string;
        postcode: string;
        contactId: number;
        agreed: boolean;
        locked: boolean;
        location: string;
        comments: string;
        loggedIn: string;
        generatedTamCode: string;

        addBroker() {
            if (!this.agreed) {
                this.scope.alerts.push({
                    type: "danger",
                    msg: "Please read and agree our Terms of Business Agreement before applying"
                });
            } else {
                this.http.post(
                    "api/v1/brokers",
                    {
                        tamCode: this.tamCode,
                        telephone: this.telephone,
                        firstName: this.firstName,
                        surname: this.surname,
                        email: this.email,
                        password: this.password,
                        brokerName: this.brokerName,
                        fsaNumber: this.fsaNumber,
                        address1: this.address1,
                        address2: this.address2,
                        city: this.city,
                        county:this.county,
                        postcode: this.postcode,
                        location: this.location
                    }).then(() => {
                        this.window.scrollTo(0, 0);
                        this.scope.alerts.push({
                            type: "success",
                            msg: "Thank you, your application has been received"
                        });
                        this.tamCode = "";
                        this.firstName = "";
                        this.surname = "";
                        this.telephone = "";
                        this.email = "";
                        this.password = "";
                        this.brokerName = "";
                        this.fsaNumber = "";
                        this.address1 = "";
                        this.address2 = "";
                        this.city = "";
                        this.postcode = "";
                        this.scope.addBroker.$setPristine();
                        this.scope.addBroker.$setUntouched();
                    }, response => {
                        this.scope.alerts.push({
                            type: "danger",
                            msg: `There was a problem adding your information: ${response.statusText}`
                        });
                    });
            }
        }


        addBrokerLive() {
            this.http.post(
                "api/v1/brokerLive",
                {
                    tamCode: this.tamCode,
                    telephone: this.telephone,
                    firstName: this.firstName,
                    surname: this.surname,
                    email: this.email,
                    password: this.password,
                    brokerName: this.brokerName,
                    fsaNumber: this.fsaNumber,
                    address1: this.address1,
                    address2: this.address2,
                    city: this.city,
                    county: this.county,
                    postcode: this.postcode,
                    location: this.location,
                    locked: this.locked,
                    contactId: this.contactId
                }).then((response: any) => {
                    this.generatedTamCode = response.data.tamCode;
                    this.window.scrollTo(0, 0);
                    this.scope.alerts.push({
                        type: "success",
                        msg: "Broker Added"
                    });
                }, response => {
                    this.scope.alerts.push({
                        type: "danger",
                        msg: `There was a problem adding your information: ${response.statusText}`
                    });
                });
        }
        

        updateBroker() {
            this.http.post(
                `api/v1/brokers/${this.tamCode}/update`,
                {
                    locked: this.locked,
                    entityName: this.brokerName,
                    firstname: this.comments,
                    telephone: this.telephone,
                    email: this.email,
                    fsaNumber: this.fsaNumber,
                    address1: this.address1,
                    address2: this.address2,
                    city: this.city,
                    county: this.county,
                    postcode: this.postcode
                })
                .then(() => {
                    this.window.scrollTo(0, 0);
                    this.scope.alerts.push({
                        type: "success",
                        msg: "Broker updated"
                    });
                },
                response => {
                    this.window.scrollTo(0, 0);
                    this.scope.alerts.push({
                        type: "danger",
                        msg: `There was a problem updating the broker: ${response.statusText}`
                    });
                });
        }

        findBroker() {
            //alert(this.loggedIn);
            this.http.get(
                `api/v1/brokers/${this.tamCode}`
            )
                .then((response: any) => {
                    this.window.scrollTo(0, 0);
                    this.tamCode = response.data.tamCode;
                    this.brokerName = response.data.entityName;
                    this.comments = response.data.firstname;
                    this.telephone = response.data.telephone;
                    this.email = response.data.email;
                    this.fsaNumber = response.data.fsaNumber;
                    this.address1 = response.data.address1;
                    this.address2 = response.data.address2;
                    this.city = response.data.city;
                    this.county = response.data.county,
                    this.postcode = response.data.postcode;
                    this.locked = response.data.locked;
                    this.generatedTamCode = response.data.tamCode;
                },
                response => {
                    this.window.scrollTo(0, 0);
                    this.scope.alerts.push({
                        type: "danger",
                        msg: "Could not find the broker"
                    });
                });
        }

        clearBroker() {
            this.tamCode = "";
            this.firstName = "";
            this.surname = "";
            this.telephone = "";
            this.email = "";
            this.password = "";
            this.brokerName = "";
            this.fsaNumber = "";
            this.address1 = "";
            this.address2 = "";
            this.city = "";
            this.county = "";
            this.postcode = "";
            this.comments = "";
            this.tamCode = "";
            this.generatedTamCode = "";
            this.locked = false;
            this.scope.addBroker.$setPristine();
            this.scope.addBroker.$setUntouched();
        }


        addBrokerAdmin() {
            this.http.post(
                "api/v1/brokers/admin",
                {
                    tamCode: this.tamCode,
                    email: this.email,
                    password: this.password,
                }).then(() => {
                    this.window.scrollTo();
                    this.scope.alerts.push({
                        type: "success",
                        msg: "Broker successfully added"
                    });
                    this.tamCode = "";
                    this.firstName = "";
                    this.surname = "";
                    this.telephone = "";
                    this.email = "";
                    this.password = "";
                    this.brokerName = "";
                    this.fsaNumber = "";
                    this.address1 = "";
                    this.address2 = "";
                    this.city = "";
                    this.postcode = "";
                    this.scope.addBroker.$setPristine();
                    this.scope.addBroker.$setUntouched();
                }, response => {
                    this.scope.alerts.push({
                        type: "danger",
                        msg: `There was an issue adding the broker: ${response.statusText}`
                    });
                });
        }

        closeAlert(index) {
            this.scope.alerts.splice(index, 1);
        };

        static routing($routeProvider) {
            $routeProvider.when("/admin/broker/add",
                {
                    controller: "BrokerController",
                    templateUrl: "/app/admin/broker/add.html",
                    controllerAs: "broker",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            $routeProvider.when("/admin/broker/review/:BROKERID",
                {
                    controller: "BrokerController",
                    templateUrl: "/app/admin/broker/review.html",
                    controllerAs: "broker",
                    metadata: {
                        title: "Leisure Insure - Admin"
                    }
                });
            $routeProvider.when("/broker-application",
                {
                    controller: "BrokerController",
                    templateUrl: "/app/admin/broker/apply.html",
                    controllerAs: "broker",
                    metadata: {
                        title: "Leisure Insure - New Broker Application"
                    }
                });
            $routeProvider.when("/admin/broker/review",
                {
                    controller: "BrokerController",
                    templateUrl: "/app/admin/broker/review.html",
                    controllerAs: "broker",
                    metadata: {
                        title: "Leisure Insure - New Broker Application"
                    }
                });
        }
    }
    angular.module("LeisureInsure.Admin")
        .controller("BrokerController", BrokerController)
        .config(["$routeProvider", BrokerController.routing]);
}