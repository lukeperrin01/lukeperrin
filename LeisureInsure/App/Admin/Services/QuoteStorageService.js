var LeisureInsure;
(function (LeisureInsure) {
    var Admin;
    (function (Admin) {
        var QuoteStorageService = (function () {
            function QuoteStorageService($http, $q, $rootScope) {
                var _this = this;
                this.http = $http;
                this.q = $q;
                this.rootScope = $rootScope;
                this.http.get("api/v1/admin/documents")
                    .then(function (response) {
                    _this.documents = angular.copy(response.data);
                });
                $("#allGifs").css("display", "block");
            }
            QuoteStorageService.prototype.getWebErrors = function () {
                var defer = this.q.defer();
                this.http({
                    url: "/api/v1/admin/GetWebErrors",
                    method: "GET"
                })
                    .success(function (response) {
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.addToTam = function (quoteId) {
                var defer = this.q.defer();
                this.http({
                    url: "/api/v1/admin/quote/addToTam/" + quoteId,
                    data: {},
                    method: "POST"
                })
                    .success(function (response) {
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.addNote = function (note, quoteId) {
                var _this = this;
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quote/addNote/" + quote.quoteReference,
                    data: {
                        passcode: quote.password,
                        note: note.note,
                        author: note.author,
                        title: note.title
                    },
                    method: "POST"
                })
                    .success(function (response) {
                    var quotePosition = _this.quotes.indexOf(quote);
                    _this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.addEndorsement = function (endorsement, quoteId) {
                var _this = this;
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quote/addendorsement/" + quote.quoteReference,
                    data: {
                        passcode: quote.password,
                        endorsementText: endorsement
                    },
                    method: "POST"
                })
                    .success(function (response) {
                    var quotePosition = _this.quotes.indexOf(quote);
                    _this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.removeEndorsement = function (endorsementId, quoteId) {
                var _this = this;
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quote/deleteEndorsement/" + quote.quoteReference,
                    data: {
                        passcode: quote.password,
                        endorsementId: endorsementId
                    },
                    method: "POST"
                })
                    .success(function (response) {
                    var quotePosition = _this.quotes.indexOf(quote);
                    _this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.updateEndorsement = function (quoteId, endorsementText, endorsementId) {
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quote/updateEndorsement/" + quote.quoteReference,
                    data: {
                        passcode: quote.password,
                        endorsementId: endorsementId,
                        endorsementText: endorsementText
                    },
                    method: "POST"
                })
                    .success(function (response) {
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.removeDocument = function (documentId, quoteId) {
                var _this = this;
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quote/deleteDocument/" + quote.quoteReference,
                    data: {
                        passcode: quote.password,
                        documentId: documentId
                    },
                    method: "POST"
                })
                    .success(function (response) {
                    var quotePosition = _this.quotes.indexOf(quote);
                    _this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.addDocument = function (documentId, quoteId) {
                var _this = this;
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quote/adddocument/" + quote.quoteReference,
                    data: {
                        passcode: quote.password,
                        documentId: documentId
                    },
                    method: "POST"
                })
                    .success(function (response) {
                    var quotePosition = _this.quotes.indexOf(quote);
                    _this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.getPolicyDocuments = function (quoteId) {
                var _this = this;
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quote/getPolicyDocuments/" + quote.quoteReference,
                    data: {
                        passcode: quote.password
                    },
                    method: "POST"
                })
                    .success(function (response) {
                    var quotePosition = _this.quotes.indexOf(quote);
                    _this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.emailQuote = function (quoteId) {
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quotes/emailcustomer/" + quote.quoteReference,
                    data: {
                        passcode: quote.password
                    },
                    method: "POST"
                })
                    .success(function () {
                    defer.resolve();
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.emailNewBusinessQuote = function (quoteId) {
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quotes/emailnewbusiness/" + quote.quoteReference,
                    data: {
                        passcode: quote.password
                    },
                    method: "POST"
                })
                    .success(function () {
                    defer.resolve();
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.emailRenewal = function (quoteId) {
                var defer = this.q.defer();
                var quote = _.findWhere(this.quotes, { id: quoteId });
                this.http({
                    url: "/api/v1/admin/quotes/emailcustomerRenewal/" + quote.quoteReference,
                    data: {
                        passcode: quote.password
                    },
                    method: "POST"
                })
                    .success(function () {
                    defer.resolve();
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.searchQuotes = function (data) {
                var _this = this;
                var defer = this.q.defer();
                this.rootScope.searchingQuote = true;
                this.http({
                    headers: { "Authorization": "Bearer " + window.localStorage["jwtToken"] },
                    url: "/api/v1/admin/search",
                    data: data,
                    method: "POST"
                })
                    .success(function (response) {
                    _this.quotes = angular.copy(response);
                    defer.resolve(_this.quotes);
                    _this.rootScope.searchingQuote = false;
                })
                    .catch(function (data) {
                    defer.reject();
                    _this.rootScope.searchingQuote = false;
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.updateQuote = function (quote) {
                var _this = this;
                var defer = this.q.defer();
                this.http({
                    url: "/api/v1/admin/quote/update/",
                    data: quote,
                    method: "POST"
                })
                    .success(function (response) {
                    var quotePosition = _this.quotes.indexOf(quote);
                    _this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    defer.reject();
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.EmailQuote = function (quoteRef, password, testAddress) {
                var _this = this;
                var defer = this.q.defer();
                this.rootScope.loading = true;
                this.http({
                    url: "api/quoteEngine/emailQuote?quoteRef=" + quoteRef + "&password=" + password + "&testAddress=" + testAddress,
                    method: "POST"
                })
                    .success(function (response) {
                    _this.rootScope.loading = false;
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    _this.rootScope.loading = false;
                    defer.reject(reason.data);
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.SavePdf = function (file, quoteId, filename) {
                var _this = this;
                var defer = this.q.defer();
                this.rootScope.loading = true;
                var input = btoa(file);
                this.http({
                    url: "api/quoteEngine/saveFile?quoteId=" + quoteId + "&filename=" + filename,
                    method: "POST",
                    data: input
                })
                    .success(function (response) {
                    _this.rootScope.loading = false;
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    _this.rootScope.loading = false;
                    defer.reject(reason.data);
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.DeletePdf = function (quoteId, filename) {
                var _this = this;
                var defer = this.q.defer();
                this.rootScope.loading = true;
                this.http({
                    url: "api/quoteEngine/deleteFile?quoteId=" + quoteId + "&filename=" + filename,
                    method: "DELETE"
                })
                    .success(function (response) {
                    _this.rootScope.loading = false;
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    _this.rootScope.loading = false;
                    defer.reject(reason.data);
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.CreateUpdatePolicy = function (policy) {
                var _this = this;
                var defer = this.q.defer();
                var json = JSON.stringify(policy);
                this.rootScope.loading = true;
                this.http({
                    url: 'api/quoteEngine/createPolicy',
                    method: "POST",
                    data: json
                })
                    .success(function (response) {
                    _this.rootScope.loading = false;
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    _this.rootScope.loading = false;
                    defer.reject(reason.data);
                });
                return defer.promise;
            };
            QuoteStorageService.prototype.GetClients = function (localeid) {
                var _this = this;
                var defer = this.q.defer();
                this.rootScope.loading = true;
                this.http({
                    url: 'api/quoteEngine/getClients?localeId=' + localeid,
                    method: "GET"
                })
                    .success(function (response) {
                    _this.rootScope.loading = false;
                    defer.resolve(response);
                })
                    .catch(function (reason) {
                    _this.rootScope.loading = false;
                    defer.reject(reason.data);
                });
                return defer.promise;
            };
            QuoteStorageService.$inject = ["$http", "$q", "$rootScope"];
            return QuoteStorageService;
        }());
        Admin.QuoteStorageService = QuoteStorageService;
        angular.module("LeisureInsure.Admin").service("QuoteStorageService", QuoteStorageService);
    })(Admin = LeisureInsure.Admin || (LeisureInsure.Admin = {}));
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=QuoteStorageService.js.map