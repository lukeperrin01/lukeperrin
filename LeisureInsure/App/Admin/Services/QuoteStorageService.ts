﻿namespace LeisureInsure.Admin {
    export class QuoteStorageService {

        static $inject = ["$http", "$q", "$rootScope"];
        http: ng.IHttpService;
        q: ng.IQService;
        rootScope: any;

        constructor($http, $q, $rootScope) {
            this.http = $http;
            this.q = $q;
            this.rootScope = $rootScope;
            this.http.get("api/v1/admin/documents")
                .then((response: any) => {
                    this.documents = angular.copy(response.data);
                });

            $("#allGifs").css("display", "block");
        }

        quotes: Array<IQuote>;
        documents: Array<IDocument>;
        quotesLoaded: boolean;
        renewalList: Array<IRenewalListItem>;

        getWebErrors(): ng.IPromise<Array<IWebErrors>> {

            var defer = this.q.defer<Array<IWebErrors>>();

            this.http({
                url: `/api/v1/admin/GetWebErrors`,
                method: "GET"
            })
                .success((response: Array<IWebErrors>) => {
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        addToTam(quoteId: number): ng.IPromise<IQuote> {
            var defer = this.q.defer<IQuote>();
            this.http({
                url: `/api/v1/admin/quote/addToTam/${quoteId}`,
                data: {},
                method: "POST"
            })
                .success((response: IQuote) => {
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        addNote(note: IQuoteNote, quoteId: number): ng.IPromise<IQuote> {
            var defer = this.q.defer<IQuote>();
            var quote = _.findWhere(this.quotes, { id: quoteId });
            this.http({
                url: `/api/v1/admin/quote/addNote/${quote.quoteReference}`,
                data: {
                    passcode: quote.password,
                    note: note.note,
                    author: note.author,
                    title: note.title

                },
                method: "POST"
            })
                .success((response: IQuote) => {
                    var quotePosition = this.quotes.indexOf(quote);
                    this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        addEndorsement(endorsement: string, quoteId: number): ng.IPromise<IQuote> {
            var defer = this.q.defer<IQuote>();
            var quote = _.findWhere(this.quotes, { id: quoteId });
            this.http({
                url: `/api/v1/admin/quote/addendorsement/${quote.quoteReference}`,
                data: {
                    passcode: quote.password,
                    endorsementText: endorsement
                },
                method: "POST"
            })
                .success((response: IQuote) => {
                    var quotePosition = this.quotes.indexOf(quote);
                    this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        removeEndorsement(endorsementId: number, quoteId: number): ng.IPromise<IQuote> {

            var defer = this.q.defer<IQuote>();
            var quote = _.findWhere(this.quotes, { id: quoteId });
            this.http({
                url: `/api/v1/admin/quote/deleteEndorsement/${quote.quoteReference}`,
                data: {
                    passcode: quote.password,
                    endorsementId: endorsementId
                },
                method: "POST"
            })
                .success((response: IQuote) => {
                    var quotePosition = this.quotes.indexOf(quote);
                    this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        updateEndorsement(quoteId: number, endorsementText: string, endorsementId: number): ng.IPromise<IQuote> {

            var defer = this.q.defer<IQuote>();
            var quote = _.findWhere(this.quotes, { id: quoteId });
            this.http({
                url: `/api/v1/admin/quote/updateEndorsement/${quote.quoteReference}`,
                data: {
                    passcode: quote.password,
                    endorsementId: endorsementId,
                    endorsementText: endorsementText
                },
                method: "POST"
            })
                .success((response: IQuote) => {
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        removeDocument(documentId: number, quoteId: number): ng.IPromise<IQuote> {
            var defer = this.q.defer<IQuote>();
            var quote = _.findWhere(this.quotes, { id: quoteId });
            this.http({
                url: `/api/v1/admin/quote/deleteDocument/${quote.quoteReference}`,
                data: {
                    passcode: quote.password,
                    documentId: documentId
                },
                method: "POST"
            })
                .success((response: IQuote) => {
                    var quotePosition = this.quotes.indexOf(quote);
                    this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        addDocument(documentId: number, quoteId: number): ng.IPromise<IQuote> {
            var defer = this.q.defer<IQuote>();
            var quote = _.findWhere(this.quotes, { id: quoteId });

            this.http({
                url: `/api/v1/admin/quote/adddocument/${quote.quoteReference}`,
                data: {
                    passcode: quote.password,
                    documentId: documentId
                },
                method: "POST"
            })
                .success((response: IQuote) => {
                    var quotePosition = this.quotes.indexOf(quote);
                    this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();

                });
            return defer.promise;
        }

        getPolicyDocuments(quoteId: number): ng.IPromise<IQuote> {
            var defer = this.q.defer<IQuote>();
            var quote = _.findWhere(this.quotes, { id: quoteId });

            this.http({
                url: `/api/v1/admin/quote/getPolicyDocuments/${quote.quoteReference}`,
                data: {
                    passcode: quote.password
                },
                method: "POST"
            })
                .success((response: IQuote) => {
                    var quotePosition = this.quotes.indexOf(quote);
                    this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();

                });
            return defer.promise;
        }

        emailQuote(quoteId: number): ng.IPromise<any> {
            var defer = this.q.defer();
            var quote = _.findWhere(this.quotes, { id: quoteId });
            this.http({
                url: `/api/v1/admin/quotes/emailcustomer/${quote.quoteReference}`,
                data: {
                    passcode: quote.password
                },
                method: "POST"
            })
                .success(() => {
                    defer.resolve();
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        emailNewBusinessQuote(quoteId: number): ng.IPromise<any> {
            var defer = this.q.defer();
            var quote = _.findWhere(this.quotes, { id: quoteId });
            this.http({
                url: `/api/v1/admin/quotes/emailnewbusiness/${quote.quoteReference}`,
                data: {
                    passcode: quote.password
                },
                method: "POST"
            })
                .success(() => {
                    defer.resolve();
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        emailRenewal(quoteId: number): ng.IPromise<any> {
            var defer = this.q.defer();
            var quote = _.findWhere(this.quotes, { id: quoteId });
            this.http({
                url: `/api/v1/admin/quotes/emailcustomerRenewal/${quote.quoteReference}`,
                data: {
                    passcode: quote.password
                },
                method: "POST"
            })
                .success(() => {
                    defer.resolve();
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }

        searchQuotes(data): ng.IPromise<Array<IQuote>> {
            var defer = this.q.defer<Array<IQuote>>();
            this.rootScope.searchingQuote = true;
            this.http({
                headers: { "Authorization": "Bearer " + window.localStorage["jwtToken"] },
                url: "/api/v1/admin/search",
                data: data,
                method: "POST"
            })
                .success((response: Array<IQuote>) => {
                    this.quotes = angular.copy(response);
                    defer.resolve(this.quotes);
                    this.rootScope.searchingQuote = false;
                })
                .catch((data: any) => {
                    defer.reject();
                    this.rootScope.searchingQuote = false;
                    eval("ShowUIDialog('An error has occured and been logged.<br> Please retain your reference: " + data.data.message + "');");
                });
            return defer.promise;
        }

        updateQuote(quote: IQuote): ng.IPromise<IQuote> {
            var defer = this.q.defer<IQuote>();
            this.http({
                url: "/api/v1/admin/quote/update/",
                data: quote,
                method: "POST"
            })
                .success((response: IQuote) => {
                    var quotePosition = this.quotes.indexOf(quote);
                    this.quotes.splice(quotePosition, 1, response);
                    defer.resolve(response);
                })
                .catch(reason => {
                    defer.reject();
                });
            return defer.promise;
        }       

        //QuoteEngine --------------------------------------------------------------------------------- 

        EmailQuote(quoteRef: string, password: string,testAddress:string): ng.IPromise<string> {

            var defer = this.q.defer<string>();
            this.rootScope.loading = true;           

            this.http({
                url: "api/quoteEngine/emailQuote?quoteRef=" + quoteRef + "&password=" + password + "&testAddress="+testAddress,
                method: "POST"                
            })
                .success((response: string) => {
                    this.rootScope.loading = false;
                    defer.resolve(response);
                })
                .catch((reason: any) => {
                    this.rootScope.loading = false;
                    defer.reject(reason.data);
                });

            return defer.promise;
        }


        SavePdf(file: any, quoteId: number, filename: string): ng.IPromise<IPolicyView> {

            var defer = this.q.defer<IPolicyView>();          
            this.rootScope.loading = true;            

            var input = btoa(file);

            this.http({
                url: "api/quoteEngine/saveFile?quoteId="+quoteId+"&filename="+filename,
                method: "POST",
                data: input
            })
                .success((response: IPolicyView) => {
                    this.rootScope.loading = false;
                    defer.resolve(response);
                })
                .catch((reason: any) => {
                    this.rootScope.loading = false;
                    defer.reject(reason.data);
                });

            return defer.promise;
        }

        DeletePdf(quoteId: number, filename: string): ng.IPromise<IPolicyView> {

            var defer = this.q.defer<IPolicyView>();

            this.rootScope.loading = true;
            this.http({
                    url: "api/quoteEngine/deleteFile?quoteId=" + quoteId + "&filename=" + filename,
                    method: "DELETE"
                })
                .success((response: IPolicyView) => {
                    this.rootScope.loading = false;
                    defer.resolve(response);
                })
                .catch((reason: any) => {
                    this.rootScope.loading = false;
                    defer.reject(reason.data);
                });

            return defer.promise;
        }

        CreateUpdatePolicy(policy: IQuoteEngine): ng.IPromise<IPolicyView> {

            var defer = this.q.defer<IPolicyView>();

            var json = JSON.stringify(policy);
            this.rootScope.loading = true;           

            this.http({
                url: 'api/quoteEngine/createPolicy',
                method: "POST",
                data: json                                                           
            })
                .success((response: IPolicyView) => {
                    this.rootScope.loading = false;
                    defer.resolve(response);                    
                })
                .catch((reason:any) => {
                    this.rootScope.loading = false;                    
                    defer.reject(reason.data);
                });

            return defer.promise;
        }

        GetClients(localeid: number): ng.IPromise<IDefaultEngine> {

            var defer = this.q.defer<IDefaultEngine>();
           
            this.rootScope.loading = true;

            this.http({
                url: 'api/quoteEngine/getClients?localeId='+localeid,               
                method: "GET"                
            })
                .success((response: IDefaultEngine) => {
                    this.rootScope.loading = false;
                    defer.resolve(response);
                })
                .catch((reason: any) => {
                    this.rootScope.loading = false;
                    defer.reject(reason.data);
                });

            return defer.promise;
        }
    }
    angular.module("LeisureInsure.Admin").service("QuoteStorageService", QuoteStorageService);
}
