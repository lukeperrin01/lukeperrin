﻿namespace LeisureInsure.Admin {
    export interface IWebErrors {
        cause: string;
        ErrorLogPK: number;
        QuoteReference: string;
        url: string;
        Msg: string;
        StackTrace: string;
        Cause: string;
        Reported: Date;
    }
}