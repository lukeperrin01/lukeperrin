﻿namespace LeisureInsure.Admin {
    export interface IQuote {
        quoteReference: string;
        tamClientCode: string;
        brokerName: string;
        primaryCustomer: ICustomer;
        answers: Array<IAnswer>;
        chargeSummaryItems: Array<IChargeSummaryItem>;
        notes:Array<IQuoteNote>
        documents: Array<IDocument>;
        tamBrokerCode: string;
        netPremium: number;
        primaryAddress: IAddress;
        addresses: Array<IAddress>;
        customers: Array<ICustomer>;
        id: number;
        password: string;
        referralType?: number;
        localeId: number;
        policyId: number;
        rateTypeFK: number;
        businessDescription: string;
        dateFrom: string;
        dateTo: string;
        renewal: number;
        isAgent: boolean;
        taxRate: number;
        tax: number;
        legalFees: number;
        commission: number;
        commissionPercentage: number;
        legalFeesAdded: number;
        total: number;
        leisureInsureFees: number;
        noInstructors: number;
        contactEmail: string;
        contactName: string;
    }
}