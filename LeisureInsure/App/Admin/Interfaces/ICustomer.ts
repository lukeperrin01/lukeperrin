﻿namespace LeisureInsure.Admin {
    export interface ICustomer {
        email: string;
        entityName: string;
        entityType: string;
    }
}