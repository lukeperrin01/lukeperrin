﻿module LeisureInsure.Admin {
    export interface IQuoteNote {
        quoteId: number;
        note: string;
        author: string;
        dateLogged: Date;
        title: string;
    }
}