﻿"use strict";

module LeisureInsure.Admin {
    export interface IPolicy {
        id: number;
        name: string;
        bitwiseId: number;
    }
}

