﻿

module LeisureInsure.Admin {
    export interface ILandingPageDefinition {
        id: number;
        url: string;
        title: string;
        description: string;
        keywords: string;
        pictureUrl: string;
        landingPageDescription: string;       
        policyId: number;
        newPolicyId: number;
        //for new landing page ***
        header1: string;
        paragraph1: string;
        buynow1: string;
        chooseUs1: string;
        chooseUs2: string;
        chooseUs3: string;
        chooseUs4: string;
        header2: string;
        paragraph2: string;      
        
    }
}