﻿namespace LeisureInsure.Admin {
    export interface IAddress {
        postcode: string;
    }
}