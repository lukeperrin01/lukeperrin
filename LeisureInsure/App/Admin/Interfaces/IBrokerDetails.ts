﻿namespace LeisureInsure.Admin {
    export interface IBrokerDetail {
        brokerCode: string;
        brokerName: string;
        firstname: string;
        lastname: string;
        entityName: string;
        telephone: string;
        email: string;
        fsaNumber: string;
        address1: string;
        address2: string;
        city: string;
        county: string;
        postcode: string;
    }
}