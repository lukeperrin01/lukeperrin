﻿"use strict";

module LeisureInsure.Admin {
    export interface ICoverSections {
        sectionName: string;
        sectionNo: number;
        sectionOrdinal: number;
        indemnity: number;
        sumInsured: number
        netPremium: number;
        covers: Array<IAnswer>;
        listVis: number;
        coverCount: number;
    }
}