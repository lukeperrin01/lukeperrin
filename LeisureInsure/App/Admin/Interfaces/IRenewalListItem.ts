﻿namespace LeisureInsure.Admin {
    export interface IRenewalListItem {
        TamPolicyRef: string;
        TamQuoteNo: string;
        ClientName: string;
        ExpiryDate: Date;
        QuoteRef: string;
    }
}