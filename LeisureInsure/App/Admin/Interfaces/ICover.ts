﻿"use strict";

module LeisureInsure.Admin {
    export interface ICover {
        id: number;
        name: string;
        description: string;
        bitwiseId: number;
    }
}