﻿namespace LeisureInsure.Admin {
    export interface IBrokerQuote {
        quotePK: number;
        quoteRef: string;
        dateCreated: Date;
        brokerContact: string;
        customerFK: number;
    }

}