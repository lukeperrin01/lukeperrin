﻿

module LeisureInsure.Admin {
    export interface INavigationData {
        inputId: number;
        rateId: number;
    }
}