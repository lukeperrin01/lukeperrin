﻿namespace LeisureInsure.Admin {
    export interface IBroker {
        id: number;
        tamClientRef: string;
    }
}