﻿namespace LeisureInsure.Admin {
    export interface IRenewalList {
        renewalListItems: Array<IRenewalListItem>;
    }
}