﻿namespace LeisureInsure.Admin {
    export interface IDocument {
        id: number;
        name: string;
        link: string;
        type: string;
    }
}