var LeisureInsure;
(function (LeisureInsure) {
    var PaymentController = (function () {
        function PaymentController($q, $http, $scope, $rootScope, $window, $location, $routeParams, chargeService, policyUpdateDataService, locationService, certificateService, $filter, $timeout, PostCodeService, PolicyService) {
            var _this = this;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.certificateService = certificateService;
            this.postCodeService = PostCodeService;
            this.policyService = PolicyService;
            this.quotePk = this.chargeService.quoteId;
            this.password = this.chargeService.password;
            this.quoteReference = this.chargeService.quoteReference;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.q = $q;
            this.creditcards = [];
            this.addressLookups = [];
            this.selectedAddress = [];
            this.scope.alerts = [];
            this.scope.agentAlerts = [];
            this.instructors = 1;
            this.purchase = false;
            var datenow = new Date();
            this.dates = {
                dateFrom: null,
                dateTo: null,
                minDate: "",
                maxDate: "",
                startingHour: "00",
                startingMinute: "00",
                endingHour: "23",
                endingMinute: "59",
                dateToRequired: false,
                startTimeRequired: false,
                endTimeRequired: false,
                minToMaxDays: null,
                coverDays: null,
                minExpDate: "",
                maxExpDate: "",
            };
            this.dayOfMonth = "14";
            this.title = "Mr";
            var cardPromise = $http({
                url: "/api/v1/payment-cards",
                method: "GET"
            })
                .then(function (response) {
                _this.paymentCards = angular.copy(response.data);
                _this.populateCards();
            });
            $http({
                url: "/api/v1/documents/",
                data: {
                    quoteReference: $routeParams["QUOTEREFERENCE"],
                    passcode: $routeParams["PASSCODE"]
                },
                method: "POST"
            }).then(function (response) {
                _this.documents = angular.copy(response.data);
            }, function () {
                _this.scope.alerts.push({
                    type: "danger",
                    msg: "There was a problem loading your quote information. Please call us quoting reference: " + $routeParams["QUOTEREFERENCE"]
                });
            });
            this.certHref = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("checkout")) + 'certificate/' + $routeParams["QUOTEREFERENCE"] + '/' + $routeParams["PASSCODE"];
            if (this.chargeService.chargeSummary) {
                this.chargeSummary = this.chargeService.chargeSummary;
                this.quoteReference = $routeParams["QUOTEREFERENCE"];
                this.password = $routeParams["PASSCODE"];
                this.addresses = angular.copy(this.chargeSummary.addresses);
                this.contactEmail = this.chargeService.chargeSummary.contactEmail;
                this.contactName = this.chargeService.chargeSummary.contactName;
                if (this.chargeSummary.customers) {
                    this.customers = angular.copy(this.chargeSummary.customers);
                }
                this.renewal = chargeService.renewal;
                if (this.chargeSummary.policyId == 15 || this.chargeSummary.policyId == 16) {
                    this.inputInstructors = new Array(this.chargeService.chargeSummary.noInstructors + 1);
                }
                this.quotePk = this.chargeService.quoteId;
                this.dateToDt = new Date(this.chargeSummary.dateTo);
                this.dateFromDt = new Date(this.chargeSummary.dateFrom);
                if (this.chargeService.chargeSummary.paymentOptions === null ||
                    this.chargeService.chargeSummary.paymentOptions.length === 0) {
                    this.rootScope.loadingdocuments = true;
                    this.chargeService.loadQuote($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then(function (data) {
                        _this.chargeSummary = _this.chargeService.chargeSummary;
                        cardPromise.then(function () {
                            _this.selectedPaymentCard = _
                                .findWhere(_this.paymentCards, { id: _this.chargeSummary.paymentCardId });
                            _this.rootScope.loadingdocuments = false;
                        });
                    }).catch(function (data) { this.rootScope.loadingdocuments = false; });
                }
                this.getPolicies(this.chargeSummary.numberOfDays, this.chargeService.chargeSummary.policyType);
            }
            else {
                policyUpdateDataService.initialise("", 0, 0)
                    .then(function (policydata) {
                    if ($routeParams["QUOTEREFERENCE"]) {
                        _this.chargeService.loadQuote($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                            .then(function (data) {
                            _this.chargeSummary = _this.chargeService.chargeSummary;
                            _this.quoteReference = $routeParams["QUOTEREFERENCE"];
                            _this.password = $routeParams["PASSCODE"];
                            var cookie = document.cookie;
                            _this.termsAccepted = data.termsAndConditions;
                            _this.contactEmail = _this.chargeService.chargeSummary.contactEmail;
                            _this.contactName = _this.chargeService.chargeSummary.contactName;
                            var addy = Array();
                            var cust = Array();
                            _this.dates.dateFrom = new Date(data.dateFrom);
                            _this.dates.dateTo = new Date(data.dateTo);
                            _this.dates.startingHour = data.timeFrom.split(":")[0];
                            _this.dates.startingMinute = data.timeFrom.split(":")[1];
                            _this.dates.endingHour = data.timeTo.split(":")[0];
                            _this.dates.endingMinute = data.timeTo.split(":")[1];
                            var results = cookie.split(";");
                            for (var i = 0; i < results.length; i++) {
                                var c = results[i];
                                var d = c.split('=');
                                if (d[0].trim() == "customers") {
                                    cust = JSON.parse(d[1]);
                                }
                                if (d[0].trim() == "addresses") {
                                    addy = JSON.parse(d[1]);
                                }
                            }
                            if (data.addresses.length == 0) {
                                data.addresses = addy;
                            }
                            if (data.customers.length == 0) {
                                data.customers = cust;
                            }
                            _this.addresses = angular.copy(data.addresses);
                            _this.customers = angular.copy(data.customers);
                            _this.documents = angular.copy(data.documents);
                            cardPromise.then(function () {
                                _this.selectedPaymentCard = _
                                    .findWhere(_this.paymentCards, { id: _this.chargeSummary.paymentCardId });
                            });
                            _this.reorder();
                            _this.dateToDt = new Date(_this.chargeSummary.dateTo);
                            _this.dateFromDt = new Date(_this.chargeSummary.dateFrom);
                            if (_this.chargeSummary.policyId == 15 || _this.chargeSummary.policyId == 16) {
                                _this.inputInstructors = new Array(_this.chargeService.chargeSummary.noInstructors + 1);
                            }
                            if (_this.dateFromDt.getTime() < new Date().getTime()) {
                                _this.dateRequiresUpdating = true;
                            }
                            else {
                                _this.dateRequiresUpdating = false;
                            }
                            _this.getPolicies(data.numberOfDays, data.policyType);
                        });
                    }
                });
            }
            this.chargeService.onChargeSummaryChanged($scope, function (chargeSummary) {
                _this.chargeSummary = chargeSummary;
                _this.quoteReference = $routeParams["QUOTEREFERENCE"];
                _this.password = $routeParams["PASSCODE"];
            });
            this.locationService.initialised.then(function (locationService) {
                _this.selectedLocale = locationService.selectedLocale;
                if (_this.locationService != null && _this.locationService.selectedLocale != null) {
                    _this.locationService.selectedLocale.showFlag = false;
                }
            });
            $scope.onEnd = function () {
            };
        }
        PaymentController.prototype.updatePolicyPeriod = function () {
            if (this.dateFromDt >= new Date) {
                this.chargeService.dateFromDt = this.dateFromDt;
                this.chargeService.dateToDt = this.dateToDt;
                this.dateRequiresUpdating = false;
                this.chargeService.updateQuotePeriod();
            }
            else {
                this.dateRequiresUpdating = true;
            }
        };
        PaymentController.prototype.getPolicies = function (numdays, policyType) {
            var _this = this;
            var policyID = this.chargeSummary.policyId;
            this.policyService.GetPolicies(this.chargeSummary.localeId)
                .then(function (data) {
                _this.policyService.selectPolicyById(policyID);
                _this.dates.minDate = _this.policyService.selectedPolicy.minDate;
                _this.dates.maxDate = _this.policyService.selectedPolicy.maxDate;
                var isEventSupplier = false;
                var min = new Date(_this.policyService.selectedPolicy.minDate);
                var max = new Date(_this.policyService.selectedPolicy.maxDate);
                var days = daysBetween(max, min);
                _this.dates.minToMaxDays = days;
                if (policyID == 8) {
                    _this.dates.minToMaxDays = numdays;
                }
                if (policyID == 18) {
                    _this.dates.coverDays = numdays;
                }
                if (policyID == 25) {
                    var cancellation = _.filter(_this.chargeSummary.chargeSummaryItems, function (c) {
                        return c.coverFK == 30 && c.price != null && c.price > 0;
                    });
                    if (cancellation.length > 0) {
                        var dt = new Date(min.setDate(min.getDate() + 14));
                        min = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
                        max = new Date(min.getFullYear() + 1, dt.getMonth(), dt.getDate());
                        var m = "";
                        if (min.getMonth() < 10) {
                            m = '0' + (min.getMonth() + 1).toString();
                        }
                        else {
                            m = (dt.getMonth() + 1).toString();
                        }
                        _this.dates.minDate = min.getFullYear() + '-' + m + '-' + min.getDate();
                        _this.dates.maxDate = max.getFullYear() + '-' + m + '-' + min.getDate();
                        days = daysBetween(max, min);
                        _this.dates.minToMaxDays = days;
                    }
                }
                if (policyID == 6 && policyType == "E") {
                    _this.dates.minToMaxDays = numdays;
                    if (_this.chargeSummary.businessDescription.indexOf("Supplier") != -1) {
                        _this.dates.coverDays = numdays;
                        isEventSupplier = true;
                    }
                }
                if (policyType == "E") {
                    if (policyID == 8 || policyID == 6 && !isEventSupplier)
                        _this.dates.dateToRequired = true;
                    if (policyID == 5 || policyID == 25 || policyID == 6 || policyID == 18)
                        _this.dates.startTimeRequired = true;
                    if (policyID == 6)
                        _this.dates.endTimeRequired = true;
                }
            }, function (data) {
            });
        };
        PaymentController.prototype.TermsClicked = function (form) {
            var display = $("#termsTick").css("display");
            if (display == "none") {
                $("#termsTick").css("display", "block");
                form.termsAndCond.$setValidity('required', true);
                this.termsAccepted = true;
            }
            else {
                $("#termsTick").css("display", "none");
                form.termsAndCond.$setValidity('required', false);
                this.termsAccepted = false;
            }
        };
        PaymentController.prototype.ShowCert = function () {
            var _this = this;
            if (this.dates.dateFrom != null) {
                var win = window.open();
                this.updateEndDate();
                this.chargeService.updateQuoteDates(this.dates)
                    .then(function (data) {
                    win.location.href = _this.certHref;
                }).catch(function (data) { this.rootScope.loadingdocuments = false; });
            }
            else
                eval("ShowUIDialog('Please select a start date before viewing a quote');");
        };
        PaymentController.prototype.setEntityNamePlaceHolder = function () {
            if (this.customers.length) {
                if (this.customers[0].entityType == "TradingAs") {
                    this.entityNamePlaceHolder = "Example: Joe Blogs and Joanna Blogs t/a Blogs Enterprises";
                }
                else {
                    this.entityNamePlaceHolder = "";
                }
            }
        };
        PaymentController.prototype.Debug = function () {
            console.log("paymentcards", this.paymentCards);
            console.log("cards", this.creditcards);
            console.log("selectedPaymentOption", this.selectedPaymentOption);
            console.log("paymentOptions", this.chargeSummary.paymentOptions);
            console.log("chargeSummary", this.chargeSummary);
            console.log("address lookups", this.addressLookups);
            this.scope.startDate = new Date();
        };
        PaymentController.prototype.populateAddress = function () {
            this.addresses[0] = this.selectedAddress;
        };
        PaymentController.prototype.getAddress = function () {
            var _this = this;
            if (this.addresses[0] != null) {
                if (this.addresses[0].postcode != null) {
                    this.postCodeService.GetAddresses(this.addresses[0].postcode)
                        .then(function (addresses) {
                        _this.addressLookups = [];
                        _.each(addresses, function (address) {
                            var addy = {};
                            addy.addressID = address.addressID;
                            addy.address1 = address.addressLine1;
                            addy.address2 = address.addressLine2;
                            addy.town = address.town;
                            addy.county = address.county;
                            addy.postcode = address.postCode;
                            _this.addressLookups.push(addy);
                        });
                        _this.selectedAddress = _this.addressLookups[0];
                        _this.addresses[0] = _this.selectedAddress;
                    });
                }
            }
        };
        PaymentController.prototype.showPaymentOptions = function () {
            if (this.chargeSummary === undefined || this.chargeSummary.paymentOptions === undefined ||
                this.chargeSummary.paymentOptions === null) {
                return false;
            }
            return this.chargeSummary.paymentOptions.length > 1;
        };
        PaymentController.prototype.removeCover = function (cover) {
            var _this = this;
            var coverId = cover.coverFK;
            var button = "removeCoverButton" + coverId;
            this.scope[button] = true;
            this.chargeService.removeCover(coverId)
                .then(function () {
                _this.scope[button] = false;
            }, function () {
                _this.scope[button] = false;
                _this.scope.agentAlerts.push({
                    type: "danger",
                    msg: "There was a problem removing the cover, please try again or give us a call quoting your reference displayed above"
                });
            });
        };
        PaymentController.prototype.completeAsAgent = function () {
            var _this = this;
            if (!this.backDatedStartDate()) {
                this.updateEndDate();
                this.submitCustomer()
                    .then(function () {
                    _this.chargeService.completeAsAgent(_this.dates)
                        .then(function () {
                        _this.scope.payNowButton = false;
                        _this.scope.agentAlerts.push({
                            type: "success",
                            msg: "Policy purchased"
                        });
                    }, function (response) {
                        _this.scope.payNowButton = false;
                        var msgText = "There was a problem submitting the policy: " + response;
                        _this.scope.agentAlerts.push({
                            type: "danger",
                            msg: msgText
                        });
                    });
                }, function () {
                    _this.scope.payNowButton = false;
                    _this.scope.agentAlerts.push({
                        type: "danger",
                        msg: "There was a problem submitting the policy address information, please check the form for errors"
                    });
                });
            }
            else
                eval("ShowUIDialog('Please check start date/time. Cover cannot be backdated');");
        };
        PaymentController.prototype.reorder = function () {
            this.chargeSummary.paymentOptions = _.sortBy(this.chargeSummary.paymentOptions, "id");
        };
        PaymentController.prototype.backDatedStartDate = function () {
            var backdated = false;
            if (this.dates.startTimeRequired) {
                var currentDate = new Date();
                var startTime = this.dates.startingHour;
                var startDate = this.dates.dateFrom;
                startDate.setHours(parseInt(this.dates.startingHour));
                startDate.setMinutes(parseInt(this.dates.startingMinute));
                if (startDate < currentDate)
                    backdated = true;
            }
            return backdated;
        };
        PaymentController.prototype.updateEndDate = function () {
            if (!this.dates.dateToRequired && this.chargeService.chargeSummary.policyType == "E") {
                if (this.dates.coverDays != null) {
                    var newDateTo = addhoursToDate(this.dates.dateFrom, this.dates.coverDays);
                    this.dates.dateTo = newDateTo;
                }
            }
        };
        PaymentController.prototype.completeAsCustomer = function () {
            var _this = this;
            if (!this.backDatedStartDate()) {
                if (!this.validatePaymentOption() || this.selectedPaymentCard === undefined ||
                    this.selectedPaymentCard.id === 0 || this.selectedPaymentOption === undefined ||
                    this.selectedPaymentOption.initialPayment === undefined) {
                    var msgText = "There was a problem with your payment information, please try again";
                    this.scope.agentAlerts.push({
                        type: "danger",
                        msg: msgText
                    });
                    this.scope.payNowButton = false;
                    return;
                }
                this.submitCustomer()
                    .then(function () {
                    _this.chargeService.paymentTypeId = _this.selectedPaymentOption.id;
                    _this.chargeService.selectCard(_this.selectedPaymentCard.id)
                        .then(function () {
                        setTimeout(function () {
                            var numberOfPayments = 1;
                            if (_this.selectedPaymentOption.numberOfPayments) {
                                numberOfPayments = _this.selectedPaymentOption.numberOfPayments;
                            }
                            var form = $("#payment-" + numberOfPayments);
                            _this.updateEndDate();
                            var dateFrom = GetDateTime(_this.dates.dateFrom);
                            var dateTo = dateFrom;
                            if (_this.dates.dateTo != null)
                                dateTo = GetDateTime(_this.dates.dateTo);
                            _this.http.post("api/v1/payments/" + _this.quoteReference, {
                                passcode: _this.password,
                                sortCode: _this.sortCode,
                                account: _this.account,
                                accountType: _this.accountType,
                                title: _this.title,
                                name: _this.name,
                                collectionDate: _this.dayOfMonth,
                                businessName: _this.businessName,
                                termsAndConditions: _this.termsAccepted,
                                numberOfPayments: _this.selectedPaymentOption.numberOfPayments,
                                startDate: dateFrom,
                                endDate: dateTo,
                                timeFrom: _this.dates.startingHour + ":" + _this.dates.startingMinute,
                                timeTo: _this.dates.endingHour + ":" + _this.dates.endingMinute,
                                dateToRequired: _this.dates.dateToRequired,
                                startTimeRequired: _this.dates.startTimeRequired,
                                endTimeRequired: _this.dates.endTimeRequired,
                                numberOfDays: _this.dates.coverDays
                            }).then(function () {
                                form.submit();
                            }, function () {
                                _this.scope.payNowButton = false;
                                var msgText = "There was a problem with your payment information, please try again";
                                _this.scope.agentAlerts.push({
                                    type: "danger",
                                    msg: msgText
                                });
                            });
                        }, 100);
                    }, function (error) {
                        _this.scope.payNowButton = false;
                        _this.scope.agentAlerts.push({
                            type: "danger",
                            msg: error
                        });
                    });
                }, function () {
                    _this.scope.payNowButton = false;
                    _this.scope.agentAlerts.push({
                        type: "danger",
                        msg: "There was a problem submitting the policy address information, please check the form for errors"
                    });
                });
            }
            else
                eval("ShowUIDialog('Please check start date/time. Cover cannot be backdated');");
        };
        PaymentController.prototype.GetInvalidFields = function (frm) {
            alert("submitted");
            var errors = frm.$error.required;
            console.clear();
            angular.forEach(errors, function (field) {
                if (field.$invalid) {
                    var fieldName = field.$name;
                }
            });
        };
        ;
        PaymentController.prototype.payNow = function (formvalid, paymentMethodId) {
            if (formvalid) {
                this.scope.payNowButton = true;
                if (this.chargeSummary.isAgent == true) {
                    this.completeAsAgent();
                    return;
                }
                if (!this.termsAccepted && this.renewal === 0) {
                    this.scope.agentAlerts.push({
                        type: "danger",
                        msg: "Please confirm that you have read the policy documents"
                    });
                    this.scope.payNowButton = false;
                    return;
                }
                if (this.chargeSummary.isAgent) {
                    this.completeAsAgent();
                }
                else {
                    this.completeAsCustomer();
                }
            }
            this.http.post("api/v1/setBuy/" + this.quoteReference + "/1");
        };
        PaymentController.prototype.unlockQuote = function () {
            this.http.post("api/v1/unlockQuote/" + this.quoteReference + "/0");
        };
        PaymentController.prototype.populateCards = function () {
            var cards = Array();
            for (var _i = 0, _a = this.paymentCards; _i < _a.length; _i++) {
                var card = _a[_i];
                var item = {};
                var currencySymbol = this.locationService.selectedLocale.currencySymbol;
                var uk = this.locationService.selectedLocale.localePK === 1;
                var charge = 0;
                if (card.addRate > 0) {
                    if (this.selectedPaymentOption === undefined ||
                        this.selectedPaymentOption.initialPayment === undefined) {
                        item.name = card.name + " (" + card.addRate * 100 + "% card charge)";
                        item.id = card.id;
                        this.creditcards.push(item);
                        continue;
                    }
                    charge = this.selectedPaymentOption.initialPayment * card.addRate;
                    charge = Math.max(1, charge);
                }
                if (uk) {
                    charge += card.addChargeUk;
                }
                else {
                    charge += card.addChargeIe;
                }
                charge = Math.round(charge * 100) / 100;
                item.id = card.id;
                item.name = card.name + " (" + currencySymbol + charge + " card charge)";
                this.creditcards.push(item);
            }
        };
        PaymentController.prototype.validatePaymentOption = function () {
            if (this.selectedPaymentOption.numberOfPayments > 1) {
                if (this.sortCode === "" || this.account === "") {
                    return false;
                }
                if (!this.accountConfirm || !this.accountHolder) {
                    return false;
                }
                if (this.accountType === "B") {
                    if (this.businessName === "") {
                        return false;
                    }
                }
                else {
                    if (this.name === "") {
                        return false;
                    }
                }
            }
            return true;
        };
        PaymentController.prototype.sendReferral = function () {
            var _this = this;
            var url = "http://" + this.window.location.host + "/";
            this.window.location.href = url;
            this.scope.sendRefferalButton = true;
            if (this.clientName == null || this.clientName == "") {
                this.clientName = this.name;
            }
            this.http.post("api/v1/payment/referralcallbacks", {
                quoteReference: this.quoteReference,
                name: this.name,
                email: this.email,
                phone: this.phone,
                clientName: this.clientName
            }).then(function () {
                _this.scope.sendRefferalButton = false;
                _this.scope.alerts.push({
                    type: "success",
                    msg: "We have received your information and will look at your policy and be in touch ASAP"
                });
            }, function (response) {
                _this.scope.sendRefferalButton = false;
                _this.scope.alerts.push({
                    type: "danger",
                    msg: response.statusText
                });
            });
        };
        PaymentController.prototype.closeAlert = function (index, array) {
            this.scope[array].splice(index, 1);
        };
        ;
        PaymentController.prototype.toggleLegalCare = function () {
            var _this = this;
            this.chargeService.toggleLegalCare()
                .then(function (chargeSummary) {
                _this.chargeSummary = chargeSummary;
                _this.documents = chargeSummary.documents;
                _this.reorder();
            });
        };
        PaymentController.prototype.addExcessWaiver = function () {
            var _this = this;
            this.chargeService.toggleExcessWaiver()
                .then(function (chargeSummary) {
                _this.chargeSummary = chargeSummary;
                _this.reorder();
            });
        };
        PaymentController.prototype.selectCard = function () {
            var _this = this;
            this.chargeService.updatePaymentCard(this.selectedPaymentCard.id)
                .then(function (chargeSummary) {
                _this.chargeSummary = chargeSummary;
                _this.reorder();
            });
        };
        PaymentController.prototype.submitCustomer = function () {
            var _this = this;
            var deferred = this.q.defer();
            var qr = this.quoteReference;
            this.quotePk = parseInt(qr.substring(qr.length - 6, qr.length));
            var addx = [];
            var custx = [];
            this.customers[0].contactType = 1;
            custx.push(this.customers[0]);
            if (this.customers[0].entityType != "Customer" &&
                (this.customers[0].entityName === undefined || this.customers[0].entityName === "")) {
                eval("ShowUIDialog('Please ensure that the entity name is completed');");
                deferred.reject("Please ensure that the entity name is completed");
            }
            else {
                var i = 0;
                _.each(this.customers, function (cust) {
                    if (i == 0 && _this.contactEmail == '') {
                        _this.contactEmail = cust.email;
                    }
                    if (i == 0 && _this.contactName == '') {
                        _this.contactName = cust.firstName;
                    }
                    if (i > 0) {
                        cust.contactType = 1;
                        cust.quotePK = _this.customers[0].quotePK;
                        cust.entityType = "Customer";
                        custx.push(cust);
                    }
                    i++;
                    _this.instructors = i;
                });
                if (this.addresses.length > 1) {
                }
                this.addresses[0].quoteFK = this.quotePk;
                this.addresses[0].addressType = 1;
                this.addresses[0].locale = this.selectedLocale.strLocale;
                addx.push(this.addresses[0]);
                this.contactDetail = {
                    contact: custx,
                    address: addx,
                    quoteReference: this.quoteReference,
                    passCode: this.password,
                    contactName: this.contactName,
                    contactEmail: this.contactEmail
                };
                this.chargeService.addCustomerDetails(this.contactDetail)
                    .then(function () {
                    deferred.resolve();
                }, function (response) {
                    _this.scope.alerts.push({
                        type: "danger",
                        msg: response
                    });
                    deferred.reject(response);
                });
            }
            document.cookie = "customers=" + JSON.stringify(this.customers);
            document.cookie = "addresses=" + JSON.stringify(this.addresses);
            var x = document.cookie;
            return deferred.promise;
        };
        PaymentController.prototype.customerLoadTest = function () {
            var contact = [];
            contact.push({
                contactPK: 0,
                contactName: "TestOne",
                pWord: "",
                entityName: "Bob",
                tamClientCode: "x",
                lock: 0,
                entityType: "1",
                firstName: "FirstName",
                lastName: "LastName",
                telephone: "01865 847166",
                email: "danny.woodhouse@leisureinsure.co.uk",
                contactType: 1,
                quotePK: this.quotePk,
                entityTypeName: "Personxx Name Below"
            });
            var add = [{
                    contactFK: 0,
                    addressPK: 0,
                    address1: "TestAddress1",
                    address2: "TestAddress2",
                    address3: "TestAddress3",
                    town: "TestTown",
                    county: "TestCounty",
                    country: "UK",
                    postcode: "OX5 2TQ",
                    locale: this.selectedLocale.strLocale,
                    quoteFK: this.quotePk,
                    addressType: 1
                }];
            this.customers = contact;
            this.addresses = add;
        };
        PaymentController.prototype.createCertificate = function () {
            this.quotePk = parseInt(this.quoteReference.replace("LEI", ""));
            this.certificateService.GenerateCertificate(this.quoteReference, this.password);
        };
        PaymentController.prototype.getStatementofFact = function () {
            this.certificateService.GetStatementofFact(this.quoteReference);
        };
        PaymentController.prototype.getCertificate = function () {
            this.quotePk = parseInt(this.quoteReference.replace("LEI", ""));
            this.certificateService.getCertificate(this.quoteReference);
        };
        PaymentController.routing = function ($routeProvider) {
            $routeProvider.when("/checkout", {
                controller: "PaymentController",
                templateUrl: "/app/paymentpage/payment.html",
                controllerAs: "paymentController",
                metadata: {
                    title: "Leisure Insure - Shopping Basket",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/checkout/:QUOTEREFERENCE/:PASSCODE", {
                controller: "PaymentController",
                templateUrl: "/app/paymentpage/payment.html",
                controllerAs: "paymentController",
                metadata: {
                    title: "Leisure Insure - Shopping Basket",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        PaymentController.$inject = [
            "$q", "$http", "$scope", "$rootScope", "$window", "$location", "$routeParams", "ChargeService", "PolicyUpdateDataService",
            "LocationService", "CertificateService", "$filter", "$timeout", "PostCodeService", "PolicyService"];
        return PaymentController;
    }());
    LeisureInsure.PaymentController = PaymentController;
    angular.module("App")
        .controller("PaymentController", PaymentController)
        .config(["$routeProvider", PaymentController.routing]);
})(LeisureInsure || (LeisureInsure = {}));
function GetDateTime(jsDate) {
    var day = ("0" + jsDate.getDate()).slice(-2);
    var month = ("0" + (jsDate.getMonth() + 1)).slice(-2);
    var year = jsDate.getFullYear();
    var hour = ("0" + jsDate.getHours()).slice(-2);
    var minute = ("0" + jsDate.getMinutes()).slice(-2);
    var second = ("0" + jsDate.getSeconds()).slice(-2);
    var backendDate = year + "-" + month + "-" + day + "T" + hour + ":" + minute;
    return backendDate;
}
function daysBetween(date1, date2) {
    var ONE_DAY = 1000 * 60 * 60 * 24;
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();
    var difference_ms = Math.abs(date1_ms - date2_ms);
    return Math.round(difference_ms / ONE_DAY);
}
function addhoursToDate(date1, days) {
    var ONE_DAY = 1000 * 60 * 60 * 24;
    var date1_ms = date1.getTime();
    var days_ms = ONE_DAY * days;
    var new_ms = Math.abs(date1_ms + days_ms);
    var newDate = new Date(new_ms);
    return newDate;
}
//# sourceMappingURL=PaymentController.js.map