﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />
////$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$route"

module LeisureInsure {
    export class PaymentController {
        static $inject = [
            "$q", "$http", "$scope", "$rootScope", "$window", "$location", "$routeParams", "ChargeService", "PolicyUpdateDataService",
            "LocationService", "CertificateService", "$filter", "$timeout", "PostCodeService", "PolicyService"];
        window: ng.IWindowService;
        location: any;
        scope: any;
        http: any;
        q: ng.IQService;
        filter: any;
        rootScope: any;
        creditcards: Array<IPaymentCard>;
        addressLookups: Array<IAddress>;
        selectedAddress: IAddress;
        locationService: LocationService;
        certificateService: CertificateService;
        postCodeService: PostCodeService;
        policyService: PolicyService;
        customers: Array<ICustomer>;
        addresses: Array<IAddress>;
        contactDetail: IContactDetail;
        selectedLocale: ILocale;
        quotePk: number;
        entityTypes: Array<IEntityTypes>;
        paymentCards: Array<IPaymentCard>;
        documents: Array<IDocument>;
        selectedPaymentCard: IPaymentCard;
        selectedPaymentOption: IPaymentOption;
        termsAccepted: boolean;
        Range: number;
        //referral details
        name: string;
        email: string;
        phone: string;
        clientName: string;
        purchase: boolean;
        entityTypeSelected: IEntityTypes;
        quoteReference: string;
        password: string;
        sortCode: string;
        account: string;
        accountType: string;
        businessName: string;
        title: string;
        dayOfMonth: string;
        accountHolder: boolean;
        accountConfirm: boolean;
        instructors: number;
        entityNamePlaceHolder: string;
        inputInstructors: Array<number>;
        contactName: string;
        contactEmail: string;
        certHref: string;
        renewal: number;
        dateFromDt: Date;
        dateToDt: Date;
        dateRequiresUpdating: boolean;
        dates: IDates;



        constructor($q, $http, $scope, $rootScope, $window, $location, $routeParams, chargeService, policyUpdateDataService: PolicyUpdateDataService, locationService,
            certificateService, $filter, $timeout, PostCodeService, PolicyService) {
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.certificateService = certificateService;
            this.postCodeService = PostCodeService;
            this.policyService = PolicyService;
            this.quotePk = this.chargeService.quoteId;
            this.password = this.chargeService.password;
            this.quoteReference = this.chargeService.quoteReference;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.q = $q;
            this.creditcards = [];
            this.addressLookups = [];
            this.selectedAddress = [];
            //this.instructors = 5;
            this.scope.alerts = [];
            this.scope.agentAlerts = [];
            this.instructors = 1;
            this.purchase = false;

            var datenow = new Date();
            //note this is the default start and end time for events
            this.dates = {
                dateFrom: null,
                dateTo: null,
                minDate: "",
                maxDate: "",
                startingHour: "00",
                startingMinute: "00",
                endingHour: "23",
                endingMinute: "59",
                dateToRequired: false,
                startTimeRequired: false,
                endTimeRequired: false,
                minToMaxDays: null,
                coverDays: null,
                minExpDate: "",
                maxExpDate: "",
            };

            //$scope.timesettings = {};
            //$scope.onApplyTimePicker = function () {
            //    console.log('Time range applied.');
            //};
            //$scope.onClearTimePicker = function () {
            //    console.log('Time range current operation cancelled.');
            //};

            this.dayOfMonth = "14";
            this.title = "Mr";

            var cardPromise = $http
                (
                {
                    url: "/api/v1/payment-cards",
                    method: "GET"
                })
                .then(response => {
                    this.paymentCards = angular.copy(response.data);
                    this.populateCards();
                });

            $http({
                url: "/api/v1/documents/",
                data: {
                    quoteReference: $routeParams["QUOTEREFERENCE"],
                    passcode: $routeParams["PASSCODE"]
                },
                method: "POST"
            }).then(response => {
                this.documents = angular.copy(response.data);

            }, () => {
                this.scope.alerts.push({
                    type: "danger",
                    msg: `There was a problem loading your quote information. Please call us quoting reference: ${
                    $routeParams["QUOTEREFERENCE"]}`
                });
            });

            this.certHref = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("checkout")) + 'certificate/' + $routeParams["QUOTEREFERENCE"] + '/' + $routeParams["PASSCODE"];

            //user clicked proceed with policy
            if (this.chargeService.chargeSummary) {

                this.chargeSummary = this.chargeService.chargeSummary;
                this.quoteReference = $routeParams["QUOTEREFERENCE"];
                this.password = $routeParams["PASSCODE"];
                this.addresses = angular.copy(this.chargeSummary.addresses);
                this.contactEmail = this.chargeService.chargeSummary.contactEmail;
                this.contactName = this.chargeService.chargeSummary.contactName;
                if (this.chargeSummary.customers) {
                    this.customers = angular.copy(this.chargeSummary.customers);
                }

                this.renewal = chargeService.renewal;
               

                if (this.chargeSummary.policyId == 15 || this.chargeSummary.policyId == 16) {
                    this.inputInstructors = new Array(this.chargeService.chargeSummary.noInstructors + 1);
                }
                

                this.quotePk = this.chargeService.quoteId;

                this.dateToDt = new Date(this.chargeSummary.dateTo);
                this.dateFromDt = new Date(this.chargeSummary.dateFrom);

                if (this.chargeService.chargeSummary.paymentOptions === null ||
                    this.chargeService.chargeSummary.paymentOptions.length === 0) {

                    this.rootScope.loadingdocuments = true;

                    this.chargeService.loadQuote($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                        .then((data) => {

                            this.chargeSummary = this.chargeService.chargeSummary;

                            cardPromise.then(() => {
                                this.selectedPaymentCard = _
                                    .findWhere(this.paymentCards, { id: this.chargeSummary.paymentCardId });
                                this.rootScope.loadingdocuments = false;
                            });
                        }).catch(function (data: any) { this.rootScope.loadingdocuments = false; });
                }

                this.getPolicies(this.chargeSummary.numberOfDays, this.chargeService.chargeSummary.policyType);

            } else {

                policyUpdateDataService.initialise("", 0, 0)
                    .then((policydata: any) => {
                        //loading existing quote
                        if ($routeParams["QUOTEREFERENCE"]) {
                            this.chargeService.loadQuote($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                                .then((data) => {

                                    this.chargeSummary = this.chargeService.chargeSummary;
                                    this.quoteReference = $routeParams["QUOTEREFERENCE"];
                                    this.password = $routeParams["PASSCODE"];
                                    var cookie = document.cookie;
                                    this.termsAccepted = data.termsAndConditions;
                                    this.contactEmail = this.chargeService.chargeSummary.contactEmail;
                                    this.contactName = this.chargeService.chargeSummary.contactName;
                                    var addy = Array<IAddress>();
                                    var cust = Array<ICustomer>();

                                    //load start times and dates into our model

                                    this.dates.dateFrom = new Date(data.dateFrom);
                                    this.dates.dateTo = new Date(data.dateTo);
                                    this.dates.startingHour = data.timeFrom.split(":")[0];
                                    this.dates.startingMinute = data.timeFrom.split(":")[1];
                                    this.dates.endingHour = data.timeTo.split(":")[0];
                                    this.dates.endingMinute = data.timeTo.split(":")[1];

                                    //make sure we reload customer and address from cookie
                                    var results = cookie.split(";");
                                    for (var i = 0; i < results.length; i++) {
                                        var c = results[i];
                                        var d = c.split('=');
                                        if (d[0].trim() == "customers") {
                                            cust = JSON.parse(d[1]);
                                        }
                                        if (d[0].trim() == "addresses") {
                                            addy = JSON.parse(d[1]);
                                        }
                                    }
                                    //load from cookies if we get no data back from API because of chrome caching                                     
                                    if (data.addresses.length == 0) {
                                        data.addresses = addy;
                                    }
                                    if (data.customers.length == 0) {
                                        data.customers = cust;
                                    }

                                    this.addresses = angular.copy(data.addresses);
                                    this.customers = angular.copy(data.customers);
                                    this.documents = angular.copy(data.documents);


                                    cardPromise.then(() => {
                                        this.selectedPaymentCard = _
                                            .findWhere(this.paymentCards, { id: this.chargeSummary.paymentCardId });
                                    });
                                    this.reorder();
                                    this.dateToDt = new Date(this.chargeSummary.dateTo);
                                    this.dateFromDt = new Date(this.chargeSummary.dateFrom);

                                    if (this.chargeSummary.policyId == 15 || this.chargeSummary.policyId == 16) {
                                        this.inputInstructors = new Array(this.chargeService.chargeSummary.noInstructors + 1);
                                    }
                                    if (this.dateFromDt.getTime() < new Date().getTime()) {
                                        this.dateRequiresUpdating = true;
                                    }
                                    else {
                                        this.dateRequiresUpdating = false;
                                    }

                                    this.getPolicies(data.numberOfDays, data.policyType);

                                });
                        }

                    });
            }

            this.chargeService.onChargeSummaryChanged($scope,
                chargeSummary => {
                    this.chargeSummary = chargeSummary;
                    this.quoteReference = $routeParams["QUOTEREFERENCE"];
                    this.password = $routeParams["PASSCODE"];
                });

            this.locationService.initialised.then(locationService => {
                this.selectedLocale = locationService.selectedLocale;
                if (this.locationService != null && this.locationService.selectedLocale != null) {
                    this.locationService.selectedLocale.showFlag = false;
                }
            });

            //this is here as a test, uses directive when ngrepeat finishes
            $scope.onEnd = function () {
                //$timeout(function () {
                //    //alert('all done');
                //});
                //alert('all done');
            };

        } //end constructor  *********************************************        

        updatePolicyPeriod() {
            if (this.dateFromDt >= new Date) {
                this.chargeService.dateFromDt = this.dateFromDt;
                this.chargeService.dateToDt = this.dateToDt;
                this.dateRequiresUpdating = false;

                this.chargeService.updateQuotePeriod();
            }
            else {
                this.dateRequiresUpdating = true;
            }
        }


        getPolicies(numdays: number, policyType: string) {
            var policyID = this.chargeSummary.policyId;

            this.policyService.GetPolicies(this.chargeSummary.localeId)
                .then((data: any) => {
                    this.policyService.selectPolicyById(policyID);
                    this.dates.minDate = this.policyService.selectedPolicy.minDate;
                    this.dates.maxDate = this.policyService.selectedPolicy.maxDate;
                    var isEventSupplier = false;
                    
                    var min = new Date(this.policyService.selectedPolicy.minDate);
                    var max = new Date(this.policyService.selectedPolicy.maxDate);
                    var days = daysBetween(max, min);

                    this.dates.minToMaxDays = days;

                    //fieldsports - number of days determines our min/max range
                    if (policyID == 8) {
                        this.dates.minToMaxDays = numdays;
                    }
                    //exhibitors - number of days determines our end date 7 or 1
                    if (policyID == 18) {
                        this.dates.coverDays = numdays;
                    }
                    if (policyID == 25) { //stallholders
                        var cancellation = _.filter(this.chargeSummary.chargeSummaryItems, (c: any) => {
                            return c.coverFK == 30 && c.price != null && c.price > 0;
                        });
                        // if cancellation cover has been selected then min date = today + 14 days
                        if (cancellation.length > 0) {
                            var dt = new Date(min.setDate(min.getDate() + 14));
                            min = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
                            max = new Date(min.getFullYear() + 1, dt.getMonth(), dt.getDate())
                            var m = "";
                            if (min.getMonth() < 10) {
                                m = '0' +(min.getMonth()+1).toString();
                            }
                            else {
                                m = (dt.getMonth() + 1).toString();
                            }

                            this.dates.minDate = min.getFullYear() + '-' + m + '-' + min.getDate();
                            this.dates.maxDate = max.getFullYear() + '-' + m + '-' + min.getDate();
                            days = daysBetween(max, min);
                            this.dates.minToMaxDays = days;
                        }

                    }
                    //events - number of days determines our min/max range
                    if (policyID == 6 && policyType == "E") {
                        this.dates.minToMaxDays = numdays;                        
                        
                        if (this.chargeSummary.businessDescription.indexOf("Supplier") != -1)
                        {
                            this.dates.coverDays = numdays;
                            isEventSupplier = true;
                        }
                    }

                    //set whether we are showing dateTo/times
                    if (policyType == "E") {
                        if (policyID == 8 || policyID == 6 && !isEventSupplier)
                            this.dates.dateToRequired = true;
                        if (policyID == 5 || policyID == 25 || policyID == 6 || policyID == 18)
                            this.dates.startTimeRequired = true;
                        if (policyID == 6)
                            this.dates.endTimeRequired = true;
                    }


                },
                (data: any) => {

                });
        }

        TermsClicked(form: any) {
            var display = $("#termsTick").css("display");

            if (display == "none") {
                $("#termsTick").css("display", "block");
                form.termsAndCond.$setValidity('required', true);
                this.termsAccepted = true;
            }
            else {
                $("#termsTick").css("display", "none");
                form.termsAndCond.$setValidity('required', false);
                this.termsAccepted = false;
            }
        }

        ShowCert() {

            if (this.dates.dateFrom != null) {

                var win = window.open();

                this.updateEndDate();
                this.chargeService.updateQuoteDates(this.dates)
                    .then((data) => {
                        win.location.href = this.certHref;
                    }).catch(function (data: any) { this.rootScope.loadingdocuments = false; });
            }
            else
                eval("ShowUIDialog('Please select a start date before viewing a quote');");
        }

        setEntityNamePlaceHolder(): void {
            if (this.customers.length) {
                if (this.customers[0].entityType == "TradingAs") {
                    this.entityNamePlaceHolder = "Example: Joe Blogs and Joanna Blogs t/a Blogs Enterprises";
                }
                else {
                    this.entityNamePlaceHolder = "";
                }
            }
        }

        Debug(): void {
            console.log("paymentcards", this.paymentCards);
            console.log("cards", this.creditcards);
            console.log("selectedPaymentOption", this.selectedPaymentOption);
            console.log("paymentOptions", this.chargeSummary.paymentOptions);
            console.log("chargeSummary", this.chargeSummary);

            console.log("address lookups", this.addressLookups);

            this.scope.startDate = new Date();
        }

        populateAddress(): void {

            this.addresses[0] = this.selectedAddress;
        }

        getAddress(): void {
            if (this.addresses[0] != null) {
                if (this.addresses[0].postcode != null) {

                    this.postCodeService.GetAddresses(this.addresses[0].postcode)
                        .then((addresses) => {
                            this.addressLookups = [];
                            _.each(addresses, (address: any) => {
                                let addy = <IAddress>{};
                                addy.addressID = address.addressID;
                                addy.address1 = address.addressLine1;
                                addy.address2 = address.addressLine2;
                                addy.town = address.town;
                                addy.county = address.county;
                                addy.postcode = address.postCode;

                                this.addressLookups.push(addy);
                            });
                            //get first address and assign to our select, also update form
                            this.selectedAddress = this.addressLookups[0];
                            this.addresses[0] = this.selectedAddress;

                        });

                }
            }
        }

        showPaymentOptions(): boolean {
            if (this.chargeSummary === undefined || this.chargeSummary.paymentOptions === undefined ||
                this.chargeSummary.paymentOptions === null) {
                return false;
            }
            return this.chargeSummary.paymentOptions.length > 1;
        }

        removeCover(cover) {
            var coverId = cover.coverFK;
            var button = "removeCoverButton" + coverId;
            this.scope[button] = true;

            this.chargeService.removeCover(coverId)
                .then(() => {
                    this.scope[button] = false;
                },
                () => {
                    this.scope[button] = false;
                    this.scope.agentAlerts.push({
                        type: "danger",
                        msg:
                        "There was a problem removing the cover, please try again or give us a call quoting your reference displayed above"
                    });
                });
        }

        completeAsAgent() {

            if (!this.backDatedStartDate()) {

                this.updateEndDate();

                this.submitCustomer()
                    .then(() => {
                        this.chargeService.completeAsAgent(this.dates)
                            .then(() => {
                                this.scope.payNowButton = false;
                                this.scope.agentAlerts.push({
                                    type: "success",
                                    msg: "Policy purchased"
                                });
                            }, response => {
                                // Failed completeAdAgent()
                                this.scope.payNowButton = false;
                                var msgText = `There was a problem submitting the policy: ${response}`;

                                this.scope.agentAlerts.push({
                                    type: "danger",
                                    msg: msgText
                                });
                            });
                    }, () => {
                        // Failed submitCustomer()
                        this.scope.payNowButton = false;
                        this.scope.agentAlerts.push({
                            type: "danger",
                            msg: "There was a problem submitting the policy address information, please check the form for errors"
                        });
                    });
            }
            else
                eval("ShowUIDialog('Please check start date/time. Cover cannot be backdated');");
        }

        private reorder() {
            this.chargeSummary.paymentOptions = _.sortBy(this.chargeSummary.paymentOptions, "id");
        }

        chargeService: ChargeService;
        chargeSummary: IChargeSummary;


        backDatedStartDate() {

            var backdated = false
            //if we have a start time then check backdating
            if (this.dates.startTimeRequired) {
                var currentDate = new Date();
                var startTime = this.dates.startingHour;
                var startDate = this.dates.dateFrom;
                startDate.setHours(parseInt(this.dates.startingHour));
                startDate.setMinutes(parseInt(this.dates.startingMinute));

                if (startDate < currentDate)
                    backdated = true;
            }

            return backdated;

        }

        updateEndDate() {
            //if there is no endDate and its an event and we have a specific number of days
            //then set the endDate
            if (!this.dates.dateToRequired && this.chargeService.chargeSummary.policyType == "E") {
                if (this.dates.coverDays != null) {
                    var newDateTo = addhoursToDate(this.dates.dateFrom, this.dates.coverDays);
                    this.dates.dateTo = newDateTo;
                }
            }

        }

        completeAsCustomer() {

            if (!this.backDatedStartDate()) {
                if (!this.validatePaymentOption() || this.selectedPaymentCard === undefined ||
                    this.selectedPaymentCard.id === 0 || this.selectedPaymentOption === undefined ||
                    this.selectedPaymentOption.initialPayment === undefined) {
                    var msgText = "There was a problem with your payment information, please try again";

                    this.scope.agentAlerts.push({
                        type: "danger",
                        msg: msgText
                    });
                    this.scope.payNowButton = false;
                    return;
                }

                this.submitCustomer()

                    .then(() => {
                        // Submit the new card info
                        this.chargeService.paymentTypeId = this.selectedPaymentOption.id;

                        this.chargeService.selectCard(this.selectedPaymentCard.id)
                            .then(() => {
                                setTimeout(() => {
                                    // Submit the payment form
                                    var numberOfPayments = 1;
                                    if (this.selectedPaymentOption.numberOfPayments) {
                                        numberOfPayments = this.selectedPaymentOption.numberOfPayments;
                                    }

                                    var form = $(`#payment-${numberOfPayments}`);

                                    this.updateEndDate();

                                    var dateFrom = GetDateTime(this.dates.dateFrom);
                                    var dateTo = dateFrom;
                                    if (this.dates.dateTo != null)
                                        dateTo = GetDateTime(this.dates.dateTo);

                                    //console.log("dates", this.dates);

                                    this.http.post(`api/v1/payments/${this.quoteReference}`,
                                        {
                                            passcode: this.password,
                                            sortCode: this.sortCode,
                                            account: this.account,
                                            accountType: this.accountType,
                                            title: this.title,
                                            name: this.name,
                                            collectionDate: this.dayOfMonth,
                                            businessName: this.businessName,
                                            termsAndConditions: this.termsAccepted,
                                            numberOfPayments: this.selectedPaymentOption.numberOfPayments,
                                            startDate: dateFrom,
                                            endDate: dateTo,
                                            timeFrom: this.dates.startingHour + ":" + this.dates.startingMinute,
                                            timeTo: this.dates.endingHour + ":" + this.dates.endingMinute,
                                            dateToRequired: this.dates.dateToRequired,
                                            startTimeRequired: this.dates.startTimeRequired,
                                            endTimeRequired: this.dates.endTimeRequired,
                                            numberOfDays: this.dates.coverDays
                                        }).then(() => {
                                            form.submit();
                                        }, () => {
                                            this.scope.payNowButton = false;
                                            var msgText = "There was a problem with your payment information, please try again";

                                            this.scope.agentAlerts.push({
                                                type: "danger",
                                                msg:
                                                msgText
                                            });

                                        });

                                }, 100);
                            },
                            (error) => {
                                // Failed to submit new card
                                this.scope.payNowButton = false;
                                this.scope.agentAlerts.push({
                                    type: "danger",
                                    msg: error
                                });
                            }
                            );
                    }, () => {
                        // Failed submitCustomer()
                        this.scope.payNowButton = false;
                        this.scope.agentAlerts.push({
                            type: "danger",
                            msg: "There was a problem submitting the policy address information, please check the form for errors"
                        });
                    });
            }
            else
                eval("ShowUIDialog('Please check start date/time. Cover cannot be backdated');");
        }

        GetInvalidFields(frm: any) {

            // check to make sure the form is completely valid
            alert("submitted");
            var errors = frm.$error.required;
            console.clear();
            angular.forEach(errors, function (field) {

                if (field.$invalid) {

                    var fieldName = field.$name;
                    //console.log(fieldName);
                }
            });


        };

        payNow(formvalid: boolean, paymentMethodId: number): void {
            if (formvalid) {
                this.scope.payNowButton = true;
                if (this.chargeSummary.isAgent == true) {
                    this.completeAsAgent();
                    return;
                }

                if (!this.termsAccepted && this.renewal === 0) {
                    this.scope.agentAlerts.push({
                        type: "danger",
                        msg: "Please confirm that you have read the policy documents"
                    });
                    this.scope.payNowButton = false;
                    return;
                }

                if (this.chargeSummary.isAgent) {
                    this.completeAsAgent();

                } else {
                    this.completeAsCustomer();

                }

            }
            this.http.post(`api/v1/setBuy/${this.quoteReference}/1`);
        }
        unlockQuote() {
            this.http.post(`api/v1/unlockQuote/${this.quoteReference}/0`);
        }

        populateCards() {

            var cards = Array<string>();

            for (let card of this.paymentCards) {

                var item = <IPaymentCard>{};

                var currencySymbol = this.locationService.selectedLocale.currencySymbol;
                var uk = this.locationService.selectedLocale.localePK === 1;
                var charge = 0;
                if (card.addRate > 0) {
                    if (this.selectedPaymentOption === undefined ||
                        this.selectedPaymentOption.initialPayment === undefined) {
                        item.name = card.name + " (" + card.addRate * 100 + "% card charge)"
                        item.id = card.id;
                        this.creditcards.push(item)
                        continue;
                    }
                    charge = this.selectedPaymentOption.initialPayment * card.addRate;
                    charge = Math.max(1, charge);
                }
                if (uk) {
                    charge += card.addChargeUk;
                } else {
                    charge += card.addChargeIe;
                }
                charge = Math.round(charge * 100) / 100;

                item.id = card.id;
                item.name = card.name + " (" + currencySymbol + charge + " card charge)";
                this.creditcards.push(item);
            }

        }

        validatePaymentOption(): boolean {
            if (this.selectedPaymentOption.numberOfPayments > 1) {
                if (this.sortCode === "" || this.account === "") {
                    return false;
                }
                if (!this.accountConfirm || !this.accountHolder) {
                    return false;
                }
                if (this.accountType === "B") {
                    if (this.businessName === "") {
                        return false;
                    }
                } else {
                    if (this.name === "") {
                        return false;
                    }
                }
            }

            return true;
        }




        sendReferral(): void {

            var url = "http://" + this.window.location.host + "/";
            this.window.location.href = url;
            this.scope.sendRefferalButton = true;
            if (this.clientName == null || this.clientName == "") {
                this.clientName = this.name;
            }
            this.http.post("api/v1/payment/referralcallbacks",
                {
                    quoteReference: this.quoteReference,
                    name: this.name,
                    email: this.email,
                    phone: this.phone,
                    clientName: this.clientName
                }).then(() => {
                    this.scope.sendRefferalButton = false;
                    this.scope.alerts.push({
                        type: "success",
                        msg: "We have received your information and will look at your policy and be in touch ASAP"
                    });
                }, response => {
                    this.scope.sendRefferalButton = false;
                    this.scope.alerts.push({
                        type: "danger",
                        msg: response.statusText
                    });
                });
        }

        closeAlert(index, array) {
            this.scope[array].splice(index, 1);
        };

        toggleLegalCare() {
            this.chargeService.toggleLegalCare()
                .then(chargeSummary => {
                    this.chargeSummary = chargeSummary;
                    this.documents = chargeSummary.documents;
                    this.reorder();
                });

        }

        addExcessWaiver() {
            this.chargeService.toggleExcessWaiver()
                .then(chargeSummary => {
                    this.chargeSummary = chargeSummary;
                    this.reorder();
                });
        }

        selectCard() {
            this.chargeService.updatePaymentCard(this.selectedPaymentCard.id)
                .then(chargeSummary => {
                    this.chargeSummary = chargeSummary;
                    this.reorder();
                });
        }

        submitCustomer(): ng.IPromise<any> {
            var deferred = this.q.defer();
            var qr = this.quoteReference;
            this.quotePk = parseInt(qr.substring(qr.length - 6, qr.length));
            var addx = [];
            var custx = [];
            this.customers[0].contactType = 1;

            custx.push(this.customers[0]);

            //var eType = this.customer[0].entityType;
            if (this.customers[0].entityType != "Customer" &&
                (this.customers[0].entityName === undefined || this.customers[0].entityName === "")) {
                eval("ShowUIDialog('Please ensure that the entity name is completed');");
                deferred.reject("Please ensure that the entity name is completed");
            }
            else {
                //if (this.customers.length > 1) {
                //this.customer.splice(1, this.customer.length - 1);
                var i = 0;

                _.each(this.customers, (cust: ICustomer) => {
                    if (i == 0 && this.contactEmail == '') {
                        this.contactEmail = cust.email;
                    }
                    if (i == 0 && this.contactName == '') {
                        this.contactName = cust.firstName;
                    }
                    if (i > 0) {
                        //alert("MORE THAN ONE CUSTOMER");
                        cust.contactType = 1;
                        cust.quotePK = this.customers[0].quotePK;
                        cust.entityType = "Customer";
                        custx.push(cust);
                    }
                    i++;
                    this.instructors = i;
                });
                if (this.addresses.length > 1) {
                    //this.address.splice(1, this.address.length - 1);
                }

                this.addresses[0].quoteFK = this.quotePk;
                this.addresses[0].addressType = 1;
                this.addresses[0].locale = this.selectedLocale.strLocale;

                addx.push(this.addresses[0]);

                this.contactDetail = {
                    contact: custx,
                    address: addx,
                    quoteReference: this.quoteReference,
                    passCode: this.password,
                    contactName: this.contactName,
                    contactEmail: this.contactEmail
                };

                this.chargeService.addCustomerDetails(this.contactDetail)
                    .then(() => {
                        deferred.resolve();
                    }, response => {
                        this.scope.alerts.push({
                            type: "danger",
                            msg: response
                        });
                        deferred.reject(response);
                    });
            }


            document.cookie = "customers=" + JSON.stringify(this.customers);
            document.cookie = "addresses=" + JSON.stringify(this.addresses);
            //console.log("write cookie");
            var x = document.cookie;

            return deferred.promise;
        }

        customerLoadTest() {
            var contact = [];
            contact.push({
                contactPK: 0,
                contactName: "TestOne",
                pWord: "",
                entityName: "Bob",
                tamClientCode: "x",
                lock: 0,
                entityType: "1",
                firstName: "FirstName",
                lastName: "LastName",
                telephone: "01865 847166",
                email: "danny.woodhouse@leisureinsure.co.uk",
                contactType: 1,
                quotePK: this.quotePk,
                entityTypeName: "Personxx Name Below"
            });
            var add = [{
                contactFK: 0,
                addressPK: 0,
                address1: "TestAddress1",
                address2: "TestAddress2",
                address3: "TestAddress3",
                town: "TestTown",
                county: "TestCounty",
                country: "UK",
                postcode: "OX5 2TQ",
                locale: this.selectedLocale.strLocale,
                quoteFK: this.quotePk,
                addressType: 1
            }];
            this.customers = contact;
            this.addresses = add;
        }

        createCertificate() {
            this.quotePk = parseInt(this.quoteReference.replace("LEI", ""));
            this.certificateService.GenerateCertificate(this.quoteReference, this.password);
        }
        getStatementofFact() {

            this.certificateService.GetStatementofFact(this.quoteReference);
        }
        getCertificate() {
            this.quotePk = parseInt(this.quoteReference.replace("LEI", ""));
            this.certificateService.getCertificate(this.quoteReference);
        }

        static routing($routeProvider) {
            $routeProvider.when("/checkout",
                {
                    controller: "PaymentController",
                    templateUrl: "/app/paymentpage/payment.html",
                    controllerAs: "paymentController",
                    metadata: {
                        title: "Leisure Insure - Shopping Basket",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/checkout/:QUOTEREFERENCE/:PASSCODE",
                {
                    controller: "PaymentController",
                    templateUrl: "/app/paymentpage/payment.html",
                    controllerAs: "paymentController",
                    metadata: {
                        title: "Leisure Insure - Shopping Basket",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
        }

    } //end controller            

    angular.module("App")
        .controller("PaymentController", PaymentController)
        .config(["$routeProvider", PaymentController.routing])

}

function GetDateTime(jsDate: Date): string {

    //make sure we have 2 digits 
    var day = ("0" + jsDate.getDate()).slice(-2);
    var month = ("0" + (jsDate.getMonth() + 1)).slice(-2);
    var year = jsDate.getFullYear();
    var hour = ("0" + jsDate.getHours()).slice(-2);
    var minute = ("0" + jsDate.getMinutes()).slice(-2);
    var second = ("0" + jsDate.getSeconds()).slice(-2);
    //make compatible with dateTime
    var backendDate = year + "-" + month + "-" + day + "T" + hour + ":" + minute;

    return backendDate;
}

function daysBetween(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime()
    var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms)

    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY)

}

//note this is in datepicker directive.. 
function addhoursToDate(date1, days) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert date to milliseconds
    var date1_ms = date1.getTime()

    //get milliseconds from days
    var days_ms = ONE_DAY * days;

    //total milliseconds
    var new_ms = Math.abs(date1_ms + days_ms)

    // Convert milliseconds to date
    var newDate = new Date(new_ms);

    return newDate;

}







