﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class PurchaseController {
        static $inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "CommonDataService", "PriceService", "StripeService"];
        filter: any; // ng.IRootScopeService;
        window: ng.IWindowService;
        location: any;
        scope: any;
        rootScope: any;
        timeout: any;
        http: ng.IHttpService;
        q: ng.IQService;
        locationService: LocationService;
        locales: Array<ILocale>;
        chargeService: ChargeService;
        policyData: IPolicyView;
        commonDataService: CommonDataService;
        priceService: PriceService;
        stripeService: StripeService;
        descriptionSrc: string;
        //have we attempted to get a price? used for validation                
        navigationData: INavigationObject;
        dates: IDates;
        //stripeData: IStripe;        
        paymentCard: StripeTokenData;
        certHref: string;
        cardMessage: string;
        validCard: boolean;
        instalment: boolean;
        instalmentData: IInstalments;


        constructor($http, $q: ng.IQService, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies,
            locationService, chargeService, urlToNavigationService: UrlToNavigationService, metadataService: MetadataService,
            $timeout, commonDataService: CommonDataService, priceService: PriceService, stripeService: StripeService) {
            var initialisationPromises = [];
            var navigationData: INavigationObject;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.commonDataService = commonDataService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.priceService = priceService;
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            this.stripeService = stripeService;
            this.certHref = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("payment")) + 'newcertificate/' + $routeParams["QUOTEREFERENCE"] + '/' + $routeParams["PASSCODE"];
            $("#cardLabel").css("display", "none");


            //not doing it this way, just put published key on policy data
            //this.stripeService.GetStripeData().then((data: IStripe) => {
            //    this.stripeData = data;
            //    //identify website with stripes API
            //    Stripe.setPublishableKey(this.stripeData.publishedKey);
            //});

            locationService.onSelectedLocaleChanged($scope, (locale: ILocale) => {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            })

            this.dates = {
                dateFrom: null,
                dateTo: null,
                minDate: "",
                maxDate: "",
                startingHour: "00",
                startingMinute: "00",
                endingHour: "23",
                endingMinute: "59",
                dateToRequired: false,
                startTimeRequired: false,
                endTimeRequired: false,
                minToMaxDays: null,
                coverDays: null,
                minExpDate: "",
                maxExpDate: "",
            };

            if (this.instalment == null)
                this.instalment = false;

            if (this.instalmentData == null)
                this.instalmentData = <IInstalments>{};

            //load existing quote *******************************************************


            if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                var quoteRef = $routeParams["QUOTEREFERENCE"];
                var password = $routeParams["PASSCODE"];

                //urlToNavigationService.GetLandingPageNew(quoteRef, password)
                //    .then((data: INavigationObject) => {
                //        this.navigationData = data;

                //        if (this.navigationData.policyId > 0) {
                //            metadataService.setMetadata(this.navigationData.title,
                //                this.navigationData.description,
                //                this.navigationData.keywords);
                //            if (this.locationService.selectedLocale != null) {

                //            }

                //            this.policyData = <IPolicyView>{};
                //            this.InitMinDetails();
                //        }
                //    });

                this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                    .then((data: IPolicyView) => {

                        this.policyData = data;
                        this.InitMinDetails();

                        this.SetDates(365, this.policyData.policyType);

                        if (this.locationService.selectedLocale != null) {
                            this.locationService.selectedLocale.showFlag = false;
                        }                        

                    })

            }
        }//****************************************** End Constructor *************************************  

        SetDates(numdays: number, policyType: string) {

            var isEventSupplier = false;
            this.dates.minDate = new Date(this.policyData.minStartDate).toISOString().slice(0, 10);
            this.dates.maxDate = new Date(this.policyData.maxStartDate).toISOString().slice(0, 10);

            var min = new Date(this.dates.minDate);
            var max = new Date(this.dates.maxDate);
            var days = daysBetween(max, min);

            this.dates.minToMaxDays = days;

            //we are not getting dates from selects, we already have them on the quote
            //but this will be overridden by the customer
            //this.dates.dateFrom = new Date(this.policyData.dateFrom);
            //this.dates.dateTo = new Date(this.policyData.dateTo);          
            //this.dates.startingHour = this.policyData.timeFrom.split(":")[0];
            //this.dates.startingMinute = this.policyData.timeFrom.split(":")[1];
            //this.dates.endingHour = this.policyData.timeTo.split(":")[0];
            //this.dates.endingMinute = this.policyData.timeTo.split(":")[1];

           
        }

        //the minimum required to show the page, picture, policyname, locale       
        InitMinDetails(): void {            

            if (this.policyData.addresses == null)
                this.policyData.addresses = [];

            if (this.policyData.covers != null)
                this.policyData.covers = this.filter("orderBy")(this.policyData.covers, ["coverOrdinal"]);

            $('.cc-number').payment('formatCardNumber');
            $('.cc-exp').payment('formatCardExpiry');
            $('.cc-cvc').payment('formatCardCVC');

        }

        ShowCardImage(): void {

            $(".card-images img").removeClass("disabled");

            var cardType = $.payment.cardType($('.cc-number').val());
            if (!cardType) {
                return;
            }

            $(".cc-number").css("background", "url(/Content/Images/CreditCards/" + cardType + ".png) no-repeat");
            $(".cc-number").css("background-position", "98% 50%");

            var selector = ".card-images img[alt!='cardType']";
            selector = selector.replace("cardType", cardType);
            $(selector).addClass("disabled");
        }

        Debug(): void {
            console.log("policyData", this.policyData);
            console.log("dates", this.dates);
            console.log("certRef", this.certHref);
            //console.log("stripeData", this.stripeData);
            console.log("card", this.paymentCard);
            console.log("valid card", this.validCard);
            console.log("card message", this.cardMessage);
            console.log("instalmentData", this.instalmentData);
            console.log("instalment", this.instalment);

            _.each(this.policyData.covers, (cover: ICoverView) => {
                if (cover.net > 0)
                    console.log("covers", cover);
            });

        }

        PayByInstalment(): void {
            //recalculate total for instalment
            //this.priceService.GetPrice(this.P
            this.priceService.CalculateInstalments(this.instalmentData, this.policyData);
            //this.policyData.instalmentData = this.instalmentData;
            //this.commonDataService.SetPolicyData(this.policyData);
        }

        RemoveInstalment(): void {

            this.instalmentData = <IInstalments>{};
            ResetInstalmentData();
            //this.policyData.instalmentData = null;
            //this.commonDataService.SetPolicyData(this.policyData);
        }

        StripeResponse(status: number, response: StripeTokenResponse) {


            //we do not have the context of angular with a stripe callback 
            //so use jquery/javascript
            if (response.id) {
                $("#cardInfo").css("display", "none");
                this.validCard = true;
                this.cardMessage = "";

                PaymentData.stripeToken = response.id;

                //$("#purchaseBtn").attr("disabled", "disabled");

                $.ajax({
                    type: "POST",
                    url: "api/v1/stripepayment",
                    data: {
                        StripeToken: PaymentData.stripeToken,
                        QuoteRef: PaymentData.quoteRef,
                        QuotePass: PaymentData.quotePass,
                        CurrencyId: PaymentData.currencyId,
                        InstalmentData: InstalmentData,
                        PolicyDates: Dates
                    },
                    beforeSend: function () {
                        $("#loadingAnim").css("display", "block");
                    },
                    success: function (data, test) {
                        //proceed to confirmation page
                        window.location.href = confirmLink;

                    },
                    complete: function () {
                        $("#loadingAnim").css("display", "none");
                    },
                    error: function (error, test) {
                        $("#cardInfo").css("display", "block");
                        $("#cardInfo").html(error.responseJSON.message);
                    },
                    dataType: "json"
                });

            }
            else {
                $("#cardInfo").css("display", "block");
                $("#cardInfo").html(response.error.message);
            }
        }

        CompleteAsAgent(): void {
            //even though we are not using sripe here we are keeping our 
            //payment data for agents and customers consistent
            this.GetDates();

            this.chargeService.completeAsAgentNew().then((data: any) => {
                window.location.href = confirmLink;
            }).catch((data: any) => {
                $("#cardInfo").css("display", "block");
                $("#cardInfo").html(data);
            });
        }

        GetDates(): void {
            Dates.dateFrom = GetDateTime(this.dates.dateFrom);
            if (this.dates.dateTo != null)
                Dates.dateTo = GetDateTime(this.dates.dateTo);
            Dates.maxDate = this.dates.maxDate;
            Dates.minDate = this.dates.minDate;
            Dates.startingHour = this.dates.startingHour;
            Dates.startingMinute = this.dates.startingMinute;
            Dates.endingHour = this.dates.endingHour;
            Dates.endingMinute = this.dates.endingMinute;
            Dates.dateToRequired = this.dates.dateToRequired;
            Dates.startTimeRequired = this.dates.startTimeRequired;
            Dates.endTimeRequired = this.dates.endTimeRequired;
            Dates.minToMaxDays = this.dates.minToMaxDays;
            Dates.coverDays = this.dates.coverDays;
            Dates.minExpDate = this.dates.minExpDate;
            Dates.maxExpDate = this.dates.maxExpDate;
        }

        submitPaymentForm(form: any, valid: boolean): void {

            if (valid) {

                PaymentData.quoteRef = this.policyData.quoteReference;
                PaymentData.quotePass = this.policyData.password;
                PaymentData.currencyId = this.policyData.locale.currencySymbol;

                confirmLink = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("purchase")) + 'confirmation/' + PaymentData.quoteRef + '/' + PaymentData.quotePass;

                if (this.policyData.brokerName != null) {
                    this.CompleteAsAgent();
                }
                else {

                    let card: StripeTokenData;
                    card = angular.copy(this.paymentCard, card);
                    let addy = this.policyData.addresses[0];
                    //update card data with customer address
                    card.address_country = addy.country;
                    card.address_line1 = addy.address1;
                    card.address_line2 = addy.address2;
                    card.address_zip = addy.postcode;
                    card.name = addy.contactName;
                    //send card data to stripe so we get back a token                   

                    //update instalment if we added it
                    if (this.instalmentData.totalToPay != null) {
                        InstalmentData.totalToPay = this.instalmentData.totalToPay;
                        InstalmentData.deposit = this.instalmentData.deposit;
                        InstalmentData.numberOfPayments = this.instalmentData.numberOfPayments;
                        InstalmentData.eachPayment = this.instalmentData.eachPayment;
                        InstalmentData.sortCode = this.instalmentData.sortCode;
                        InstalmentData.title = this.instalmentData.title;
                        InstalmentData.businessName = this.instalmentData.businessName;
                        InstalmentData.name = this.instalmentData.name;
                        InstalmentData.accountHolder = this.instalmentData.accountHolder;
                        InstalmentData.accountType = this.instalmentData.accountType;
                        InstalmentData.accountConfirm = this.instalmentData.accountConfirm;
                        InstalmentData.accountNumber = this.instalmentData.accountNumber;
                    }

                    this.GetDates();

                    Stripe.setPublishableKey(this.policyData.stripePublishableKey);
                    Stripe.card.createToken(card, this.StripeResponse);
                }

            }

        }

        static routing($routeProvider) {
            $routeProvider.when("/purchase",
                {
                    controller: "PurchaseController",
                    templateUrl: "/app/purchase/purchase.html",
                    controllerAs: "PurchaseController",
                    metadata: {
                        title: "Leisure Insure Personal Details",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/purchase/:QUOTEREFERENCE/:PASSCODE",
                {
                    controller: "PurchaseController",
                    templateUrl: "/app/purchase/purchase.html",
                    controllerAs: "PurchaseController",
                    metadata: {
                        title: "Leisure Insure Personal Details",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });

        }

    }

    angular.module("App")
        .controller("PurchaseController", PurchaseController)
        .config(["$routeProvider", PurchaseController.routing]);
}
//stripe response does not recognise angular 
var PaymentData = { stripeToken: "", quoteRef: "", quotePass: "", currencyId: "" };

var Dates = {
    dateFrom: "",
    dateTo: "",
    maxDate: "",
    minDate: "",
    startingHour: "",
    startingMinute: "",
    endingHour: "",
    endingMinute: "",
    dateToRequired: false,
    startTimeRequired: false,
    endTimeRequired: false,
    minToMaxDays: 0,
    coverDays: 0,
    maxExpDate: "",
    minExpDate: ""
};

var InstalmentData = {
    totalToPay: 0, deposit: 0, numberOfPayments: 0, eachPayment: 0, accountNumber: 0, sortCode: "",
    accountType: "", title: "", businessName: "", name: "", accountHolder: false, accountConfirm: false
};

var confirmLink = "";











