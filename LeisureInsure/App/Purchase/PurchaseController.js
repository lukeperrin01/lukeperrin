var LeisureInsure;
(function (LeisureInsure) {
    var PurchaseController = (function () {
        function PurchaseController($http, $q, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies, locationService, chargeService, urlToNavigationService, metadataService, $timeout, commonDataService, priceService, stripeService) {
            var _this = this;
            var initialisationPromises = [];
            var navigationData;
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.commonDataService = commonDataService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            this.priceService = priceService;
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            this.stripeService = stripeService;
            this.certHref = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("payment")) + 'newcertificate/' + $routeParams["QUOTEREFERENCE"] + '/' + $routeParams["PASSCODE"];
            $("#cardLabel").css("display", "none");
            locationService.onSelectedLocaleChanged($scope, function (locale) {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            });
            this.dates = {
                dateFrom: null,
                dateTo: null,
                minDate: "",
                maxDate: "",
                startingHour: "00",
                startingMinute: "00",
                endingHour: "23",
                endingMinute: "59",
                dateToRequired: false,
                startTimeRequired: false,
                endTimeRequired: false,
                minToMaxDays: null,
                coverDays: null,
                minExpDate: "",
                maxExpDate: "",
            };
            if (this.instalment == null)
                this.instalment = false;
            if (this.instalmentData == null)
                this.instalmentData = {};
            if ($routeParams["QUOTEREFERENCE"] && $routeParams["PASSCODE"]) {
                var quoteRef = $routeParams["QUOTEREFERENCE"];
                var password = $routeParams["PASSCODE"];
                this.chargeService.loadPolicyData($routeParams["QUOTEREFERENCE"], $routeParams["PASSCODE"])
                    .then(function (data) {
                    _this.policyData = data;
                    _this.InitMinDetails();
                    _this.SetDates(365, _this.policyData.policyType);
                    if (_this.locationService.selectedLocale != null) {
                        _this.locationService.selectedLocale.showFlag = false;
                    }
                });
            }
        }
        PurchaseController.prototype.SetDates = function (numdays, policyType) {
            var isEventSupplier = false;
            this.dates.minDate = new Date(this.policyData.minStartDate).toISOString().slice(0, 10);
            this.dates.maxDate = new Date(this.policyData.maxStartDate).toISOString().slice(0, 10);
            var min = new Date(this.dates.minDate);
            var max = new Date(this.dates.maxDate);
            var days = daysBetween(max, min);
            this.dates.minToMaxDays = days;
        };
        PurchaseController.prototype.InitMinDetails = function () {
            if (this.policyData.addresses == null)
                this.policyData.addresses = [];
            if (this.policyData.covers != null)
                this.policyData.covers = this.filter("orderBy")(this.policyData.covers, ["coverOrdinal"]);
            $('.cc-number').payment('formatCardNumber');
            $('.cc-exp').payment('formatCardExpiry');
            $('.cc-cvc').payment('formatCardCVC');
        };
        PurchaseController.prototype.ShowCardImage = function () {
            $(".card-images img").removeClass("disabled");
            var cardType = $.payment.cardType($('.cc-number').val());
            if (!cardType) {
                return;
            }
            $(".cc-number").css("background", "url(/Content/Images/CreditCards/" + cardType + ".png) no-repeat");
            $(".cc-number").css("background-position", "98% 50%");
            var selector = ".card-images img[alt!='cardType']";
            selector = selector.replace("cardType", cardType);
            $(selector).addClass("disabled");
        };
        PurchaseController.prototype.Debug = function () {
            console.log("policyData", this.policyData);
            console.log("dates", this.dates);
            console.log("certRef", this.certHref);
            console.log("card", this.paymentCard);
            console.log("valid card", this.validCard);
            console.log("card message", this.cardMessage);
            console.log("instalmentData", this.instalmentData);
            console.log("instalment", this.instalment);
            _.each(this.policyData.covers, function (cover) {
                if (cover.net > 0)
                    console.log("covers", cover);
            });
        };
        PurchaseController.prototype.PayByInstalment = function () {
            this.priceService.CalculateInstalments(this.instalmentData, this.policyData);
        };
        PurchaseController.prototype.RemoveInstalment = function () {
            this.instalmentData = {};
            ResetInstalmentData();
        };
        PurchaseController.prototype.StripeResponse = function (status, response) {
            if (response.id) {
                $("#cardInfo").css("display", "none");
                this.validCard = true;
                this.cardMessage = "";
                PaymentData.stripeToken = response.id;
                $.ajax({
                    type: "POST",
                    url: "api/v1/stripepayment",
                    data: {
                        StripeToken: PaymentData.stripeToken,
                        QuoteRef: PaymentData.quoteRef,
                        QuotePass: PaymentData.quotePass,
                        CurrencyId: PaymentData.currencyId,
                        InstalmentData: InstalmentData,
                        PolicyDates: Dates
                    },
                    beforeSend: function () {
                        $("#loadingAnim").css("display", "block");
                    },
                    success: function (data, test) {
                        window.location.href = confirmLink;
                    },
                    complete: function () {
                        $("#loadingAnim").css("display", "none");
                    },
                    error: function (error, test) {
                        $("#cardInfo").css("display", "block");
                        $("#cardInfo").html(error.responseJSON.message);
                    },
                    dataType: "json"
                });
            }
            else {
                $("#cardInfo").css("display", "block");
                $("#cardInfo").html(response.error.message);
            }
        };
        PurchaseController.prototype.CompleteAsAgent = function () {
            this.GetDates();
            this.chargeService.completeAsAgentNew().then(function (data) {
                window.location.href = confirmLink;
            }).catch(function (data) {
                $("#cardInfo").css("display", "block");
                $("#cardInfo").html(data);
            });
        };
        PurchaseController.prototype.GetDates = function () {
            Dates.dateFrom = GetDateTime(this.dates.dateFrom);
            if (this.dates.dateTo != null)
                Dates.dateTo = GetDateTime(this.dates.dateTo);
            Dates.maxDate = this.dates.maxDate;
            Dates.minDate = this.dates.minDate;
            Dates.startingHour = this.dates.startingHour;
            Dates.startingMinute = this.dates.startingMinute;
            Dates.endingHour = this.dates.endingHour;
            Dates.endingMinute = this.dates.endingMinute;
            Dates.dateToRequired = this.dates.dateToRequired;
            Dates.startTimeRequired = this.dates.startTimeRequired;
            Dates.endTimeRequired = this.dates.endTimeRequired;
            Dates.minToMaxDays = this.dates.minToMaxDays;
            Dates.coverDays = this.dates.coverDays;
            Dates.minExpDate = this.dates.minExpDate;
            Dates.maxExpDate = this.dates.maxExpDate;
        };
        PurchaseController.prototype.submitPaymentForm = function (form, valid) {
            if (valid) {
                PaymentData.quoteRef = this.policyData.quoteReference;
                PaymentData.quotePass = this.policyData.password;
                PaymentData.currencyId = this.policyData.locale.currencySymbol;
                confirmLink = "http:" + window.location.href.substring(window.location.protocol.length, window.location.toString().indexOf("purchase")) + 'confirmation/' + PaymentData.quoteRef + '/' + PaymentData.quotePass;
                if (this.policyData.brokerName != null) {
                    this.CompleteAsAgent();
                }
                else {
                    var card = void 0;
                    card = angular.copy(this.paymentCard, card);
                    var addy = this.policyData.addresses[0];
                    card.address_country = addy.country;
                    card.address_line1 = addy.address1;
                    card.address_line2 = addy.address2;
                    card.address_zip = addy.postcode;
                    card.name = addy.contactName;
                    if (this.instalmentData.totalToPay != null) {
                        InstalmentData.totalToPay = this.instalmentData.totalToPay;
                        InstalmentData.deposit = this.instalmentData.deposit;
                        InstalmentData.numberOfPayments = this.instalmentData.numberOfPayments;
                        InstalmentData.eachPayment = this.instalmentData.eachPayment;
                        InstalmentData.sortCode = this.instalmentData.sortCode;
                        InstalmentData.title = this.instalmentData.title;
                        InstalmentData.businessName = this.instalmentData.businessName;
                        InstalmentData.name = this.instalmentData.name;
                        InstalmentData.accountHolder = this.instalmentData.accountHolder;
                        InstalmentData.accountType = this.instalmentData.accountType;
                        InstalmentData.accountConfirm = this.instalmentData.accountConfirm;
                        InstalmentData.accountNumber = this.instalmentData.accountNumber;
                    }
                    this.GetDates();
                    Stripe.setPublishableKey(this.policyData.stripePublishableKey);
                    Stripe.card.createToken(card, this.StripeResponse);
                }
            }
        };
        PurchaseController.routing = function ($routeProvider) {
            $routeProvider.when("/purchase", {
                controller: "PurchaseController",
                templateUrl: "/app/purchase/purchase.html",
                controllerAs: "PurchaseController",
                metadata: {
                    title: "Leisure Insure Personal Details",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
            $routeProvider.when("/purchase/:QUOTEREFERENCE/:PASSCODE", {
                controller: "PurchaseController",
                templateUrl: "/app/purchase/purchase.html",
                controllerAs: "PurchaseController",
                metadata: {
                    title: "Leisure Insure Personal Details",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        PurchaseController.$inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "LocationService", "ChargeService", "UrlToNavigationService", "MetadataService", "$timeout", "CommonDataService", "PriceService", "StripeService"];
        return PurchaseController;
    }());
    LeisureInsure.PurchaseController = PurchaseController;
    angular.module("App")
        .controller("PurchaseController", PurchaseController)
        .config(["$routeProvider", PurchaseController.routing]);
})(LeisureInsure || (LeisureInsure = {}));
var PaymentData = { stripeToken: "", quoteRef: "", quotePass: "", currencyId: "" };
var Dates = {
    dateFrom: "",
    dateTo: "",
    maxDate: "",
    minDate: "",
    startingHour: "",
    startingMinute: "",
    endingHour: "",
    endingMinute: "",
    dateToRequired: false,
    startTimeRequired: false,
    endTimeRequired: false,
    minToMaxDays: 0,
    coverDays: 0,
    maxExpDate: "",
    minExpDate: ""
};
var InstalmentData = {
    totalToPay: 0, deposit: 0, numberOfPayments: 0, eachPayment: 0, accountNumber: 0, sortCode: "",
    accountType: "", title: "", businessName: "", name: "", accountHolder: false, accountConfirm: false
};
var confirmLink = "";
//# sourceMappingURL=PurchaseController.js.map