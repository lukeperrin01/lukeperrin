﻿

module LeisureInsure {
    export interface IChargeInputs {
        coverPK: number;
        listNo: number;
        listOrdinal: number;
        inputPK: number;
        ratePK: number;
        rateValue: string;
        inputString: string;
        lblSumInsured: string;
        sumInsured: number;
        lblNoItems: string;
        noItems: number;
        rateTypeFK: number;
        pageNo: number;
        inputTrigger: number;
        src: string;        
        excess: number;
        indemnity: number;
                
    }
}
