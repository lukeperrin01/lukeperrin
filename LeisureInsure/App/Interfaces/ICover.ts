﻿

module LeisureInsure {
    export interface ICover {
        coverPK: number;
        sectionName: string;
        sectionNumber: number;
        coverName: string;
        coverTip: string;
        coverPKSelected: number;
        coverDetailsCompleted: number;
        pages: number;
        optional: number;
        bitWise: Number;
        isOpen: boolean;
        isDisabled: boolean;
        isPristine: boolean;
        coverEdit: boolean;
        referValue: number;
        inputsForCover: Array<IFlatInput>;              
    }
}
