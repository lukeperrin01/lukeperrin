﻿

module LeisureInsure {
    export interface IEntityTypes {
        EntityTypeNo: number;
        EntityTypeString: string;

    }
}