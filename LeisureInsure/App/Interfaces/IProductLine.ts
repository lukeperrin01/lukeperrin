﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />



module LeisureInsure {
    export interface IProductLine {
        url: string;
        title: string;
        keywords: string;
    }
}