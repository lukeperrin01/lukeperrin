﻿module LeisureInsure {
    export enum ReferralType {
        None = 0,
        Referred = 1,
        Overridden = 2,
    }
}