﻿namespace LeisureInsure {
    export interface IEndorsement {
        id: number;
        text: string;
    }
}