﻿module LeisureInsure {
    export interface IGlobalIrisFields {
        url: string;
        merchantId: string;
        orderId: string;
        amount: number;
        currency: string;
        timestamp: number;
        hash: string;
        autoSettleFlag: number;
    }
}
