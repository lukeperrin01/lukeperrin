﻿
module LeisureInsure {
    export interface IAnswer {
        coverPk: number;
        coverSection: number;
        inputPk: number;
        inputString: string;
        inputName: string;
        listNo: number;
        listOrdinal: number;
        ratePk: number;
        rateValue: string;
        rateVis: number;
        coverName: string;
        cover: any;
        refer: number;
        id: number;
        sumInsured: number;
        territoryCover: string;
        certListItem: number;
        rateTypeFK: number;
    }
}