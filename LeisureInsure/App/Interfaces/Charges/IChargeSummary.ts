﻿module LeisureInsure {
    export interface IChargeSummary {
        chargeSummaryItems: Array<IChargeLine>;
        discounts: Array<IChargeLine>;
        answers: Array<IAnswer>;
        paymentOptions: Array<IPaymentOption>;
        referralItems: Array<IReferralItem>;
        legalFees: number;
        legalFeesAdded: boolean;
        leisureInsureFees: number;
        tax: number;
        excess: number;
        total: number;
        excessWaiver: number;
        excessWaiverAdded: boolean;
        quoteReference: string;
        quoteId: number,
        id:number,
        password: string;
        policyId: number;
        paymentCardId: number;
        localeId: number;
        referralType: ReferralType;
        isAgent: boolean;
        addresses: Array<IAddress>;
        customers: Array<ICustomer>;
        landingPageId?: number;
        lockedForCustomer: boolean;
        endorsements: Array<IEndorsement>;
        documents: Array<IDocument>;
        noInstructors: number;
        policyType: string;
        termsAndConditions: boolean;
        taxRate: number;
        brokerName: string;
	    payOption: number;
        contactName: string;
        contactEmail: string;
        renewal: number;
        netPremium: number;
        dateFrom: string;
        dateTo: string;
        timeFrom: string;
        timeTo: string;
        numberOfDays: number;
        businessDescription: string;
    }

    export interface IChargeLine {
        lineDescription: string;
        price: number;
        commision: number;
        coverPK: number;
    }
}