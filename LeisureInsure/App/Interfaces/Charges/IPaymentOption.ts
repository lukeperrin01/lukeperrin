﻿
module LeisureInsure {
    export interface IPaymentOption {
        name?: string;
        initialPayment?: number;
        subsequentPayments?: number;
        numberOfPayments?: number;
        globalIrisFields?: IGlobalIrisFields;
        id: number;
    }
}
