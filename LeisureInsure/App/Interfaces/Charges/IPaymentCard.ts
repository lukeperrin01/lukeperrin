﻿module LeisureInsure {
    export interface IPaymentCard {
        id?: number;
        name: string;
        addChargeUk?: number;
        addChargeIe?: number;
        addRate?: number;
        imageUrl?: string;
    }
}

