﻿module LeisureInsure {
    export interface IReferralItem {
        id: number;
        rateId: number;
    }
}