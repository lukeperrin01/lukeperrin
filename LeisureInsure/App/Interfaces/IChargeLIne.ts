﻿module LeisureInsure {
    export interface IChargeLine {
        quotePK: number;
        netPremium: number;
        netLegal: number;
        fee: number;
        tax: number;
        taxRate: number;
        brokerCommission: number;
        cardCharge: number;
        coverFK: number;
        payOption: number;
        payOptionCharge: number;
        totalPayable: number;
        sumInsured: number;
        sumInsuredShort: string;
        sectiondescription: string;
        sectionTypeDescription: string;
        description: string;
        net: number;     
        showInSummary: boolean; 
        extraInfo: string; 
    }

}











