﻿
    
module LeisureInsure {
    export interface IRateItemView {

        id: number;
        description: string;
        rateId: number;
        itemTypeId: number;                
    }
}
