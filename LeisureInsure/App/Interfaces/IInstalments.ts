﻿//used with date picker and time directives
module LeisureInsure {
    export interface IInstalments {
        totalToPay: number;
        deposit:number;
        numberOfPayments: number;
        eachPayment: number;
        sortCode: string;
        accountNumber: number;
        accountType: string;
        title: string;
        businessName: string;
        name: string;
        accountHolder: boolean;
        accountConfirm: boolean;
        
    }

}