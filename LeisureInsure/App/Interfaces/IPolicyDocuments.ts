﻿module LeisureInsure {
    export interface IPolicyDocuments {        
        docType: string;
        linkType: string;
        docLink: string;  
        mustRead: boolean;      
    }
}
