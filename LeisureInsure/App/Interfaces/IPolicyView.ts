﻿
    
module LeisureInsure {
    export interface IPolicyView {

        id: number;
        policyName: string;        
        tamType: string;
        ordinal: number;
        leisureInsureFee: number;
        brokerCommission: number;
        commissionPercentage: number;
        tamBrokerCode: string;
        brokerName: string;
        periodYear: number;
        periodDay: number;
        policyType: string;
        policyGroupFK: number;
        premiseRequired: number;
        timeRequired: number;        
        tamOccupation: string;
        clauseCode: string;
        excess: number;
        businessDescription: number;
        certificateBody: string;
        covers: Array<ICoverView>;
        addresses: Array<IAddressView>;
        documents: Array<IPolicyDocuments>;
        locale: ILocale;
        locales: any;       
        browser: string;
        quoteReference: string;
        password: string;
        localeId: number;
        tradeMember: boolean;
        net: number;
        tax: number;
        legalFees: number;
        legalNet: number;
        taxRate: number;        
        legalRate: number;        
        total: number;
        refer: boolean;
        termsAccepted: boolean;
        minStartDate: number;
        maxStartDate: number;
        dates: IDates;     
        legalFeesAdded: boolean;   
        stripePublishableKey: string; 
        instalmentData: IInstalments;
        mdSectionAdded: boolean;
        biSectionAdded: boolean;
        dateFrom: string;
        dateTo: string;
        timeFrom: string;
        timeTo: string;
        CSR: string;
        landingPage: INavigationObject;
    }
}
