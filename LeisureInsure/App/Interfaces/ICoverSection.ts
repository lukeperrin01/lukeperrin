﻿module LeisureInsure {
    export interface ICoverSection {
        sectionName: string;
        sectionNumber: number;
        sectionHazardsCompleted:number;
        sectionTip: string;
        sectionCovers: Array<ICover>;
        isOpen: boolean;
        isDisabled: boolean;
        isPristine: boolean;
        noCovers: number;
        hazardCoverPK: number;            
    }
}

