﻿
    
module LeisureInsure {
    export interface ICoverView {
        id: number;
        policyId: number;
        localeBW: number;
        section: number;
        coverName: string;
        coverHeader: string;
        angularForm: string;
        sectionName: string;
        specification: string;
        coverTip: string;       
        referValue: number;
        coverOrdinal : number;
        rates: Array<IRateView>;
        rateItems: Array<IRateItemView>;  
        itemsAdded: Array<IListItem>;  
        coverType: string;                
        coverRequired: boolean;   
        ernExempt: boolean;
        table: ITable;
        tax: number;
        net: number;
        total: number;
        leisureInsureFee: number;       
        excess: number;
        indemnity: number;
        turnover: number;
        sumInsured: number;
        coverTypeDesc: string;
        requiredParent: number;
        requiredChild: number;
        requiredInfo: string;
        question: string;
        coverTypeId: number;
        showCover: boolean;
        sectionType: boolean;
        sectionHeader: boolean;
        itemCover: boolean;
        otherCoverAdded: number;
        ern: string;
        employeeTotal: number;
        mdAddress: string;
        mdPostCode: string;
        subsidence: number;
        rate: number;
        selected: boolean;
        customDescription:string;
      
    }

    export class CoverView implements ICoverView {

        constructor() {
        }

        id: number;
        policyId: number;
        localeBW: number;
        section: number;
        coverName: string;
        coverHeader: string;
        angularForm: string;
        sectionName: string;
        specification: string;
        coverTip: string;
        referValue: number;
        coverOrdinal: number;
        rates: Array<IRateView>;
        rateItems: Array<IRateItemView>;
        itemsAdded: Array<IListItem>;
        coverType: string;
        coverRequired: boolean;
        ernExempt: boolean;
        table: ITable;
        tax: number;
        net: number;
        total: number;
        leisureInsureFee: number;
        excess: number;
        indemnity: number;
        turnover: number;
        sumInsured: number;
        coverTypeDesc: string;
        requiredParent: number;
        requiredChild: number;
        requiredInfo: string;
        question: string;
        coverTypeId: number;
        showCover: boolean;
        sectionType: boolean;
        sectionHeader: boolean;
        itemCover: boolean;
        otherCoverAdded: number;
        ern: string;
        employeeTotal: number;
        mdAddress: string;
        mdPostCode: string;
        subsidence: number;
        rate: number;
        selected: boolean;
        customDescription: string;

        
    }
}
