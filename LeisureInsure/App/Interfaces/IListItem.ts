﻿

module LeisureInsure {
    export interface IListItem {
        rateId: number;                
        extraInfo: string;
        itemDescription: string;
        length: number;
        width: number;
        quantity: number;    
        premium: number;  
        rate: number;           
        firstRate: number;          
        discountFirstRate: number;
        discountsubsequentRate: number;
        indemnity: number;    
        rateValue: string; 
        rateValueNum: number; 
        extraCover: string;          
        extraCoverId: number; 
        //these are the fields actually 
        //holding the correct charge we will use
        initialCharge: number;   
        subsequentCharge: number; 
        firstCharge: boolean;  
        sumInsured: number;
        //for quote engine
        turnover: number;
        excess: number;
        selected: boolean;
        activity: string;
        wageRoll: number;
        indemnityPeriod: string;
    }
}
