﻿

module LeisureInsure {
    export interface ICoverInputs {
        coverPK: number;
        inputPK: number;
        inputName: string;
        inputOrdinal: number;
        inputType: number;
        inputSelectedPK: number;
        inputString: string;
        inputTriggerFK: number;
        pageNo: number;
        parentPK: number;
        inputVis: number;
        ngRequired: string;
        ngClass: string;
        ngPattern: string;
        listNo: number;
        listOrdinal: number;
        inputStatus: number;
        inputDetails:Array<IInputDetail>;
        inputSelected: IInputDetail;
        unitType: number;
        ratePK: number;
        rateValue: string;
        childBW:number;
    }
}
