﻿

module LeisureInsure {
    export interface IClientEngine {
        id: number;
        tamRef: string;
        name: string;
        attention: string;
        tradingName: string;
        certname: string;
        sector: string;
        street1: string;
        street2: string;
        postCode: string;
        city: string;
        phone1: string;
        phone2: string;
        email: string;        
        agency: string;        
        quotes: Array<IPolicyEngine>;       

    }          
}