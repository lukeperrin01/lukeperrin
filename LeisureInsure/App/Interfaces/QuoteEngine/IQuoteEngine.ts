﻿

module LeisureInsure {
    export interface IQuoteEngine {
        policyDetail: IPolicyEngine;      
        coverDetail: ICoverEngine;
        clientDetail: IClientEngine;
        localeId: number;
    }
}