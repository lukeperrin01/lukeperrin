﻿

module LeisureInsure {
    export interface ICoverEngine {
        desc1: string;        
        desc2: string;        
        startDate: string;
        endDate: string;       
        issuedDate: string;
        purchasedDate: string;
        startDateDt: Date;
        endDateDt: Date;
        issuedDateDt: Date;
        purchasedDateDt: Date;
        startTime: string;
        endTime: string;        
        startTimeDt: Date;
        endTimeDt: Date;       
        purchasedTime: string;        
        purchasedTimeDt: Date; 
        nightclub: string;
        biha: string;
        showman: string;
        eventname: string;
        numVisitors: number;
        numEvents: string;
        instructors: Array<string>;
        cancelSumInsured: number;       
        policyWording: string;
        cancellation: string;               
        
    }
}