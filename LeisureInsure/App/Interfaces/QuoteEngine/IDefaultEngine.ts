﻿

module LeisureInsure {
        
    export interface IDefaultEngine {
        clients: Array<IClientEngine>;
        sectors: Array<ISector>;
        statuslist: Array<IPolicyStatus>;
        payModes: Array<IPayMode>;
        producers: Array<IProducer>;
        clauses: Array<IClause>;
        indemnities: Array<IIndemnity>;
        mdCovers:Array<ICoverView>;
    }

    export interface ISector {
        policyId: number;
        policyName: string;
    }

    export interface IPolicyStatus {
        code: number;
        description: string;
    }

    export interface IPayMode {
        code: number;
        description: string;
    }

    export interface IProducer {
        code: string;
        description: string;
        commission: number;
    }

    export interface IClause {
        id: number;
        code: string;
        name: string;
    }

    export interface IIndemnity {
        id: number;
        indemnityString: string;
        indemnity: number;        
    }
   
}