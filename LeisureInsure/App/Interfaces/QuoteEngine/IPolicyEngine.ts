﻿

module LeisureInsure {
    export interface IPolicyEngine {
        id: number;
        quoteRef: string;
        pword: string;
        type: string;        
        status: string;
        csr: string;
        note: string;
        dep: string;
        policyNumber: string;
        payMode: string;
        billing: string;
        ourCommission: number;
        brokerCommission: number;
        agentCode: string;
        brokerContact: string;
        netPrem: number;
        liFee: number;
        tamOccupation: string;
        //fields on first tab
        cover: ICoverEngine;
        policyType: string;
        producer: IProducer;
        policyClause: IClause;
        clauses: Array<IClause>;
        customClauses:Array<string>;
        customClauseName: string;
        plIndemnity: IIndemnity;
        //level of indeminty
        profIndemnity: IIndemnity;
        prodIndemnity: IIndemnity;
        elIndemnity: IIndemnity;
        //actual covers
        plCover: ICoverView;
        elCover: ICoverView;
        mdCover: ICoverView;
        mdCovers: Array<ICoverView>;
        biCover: ICoverView;
        cancelCover: ICoverView;
    }
}

