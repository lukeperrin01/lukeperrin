﻿module LeisureInsure {
    export interface IChargeSummaryItem {
        coverFK: number;
        coverSection: number;
        invLevel: number;
        lineType: string;
        charge: number;
        net: number;
        rateValue: string;
        section: number;
        rate: number;
        ordinal: number;
        sectionTypeDescription: string;
        sectiondescription: string;
        sumInsured: number;
        quoteId: number;
        indemnity: number;
        id: number;
        lineDescription: string;
    }
}




