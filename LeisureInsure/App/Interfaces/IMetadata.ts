﻿

module LeisureInsure {
    export interface IMetadata {
        title: string;
        description: string;
        keywords: string;
    }
}