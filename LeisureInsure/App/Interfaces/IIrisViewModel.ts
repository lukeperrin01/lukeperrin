﻿

module LeisureInsure {
    export interface IIrisViewModel {
        merchantId: string;
        orderId: string;
        currency: string;
        amount: string;
        timestamp: string;
        sha1Hash: string;
        autoSettleFlag: string;
        result: string;
        message: string;
        authCode: string;
        pasRef: string;
    }
}