﻿//used with date picker and time directives
module LeisureInsure {
    export interface IDates {
        dateFrom: Date;
        dateTo: Date;
        maxDate: string;
        minDate: string;
        startingHour: string;        
        startingMinute: string;
        endingHour: string;
        endingMinute: string;        
        dateToRequired: boolean;
        startTimeRequired: boolean;
        endTimeRequired: boolean;
        minToMaxDays: number;
        coverDays: number;
        maxExpDate: string;
        minExpDate: string;
    }

}