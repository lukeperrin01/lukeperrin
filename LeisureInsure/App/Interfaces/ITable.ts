﻿

module LeisureInsure {
    export interface ITable {
        rowCount: number;
        columnCount: number;
        rows: Array<IRow>;                     
    }

    export interface ICell {
        cellid: number;
        rateid: number;
        description: string;        
        header: boolean;   
        rate: number;
        indemnity: number;
        turnover: number;
        coverid: number;
        divby: number;      
    }

    export interface IRow {       
        selected: boolean;
        rowid: number;        
        cells: Array<ICell>;
    }
}
