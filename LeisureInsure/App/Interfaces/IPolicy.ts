﻿

module LeisureInsure {
    export interface IPolicy {
        policyPK: number;
        policyBW: number;
        policyName: string;
        periodYear: number;
        periodDay: number;
        premiseRequired: number;
        timeRequired: number;
        coverBW: Number;
        minDate: string;
        maxDate: string;
    }
}
