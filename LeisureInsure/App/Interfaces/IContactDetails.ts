﻿module LeisureInsure {
    export interface IContactDetail {
        contact: Array<ICustomer>;
        address: Array<IAddress>;
        quoteReference: string;
        passCode: string;
        contactName: string;
        contactEmail: string;
    }

}