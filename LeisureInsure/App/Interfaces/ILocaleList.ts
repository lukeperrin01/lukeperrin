﻿module LeisureInsure {
    export interface ILocaleList {
        locales: Array<ILocale>;
        selectedLocale: ILocale;
    }
}