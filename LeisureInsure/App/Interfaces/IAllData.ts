﻿
    
module LeisureInsure {
    export interface IAllData {
        navMessage: string;
        quotePk: number;
        progress: number;
        currentPage: number;
        policyPk: number;
        iris: IIrisViewModel;
        dateFrom: Date;
        dateTo: Date;
        locales: any;
        policies: any;
        covers: any;
        inputs: Array<any>;
        saveInputs: Array<any>;
        inputsFlat: Array<IFlatInput>;
    }
}
