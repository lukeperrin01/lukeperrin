﻿
    
module LeisureInsure {
    export interface IAddressView {
        id: number;
        email: string;
        entityType: string;
        entityName: string;
        contactName: string;
        firstName: string;
        lastName: string;
        telephone: string;
        postcode: string;
        address1: string;
        address2: string;
        address3: string;
        town: string;
        county: string;
        country: string;        
        locale: string;
    }



    export class AddressView implements IAddressView {

        constructor() {
        }

        id: number;
        email: string;
        entityType: string;
        entityName: string;
        contactName: string;
        firstName: string;
        lastName: string;
        telephone: string;
        postcode: string;
        address1: string;
        address2: string;
        address3: string;
        town: string;
        county: string;
        country: string;
        locale: string;
    }

   
}
