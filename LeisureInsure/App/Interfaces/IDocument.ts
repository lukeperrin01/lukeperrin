﻿namespace LeisureInsure {
    export interface IDocument {
        description: string;
        url: string;
    }
}