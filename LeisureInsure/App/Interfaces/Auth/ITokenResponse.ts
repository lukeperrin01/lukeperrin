﻿

module LeisureInsure {
    export interface ITokenResponse {
        access_token: string;
        token_type: string;
        expired_in: number;
    }
}