﻿

module LeisureInsure {
    export interface IToken {
        unique_name: string;
        user_type: number;
        exp: number;
    }
}