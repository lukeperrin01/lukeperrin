﻿module LeisureInsure {
    export interface ICustomer {
        contactType?: any;
        contactPK?: number;
        contactName?: string;
        
        firstName?: string;
        lastName?: string;
        pWord?: string;
        telephone?: string;
        email?: string;
        entityType: string;
        entityName?: string;
        tamClientCode?: string;
        lock?: number;
        quotePK?: string;

    }

}
