﻿module LeisureInsure {
    export interface IAddress {
        addressID?: number;
        contactFK?: number;
        addressPK?: number;
        address1?: string;
        address2?: string;
        address3?: string;
        town?: string;
        county?: string;
        country?: string;
        locale?: string;
        quoteFK?: number;
        postcode?: string;
        addressType?: number;
    }

}
