﻿

module LeisureInsure {
    export interface ILocale {
        localePK: number;
        strLocale: string;
        flagUrl: string;
        inputTriggerFK: number;
        currencySymbol: string;
        country: string;
        territory: string;
        localeBW: number;
        localePKselected: number;
        showFlag: boolean;
        multiplier: number;
        exchangeRate: number;
        localeRefer: number;
        tamAgency: number;
    }
}

