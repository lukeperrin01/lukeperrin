﻿

module LeisureInsure {
    export interface IInputDetail {
        coverPK: number;
        childBW: number;
        inputPK: number;
        ratePK: number;
        rateName: string;
        rate: number;
        rateValue: string;
        threshold: number;
        excess: number;
        rateLabel: string;
        rateTip: string;
        refer: number;
        divBy: number;
        rateOrdinal: number;
        parentPK: number;
        rateVis: number;
        inputSelectedPK: number;
        listNo: number;
        listOrdinal: number;
        unitTypex: string;
        tableChildren: number;
        tableFirstColumn: number;
    }
}
