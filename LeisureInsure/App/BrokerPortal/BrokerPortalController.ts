﻿module LeisureInsure.Admin {
    export class BrokerPortalController {

        static $inject = ["$http", "$scope", "AuthenticationService", "$rootScope", "$location", "$routeParams", "PostCodeService",  "ChargeService","$q"];
        http: ng.IHttpService;
        location: any;
        scope: any;
        rootScope: any;
        q: ng.IQService;
        postCodeService: PostCodeService;
        chargeService: ChargeService;
        certlink: string;
        quotelink: string;
        checkoutlink: string;
        authenticationService: AuthenticationService;
        constructor($http, $scope, authenticationService: AuthenticationService, $rootScope, $location, $routeParams, PostCodeService,  chargeService,$q) {
            this.authenticationService = authenticationService;
            this.rootScope = $rootScope;
            this.postCodeService = PostCodeService;
            this.chargeService = chargeService;
            this.http = $http;
            this.q = $q;
            this.scope = $scope;
            $("#allGifs").css("display", "block");
            this.scope.alerts = [];
            this.brokerName = authenticationService.name;
            this.location = $location;
            var link = "";
            if (window.location.href.indexOf("localhost") > -1) {
                this.certlinkbase = "http://" + window.location.hostname + ":" + window.location.port;
                this.quotelinkbase = "http://" + window.location.hostname + ":" + window.location.port;
                this.checkoutlinkbase = "http://" + window.location.hostname + ":" + window.location.port;
            }
            else {
                this.certlinkbase = "http://" + window.location.hostname;
                this.quotelinkbase = "https://" + window.location.hostname;
                this.checkoutlinkbase = "https://" + window.location.hostname;
            }


            this.getpolicies();

            if ($routeParams["quotePK"] !== undefined) {
                var quotePK = $routeParams["quotePK"];
                
                this.getQuote(quotePK);
            }
            if (this.location.path().indexOf('/brokerQuotes')>-1) {
                this.getBrokerQuotes();
            }
            if (this.location.path().indexOf('/brokerDetails') > -1) {
                this.getBrokerDetails();
            }
 

        }
        quoteRef: string;
        brokerName: string;
        brokerQuotes: Array<IBrokerQuote>;
        brokerQuotesView: Array<IBrokerQuote>;
        brokerDetails: IBrokerDetail;
        viewPurchased: boolean;
        policies: Array<IPolicy>;
        selectedPolicy: IPolicy;
        quote: IQuote;
        addressLookups: Array<LeisureInsure.IAddress>;
        selectedAddress: LeisureInsure.IAddress;
        addresses: Array<LeisureInsure.IAddress>;
        customers: Array<LeisureInsure.ICustomer>;
        contactDetail: IContactDetail;
        contactEmail: string;
        contactName: string;
        quotePK: number;
        instructors: number;
        password: string;
        certlinkbase: string;
        quotelinkbase: string;
        checkoutlinkbase: string;
        getpolicies() {
            this.http({
                url: "/api/v1/data/getPolicies",
                method: "GET",
                params: { strLocale: 0 }
            })
                .success((data: Array<IPolicy>) => {
                    this.policies = data;
                })
                .catch((data: any) => {
                    // Error
                });
        }


        submitCustomer(): ng.IPromise<any> {
            var deferred = this.q.defer();
            var qr = this.quoteRef;
            this.quotePK = this.quote.id;
            var addx = [];
            var custx = [];
            this.customers[0].contactType = 1;

            custx.push(this.customers[0]);

            //var eType = this.customer[0].entityType;
            if (this.customers[0].entityType != "Customer" &&
                (this.customers[0].entityName === undefined || this.customers[0].entityName === "")) {
                deferred.reject("Please ensure that the entity name is completed");
            }
            else {
                //if (this.customers.length > 1) {
                //this.customer.splice(1, this.customer.length - 1);
                var i = 0;

                _.each(this.customers, (cust: LeisureInsure.ICustomer) => {
                    if (i == 0 && this.contactEmail == '') {
                        this.contactEmail = cust.email;
                    }
                    if (i == 0 && this.contactName == '') {
                        this.contactName = cust.firstName;
                    }
                    if (i > 0) {
                        //alert("MORE THAN ONE CUSTOMER");
                        cust.contactType = 1;
                        cust.quotePK = this.customers[0].quotePK;
                        cust.entityType = "Customer";
                        custx.push(cust);
                    }
                    i++;
                    this.instructors = i;
                });
                if (this.addresses.length > 1) {
                    //this.address.splice(1, this.address.length - 1);
                }
                var loc = 'en-GB'
                if (this.quote.localeId == 2) {
                    loc = 'ga-IE'
                }
                this.addresses[0].quoteFK = this.quotePK;
                this.addresses[0].addressType = 1;
                this.addresses[0].locale = loc;

                addx.push(this.addresses[0]);

                this.contactDetail = {
                    contact: custx,
                    address: addx,
                    quoteReference: this.quote.quoteReference,
                    passCode: this.quote.password,
                    contactName: this.quote.contactName,
                    contactEmail: this.quote.contactEmail
                };

                this.chargeService.addCustomerDetails(this.contactDetail)
                    .then(() => {
                        deferred.resolve();
                    }, response => {
                        this.scope.alerts.push({
                            type: "danger",
                            msg: response
                        });
                        deferred.reject(response);
                    });
             }


            document.cookie = "customers=" + JSON.stringify(this.customers);
            document.cookie = "addresses=" + JSON.stringify(this.addresses);
            var x = document.cookie;
            return deferred.promise;
        }


        getQuote(quotePK) {
            this.http({
                url: 'api/v1/brokerPortal/getQuote/{quotePK}',
                method: "GET",
                params: { quotePK: quotePK }
            })
                .success((data: IQuote) => {
                    this.quote = data;
                    if (this.quote.addresses != null) {
                        this.addresses = this.quote.addresses;
                    }
                    else {
                        this.addresses = [];
                    }
                    if (this.quote.customers != null) {
                        this.customers = this.quote.customers;
                    }
                    else {
                        this.customers = [];
                    }
                    

                    this.selectedPolicy = _.findWhere(this.policies, {
                        id: this.quote.policyId
                    });
                    this.password = this.quote.password;
                    this.contactEmail = this.quote.contactEmail;
                    this.contactName = this.quote.contactName;
                    this.instructors = this.quote.noInstructors;
                    this.certlink = this.certlinkbase + "/certificate/" + this.quote.quoteReference + "/" + this.quote.password;
                    this.quotelink = this.quotelinkbase + "/quote/" + this.quote.quoteReference + "/" + this.quote.password;
                    this.checkoutlink = this.checkoutlinkbase + "/checkout/" + this.quote.quoteReference + "/" + this.quote.password;
                })
                .catch((data: any) => {
                    // Error
                });
        }
        getAddress(): void {
            if (this.addresses[0] != null) {
                if (this.addresses[0].postcode != null) {

                    this.postCodeService.GetAddresses(this.addresses[0].postcode)
                        .then((addresses) => {

                            this.addressLookups = [];

                            _.each(addresses, (address: any) => {

                                let addy = <LeisureInsure.IAddress>{};

                                addy.addressID = address.addressID;
                                addy.address1 = address.addressLine1;
                                addy.address2 = address.addressLine2;
                                addy.town = address.town;
                                addy.county = address.county;
                                addy.postcode = address.postCode;

                                this.addressLookups.push(addy);
                            });
                            //get first address and assign to our select, also update form
                            this.selectedAddress = this.addressLookups[0];
                            this.addresses[0] = this.selectedAddress;
                        });

                }
            }
        }

        getBrokerQuotes() {
            this.rootScope.searchingQuote = true;
            this.http({
                url: "/api/v1/brokerPortal/GetBrokerQuotes/",
                method: "GET",
                params: { broker: this.brokerName, quoteRef: this.quoteRef }
            })
                .success((data: Array<IBrokerQuote>) => {
                    this.brokerQuotes = [];
                    this.brokerQuotes = data;
                    this.rootScope.searchingQuote = false;
                })
                .catch((data: any) => {
                    // Error
                    this.rootScope.searchingQuote = false;
                });

        }
        getBrokerDetails() {
            //alert(this.loggedIn);
            this.http.get(
                `api/v1/brokerPortal/${this.brokerName}`
            )
                .then((response: any) => {

                    this.brokerDetails = response.data;
                },
                response => {
                    this.scope.alerts.push({
                        type: "danger",
                        msg: "Could not find the broker"
                    });
                });
        }

        updateBroker() {
            this.http.post(
                `api/v1/brokerLive`,
                {
                    locked: false,
                    entityName: this.brokerDetails.entityName,
                    firstname: this.brokerDetails.firstname,
                    surname: this.brokerDetails.lastname,
                    telephone: this.brokerDetails.telephone,
                    email: this.brokerDetails.email,
                    fsaNumber: this.brokerDetails.fsaNumber,
                    address1: this.brokerDetails.address1,
                    address2: this.brokerDetails.address2,
                    city: this.brokerDetails.city,
                    county: this.brokerDetails.county,
                    postcode: this.brokerDetails.postcode,
                    tamCode:this.brokerName
                })
                .then(() => {
                    this.scope.alerts.push({
                        type: "success",
                        msg: "Broker updated"
                    });
                    eval("ShowUIDialog('Details updated');");
                },
                response => {
                    this.scope.alerts.push({
                        type: "danger",
                        msg: `There was a problem updating the broker: ${response.statusText}`
                    });
                });
        }

        ShowQuote() {
            window.open(this.quotelink);
        }

        ShowCert() {
            window.open(this.certlink);
        }

        showCheckout() {
            window.open(this.checkoutlink);
        }

        static routing($routeProvider) {
            $routeProvider.when("/brokerPortal",
                {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/brokerPortal.html",
                    controllerAs: "brokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/brokerQuotes",
                {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/brokerQuotes.html",
                    controllerAs: "brokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/brokerDetails",
                {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/brokerDetails.html",
                    controllerAs: "brokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
            $routeProvider.when("/getQuote/:quotePK",
                {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/quoteDetails.html",
                    controllerAs: "brokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
        }
    }

    angular.module("App")
        .controller("BrokerPortalController", BrokerPortalController)
        .config(["$routeProvider", BrokerPortalController.routing]);
}