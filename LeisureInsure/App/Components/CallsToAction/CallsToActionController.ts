﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />

module LeisureInsure {
    export class CallsToActionController {
        static $inject = ["$location", "$http", "$q"];
        constructor($location, $http, $q) {
            this.location = $location;
            this.http = $http;
            this.thankYouVisible = false;
            this.buttonsDisabled = false;
            this.q = $q;
            this.productLines();
            this.search = {url:"", title:"",keywords:""};
        }

        location: ng.ILocationService;
        http: ng.IHttpService;
        q: ng.IQService;

        name: string;
        company: string;
        phone: string;
        email: string;
        message: string;
        marketing: string;

        thankYouVisible: boolean;
        buttonsDisabled: boolean;

        search: IProductLine;
        async: Array<IProductLine>;

        searchSelected() {
            this.location.path("/quote/" + this.search.url);
        }

        productLines(): any {
            var defer = this.q.defer();
            this.http.get("api/v1/search").then(response => {
                this.async = <Array<IProductLine>>angular.copy(response.data);
                defer.resolve(this.async);
            });
            return defer.promise;
        }

        submitMessage(valid: boolean): void {
            if (valid) {
                this.buttonsDisabled = true;
                var contactModel = {
                    name: this.name,
                    phone: this.phone,
                    email: this.email,
                    message: this.message,
                    HowDidYouFindUs: this.marketing,
                    company: this.company
                }
                this.http.post("/api/v1/communications/contactus", contactModel).then(() => {
                    this.location.path("/contact-us/thank-you");
                }, () => {
                    this.buttonsDisabled = false;
                });
            }
        }

        callback(valid: boolean): void {
            if (valid) {
                this.buttonsDisabled = true;
                var callbackModel = {
                    number: this.phone,
                    name: this.name,
                    message: this.message
                }
                this.http.post("/api/v1/communications/callmeback", callbackModel).then(() => {
                    this.showThankYou();
                }, () => {
                    this.buttonsDisabled = false;
                });
            }
        }

        showThankYou(): void {
            this.thankYouVisible = true;
        }
    }

    angular.module("App")
        .controller("CallsToActionController", CallsToActionController)
        .component("callsToAction", {
            templateUrl: "/app/components/callstoaction/cta.html",
            controller: CallsToActionController,
            controllerAs: "callsToActionController"
        });
}

