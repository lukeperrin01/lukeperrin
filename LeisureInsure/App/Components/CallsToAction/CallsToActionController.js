var LeisureInsure;
(function (LeisureInsure) {
    var CallsToActionController = (function () {
        function CallsToActionController($location, $http, $q) {
            this.location = $location;
            this.http = $http;
            this.thankYouVisible = false;
            this.buttonsDisabled = false;
            this.q = $q;
            this.productLines();
            this.search = { url: "", title: "", keywords: "" };
        }
        CallsToActionController.prototype.searchSelected = function () {
            this.location.path("/quote/" + this.search.url);
        };
        CallsToActionController.prototype.productLines = function () {
            var _this = this;
            var defer = this.q.defer();
            this.http.get("api/v1/search").then(function (response) {
                _this.async = angular.copy(response.data);
                defer.resolve(_this.async);
            });
            return defer.promise;
        };
        CallsToActionController.prototype.submitMessage = function (valid) {
            var _this = this;
            if (valid) {
                this.buttonsDisabled = true;
                var contactModel = {
                    name: this.name,
                    phone: this.phone,
                    email: this.email,
                    message: this.message,
                    HowDidYouFindUs: this.marketing,
                    company: this.company
                };
                this.http.post("/api/v1/communications/contactus", contactModel).then(function () {
                    _this.location.path("/contact-us/thank-you");
                }, function () {
                    _this.buttonsDisabled = false;
                });
            }
        };
        CallsToActionController.prototype.callback = function (valid) {
            var _this = this;
            if (valid) {
                this.buttonsDisabled = true;
                var callbackModel = {
                    number: this.phone,
                    name: this.name,
                    message: this.message
                };
                this.http.post("/api/v1/communications/callmeback", callbackModel).then(function () {
                    _this.showThankYou();
                }, function () {
                    _this.buttonsDisabled = false;
                });
            }
        };
        CallsToActionController.prototype.showThankYou = function () {
            this.thankYouVisible = true;
        };
        CallsToActionController.$inject = ["$location", "$http", "$q"];
        return CallsToActionController;
    }());
    LeisureInsure.CallsToActionController = CallsToActionController;
    angular.module("App")
        .controller("CallsToActionController", CallsToActionController)
        .component("callsToAction", {
        templateUrl: "/app/components/callstoaction/cta.html",
        controller: CallsToActionController,
        controllerAs: "callsToActionController"
    });
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=CallsToActionController.js.map