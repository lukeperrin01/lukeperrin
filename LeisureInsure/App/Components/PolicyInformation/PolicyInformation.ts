﻿/// <reference path="../../../scripts/typings/angularjs/angular.d.ts" />



module LeisureInsure.Components {
    export class PolicyInformationController {
        static $inject = ["CoverService", "PolicyService", "PolicyNameService", "CoverNameService"];
        constructor(coverService, policyService, policyName, coverName) {
            this.coverService = coverService;
            this.policyService = policyService;
            this.policyName = policyName;
            this.coverName = coverName;
        }

        coverService: CoverService;
        policyService: PolicyService;
        policyName: PolicyNameService;
        coverName: CoverNameService;

        policyInformationTemplate() {
            var policyId = this.policyService.selectedPolicyId;
            var policyName = this.policyName.nameForId(policyId);
            var url = "/app/components/policyinformation/html/policies/" + policyName + "/policy.html";
            return url;
        }

        coverInformationTemplate() {
            var coverId = this.coverService.selectedCoverId;
            var policyId = this.policyService.selectedPolicyId;
            var policyName = this.policyName.nameForId(policyId);
            var coverName = this.coverName.nameForId(coverId);
            var url = "/app/components/policyinformation/html/policies/" + policyName + "/" + coverName + ".html";
            return url;
        }
    }

    angular.module("App").controller("PolicyInformationController", PolicyInformationController);
}
