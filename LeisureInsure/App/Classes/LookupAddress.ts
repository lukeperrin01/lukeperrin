﻿

module LeisureInsure {
    export class LookupAddress {       
        addressID: number;
        address1: string;
        address2: string;
        town: string;
        county: string;
        postcode: string;

        constructor(id: number, address1: string, address2: string, town: string, county: string, postcode: string) {
            this.addressID = id;
            this.address1 = address1;
            this.address2 = address2;
            this.town = town;
            this.county = county;
            this.postcode = postcode;
        }
    }
}
