﻿module LeisureInsure {
    export class StaticPageLoginController {
        static $inject = ["$location", "$rootScope", "AuthenticationService"];
        authenticationService: AuthenticationService;
        location: ng.ILocationService;
        rootScope: any;

        constructor($location, $rootScope,
            authenticationService: AuthenticationService) {

            this.authenticationService = authenticationService;
            this.rootScope = $rootScope;
            this.location = $location;
        }

        username: string;
        password: string;
        errorMessage: string;
        login() {           
            this.authenticationService.login(this.username, this.password).then((response: ITokenResponse) => {
                var redirectUrl = this.rootScope.redirectUrl;
                if (redirectUrl) {
                    this.location.path(redirectUrl);
                } else {
                    this.location.path("/");
                }
            }, () => {
                this.errorMessage = "We could not validate the username and password entered. Please try again or contact us on 01993 700 761";
            });
        }
    }

    angular.module("App")
        .controller("StaticPageLoginController", StaticPageLoginController);
}