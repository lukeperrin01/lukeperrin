﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />



module LeisureInsure {
    export class MetadataController {
        static $inject = ["MetadataService"];

        constructor(metadataService: MetadataService) {
            this.metadataService = metadataService;
        }

        metadataService: MetadataService;

        title(): string { return this.metadataService.title; }
        description(): string { return this.metadataService.description; }
        keywords(): string { return this.metadataService.keywords; }

        ogUrl(): string { return this.metadataService.ogUrl; }
        ogTitle(): string { return this.metadataService.ogTitle; }
        ogDescription(): string { return this.metadataService.ogDescription; }
        ogType(): string { return this.metadataService.ogType; }
        ofImage(): string { return this.metadataService.ofImage; }

        twitterCard(): string { return this.metadataService.twitterCard; }
        twitterTitle(): string { return this.metadataService.twitterTitle; }
        twitterDescription(): string { return this.metadataService.twitterDescription; }
        twitterSite(): string { return this.metadataService.twitterSite; }
        twitterImage(): string { return this.metadataService.twitterImage; }
    }
    
    angular.module("App").controller("MetadataController", MetadataController);
}

