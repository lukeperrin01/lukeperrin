﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/angular-ui-bootstrap/angular-ui-bootstrap.d.ts" />



module LeisureInsure {
    export class UserController {
        static $inject = ["$rootScope", "AuthenticationService", "$uibModal","$location"];
        authenticationService: AuthenticationService;
        location: any;
        modalService: angular.ui.bootstrap.IModalService;
        modalInstance: angular.ui.bootstrap.IModalServiceInstance;

        constructor($rootScope, authenticationService: AuthenticationService, $uibModal, $location) {
        this.modalService = $uibModal;
        this.location = $location;
            this.authenticationService = authenticationService;
            this.authenticationService.onLoginStatusChanged($rootScope, () => {
                this.update();
            });
            this.update();
        }

        username: string;
        password: string;

        name: string;
        isAuthenticated: boolean;

        showLogin() {
            this.modalInstance = this.modalService.open({
                animation: true,
                templateUrl: "login-dialog.html",
                controller: "LoginController",
                controllerAs: "modal"
            });

            this.modalInstance.result.then(() => {
                this.update();
            });
        }

        logout(){ 
            this.authenticationService.logout();
            this.update();
            if (this.location.path().indexOf('/brokerQuotes') > -1) {
                this.location.path('https://leisureinsure.co.uk/');
            }
            if (this.location.path().indexOf('/brokerDetails') > -1) {
                this.location.path('https://leisureinsure.co.uk/');
            }
            if (this.location.path().indexOf('/brokerPortal') > -1) {
                this.location.path('https://leisureinsure.co.uk/');
            }
        }

        private update() {
            this.name = this.authenticationService.name;
            this.isAuthenticated = this.authenticationService.isAuthenticated();
        }
    }

    export class LoginController {
        static $inject = ["$location", "$rootScope", "AuthenticationService", "$uibModalInstance"];
        authenticationService: AuthenticationService;
        location: ng.ILocationService;
        rootScope: any;

        modalInstance: angular.ui.bootstrap.IModalServiceInstance;
        constructor($location, $rootScope,
            authenticationService: AuthenticationService,
            $uibModalInstance) {

            this.authenticationService = authenticationService;
            this.modalInstance = $uibModalInstance;
            this.rootScope = $rootScope;
            this.location = $location;
            $("#allGifs").css("display", "block");
        }

        username: string;
        password: string;
        errorMessage: string;
        login() {
            this.rootScope.loggingin = true;
            this.authenticationService.login(this.username, this.password).then((response: ITokenResponse) => {
            }).then(() => {
                this.modalInstance.close();
                this.rootScope.loggingin = false;
            }, () => {
                this.errorMessage = "We could not validate the username and password entered. Please try again or contact us on 01993 700 761";
                this.rootScope.loggingin = false;
            });
        }

        cancel() {
            this.modalInstance.close();
        }
    }

    angular.module("App")
        .controller("LoginController", LoginController)
        .controller("UserController", UserController);
}