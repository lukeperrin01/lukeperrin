﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />

module LeisureInsure {
    export class LocaleController {
        static $inject = ["$rootScope", "LocationService", "PolicyUpdateDataService", "$location"];

        localeService: LocationService;
        location: any;
        constructor($rootScope, locationService, policyUpdateDataService: PolicyUpdateDataService, $location) {
            this.location = $location;
            this.localeService = locationService;
            locationService.initialised.then((service: LocationService) => {
                //service.getlocals("");
                //this.locales = this.localeService.locales;
                //this.selectedLocale = this.localeService.selectedLocale;

                this.locales = service.locales;
                this.selectedLocale = service.selectedLocale;
            });

            this.localeService.onSelectedLocaleChanged($rootScope, locale => {
                this.selectedLocale = this.localeService.selectedLocale;
            });

            this.policyUpdateDataService = policyUpdateDataService;

            this.irelandMessage = false;
        }

        policyUpdateDataService: PolicyUpdateDataService;
        locales: Array<ILocale>;
        selectedLocale: ILocale;
        irelandMessage: boolean;

        brokerPortal() {
            alert("HIT");
            this.location.path("\brokerPortal");
        }


        selectLocale(locale: ILocale) {
            this.selectedLocale = locale;
            this.localeService.getlocales(locale.strLocale,1);

            //this.policyUpdateDataService.initialise(this.selectedLocale.strLocale, 0, 0);
        }

        ClickedFlag() {
            if (this.selectedLocale.showFlag)
                ShowDialog();
        }

        SelectUK() {

            this.irelandMessage = false;

            var uklocale = this.locales[0];
            this.selectedLocale = uklocale;
            this.localeService.getlocales(uklocale.strLocale,1);

            //this.policyUpdateDataService.initialise(uklocale.strLocale, 0, 0);

            HideDialog();
        }

        SelectIreland() {

            if (window.location.href.indexOf("localhost") > -1 || window.location.href.indexOf("preprod") > -1) {
                var eirlocale = this.locales[1];
                this.selectedLocale = eirlocale;
                this.localeService.getlocales(eirlocale.strLocale,1);
                HideDialog();
            }
            else
                this.irelandMessage = true;

        }
        static routing($routeProvider) {
            $routeProvider.when("/brokerPortal",
                {
                    controller: "BrokerPortalController",
                    templateUrl: "/app/brokerportal/brokerPortal.html",
                    controllerAs: "BrokerPortalController",
                    metadata: {
                        title: "Broker Portal",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });

        }
    }

    angular.module("App").controller("LocaleController", LocaleController);
}

function ShowDialog() {

    $("#countryDialog").css("display", "block");

}

function HideDialog() {

    $("#countryDialog").css("display", "none");

}


