﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/angularjs/angular-route.d.ts" />

module LeisureInsure {
    export class CustomerDetailsPageController {
        static $inject = ["LocationService", "ChargeService"];
        constructor(locationService, chargeService) {
            this.locationService = locationService;
            this.chargeService = chargeService;
            this.quotePk = chargeService.quoteId;
            this.createEntityTypes();

            locationService.initialised.then(locationService => {
                this.selectedLocale = locationService.selectedLocale;
            });
        }
        locationService: LocationService;
        chargeService: ChargeService;
        customer: Array<ICustomer>;
        address: Array<IAddress>;
        contactDetail: IContactDetail;
        selectedLocale: ILocale;
        quotePk: number;
        //quotePk: string = "100035";
        entityTypes: Array<IEntityTypes>;
        entityTypeSelected: IEntityTypes;
        contactEmail: string;
        contactName: string;

        createEntityTypes() {
            //alert("hit entity");
            var e = [];
            for (var i = 1; i < 6; i++) {

                switch (i) {
                    case 1:
                        e.push({
                            EntityTypeNo: i,
                            EntityTypeString: "The person named below"

                        });
                        break;
                    case 2:
                        e.push({
                            EntityTypeNo: i,
                            EntityTypeString: "Limited Company"

                        });
                        break;
                    case 3:
                        e.push({
                            EntityTypeNo: i,
                            EntityTypeString: "Trading As"

                        });
                        break;
                    case 4:
                        e.push({
                            EntityTypeNo: i,
                            EntityTypeString: "Committee"

                        });
                        break;
                    case 5:
                        e.push({
                            EntityTypeNo: i,
                            EntityTypeString: "Charity"

                        });
                        break;
                };

            };

            this.entityTypes = e;

        };

        submitCustomer(contacts, address) {
            this.entityTypeSelected.EntityTypeNo = 1;
            this.entityTypeSelected.EntityTypeString = "The person named below";
            var contactx = [];
            angular.forEach(this.customer, function (c, i) {
                if (i === 0) {
                    c.contactType = 1;
                }
                else {
                    c.contactType = -1;
                };
                c.contactPK = 0;
                c.contactName = "";
                c.pWord = "";
                c.entityName = "";
                c.tamClientCode = "";
                c.lock = 0;
                //c.EntityType = c.EntityType;
                contactx.push({
                    ContactPK: 0,
                    ContactName: "BOB",
                    PWord: "BOB",
                    EntityName: "BOB",
                    TamClientCode: "x",
                    Lock: 0,
                    EntityType: c.entityType,
                    FirstName: c.firstName,
                    LastName: c.lastName,
                    Telephone: c.telephone,
                    Email: c.email,
                    ContactType: c.contactType,
                    QuotePK: this.quotePk,
                    EntityTypeName: c.entityTypeName
                });
            });
            //address.Locale = this.selectedLocale.strLocale;
            //address.ContactFK = 0;
            //address.AddressPK = 0;
            //address.DateTime = null;
            //address.Lock = 0;
            var add = [{
                ContactFK: 0,
                AddressPK: 0,
                Address1: this.address[0].address1,
                Address2: this.address[0].address2,
                Address3: this.address[0].address3,
                Town: this.address[0].town,
                County: this.address[0].county,
                Country: "UK",
                PostCode: this.address[0].postcode,
                Locale: this.selectedLocale.strLocale,
                QuoteFK: this.quotePk
            }];

            this.contactDetail = {
                contact: this.customer,
                address: this.address,
                passCode: "",
                quoteReference: "",
                contactEmail: this.contactEmail,
                contactName:this.contactName
            };
            this.chargeService.addCustomerDetails(this.contactDetail)
                .then(() => {
                }, () => {
                    alert("homeController:submitCustomer_Error");
                }).then(() => {
                    //$scope.isBusy = false;
                    //if ($rootScope.iris.go !== 1) {
                    //    $rootScope.iris.go = 1;

                    //}
                    //if ($rootScope.payOption === 3) {
                    //    $rootScope.progress = 4;
                    //};
                });
        };

        customerLoadTest() {

            var contact = [];
            contact.push({
                ContactPK: 0,
                ContactName: "TestOne",
                PWord: "",
                EntityName: "Bob",
                TamClientCode: "x",
                Lock: 0,
                EntityType:2,
                FirstName: "test",
                LastName: "test",
                Telephone: "01865 847166",
                Email: "test@test.com",
                ContactType: 1,
                QuotePK: this.quotePk,
                EntityTypeName: "Person Name Below"
            });
            var add = [{
                ContactFK: 0,
                AddressPK: 0,
                Address1: "TestAddress1",
                Address2: "TestAddress2",
                Address3: "TestAddress3",
                Town: "TestTown",
                County: "TestCounty",
                Country: "UK",
                PostCode: "OX5 2TQ",
                Locale: this.selectedLocale.strLocale,
                QuoteFK: this.quotePk,
                AddressType:3
            }];
            this.customer = contact;
            this.address = add;
            //$rootScope._Customer = contact;
            //$rootScope._Address = add[0];
        };


    }

    angular.module("App").controller("CustomerDetailsPageController", CustomerDetailsPageController);
}