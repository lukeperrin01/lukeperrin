﻿/// <reference path="../../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../../scripts/typings/underscore/underscore.d.ts" />

module LeisureInsure {
    export class LandingController {
        static $inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "PolicyUpdateDataService", "LocationService", "UrlToNavigationService", "MetadataService", "$timeout", "CommonDataService"];
        filter: any; // ng.IRootScopeService;
        window: ng.IWindowService;
        location: any;
        scope: any;
        rootScope: any;
        timeout: any;
        http: ng.IHttpService;
        q: ng.IQService;
        policyUpdateDataService: PolicyUpdateDataService;
        locationService: LocationService;
        chargeService: ChargeService;
        heroImageSrc: string;
        landingPageDescription: string;                
        descriptionSrc: string;
        //have we attempted to get a price? used for validation              
        navigationData: INavigationObject;
        policyForm: any;
        coverOptions:Array<string>
        routeParams: any;
        coverTable: ITable;

        constructor($http, $q: ng.IQService, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies,
            policyUpdateDataService: PolicyUpdateDataService, locationService,urlToNavigationService: UrlToNavigationService, metadataService: MetadataService, $timeout, commonDataService: CommonDataService) {
            var initialisationPromises = [];
            var navigationData: INavigationObject;
            this.heroImageSrc = "";
            this.landingPageDescription = "";
            this.policyUpdateDataService = policyUpdateDataService;
            this.locationService = locationService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;                      
            //these were hidden so they dont show on home page
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;         
            this.routeParams = $routeParams;

            console.log("PLconstructor called");

            locationService.onSelectedLocaleChanged($scope, (locale: ILocale) => {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            })

            if (this.locationService.selectedLocale != null) {
                this.locationService.selectedLocale.showFlag = false;
            }
          
            urlToNavigationService.translateUrlNew("POLICYDESCRIPTION")
                .then((data: INavigationObject) => {
                    this.navigationData = data;
                    if (this.navigationData.newPolicyId > 0) {

                        metadataService.setMetadata(this.navigationData.title,
                            this.navigationData.description,
                            this.navigationData.keywords);

                        var table: ITable; 
                        var rows: Array<IRow>; rows = [];
                        var cells: Array<ICell>; cells = [];
                        table = { rowCount: 0, columnCount: 0, rows: rows };

                        var coveroptions = this.navigationData.landingPageDescription.split(";");
                        //put our cover options in a table and add a row for every 2 items
                        for (var x = 0; x <= coveroptions.length; x++) {
                            var cell: ICell;                                                        
                            cell = {description: coveroptions[x],cellid: x,rateid: 0,header: true,divby: 0,rate: 0,coverid: 0,indemnity: 0,turnover:0};                                                       

                            if (x % 2 == 0 && x > 0) {
                                var row: IRow; 
                                row = {selected: false, cells: cells,rowid: x}                                
                                cells = [];                                
                                table.rows.push(row);
                            }

                            cells.push(cell);
                        }

                        this.coverOptions = coveroptions;                       
                    }

                    this.coverTable = table;

                    policyUpdateDataService.initialiseNew(this.locationService.selectedLocale.strLocale, 0, this.navigationData.newPolicyId)
                        .then((policyData: IPolicyView) => {
                            //defer.resolve();
                        });
                });

        }//****************************************** End Constructor *************************************  

        Quote(): void {
            var policy = this.routeParams["POLICYDESCRIPTION"];
            this.location.path("/quotepage/"+policy); 
        }

        ContactUs(): void {
           
            var url = window.location.origin + "/contact-us";
            window.location.href = url;
        }

        Debug(): void {        
          
            console.log(this.locationService, "location service");
            console.log(this.navigationData, "navigation");
            console.log(this.coverOptions, "cover options");
            console.log(this.coverTable, "table");

        }

        submitPolicyForm(form: any, valid: boolean): void {


            // this.location.path("/quotepage/" + this.);


        }


        static routing($routeProvider) {
            $routeProvider.when("/landingpage/:POLICYDESCRIPTION",
                {
                    controller: "LandingController",
                    templateUrl: "/app/Landing/Landing.html",
                    controllerAs: "LandingController",
                    metadata: {
                        title: "Leisure Insure - Public Liability",
                        description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                        keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                    }
                });
        }

    }

    angular.module("App")
        .controller("LandingController", LandingController)
        .config(["$routeProvider", LandingController.routing]);
}



