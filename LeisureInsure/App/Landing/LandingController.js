var LeisureInsure;
(function (LeisureInsure) {
    var LandingController = (function () {
        function LandingController($http, $q, $filter, $window, $location, $routeParams, $scope, $rootScope, $route, $cookies, policyUpdateDataService, locationService, urlToNavigationService, metadataService, $timeout, commonDataService) {
            var _this = this;
            var initialisationPromises = [];
            var navigationData;
            this.heroImageSrc = "";
            this.landingPageDescription = "";
            this.policyUpdateDataService = policyUpdateDataService;
            this.locationService = locationService;
            this.filter = $filter;
            this.window = $window;
            this.location = $location;
            this.timeout = $timeout;
            this.scope = $scope;
            this.rootScope = $rootScope;
            this.http = $http;
            $("#allGifs").css("display", "block");
            this.scope = $scope;
            this.scope.alerts = new Array;
            this.routeParams = $routeParams;
            console.log("PLconstructor called");
            locationService.onSelectedLocaleChanged($scope, function (locale) {
                $cookies.put("locale", locale.strLocale);
                $route.reload();
            });
            if (this.locationService.selectedLocale != null) {
                this.locationService.selectedLocale.showFlag = false;
            }
            urlToNavigationService.translateUrlNew("POLICYDESCRIPTION")
                .then(function (data) {
                _this.navigationData = data;
                if (_this.navigationData.newPolicyId > 0) {
                    metadataService.setMetadata(_this.navigationData.title, _this.navigationData.description, _this.navigationData.keywords);
                    var table;
                    var rows;
                    rows = [];
                    var cells;
                    cells = [];
                    table = { rowCount: 0, columnCount: 0, rows: rows };
                    var coveroptions = _this.navigationData.landingPageDescription.split(";");
                    for (var x = 0; x <= coveroptions.length; x++) {
                        var cell;
                        cell = { description: coveroptions[x], cellid: x, rateid: 0, header: true, divby: 0, rate: 0, coverid: 0, indemnity: 0, turnover: 0 };
                        if (x % 2 == 0 && x > 0) {
                            var row;
                            row = { selected: false, cells: cells, rowid: x };
                            cells = [];
                            table.rows.push(row);
                        }
                        cells.push(cell);
                    }
                    _this.coverOptions = coveroptions;
                }
                _this.coverTable = table;
                policyUpdateDataService.initialiseNew(_this.locationService.selectedLocale.strLocale, 0, _this.navigationData.newPolicyId)
                    .then(function (policyData) {
                });
            });
        }
        LandingController.prototype.Quote = function () {
            var policy = this.routeParams["POLICYDESCRIPTION"];
            this.location.path("/quotepage/" + policy);
        };
        LandingController.prototype.ContactUs = function () {
            var url = window.location.origin + "/contact-us";
            window.location.href = url;
        };
        LandingController.prototype.Debug = function () {
            console.log(this.locationService, "location service");
            console.log(this.navigationData, "navigation");
            console.log(this.coverOptions, "cover options");
            console.log(this.coverTable, "table");
        };
        LandingController.prototype.submitPolicyForm = function (form, valid) {
        };
        LandingController.routing = function ($routeProvider) {
            $routeProvider.when("/landingpage/:POLICYDESCRIPTION", {
                controller: "LandingController",
                templateUrl: "/app/Landing/Landing.html",
                controllerAs: "LandingController",
                metadata: {
                    title: "Leisure Insure - Public Liability",
                    description: "Insurance for Bouncy Castles, Inflatable Play Equipment, Quad Bikes, Off-road Karts and much more. Get quotes and buy securely on-line from Leisureinsure",
                    keywords: "event insurance, events, liability, public liability, exhibition, bouncy castle, inflatable play equipment, quad bikes, instructors, insurance, equipment hirers"
                }
            });
        };
        LandingController.$inject = ["$http", "$q", "$filter", "$window", "$location", "$routeParams", "$scope", "$rootScope", "$route", "$cookies",
            "PolicyUpdateDataService", "LocationService", "UrlToNavigationService", "MetadataService", "$timeout", "CommonDataService"];
        return LandingController;
    }());
    LeisureInsure.LandingController = LandingController;
    angular.module("App")
        .controller("LandingController", LandingController)
        .config(["$routeProvider", LandingController.routing]);
})(LeisureInsure || (LeisureInsure = {}));
//# sourceMappingURL=LandingController.js.map