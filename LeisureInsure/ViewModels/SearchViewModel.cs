namespace LeisureInsure.ViewModels
{
    public class SearchViewModel
    {
        public string Url { get; set; }
        public string Keywords { get; set; }
        public string Title { get; set; }
    }
}