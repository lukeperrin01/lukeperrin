﻿namespace LeisureInsure.ViewModels
{
    public class ReferralCallbackVm
    {
        public string QuoteReference { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ClientName { get; set; }
    }
}