﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeisureInsure.ViewModels
{
    public class QuoteNotevm
    {
        public string Passcode { get; set; }
        public string Note { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }

    }
}