﻿namespace LeisureInsure.ViewModels
{
    public class EmailQuoteVm
    {
        public string Email { get; set; }
        public string QuoteReference { get; set; }
        public string Passcode { get; set; }
    }
}