﻿using System.Collections.Generic;

namespace LeisureInsure.ViewModels
{
    public class SitemapVm
    {
        public List<SitePage> Pages { get; set; }
        public string BaseUrl { get; set; }
        public string LastChanged { get; set; }
    }

    public class SitePage
    {
        public string Location { get; set; }
        public string LastModified { get; set; }
        public string ChangeFrequency { get; set; }
    }
}