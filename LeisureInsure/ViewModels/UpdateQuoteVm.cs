﻿namespace LeisureInsure.ViewModels
{
    public class UpdateQuoteVm
    {
        public string Password { get; set; }
        public bool LegalCare { get; set; }
        public bool ExcessWaiver { get; set; }
        public int CardId { get; set; }
        public int LocaleId { get; set; }
        public int? payOption { get; set; }   
        public string dateFrom { get; set; }

        public string dateTo { get; set; }
    }
}