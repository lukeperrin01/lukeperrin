﻿namespace LeisureInsure.ViewModels
{
    public class QuoteSearchVm
    {
        public string QuoteReference { get; set; }
        public string LastName { get; set; }
        public string TamReference { get; set; }
        public string Postcode { get; set; }
        public string BrokerCode { get; set; }
        public string Email { get; set; }
    }
}