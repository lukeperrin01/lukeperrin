﻿namespace LeisureInsure.ViewModels
{
    public class ResetPasswordVm
    {
        public string UserId { get; set; }
        public string Code { get; set; }
        public string NewPassword { get; set; }
    }
}