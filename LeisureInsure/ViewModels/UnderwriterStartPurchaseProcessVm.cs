﻿namespace LeisureInsure.ViewModels
{
    public class UnderwriterStartPurchaseProcessVm
    {
        public string AgentCode { get; set; }
        public string QuoteReference { get; set; }
    }
}