﻿namespace LeisureInsure.ViewModels
{
    public class CallMeBackVm
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
    }
}