﻿namespace LeisureInsure.ViewModels
{
    public class AdminVm
    {
        public string Passcode { get; set; }
        public int DocumentId { get; set; }
        public int EndorsementId { get; set; }
        public string EndorsementText { get; set; }
    }
}