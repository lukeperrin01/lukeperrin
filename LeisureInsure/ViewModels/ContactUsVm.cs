﻿namespace LeisureInsure.ViewModels
{
    public class ContactUsVm
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public string HowDidYouFindUs { get; set; }
    }
}
