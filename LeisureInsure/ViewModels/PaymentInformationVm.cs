﻿namespace LeisureInsure.ViewModels
{
    public class PaymentInformationVm
    {
        public string Passcode { get; set; }
        public string SortCode { get; set; }
        public string Account { get; set; }
        public string AccountType { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public int CollectionDate { get; set; }
        public string BusinessName { get; set; }
        public bool TermsAndConditions { get; set; }
        public int NumberOfPayments { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public bool dateToRequired { get; set; }
        public bool startTimeRequired { get; set; }
        public bool endTimeRequired { get; set; }
        public int numberOfDays { get; set; }
    }
    
}