﻿namespace LeisureInsure.ViewModels
{
    public class QuoteQuery
    {
        public string QuoteReference { get; set; }
        public string Passcode { get; set; }
    }
}