﻿using LeisureInsure.DB.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.ViewModels.QuoteEngine
{
    public class DefaultEngineView
    {
        public List<ClientEngineView> Clients { get; set; }
        public List<SectorView> Sectors { get; set; }
        public List<PolicyStatusView> StatusList { get; set; }
        public List<PayModeView> PayModes { get; set; }
        public List<ProducerView> Producers { get; set; }
        public List<ClauseView> Clauses { get; set; }
        public List<IndemnityView> Indemnities { get; set; }
        public List<CoverView> MdCovers { get; set; }
    }
}
