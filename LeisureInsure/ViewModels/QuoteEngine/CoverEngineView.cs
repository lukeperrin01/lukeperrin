﻿using System;
using System.Collections.Generic;

namespace LeisureInsure.ViewModels.QuoteEngine
{
    public class CoverEngineView
    {
        public string Desc1 { get; set; }
        public string Desc2 { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime IssuedDate { get; set; }
        public DateTime PurchasedDate { get; set; }
        public string PurchasedTime { get; set; }
        public string NightClub { get; set; }        
        public string BIHA { get; set; }
        public string Showman { get; set; }
        public string Eventname { get; set; }
        public int NumVisitors { get; set; }
        public int NumInstructors { get; set; }
        public int NumEvents { get; set; }
        public decimal CancelSumInsured { get; set; }
        public string PolicyWording { get; set; }
        public string Cancellation { get; set; }
        public List<string> Instructors { get; set; }

    }
}
