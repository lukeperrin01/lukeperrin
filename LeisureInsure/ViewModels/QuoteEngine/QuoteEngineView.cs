﻿using System;
using System.Collections.Generic;

namespace LeisureInsure.ViewModels.QuoteEngine
{
    public class QuoteEngineView
    {       
        public PolicyEngineView PolicyDetail { get; set; }
        public ClientEngineView ClientDetail { get; set; }
        public int LocaleId { get; set; }

    }
}
