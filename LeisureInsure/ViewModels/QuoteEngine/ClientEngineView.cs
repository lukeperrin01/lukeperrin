﻿using System;
using System.Collections.Generic;

namespace LeisureInsure.ViewModels.QuoteEngine
{
    public class ClientEngineView
    {
        public int Id { get; set; }
        public string TamRef { get; set; }
        public string Name { get; set; }       
        public string Attention { get; set; }

        public string TradingName { get; set; }

        public string CertName { get; set; }
        public string Sector { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string PostCode { get; set; }

        public string Town { get; set; }
        public string City { get; set; }
        public string Phone1 { get; set; }        
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public string Agency { get; set; }                
        public List<PolicyEngineView> Quotes { get; set; }
        

    }

    public class SectorView
    {
        public int PolicyId { get; set; }
        public string PolicyName { get; set; }
    }

    public class PolicyStatusView
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class PayModeView
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
