﻿using System;
using System.Collections.Generic;
using System.Web;
using LeisureInsure.DB.ViewModels;

namespace LeisureInsure.ViewModels.QuoteEngine
{
    public class PolicyEngineView
    {
        public int Id { get; set; }
        public string QuoteRef { get; set; }
        public string Pword { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Csr { get; set; }
        public string Note { get; set; }
        public string Dep { get; set; }
        public string PolicyNumber { get; set; }
        public string PayMode { get; set; }
        public string Billing { get; set; }
        public decimal OurCommission { get; set; }
        public decimal BrokerCommission { get; set; }        
        public string AgentCode { get; set; }
        public string BrokerContact { get; set; }
        public decimal NetPrem { get; set; }
        public decimal LiFee { get; set; }
        public string TamOccupation { get; set; }
        public string PolicyType { get; set; }
        public ProducerView Producer  { get; set; }
        public CoverEngineView Cover { get; set; }
        public ClauseView PolicyClause { get; set; }
        public List<ClauseView> Clauses { get; set; }
        public string CustomClauseName { get; set; }
        public List<string> CustomClauses { get; set; }
        //Cover Tabs ----------------------------------
        public IndemnityView PlIndemnity { get; set; }
        public IndemnityView ProfIndemnity { get; set; }
        public IndemnityView ProdIndemnity { get; set; }
        public IndemnityView ELIndemnity { get; set; }
        public List<CoverView> MdCovers { get; set; }
        public CoverView PlCover { get; set; }
        public CoverView ElCover { get; set; }
        public CoverView MdCover { get; set; }
        public CoverView BiCover { get; set; }
        public CoverView CancelCover { get; set; }

    }

    public class ProducerView
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal? Commission { get; set; }
    }

    public class ClauseView
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }        
    }

    public class IndemnityView
    {
        public int Id { get; set; }
        public string IndemnityString { get; set; }
        public decimal? Indemnity { get; set; }
    }
}
