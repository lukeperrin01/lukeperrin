﻿namespace LeisureInsure.ViewModels
{
    public class AddBrokerVm
    {
        public string TamCode { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }

        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string BrokerName { get; set; }
        public string FsaNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Location { get; set; }
        public int ContactId { get; set; }
        public bool Locked { get; set; }
        public string EntityName { get; set; }
    }
}