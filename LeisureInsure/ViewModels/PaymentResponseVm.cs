﻿namespace LeisureInsure.ViewModels
{
    public class PaymentResponseVm
    {
        public string MerchantId { get; set; }
        public string OrderId { get; set; }
        public string Currency { get; set; }
        public string Amount { get; set; }
        public string Timestamp { get; set; }
        public string Hash { get; set; }
        public string AutoSettleFlag { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
        public string AuthCode { get; set; }
        public string PasRef { get; set; }
        public string QuoteReference { get; set; }
        public string ResponseUrl { get; set; }
        public string Account { get; set; }
        public string Url { get; set; }
        public string EmailQuoteUrl { get; set; }
        public string Passcode { get; set; }
        public string HomepageUrl { get; set; }
        public string ProductDescription { get; set; }
    }
}
