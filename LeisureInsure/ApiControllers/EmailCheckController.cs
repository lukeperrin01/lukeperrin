﻿using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Web.Http;
using Newtonsoft.Json;
using System;
using LeisureInsure.DB.Logging;
using Microsoft.Azure;


namespace LeisureInsure.ApiControllers
{


    /// <summary>
    /// Check for spelling errors in an email such luke.perrin@googlemain.co.uk ie wont send
    /// </summary>
    public class EmailController : ApiController
    {
        [HttpGet]
        [Route("api/v1/testemail")]
        public IHttpActionResult TestEmail(string email)
        {

            string token = "f406bdd3dbe1ef0eeffc6787632b89b5";//CloudConfigurationManager.GetSetting("craftyClicksToken");

            var addressList = new List<ClsAddress>();
            bool showErrorDetail = false;

            try
            {
                string url = $"http://apilayer.net/api/check?access_key={token}&email={email}&smtp=1&format=1";

                
                //Complete XML HTTP Request
                WebRequest request = WebRequest.Create(url);
                //Complete XML HTTP Response
                WebResponse response = request.GetResponse();

                //Declare and set a stream reader to read the returned XML
                StreamReader reader = new StreamReader(response.GetResponseStream());

                // Get the requests json object and convert it to in memory dynamic
                // Note: that you are able to convert to a specific object if required.
                var jsonResponse = JsonConvert.DeserializeObject<dynamic>(reader.ReadToEnd());

                var objectResponse = new TestEmailResponse();

                objectResponse.email = jsonResponse.email;
                objectResponse.did_you_mean = jsonResponse.did_you_mean;
                objectResponse.user = jsonResponse.user;
                objectResponse.domain = jsonResponse.domain;
                objectResponse.format_valid = jsonResponse.format_valid;
                objectResponse.mx_found = jsonResponse.mx_found;
                objectResponse.smtp_check = jsonResponse.smtp_check;
                if(jsonResponse.catch_all != null)
                    objectResponse.catch_all = jsonResponse.catch_all;
                objectResponse.role = jsonResponse.role;
                objectResponse.disposable = jsonResponse.disposable;
                objectResponse.free = jsonResponse.free;
                objectResponse.score = jsonResponse.score;                

                return Ok(jsonResponse);               
               
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                string errorCode = "";
                errorCode = AppInsightLog.LogError(ex, $"api/v1/testemail{email}");
                if (!showErrorDetail)
                    message = errorCode;                   

                return BadRequest(message);
            }

            
        }
    }

    public class TestEmailResponse
    {
        public string email { get; set; }
        public string did_you_mean { get; set; }
        public string user { get; set; }
        public string domain { get; set; }
        public bool format_valid { get; set; }
        public bool mx_found { get; set; }
        public bool smtp_check { get; set; }
        public bool catch_all { get; set; }
        public bool role { get; set; }
        public bool disposable { get; set; }
        public bool free { get; set; }
        public decimal score { get; set; }

    }
   
}
