﻿using LeisureInsure.DB;
using LeisureInsure.ViewModels.QuoteEngine;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.DB.Logging;
using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Collections.Generic;
using LeisureInsure.Charges.AdditionalCharges;
using Microsoft.Azure;
using System.Text;
using LeisureInsure.Controllers;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf;
using LeisureInsure.DB.Utilities;
using ItOps.Endpoint.Email;
using ItOps.Commands.Email;

namespace LeisureInsure.ApiControllers
{
    public class QuoteEngineController : ApiController
    {
        private const string ukLocale = "en-GB";
        private const string eirLocale = "ga-IE";

        public QuoteEngineController()
        {

        }

        [HttpPost]
        [Route("api/quoteEngine/emailQuote")]
        public IHttpActionResult EmailQuote(string quoteRef, string password, string testAddress)
        {
            try
            {
                var website = CloudConfigurationManager.GetSetting("website.address");

                var emailTo = "";

                var quote = new Quotes();

                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == quoteRef && x.PWord == password);

                    if (!quote.IsAgent)
                        emailTo = repo.Contacts.FirstOrDefault(x => x.ContactPK == quote.CustomerFK).Email;
                    else
                        emailTo = repo.Contacts.FirstOrDefault(x => x.ContactPK == quote.BrokerFK).Email;

                }

                if (!string.IsNullOrEmpty(testAddress))
                    emailTo = testAddress;

                var quoteOps = new QuoteOps();
                var emailQuote = new EmailQuote
                {
                    QuoteReference = quoteRef,
                    Email = emailTo,
                    Passcode = password,
                    BaseLink = $"{website}"
                };

                if (!quote.IsAgent)
                    quoteOps.EmailQuoteClient(emailQuote);
                else
                    quoteOps.EmailQuoteBroker(emailQuote);

                return Ok("");
            }
            catch (Exception ex)
            {
                var errorCode = AppInsightLog.LogError(ex, $"api/quoteEngine/emailQuote{quoteRef}");
                return BadRequest(errorCode);
            }

        }

        [HttpDelete]
        [Route("api/quoteEngine/deleteFile")]
        public IHttpActionResult DeletePdf(int quoteid, string filename)
        {
            try
            {
                using (var repo = new LeisureInsureEntities())
                {

                    var clause = repo.QuoteClause.FirstOrDefault(x => x.CustomClauseName == filename && x.QuoteId == quoteid);

                    repo.QuoteClause.Remove(clause);

                    repo.SaveChanges();
                }

                return Ok("");
            }
            catch (Exception ex)
            {
                var errorCode = AppInsightLog.LogError(ex, $"api/quoteEngine/deleteFile{quoteid}");
                return BadRequest(errorCode);
            }


        }

        [HttpPost]
        [Route("api/quoteEngine/saveFile")]
        public IHttpActionResult SavePdf(int quoteid, string filename)
        {
            try
            {
                var result = Request.Content.ReadAsStreamAsync();

                if (result.IsCompleted)
                {
                    Stream content = result.Result;
                    var br = new BinaryReader(content);
                    var rawdata = br.ReadBytes((Int32)content.Length);
                    string text = Encoding.UTF8.GetString(rawdata);
                    rawdata = Convert.FromBase64String(text);

                    //var temppath = $"{Directory.GetCurrentDirectory()}\\{"LukeEndorse1.pdf"}";

                    var reader = new PdfReader(new MemoryStream(rawdata));
                    PdfDocument sourcePdf = new PdfDocument(reader);
                    var strategy = new TextMeasurementListener();
                    var parser = new PdfCanvasProcessor(strategy);
                    PdfPage currentPage = sourcePdf.GetPage(1);
                    parser.ProcessPageContent(currentPage);
                    var space = strategy.GetReservedSpaceInPoints();
                    var spaceAsDec = (decimal)space;

                    using (var repo = new LeisureInsureEntities())
                    {
                        var clause = new QuoteClause
                        {
                            CustomClauseName = filename,
                            CustomClause = rawdata,
                            QuoteId = quoteid,
                            ByteLength = rawdata.Length,
                            FreeSpace = spaceAsDec
                        };

                        repo.QuoteClause.Add(clause);

                        repo.SaveChanges();
                    }
                }

                return Ok("");
            }
            catch (Exception ex)
            {
                var errorCode = AppInsightLog.LogError(ex, $"api/quoteEngine/saveFile{quoteid}");
                return BadRequest(errorCode);
            }


        }

        [Route("api/quoteEngine/getClients")]
        [HttpGet]
        public IHttpActionResult GetClients(int localeId)
        {

            var clientViews = new List<ClientEngineView>();
            var defaultview = new DefaultEngineView();

            try
            {
                using (var repo = new LeisureInsureEntities())
                {

                    var locale = repo.Locales.FirstOrDefault(x => x.LocalePK == localeId);

                    var clients = repo.Contacts.Where(x => x.SystemType == "M").ToList();
                    var sectors = repo.Sectors.ToList();
                    var statuslist = repo.PolicyStatus.ToList();
                    var paymodes = repo.PayMode.ToList();
                    var producers = repo.Codes.Where(x => x.CodeType == "PROD").ToList();
                    var docs = repo.Documents.ToList();

                    //using annual bespoke PL cover to get these indeminty rates 
                    //but it doesnt matter if user chooses event, it will be the same
                    var indemnities = repo.RatesNew.Where(x => x.CoverId == 59 && x.RateTypeId == 5).ToList();

                    //we are showing the same MD cover options that we see on paintball
                    var mdCovers = repo.CoversNew.Where(x => x.CoverTypeId == 3 && x.ProductId == 2);


                    var sectorviews = new List<SectorView>();
                    var statusviews = new List<PolicyStatusView>();
                    var paymodeviews = new List<PayModeView>();
                    var prodviews = new List<ProducerView>();
                    var clauseViews = new List<ClauseView>();
                    var indemnityViews = new List<IndemnityView>();
                    var mdCoverViews = new List<CoverView>();


                    foreach (var cover in mdCovers)
                    {
                        var coverview = ViewMapper.MapCoverView(cover);
                        mdCoverViews.Add(coverview);
                    }

                    foreach (var doc in docs)
                    {
                        if (doc.DocType == "Clause")
                            clauseViews.Add(new ClauseView { Name = $"{doc.DocName}", Code = doc.ClauseCode, Id = doc.DocumentPK });
                    }

                    foreach (var sec in sectors)
                    {
                        sectorviews.Add(new SectorView { PolicyId = sec.Id, PolicyName = sec.SectorName });
                    }

                    foreach (var status in statuslist)
                    {
                        statusviews.Add(new PolicyStatusView { Code = status.Code, Description = status.Description });
                    }

                    foreach (var paymode in paymodes)
                    {
                        paymodeviews.Add(new PayModeView { Code = paymode.Code, Description = paymode.Description });
                    }

                    foreach (var prod in producers)
                    {
                        prodviews.Add(new ProducerView { Code = prod.CodeName, Commission = prod.Commission, Description = prod.CodeDescription });
                    }

                    foreach (var indem in indemnities)
                    {
                        var localeIndem = (decimal)indem.Indemnity * locale.ExchangeRate;
                        var indemString = $"{locale.CurrencySymbol}{string.Format("{0:n0}", localeIndem)}";

                        indemnityViews.Add(new IndemnityView { Id = indem.Id, Indemnity = (int)localeIndem, IndemnityString = indemString });
                    }

                    defaultview.Sectors = sectorviews;
                    defaultview.StatusList = statusviews;
                    defaultview.PayModes = paymodeviews;
                    defaultview.Producers = prodviews;
                    defaultview.Clauses = clauseViews;
                    defaultview.Indemnities = indemnityViews;
                    defaultview.MdCovers = mdCoverViews;


                    foreach (var client in clients)
                    {
                        //map client view
                        var clientView = new ClientEngineView();
                        clientView.Quotes = new List<PolicyEngineView>();

                        //get address for client
                        var addy = repo.Addresses.FirstOrDefault(x => x.ContactFK == client.ContactPK);

                        clientView.Name = client.EntityName;
                        clientView.Id = client.ContactPK;
                        clientView.TamRef = client.TamClientRef;
                        clientView.Attention = client.ForAttentionOf;
                        clientView.Phone1 = client.Telephone;
                        clientView.Email = client.Email;
                        clientView.TradingName = client.TradingName;
                        clientView.Sector = client.Sector;
                        clientView.Phone2 = client.Telephone2;
                        clientView.CertName = client.CertificateName;
                        clientView.Agency = client.Agency;
                        clientView.Street1 = addy.Address1;
                        clientView.Street2 = addy.Address2;
                        clientView.Town = addy.Town;
                        clientView.City = addy.County;
                        clientView.PostCode = addy.Postcode;


                        //needs to be added to contact?
                        //TamRef, Csr, Attention, Sector, Note

                        //add add quotes for each client
                        var quotes = repo.Quotes.Where(x => x.CustomerFK == client.ContactPK).ToList();


                        foreach (var quote in quotes)
                        {
                            var quoteView = new PolicyEngineView();
                            quoteView.QuoteRef = quote.QuoteRef;
                            quoteView.Id = quote.QuotePK;
                            quoteView.Note = quote.Notes;
                            quoteView.Csr = quote.Csr;
                            quoteView.BrokerCommission = quote.BrokerCommission;
                            quoteView.NetPrem = quote.NetPremium;
                            quoteView.LiFee = quote.Fee;
                            quoteView.TamOccupation = quote.TamOccupation;
                            quoteView.Pword = quote.PWord;

                            if (quote.CodeName != null)
                            {
                                var prod = repo.Codes.FirstOrDefault(x => x.CodeName == quote.CodeName);
                                var prodview = new ProducerView { Description = prod.CodeDescription, Code = prod.CodeName, Commission = prod.Commission };
                                quoteView.Producer = prodview;
                            }

                            if (quote.PolicyType == "E")
                                quoteView.PolicyType = "Events";
                            else
                                quoteView.PolicyType = "Annual";

                            var quoteEngine = quote.QuoteEngine.FirstOrDefault();

                            var coverView = new CoverEngineView();

                            if (quoteEngine != null)
                            {
                                quoteView.Type = quoteEngine.PolicyType;
                                quoteView.Status = quoteEngine.PolicyStatus;
                                quoteView.Dep = quoteEngine.Department;
                                quoteView.PayMode = quoteEngine.PayMode;
                                quoteView.Billing = quoteEngine.Billing;
                                quoteView.BrokerContact = quoteEngine.BrokerContact;

                                if (quote.PolicyType == "E")
                                {
                                    coverView.Eventname = quoteEngine.EventName;
                                    coverView.NumVisitors = quoteEngine.NoVisitors ?? 0;
                                    coverView.NumEvents = quoteEngine.NoEvents ?? 0;
                                }
                                else
                                {
                                    if (quoteEngine.Instructors != null)
                                        coverView.Instructors = quoteEngine.Instructors.Split(';').ToList();
                                    coverView.NightClub = quoteEngine.Nightclub;
                                    coverView.Showman = quoteEngine.Showman;
                                }

                                coverView.CancelSumInsured = quoteEngine.CancellationSumInsured ?? 0;
                                coverView.PolicyWording = quoteEngine.PolicyWording;
                                coverView.Cancellation = quoteEngine.CancelReason;
                                coverView.Desc1 = quoteEngine.BusinessEventDescription1;
                                coverView.Desc2 = quoteEngine.BusinessEventDescription2;
                            }

                            /* ----------------------- Clauses ------------------------- */
                            var clauses = quote.QuoteClause.ToList();

                            //get all custom clauses
                            var customClauses = clauses.Where(x => x.CustomClauseName != null);
                            var customClauseViews = customClauses.Select(customClause => customClause.CustomClauseName).ToList();
                            quoteView.CustomClauses = customClauseViews;

                            //get all website clauses
                            var webClauseViews = new List<ClauseView>();
                            var websiteClauses = clauses.Where(x => x.ClauseCode != null).ToList();

                            foreach (var webclause in websiteClauses)
                            {
                                var clause = repo.Documents.FirstOrDefault(x => x.ClauseCode == webclause.ClauseCode);
                                var webclauseview = new ClauseView { Id = clause.DocumentPK, Code = clause.ClauseCode, Name = clause.DocName };
                                webClauseViews.Add(webclauseview);
                            }

                            quoteView.Clauses = webClauseViews;



                            var datePurchased = (DateTime)quote.DatePurchased;

                            coverView.BIHA = quote.BIHA;
                            //Dates and Times
                            coverView.StartDate = (DateTime)quote.DateFrom;
                            coverView.EndDate = (DateTime)quote.DateTo;
                            coverView.PurchasedDate = datePurchased;
                            coverView.PurchasedTime = datePurchased.ToString("HH:mm");
                            coverView.IssuedDate = (DateTime)quote.DateIssued;
                            coverView.StartTime = quote.TimeFrom;
                            coverView.EndTime = quote.TimeTo;

                            coverView.NumInstructors = quote.NoInstructors ?? 0;
                            coverView.PurchasedDate = (DateTime)quote.DatePurchased;

                            quoteView.Cover = coverView;

                            //broker?
                            if (quote.IsAgent)
                            {
                                quoteView.AgentCode = quote.BrokerName;
                            }

                            //load public liability tab ------------------------------------------------
                            var plcover = repo.CoversNew.FirstOrDefault(x => x.CoverTypeId == 1 && x.ProductId == quote.PolicyFK);
                            var plCoverline = repo.QuoteLineNew.FirstOrDefault(x => x.QuoteId == quote.QuotePK
                            && x.CoverId == plcover.Id);
                            if (plCoverline != null)
                            {
                                var plCoverView = new CoverView();
                                plCoverView.Total = plCoverline.Total;
                                plCoverView.Excess = plCoverline.Excess;
                                plCoverView.Id = plCoverline.Id;
                                plCoverView.Turnover = plCoverline.Turnover;

                                var plitemviews = new List<ListItemView>();

                                foreach (var item in plCoverline.QuoteListItem)
                                {
                                    var quoteItem = new ListItemView();
                                    quoteItem.ItemDescription = item.ItemDescription;
                                    quoteItem.Turnover = item.Turnover ?? 0;
                                    quoteItem.Rate = item.Rate ?? 0;
                                    quoteItem.Premium = item.Premium ?? 0;
                                    quoteItem.Excess = item.Excess ?? 0;
                                    quoteItem.SumInsured = item.SumInsured ?? 0;
                                    plitemviews.Add(quoteItem);
                                }
                                plCoverView.ItemsAdded = plitemviews;

                                quoteView.PlCover = plCoverView;
                                quoteView.PlIndemnity = indemnityViews.FirstOrDefault(x => x.Indemnity == plCoverline.Indemnity);

                            }

                            var prodcover = repo.CoversNew.FirstOrDefault(x => x.CoverTypeId == 4 && x.ProductId == quote.PolicyFK);
                            var prodCoverline = repo.QuoteLineNew.FirstOrDefault(x => x.QuoteId == quote.QuotePK && x.CoverId == prodcover.Id);
                            if (prodCoverline != null)
                                quoteView.ProdIndemnity = indemnityViews.FirstOrDefault(x => x.Indemnity == prodCoverline.Indemnity);


                            var profcover = repo.CoversNew.FirstOrDefault(x => x.CoverTypeId == 24 && x.ProductId == quote.PolicyFK);
                            var profCoverline = repo.QuoteLineNew.FirstOrDefault(x => x.QuoteId == quote.QuotePK && x.CoverId == profcover.Id);
                            if (profCoverline != null)
                                quoteView.ProfIndemnity = indemnityViews.FirstOrDefault(x => x.Indemnity == profCoverline.Indemnity);


                            //load employers liability tab ------------------------------------------------
                            var elcover = repo.CoversNew.FirstOrDefault(x => x.CoverTypeId == 2 && x.ProductId == quote.PolicyFK);
                            var elCoverline = repo.QuoteLineNew.FirstOrDefault(x => x.QuoteId == quote.QuotePK
                            && x.CoverId == elcover.Id);

                            if (elCoverline != null)
                            {
                                var elCoverView = new CoverView();
                                elCoverView.Total = elCoverline.Total;
                                elCoverView.Id = elCoverline.Id;
                                elCoverView.Ern = elCoverline.Ern;
                                elCoverView.EmployeeTotal = elCoverline.NoEmployees ?? 0;

                                var elitemviews = new List<ListItemView>();

                                foreach (var item in elCoverline.QuoteListItem)
                                {
                                    var quoteItem = new ListItemView();
                                    quoteItem.Activity = item.Activity;
                                    quoteItem.WageRoll = item.WageRoll ?? 0;
                                    quoteItem.Rate = item.Rate ?? 0;
                                    quoteItem.Premium = item.Premium ?? 0;
                                    elitemviews.Add(quoteItem);
                                }

                                elCoverView.ItemsAdded = elitemviews;

                                quoteView.ElCover = elCoverView;
                                quoteView.ELIndemnity = indemnityViews.FirstOrDefault(x => x.Indemnity == elCoverline.Indemnity);

                            }


                            //load material damage tab  ------------------------------------------------
                            var mdcover = repo.CoversNew.FirstOrDefault(x => x.CoverTypeId == 3 && x.ProductId == quote.PolicyFK);
                            var mdCoverline = repo.QuoteLineNew.FirstOrDefault(x => x.QuoteId == quote.QuotePK && x.CoverId == mdcover.Id);

                            //main md coverline to hold our tab information
                            if (mdCoverline != null)
                            {
                                var mdCoverView = new CoverView();
                                mdCoverView.Total = mdCoverline.Total;
                                mdCoverView.Id = mdCoverline.Id;
                                mdCoverView.MDAddress = mdCoverline.MDAddress;
                                mdCoverView.MDPostCode = mdCoverline.MDPostCode;
                                mdCoverView.SumInsured = mdCoverline.SumInsured;
                                mdCoverView.Excess = mdCoverline.Excess;
                                mdCoverView.Subsidence = mdCoverline.Subsidence ?? 0;

                                var mditemviews = new List<ListItemView>();

                                foreach (var item in mdCoverline.QuoteListItem)
                                {
                                    var quoteItem = new ListItemView();
                                    quoteItem.ItemDescription = item.ItemDescription;
                                    quoteItem.Rate = item.Rate ?? 0;
                                    quoteItem.Premium = item.Premium ?? 0;
                                    quoteItem.SumInsured = item.SumInsured ?? 0;
                                    quoteItem.Excess = item.Excess ?? 0;
                                    mditemviews.Add(quoteItem);
                                }

                                mdCoverView.ItemsAdded = mditemviews;
                                quoteView.MdCover = mdCoverView;

                                //get all saved coverlines eg Stock, Buildings
                                var mdCoverLines = repo.QuoteLineNew.Where(x => x.QuoteId == quote.QuotePK
                                    && x.CoversNew.CoverTypeId == 3 && x.CoversNew.ProductId == 2).ToList();

                                var mdlineViews = new List<CoverView>();

                                foreach (var line in mdCoverLines)
                                {
                                    var lineview = new CoverView();
                                    lineview = ViewMapper.MapQuoteLineToView(lineview, line);
                                    var rateitem = line.QuoteInputNew.Where(x => x.RateTypeId == 10).FirstOrDefault();
                                    //note the website may have multiple rates for a cover
                                    if (rateitem != null)
                                    {
                                        lineview.CoverHeader = rateitem.RateName;
                                        lineview.SumInsured = rateitem.RateValue != "" ? Convert.ToDecimal(rateitem.RateValue) : 0;
                                    }

                                    mdlineViews.Add(lineview);
                                }

                                quoteView.MdCovers = mdlineViews;

                            }

                            //load business interruption tab (loading covers)  ------------------------------------------------
                            var bicover = repo.CoversNew.FirstOrDefault(x => x.CoverTypeId == 22 && x.ProductId == quote.PolicyFK);

                            var biCoverlines = repo.QuoteLineNew.Where(x => x.QuoteId == quote.QuotePK && x.CoverId == bicover.Id).ToList();

                            if (biCoverlines.Any())
                            {
                                var biCoverView = new CoverView();
                                biCoverView.Total = quoteEngine.BITotalNet;
                                biCoverView.SumInsured = quoteEngine.BITotalSumInsured;
                                biCoverView.Excess = quoteEngine.BITotalExcess;

                                var biItemviews = new List<ListItemView>();

                                foreach (var coverline in biCoverlines)
                                {
                                    var item = new ListItemView();

                                    var indemperiod = repo.QuoteInputNew.FirstOrDefault(x => x.QuoteLineId == coverline.Id && x.RateTypeId == 40);
                                    var sumInsured = repo.QuoteInputNew.FirstOrDefault(x => x.QuoteLineId == coverline.Id && x.RateTypeId == 10);

                                    //custom rates
                                    item.Rate = sumInsured.Rate ?? 0;
                                    item.SumInsured = sumInsured.RateValueNum ?? 0;
                                    item.IndemnityPeriod = indemperiod.RateValue;
                                    //cover level
                                    item.Premium = coverline.Total;
                                    item.Excess = coverline.Excess ?? 0;
                                    item.ItemDescription = coverline.CustomDescription;

                                    biItemviews.Add(item);

                                }

                                biCoverView.ItemsAdded = biItemviews;

                                quoteView.BiCover = biCoverView;

                            }

                            clientView.Quotes.Add(quoteView);
                        }

                        clientViews.Add(clientView);
                    }

                    defaultview.Clients = clientViews;
                }

                return Ok(defaultview);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/quoteEngine/getClients");
                return BadRequest(ex.Message);
            }
        }

        [Route("api/quoteEngine/createPolicy")]
        [HttpPost]
        public IHttpActionResult PostPolicy(QuoteEngineView quoteData)
        {

            var quoteRef = "";
            var client = quoteData.ClientDetail;
            var policy = quoteData.PolicyDetail;
            var isBroker = false;
            var cover = quoteData.PolicyDetail.Cover;
            var policyType = "";
            var broker = new Contacts();
            var csr = new Contacts();
            var product = new Products();

            var csrInitial = "";
            var user = User.Identity;
            if (user.IsAuthenticated)
            {
                //are we a broker?
                using (var repo = new LeisureInsureEntities())
                {
                    //tamclientRef will be login, ie TESTAGO, LP00000
                    broker = repo.Contacts.FirstOrDefault(x => x.TamClientRef == user.Name && x.ContactType == 2);
                    csr = repo.Contacts.FirstOrDefault(x => x.TamClientRef == user.Name && x.ContactType == 4);
                }
            }

            bool isCsr = csr != null ? true : false;

            if (isCsr)
                csrInitial = csr.TamClientRef.Substring(0, 2);



            var startTime = DateTime.Parse(cover.StartTime).ToString("HH:mm");
            var endTime = DateTime.Parse(cover.EndTime).ToString("HH:mm");

            var purchasedHours = DateTime.Parse(cover.PurchasedTime).Hour;
            var purchasedMins = DateTime.Parse(cover.PurchasedTime).Minute;
            cover.PurchasedDate = cover.PurchasedDate.AddHours(purchasedHours);
            cover.PurchasedDate = cover.PurchasedDate.AddMinutes(purchasedMins);

            var plCoverTab = quoteData.PolicyDetail.PlCover;
            var elCoverTab = quoteData.PolicyDetail.ElCover;
            var biCoverTab = quoteData.PolicyDetail.BiCover;
            var mdCoverTab = quoteData.PolicyDetail.MdCover;

            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
            var instructors = "";

            decimal taxRate;
            using (var repo = new Repository())
            {
                var taxCalc = new TaxCalculator(repo);
                taxRate = taxCalc.Rate(quoteData.LocaleId, dateNow);
            }

            var legalRate = Convert.ToDecimal(CloudConfigurationManager.GetSetting("legalcare.rate"));
            var legalCareRate = legalRate / 100;
            var netLegalCare = policy.NetPrem * legalCareRate;
            var legalCareTax = netLegalCare * taxRate;
            var legalCare = netLegalCare + legalCareTax;
            var tax = policy.NetPrem * taxRate;

            // are we always adding legal care?
            var total = policy.NetPrem + tax + policy.LiFee + legalCare;

            var customerid = 0;

            var policyData = new PolicyView();

            try
            {
                using (var repo = new LeisureInsureEntities())
                {

                    var contact = new Contacts();

                    var updatingContact = client.Id != 0 ? true : false;
                    var updatingQuote = policy.Id != 0 ? true : false;

                    if (updatingContact)
                        contact = repo.Contacts.FirstOrDefault(x => x.ContactPK == client.Id);

                    if (policy.PolicyType == "Annual")
                    {
                        policyType = "A";
                        product = repo.Products.FirstOrDefault(x => x.Id == 8);


                        if (quoteData.PolicyDetail.Cover.Instructors != null)
                        {
                            for (int i = 0; i < quoteData.PolicyDetail.Cover.Instructors.Count; i++)
                            {
                                if (i + 1 != quoteData.PolicyDetail.Cover.Instructors.Count)
                                    instructors += quoteData.PolicyDetail.Cover.Instructors[i] + ";";
                                else
                                    instructors += quoteData.PolicyDetail.Cover.Instructors[i];
                            }
                        }
                    }
                    else
                    {
                        policyType = "E";
                        product = repo.Products.FirstOrDefault(x => x.Id == 7);
                    }

                    if (!string.IsNullOrEmpty(policy.AgentCode))
                    {
                        broker = repo.Contacts.Where(x => x.TamClientRef == policy.AgentCode).FirstOrDefault();
                        isBroker = true;
                    }

                    //Contact -----------------------
                    //contact.EntityType = policyData.Addresses[0].EntityType;
                    contact.EntityName = client.Name ?? "";
                    contact.TamClientRef = client.TamRef;
                    contact.ForAttentionOf = client.Attention;
                    //contact.FirstName = client.Name;
                    //contact.LastName = client.Name;
                    contact.Telephone = client.Phone1;
                    contact.Email = client.Email;
                    contact.TradingName = client.TradingName;
                    contact.Sector = client.Sector;
                    //manual quote engine/not public
                    contact.SystemType = "M";
                    contact.CertificateName = client.CertName;
                    contact.Telephone2 = client.Phone2;
                    contact.Agency = client.Agency;

                    if (isBroker)
                    {
                        contact.BrokerCommission = policy.BrokerCommission;
                    }


                    contact.Locked = false;
                    if (!updatingContact)
                        repo.Contacts.Add(contact);

                    repo.SaveChanges();

                    customerid = contact.ContactPK;


                    //Address ---------------------
                    var addy = new Addresses();
                    if (updatingContact)
                        addy = repo.Addresses.FirstOrDefault(x => x.ContactFK == contact.ContactPK);

                    addy.ContactFK = customerid;
                    addy.Address1 = client.Street1;
                    addy.Address2 = client.Street2 ?? "";
                    addy.Town = client.Town ?? "";
                    addy.County = client.City ?? "";
                    addy.Timestamp = dateNow;
                    //addy.Country = policyData.Locale.country;
                    addy.Postcode = client.PostCode;
                    //addy.Locale = policyData.Locale.strLocale;                                       

                    if (!updatingContact)
                        repo.Addresses.Add(addy);

                    repo.SaveChanges();

                    var quote = new Quotes();

                    if (updatingQuote)
                        quote = repo.Quotes.FirstOrDefault(x => x.QuotePK == policy.Id);


                    //manual quote engine
                    quote.SystemType = "M";
                    //quote.QuoteRef  *************************** generate quote ref

                    if (!updatingQuote)
                    {
                        quote.QuoteRef = "";
                        quote.PWord = Path.GetRandomFileName().Replace(".", "");
                    }

                    quote.TamOccupation = quoteData.PolicyDetail.TamOccupation;
                    quote.ContactName = client.Name;
                    quote.ContactEmail = client.Email;
                    quote.CustomerFK = customerid;
                    quote.AddressFK = addy.AddressPK;
                    quote.Postcode = client.PostCode;
                    quote.BIHA = cover.BIHA;
                    quote.Purchased = 0;
                    //Date/Times
                    quote.DateFrom = cover.StartDate;
                    quote.DateTo = cover.EndDate;
                    quote.DatePurchased = cover.PurchasedDate;//combine with purchased time
                    quote.DateIssued = cover.IssuedDate;
                    quote.TimeFrom = startTime;
                    quote.TimeTo = endTime;
                    quote.DateAdded = dateNow;

                    //price related
                    quote.NetPremium = policy.NetPrem;
                    quote.NetLegal = netLegalCare;
                    quote.Fee = policy.LiFee;
                    quote.TaxRate = taxRate;
                    quote.TotalLegal = legalCare;
                    quote.Total = total;
                    quote.Tax = tax;

                    if (isBroker)
                    {
                        quote.BrokerCommission = policy.BrokerCommission;
                        quote.BrokerFK = broker.ContactPK;
                        quote.BrokerName = policy.AgentCode;
                    }
                    //quote.CardCharge
                    //quote.PayOption
                    quote.Csr = policy.Csr;
                    quote.IsAgent = isBroker;
                    //quote.Excess
                    //quote.LocaleId?
                    //quote.PaymentCardId?                   
                    quote.Notes = policy.Note;
                    quote.PolicyType = policyType;
                    quote.TermsAndConditions = true;
                    quote.NoInstructors = cover.NumInstructors;

                    quote.Version = 1;
                    quote.TamOccupation = policy.TamOccupation;
                    quote.PolicyFK = product.Id;
                    quote.LocaleId = quoteData.LocaleId;
                    quote.CodeName = policy.Producer?.Code;

                    if (!updatingQuote)
                        repo.Quotes.Add(quote);

                    repo.SaveChanges();

                    if (!updatingQuote)
                    {
                        //this will always be a CSR but just in case
                        if (isCsr)
                        {
                            quote.QuoteRef = csrInitial + quote.QuotePK.ToString("D8");
                            quote.Csr = csrInitial;
                        }
                        else
                            quote.QuoteRef = "WU" + quote.QuotePK.ToString("D8");

                        addy.QuoteFK = quote.QuotePK;
                    }

                    //also need event information

                    var quoteEngine = new QuoteEngine();

                    if (updatingQuote)
                        quoteEngine = quote.QuoteEngine.FirstOrDefault();

                    quoteEngine.CancellationSumInsured = Convert.ToDecimal(cover.CancelSumInsured);
                    quoteEngine.CancelReason = cover.Cancellation;
                    quoteEngine.PolicyWording = cover.PolicyWording;
                    quoteEngine.QuoteId = quote.QuotePK;
                    quoteEngine.PolicyType = policy.Type;
                    quoteEngine.PolicyStatus = policy.Status;
                    quoteEngine.Department = policy.Dep;
                    quoteEngine.PayMode = policy.PayMode;
                    quoteEngine.Billing = policy.Billing;

                    if (isBroker)
                        quoteEngine.BrokerContact = policy.BrokerContact;

                    quoteEngine.BusinessEventDescription1 = cover.Desc1;
                    quoteEngine.BusinessEventDescription2 = cover.Desc2;

                    if (policyType == "E")
                    {

                        quoteEngine.EventName = cover.Eventname;
                        quoteEngine.NoVisitors = cover.NumVisitors;
                        quoteEngine.NoEvents = cover.NumEvents;
                        quoteEngine.Instructors = instructors;
                    }
                    else
                    {
                        quoteEngine.NoInstructors = cover.NumInstructors;
                        quoteEngine.Showman = cover.Showman;
                        quoteEngine.Nightclub = cover.NightClub;
                    }

                    if (!updatingQuote)
                        repo.QuoteEngine.Add(quoteEngine);

                    //remove existing website clauses
                    var webclauses = repo.QuoteClause.Where(x => x.QuoteId == quote.QuotePK && x.ClauseCode != null)
                        .ToList();
                    if (webclauses.Any())
                        repo.QuoteClause.RemoveRange(webclauses);

                    if (policy.Clauses.Any())
                    {
                        foreach (var clause in policy.Clauses)
                        {
                            if (clause == null)
                                continue;

                            var quoteclause = new QuoteClause
                            {
                                ClauseCode = clause.Code,
                                QuoteId = quote.QuotePK
                            };

                            repo.QuoteClause.Add(quoteclause);
                        }
                    }
                    // ---------------- Save Public Liability Tab  ------------------------
                    if (policy.PlIndemnity != null)
                    {
                        var plcover = repo.CoversNew.FirstOrDefault(x => x.ProductId == product.Id && x.CoverTypeId == 1);
                        var plrate = repo.RatesNew.FirstOrDefault(x => x.RateTypeId == 49);

                        var plline = repo.QuoteLineNew.FirstOrDefault(x => x.CoverId == plcover.Id);
                        if (plline != null)
                        {
                            var listitems = repo.QuoteListItem.Where(x => x.QuoteLineId == plline.Id);

                            repo.QuoteListItem.RemoveRange(listitems);
                            repo.QuoteLineNew.Remove(plline);

                        }

                        plline = new QuoteLineNew
                        {
                            SectionDescription = "Public Liability",
                            CustomDescription = "",
                            Total = plCoverTab.Total ?? 0,
                            Excess = plCoverTab.Excess,
                            Indemnity = policy.PlIndemnity.Indemnity,
                            QuoteId = quote.QuotePK,
                            CoverId = plcover.Id,
                            Turnover = plCoverTab.Turnover,
                            SumInsured = plCoverTab.SumInsured,
                            Required = true
                        };

                        repo.QuoteLineNew.Add(plline);

                        repo.SaveChanges();

                        foreach (var item in plCoverTab.ItemsAdded)
                        {

                            var newitem = new QuoteListItem();
                            newitem.ItemDescription = item.ItemDescription;
                            newitem.Rate = item.Rate;
                            newitem.Premium = item.Premium;
                            newitem.Turnover = item.Turnover;
                            newitem.Excess = item.Excess;
                            newitem.QuoteLineId = plline.Id;
                            newitem.SumInsured = item.SumInsured;
                            //we are creating a new rate for these items so point them to 
                            //bespoke rate                            
                            newitem.RateId = plrate.Id;

                            repo.QuoteListItem.Add(newitem);
                        }
                    }

                    // ---------------- Save Product Liability 
                    var prodcover = repo.CoversNew.FirstOrDefault(x => x.ProductId == product.Id && x.CoverTypeId == 4);

                    var prodline = repo.QuoteLineNew.FirstOrDefault(x => x.CoverId == prodcover.Id);

                    if (prodline != null)
                        repo.QuoteLineNew.Remove(prodline);

                    prodline = new QuoteLineNew
                    {
                        SectionDescription = "Product Liability",
                        CustomDescription = "",
                        Total = plCoverTab.Total ?? 0,
                        Excess = plCoverTab.Excess,
                        Indemnity = policy.ProdIndemnity?.Indemnity ?? 0,
                        QuoteId = quote.QuotePK,
                        CoverId = prodcover.Id,
                        Required = policy.ProdIndemnity != null ? true : false
                    };

                    repo.QuoteLineNew.Add(prodline);


                    // ---------------- Save Professional Liability  ------------------------
                    var profcover = repo.CoversNew.FirstOrDefault(x => x.ProductId == product.Id && x.CoverTypeId == 24);

                    var profline = repo.QuoteLineNew.FirstOrDefault(x => x.CoverId == profcover.Id);
                    if (profline != null)
                        repo.QuoteLineNew.Remove(profline);

                    profline = new QuoteLineNew
                    {
                        SectionDescription = "Professional Liability",
                        CustomDescription = "",
                        Total = plCoverTab.Total ?? 0,
                        Excess = plCoverTab.Excess,
                        Indemnity = policy.ProfIndemnity?.Indemnity ?? 0,
                        QuoteId = quote.QuotePK,
                        CoverId = profcover.Id,
                        Required = policy.ProfIndemnity != null ? true : false
                    };

                    repo.QuoteLineNew.Add(profline);


                    // ---------------- Save Employer Liability Tab ------------------------
                    if (elCoverTab != null)
                    {
                        var elcover = repo.CoversNew.FirstOrDefault(x => x.ProductId == product.Id && x.CoverTypeId == 2);
                        var bespokeRate = repo.RatesNew.FirstOrDefault(x => x.RateTypeId == 49);

                        var elline = repo.QuoteLineNew.FirstOrDefault(x => x.CoverId == elcover.Id);
                        if (elline != null)
                        {
                            var listitems = repo.QuoteListItem.Where(x => x.QuoteLineId == elline.Id);
                            repo.QuoteListItem.RemoveRange(listitems);
                            repo.QuoteLineNew.Remove(elline);

                            repo.SaveChanges();
                        }

                        elline = new QuoteLineNew
                        {
                            SectionDescription = "Employers Liability",
                            CustomDescription = "",
                            Total = elCoverTab.Total ?? 0,
                            Indemnity = policy.ELIndemnity.Indemnity,
                            Ern = elCoverTab.Ern,
                            NoEmployees = elCoverTab.EmployeeTotal,
                            QuoteId = quote.QuotePK,
                            CoverId = elcover.Id,
                            Required = true
                        };

                        repo.QuoteLineNew.Add(elline);

                        repo.SaveChanges();

                        foreach (var item in elCoverTab.ItemsAdded)
                        {

                            var newitem = new QuoteListItem();
                            newitem.Activity = item.Activity;
                            newitem.Rate = item.Rate;
                            newitem.Premium = item.Premium;
                            newitem.WageRoll = item.WageRoll;
                            newitem.QuoteLineId = elline.Id;
                            newitem.RateId = bespokeRate.Id;

                            repo.QuoteListItem.Add(newitem);
                        }
                    }

                    // ---------------- Save Material Damage Tab ------------------------
                    if (mdCoverTab != null)
                    {
                        var mdcover = repo.CoversNew.FirstOrDefault(x => x.ProductId == product.Id && x.CoverTypeId == 3);
                        var bespokeRate = repo.RatesNew.FirstOrDefault(x => x.RateTypeId == 49);

                        var mdlines = repo.QuoteLineNew.Where(x => x.QuoteId == quote.QuotePK && x.CoversNew.CoverTypeId == 3).ToList();

                        foreach (var coverLine in mdlines)
                        {
                            var listitems = coverLine.QuoteListItem.ToList();
                            if (listitems.Any())
                                repo.QuoteListItem.RemoveRange(listitems);

                            var rateitems = coverLine.QuoteInputNew.ToList();
                            if (rateitems.Any())
                                repo.QuoteInputNew.RemoveRange(rateitems);

                            repo.QuoteLineNew.Remove(coverLine);
                        }

                        repo.SaveChanges();
                        //this is the main MD Cover for the tab in general and for 
                        //the rate items being assigned
                        var mdline = new QuoteLineNew
                        {
                            SectionDescription = "Material Damage",
                            CustomDescription = "",
                            Total = mdCoverTab.Total ?? 0,
                            MDAddress = mdCoverTab.MDAddress,
                            MDPostCode = mdCoverTab.MDPostCode,
                            SumInsured = mdCoverTab.SumInsured,
                            Excess = mdCoverTab.Excess,
                            QuoteId = quote.QuotePK,
                            CoverId = mdcover.Id,
                            Subsidence = mdCoverTab.Subsidence,
                            Required = true
                        };

                        repo.QuoteLineNew.Add(mdline);

                        repo.SaveChanges();

                        foreach (var item in mdCoverTab.ItemsAdded)
                        {

                            var newitem = new QuoteListItem();
                            newitem.ItemDescription = item.ItemDescription;
                            newitem.SumInsured = item.SumInsured;
                            newitem.Rate = item.Rate;
                            newitem.Premium = item.Premium;
                            newitem.Excess = item.Excess;
                            newitem.QuoteLineId = mdline.Id;
                            newitem.RateId = bespokeRate.Id;

                            repo.QuoteListItem.Add(newitem);
                        }

                        foreach (var mdCover in policy.MdCovers)
                        {
                            //save additional MD Covers - eg Stock, Buildings
                            var newline = new QuoteLineNew();
                            newline = ViewMapper.MapQuoteLine(mdCover, ref newline, quote.QuotePK, 1);
                            newline.Required = true;
                            newline.CustomDescription = mdCover.CoverHeader;
                            repo.QuoteLineNew.Add(newline);
                            if (newline.CoverId == 0)
                                throw new Exception("Material Damage Cover must be selected from the pre defined list");

                            repo.SaveChanges();

                            //create rate item
                            var item = new QuoteInputNew();
                            item.QuoteId = quote.QuotePK;
                            item.RateValue = mdCover.SumInsured.ToString();
                            item.RateName = mdCover.CoverHeader;
                            item.QuoteLineId = newline.Id;
                            item.RateId = bespokeRate.Id;
                            item.RateTypeId = 10;

                            repo.QuoteInputNew.Add(item);

                        }
                    }

                    // ---------------- Save Business Interruption Tab ------------------------
                    if (biCoverTab != null)
                    {
                        var biCover = repo.CoversNew.FirstOrDefault(x => x.ProductId == product.Id && x.CoverTypeId == 22);
                        var bespokeRate = repo.RatesNew.FirstOrDefault(x => x.RateTypeId == 49);

                        var bilines = repo.QuoteLineNew.Where(x => x.CoverId == biCover.Id && x.QuoteId == quote.QuotePK).ToList();

                        //remove previous quoteline/quoteinput
                        foreach (var biline in bilines)
                        {
                            if (biline != null)
                            {
                                var quoteitems = biline.QuoteInputNew.ToList();
                                if (quoteitems.Any())
                                    repo.QuoteInputNew.RemoveRange(quoteitems);

                                repo.QuoteLineNew.Remove(biline);
                                repo.SaveChanges();
                            }
                        }

                        //save general details to quoteengine
                        quoteEngine.BITotalNet = biCoverTab.Total ?? 0;
                        quoteEngine.BITotalSumInsured = biCoverTab.SumInsured ?? 0;
                        quoteEngine.BITotalExcess = biCoverTab.Excess;

                        //adding covers, each with quoteitem
                        foreach (var item in biCoverTab.ItemsAdded)
                        {

                            var biLine = new QuoteLineNew
                            {
                                CustomDescription = item.ItemDescription,
                                Total = item.Premium,
                                QuoteId = quote.QuotePK,
                                CoverId = biCover.Id,
                                Excess = item.Excess,
                                Required = true
                            };

                            repo.QuoteLineNew.Add(biLine);
                            repo.SaveChanges();

                            //save custom rates, note we are not adding actual rates, only quote specific 
                            var itemIndemPeriod = new QuoteInputNew();
                            itemIndemPeriod.QuoteId = quote.QuotePK;
                            itemIndemPeriod.RateValue = item.IndemnityPeriod;
                            itemIndemPeriod.QuoteLineId = biLine.Id;
                            itemIndemPeriod.RateId = bespokeRate.Id;
                            itemIndemPeriod.RateTypeId = 40;

                            var itemPremium = new QuoteInputNew();
                            itemPremium.RateValueNum = item.SumInsured;
                            itemPremium.Rate = item.Rate;
                            itemPremium.QuoteId = quote.QuotePK;
                            itemPremium.QuoteLineId = biLine.Id;
                            itemPremium.RateId = bespokeRate.Id;
                            itemPremium.RateTypeId = 10;

                            repo.QuoteInputNew.Add(itemIndemPeriod);
                            repo.QuoteInputNew.Add(itemPremium);
                        }
                    }


                    repo.SaveChanges();

                    policyData.QuoteReference = quote.QuoteRef;
                    policyData.Password = quote.PWord;

                    quoteRef = quote.QuoteRef;

                }

                return Ok(policyData);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/quoteEngine/createPolicy");
                return BadRequest(ex.Message);
            }
        }
    }
}

