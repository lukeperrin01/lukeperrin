﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.LandingPages;
using LeisureInsure.ViewModels;
using LeisureInsure.DB;

namespace LeisureInsure.ApiControllers
{
    public class SearchController : ApiController
    {
        readonly IRepository _repository;

        public SearchController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("api/v1/search")]
        public Task<List<SearchViewModel>> ProductLines()
        {

            IList<LandingPages> lp = _repository.getLandingPages();
            var searches = new List<SearchViewModel>();

                foreach (LandingPages l in lp)
            {
                searches.Add(new SearchViewModel()
                {
                    Keywords = l.Keywords + ", " + l.Title,
                    Title = l.Title,
                    Url = l.Url
                });

            }

            //var searches = _repository.Q<LandingPage>().Where(x=>x.Enabled == true).Select(x => new SearchViewModel
            //{
            //    Url = x.Url,
            //    Keywords = x.Keywords + ", " + x.Title,
            //    Title = x.Title
            //}).ToList();

            return Task.FromResult(searches);
        }
    }
}
