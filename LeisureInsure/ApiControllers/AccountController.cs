﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using LeisureInsure.DB.Models.Identity;
using LeisureInsure.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Azure;

namespace LeisureInsure.ApiControllers
{
    public class AccountController : ApiController
    {
        private readonly ApplicationUserManager _userManager;

        public AccountController(
            ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpPost]
        [Route("api/v1/account/create")]
        public async Task<IdentityResult> Create(UserViewModel model)
        {
            LiApplicationUser user;
            if (string.IsNullOrEmpty(model.Email))
            {
                user = new LiApplicationUser
                {
                    UserName = model.Username,
                };
            }
            else
            {
                user = new LiApplicationUser
                {
                    UserName = model.Username,
                    Email = model.Email,
                };
            }
            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) {ReasonPhrase = result.Errors.FirstOrDefault()});
            }
            return result;

            //is this in live?! hope not!
        }

        [HttpGet]
        [Route("api/v1/account/forgottenpassword")]
        public async Task ForgottenPassword(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) {ReasonPhrase = "Please enter a username"});
            }
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(user.Email))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "There is no email associated with this account, please call LeisureInsure to have your password changed"});
            }
            
            var code = await _userManager.GeneratePasswordResetTokenAsync(user.Id);
            var callbackUrl = $"{CloudConfigurationManager.GetSetting(Constants.Configuration.WebsiteBaseAddress)}/account/reset-password/{user.Id}?code={HttpUtility.UrlEncode(code)}";
            
            await _userManager.SendEmailAsync(user.Id, "Reset Password",
            "Please reset your password by clicking here: <a href=\"" + callbackUrl + "\">link</a>");
        }

        [HttpPost]
        [Route("api/v1/account/resetpassword")]
        public async Task ResetPassword(ResetPasswordVm model)
        {
            var result = await _userManager.ResetPasswordAsync(model.UserId, model.Code, model.NewPassword);
            if (!result.Succeeded)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = string.Join(", ", result.Errors)});
            }
        }
    }
}