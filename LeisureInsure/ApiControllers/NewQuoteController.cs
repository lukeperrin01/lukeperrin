﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.Charges;
using LeisureInsure.Charges.AdditionalCharges;
using LeisureInsure.Charges.Completion;
using LeisureInsure.Services.Services.Payment;
using LeisureInsure.Services.Services.Utilities;
using LeisureInsure.Services.Services.Validation;
using LeisureInsure.DB;
using NServiceBus;
using LeisureInsure.Charges.Discounts;
using System;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB.Utilities;
using LeisureInsure.Helpers;
using Microsoft.Azure;
using LeisureInsure.Utilities;

//test patch
//make sure this doesnt get pulled in live..
//2nd test
//2nd patch test for live
namespace LeisureInsure.ApiControllers
{
    public class NewQuoteController : ApiController
    {
        private readonly IMapper _mapper;
        private readonly IAddPaymentOptions _paymentOptions;
        private readonly ICalculateAllCoverCharges _allCovers;
        private readonly IAddGlobalIrisInformation _globalIris;
        private readonly ICreateQuoteDetails _quoteDetails;
        private readonly IClock _clock;
        private readonly ICalculateTax _tax;
        private readonly IValidateQuotes _quote;        
        private readonly IEndpointInstance _endpoint;
        private readonly IProvideDiscountEngines _discount;
        private readonly ICalculateQuoteTotals _totals;


        public NewQuoteController(
            IRepository rep,
            IMapper mapper,
            IAddPaymentOptions paymentOptions,
            ICalculateAllCoverCharges allCovers,
            IAddGlobalIrisInformation globalIris,
            ICreateQuoteDetails quoteDetails,
            IClock clock,
            ICalculateTax tax, IValidateQuotes quote,
            IEndpointInstance endpoint,

        IProvideDiscountEngines discount, ICalculateQuoteTotals totals)
        {            
            _mapper = mapper;
            _paymentOptions = paymentOptions;
            _allCovers = allCovers;
            _globalIris = globalIris;
            _quoteDetails = quoteDetails;
            _clock = clock;
            _tax = tax;
            _quote = quote;
            _endpoint = endpoint;
            _discount = discount;
            _totals = totals;
        }

        [HttpPost]
        [Route("api/v1/updateQuoteDatesNew")]
        public IHttpActionResult UpdateQuoteDates([FromBody] PolicyView policyData)
        {
            try
            {
                var quote = new Quotes();

                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == policyData.QuoteReference && x.PWord == policyData.Password);


                    var dates = QuoteDateHelper.GetQuoteDates(policyData.Dates, (int)quote.PolicyFK, quote.PolicyType);

                    quote.DateFrom = dates.startDate;
                    quote.DateTo = dates.endDate;
                    quote.TimeFrom = dates.startTime;
                    quote.TimeTo = dates.endTime;

                    repo.SaveChanges();

                }

                if (quote == null)
                {
                    throw new Exception("Quote not found");
                }

                return Ok(0);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"post:api/v1/updateQuoteDatesNew/{policyData.QuoteReference}");
                return BadRequest(errorid);
            }
        }

        [HttpGet]
        [Route("api/v1/loadquote")]
        public IHttpActionResult LoadQuote(string quoteRef, string password)
        {
            try
            {
                var dbContext = new LeisureInsureEntities();

                var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

                var quote = dbContext.Quotes.FirstOrDefault(x => x.QuoteRef == quoteRef && x.PWord == password);
                var policy = dbContext.Products.FirstOrDefault(x => x.Id == quote.PolicyFK);
                var quotelines = dbContext.QuoteLineNew.Where(x => x.QuoteId == quote.QuotePK).ToList();
                var coverviews = new List<CoverView>();
                var addressviews = new List<AddressView>();
                var allItemRates = dbContext.ItemRates.ToList();
                var legalRate = CloudConfigurationManager.GetSetting("legalcare.rate");
                var publishedKey = CloudConfigurationManager.GetSetting("stripePublishedKey");

                //get min start date, and max start date
                var minStartDate = new DateTime();
                var maxStartDate = new DateTime();
                int minDateDays = (int)policy.MinDateDays;
                int maxDateDays = (int)policy.MaxDateMonths;
                minStartDate = dateNow.AddDays(minDateDays);
                maxStartDate = dateNow.AddDays(maxDateDays);

                var minStart = JavaScriptDateConverter.Convert(minStartDate);
                var maxStart = JavaScriptDateConverter.Convert(maxStartDate);

                var landingdata = dbContext.LandingPages.FirstOrDefault(x => x.NewPolicyId == quote.PolicyFK);

                //for agents the contact doesn't have the quotepk
                var agentContact = new Contacts();
                var customerContact = new Contacts();

                if (quote.IsAgent)
                    agentContact = dbContext.Contacts.FirstOrDefault(x => x.TamClientRef == quote.BrokerName);

                customerContact = dbContext.Contacts.FirstOrDefault(x => x.ContactPK == quote.CustomerFK);

                var addresses = dbContext.Addresses.Where(x => x.QuoteFK == quote.QuotePK).ToList();

                var localetest = dbContext.Locales.FirstOrDefault(x => x.LocalePK == quote.LocaleId);
                var locales = new vmLocales();

                using (var repo = new PoliciesRepository())
                {
                    locales = repo.GetLocales(localetest.strLocale, policy.Id);
                }

                addressviews = ViewMapper.MapAddressViews(addresses, customerContact);

                var mdHeaderAdded = false;
                var biHeaderAdded = false;
                var policyView = new PolicyView();


                foreach (var quoteline in quotelines)
                {
                    var coverview = new CoverView();
                    var cover = quoteline.CoversNew;
                    var quoteInputs = quoteline.QuoteInputNew.ToList();
                    var rateItemviews = ViewMapper.MapItemRateViews(cover.ItemTypes.ToList());
                    var ratesForCover = cover.RatesNew.Where(x => (x.LocaleBW & quote.LocaleId) > 0).ToList();
                    var quotelistItems = quoteline.QuoteListItem.ToList();
                    var quoteListItemViews = ViewMapper.MapListItemViews(quotelistItems);

                    var rateviews = new List<RateView>();

                    foreach (var rate in ratesForCover)
                    {

                        //indemnity (table)
                        if (rate.RateTypeId == 5)
                        {
                            var localeIndemnity = (decimal)rate.Indemnity * locales.localeSelected.exchangeRate;
                            rate.Indemnity = (int)localeIndemnity;
                            //apply multiplier on minimum charges for PL table
                            if (rate.MinimumRate > 0)
                                rate.MinimumRate = rate.MinimumRate * locales.localeSelected.multiplier;
                        }

                        //turnover (column table)                 
                        if (rate.RateTypeId == 48)
                        {
                            if (rate.DivBy == 1)
                                rate.Rate = rate.Rate * locales.localeSelected.multiplier;
                        }

                        //premiums - eg items like equipment
                        if (rate.RateTypeId == 10)
                        {
                            //apply exchange rate
                            if (rate.DivBy == 1)
                            {
                                rate.Rate = rate.Rate * locales.localeSelected.multiplier;
                                rate.FirstRate = rate.FirstRate * locales.localeSelected.multiplier;
                                rate.DiscountFirstRate = rate.DiscountFirstRate * locales.localeSelected.multiplier;
                                rate.DiscountSubsequentRate = rate.DiscountSubsequentRate * locales.localeSelected.multiplier;
                            }
                            //apply multiplier on minimum charges for keyed value (turnover) 
                            if (rate.MinimumRate > 0)
                                rate.MinimumRate = rate.MinimumRate * locales.localeSelected.multiplier;
                        }



                        var rateview = ViewMapper.MapRateView(rate, quote.LocaleId);

                        //do we have a select ?
                        //get list of all default options for a select
                        if (rateview.InputTypeId == 7)
                        {
                            //get rates based on our item type
                            var rateIdsForSelect = allItemRates.Where(x => x.ItemTypeId == rateview.ItemTypeId).ToList();
                            var optionIds = rateIdsForSelect.Select(x => x.RateId).ToList();
                            var options = ratesForCover.Where(x => optionIds.Contains(x.Id)).ToList();
                            //SumInsured Dropdowns - we are not applying exchange rate to this
                            foreach (var option in options)
                            {
                                if (option.RateTypeId == 6 && option.InputTypeId == 20)
                                {
                                    decimal rateDec = option.RateAsNum ?? 0;
                                    option.RateName = $"{locales.localeSelected.currencySymbol}{string.Format("{0:n}", rateDec)}";
                                }
                            }

                            var optionViews = ViewMapper.MapRateViews(options);
                            rateview.Options = optionViews;
                        }

                        //loading any saved input text, select lists, currency fields, radio yes/no, item spec
                        //still get everything else, other rates etc for our form, text area, checkbox, number
                        if (rate.InputTypeId == 2 || rate.InputTypeId == 7 || rate.InputTypeId == 18 || rate.InputTypeId == 19
                            || rate.InputTypeId == 23 || rate.InputTypeId == 24 || rate.InputTypeId == 11 || rate.InputTypeId == 13
                            || rate.InputTypeId == 3)
                        {
                            //map additional saved details
                            var savedInput = quoteInputs.FirstOrDefault(x => x.RateId == rate.Id);
                            if (savedInput != null)
                            {
                                //map option to our select 
                                if (savedInput.InputSelectedId != 0)
                                {

                                    //get our main selection
                                    var option = ratesForCover.FirstOrDefault(x => x.Id == savedInput.InputSelectedId);
                                    var optionview = ViewMapper.MapRateView(option, quote.LocaleId);
                                    rateview.InputSelected = optionview;

                                    //does this option show anything?                                        
                                    if (optionview.VisibleParent != null)
                                    {
                                        var displayInputs = ratesForCover.Where(x => x.VisibleChild == optionview.VisibleParent).ToList();
                                        foreach (var input in displayInputs)
                                        {
                                            input.Display = 1;
                                        }
                                    }
                                    //if this was based on a parent category get the options from that                                    
                                    if (rateview.SelectChild != null)
                                    {
                                        //get options for select parent
                                        var parentCategories = rateviews.Where(x => x.SelectParent == rateview.SelectChild).ToList();
                                        var inputSelects = quoteInputs.Select(x => x.InputSelectedId).ToList();
                                        //which parent option was selected?
                                        var parentOption = parentCategories.Where(x => inputSelects.Contains(x.Id)).FirstOrDefault();
                                        var rateIdsForSelect = allItemRates.Where(x => x.ItemTypeId == parentOption.ItemTypeId).ToList();
                                        //get all options based on the parents option
                                        var optionIds = rateIdsForSelect.Select(x => x.RateId).ToList();
                                        var options = ratesForCover.Where(x => optionIds.Contains(x.Id)).ToList();
                                        var optionViews = ViewMapper.MapRateViews(options);
                                        rateview.Options = optionViews;
                                    }
                                }
                                //update rate with our saved input
                                rateview.RateValue = savedInput.RateValue;
                                rateview.RateValueNum = savedInput.RateValueNum ?? 0;
                                rateview.Refer = savedInput.Refer ?? 0;
                            }
                        }

                        rateviews.Add(rateview);
                    }

                    coverview.RateItems = rateItemviews;
                    //make sure rates are ordered
                    var ordereredRates = rateviews.OrderBy(x => x.Ordinal).ToList();
                    coverview.Rates = ordereredRates;
                    //add our selected items - equipment etc
                    coverview.ItemsAdded = quoteListItemViews;
                    coverview.PolicyId = policy.Id;
                   
                    coverview = ViewMapper.MapQuoteLineToView(coverview, quoteline);

                    //specify which covers are in a section and which are not
                    if ((cover.CoverTypeId == 3 || cover.CoverTypeId == 22) && cover.CoverType == "Optional")
                    {
                        coverview.ShowCover = false;
                        coverview.SectionType = true;
                    }
                    else
                    {
                        coverview.ShowCover = true;
                        coverview.SectionType = false;
                    }

                    //one optional cover (will have yes/no) in this section needs to be responsible for showing the main section question                    
                    if ((cover.CoverTypeId == 3) && cover.CoverType == "Optional")
                    {
                        if (!mdHeaderAdded)
                            coverview.sectionHeader = true;
                        mdHeaderAdded = true;
                        //make sure section question is updated yes/no
                        if (quoteline.Required)
                        {
                            policyView.MdSectionAdded = true;
                            coverview.ShowCover = true;
                        }

                        if (policyView.MdSectionAdded == true)
                            coverview.ShowCover = true;

                    }

                    if ((cover.CoverTypeId == 22) && cover.CoverType == "Optional")
                    {
                        if (!biHeaderAdded)
                            coverview.sectionHeader = true;
                        biHeaderAdded = true;
                        //make sure section question is updated yes/no
                        if (quoteline.Required)
                        {
                            policyView.BiSectionAdded = true;
                            coverview.ShowCover = true;
                        }

                        if (policyView.BiSectionAdded == true)
                            coverview.ShowCover = true;
                    }

                    coverviews.Add(coverview);

                }//end loop through quotelines                


                policyView.Id = policy.Id;
                policyView.PolicyName = policy.ProductName;
                policyView.TamType = policy.TamType;
                policyView.Ordinal = policy.Ordinal;
                policyView.PeriodYear = policy.PeriodDay;
                policyView.PeriodDay = policy.PeriodDay;
                policyView.PolicyType = policy.PolicyType;
                policyView.PolicyGroupFK = policy.PolicyGroupFK;
                policyView.PremiseRequired = policy.PremiseRequired;
                policyView.TimeRequired = policy.TimeRequired;
                policyView.TamOccupation = policy.TamOccupation;
                policyView.ClauseCode = policy.ClauseCode;
                policyView.BusinessDescription = policy.BusinessDescription;
                policyView.CertificateBody = policy.CertificateBody;
                policyView.Covers = coverviews;
                //price related
                policyView.LeisureInsureFee = quote.Fee;
                policyView.LegalRate = Convert.ToDecimal(legalRate);
                policyView.Net = quote.NetPremium;
                policyView.Total = quote.Total;
                policyView.Tax = quote.Tax;
                policyView.TaxRate = quote.TaxRate ?? 0;
                policyView.Excess = quote.Excess;
                //dates
                policyView.DateFrom = quote.DateFrom;
                policyView.DateTo = quote.DateTo;
                policyView.TimeFrom = quote.TimeFrom;
                policyView.TimeTo = quote.TimeTo;
                //other quote information
                policyView.QuoteReference = quote.QuoteRef;
                policyView.Password = quote.PWord;
                policyView.Addresses = addressviews;
                //broker?
                policyView.BrokerCommission = quote.BrokerCommission;
                policyView.BrokerName = quote.BrokerName;
                policyView.TamBrokerCode = quote.TamBrokerCode;
                policyView.CommissionPercentage = quote.CommissionPercentage;
                //start dates
                policyView.MinStartDate = minStart;
                policyView.MaxStartDate = maxStart;
                policyView.LegalFeesAdded = quote.LegalFeesAdded;
                policyView.LegalFees = quote.TotalLegal ?? 0;
                policyView.LegalNet = quote.NetLegal;
                policyView.StripePublishableKey = publishedKey;
                policyView.Locale = locales.localeSelected;
                policyView.LocaleId = locales.localeSelected.localePK;
                policyView.LandingPage = landingdata;


                //get docs for policy
                using (var repo = new PoliciesRepository())
                {
                    policyView.Documents = repo.GetDocsForPolicy(quote.QuotePK, policyView.LegalFeesAdded);
                }

                dbContext.Dispose();

                return Ok(policyView);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/v1/savequote")]
        public IHttpActionResult SaveQuote([FromBody] PolicyView policyData)
        {

            try
            {
                var dbContext = new LeisureInsureEntities();

                int quoteId = 0;
                var newquote = new Quotes();
                var existingquote = new Quotes();
                int numberOfDays = 0;
                bool showError = false;
                bool updatingQuote = false;
                int quoteversion = 0;

                //Set up date and time variables - nb these are only temporary 
                //the real ones are set at the point of purchase           
                var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

                DateTime dateEffective = dateNow;
                DateTime dateExpires = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 23, 59, 0);
                string dateEffectiveTime = dateNow.ToString("HH:mm"); ;
                string dateExpiresTime = "23:59";

                //validate code
                //var enteredcode = model.Inputs.Where(x => x.RatePk == 5303).FirstOrDefault();
                //if (!String.IsNullOrEmpty(enteredcode?.RateValue))
                //{
                //    var code = _rep.GetCode(enteredcode.RateValue);
                //    if (code == null)
                //    {
                //        showError = true;
                //        throw new Exception("Please enter a valid promo or producer code");
                //    }
                //}       

                //creating new quote        
                if (string.IsNullOrEmpty(policyData.QuoteReference))
                    quoteversion = 1;
                else
                {
                    existingquote = dbContext.Quotes.FirstOrDefault(x => x.QuoteRef == policyData.QuoteReference && x.PWord == policyData.Password);
                    var currentversion = existingquote.Version;
                    quoteversion = (int)currentversion + 1;
                    existingquote.Version = quoteversion;
                    updatingQuote = true;
                    quoteId = existingquote.QuotePK;
                }

                if (!updatingQuote)
                {
                    newquote.PWord = Path.GetRandomFileName().Replace(".", "");
                    var prefix = "";
                    //policy ref will be WU for brokers/clients, but initial for CSRs
                    if (policyData.CSR != "")
                    {
                        newquote.Csr = policyData.CSR;
                        prefix = policyData.CSR;
                    }
                    else
                        prefix = "WU";
                    //cant create a quote without having assigned customer - revise this
                    newquote.CustomerFK = 7616;
                    newquote.QuoteRef = "";
                    //commit to get id
                    dbContext.Quotes.Add(newquote);
                    dbContext.SaveChanges();
                    newquote.QuoteRef = prefix + newquote.QuotePK.ToString("D8");
                    var id = newquote.QuotePK;
                    quoteId = id;
                    newquote = dbContext.Quotes.FirstOrDefault(x => x.QuotePK == id);
                    newquote.Version = quoteversion;
                    newquote.DateFrom = dateEffective;
                    newquote.DateTo = dateExpires;
                    newquote.TimeFrom = dateEffectiveTime;
                    newquote.TimeTo = dateExpiresTime;
                    newquote.DateAdded = dateNow;
                    newquote.Buy = 0;
                    //zero for now
                    newquote.NoInstructors = 0;
                    newquote.CardCharge = 0;
                    newquote.TermsAndConditions = false;
                    newquote.LegalFeesAdded = false;
                    newquote.LocaleId = policyData.LocaleId;
                    //publicly created quote
                    newquote.SystemType = "P";

                }

                if (!updatingQuote)
                    newquote = ViewMapper.MapQuote(policyData, ref newquote);
                else
                    existingquote = ViewMapper.MapQuote(policyData, ref existingquote);

                //we are sending our updated policy object back to the calling form
                //so get any additional quote information
                if (!updatingQuote)
                {
                    policyData.DateFrom = newquote.DateFrom;
                    policyData.DateTo = newquote.DateTo;
                    policyData.TimeFrom = newquote.TimeFrom;
                    policyData.TimeTo = newquote.TimeTo;
                    //other quote information
                    policyData.QuoteReference = newquote.QuoteRef;
                    policyData.Password = newquote.PWord;
                }

                dbContext.SaveChanges();

                // Save covers (quotelinenew)                   
                foreach (var cover in policyData.Covers)
                {
                    var quoteline = new QuoteLineNew();
                    var updateQuoteline = new QuoteLineNew();

                    quoteline = ViewMapper.MapQuoteLine(cover, ref quoteline, quoteId, quoteversion);
                    dbContext.QuoteLineNew.Add(quoteline);

                    dbContext.SaveChanges();

                    if (cover.ItemsAdded != null)
                    {
                        //each cover may have items added, equipment, sports 
                        foreach (var item in cover.ItemsAdded)
                        {
                            //save items eg equipment, employers dropdown
                            var listitem = new QuoteListItem();

                            listitem = ViewMapper.MapQuoteListItem(item, ref listitem, quoteversion, quoteline.Id);
                            dbContext.QuoteListItem.Add(listitem);
                        }
                        dbContext.SaveChanges();
                    }

                    //we need to save rate views (quoteinputs)
                    foreach (var rate in cover.Rates)
                    {
                        //we are only saving input text, select lists, currency fields, radio yes/no, item spec, text area, checkbox,
                        //number fields
                        if (rate.InputTypeId == 2 || rate.InputTypeId == 7 || rate.InputTypeId == 18 ||
                            rate.InputTypeId == 19 || rate.InputTypeId == 23 || rate.InputTypeId == 24 || rate.InputTypeId == 11
                            || rate.InputTypeId == 13 || rate.InputTypeId == 3)
                        {
                            if (rate.Display == 1)
                            {

                                var quoteinput = new QuoteInputNew();
                                quoteinput.InputSelectedId = 0;
                                quoteinput.RateValue = rate.RateValue ?? "";
                                quoteinput.RateValueNum = rate.RateValueNum;

                                if (rate.InputSelected != null)
                                {
                                    quoteinput.InputSelectedId = rate.InputSelected.Id;
                                    quoteinput.RateValue = rate.InputSelected.RateName ?? "";
                                }

                                quoteinput.QuoteId = quoteId;
                                quoteinput.RateId = rate.Id;
                                quoteinput.QuoteLineId = quoteline.Id;
                                quoteinput.RateTypeId = rate.RateTypeId;
                                //quoteinput.ListNo 
                                //quoteinput.ListOrdinal                                 
                                quoteinput.RateName = rate.RateName ?? "";
                                quoteinput.Refer = rate.Refer;
                                quoteinput.TerritoryCover = "";
                                quoteinput.CertListItem = 0;
                                quoteinput.Version = quoteversion;
                                dbContext.QuoteInputNew.Add(quoteinput);
                            }

                        }
                    }

                    dbContext.SaveChanges();

                }

                //we are here so all is good, if updating delete old inputted data              
                if (updatingQuote)
                {
                    var oldVersion = --quoteversion;
                    var oldQuoteLines = dbContext.QuoteLineNew.Where(x => x.QuoteId == quoteId && x.Version == oldVersion).ToList();
                    var oldQuoteInputs = dbContext.QuoteInputNew.Where(x => x.QuoteId == quoteId && x.Version == oldVersion).ToList();
                    var oldQuoteListItems = new List<QuoteListItem>();
                    //remove list items
                    foreach (var quoteline in oldQuoteLines)
                    {
                        var oldlistitems = quoteline.QuoteListItem.ToList();
                        dbContext.QuoteListItem.RemoveRange(oldlistitems);
                    }
                    //remove inputs, covers (quote lines)
                    dbContext.QuoteInputNew.RemoveRange(oldQuoteInputs);
                    dbContext.QuoteLineNew.RemoveRange(oldQuoteLines);

                    dbContext.SaveChanges();
                }

                //get docs for policy
                using (var repo = new PoliciesRepository())
                {
                    policyData.Documents = repo.GetDocsForPolicy(quoteId, policyData.LegalFeesAdded);
                }

                return Ok(policyData);
            }
            catch (Exception ex)
            {
                var browser = policyData.Browser;
                var policyName = policyData.PolicyName;
                var errorid = AppInsightLog.LogError(ex, $"post:api/v1/savequote {policyName} browser:{browser}", policyData.QuoteReference);
                return BadRequest(errorid);
            }
        }

        [HttpPost]
        [Route("api/v1/saveaddress")]
        public IHttpActionResult SaveAddress([FromBody] PolicyView policyData)
        {
            try
            {
                using (var repo = new LeisureInsureEntities())
                {
                    //customerType 1 customer 2 agent 3 underwriter, entityType: Broker                    
                    int customerid = 0;
                    int agentid = 0;
                    bool updating = false;

                    var quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == policyData.QuoteReference && x.PWord == policyData.Password);

                    Contacts agentContact = null;
                    Contacts custContact = null;
                    var newcontact = new Contacts();

                    agentContact = repo.Contacts.FirstOrDefault(x => x.TamClientRef == quote.BrokerName);

                    custContact = repo.Contacts.FirstOrDefault(x => x.ContactPK == quote.CustomerFK);

                    if (quote.IsAgent)
                        agentid = agentContact.ContactPK;

                    //existing customer
                    if (custContact != null)
                    {
                        updating = true;
                        custContact.EntityName = policyData.Addresses[0].EntityName;
                        custContact.FirstName = policyData.Addresses[0].FirstName;
                        custContact.LastName = policyData.Addresses[0].LastName;
                        custContact.Telephone = policyData.Addresses[0].Telephone;
                        custContact.Email = policyData.Addresses[0].Email;
                        custContact.BrokerCommission = policyData.BrokerCommission;                        
                        customerid = custContact.ContactPK;

                        var addresses = repo.Addresses.Where(x => x.QuoteFK == quote.QuotePK).ToList();
                        //delete old addresses 
                        repo.Addresses.RemoveRange(addresses);
                        repo.SaveChanges();
                    }
                    else //new customer
                    {
                        // fsaNumber? Locked : BrokerActive Lock: Cant be deleted

                        //publicly created contact
                        newcontact.SystemType = "P";
                        newcontact.EntityType = policyData.Addresses[0].EntityType;
                        newcontact.EntityName = policyData.Addresses[0].EntityName;
                        newcontact.FirstName = policyData.Addresses[0].FirstName;
                        newcontact.LastName = policyData.Addresses[0].LastName;
                        newcontact.Telephone = policyData.Addresses[0].Telephone;
                        newcontact.Email = policyData.Addresses[0].Email;
                        newcontact.BrokerCommission = policyData.BrokerCommission;                        
                        newcontact.Locked = false;
                        repo.Contacts.Add(newcontact);
                        repo.SaveChanges();
                        customerid = newcontact.ContactPK;
                    }

                    //existing addresses would be deleted if updating so always
                    //add new ones, sports instructors may go back and add more addresses
                    var newAddys = new List<Addresses>();

                    foreach (var address in policyData.Addresses)
                    {
                        var addy = new Addresses();
                        addy.ContactFK = customerid;
                        addy.Address1 = policyData.Addresses[0].Address1;
                        addy.Address2 = policyData.Addresses[0].Address2;
                        addy.Address3 = policyData.Addresses[0].Address3;
                        addy.Town = policyData.Addresses[0].Town;
                        addy.County = policyData.Addresses[0].County;
                        addy.Country = policyData.Locale.country;
                        addy.Postcode = policyData.Addresses[0].Postcode;
                        addy.Locale = policyData.Locale.strLocale;
                        addy.QuoteFK = quote.QuotePK;

                        newAddys.Add(addy);
                        repo.Addresses.Add(addy);
                    }

                    //add addresses
                    repo.SaveChanges();

                    quote.CustomerFK = customerid;                    
                    quote.BrokerFK = agentid;
                    quote.AddressFK = newAddys.FirstOrDefault().AddressPK;

                    //update contact with name/email if not a broker
                    if (!quote.IsAgent)
                    {
                        quote.ContactEmail = policyData.Addresses[0].Email;
                        quote.ContactName = $"{policyData.Addresses[0].FirstName}  {policyData.Addresses[0].LastName}";
                    }

                    var contact = new Contacts();

                    if (updating)
                        contact = custContact;
                    else
                        contact = newcontact;

                    //each step of the breadcrumb we are accumulating more info for our policyData
                    var addressviews = ViewMapper.MapAddressViews(newAddys, contact);
                    policyData.Addresses = addressviews;

                    repo.SaveChanges();
                }

                return Ok(policyData);
            }
            catch (Exception ex)
            {
                var browser = policyData.Browser;
                var policyName = policyData.PolicyName;
                var errorid = AppInsightLog.LogError(ex, $"post:api/v1/saveaddress {policyName} browser:{browser}", policyData.QuoteReference);
                return BadRequest(errorid);
            }
        }


    }
}

