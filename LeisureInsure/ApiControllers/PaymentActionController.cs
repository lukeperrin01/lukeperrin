﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ItOps.Commands.Email;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Payments;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Charges;
using LeisureInsure.Charges.Discounts;
using LeisureInsure.Services.Services.Payment;
using LeisureInsure.ViewModels;
using Microsoft.Azure;
using NServiceBus;
using Quote = LeisureInsure.Services.Dal.LeisureInsure.Quotes.Quote;
using System;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.Helpers;
using ItOps.Commands;
using System.Web;

namespace LeisureInsure.ApiControllers
{
    public class PaymentActionController : ApiController
    {
        private readonly ILiRepository _repository;
        private readonly IEndpointInstance _bus;
        private readonly IAddPaymentOptions _paymentOptions;
        private readonly ICalculateQuoteTotals _totals;
        private readonly IProvideDiscountEngines _discount;
        private readonly IEndpointInstance _endpoint;

        public PaymentActionController(
            ILiRepository repository,
            IEndpointInstance bus,
            IAddPaymentOptions paymentOptions,
            ICalculateQuoteTotals totals,
            IProvideDiscountEngines discount,
            IEndpointInstance endpoint)
        {
            _repository = repository;
            _bus = bus;
            _paymentOptions = paymentOptions;
            _totals = totals;
            _discount = discount;
            _endpoint = endpoint;
        }

        [HttpGet]
        [Route("api/v1/payment-cards")]
        public IHttpActionResult PaymentCards()
        {
            try
            {
                var result = _repository.Q<PaymentCard>().ToList();
                return Ok(result);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PaymentActionController.PaymentCards] get:api/v1/payment-cards");
                return BadRequest(errorid);
            }
        }

        [HttpPost]
        [Route("api/v1/documents")]
        public IHttpActionResult Documents(QuoteQuery model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.Documents)
                            .First(x => x.QuoteReference == model.QuoteReference && x.Password == model.Passcode);
                var result = quote.Documents.ToList();
                return Ok(result);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PaymentActionController.Documents] post:api/v1/documents", model.QuoteReference);
                return BadRequest(errorid);
            }
        }

        [HttpPost]
        [Route("api/v1/payment/removecover/{coverId}")]
        public IHttpActionResult RemoveCover(int coverId, QuoteQuery model)
        {
            try
            {
                var quote = _repository.Q<Quote>()
                            .First(x => x.QuoteReference == model.QuoteReference && x.Password == model.Passcode);
                var cover = quote.ChargeSummaryItems.First(x => x.coverFK == coverId);
                var coverInputs = quote.Answers.Where(x => x.CoverPk == coverId).ToList();

                quote.ChargeSummaryItems.Remove(cover);

                _repository.Delete(cover);

                foreach (Answer a in coverInputs)
                {
                    quote.Answers.Remove(a);
                    _repository.Delete(a);
                }

                var discounts = quote.Discounts.Where(x => x.coverFK == coverId).ToList();
                foreach (var discount in discounts)
                {
                    quote.Discounts.Remove(discount);
                    _repository.Delete(discount);
                }

                var useOnlineDiscount = CloudConfigurationManager.GetSetting("use.online.discount") == "1";
                if (useOnlineDiscount)
                {
                    _discount.EngineFor(DicountType.Online).Calculate(quote, null, quote.PolicyId);
                }

                // Adjust the legal fees
                _totals.Calculate(quote);

                _repository.Commit();

                return Ok(quote);
            }
            catch (Exception ex)
            {
                string reference = "RemoveCover";
                if (coverId != 0)
                    reference += coverId.ToString();

                var errorid = AppInsightLog.LogError(ex, "[PaymentActionController.RemoveCover] post:api/v1/payment/removecover" + reference, model.QuoteReference);
                return BadRequest(errorid);
            }
        }
        [HttpPost]
        [Route("api/v1/setBuy/{quoteReference}/{lockQuote}")]
        public IHttpActionResult setBuy(string quoteReference, int lockQuote)
        {
            try
            {
                var quote = _repository.Q<Quote>().First(x => x.QuoteReference == quoteReference);

                if (lockQuote == 1)
                {
                    quote.Buy = 1;
                }
                else
                {
                    quote.Buy = 0;
                }
                _repository.Commit();
                return Ok(0);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PaymentActionController.SetBuy] post:api/v1/setBuy" + quoteReference, quoteReference);
                return BadRequest(errorid);
            }
        }

        [HttpPost]
        [Route("api/v1/unlockQuote/{quoteReference}/{lockQuote}")]
        public IHttpActionResult unlockQuote(string quoteReference, int lockQuote)
        {
            try
            {
                var quote = _repository.Q<Quote>().First(x => x.QuoteReference == quoteReference);

                if (lockQuote == 1)
                {
                    quote.LockedForCustomer = true;
                }
                else
                {
                    quote.LockedForCustomer = false;
                }
                _repository.Commit();
                return Ok(0);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PaymentActionController.UnlockQuote] post:api/v1/unlockQuote" + quoteReference, quoteReference);
                return BadRequest(errorid);
            }
        }



        [HttpPost]
        [Route("api/v1/completeAgent/{quoteReference}")]
        public IHttpActionResult CompleteAsAgent(string quoteReference, PaymentInformationVm model)
        {
            bool purchasedBefore = false;
            bool notloggedIn = false;
            bool noAddressFound = false;
            bool noCustomerFound = false;
            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

            try
            {
                if (String.IsNullOrEmpty(User.Identity.Name))
                {
                    notloggedIn = true;
                    throw new Exception("Agent not logged in");
                }

                var user = _repository.Q<Contact>().FirstOrDefault(x => x.TamClientRef == User.Identity.Name);

                if (user == null)
                {
                    throw new Exception("User not found");
                }

                if (user.Locked == true)
                {
                    throw new HttpRequestValidationException("This account is currently suspended. Please contact the office for further details: 01993 700761 or info@leisureinsure.co.uk");
                }

                var quote = _repository.Q<Quote>(x => x.Answers)
                    .FirstOrDefault(x => x.QuoteReference == quoteReference);

                if (quote == null)
                {
                    throw new Exception("Quote not found");
                }

                if (quote.LockedForCustomer || quote.Purchased > 0)
                {
                    purchasedBefore = true;
                    throw new Exception($"QuoteRef: {quote.QuoteReference} | This policy has already been purchased");
                }

                if (quote.AddressFK == 0)
                {
                    noAddressFound = true;
                    quote.Purchased = 0;
                    quote.LockedForCustomer = false;
                    quote.DatePurchased = null;
                    _repository.Commit();
                    throw new Exception($"QuoteRef: {quote.QuoteReference} | No Address associated with this quote");
                }

                if (quote.CustomerFK == 0)
                {
                    noCustomerFound = true;
                    quote.Purchased = 0;
                    quote.LockedForCustomer = false;
                    quote.DatePurchased = null;
                    _repository.Commit();
                    throw new Exception($"QuoteRef: {quote.QuoteReference} | No Customer associated with this quote");
                }


                //ensure dates times are checked for client
                var dt = new vmDateCheck()
                {
                    startDate = DateTime.Parse(model.StartDate),
                    startTime = model.TimeFrom,
                    endDate = DateTime.Parse(model.EndDate),
                    endTime = model.TimeTo,
                    PolicyPK = quote.PolicyId,
                    PolicyType = quote.PolicyType,
                    endTimeRequired = model.endTimeRequired,
                    startTimeRequired = model.startTimeRequired,
                    dateToRequired = model.dateToRequired,
                    numberOfDays = model.numberOfDays
                };

                var dateHelper = new DateTimeHelper();
                var dates = dateHelper.DateChecker(dt);
                quote.DateFrom = dates.startDate;
                quote.DateTo = dates.endDate;
                quote.TimeFrom = dates.startTime;
                quote.TimeTo = dates.endTime;


                if (string.IsNullOrEmpty(quote.ContactEmail))
                {
                    quote.ContactEmail = user.Email;
                }
                quote.LockedForCustomer = true;
                quote.Purchased = 1;
                quote.DatePurchased = dateNow;


                if (User.Identity.IsAuthenticated && !User.Identity.Name.Contains("00000"))
                {
                    quote.BrokerName = User.Identity.Name;
                    quote.TamBrokerCode = User.Identity.Name;
                    quote.IsAgent = true;
                }
                else if (User.Identity.IsAuthenticated && string.IsNullOrEmpty(quote.BrokerName))
                {
                    quote.BrokerName = User.Identity.Name;
                    quote.TamBrokerCode = User.Identity.Name;
                    quote.IsAgent = true;
                }
                else if (!User.Identity.IsAuthenticated)
                {
                    quote.TamBrokerCode = null;
                    quote.BrokerName = null;
                    quote.IsAgent = false;
                }

                _repository.Commit();

                _endpoint.Send<CreatePolicyForAgent>(m =>
                {
                    m.PolicyReference = quoteReference;
                    m.PayByInstallments = false;
                    m.AgentCode = user.TamClientRef;
                });
                //ComeBackDW
                return Ok(0);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[PaymentActionController.CompleteAsAgent] post:api/v1/quotes/{quoteReference}/agent", quoteReference);
                //display meaningfull error in some cases 
                if (purchasedBefore || notloggedIn || noAddressFound || noCustomerFound)

                    return BadRequest(ex.Message);
                else
                    return BadRequest(errorid);
            }
        }

        /// <summary>
        /// Store customer information for clients
        /// </summary>
        /// <param name="quoteReference"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/payments/{quoteReference}")]
        public IHttpActionResult AddPaymentInformation(string quoteReference, PaymentInformationVm model)
        {

            try
            {
                var quote = _repository.Q<Quote>()
                            .First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);

                var dt = new vmDateCheck()
                {
                    startDate = DateTime.Parse(model.StartDate),
                    startTime = model.TimeFrom,
                    endDate = DateTime.Parse(model.EndDate),
                    endTime = model.TimeTo,
                    PolicyPK = quote.PolicyId,
                    PolicyType = quote.PolicyType,
                    startTimeRequired = model.startTimeRequired,
                    endTimeRequired = model.endTimeRequired,
                    dateToRequired = model.dateToRequired,
                    numberOfDays = model.numberOfDays
                };

                var dateHelper = new DateTimeHelper();
                var dates = dateHelper.DateChecker(dt);
                quote.DateFrom = dates.startDate;
                quote.DateTo = dates.endDate;
                quote.TimeFrom = dates.startTime;
                quote.TimeTo = dates.endTime;

                quote.TermsAndConditions = model.TermsAndConditions;

                if (model.NumberOfPayments > 1)
                {
                    _paymentOptions.AddPaymentOptionsTo(quote);
                    var paymentOption = quote.PaymentOptions.First(x => x.NumberOfPayments > 1);

                    var accountName = model.Name;
                    if (model.AccountType.Equals("B"))
                    {
                        accountName = model.BusinessName;
                    }
                    var instalment = _repository.Q<Instalment>().FirstOrDefault(x => x.QuoteId == quote.Id);
                    if (instalment == null)
                    {
                        instalment = new Instalment();
                    }
                    instalment.AccountNumber = model.Account;
                    instalment.CollectionDate = model.CollectionDate.ToString();
                    instalment.CustomerId = quote.CustomerFK;
                    instalment.Deposit = paymentOption.InitialPayment;
                    instalment.FullPremium = paymentOption.InitialPayment + paymentOption.NumberOfPayments * paymentOption.SubsequentPayments;
                    instalment.MonthlyInstallment = paymentOption.SubsequentPayments;
                    instalment.QuoteId = quote.Id;
                    instalment.SortCode = model.SortCode;
                    instalment.Title = model.Title;
                    instalment.NumberOfPayments = paymentOption.NumberOfPayments;
                    instalment.AccountName = accountName;
                    instalment.BusinessName = model.BusinessName;
                    instalment.Name = model.Name;


                    if (instalment.Id == 0)
                    {
                        _repository.Add(instalment);
                    }
                }
                _repository.Commit();
                return Ok(true);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PaymentActionController.PaymentCards] post:api/v1/payments" + quoteReference, quoteReference);
                return BadRequest(errorid);
            }
        }

        [HttpPost]
        [Route("api/v1/payment/referralcallbacks")]
        public async Task<IHttpActionResult> AddReferralCallback(ReferralCallbackVm model)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(model.Email) && string.IsNullOrWhiteSpace(model.Phone))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Please include either an email address or phone number for us to get in touch with you" });
                }

                await _bus.Send<ReferralCallback>(m =>
                {
                    m.Email = model.Email;
                    m.Name = model.Name;
                    m.Phone = model.Phone;
                    m.ClientName = model.ClientName;
                    m.QuoteReference = model.QuoteReference;
                });

                return Ok(true);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PaymentActionController.AddReferralCallback] post:api/v1/payment/referralcallbacks", model.QuoteReference);
                return BadRequest(errorid);
            }
        }
    }
}

