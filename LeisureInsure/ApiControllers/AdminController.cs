﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ItOps.Commands.Email;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using ItOps.Commands;
using LeisureInsure.DB.Models.Identity;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Charges;
using LeisureInsure.ViewModels;
using NServiceBus;
using LeisureInsure.DB;
using LeisureInsure.SSIS;
using Quote = LeisureInsure.Services.Dal.LeisureInsure.Quotes.Quote;
using Policy = LeisureInsure.Services.Dal.LeisureInsure.Information.Policy;
using Newtonsoft.Json;
using System;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB.ViewModels;

using System.Web.UI.WebControls;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
//using RazorEngine;

namespace LeisureInsure.ApiControllers
{
    [Authorize]
    public class AdminController : ApiController
    {
        private readonly ApplicationUserManager _userManager;
        private readonly IEndpointInstance _bus;
        private readonly ILiRepository _repository;
        private readonly IRepository _rep;
        private readonly ISSISRepository _ssisRep;

        private readonly ICalculateQuoteTotals _totals;
        private IInterfaceWithTam _tam;

        public AdminController(
            ApplicationUserManager userManager,
            IEndpointInstance bus,
            ILiRepository repository,
            ISSISRepository ssisRep,
            IRepository rep,
            ICalculateQuoteTotals totals,
            IInterfaceWithTam tam)
        {
            _userManager = userManager;
            _bus = bus;
            _repository = repository;
            _rep = rep;
            _totals = totals;
            _tam = tam;
            _ssisRep = ssisRep;
        }

        [Route("api/v1/admin/testSSIS")]
        [HttpPost]
        public IHttpActionResult testSSIS()
        {
            try
            {
                _ssisRep.StoredProcedure<int>("S_TestSISS");

            }
            catch
            {

            }
            return Ok();
        }


        [Route("api/v1/admin/failedTAM")]
        [HttpPost]
        public IHttpActionResult FailedTamList()
        {
            var quotes = _rep.GetQuotesFailedTAM();

            return Ok(quotes);
        }

        [Route("api/v1/admin/documents")]
        [HttpGet]
        public IHttpActionResult Documents()
        {
            try
            {
                var docs = _repository.Q<Document>().OrderBy(x => x.Name).ToList();
                foreach (Document doc in docs)
                {

                    if (doc.Link.IndexOf(".pdf") == -1 && doc.Type != "SoF")
                    {
                        doc.Link = doc.Link + ".pdf";
                    }
                }
                return Ok(docs);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/v1/admin/documents");
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/admin/quote/AddToTam/{quoteReference}")]
        [HttpPost]
        public async Task AddToTam(int quoteReference)
        {
            var quote = _rep.GetQuotebyQuotePK(quoteReference);
            if (!string.IsNullOrEmpty(quote.BrokerName))
            {
                await _bus.Send<CreatePolicyForAgent>(m =>
                {
                    m.PolicyReference = quote.QuoteRef;
                    m.PayByInstallments = false;
                    m.AgentCode = quote.BrokerName;
                });
            }
            else
            {
                await _bus.Send<CreatePolicyForClient>(m =>
                {
                    m.PolicyReference = quote.QuoteRef;
                    m.PayByInstallments = false;
                    m.AgentCode = quote.BrokerName;
                });
                await _bus.Send<PaymentConfirmed>(m =>
                {
                    m.QuoteReference = quote.QuoteRef;
                });



            }

            //await _bus.Send<CreateClientOnTam>(m =>
            //{
            //    m.QuoteReference = quote.QuoteRef;
            //}).ConfigureAwait(false);
        }

        [Route("api/v1/admin/quote/addNote/{quoteReference}")]
        [HttpPost]
        public IHttpActionResult AddNote(string quoteReference, QuoteNotevm model)
        {

            string quoteref = "";
            try
            {
                var quote = _repository.Q<Quote>(x => x.Notes).First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);
                var note = new LeisureInsure.Services.Dal.LeisureInsure.Quotes.QuoteNotes
                {
                    Note = model.Note,
                    Author = model.Author,
                    QuoteId = quote.Id,
                    Title = model.Title
                };


                quote.Notes.Add(note);
                _repository.Commit();

                return Ok(quote);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quote/addNote{quoteReference}");
                return BadRequest(errorid);
            }
        }
        
        [Route("api/v1/admin/getErrors")]
        [HttpGet]
        public IHttpActionResult GetWebErrors()
        {
            var errors = _rep.getWebErrors().OrderBy(x => x.ErrorLogPK).ToList();

            return Ok(errors);
        }

        [Route("api/v1/admin/getBrokers")]
        [HttpGet]
        public IHttpActionResult GetBrokers()
        {
            try
            {
                var brokers = _repository.Q<Contact>().Where(x => x.EntityType.ToLower() == "broker" && x.TamClientRef != null).OrderBy(x => x.TamClientRef).ToList();

                return Ok(brokers);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/getBrokers");
                return BadRequest(errorid);
            }

        }


        [Route("api/v1/admin/quote/adddocument/{quoteReference}")]
        [HttpPost]
        public IHttpActionResult AddDocument(string quoteReference, AdminVm model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.Documents).First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);
                var document = _repository.Find<Document>(model.DocumentId);
                var quoteDocument = new QuoteDocument
                {
                    Description = document.Name,
                    Url = document.Link,
                    DocumentType = document.Type,
                    DocumentFK = model.DocumentId,
                    Lock = 1
                };
                if (quoteDocument.Url.IndexOf(".pdf") == -1 && quoteDocument.DocumentType != "SoF")
                {
                    quoteDocument.Url = quoteDocument.Url + ".pdf";
                }

                quote.Documents.Add(quoteDocument);
                _repository.Commit();

                return Ok(quote);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quote/adddocument/{quoteReference}");
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/admin/quote/deleteDocument/{quoteReference}")]
        [HttpPost]
        public IHttpActionResult DeleteDocument(
            string quoteReference, AdminVm model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.Documents).First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);
                var document = quote.Documents.First(x => x.Id == model.DocumentId);
                _repository.Delete(document);
                //_repository.StoredProcedure<QuoteDocument>("S_DeleteDocument", quote.Id);
                quote.Documents.Remove(document);
                _repository.Commit();

                return Ok(quote);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quote/deleteDocument/{quoteReference}");
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/admin/quote/updateEndorsement/{quoteReference}")]
        [HttpPost]
        public IHttpActionResult UpdateEndorsement(string quoteReference, AdminVm model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.Endorsements).First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);
                var endorsement = quote.Endorsements.First(x => x.Id == model.EndorsementId);
                endorsement.Text = model.EndorsementText;
                _repository.Commit();

                return Ok();
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quote/updateEndorsement/{quoteReference}");
                return BadRequest(errorid);
            }
        }


        [Route("api/v1/admin/quote/deleteEndorsement/{quoteReference}")]
        [HttpPost]
        public IHttpActionResult DeleteEndorsement(
            string quoteReference, AdminVm model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.Endorsements).First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);
                var endorsement = quote.Endorsements.First(x => x.Id == model.EndorsementId);
                _repository.Delete(endorsement);
                quote.Endorsements.Remove(endorsement);
                _repository.Commit();

                return Ok(quote);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quote/updateEndorsement/{quoteReference}");
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/admin/quote/addendorsement/{quoteReference}")]
        [HttpPost]
        public IHttpActionResult AddEndorsement(string quoteReference, AdminVm model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.Documents).First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);
                var endorsement = new Endorsement
                {
                    Text = model.EndorsementText,
                };
                quote.Endorsements.Add(endorsement);
                _repository.Commit();

                return Ok(quote);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quote/addendorsement/{quoteReference}");
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/admin/quote/update")]
        [HttpPost]
        public IHttpActionResult UpdateQuote(Quote model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.Discounts, x => x.ChargeSummaryItems, x => x.Answers)
                        .First(x => x.QuoteReference == model.QuoteReference && x.Password == model.Password);

                var quoteOverRide = quote.Buy;

                if (quote.LeisureInsureFees != model.LeisureInsureFees || quote.ChargeSummaryItems.Sum(x => x.Fees) != model.LeisureInsureFees)
                {
                    quote.Buy = -1;

                }
                if (model.BrokerName == "Remove")
                {
                    quote.BrokerFK = 0;
                    quote.BrokerName = null;
                    quote.IsAgent = false;
                    quote.TamBrokerCode = null;
                }
                else if (model.BrokerName != quote.BrokerName)
                {
                    quote.BrokerFK = model.BrokerFK;
                    quote.BrokerName = model.BrokerName;
                    quote.IsAgent = true;
                    quote.TamBrokerCode = model.BrokerName;
                }
                quote.LeisureInsureFees = model.LeisureInsureFees;
                quote.TaxRate = model.TaxRate;
                quote.CommissionPercentage = model.CommissionPercentage;
                quote.Commission = model.Commission;
                IList<Answer> delAnswers = new List<Answer>();
                IList<QuoteLine> delQuoteLines = new List<QuoteLine>();

                foreach (var line in quote.ChargeSummaryItems)
                {
                    var updatedLine = model.ChargeSummaryItems.FirstOrDefault(x => x.Id == line.Id);
                    if (updatedLine != null)
                    {
                        line.Net = Math.Round(updatedLine.Net, 2);
                        line.Territory = updatedLine.Territory;
                        line.indemnity = updatedLine.indemnity;
                        line.SumInsured = updatedLine.SumInsured;
                        line.Excess = updatedLine.Excess;
                        line.Net = updatedLine.Net;
                        line.PeriodOfCover = updatedLine.PeriodOfCover;
                    }
                    else
                    {
                        delQuoteLines.Add(line);

                    }
                }

                foreach (var chargeitem in model.ChargeSummaryItems.Where(x => x.Id == -1))
                {
                    quote.ChargeSummaryItems.Add(chargeitem);
                }

                foreach (var answer in quote.Answers)
                {
                    var updatedAnswer = model.Answers.FirstOrDefault(x => x.Id == answer.Id);
                    if (updatedAnswer != null)
                    {
                        answer.InputString = updatedAnswer.InputString;
                        answer.NoItems = updatedAnswer.NoItems;
                        answer.SumInsured = updatedAnswer.SumInsured;
                        answer.TerritoryCover = updatedAnswer.TerritoryCover;
                        answer.ListNo = updatedAnswer.ListNo;
                        answer.ListOrdinal = updatedAnswer.ListNo;

                    }
                    else
                    {
                        delAnswers.Add(answer);
                    }

                }

                foreach (var line in model.Answers.Where(x => x.Id == -1))
                {
                    quote.Answers.Add(line);
                }

                foreach (QuoteLine ql in delQuoteLines)
                {
                    _repository.Delete(ql);
                    quote.ChargeSummaryItems.Remove(ql);
                }
                foreach (Answer a in delAnswers)
                {
                    _repository.Delete(a);
                    quote.Answers.Remove(a);
                }

                foreach (var discount in quote.Discounts)
                {
                    var updatedDiscount = model.Discounts.FirstOrDefault(x => x.Id == discount.Id);
                    if (updatedDiscount != null)
                    {
                        discount.Price = Math.Round(updatedDiscount.Price, 2);
                    }
                }
                quote.Renewal = model.Renewal;
                quote.DateFrom = model.DateFrom;
                quote.DateTo = model.DateTo;

                quote.TimeFrom = model.TimeFrom;
                if (model.Customer != null)
                {
                    quote.Customer.EntityName = model.Customer.EntityName;
                }
                quote.BusinessDescription = model.BusinessDescription;

                quote.PolicyId = model.PolicyId;
                quote.ReferralType = model.ReferralType;
                quote.Notes = model.Notes;
                if (!string.IsNullOrEmpty(model.ContactEmail))
                {
                    quote.ContactEmail = model.ContactEmail;
                }
                _totals.Calculate(quote, quote.TaxRate);
                quote.Tax = Math.Round(quote.Tax, 2);
                quote.LegalFeesAdded = model.LegalFeesAdded;
                quote.LeisureInsureFees = Math.Round(model.LeisureInsureFees, 2);
                quote.Commission = Math.Round(model.Commission, 2);
                quote.LegalFees = Math.Round(quote.LegalFees, 2);
                if (quote.LegalFeesAdded == true)
                {
                    quote.Total = Math.Round(quote.NetPremium + quote.Tax + quote.LeisureInsureFees + quote.LegalFees, 2);
                }
                else
                {
                    quote.Total = Math.Round(quote.NetPremium + quote.Tax + quote.LeisureInsureFees, 2);
                }

                foreach (var charge in quote.ChargeSummaryItems)
                {

                    if (charge != null)
                    {
                        charge.Price = Math.Round(charge.Price, 2);
                        charge.SumInsured = Math.Round(charge.SumInsured, 2);
                        charge.Net = Math.Round(charge.Net, 2);
                    }
                }
                quote.CommissionPercentage = model.CommissionPercentage;
                quote.Buy = quoteOverRide;
                quote.OverrideDatesVal = model.OverrideDatesVal;
                decorateQuote(quote, 1);

                _repository.Commit();

                return Ok(quote);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quote/update{model.QuoteReference}");
                return BadRequest(errorid);
            }


        }

        [Route("api/v1/admin/quotes/emailcustomer/{quoteReference}")]
        [HttpPost]
        public async Task<IHttpActionResult> EmailCustomer(string quoteReference, QuoteQuery model)
        {
            try
            {
                var quote = _repository.Q<Quote>().First(x => x.QuoteReference == quoteReference &&
                                                                  x.Password == model.Passcode);
                await _bus.Send<EmailCustomerAfterReferral>(m =>
                {
                    m.QuoteReference = quote.QuoteReference;
                }).ConfigureAwait(false);

                return Ok();
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quotes/emailcustomer/{quoteReference}");
                return BadRequest(errorid);
            }
        }
        [Route("api/v1/admin/quotes/emailnewbusiness/{quoteReference}")]
        [HttpPost]
        public async Task<IHttpActionResult> EmailNewBusiness(string quoteReference, QuoteQuery model)
        {
            try
            {
                var quote = _repository.Q<Quote>().First(x => x.QuoteReference == quoteReference &&
                                                                  x.Password == model.Passcode);
                await _bus.Send<NewPolicyEmail>(m =>
                {
                    m.QuoteReference = quote.QuoteReference;
                }).ConfigureAwait(false);

                return Ok();
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quotes/emailnewbusiness/{quoteReference}");
                return BadRequest(errorid);
            }
        }



        [Route("api/v1/admin/quotes/emailcustomerRenewal/{quoteReference}")]
        [HttpPost]
        public async Task<IHttpActionResult> EmailCustomerRenewal(string quoteReference, QuoteQuery model)
        {
            try
            {
                var quote = _repository.Q<Quote>().First(x => x.QuoteReference == quoteReference &&
                                                                  x.Password == model.Passcode);

                _rep.RenewalEmailSent(quote.Id);
                quote.TermsAndConditions = true;
                quote.LockedForCustomer = false;
                quote.Purchased = 0;

                _repository.Commit();
                await _bus.Send<EmailRenewal>(m =>
                {
                    m.QuoteReference = quote.QuoteReference;
                }).ConfigureAwait(false);

                return Ok(quote);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/quotes/emailcustomerRenewal/{quoteReference}");
                return BadRequest(errorid);
            }
        }

        /// <summary>
        /// This is for testing
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Authorize]
        [Route("api/v1/admin/postname")]
        [HttpPost]
        public IHttpActionResult PostTest([FromBody]QuoteSearchVm name)
        {
            try
            {
                var quotes = _repository.Q<Quote>(x => x.ChargeSummaryItems,
                        x => x.Documents, x => x.Discounts, x => x.ChargeSummaryItems,
                        x => x.Answers, x => x.Endorsements);

                quotes = quotes.Take(10);

                var result = quotes.ToList();

                return Ok(result);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"api/v1/admin/postname");
                return BadRequest(errorid);
            }
        }

        [Authorize]
        [Route("api/v1/admin/search")]
        [HttpPost]
        public IHttpActionResult PostSearchQuotes([FromBody]QuoteSearchVm model)
        {

            string reference = "";

            try
            {

                var quotes = _repository.Q<Quote>(x => x.ChargeSummaryItems,
                        x => x.Documents, x => x.Discounts,
                        x => x.Answers, x => x.Endorsements, x => x.Notes); // Where(x => x.Purchased > 0)

                if (model != null)
                {
                    if (!string.IsNullOrEmpty(model.QuoteReference))
                    {
                        quotes = quotes.Where(x => x.QuoteReference.Contains(model.QuoteReference));
                    }
                    if (!string.IsNullOrEmpty(model.Email))
                    {
                        quotes = quotes.Where(x => x.Customer.Email == model.Email);
                    }
                    if (!string.IsNullOrEmpty(model.BrokerCode))
                    {
                        quotes = quotes.Where(x => x.TamBrokerCode == model.BrokerCode);
                    }
                    if (!string.IsNullOrEmpty(model.LastName))
                    {
                        quotes = quotes
                            .Where(x => x.Customer.LastName.Contains(model.LastName));
                    }
                    if (!string.IsNullOrEmpty(model.Postcode))
                    {
                        quotes = quotes.Where(x => x.PrimaryAddress.Postcode == model.Postcode);
                    }
                    if (!string.IsNullOrEmpty(model.TamReference))
                    {
                        quotes = quotes
                            .Where(x => x.TamPolicyCode.Contains(model.TamReference));
                    }
                }

                if (quotes.Count() > 500)
                {
                    quotes = quotes.OrderByDescending(x => x.DateCreated);
                    quotes = quotes.Take(500);
                }
                if (quotes.Count() == 1)
                {
                    decorateQuote(quotes.FirstOrDefault(), 0);

                }


                var sortedList = quotes;
                var result = quotes.ToList();


                //the payload must be a list of actual types not IQueryable
                return Ok(result);

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "post:api/v1/admin/search", model.QuoteReference);
                return BadRequest(errorid);
            }

        }

        private IHttpActionResult decorateQuote(Quote quote, int update)
        {
            try
            {
                IEnumerable<vmCertLists> certListItems = _rep.GetCertList(quote.Id).ToList();

                foreach (var a in quote.Answers)
                {
                    var listItem = certListItems.Where(x => x.QuoteInputPK == a.Id).FirstOrDefault();
                    if (listItem != null)
                    {
                        a.CertListItem = 1;
                    }
                    else
                    {
                        a.CertListItem = 0;
                    }
                    if (a.RatePk > 0)
                    {
                        a.Refer = _rep.checkRateReffered(a.RatePk);
                    }
                }
                if (update == 1)
                {
                    quote.BusinessDescription = _rep.setBusinessDescription(quote.Id, quote.BusinessDescription);
                }

                foreach (var doc in quote.Documents)
                {

                    if (doc.Url.IndexOf(".pdf") == -1 && doc.DocumentType != "SoF")
                    {
                        doc.Url = doc.Url + ".pdf";
                    }
                }
                if (quote.Notes.Count > 0)
                {
                    quote.Notes = quote.Notes.OrderBy(x => x.DateLogged).ToList();
                }
                foreach (var a in quote.Answers)
                {
                    if (a.RatePk > 0)
                    {
                        var r = _rep.getRatebyPK(a.RatePk);
                        if (r.ListRatePK != null && r.ListRatePK < 0)
                        {
                            int parentpk = r.ListRatePK * -1 ?? 0;
                            var parent = _rep.getRatebyPK(parentpk);
                            if (!a.InputString.Contains(parent.RateName))
                            {
                                a.InputString = parent.RateName + ": " + a.InputString;
                            }
                        }
                    }
                }
                return Ok(quote);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[AdminController.DecorateQuote] post:api/v1/admin/search", quote.QuoteReference);
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/brokers")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> AddBroker(AddBrokerVm model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.BrokerName))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Broker name is required" });
                }
                if (string.IsNullOrEmpty(model.Email))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Email is required" });
                }
                if (string.IsNullOrEmpty(model.Firstname) || string.IsNullOrEmpty(model.Surname))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Your name is required" });
                }
                if (string.IsNullOrEmpty(model.FsaNumber))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Your FCA number is required" });
                }
                if (string.IsNullOrEmpty(model.Address1) || string.IsNullOrEmpty(model.City) || string.IsNullOrEmpty(model.Postcode))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Your address is required" });
                }

                model.Email = model.Email.Trim();

                // Add it to the contacts table
                var agent = new Agent
                {
                    Email = model.Email,
                    EntityName = model.BrokerName,
                    LastName = model.Surname,
                    FirstName = model.Firstname,
                    FSANumber = model.FsaNumber,
                    Telephone = model.Telephone,
                    Locked = true,
                };

                _repository.Add(agent);
                _repository.Commit();

                await _bus.Send<CreateBrokerInTam>(m =>
                {
                    m.Email = model.Email;
                    m.EntityName = model.BrokerName.Trim();
                    m.FsaNumber = model.FsaNumber;
                    m.Lastname = model.Surname;
                    m.Firstname = model.Firstname;
                    m.Telephone = model.Telephone;
                    m.Address1 = model.Address1;
                    m.Address2 = model.Address2;
                    m.City = model.City;
                    m.Postcode = model.Postcode;
                    m.Location = model.Location;
                    m.ContactId = agent.Id;
                }).ConfigureAwait(false);

                // Start the process
                await _bus.Send<SendBrokerApplication>(m =>
                {
                    m.Email = model.Email;
                    m.Brokerage = model.BrokerName;
                    m.Name = model.Firstname + " " + model.Surname;
                }).ConfigureAwait(false);

                return Ok();
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/brokers{model.BrokerName}");
                return BadRequest(errorid);
            }
        }

        [Route("api/v1/brokerLive")]
        [HttpPost]
        public async Task<IHttpActionResult> AddLiveBroker(AddBrokerVm broker)
        {
            try
            {
                var tamCode = "";
                var _loc = "en-GB";
                var _brokerPK = 0;
                if (broker.Location == "2")
                {
                    _loc = "ga-IE";
                }
                var m = new UpdateAgentVm();

                m.Email = broker.Email;
                m.EntityName = broker.EntityName;
                m.FsaNumber = broker.FsaNumber;
                m.Lastname = broker.Surname;
                m.Firstname = broker.Firstname;
                m.Telephone = broker.Telephone;
                m.Address1 = broker.Address1;
                m.Address2 = broker.Address2;
                m.City = broker.City;
                m.County = broker.County;
                m.Postcode = broker.Postcode;
                m.Location = broker.Location;
                m.Locked = broker.Locked;
                m.ContactId = broker.ContactId;
                m.TamCode = broker.TamCode;

                // ADD IF BROKER DOESN'T EXIST IN TAM
                if (string.IsNullOrEmpty(broker.TamCode))
                {
                    tamCode = _tam.AddTamBroker(m).ToUpperInvariant();
                }
                else // ELSE UPDATE BROKER IN TAM
                {
                    _tam.UpdateAgent(m);
                    tamCode = broker.TamCode.ToUpperInvariant();
                }


                // GET Website record for agent if it exists
                //var agent = _repository.Q<Agent>().Where(x => x.TamClientRef == tamCode).FirstOrDefault();
                var agent = _rep.GetContactByBrokerCode(tamCode);
                // IF BROKER EXISTS ON THE WEBSITE UPDATE IT
                if (agent != null)
                {
                    agent.Email = m.Email;
                    agent.EntityName = m.EntityName ?? "";
                    agent.LastName = m.Lastname ?? "";
                    agent.FirstName = m.Firstname ?? "";
                    agent.FSANumber = m.FsaNumber;
                    agent.Telephone = m.Telephone;
                    agent.TamClientRef = tamCode;
                    agent.Locked = broker.Locked;
                    broker.ContactId = _rep.updateContact(agent);

                    _brokerPK = agent.ContactPK;
                }
                else // ADD BROKER TO WEBSITE
                {
                    agent = new Contacts();
                    agent.Email = m.Email;
                    agent.EntityName = m.EntityName ?? "";
                    agent.LastName = m.Lastname ?? "";
                    agent.FirstName = m.Firstname ?? "";
                    agent.FSANumber = m.FsaNumber;
                    agent.Telephone = m.Telephone;
                    agent.TamClientRef = tamCode;
                    agent.Locked = broker.Locked;
                    agent.ContactPK = 0;
                    broker.ContactId = _rep.updateContact(agent);
                    _brokerPK = agent.ContactPK;
                }

                // GET Website record for agent if it exists
                //var brokerAddress = _repository.Q<LeisureInsure.Services.Dal.LeisureInsure.Quotes.Address>().Where(x => x.ContactFK == broker.ContactId).FirstOrDefault();
                var brokerAddress = _rep.GetAddressByContactPK(_brokerPK);

                if (brokerAddress != null)
                {
                    brokerAddress.ContactFK = _brokerPK;
                    brokerAddress.Address1 = broker.Address1 ?? "";
                    brokerAddress.Address2 = broker.Address2 ?? "";
                    brokerAddress.Town = broker.City ?? "";
                    brokerAddress.County = broker.County ?? "";
                    brokerAddress.Postcode = broker.Postcode ?? "";
                    brokerAddress.Locale = _loc;
                    brokerAddress.AddressPK = _rep.updateAddress(brokerAddress);

                }
                else // ADD BROKER TO WEBSITE
                {
                    brokerAddress = new Addresses();
                    brokerAddress.ContactFK = _brokerPK;
                    brokerAddress.Address1 = broker.Address1 ?? "";
                    brokerAddress.Address2 = broker.Address2 ?? "";
                    brokerAddress.Town = broker.City ?? "";
                    brokerAddress.County = broker.County ?? "";
                    brokerAddress.Postcode = broker.Postcode ?? "";
                    brokerAddress.Locale = _loc;
                    brokerAddress.AddressPK = 0;
                    brokerAddress.AddressPK = _rep.updateAddress(brokerAddress);
                }

                _repository.Commit();

                var aspNetUser = _rep.GetAspNetUser(tamCode);

                if (aspNetUser == "Not Found")
                {
                    //IF BROKER HAS NO LOG ON
                    var user = new LiApplicationUser
                    {
                        Email = broker.Email,
                        UserName = tamCode
                    };
                    var result = await _userManager.CreateAsync(user, "123456");

                }
                else
                {
                    _rep.UpdateAspNetEmail(tamCode, broker.Email);
                }

                broker.TamCode = tamCode;


                return Ok(m);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/brokerlive{broker.BrokerName}");
                return BadRequest(errorid);
            }

        }


        [Route("api/v1/brokers/{brokerCode}")]
        [HttpGet]
        public IHttpActionResult GetAgent(string brokerCode)
        {
            try
            {
                var agent = _tam.GetAgent(brokerCode);
                //var contact = _repository.Q<Agent>().Where(x => x.TamClientRef == brokerCode).FirstOrDefault();
                agent.Locked = false;//contact.Locked;

                return Ok(agent);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/brokers/{brokerCode}");
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/brokers/{brokerCode}/update")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateAgent(string brokerCode, UpdateAgentVm model)
        {
            try
            {
                var tamCode = brokerCode.ToUpperInvariant();
                var agent = _repository.Q<Agent>().First(x => x.TamClientRef == tamCode);
                agent.Locked = model.Locked;
                _repository.Commit();
                await _bus.Send<UpdateAgentInTam>(m =>
                {
                    m.Email = model.Email;
                    m.Address1 = model.Address1;
                    m.Address2 = model.Address2;
                    m.City = model.City;
                    m.EntityName = model.EntityName;
                    m.Firstname = model.Firstname;
                    m.FsaNumber = model.FsaNumber;
                    m.Lastname = model.Lastname;
                    m.Postcode = model.Postcode;
                    m.TamCode = brokerCode;
                    m.Telephone = model.Telephone;
                });

                return Ok(true);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"api/v1/brokers/{brokerCode}/update");
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/brokers/admin")]
        [HttpPost]
        public async Task<IHttpActionResult> AddBrokerAdmin(AddBrokerVm model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Email) || string.IsNullOrEmpty(model.Password))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Please check the information" });
                }
                var contact = _repository.Q<Agent>().OrderByDescending(x => x.Id).First(x => x.Email == model.Email && x.TamClientRef != null);
                contact.Locked = false;

                var user = new LiApplicationUser
                {
                    Email = model.Email,
                    UserName = contact.TamClientRef,
                };

                var result = await _userManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = result.Errors.FirstOrDefault() });
                }
                _repository.Commit();

                return Ok();
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/brokers/admin{model.BrokerName}");
                return BadRequest(errorid);
            }
        }

        [Route("api/v1/underwriters")]
        [HttpPost]
        public async Task<IHttpActionResult> AddUnderwriter(AddBrokerVm model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.Password))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Password is required" });
                }
                if (string.IsNullOrEmpty(model.Email) && string.IsNullOrEmpty(model.Telephone))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Email or telephone is required" });
                }
                if (string.IsNullOrEmpty(model.Firstname))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Firstname is required" });
                }
                if (string.IsNullOrEmpty(model.Surname))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Surname is required" });
                }
                if (string.IsNullOrEmpty(model.TamCode))
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = "Tam Code is required" });
                }

                var user = new LiApplicationUser
                {
                    Email = model.Email,
                    UserName = model.TamCode,
                };
                var result = await _userManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest) { ReasonPhrase = result.Errors.FirstOrDefault() });
                }

                // Add it to the contacts table
                var contact = new Underwriter
                {
                    Email = model.Email,
                    Telephone = model.Telephone,
                    FirstName = model.Firstname,
                    LastName = model.Surname,
                    TamClientRef = model.TamCode,
                };
                _repository.Add(contact);
                _repository.Commit();

                return Ok();
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"api/v1/underwriters");
                return BadRequest(errorid);
            }
        }
        // renewals
        [Route("api/v1/GetRenewalsByCsr")]
        [HttpGet]
        public IHttpActionResult GetRenewalsByCsr(string csr)
        {
            try
            {

                IList<vmRenewalListItem> rl = _rep.GetRenewalsByCsr(csr).OrderBy(x => x.ExpiryDate).ToList();

                return Ok(rl);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/GetRenewalsByCsr");
                return BadRequest(ex.Message);
            }
        }

        [Route("api/v1/data/getPolicies")]
        public IHttpActionResult GetPolicies(int localeid = 0)
        {
            try
            {
                IList<vmPolList> p = new List<vmPolList>();

                p = _rep.getPolicies(localeid).ToList();


                return Ok(p);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, "api/v1/data/getPolicies");
                return BadRequest(errorid);
            }
        }

        [Route("api/v1/admin/quote/getPolicyDocuments/{quoteReference}")]
        [HttpPost]
        public IHttpActionResult GetPolicyDocuments(string quoteReference, AdminVm model)
        {

            try
            {
                var quote = _repository.Q<Quote>(x => x.Documents).First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);
                quote.Documents = quote.Documents.Where(x => x.Lock == 1).ToList();
                var polDocs = _rep.getDocuments(quote.Id).ToList();
                quote = _repository.Q<Quote>(x => x.Documents).First(x => x.QuoteReference == quoteReference && x.Password == model.Passcode);

                return Ok(quote);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/v1/data/getPolicies");
                return BadRequest(errorid);
            }
        }

        [Route("api/v1/data/getCovers")]
        public IHttpActionResult GetCovers()
        {
            try
            {
                IList<Covers> p = new List<Covers>();

                p = _rep.getCovers().ToList();

                return Ok(p);
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, "[AdminController.GetCovers] api/v1/data/getCovers");
                return BadRequest(errorid);
            }
        }

        [HttpGet]
        [Route("api/v1/data/getInstallments")]
        public HttpResponseMessage GetInstallments()
        {
            var xmlString = "<xml><name>Some XML</name></xml>";
            var result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StringContent(xmlString, Encoding.UTF8, "application/xml");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "testDownload.xml"
            };

            return result;
        }



    }

}

public class UpdateAgentVm : BrokerInformation
{
    public bool Locked { get; set; }
    public string Email { get; set; }
    public string TamCode { get; set; }
    public string FsaNumber { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public string EntityName { get; set; }
    public string Telephone { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string County { get; set; }
    public string Postcode { get; set; }
    public string Location { get; set; }
    public int ContactId { get; set; }

}
public class BrokerInfo
{
    public bool Locked { get; set; }
    public string Email { get; set; }
    public string TamCode { get; set; }
    public string FsaNumber { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public string EntityName { get; set; }
    public string Telephone { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string County { get; set; }
    public string Postcode { get; set; }
    public string Location { get; set; }
    public int ContactId { get; set; }
}

