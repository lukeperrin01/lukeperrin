﻿using System.Threading.Tasks;
using System.Web.Http;
using ItOps.Endpoint.Tam;
using LeisureInsure.Services.Dal.LeisureInsure;
using Newtonsoft.Json;
using NServiceBus;
using ItOps.Commands;

namespace LeisureInsure.ApiControllers
{
    public class TestController : ApiController
    {
        private readonly IEndpointInstance _endpointInstance;
        private readonly IInterfaceWithTam _tam;
        private ILiRepository _repository;

        public TestController(
            IEndpointInstance endpointInstance,
            IInterfaceWithTam tam,
            ILiRepository repository)
        {
            _endpointInstance = endpointInstance;
            _tam = tam;
            _repository = repository;
        }

        [HttpGet]
        [Route("api/v1/test/tam/addclient/{quoteReference}")]
        public Task AddTamClient(string quoteReference)
        {

            _tam.AddTamCustomer(quoteReference);

            _tam.AddTamPolicy(quoteReference, false, "");
            _tam.AddTamLegalPolicy(quoteReference, false, "");
            _tam.AddPolicyTransaction(quoteReference);
            _tam.AddTamCustomDec(quoteReference);
            _tam.AddLegalFeeTransaction(quoteReference);
            _tam.AddCardChargeTransaction(quoteReference);
            _tam.AddAgencyFeeTransaction(quoteReference);


            _tam.AddTaxTransaction(quoteReference);

            _tam.AddTamAttachment(quoteReference);
            _tam.AddMailMessage(quoteReference, "danny.woodhouse@leisureinsure.co.uk");
            //void AddTamBroker(BrokerInformation message);
            //void UpdateAgent(BrokerInformation message);
            //BrokerInformation GetAgent(string agentCode);
            //void AddAccountingContact(string quoteReference);
            //void AddNotes(string quoteReference);



            return Task.FromResult(true);
        }

        public Task AddTamPolicy(string quoteReference)
        {
            _tam.AddTamPolicy(quoteReference, false, "");
            return Task.FromResult(true);
        }
        public Task AddTamCustomDec(string quoteReference)
        {
            _tam.AddTamCustomDec(quoteReference);
            return Task.FromResult(true);
        }

        [HttpGet]
        [Route("api/v1/test/message")]
        public async Task Message()
        {
            await _endpointInstance.Send<Test>(e =>
            {
                e.Number = 1;
                e.Word = "thingy";
            });
        }

        [HttpGet]
        [Route("api/v1/test/repo")]
        public Task<string> TestRepoFunctions()
        {
            var result = _repository.Function<FInput>("f_inputs", 1, 1, 1);
            return Task.FromResult(JsonConvert.SerializeObject(result));
        }
    }

    public class FInput
    {
        public int CoverPk { get; set; }
        public int InputPk { get; set; }
        public string InputName { get; set; }
        public decimal InputOrdinal { get; set; }
    }
}
