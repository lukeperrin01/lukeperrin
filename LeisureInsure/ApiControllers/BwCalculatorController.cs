﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Information;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.ApiControllers
{
    [Authorize]
    public class BwCalculatorController : ApiController
    {
        readonly ILiRepository _repository;

        public BwCalculatorController(ILiRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("api/v1/calculators/policies")]
        public Task<List<Policy>> Policies()
        {
            return Task.FromResult(_repository.Q<Policy>().ToList());
        }

        [HttpGet]
        [Route("api/v1/calculators/covers")]
        public Task<List<Cover>> Covers()
        {
            return Task.FromResult(_repository.Q<Cover>().ToList());
        }
    }
}
