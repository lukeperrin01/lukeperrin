﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using LeisureInsure.ViewModels;
using Microsoft.Azure;
using LeisureInsure.DB.Logging;
using ItOps.Commands.Email;
using ItOps.Endpoint.Email;
using LeisureInsure.DB;


namespace LeisureInsure.ApiControllers
{
    public class CommunicationsController : ApiController
    {
        private readonly IRepository _repository;


        public CommunicationsController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpPost]
        [Route("api/v1/communications/callmeback")]
        public Task CallMeBack(CallMeBackVm model)
        {
            var mailBody = $"Please call {model.Name} on {model.Number}\n\nMessage: {model.Message}";

            using (var mailClient = new SmtpClient())
            {
                var message = new MailMessage();
                message.To.Add(new MailAddress(CloudConfigurationManager.GetSetting("callback.email.to")));
                message.From = new MailAddress("callbacks@leisureinsure.co.uk", "CALL BACK REQUEST");
                message.Subject = "NEW WEBSITE CALLBACK REQUEST";
                message.Body = mailBody;

                mailClient.Send(message);
            }
            return Task.FromResult(0);
        }

        [HttpPost]
        [Route("api/v1/communications/contactus")]
        public Task ContactUs(ContactUsVm model)
        {
            var mailBody = string.Format("Contact request from the website:\n Name: {0}\nCompany: {1}" +
                                         "\nPhone: {2}\nEmail: {3}\nFound us by: {4}\n\nMessage: {5}",
                    model.Name, model.Company, model.Phone, model.Email, model.HowDidYouFindUs, model.Message);

            using (var mailClient = new SmtpClient())
            {
                var message = new MailMessage();
                message.To.Add(new MailAddress(CloudConfigurationManager.GetSetting("contactus.email.to")));
                message.From = new MailAddress("contact@leisureinsure.co.uk", "CONTACT REQUEST");
                message.Subject = "NEW WEBSITE CONTACT REQUEST";
                message.Body = mailBody;

                mailClient.Send(message);
            }
            return Task.FromResult(0);
        }

        [HttpPost]
        [Route("api/v1/communications/email-quote")]
        public IHttpActionResult EmailQuote(EmailQuoteVm model)
        {
            try
            {
                var url = HttpContext.Current.Request.Url;

                var emailQuote = new EmailQuote
                {
                    QuoteReference = model.QuoteReference,
                    Email = model.Email,
                    Passcode = model.Passcode,
                    BaseLink = url.Scheme + "://" + url.Host + ":" + url.Port
                };

                var sender = new QuoteOps();               
                sender.EmailQuote(emailQuote);

                return Ok("0");
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[CommunicationsController.EmailQuote] api/v1/communications/email-quote" + model.Email, model.QuoteReference);
                return BadRequest(errorid);
            }
        }
    }
}

