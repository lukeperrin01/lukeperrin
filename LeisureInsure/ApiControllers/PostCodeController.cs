﻿using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Web.Http;
using Newtonsoft.Json;
using System;
using LeisureInsure.DB.Logging;
using Microsoft.Azure;


namespace LeisureInsure.ApiControllers
{
    public class PostCodeController : ApiController
    {


        [HttpGet]
        [Route("api/v1/postcode")]
        public IHttpActionResult GetAddress(string postcode)
        {

            string token = CloudConfigurationManager.GetSetting("craftyClicksToken");

            var addressList = new List<ClsAddress>();
            bool showErrorDetail = false;

            try
            {
                string url =
                              String.Format("http://pcls1.craftyclicks.co.uk/json/rapidaddress?postcode={0}&key={1}&response=data_formatted&lines=2",
                                  postcode, token);
               
                //Complete XML HTTP Request
                WebRequest request = WebRequest.Create(url);
                //Complete XML HTTP Response
                WebResponse response = request.GetResponse();

                //Declare and set a stream reader to read the returned XML
                StreamReader reader = new StreamReader(response.GetResponseStream());

                // Get the requests json object and convert it to in memory dynamic
                // Note: that you are able to convert to a specific object if required.
                var jsonResponseObject = JsonConvert.DeserializeObject<dynamic>(reader.ReadToEnd());

                // check that there are delivery points
                if (jsonResponseObject.delivery_point_count > 0)
                {

                    //If the node list contains address nodes then move on.
                    int i = 0;
                    foreach (var node in jsonResponseObject.delivery_points)
                    {
                        ClsAddress address = new ClsAddress()
                        {
                            AddressID = i,
                            AddressLine1 = node.line_1,
                            AddressLine2 = node.line_2,
                            County = jsonResponseObject.postal_county,
                            PostCode = jsonResponseObject.postcode,
                            Town = jsonResponseObject.town
                        };

                        addressList.Add(address);
                        i++;
                    }

                    return Ok(addressList);
                }
                else
                {
                    //If no node details, there will be an error message. 

                    foreach (var node in jsonResponseObject)
                    {
                        // Get the details of the error message and return it the user.
                        switch ((string)node.Value)
                        {
                            case "0001":
                                showErrorDetail = true;
                                throw new Exception("Post Code not found");
                            case "0002":
                                showErrorDetail = true;
                                throw new Exception("Invalid Post Code format");
                            case "7001":
                                throw new Exception("Demo limit exceeded");
                            case "8001":
                                throw new Exception("Invalid or no access token");
                            case "8003":
                                throw new Exception("Account credit allowance exceeded");
                            case "8004":
                                throw new Exception("Access denied due to access rules");
                            case "8005":
                                throw new Exception("Access denied, account suspended");
                            case "9001":
                                throw new Exception("Internal server error");
                            default:
                                throw new Exception((string)node.Value);
                        }
                    }

                    throw new Exception($"an error occured in PostCodeController");
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                string errorCode = "";
                errorCode = AppInsightLog.LogError(ex, $"[PostCodeController.GetAddress] api/v1/postcode{postcode}");
                if (!showErrorDetail)
                {
                    message = errorCode;
                }
                return BadRequest(message);
            }

            
        }
    }

    public class ClsAddress
    {
        #region Private variables

        private int _mAddressId;
        private string _mAddressLine1;
        private string _mAddressLine2;
        private string _mTown;
        private string _mCounty;
        private string _mPostCode;

        #endregion

        #region Constructor
        public ClsAddress()
        {

        }

        #endregion

        #region Properties

        public int AddressID
        {
            get { return this._mAddressId; }
            set { this._mAddressId = value; }
        }

        public string AddressLine1
        {
            get { return this._mAddressLine1; }
            set { this._mAddressLine1 = value; }
        }

        public string AddressLine2
        {
            get { return this._mAddressLine2; }
            set { this._mAddressLine2 = value; }
        }

        public string Town
        {
            get { return this._mTown; }
            set { this._mTown = value; }
        }

        public string County
        {
            get { return this._mCounty; }
            set { this._mCounty = value; }
        }

        public string PostCode
        {
            get { return this._mPostCode; }
            set { this._mPostCode = value; }
        }
        #endregion
    }
}
