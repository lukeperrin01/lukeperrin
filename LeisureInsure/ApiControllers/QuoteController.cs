﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.ViewModels;
using LeisureInsure.DB.ViewModels;
using System.Web;
using LeisureInsure.Charges;
using LeisureInsure.Charges.AdditionalCharges;
using LeisureInsure.Charges.Completion;
using LeisureInsure.Services.Services.Payment;
using LeisureInsure.Services.Services.Utilities;
using LeisureInsure.Services.Services.Validation;
using LeisureInsure.DB;
using NServiceBus;
using Address = LeisureInsure.Services.Dal.LeisureInsure.Quotes.Address;
using LeisureInsure.Charges.Discounts;
using Microsoft.Azure;
using System;
using LeisureInsure.DB.Logging;
using LeisureInsure.Services.Utilities;
using LeisureInsure.Helpers;
using ItOps.Commands;

//test patch
//make sure this doesnt get pulled in live..
//2nd test
//2nd patch test for live
namespace LeisureInsure.ApiControllers
{
    public class QuoteController : ApiController
    {
        private readonly ILiRepository _repository;
        private readonly IMapper _mapper;
        private readonly IAddPaymentOptions _paymentOptions;
        private readonly ICalculateAllCoverCharges _allCovers;
        private readonly IAddGlobalIrisInformation _globalIris;
        private readonly ICreateQuoteDetails _quoteDetails;
        private readonly IClock _clock;
        private readonly ICalculateTax _tax;
        private readonly IValidateQuotes _quote;
        private readonly IRepository _rep;
        private readonly IEndpointInstance _endpoint;
        private readonly IProvideDiscountEngines _discount;
        private readonly ICalculateQuoteTotals _totals;


        public QuoteController(
            ILiRepository repository,
            IRepository rep,
            IMapper mapper,
            IAddPaymentOptions paymentOptions,
            ICalculateAllCoverCharges allCovers,
            IAddGlobalIrisInformation globalIris,
            ICreateQuoteDetails quoteDetails,
            IClock clock,
            ICalculateTax tax, IValidateQuotes quote,
            IEndpointInstance endpoint,

        IProvideDiscountEngines discount, ICalculateQuoteTotals totals)
        {
            _repository = repository;
            _rep = rep;
            _mapper = mapper;
            _paymentOptions = paymentOptions;
            _allCovers = allCovers;
            _globalIris = globalIris;
            _quoteDetails = quoteDetails;
            _clock = clock;
            _tax = tax;
            _quote = quote;
            _endpoint = endpoint;
            _discount = discount;
            _totals = totals;
        }

        [HttpPut]
        [Route("api/v1/updateQuote/{quoteReference}")]
        public IHttpActionResult AdjustQuote(string quoteReference, UpdateQuoteVm model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.ChargeSummaryItems, x => x.Discounts, x => x.Answers, x => x.Addresses, x => x.Customers)
                        .First(x => x.QuoteReference == quoteReference && x.Password == model.Password);

                //update whether this a finance or one payment
                quote.PayOption = model.payOption;

                quote.LegalFeesAdded = model.LegalCare;

                quote.DateFrom = DateTime.Parse(model.dateFrom);

                quote.DateTo = DateTime.Parse(model.dateTo);

                if (quote.ReferralType != ReferralType.Overridden)
                {
                    quote.Documents = null;
                    _repository.Commit();
                    _rep.getDocuments(quote.Id);
                }

                var docs = _repository.Q<Quote>(x => x.Documents)
                        .First(x => x.QuoteReference == quoteReference && x.Password == quote.Password);

                if (quote.BrokerName != null && !quote.BrokerName.Contains("00000"))
                {
                    quote.IsAgent = true;
                    if (string.IsNullOrEmpty(quote.ContactEmail))
                    {
                        quote.ContactEmail = _rep.getBrokerEmail(quote.BrokerName);
                    }
                }

                quote.ExcessWaiverAdded = model.ExcessWaiver;
                quote.PaymentCardId = model.CardId;
                quote.LocaleId = model.LocaleId;
                quote.Documents = docs.Documents;

                //var taxRate = (decimal)0;
                quote.TaxRate = _tax.Rate(model.LocaleId, quote.DateFrom);
                //taxRate = quote.TaxRate;

                if (!string.IsNullOrEmpty(quote.Postcode))
                {
                    if (quote.Postcode.ToUpper().StartsWith("IM") || quote.Postcode.ToUpper().StartsWith("JE") || quote.Postcode.ToUpper().StartsWith("GY"))
                    {
                        quote.TaxRate = 0;

                    }
                }


                _totals.Calculate(quote, quote.TaxRate);
                _repository.Commit();
                _paymentOptions.AddPaymentOptionsTo(quote);
                if (quote.Buy == 1)
                {
                    purchaseQuote(quote.QuoteReference);
                }

                return Ok(quote);
            }
            catch (Exception ex)
            {
                string reference = "AdjustQuote";
                if (!String.IsNullOrEmpty(quoteReference))
                    reference += quoteReference;

                var errorid = AppInsightLog.LogError(ex, "[QuoteController.AdjustQuote] api/v1/updateQuote" + reference, quoteReference);
                return BadRequest(errorid);
            }
        }


        [HttpPut]
        [Route("api/v1/updateQuotePeriod/{quotePK}")]
        public IHttpActionResult UpdateQuotePeriod(int quotePK, string dateFrom, string dateTo)
        {
            try
            {
                var quote = _repository.Q<Quote>()
                        .First(x => x.Id == quotePK);

                quote.DateFrom = DateTime.Parse(dateFrom);
                quote.DateTo = DateTime.Parse(dateTo);
                //AppInsightLog.LogInfo($"UpdateQuotePeriod:oldDateTo:{quote.DateTo}newDateTo:{dateTo}");
                _repository.Commit();

                return Ok(quote);
            }
            catch (Exception ex)
            {
                string reference = "updateQuotePeriod";
                if (!String.IsNullOrEmpty(quotePK.ToString()))
                    reference += quotePK;

                var errorid = AppInsightLog.LogError(ex, "[QuoteController.UpdateQuotePeriod] api/v1/UpdateQuotePeriod" + reference, quotePK.ToString());
                return BadRequest(errorid);
            }
        }


        [HttpGet]
        [Route("api/v1/quotes/{quoteReference}")]
        public IHttpActionResult Quote(string quoteReference, [FromUri] string password)
        {

            bool notfound = false;

            // Retrieve the quote...
            try
            {
                var result = _repository.Q<Quote>(x => x.ChargeSummaryItems, x => x.Discounts, x => x.Answers)
                        .Where(x => x.QuoteReference == quoteReference && x.Password == password);

                var quote = result.FirstOrDefault();


                if (quote == null)
                {
                    notfound = true;
                    throw new Exception("No quote matches, please call us on 01993 700 761");
                }

                if (quote.PaymentCardId != 0)
                {
                    _totals.Calculate(quote);
                }
                //quote.Customers = (List<Customer>)_rep.getContacts(quote.Id);           
                _paymentOptions.AddPaymentOptionsTo(quote);
                _globalIris.AddInformation(quote);

                return Ok(quote);
            }
            catch (Exception ex)
            {
                string reference = "Quote";
                if (!String.IsNullOrEmpty(quoteReference))
                {
                    reference += quoteReference;
                    _rep.ReferQuote(0, quoteReference);
                }
                var errorid = AppInsightLog.LogError(ex, "[QuoteController.Quote] get:api/v1/quotes/" + reference, quoteReference);

                if (notfound)
                    errorid = ex.Message;

                return BadRequest(errorid);
            }
        }

        [HttpPost]
        [Route("api/v1/charges")]
        public IHttpActionResult Charges(WebsiteChargeModel model)
        {

            //clicking revise should not trigger a post to here
            if (model.Inputs.Count() == 0)
                return Ok("");

            int quoteRefer = 0;
            // Set up variables and chargeInputs
            int quoteId = 0;
            string quoteReference = "";
            var locale = _rep.getLocale(model.LocaleId);
            Quote quoteUpdate = null;
            Quote quote = null;
            //Get Policy info
            var policy = new DB.Policies();
            policy = _rep.GetPolicy(model.PolicyId);

            var chargeInputs = new ChargeCalculationInputs();
            int numberOfDays = 0;
            string policyType = "";
            IList<ChargeElements> chargeElements = new List<ChargeElements>();
            bool showError = false;
            try
            {
                // initial validation of charge model
                var validationResult = _quote.Validate(model);

                //validate code
                var enteredcode = model.Inputs.Where(x => x.RatePk == 5303).FirstOrDefault();
                if (!String.IsNullOrEmpty(enteredcode?.RateValue))
                {
                    var code = _rep.GetCode(enteredcode.RateValue);
                    if (code == null)
                    {
                        showError = true;
                        throw new Exception("Please enter a valid promo or producer code");
                    }
                }

                if (!validationResult.Valid)
                {
                    throw new HttpRequestValidationException(validationResult.ValidationErrors.Select(x => x.Message).Aggregate((last, current) => last + ", " + current));
                }
                else
                {
                    //If quote exists delete QuoteInputs, QuoteLines and QuoteDocuments associated with quote
                    if (!string.IsNullOrEmpty(model.QuoteReference))
                    {
                        quoteReference = model.QuoteReference;
                        var q = _rep.GetQuotebyQuoteRef(model.QuoteReference);

                        if (q.Purchased == 1)
                        {
                            throw new Exception($"QuoteRef: {model.QuoteReference} | Quote already purchased");
                        }
                        else
                        {

                            if (model.Inputs.Count > 0)
                            {
                                _repository.StoredProcedure<int>("S_DeleteQuoteChildren", model.QuoteReference).ToList();
                            }
                            else
                            {
                                throw new Exception($"QuoteRef: {model.QuoteReference} | please refresh page");
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                var browser = model.Browser;
                var policyName = policy.PolicyName;
                var errorid = AppInsightLog.LogError(ex, $"[QuoteController.Charges] url:post:api/v1/charges policy:{policyName} browser:{browser}", model.QuoteReference);
                if (showError)
                    errorid = ex.Message;
                return BadRequest(errorid);

            }
            #region Update or add new quote
            try
            {
                //Update or add new quote and update with model values
                if (string.IsNullOrWhiteSpace(model.QuoteReference))
                {
                    quoteUpdate = new Quote();
                }
                else
                {
                    quoteUpdate = _repository.Q<Quote>(x => x.Answers, x => x.ChargeSummaryItems, x => x.Discounts)
                        .First(x => x.QuoteReference == model.QuoteReference && x.Password == model.Password);

                    if (quoteUpdate.LockedForCustomer || quoteUpdate.Purchased > 0)
                    {
                        throw new HttpRequestValidationException("This policy has already been purchased");
                    }
                    quoteId = quoteUpdate.Id;
                }
                if (quoteId > 0)
                {
                    _repository.Commit();
                }
                else
                {
                    quoteUpdate.Password = Path.GetRandomFileName().Replace(".", "");
                    quoteUpdate.QuoteReference = "LEI" + quoteUpdate.Password;
                    _repository.Add(quoteUpdate);
                    _repository.Commit();
                    quoteUpdate.QuoteReference = "LEI" + quoteUpdate.Id.ToString("D8");
                    _repository.Commit();
                    quoteId = quoteUpdate.Id;
                    quoteReference = quoteUpdate.QuoteReference;
                }
                chargeInputs.PolicyInformation = new PolicyInformation
                {
                    quoteReference = quoteReference,
                    LocaleId = model.LocaleId,
                    PolicyId = model.PolicyId,
                    ExchangeRate = locale.ExchangeRate
                };
                chargeInputs.Answers = new List<ChargeCalculationAnswer>();

                if (User.Identity.IsAuthenticated)
                {
                    chargeInputs.PolicyInformation.BrokerTamName = User.Identity.Name;
                    chargeInputs.PolicyInformation.ShowFees = true;
                }
                // Are they biha or other discount org registered
                // TODO: Update this with NCASS for caterers
                var answer = model.Inputs.FirstOrDefault(x => x.RateTypeFK == 4);
                if (answer != null)
                {
                    if (answer.RateValue.ToLower() != "no")
                    {
                        chargeInputs.PolicyInformation.MemberOfDiscountOrganisation = true;
                    }
                }

                //Set up date and time variables - nb these are only temporary for a test quote
                //the real ones are set at the point of purchase           
                var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

                DateTime dateEffective = dateNow;
                DateTime dateExpires = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 23, 59, 0);
                string dateEffectiveTime = dateNow.ToString("HH:mm"); ;
                string dateExpiresTime = "23:59";
                //set dates and times

                //make sure 15 or 30 events is treated as annual
                //make sure one off events and fieldsports,exhibitors update number of days selected

                policyType = policy.PolicyType;

                //exhibitors
                var oneDayCover = model.Inputs.Where(x => x.RatePk == 1088).FirstOrDefault();
                var sevenDayCover = model.Inputs.Where(x => x.RatePk == 1090).FirstOrDefault();
                if (oneDayCover != null)
                {
                    numberOfDays = 1;
                }
                if (sevenDayCover != null)
                    numberOfDays = 7;
                //events
                var eventAnnual = model.Inputs.Where(x => x.RatePk == 1247 || x.RatePk == 1248).FirstOrDefault();
                var eventOneOff = model.Inputs.Where(x => x.RatePk == 1246 || x.RatePk == 1326).FirstOrDefault();
                if (eventOneOff != null)
                {
                    numberOfDays = 5;
                }
                if (eventAnnual != null)
                {
                    policyType = "A";
                }

                var eventSupplier = model.Inputs.Where(x => x.RatePk == 1326).FirstOrDefault();
                if (eventSupplier != null)
                {
                    var liability = model.Inputs.Where(x => x.RateTypeFK == 5).FirstOrDefault();
                    if (liability != null)
                        numberOfDays = (int)liability.NoItems;
                }


                //fieldsports event
                if (policy.PolicyPK == 8)
                {
                    var duration = model.Inputs.Where(x => x.RateTypeFK == RateType.DurationOfCover).FirstOrDefault();
                    if (duration != null)
                    {
                        if (numberOfDays == 0)
                            numberOfDays = Convert.ToInt32(duration.RateValue);
                    }
                }

                quoteUpdate.TimeFrom = dateEffectiveTime;
                quoteUpdate.TimeTo = dateExpiresTime;
                quoteUpdate.DateFrom = dateEffective;
                quoteUpdate.DateTo = dateExpires;
                quoteUpdate.LocaleId = model.LocaleId;
                quoteUpdate.PolicyId = model.PolicyId;
                quoteUpdate.PolicyType = policyType;
                quoteUpdate.Postcode = model.Inputs.Where(x => x.RateTypeFK == 1).Select(x => x.RateValue).FirstOrDefault() ?? null;
                quoteUpdate.TaxRate = _tax.Rate(model.LocaleId, dateEffective, quoteUpdate.QuoteReference);

                if (!string.IsNullOrEmpty(quoteUpdate.Postcode))
                {
                    if (quoteUpdate.Postcode.ToUpper().StartsWith("IM") || quoteUpdate.Postcode.ToUpper().StartsWith("JE") || quoteUpdate.Postcode.ToUpper().StartsWith("GY"))
                    {
                        quoteUpdate.TaxRate = 0;
                    }
                }


                quoteUpdate.DateCreated = dateNow;

                quoteUpdate.BihaNumber = model.Inputs.Where(x => x.RateTypeFK == 4).Select(x => x.RateValue).FirstOrDefault() ?? null;
                quoteUpdate.NoInstructors = int.Parse(model.Inputs.Where(x => x.RateTypeFK == 24).Select(x => x.RateValue).FirstOrDefault() ?? "0");
                quoteUpdate.ExcessWaiver = decimal.Parse(model.Inputs.Where(x => x.RateTypeFK == 23).Select(x => x.SumInsured.ToString()).FirstOrDefault() ?? "0");
                quoteUpdate.CustomerFK = int.Parse(quoteUpdate.CustomerFK.ToString() ?? "0");

                if (quoteUpdate.Renewal == null)
                {
                    quoteUpdate.Renewal = 0;
                }
                if (quoteUpdate.PolicySlot == null)
                {
                    quoteUpdate.PolicySlot = 0;
                }
                quoteUpdate.LandingPageId = model.LandingPageId;
                quoteUpdate.ReferralType = 0;
                quoteUpdate.AddressFK = int.Parse(quoteUpdate.AddressFK.ToString() ?? "0");


                if (quoteUpdate.ExcessWaiver > 0)
                {
                    quoteUpdate.ExcessWaiverAdded = true;
                }
                else
                {
                    quoteUpdate.ExcessWaiverAdded = false;
                }

                if (User.Identity.IsAuthenticated && !User.Identity.Name.Contains("00000"))
                {

                    quoteUpdate.BrokerName = User.Identity.Name;
                    quoteUpdate.TamBrokerCode = User.Identity.Name;

                    if (string.IsNullOrEmpty(quoteUpdate.ContactEmail))
                    {
                        quoteUpdate.ContactEmail = _rep.getBrokerEmail(quoteUpdate.BrokerName);
                    }
                    quoteUpdate.IsAgent = true;
                }
                else if (User.Identity.IsAuthenticated)
                {

                    if (string.IsNullOrEmpty(quoteUpdate.BrokerName))
                    {
                        quoteUpdate.BrokerName = User.Identity.Name;
                        quoteUpdate.TamBrokerCode = User.Identity.Name;
                    }
                    if (quoteUpdate.BrokerName != null && !quoteUpdate.BrokerName.Contains("00000"))
                    {
                        if (string.IsNullOrEmpty(quoteUpdate.ContactEmail))
                        {
                            quoteUpdate.ContactEmail = _rep.getBrokerEmail(quoteUpdate.BrokerName);
                        }
                    }
                    quoteUpdate.IsAgent = true;
                }
                else if (!User.Identity.IsAuthenticated)
                {
                    quoteUpdate.TamBrokerCode = null;
                    quoteUpdate.BrokerName = null;
                    quoteUpdate.IsAgent = false;
                }
                _repository.Commit();

            }

            catch (Exception ex)
            {
                var browser = model.Browser;
                var policyName = policy.PolicyName;
                var errorid = AppInsightLog.LogError(ex, $"[QuoteController.Charges] region:Update or add new quote url:post:api/v1/charges policy:{policyName} browser:{browser}", model.QuoteReference);
                return BadRequest(ex.Message);

            }
            #endregion 
            try
            {
                // Add quoteInputs
                IList<QuoteInputs> qi = new List<QuoteInputs>();
                foreach (var input in model.Inputs)
                {
                    int refer = 0;
                    if (input.RatePk > 0)
                    {
                        var rate = _rep.getRatebyPK(input.RatePk);
                        if (rate != null)
                        {
                            refer = rate.Refer;
                            if (rate.Refer > 0)
                            {
                                quoteRefer = rate.Refer;
                            }
                        }
                    }
                    if (input.RateTypeFK == 20 || input.RateTypeFK == 21)
                    {
                        input.RateValue = input.InputString;
                    }

                    qi.Add(new QuoteInputs
                    {
                        QuoteId = quoteId,
                        RatePK = input.RatePk,
                        CoverPK = input.CoverPk,
                        InputString = input.InputString,
                        RateValue = input.RateValue,
                        NoItems = input.NoItems,
                        SumInsured = input.SumInsured,
                        RateTypeFK = input.RateTypeFK,
                        ListNo = input.ListNo,
                        ListOrdinal = input.ListOrdinal,
                        Excess = input.Excess,
                        Indemnity = input.Indemnity,
                        Refer = refer

                    });
                }

                _rep.AddQuoteInputs(qi);
                quoteUpdate.BusinessDescription = _rep.setBusinessDescription(quoteId, "");
                chargeElements = _rep.GetChargeInputs(quoteId, model.LocaleId);

            }

            catch (Exception ex)
            {

                _rep.ReferQuote(quoteId, "");

                var browser = model.Browser;
                var policyName = policy.PolicyName;
                var errorid = AppInsightLog.LogError(ex, $"[QuoteController.Charges] region:Update or add new quote url:post:api/v1/charges policy:{policyName} browser:{browser}", model.QuoteReference);
                return BadRequest(errorid);
            }


            try
            {

                quote = _allCovers.Calculate(chargeInputs, chargeElements);


            }
            catch (Exception ex)
            {
                var browser = model.Browser;
                var policyName = policy.PolicyName;
                var errorid = AppInsightLog.LogError(ex, $"[QuoteController.Charges._allCovers.Calculate] url:post:api/v1/charges policy:{policyName} browser:{browser}", quoteReference);
                return BadRequest(errorid);
            }
            try
            {
                if (numberOfDays > 0)
                    quote.NumberOfDays = numberOfDays;

                var useOnlineDiscount = CloudConfigurationManager.GetSetting("use.online.discount") == "1";
                if (useOnlineDiscount)
                {
                    _discount.EngineFor(DicountType.Online).Calculate(quote, null, model.PolicyId);
                }

                //update dates/times before we calculate legal care/tax
                quote.DateTo = quoteUpdate.DateTo;
                quote.TimeTo = quoteUpdate.TimeTo;
                quote.DateFrom = quoteUpdate.DateFrom;
                quote.TimeFrom = quoteUpdate.TimeFrom;

                quote.QuoteReference = quoteUpdate.QuoteReference;
                quote.Id = quoteUpdate.Id;
                //calculate tax then fees
                _totals.Calculate(quote, quoteUpdate.TaxRate);




                if (chargeElements.Sum(x => x.Refer) > 0 || quoteRefer > 0)
                {
                    quote.ReferralType = ReferralType.Referred;
                }
                if (policy.PolicyPK == 9 || policy.PolicyPK == 10 || policy.PolicyPK == 11)
                {
                    //quote.ReferralType = ReferralType.Referred;
                    if (quote.LocaleId == 2)
                    {
                        quote.ReferralType = ReferralType.Referred;
                    }
                }
                if (policy.PolicyPK == 15 || policy.PolicyPK == 16)
                {
                    var turnover = model.Inputs.Where(x => x.RatePk == 2805).FirstOrDefault();

                    if (turnover != null && int.Parse(turnover.RateValue) > 75000)
                    {
                        quote.ReferralType = ReferralType.Referred;
                    }

                }

                quoteUpdate.NetPremium = quote.NetPremium;
                quoteUpdate.Tax = quote.Tax;
                quoteUpdate.LeisureInsureFees = quote.LeisureInsureFees;
                quoteUpdate.Total = quote.NetPremium + quote.Tax + quote.LeisureInsureFees;
                quote.Total = quoteUpdate.Total;

                //update quote


                quote.Password = quoteUpdate.Password;

                quote.BrokerName = quoteUpdate.BrokerName;
                quote.TamBrokerCode = quoteUpdate.TamBrokerCode;

                quote.ContactEmail = quoteUpdate.ContactEmail;
                quote.IsAgent = quoteUpdate.IsAgent;
                quote.BihaNumber = quoteUpdate.BihaNumber;
                quote.Postcode = quoteUpdate.Postcode;
                quote.ExcessWaiverAdded = quoteUpdate.ExcessWaiverAdded;
                quote.ExcessWaiver = quoteUpdate.ExcessWaiver;
                quote.NoInstructors = quoteUpdate.NoInstructors;
                quote.Renewal = quoteUpdate.Renewal;
                quote.PolicySlot = quoteUpdate.PolicySlot;
                quote.ContactFK = quoteUpdate.ContactFK;
                quote.AddressFK = quoteUpdate.AddressFK;
                quote.PolicyId = quoteUpdate.PolicyId;
                quote.PolicyType = policyType;
                quote.LocaleId = quoteUpdate.LocaleId;
                quote.LegalFeesAdded = quoteUpdate.LegalFeesAdded;
                quote.Total = quoteUpdate.Total;
                quote.TaxRate = quoteUpdate.TaxRate;
                quote.BusinessDescription = quoteUpdate.BusinessDescription;
                _mapper.Map(quote, quoteUpdate);

                _repository.Commit();

                if (quote.ReferralType != ReferralType.Overridden)
                {
                    quote.Documents = null;
                    _repository.Commit();
                    _rep.getDocuments(quote.Id);
                }

                //_rep.getDocuments(quote.Id);                     
                return Ok(quote);
            }
            catch (Exception ex)
            {
                var browser = model.Browser;
                var policyName = policy.PolicyName;
                var errorid = AppInsightLog.LogError(ex, $"[QuoteController.Charges.SavingQuoteDetails] url:post:api/v1/charges policy:{policyName} browser:{browser}", model.QuoteReference);
                return BadRequest(errorid);
            }
        }

        [HttpPost]
        [Route("api/v1/addCustomerDetails")]
        public IHttpActionResult AddCustomerDetails(vmContactDetail contactDetails)
        {
            try
            {
                var quote = _repository.Q<Quote>().FirstOrDefault(x => x.QuoteReference == contactDetails.QuoteReference && x.Password == contactDetails.PassCode);

                quote.ContactEmail = contactDetails.ContactEmail;
                quote.ContactName = contactDetails.ContactName;
                _repository.Commit();
                if (quote.CustomerFK == 0)
                {
                    if (contactDetails.ContactName == null)
                    {
                        quote.ContactName = contactDetails.Contact[0].FirstName;
                    }
                    else
                    {
                        quote.ContactName = contactDetails.ContactName;
                    }

                    if (quote.BrokerName != null)
                    {
                        var broker = _repository.Q<Contact>().Where(x => x.TamClientRef == quote.BrokerName && x.EntityType.ToLower() == "broker").FirstOrDefault();
                        if (broker != null)
                        {
                            quote.BrokerFK = broker.Id;
                        }
                    }
                    _repository.Commit();


                    _rep.deleteContactss(quote.Id);
                    if (quote == null)
                    {
                        throw new HttpException(403, "Not authorised to modify this quote");
                    }

                    foreach (var contact in contactDetails.Contact)
                    {
                        if (contact.ContactType == 1)
                        {
                            var newCustomer = _mapper.Map<Customer>(contact);
                            newCustomer.Email = contactDetails.ContactEmail;
                            contact.EntityType = "Customer";
                            quote.Customers.Add(newCustomer);
                            //quote.ContactEmail = newCustomer.Email;
                            _repository.Commit();

                            quote.CustomerFK = newCustomer.Id;
                            _repository.Commit();
                        }
                        else if (contact.ContactType == 2)
                        {
                            var newAgent = _mapper.Map<Agent>(contact);
                            quote.BrokerFK = newAgent.Id;
                            quote.Agent = newAgent;
                            quote.Agent.Email = contactDetails.ContactEmail;
                            //quote.ContactEmail = newAgent.Email;
                            _repository.Commit();
                        }
                        else if (contact.ContactType == 3)
                        {
                            var newCsr = _mapper.Map<Underwriter>(contact);
                            quote.Underwriters.Add(newCsr);
                            _repository.Commit();

                            quote.ContactFK = newCsr.Id;
                            _repository.Commit();
                        }

                    }

                    foreach (var address in contactDetails.Address)
                    {
                        if (address.AddressType == 1)
                        {
                            var add = _mapper.Map<Address>(address);
                            quote.Addresses.Add(add);
                            _repository.Commit();
                            quote.AddressFK = add.Id;
                            _repository.Commit();
                        }
                    }

                    _repository.Commit();

                    _rep.setQuoteCustomerPK(quote.Id);

                    //_endpoint.Send<AmendTAMClient>(m => { m.QuoteReference = quote.QuoteReference; });

                    //int _customerPK = _customerRepository.AddContact(contactDetails.Contact);
                    //_customerRepository.AddAddress(contactDetails.Address);

                    //ComeBackDW
                }
                return Ok(1);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[QuoteController.Charges.AddCustomerDetails] post:api/v1/addCustomerDetails", contactDetails.QuoteReference);
                return BadRequest(errorid);
            }
        }

        [HttpPost]
        [Route("api/v1/updateQuoteDates/{quoteReference}")]
        public IHttpActionResult UpdateQuoteDates(string quoteReference, PaymentInformationVm model)
        {
            try
            {
                var quote = _repository.Q<Quote>(x => x.Answers)
                    .FirstOrDefault(x => x.QuoteReference == quoteReference);

                if (quote == null)
                {
                    throw new Exception("Quote not found");
                }


                //ensure dates times are checked for client
                var dt = new vmDateCheck()
                {
                    startDate = DateTime.Parse(model.StartDate),
                    startTime = model.TimeFrom,
                    endDate = DateTime.Parse(model.EndDate),
                    endTime = model.TimeTo,
                    PolicyPK = quote.PolicyId,
                    PolicyType = quote.PolicyType,
                    endTimeRequired = model.endTimeRequired,
                    startTimeRequired = model.startTimeRequired,
                    dateToRequired = model.dateToRequired,
                    numberOfDays = model.numberOfDays
                };

                var dateHelper = new DateTimeHelper();
                var dates = dateHelper.DateChecker(dt);
                quote.DateFrom = dates.startDate;
                quote.DateTo = dates.endDate;
                quote.TimeFrom = dates.startTime;
                quote.TimeTo = dates.endTime;

                _repository.Commit();

                return Ok(0);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[QuoteController.UpdateQuoteDates] post:api/v1/updateQuoteDates/{quoteReference}", quoteReference);
                return BadRequest(errorid);
            }
        }

        public void purchaseQuote(string quoteReference)
        {
            //note, this is not used for agents, does not need to be an API method

            bool alreadyPurchased = false;

            try
            {
                var quote = _repository.Q<Quote>(x => x.ChargeSummaryItems, x => x.Discounts, x => x.Answers)
                        .First(x => x.QuoteReference == quoteReference);

                if (quote.LockedForCustomer)
                {
                    alreadyPurchased = true;
                    throw new HttpRequestValidationException("This policy has already been purchased");
                }

                _repository.Commit();
                if (quote.PaymentCardId != 0)
                {
                    _globalIris.AddInformation(quote);
                    quote.Purchased = 1;
                    _repository.Commit();
                    _endpoint.Send<CreatePolicyForClient>(m =>
                    {
                        m.PolicyReference = quote.QuoteReference;
                    });
                }
            }
            catch (Exception ex)
            {
                string reference = "purchaseQuote";
                if (!String.IsNullOrEmpty(quoteReference))
                    reference += quoteReference;

                var errorid = AppInsightLog.LogError(ex, "[QuoteController.purchaseQuote] post:api/v1/purchaseQuote" + reference, quoteReference);

                if (alreadyPurchased)
                    errorid = ex.Message;


            }
        }

    }
}

