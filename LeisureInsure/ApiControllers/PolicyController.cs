﻿using LeisureInsure.DB;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.DB.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LeisureInsure.Charges.AdditionalCharges;
using Microsoft.Azure;


namespace LeisureInsure.ApiControllers
{
    public class PolicyController : ApiController
    {        
        private const string ukLocale = "en-GB";
        private const string eirLocale = "ga-IE";        
        

        public PolicyController()
        {
            
        }


        public vmLocales getLocales(string strLocale,int policyid)
        {
            //this should not be an api method as we are calling it directly from getHome
            var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
            HttpCookie locale = new HttpCookie("locale");

            if (string.IsNullOrWhiteSpace(strLocale))
            {
                if (context == null || !context.Request.Cookies.AllKeys.Contains("locale"))
                {

                    string culture = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                    if (culture == ukLocale || culture == eirLocale)
                    {
                        locale.Value = culture;
                    }
                    else
                    {
                        locale.Value = ukLocale;
                    }
                    locale.Expires = DateTime.Now.AddMonths(12);
                    context?.Response.Cookies.Add(locale);
                    strLocale = locale.Value;
                }
                else
                {
                    var httpCookie = context.Request.Cookies["locale"];
                    if (httpCookie != null)
                    {
                        strLocale = httpCookie.Value;
                    }
                    else
                    {
                        strLocale = ukLocale;
                    }
                }
            }
            else
            {
                locale.Expires = DateTime.Now.AddMonths(12);
                locale.Value = strLocale;
                context?.Response.Cookies.Add(locale);
                strLocale = locale.Value;
            }

            var locales = new vmLocales();

            using (var repo = new PoliciesRepository())
            {
                locales = repo.GetLocales(strLocale, policyid);
            }

            return locales;
        }


        [Route("api/v1/data/getLocales")]
        public IHttpActionResult getLocalesAPI(string strLocale = "",int policyid = 1)
        {
            try 
            {
                var result = getLocales(strLocale,policyid);

                //force to UK for live website only
                if (CloudConfigurationManager.GetSetting("website.address") == "http://leisureinsure.co.uk")
                {                    
                    if (result.localeSelected.country == "Eire")
                        strLocale = "en-GB";
                    result = getLocales(strLocale,policyid);
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[PolicyController.getLocales] api/v1/data/getLocales" + strLocale);
                return BadRequest(errorid);
            }
        }

       /// <summary>
       /// Get Policies
       /// </summary>
       /// <param name="policyPk"></param>
       /// <param name="strLocale"></param>
       /// <returns></returns>
        [Route("api/v1/data/getallpolicies")]
        [HttpGet]
        public IHttpActionResult GetAllPolicies(int localeID)
        {
            try
            {

                var policies = new vmPolicies();

                using (var repo = new Repository())
                {
                    policies = repo._getPolicies(localeID, 0);
                }

                

                return Ok(policies.policyList);
            }
            catch (Exception ex)
            {
                string reference = "GetHome";               

                var errorid = AppInsightLog.LogError(ex, "[PolicyController.GetAllPolicies] api/v1/data/policy" + reference);
                return BadRequest(errorid);
            }
        }
       


        [HttpGet]
        [Route("api/v1/data/getlandingUrl/{url}")]
        public IHttpActionResult GetLandingUrl(string url)
        {
            try
            {
                var landing = new LandingPages();

                using (var repo = new LeisureInsureEntities())
                {
                    landing = repo.LandingPages.FirstOrDefault(x => x.Url == url);                                       
                }

                return Ok(landing);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/v1/data/getlandingQuote/{quoteRef}/{password}")]
        public IHttpActionResult GetLandingQuote(string quoteRef,string password)
        {
            try
            {
                var landingdata = new LandingPages();
                using (var repo = new LeisureInsureEntities())
                {
                    var quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == quoteRef && x.PWord == password);
                    landingdata = repo.LandingPages.FirstOrDefault(x => x.NewPolicyId == quote.PolicyFK);
                }

                return Ok(landingdata);
            }
            catch (Exception ex)
            {

                return BadRequest();
            }
        }



        /// <summary>
        /// Load new policy inputs
        /// </summary>
        /// <param name="strLocale"></param>
        /// <param name="quotePk"></param>
        /// <param name="policyPk"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/v1/data/newpolicy")]
        public IHttpActionResult GetPolicyInputs(string strLocale = "", int quotePk = 0, int policyPk = -1)
        {
            try
            {

                var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
                Contacts broker = null;
                Contacts csr = null;
                var csrInitial = "";

                var user = User.Identity;               
                if(user.IsAuthenticated)
                {
                    //are we a broker?
                    using (var repo = new LeisureInsureEntities())
                    {
                        //tamclientRef will be login, ie TESTAGO, LP00000
                        broker = repo.Contacts.FirstOrDefault(x => x.TamClientRef == user.Name && x.ContactType == 2);
                        csr = repo.Contacts.FirstOrDefault(x => x.TamClientRef == user.Name && x.ContactType == 4);
                    }
                }

                bool isAgent = broker != null ? true : false ;
                bool isCsr = csr != null ? true : false;

                if (isCsr)
                    csrInitial = csr.TamClientRef.Substring(0, 2);

                var locales = getLocales(strLocale,policyPk);                                  
                
               
                var policiesRepo = new PoliciesRepository();

                var policyData = policiesRepo.GetPolicyData(policyPk,locales.localeSelected,isAgent);

                if(isAgent)
                {
                    //we already have the commision% which is set at the policy level
                    policyData.TamBrokerCode = broker.TamClientRef;
                    policyData.BrokerName = broker.TamClientRef;
                }

                policyData.CSR = csrInitial;

                decimal taxRate;
                using (var repo = new Repository())
                {
                    var taxCalc = new TaxCalculator(repo);
                    taxRate = taxCalc.Rate(locales.localeSelected.localePK, dateNow);
                }
               
                policyData.TaxRate = taxRate;
                var legalRate = CloudConfigurationManager.GetSetting("legalcare.rate");
                policyData.LegalRate = Convert.ToDecimal(legalRate);             

                policyData.Locale = locales.localeSelected;              

                return Ok(policyData);
            }
            catch (Exception ex)
            {
                string reference = "GetHome";
                if (policyPk != 0)
                    reference += $"Policy:{policyPk.ToString()}";
                if (quotePk != 0)
                    reference += $"Quote:{quotePk.ToString()}";

                var errorid = AppInsightLog.LogError(ex, "api/v1/data/policy" + reference, quotePk.ToString());
                return BadRequest(errorid);
            }
        }


        //[Route("api/v1/data/getCharges")]
        //[HttpPost]
        //public IHttpActionResult GetCharges(List<vmInputs> _Inputs)
        //{
        //    try
        //    {
        //        IList<vmChargeSummary> _charges = _repository.GetCharges(_Inputs);
        //        return Ok(_charges);
        //    }
        //    catch (Exception ex)
        //    {
        //        var errorid = AppInsightLog.LogError(ex, "post:api/v1/data/getCharges");
        //        return BadRequest(errorid);
        //    }
        //}
    }
}
