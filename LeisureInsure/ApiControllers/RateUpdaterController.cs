﻿using LeisureInsure.DB;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.Azure;


namespace LeisureInsure.ApiControllers
{
    public class RateUpdaterController : ApiController
    {
        private readonly IRepository _repository;
        private const string ukLocale = "en-GB";
        private const string eirLocale = "ga-IE";        

        public RateUpdaterController() : this(new Repository()) { }

        public RateUpdaterController(IRepository repository)
        {
            _repository = repository;
        }

        [Route("api/v1/ratechecker/updaterate")]
        [HttpPut]
        public IHttpActionResult UpdateRate([FromBody] RateView ratemodel)
        {
            try
            {
                using (var repo = new LeisureInsureEntities())
                {
                    var rate = repo.RatesNew.FirstOrDefault(x => x.Id == ratemodel.Id);
                    rate.Rate = ratemodel.Rate;
                    rate.DivBy = ratemodel.DivBy;
                    rate.FirstRate = ratemodel.FirstRate;
                    rate.DiscountFirstRate = ratemodel.DiscountFirstRate;
                    rate.DiscountSubsequentRate = ratemodel.DiscountSubsequentRate;
                    rate.Indemnity = ratemodel.Indemnity;
                    rate.Excess = ratemodel.Excess;
                    rate.MinimumRate = ratemodel.MinimumRate;

                    repo.SaveChanges();
                }
               
                return Ok(ratemodel);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/v1/ratechecker/updaterate");
                return BadRequest(errorid);
            }
        }

        [Route("api/v1/ratechecker/updatelocale")]
        [HttpPut]
        public IHttpActionResult UpdateLocale([FromBody] Locales localeModel)
        {
            try
            {
                using (var repo = new LeisureInsureEntities())
                {
                    var locale = repo.Locales.FirstOrDefault(x => x.LocalePK == localeModel.LocalePK);
                    locale.Multiplier = localeModel.Multiplier;
                    locale.ExchangeRate = localeModel.ExchangeRate;
                    locale.TamAgency = localeModel.TamAgency;
                    locale.LocaleRefer = localeModel.LocaleRefer;                   

                    repo.SaveChanges();
                }

                return Ok(localeModel);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/v1/ratechecker/updaterate");
                return BadRequest(errorid);
            }
        }

        [Route("api/v1/ratechecker/getpolicies")]
        [HttpGet]
        public IHttpActionResult GetAllPolicies()
        {
            try
            {
                var policyList = new List<PolicyView>();

                using (var repo = new PoliciesRepository())
                {
                    policyList = repo.GetPolicesForUpdating();
                }                             

                return Ok(policyList);
            }
            catch (Exception ex)
            {                           
                var errorid = AppInsightLog.LogError(ex, "api/v1/ratechecker/getpolicy");
                return BadRequest(errorid);
            }
        }

        [Route("api/v1/ratechecker/getlocales")]
        [HttpGet]
        public IHttpActionResult GetAllLocales()
        {
            try
            {
                //var loacaleModels = new List<LocaleView>();
                var locales = new List<Locales>();

                using (var repo = new LeisureInsureEntities())
                {
                    locales = repo.Locales.ToList();
                }

                return Ok(locales);                
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/v1/ratechecker/getlocales");
                return BadRequest(errorid);
            }
        }





    }
}
