﻿using System.Threading.Tasks;
using System.Web.Http;
using LeisureInsure.ViewModels;
using NServiceBus;
using ItOps.Commands;
using LeisureInsure.DB;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.ApiControllers
{
    [Authorize]
    public class UnderwriterController : ApiController
    {
        private readonly IEndpointInstance _endpoint;

        public UnderwriterController(IEndpointInstance endpoint)
        {
            _endpoint = endpoint;
        }

        [HttpPost]
        [Route("api/v1/underwriter/confirmAgentPolicy")]
        public async Task<IHttpActionResult> StartPurchaseProcess(UnderwriterStartPurchaseProcessVm model)
        {
            //get quote for tam broker code
            try
            {
                var repo = new Repository();
                var quote = repo.GetQuote(model.QuoteReference);
                if (quote.TamBrokerCode == null)
                    return BadRequest("Please make sure you select a valid agent quote, this is for a client");

                await _endpoint.Send<CreatePolicyForAgent>(m =>
                {
                    m.PolicyReference = model.QuoteReference;
                    m.PayByInstallments = false;
                    m.AgentCode = quote.TamBrokerCode;
                });

                return Ok();
            }
            catch (System.Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/v1/underwriter/confirmAgentPolicy");
                return BadRequest($"{ex.Message} more details are logged with errorid:{errorid}");
            }
           
        }

        [HttpPost]
        [Route("api/v1/underwriter/confirmClientPayment")]
        public async Task<IHttpActionResult> StartPaymentProcess(UnderwriterStartPurchaseProcessVm model)
        {
            try
            {
                var repo = new Repository();
                var quote = repo.GetQuote(model.QuoteReference);

                if (quote.TamBrokerCode != null)
                    return BadRequest("Please make sure you select a valid client quote, this is for a broker");

                await _endpoint.Send<PaymentConfirmed>(m =>
                {
                    m.QuoteReference = model.QuoteReference;
                });

                return Ok();
            }
            catch (System.Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/v1/underwriter/confirmClientPayment");
                return BadRequest($"{ex.Message} more details are logged with errorid:{errorid}");
            }

           
        }
    }
}