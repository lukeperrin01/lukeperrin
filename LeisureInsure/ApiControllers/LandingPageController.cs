﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.LandingPages;
using LeisureInsure.Services.Services.Utilities;
using LeisureInsure.DB;

namespace LeisureInsure.ApiControllers
{
    public class LandingPageController : ApiController
    {
        private readonly ILiRepository _repository;
        private readonly IClock _clock;

        public LandingPageController(
            ILiRepository repository,
            IClock clock)
        {
            _repository = repository;
            _clock = clock;
        }

        [HttpGet]
        [Route("api/v1/landingpages")]
        public Task<List<LandingPage>> All()
        {
            return Task.FromResult(_repository.Q<LandingPage>(x => x.NavigationData).ToList());
        }

        [HttpGet]
        [Route("api/v1/landingpages/{url}")]
        public Task<LandingPage> LandingPage(string url)
        {
            var xxx = _repository.Q<LandingPage>(y => y.NavigationData).FirstOrDefault(y => y.Url == url);
            var returnValue = Task.FromResult(_repository.Q<LandingPage>(x => x.NavigationData).FirstOrDefault(x => x.Url == url));
            return returnValue;
        }

        [HttpGet]
        [Route("api/v1/landingpages/{id:int}")]
        public Task<LandingPage> LandingPage(int id)
        {
            return Task.FromResult(_repository.Q<LandingPage>(x => x.NavigationData).First(x => x.Id == id));
        }

        [HttpGet]
        [Route("api/v1/landingpages/{quoteRef}/{password}")]
        public IHttpActionResult GetLandingPage(string quoteRef, string password)
        {
            try
            {
                var landingdata = new LandingPages();
                using (var repo = new LeisureInsureEntities())
                {
                    var quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == quoteRef && x.PWord == password);
                    landingdata = repo.LandingPages.FirstOrDefault(x => x.NewPolicyId == quote.PolicyFK);
                }

                return Ok(landingdata);
            }
            catch (System.Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("api/v1/landingpages/{id}")]
        [Authorize]
        public HttpResponseMessage Update(int id, LandingPage landingPage)
        {
            var existingLandingPage = _repository.Q<LandingPage>(x => x.NavigationData).FirstOrDefault(x => x.Id == id);
            if (existingLandingPage == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            existingLandingPage.Description = landingPage.Description;
            existingLandingPage.LandingPageDescription = landingPage.LandingPageDescription;
            existingLandingPage.Keywords = landingPage.Keywords;
            existingLandingPage.PictureUrl = landingPage.PictureUrl;
            existingLandingPage.Title = landingPage.Title;
            existingLandingPage.Url = landingPage.Url;
            existingLandingPage.PolicyId = landingPage.PolicyId;
            existingLandingPage.LastModified = _clock.LocalTime;

            existingLandingPage.Header1 = landingPage.Header1;
            existingLandingPage.Paragraph1 = landingPage.Paragraph1;
            existingLandingPage.Buynow1 = landingPage.Buynow1;
            existingLandingPage.ChooseUs1 = landingPage.ChooseUs1;
            existingLandingPage.ChooseUs2 = landingPage.ChooseUs2;
            existingLandingPage.ChooseUs3 = landingPage.ChooseUs3;
            existingLandingPage.ChooseUs4 = landingPage.ChooseUs4;
            existingLandingPage.Header2 = landingPage.Header2;
            existingLandingPage.Paragraph2 = landingPage.Paragraph2;


            // Nothing there...
            if (landingPage.NavigationData == null)
            {
                existingLandingPage.NavigationData.Clear();
            }
            else
            {
                // Remove any navigation data that is no longer present
                var toDelete = existingLandingPage.NavigationData
                    .Select(x => x.Id)
                    .Except(landingPage.NavigationData.Select(x => x.Id));
                foreach (var source in existingLandingPage.NavigationData.Where(x => toDelete.Contains(x.Id)).ToList())
                {
                    existingLandingPage.NavigationData.Remove(source);
                    _repository.Delete(source);
                }
            }

            if (landingPage.NavigationData != null)
            {
                // Get the list of new items
                var toAdd = landingPage.NavigationData
                    .Select(x => x.Id)
                    .Except(existingLandingPage.NavigationData.Select(x => x.Id))
                    .ToList();

                // Add or update each item
                foreach (var navigationData in landingPage.NavigationData)
                {
                    if (toAdd.Contains(navigationData.Id))
                    {
                        existingLandingPage.NavigationData.Add(navigationData);
                    }
                    else
                    {
                        var existingNavigationData = existingLandingPage.NavigationData
                            .FirstOrDefault(x => x.Id == navigationData.Id);
                        if (existingNavigationData != null)
                        {
                            existingNavigationData.InputId = navigationData.InputId;
                            existingNavigationData.RateId = navigationData.RateId;
                        }
                    }
                }
            }
            
            _repository.Commit();

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("api/v1/landingpages")]
        [Authorize]
        public LandingPage Create()
        {
            var newLandingPage = new LandingPage
            {
                LandingPageDescription = string.Empty,
                Description = string.Empty,
                Keywords = string.Empty,
                PictureUrl = string.Empty,
                Title = "NEW LANDING PAGE",
                Url = string.Empty,
                LastModified = _clock.LocalTime,
            };
            _repository.Add(newLandingPage);
            _repository.Commit();
            return newLandingPage;
        }

        [HttpDelete]
        [Route("api/v1/landingpages/{id}")]
        [Authorize]
        public HttpResponseMessage Delete(int id)
        {
            var landingPage = _repository.Q<LandingPage>(x => x.NavigationData).FirstOrDefault(x => x.Id == id);
            if (landingPage != null)
            {
                foreach (var navigationData in landingPage.NavigationData.ToList())
                {
                    _repository.Delete(navigationData);
                }
                _repository.Delete(landingPage);
                _repository.Commit();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }
    }
}
