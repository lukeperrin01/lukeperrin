﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ItOps.Commands.Email;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using ItOps.Commands;
using LeisureInsure.DB.Models.Identity;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Charges;
using LeisureInsure.ViewModels;
using NServiceBus;
using LeisureInsure.DB;
using Quote = LeisureInsure.Services.Dal.LeisureInsure.Quotes.Quote;
using Policy = LeisureInsure.Services.Dal.LeisureInsure.Information.Policy;
using Newtonsoft.Json;
using System;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB.ViewModels;

namespace LeisureInsure.ApiControllers
{
    //[Authorize]
    public class BrokerPortalController : ApiController
    {
        private readonly ApplicationUserManager _userManager;
        private readonly IEndpointInstance _bus;
        private readonly ILiRepository _repository;
        private readonly IRepository _rep;

        private readonly ICalculateQuoteTotals _totals;
        private IInterfaceWithTam _tam;

        public BrokerPortalController(
            ApplicationUserManager userManager,
            IEndpointInstance bus,
            ILiRepository repository,
            IRepository rep,
            ICalculateQuoteTotals totals,
            IInterfaceWithTam tam)
        {
            _userManager = userManager;
            _bus = bus;
            _repository = repository;
            _rep = rep;
            _totals = totals;
            _tam = tam;
        }

        [Route("api/v1/brokerPortal/getQuote/{quotePK}")]
        [HttpGet]
        public IHttpActionResult getBrokerQuote(int quotePK)
        {
            try
            {

                var quotes = _repository.Q<Quote>(x => x.ChargeSummaryItems,
                        x => x.Documents, x => x.Discounts,
                        x => x.Answers).Where(x => x.Id == quotePK); // Where(x => x.Purchased > 0)

                //the payload must be a list of actual types not IQueryable
                return Ok(quotes.FirstOrDefault());
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "get:api/v1/brokerPortal/getQuote/", quotePK.ToString());
                return BadRequest(errorid);
            }

        }

        [Route("api/v1/brokerPortal/{brokerCode}")]
        [HttpGet]
        public BrokerInformation GetAgentDetails(string brokerCode)
        {
            try
            {
                BrokerInformation agent = _tam.GetAgent(brokerCode);
                Contacts c = _rep.GetContactByBrokerCode(brokerCode);
                if (c != null)
                {
                    agent.Firstname = c.FirstName;
                    agent.Lastname = c.LastName;
                    var a = _rep.GetAddressByContactPK(c.ContactPK);
                    if (a != null)
                    {
                        agent.County = a.County;
                    }
                }
                return agent;
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, $"api/v1/brokerportal/{brokerCode}");
                //return BadRequest(errorid);
                return null;
            }

        }

        [Route("api/v1/brokerPortal/GetBrokerQuotes")]
        [HttpGet]
        public IHttpActionResult GetBrokerQuotes(string broker, string quoteRef = "")
        {
            try
            {

                var quotes = _rep.getBrokerQuotes(broker).OrderByDescending(x => x.DateCreated).ToList();

                if (string.IsNullOrEmpty(quoteRef))
                {
                    if (quotes.Count() > 100)
                    {
                        quotes = quotes.OrderByDescending(x => x.DateCreated).ToList();
                        quotes = quotes.Take(100).ToList();
                    }
                }
                else
                {
                    quotes = quotes.Where(x => x.QuoteRef == quoteRef).ToList();
                }

                return Ok(quotes);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "api/v1/brokerPortal/GetBrokerQuotes");
                return BadRequest(errorid);
            }
        }



    }
}