﻿using System;
using System.Web;
using System.Web.Http;
using Microsoft.Azure;
using LeisureInsure.DB;
using LeisureInsure.DB.ViewModels;
using System.Linq;
using System.Collections.Generic;
using Stripe;
using LeisureInsure.Utilities;
using NServiceBus;
using ItOps.Commands;

namespace LeisureInsure.ApiControllers
{
    public class StripeController : ApiController
    {

        private readonly IEndpointInstance _endpoint;

        public StripeController(IEndpointInstance endpoint)
        {
            _endpoint = endpoint;
        }

        [HttpGet]
        [Route("api/v1/getstripedata")]
        public IHttpActionResult GetStripeData()
        {
            try
            {
                var stripeData = new StripeData();
                var publishedKey = CloudConfigurationManager.GetSetting("stripePublishedKey");
                stripeData.PublishedKey = publishedKey;

                return Ok(stripeData);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("api/v1/agentpayment")]
        public IHttpActionResult PostAgentPayment([FromBody] PaymentData data)
        {
            try
            {
                var quote = new Quotes();
                var policy = new Products();
                var addy = new Addresses();
                var customer = new Contacts();
                var agent = new Contacts();
                var onBehalfOf = "";               
                var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == data.QuoteRef && x.PWord == data.QuotePass);
                    agent = repo.Contacts.FirstOrDefault(x => x.ContactPK == quote.BrokerFK);

                    if (quote.Purchased == 1)
                        throw new Exception("This policy has already been purchased, please contact Leisureinsure");
                    
                    if (String.IsNullOrEmpty(User.Identity.Name))
                    {
                        throw new Exception("Agent not logged in");
                    }

                    if (agent.Locked == true)
                    {
                        throw new Exception("This account is currently suspended. Please contact the office for further details: 01993 700761 or info@leisureinsure.co.uk");
                    }

                    var dates = QuoteDateHelper.GetQuoteDates(data.PolicyDates, (int)quote.PolicyFK, quote.PolicyType);

                    quote.DateFrom = dates.startDate;
                    quote.DateTo = dates.endDate;
                    quote.TimeFrom = dates.startTime;
                    quote.TimeTo = dates.endTime;

                    quote.Purchased = 1;
                    quote.DatePurchased = dateNow;

                    repo.SaveChanges();

                    policy = repo.Products.FirstOrDefault(x => x.Id == quote.PolicyFK);
                    customer = repo.Contacts.FirstOrDefault(x => x.ContactPK == quote.CustomerFK);
                    
                    onBehalfOf = $"{agent.FirstName} {agent.LastName}";

                    //kickstart saga for agent
                    _endpoint.Send<CreatePolicyForAgent>(m =>
                    {
                        m.PolicyReference = quote.QuoteRef;
                        m.PayByInstallments = false;
                        m.AgentCode = agent.TamClientRef;
                    });

                }

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpPost]
        [Route("api/v1/stripepayment")]
        public IHttpActionResult PostStripePayment([FromBody] PaymentData data)
        {
            try
            {
                var quote = new Quotes();
                var policy = new Products();
                var addy = new Addresses();
                var customer = new Contacts();
                var instalment = false;
                var total = 0;
                var response = "";
                var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == data.QuoteRef && x.PWord == data.QuotePass);

                    if (quote.Purchased == 1)
                        throw new Exception("This policy has already been purchased, please contact Leisureinsure");

                    var dates = QuoteDateHelper.GetQuoteDates(data.PolicyDates, (int)quote.PolicyFK, quote.PolicyType);

                    quote.DateFrom = dates.startDate;
                    quote.DateTo = dates.endDate;
                    quote.TimeFrom = dates.startTime;
                    quote.TimeTo = dates.endTime;

                    policy = repo.Products.FirstOrDefault(x => x.Id == quote.PolicyFK);
                    customer = repo.Contacts.FirstOrDefault(x => x.ContactPK == quote.CustomerFK);

                    //update quote for instalment
                    if (data.InstalmentData.TotalToPay > 0)
                    {
                        //update quote
                        quote.PayOption = 5;
                        instalment = true;

                        //add instalment record
                        var instalmentRec = new Instalments();
                        instalmentRec.QuoteRef = quote.QuotePK;
                        instalmentRec.CustomerID = customer.ContactPK;
                        instalmentRec.AccountName = data.InstalmentData.Name;
                        instalmentRec.AccountNumber = data.InstalmentData.AccountNumber;
                        instalmentRec.SortCode = data.InstalmentData.SortCode;
                        instalmentRec.Salutation = data.InstalmentData.Title;
                        instalmentRec.LastName = customer.LastName;
                        instalmentRec.FirstName = customer.FirstName;
                        instalmentRec.FullPremium = data.InstalmentData.TotalToPay;
                        instalmentRec.Deposit = data.InstalmentData.Deposit;
                        instalmentRec.FlatRate = data.InstalmentData.EachPayment;
                        instalmentRec.Lock = 0;
                        instalmentRec.InstalmentMonth = "14";
                        instalmentRec.NumberOfPayments = data.InstalmentData.NumberOfPayments;
                        instalmentRec.Timestamp = dateNow;

                        repo.Instalments.Add(instalmentRec);
                    }

                    repo.SaveChanges();

                    //stripe charges in cents/pence
                    if (!instalment)
                        total = (int)(quote.Total * 100);
                    else
                        total = (int)(data.InstalmentData.Deposit * 100);

                    var currency = (data.CurrencyId == "£") ? "GBP" : "EUR";

                    var stripeinfo = new Dictionary<String, String>();

                    stripeinfo.Add("QuoteRef", quote.QuoteRef);
                    stripeinfo.Add("QuotePassword", quote.PWord);

                    //customer id,onbehalfOf must be set up in stripe dashboard
                    var chargeOptions = new StripeChargeCreateOptions()
                    {
                        //required
                        Amount = total,
                        Currency = currency,
                        SourceTokenOrExistingSourceId = data.StripeToken,
                        //optional
                        Description = $"{policy.ProductName} Insurance",
                        ReceiptEmail = customer.Email,
                        Metadata = stripeinfo
                    };

                    //create stripe charge 
                    var chargeService = new StripeChargeService();
                    var stripeCharge = chargeService.Create(chargeOptions);

                    if (stripeCharge.Outcome.Type == "authorized")
                    {
                        response = stripeCharge.Outcome.Type;

                        //update quote, kickstart nservicebus to send email..
                        quote.Purchased = 1;
                        quote.DatePurchased = dateNow;
                        repo.SaveChanges();

                        //kickstart saga for customer
                        _endpoint.Send<PaymentConfirmed>(m =>
                        {
                            m.QuoteReference = quote.QuoteRef;
                        });
                    }
                    else
                    {
                        //notify user was not accepted
                        return BadRequest("Something went wrong authorising payment, please contact Leisureinsure");
                    }
                }

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }

    public class StripeData
    {
        public string PublishedKey { get; set; }
    }

    public class PaymentData
    {
        public string StripeToken { get; set; }
        public string QuoteRef { get; set; }
        public string QuotePass { get; set; }
        public string CurrencyId { get; set; }
        public InstalmentView InstalmentData { get; set; }
        public DatesView PolicyDates { get; set; }
    }
}
