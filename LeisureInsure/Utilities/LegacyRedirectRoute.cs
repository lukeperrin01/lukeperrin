﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Routing;
using ClosedXML.Excel;

namespace LeisureInsure.Utilities
{
    public class LegacyRedirectRoute : RouteBase
    {
        private readonly Lazy<Dictionary<string, string>> _lazyRedirects = new Lazy<Dictionary<string, string>>(() =>
        {
            var path = Path.Combine(MvcApplication.BasePath, "Redirects.xlsx");
            using (var file = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var workbook = new XLWorkbook(file);
                var sheet = workbook.Worksheets.FirstOrDefault();

                var cache = new Dictionary<string, string>();
                var row = 2;

                var from = sheet.Cell($"A{row}").Value.ToString();
                var to = sheet.Cell($"B{row}").Value.ToString();

                while (!string.IsNullOrEmpty(from))
                {
                    if (!cache.ContainsKey(from))
                    {
                        cache.Add(from, to);
                    }
                    row++;
                    from = sheet.Cell($"A{row}").Value.ToString();
                    to = sheet.Cell($"B{row}").Value.ToString();
                }

                return cache;
            }
        }, LazyThreadSafetyMode.ExecutionAndPublication);

        private Dictionary<string, string> Redirects => _lazyRedirects.Value;

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            var request = httpContext.Request;
            var response = httpContext.Response;
            var legacyUrl = request.Url.AbsolutePath.Trim('/');

            if (Redirects.ContainsKey(legacyUrl))
            {
                response.RedirectPermanent($@"{request.Url.Scheme}://{request.Url.Host}:{request.Url.Port}/{Redirects[legacyUrl]}" , true);
            }
            return null;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            return null;
        }
    }
}