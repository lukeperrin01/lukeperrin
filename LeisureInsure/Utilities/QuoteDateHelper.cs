﻿using System;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.Helpers;

namespace LeisureInsure.Utilities
{
    public static class QuoteDateHelper
    { 
        public static vmDateCheck GetQuoteDates(DatesView dates,int policyid,string policyType)
        {
           
            var dt = new vmDateCheck();
            //ensure dates times are checked for client
            var startDate = DateTime.Parse(dates.DateFrom);
            var endDate = startDate;
            var timeFrom = $"{dates.StartingHour}:{dates.StartingMinute}";
            if (!String.IsNullOrEmpty(dates.DateTo))
                endDate = DateTime.Parse(dates.DateTo);
            var timeTo = $"{dates.EndingHour}:{dates.EndingMinute}";

            dt.startDate = startDate;
            dt.startTime = timeFrom;
            dt.endDate = endDate;
            dt.endTime = timeTo;
            dt.PolicyPK = policyid;
            dt.PolicyType = policyType;
            dt.endTimeRequired = dates.EndTimeRequired;
            dt.startTimeRequired = dates.StartTimeRequired;
            dt.dateToRequired = dates.DateToRequired;
            dt.numberOfDays = dates.CoverDays;

            var dateHelper = new DateTimeHelper();
            var dateResult = dateHelper.DateChecker(dt);

            return dateResult;

        }
    }
}
