﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.Utilities
{
    public static class ExtensionsMethods
    {
        public static int? IntToZero(this int? x)
        {
            if (x == null)
                return 0;
            else
                return x;
        }
    }
}
