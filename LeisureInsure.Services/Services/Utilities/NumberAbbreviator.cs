﻿using System;
using LeisureInsure.Services.Services.Utilities.Interfaces;

namespace LeisureInsure.Services.Services.Utilities
{
    public class NumberAbbreviator : IMakeNumbersFriendly
    {
        public string Transform(decimal number)
        {
            if (number >= 1000000)
            {
                return $"{Math.Round(number / 1000000, 2):#.#}M";
            }
            
            return number.ToString("N0");
        }

        public string Transform(string number)
        {
            decimal decimalNumber;
            if (!decimal.TryParse(number, out decimalNumber))
            {
                return "Free!";
            }
            return Transform(decimalNumber);
        }
    }
}
