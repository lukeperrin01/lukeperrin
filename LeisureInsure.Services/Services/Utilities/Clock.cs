﻿using System;

namespace LeisureInsure.Services.Services.Utilities
{
    public class Clock : IClock
    {
        private readonly TimeZoneInfo _gmtTimeZone =  TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");

        public DateTime UtcNow => DateTime.UtcNow;

        public DateTime LocalTime => TimeZoneInfo.ConvertTimeFromUtc(UtcNow, _gmtTimeZone);
    }
}