﻿using System;

namespace LeisureInsure.Services.Services.Utilities
{
    public interface IClock
    {
        DateTime UtcNow { get; } 
        DateTime LocalTime { get; }
    }
}
