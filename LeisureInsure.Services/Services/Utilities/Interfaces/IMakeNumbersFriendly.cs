﻿namespace LeisureInsure.Services.Services.Utilities.Interfaces
{
    public interface IMakeNumbersFriendly
    {
        string Transform(decimal number);
        string Transform(string number);
    }
}
