﻿using System.Linq;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Services.Services.Locales
{
    //TODO: Caching candidate
    public class LocaleService : IGetLocaleInformation
    {
        private readonly ILiRepository _repository;

        public LocaleService(ILiRepository repository)
        {
            _repository = repository;
        }

        public decimal Multiplier(int localeId, int policyId, int coverId)
        {
            var record = _repository.Q<LocaleMultiplier>()
                    .FirstOrDefault(x => x.LocaleId == localeId && x.PolicyId == policyId && x.CoverId == coverId);
            if (record == null)
            {
                return 1;
            }
            return record.Multiplier;
        }

        public string Symbol(int localeId)
        {
            switch (localeId)
            {
                case 1:
                    return "£";
                case 2:
                    return "€";
            }
            return null;
        }
    }
}