﻿namespace LeisureInsure.Services.Services.Locales
{
    public interface IGetLocaleInformation
    {
        decimal Multiplier(int localeId, int policyId, int coverId);
        string Symbol(int localeId);
    }
}
