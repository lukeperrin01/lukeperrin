﻿using System.Configuration;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Services.Services.Utilities;
using LeisureInsure.Services.Utilities;

namespace LeisureInsure.Services.Services.Payment
{
    public class GlobalIrisFieldAdder : IAddGlobalIrisInformation
    {
        private readonly IClock _clock;
        private readonly IGenerateGlobalIrisHashes _generate;

        public GlobalIrisFieldAdder(IClock clock, 
            IGenerateGlobalIrisHashes generate)
        {
            _clock = clock;
            _generate = generate;
        }

        public void AddInformation(Quote quote)
        {
            if (quote.PaymentOptions == null)
            {
                return;
            }
            var useTest = ConfigurationManager.AppSettings["global-iris.use-test"].ToSafeInt();
            string url;
            if (useTest == 0) // The default
            {
                url = "https://hpp.globaliris.com/pay";
            }
            else
            {
                url = "https://hpp.sandbox.globaliris.com/pay";  
            }

            foreach (var paymentOption in quote.PaymentOptions)
            {
                var globalIrisFields = new GlobalIrisFields
                {
                    MerchantId = ConfigurationManager.AppSettings["global-iris.merchant-id"],
                    AutoSettleFlag = ConfigurationManager.AppSettings["global-iris.auto-settle-flag"],
                    Url = url,
                    QuoteReference = quote.QuoteReference,
                    ResponseUrl = ConfigurationManager.AppSettings["global-iris.responseUrl"],
                };

                globalIrisFields.OrderId = quote.QuoteReference + "-" + quote.OrderNumber.ToString("D4");
                globalIrisFields.Amount = (paymentOption.InitialPayment * 100).ToString("F0");
                globalIrisFields.Currency = Currency(quote.LocaleId);
                globalIrisFields.Timestamp = _clock.UtcNow.ToString("yyyyMMddhhmmss");

                globalIrisFields.Hash = _generate.Hash(
                    globalIrisFields.Timestamp, 
                    globalIrisFields.MerchantId,
                    globalIrisFields.OrderId, 
                    globalIrisFields.Amount, 
                    globalIrisFields.Currency);

                if (quote.LocaleId == 2)
                {
                    globalIrisFields.Account = "leisureEUR";
                }
                else
                {
                    if (paymentOption.Id == 1)
                    {
                        globalIrisFields.Account = "leisureGBP";
                    }
                    else
                    {
                        globalIrisFields.Account = "foxGBP";
                    }
                }

                paymentOption.GlobalIrisFields = globalIrisFields;
            }
        }

        private string Currency(int localeId)
        {
            switch (localeId)
            {
                case 1:
                    return "GBP";
                case 2:
                    return "EUR";
            }
            return "";
        }
    }
}