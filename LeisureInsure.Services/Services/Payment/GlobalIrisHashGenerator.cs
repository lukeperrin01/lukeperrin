﻿using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace LeisureInsure.Services.Services.Payment
{
    public class GlobalIrisHashGenerator : IGenerateGlobalIrisHashes
    {
        public string Hash(params string[] inputs)
        {
            var inputString = new StringBuilder();
            foreach (var input in inputs)
            {
                inputString.Append(input);
                inputString.Append('.');
            }
            var stringToHash = inputString.ToString(0, inputString.Length - 1);

            using (var sha1 = new SHA1Managed())
            {
                var firstHash = sha1.ComputeHash(Encoding.UTF8.GetBytes(stringToHash));
                var lowerCaseFirstHash = ConvertFromByteArray(firstHash).ToLowerInvariant();
                var secondStringToHash = lowerCaseFirstHash + "." + ConfigurationManager.AppSettings["global-iris.secret"];
                var secondHash = sha1.ComputeHash(Encoding.UTF8.GetBytes(secondStringToHash));
                return ConvertFromByteArray(secondHash).ToLowerInvariant();
            }
        }

        private string ConvertFromByteArray(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (var b in bytes)
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
