﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Services.Services.Payment
{
    public class Instalments : IAmAPaymentOption
    {
        public void AddIfAllowed(Quote quote)
        {

            if (quote.Total < 100 || quote.LocaleId == 2 || quote.PolicyType == "E" || quote.Total > 100000)
            {
                return;
            }
            var interest = 0m;
            if (quote.Total < 1000)
            {
                interest = 0.18m;
            }
            else if (quote.Total < 2000)
            {
                interest = 0.125m;
            }
            else if (quote.Total < 3000)
            {
                interest = 0.1m;
            }
            else
            {
                interest = 0.085m;
            }

            var totalPayable = quote.Total + (quote.Total * interest);
            var singleInstallment = totalPayable / 10;

            quote.PaymentOptions.Add(new PaymentOption
            {
                Id = 5,
                InitialPayment = singleInstallment * 2,
                NumberOfPayments = 8,
                Name = "Pay with 8 monthly installments (which is subject to approval by the Finance Company)",
                SubsequentPayments = singleInstallment,
            });

        }
    }
}

