﻿namespace LeisureInsure.Services.Services.Payment
{
    public class GlobalIrisValidator : IValidateGlobalIrisResponse
    {
        private readonly IGenerateGlobalIrisHashes _generate;

        public GlobalIrisValidator(IGenerateGlobalIrisHashes generate)
        {
            _generate = generate;
        }

        public bool Validate(string timestamp, string merchantId, string orderId, string result, string message, string passref,
            string authcode, string hash)
        {
            var generatedHash = _generate.Hash(timestamp, merchantId, orderId, result, message, passref, authcode);
            return (generatedHash.ToLowerInvariant() == hash);
        }
    }
}
