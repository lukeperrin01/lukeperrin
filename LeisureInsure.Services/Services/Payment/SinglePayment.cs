﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Services.Services.Payment
{
    public class SinglePayment : IAmAPaymentOption
    {
        public void AddIfAllowed(Quote quote)
        {
            // Always allowed
            quote.PaymentOptions.Add(new PaymentOption
            {
                Id = 1,
                InitialPayment = quote.Total,
                Name = "Pay with a single payment",
                NumberOfPayments = 1,
                SubsequentPayments = 0
            });
        }
    }
}