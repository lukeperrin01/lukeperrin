﻿namespace LeisureInsure.Services.Services.Payment
{
    public interface IValidateGlobalIrisResponse
    {
        bool Validate(string timestamp, string merchantId, string orderId, string result, string message, string passref,
            string authcode, string hash);
    }
}

