﻿namespace LeisureInsure.Services.Services.Payment
{
    public interface IGenerateGlobalIrisHashes
    {
        string Hash(params string[] inputs);
    }
}
