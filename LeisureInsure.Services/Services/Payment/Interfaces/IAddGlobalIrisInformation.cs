﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Services.Services.Payment
{
    public interface IAddGlobalIrisInformation
    {
        void AddInformation(Quote quote);
    }
}
