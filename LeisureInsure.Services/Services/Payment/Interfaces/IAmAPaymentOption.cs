﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Services.Services.Payment
{
    public interface IAmAPaymentOption
    {
        void AddIfAllowed(Quote quote);
    }
}
