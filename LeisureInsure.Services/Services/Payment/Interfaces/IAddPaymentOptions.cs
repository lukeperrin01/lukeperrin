﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Services.Services.Payment
{
    public interface IAddPaymentOptions
    {        
        void AddPaymentOptionsTo(Quote quote);
    }
}
