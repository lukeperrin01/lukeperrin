﻿using System.Collections.Concurrent;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Services.Services.Information
{
    public class CoverInformation : IGetCoverInformation
    {
        private readonly ILiRepository _repository;
        private static readonly ConcurrentDictionary<int, long> BitwiseIds = new ConcurrentDictionary<int, long>(); 

        public CoverInformation(ILiRepository repository)
        {
            _repository = repository;
        }

        public string Description(int coverId)
        {
            var cover = _repository.Find<Cover>(coverId);
            if (cover == null)
            {
                return "COVER NOT FOUND";
            }
            var description = cover.Name;
            if (!string.IsNullOrWhiteSpace(cover.Description))
            {
                description += " - " + cover.Description;
            }
            return description;
        }

        public string SectionDescription(int coverId)
        {
            var cover = _repository.Find<Cover>(coverId);
            if (cover == null)
            {
                return "COVER NOT FOUND";
            }
            var description = cover.Name;            
            return description;
        }

        public string SectionTypeDescription(int coverId)
        {
            var cover = _repository.Find<Cover>(coverId);
            if (cover == null)
            {
                return "COVER NOT FOUND";
            }
            var description = cover.Description;
            return description;
        }

        public long BitwiseId(int coverId)
        {
            long bitwiseId;
            if (BitwiseIds.TryGetValue(coverId, out bitwiseId))
            {
                return bitwiseId;
            }
            bitwiseId = _repository.Find<Cover>(coverId)?.BitwiseId ?? 0;
            if (bitwiseId > 0)
            {
                BitwiseIds.TryAdd(coverId, bitwiseId);
            }
            return bitwiseId;
        }
    }
}