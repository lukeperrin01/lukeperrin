﻿using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Information;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Services.Information.Interfaces;

namespace LeisureInsure.Services.Services.Information
{
    public class TamOccupationCalculator : IGetTamOccupation
    {
        private readonly ILiRepository _repository;

        public TamOccupationCalculator(ILiRepository repository)
        {
            _repository = repository;
        }

        public string Occupation(int policyId, int occupationRateId)
        {
            if (policyId != 19)
            {
                var policy = _repository.Find<Policy>(policyId);
                return policy.TamOccupation;
            }
            var rate = _repository.Find<Rate>(occupationRateId);
            return rate.TamOccupation;
        }
    }
}
