﻿using System.Collections.Concurrent;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Information;

namespace LeisureInsure.Services.Services.Information
{
    public class PolicyInformation : IGetPolicyInformation
    {
        private readonly ILiRepository _repository;
        private static readonly ConcurrentDictionary<int, int> BitwiseIds = new ConcurrentDictionary<int, int>();

        public PolicyInformation(ILiRepository repository)
        {
            _repository = repository;
        }

        public int BitwiseId(int policyId)
        {
            int bitwiseId;
            if (BitwiseIds.TryGetValue(policyId, out bitwiseId))
            {
                return bitwiseId;
            }
            bitwiseId = _repository.Find<Policy>(policyId)?.BitwiseId ?? 0;
            if (bitwiseId > 0)
            {
                BitwiseIds.TryAdd(policyId, bitwiseId);
            }
            return bitwiseId;
        }
    }
}