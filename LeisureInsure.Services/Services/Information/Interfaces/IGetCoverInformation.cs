﻿namespace LeisureInsure.Services.Services.Information
{
    public interface IGetCoverInformation
    {
        string Description(int coverId);

        string SectionDescription(int coverId);

        string SectionTypeDescription(int coverId);

        long BitwiseId(int coverId);
    }
}
