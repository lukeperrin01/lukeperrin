﻿namespace LeisureInsure.Services.Services.Information
{
    public interface IGetPolicyInformation
    {
        int BitwiseId(int policyId);
    }
}
