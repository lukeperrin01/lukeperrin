﻿namespace LeisureInsure.Services.Services.Information.Interfaces
{
    public interface IGetTamOccupation
    {
        string Occupation(int policyId, int occupationRateId);
    }
}
