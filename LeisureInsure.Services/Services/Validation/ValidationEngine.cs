﻿using System.Collections.Generic;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Services.Models.Validation;

namespace LeisureInsure.Services.Services.Validation
{
    public class ValidationEngine : IValidateQuotes
    {
        private readonly IEnumerable<IAmAValidator> _validators;

        public ValidationEngine(IEnumerable<IAmAValidator> validators)
        {
            _validators = validators;
        }
        
        public GroupedValidationResult Validate(WebsiteChargeModel quote)
        {
            var result = new GroupedValidationResult
            {
                Valid = true,
            };
            var errors = new List<ValidationError>();

            foreach (var validator in _validators)
            {
                var error = validator.Validate(quote);
                if (error != null)
                {
                    result.Valid = false;
                    errors.Add(error);
                }
            }
            result.ValidationErrors = errors;
            return result;
        }
    }
}
