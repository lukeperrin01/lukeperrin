﻿using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Services.Models.Validation;

namespace LeisureInsure.Services.Services.Validation
{
    /// <summary>
    /// Goes through all validators and returns a list of validation errors produced
    /// </summary>
    public interface IValidateQuotes
    {
        GroupedValidationResult Validate(WebsiteChargeModel quote);
    }
}
