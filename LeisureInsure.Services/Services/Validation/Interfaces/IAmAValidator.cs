﻿using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Services.Models.Validation;

namespace LeisureInsure.Services.Services.Validation
{
    /// <summary>
    /// Validates a specific validation issue
    /// </summary>
    public interface IAmAValidator
    {
        ValidationError Validate(WebsiteChargeModel quote);
    }
}