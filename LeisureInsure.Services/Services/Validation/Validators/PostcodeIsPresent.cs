﻿using System.Linq;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Services.Models.Validation;

namespace LeisureInsure.Services.Services.Validation.Validators
{
    public class PostcodeIsPresent : IAmAValidator
    {
        public ValidationError Validate(WebsiteChargeModel quote)
        {
            if (quote.LocaleId == 1)
            {
                // Ensure there is a postcode
                var date = quote.Inputs.FirstOrDefault(x => x.RateTypeFK == 1);
                if (string.IsNullOrEmpty(date?.RateValue))
                {
                    return new ValidationError
                    {
                        InputId = Inputs.Postcode,
                        Message = "Please enter a postcode"
                    };
                }
            }
            return null;
        }
    }
}
