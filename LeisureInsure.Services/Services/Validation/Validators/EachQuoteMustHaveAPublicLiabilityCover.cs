﻿using System.Linq;
using LeisureInsure.Services.Models;
using LeisureInsure.Services.Models.Quotes;
using LeisureInsure.Services.Models.Validation;

namespace LeisureInsure.Services.Services.Validation.Validators
{
    public class EachQuoteMustHaveAPublicLiabilityCover : IAmAValidator
    {
        public ValidationError Validate(WebsiteChargeModel quote)
        {

            if (quote.Inputs.Any(x => x.CoverPk == CoverId.PublicLiability))
            {
                return null;
            }

            //brand protection does not have PL
            if (quote.PolicyId == 27)
            {
                return null;
            }

            return new ValidationError
            {
                Message = "Please ensure that you complete the public liability section of the quote form"
            };
        }
    }
}
