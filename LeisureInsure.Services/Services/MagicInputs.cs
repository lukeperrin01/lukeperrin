﻿namespace LeisureInsure.Services.Services
{
    public static class Inputs
    {
        public const int Postcode = 1;
        public const int InitiationDate = 2;
        public const int EndDate = 3;
        public const int Fee = 20;
        public const int Tax = 21;

        public const int Wageroll = 47;
        public const int MaterialDamageValue = 28;
        public const int MaterialDamageItemValue = 51;
        public const int AnnualTurnover = 55;

        public const int TerritoryToCover = 60;
        public const int EstimatedGrossProfit = 63;
        public const int IndemnityPeriod = 64;
        public const int IncreasedCostOfWorking = 65;
        public const int IndemnityPeriodIncreasedCostOfWorking = 66;
        public const int ProfessionalLiability = 77;

        public const int HazardQuestions = 40;
        public const int WorldWideCoverLoading = 42;

        public const int NumberOfVolunteers = 43;
        public const int NumberOfInstructors = 50;
        public const int NumberOfItems = 16;

        public const int LevelOfIndemnity = 11;
        public const int PublicLiabilityLevelOfIndemnityByHazard = 78;

        public const int EventType = 29;
        public const int ItemList = 14;
    }
}