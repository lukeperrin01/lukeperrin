﻿using System;
using JetBrains.Annotations;
using DiagnosticContracts = System.Diagnostics.Contracts.Contract;

namespace LeisureInsure.Services.Utilities
{
    /// <summary>
    /// See; http://enterprisecraftsmanship.com/2015/03/20/functional-c-handling-failures-input-errors/
    /// </summary>
    public class Result
    {
        public bool Success { get; }
        public string Error { get; private set; }

        public bool Failure => !Success;

        protected Result(bool success, string error)
        {
            DiagnosticContracts.Requires(success || !string.IsNullOrEmpty(error));
            DiagnosticContracts.Requires(!success || string.IsNullOrEmpty(error));

            Success = success;
            Error = error;
        }

        public static Result Fail(string message, params object[] stringFormatArgs)
        {
            message = string.Format(message, stringFormatArgs);
            return new Result(false, message);
        }

        public static Result<T> Fail<T>(string message)
        {
            return new Result<T>(default(T), false, message);
        }

        public static Result Ok()
        {
            return new Result(true, String.Empty);
        }

        public static Result<T> Ok<T>(T value)
        {
            return new Result<T>(value, true, String.Empty);
        }

        public static Result Combine(params Result[] results)
        {
            foreach (Result result in results)
            {
                if (result.Failure)
                    return result;
            }

            return Ok();
        }
    }


    public class Result<T> : Result
    {
        private T _value;

        public T Value
        {
            get
            {
                DiagnosticContracts.Requires(Success);

                return _value;
            }
            [param: CanBeNull]
            private set { _value = value; }
        }

        protected internal Result([CanBeNull] T value, bool success, string error)
            : base(success, error)
        {
            DiagnosticContracts.Requires(value != null || !success);

            Value = value;
        }
    }
}
