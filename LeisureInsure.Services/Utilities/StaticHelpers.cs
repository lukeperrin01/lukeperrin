﻿/// <summary>
/// Static helpers
/// </summary>
namespace LeisureInsure.Services.Utilities
{
    public static class LocaleHelper
    {
        public static string GetLocale(int localeid)
        {
            if (localeid == 1)
                return "United Kingdom";
            else
                return "Eire";
        }

        public static string GetCurrency(int localeid)
        {
            if (localeid == 1)
                return "£";
            else
                return "€";
        }
    }

    public static class RateType
    {
        public const int PostCode = 1;
        public const int TradeMembership = 4;
        public const int Indemnity = 5;
        public const int SumInsured = 6;
        public const int Excess = 7;
        public const int Premium = 10;
        public const int MinCharge = 13;
        public const int Fee = 19;
        public const int HazardRating = 16;
        public const int FoodCovers = 34;
        public const int ReferableQuestion = 36;
        public const int IndemnityQuestion = 37;
        public const int DurationOfCover = 38;
        public const int TurnoverTable = 48;
    }

    public static class CoverType
    {
        public const int MDBusinessEquipment = 8;
        public const int Cancellation = 30;
        public const int Professional = 20;
        public const int BrandProtection = 42;
        public const int PublicLiability = 2;
        public const int EmployersLiability = 3;
        public const int Product = 18;
    }

    public static class PolicyType
    {
        public const int EquipmentHirer = 1;
        public const int Events = 6;
        public const int FieldSportsEvent = 8;
        public const int FoodContamination = 27;
        public const int FreelanceSports = 16;
        public const int FreelanceActivity = 15;
        public const int Exhibitors = 18;
        public const int MobileCatering = 26;
        public const int ConfandMeetings = 3;
    }
}
