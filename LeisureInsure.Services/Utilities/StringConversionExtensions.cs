﻿using System;

namespace LeisureInsure.Services.Utilities
{
    public static class StringConversionExtensions
    {
        public static decimal ToSafeDecimal(this string source, decimal defaultValue = 0m)
        {
            var value = defaultValue;
            try
            {
                value = decimal.Parse(source);
            }
            catch (FormatException) {}
            catch (ArgumentNullException) {}
            return value;
        }

        public static int ToSafeInt(this string source, int defaultValue = 0)
        {
            var value = defaultValue;
            try
            {
                value = int.Parse(source);
            }
            catch (FormatException) {}
            catch (ArgumentNullException) {}
            return value;
        }

        public static string Truncate(this string text, int maxLength)
        {
            if (string.IsNullOrEmpty(text) || maxLength <= 0)
            {
                return string.Empty;
            }

            if (maxLength < text.Length)
            {
                return text.Substring(0, maxLength);
            }

            return text;
        }
    }
}