﻿namespace LeisureInsure.Services.Models
{
    public static class CoverId
    {
        public const int PublicLiability = 2;
        public const int EmployersLiability = 3;
        public const int MaterialDamageBuildings = 4;
        public const int MaterialDamageBusinessEquipment = 8;
        public const int MaterialDamageTrophiesAndMemorabilia = 9;
        public const int BusinessInterruptionLossOfGrossProfit = 12;
        public const int PersonalAssault = 23;
        public const int BusinessInterruptionLossOfGrossRentals = 25;
        public const int MaterialDamageGeneric = 26;
        public const int BusinessInterruptionIncreasedCostOfWorking = 28;
        public const int Cancellation = 30;
        public const int BrandProtection = 42;
        public const int RehabilitationExpenses = 43;
        public const int RecallCosts = 44;
        public const int SuppContamination = 45;
        public const int WorkplaceViolence = 46;
        public const int PubHealthAuthorityAnnouncement = 47;
        public const int MalicousTampering = 49;
        public const int Extortion = 50;      
    }
}
