using System.Collections.Generic;

namespace LeisureInsure.Services.Models.Validation
{
    public class GroupedValidationResult
    {
        public bool Valid { get; set; }
        public IEnumerable<ValidationError> ValidationErrors { get; set; }
    }
}