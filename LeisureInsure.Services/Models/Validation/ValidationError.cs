namespace LeisureInsure.Services.Models.Validation
{
    public class ValidationError
    {
        public int InputId { get; set; }
        public string Message { get; set; }
    }
}