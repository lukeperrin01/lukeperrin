using System.Collections.Generic;

namespace LeisureInsure.Services.Models.Quotes
{
    public class ChargeCalculationAnswer
    {
        public int CoverId { get; set; }
        public int RateId { get; set; }

        public List<ChargeCalculationAnswer> Children { get; set; }

        public string RateValue { get; set; }
        public string InputString { get; set; }

        public decimal SumInsured { get; set; }

        public int NoItems { get; set; }

        public int RateTypeFK { get; set; }
    }
}