namespace LeisureInsure.Services.Models.Quotes
{
    public class PaymentOption
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal InitialPayment { get; set; }
        public decimal SubsequentPayments { get; set; }
        public int NumberOfPayments { get; set; }

        public GlobalIrisFields GlobalIrisFields { get; set; }
    }
}