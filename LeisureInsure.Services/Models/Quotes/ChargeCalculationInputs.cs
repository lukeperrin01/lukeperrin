using System.Collections.Generic;

namespace LeisureInsure.Services.Models.Quotes
{
    public class ChargeCalculationInputs
    {
        public PolicyInformation PolicyInformation { get; set; }
        public List<ChargeCalculationAnswer> Answers { get; set; }
    }
}