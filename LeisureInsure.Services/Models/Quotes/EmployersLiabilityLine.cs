namespace LeisureInsure.Services.Models.Quotes
{
    public class EmployersLiabilityLine
    {
        public decimal Wageroll { get; set; }
        public decimal Rate { get; set; }
        public bool RateIsAPercentage { get; set; }

        public int DivBy { get; set; }
    }
}