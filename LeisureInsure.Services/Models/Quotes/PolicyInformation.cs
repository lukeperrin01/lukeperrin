namespace LeisureInsure.Services.Models.Quotes
{
    public class PolicyInformation
    {

        public PolicyInformation()
        {
            MDExcessAdded = false;
        }
        public string quoteReference { get; set; }
        public int PolicyId { get; set; }
        public int LocaleId { get; set; }
        public decimal Turnover { get; set; }
        public bool MemberOfDiscountOrganisation { get; set; }
        public bool ShowFees { get; set; }
        public string BrokerTamName { get; set; }
        public decimal ExchangeRate { get; set; }
        public bool MDExcessAdded { get; set; }
    }
}