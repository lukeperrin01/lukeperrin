﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.Services.Models.Quotes
{
    public class vmRates
    {

        public int CoverPK { get; set; }

        public int ChildBW { get; set; }
        public int InputFK { get; set; }
        public int RatePK { get; set; }
        public string RateName { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Threshold { get; set; }
        public decimal? Excess { get; set; }
        public string RateLabel { get; set; }

        public int Refer { get; set; }
        public int? DivBy { get; set; }

        public int DateTill { get; set; }
        public int HazardRating { get; set; }

        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountSubsequentRate { get; set; }
        public int RateIsPercentage { get; set; }
        public int ThresholdIsPercentage { get; set; }
        public int RateType { get; set; }

        public int? InputTypeFK { get; set; }
        public string UnitType { get; set; }
        public int ParentInputPK { get; set; }
        public decimal? InputOrdinal { get; set; }

        public int? ParentRatePK { get; set; }
        public int NewParentBW { get; set; }
        public int NewChildBW { get; set; }
        public int? ListRatePK { get; set; }
        public int SumInsured { get; set; }
        public int NoItems { get; set; }
        public int DisplayBW { get; set; }
        public int ReturnBW { get; set; }

        public string lblSumInured { get; set; }
        public string lblNumber { get; set; }
        public int RateExclude { get; set; }
        public int RateTypeFK { get; set; }
        public int RateGroup { get; set; }
        public int CalcOrdinal { get; set; }

        public int CoverCalcFK { get; set; }

    }
}
