﻿namespace LeisureInsure.Services.Models.Quotes
{
    public enum ReferralType
    {
        None = 0,
        Referred = 1,
        Overridden = 2,
    }
}
