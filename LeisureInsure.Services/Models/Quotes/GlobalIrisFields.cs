﻿
namespace LeisureInsure.Services.Models.Quotes
{
    public class GlobalIrisFields
    {
        public string Url { get; set; }
        public string MerchantId { get; set; }
        public string OrderId { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public string Timestamp { get; set; }
        public string Hash { get; set; }
        public string AutoSettleFlag { get; set; }
        public string QuoteReference { get; set; }
        public string ResponseUrl { get; set; }
        public string Account { get; set; }
    }
}

