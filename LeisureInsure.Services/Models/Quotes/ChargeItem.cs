namespace LeisureInsure.Services.Models.Quotes
{
    public class ChargeItem
    {
        public decimal InitialCharge { get; set; }
        public decimal SubsequentCharge { get; set; }
        public int NumberOfItems { get; set; }
        public int QuoteInputPK { get; set; }
    }
}