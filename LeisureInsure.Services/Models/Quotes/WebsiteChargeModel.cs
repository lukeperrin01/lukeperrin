using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;

namespace LeisureInsure.Services.Models.Quotes
{
    public class WebsiteChargeModel
    {
        public int LocaleId { get; set; }
        public List<Answer> Inputs { get; set; }
        public int PolicyId { get; set; }
        public string QuoteReference { get; set; }
        public string Password { get; set; }
        public int LandingPageId { get; set; }
        public string BrowserTime { get; set; }
        public string Browser { get; set; }

    }
}