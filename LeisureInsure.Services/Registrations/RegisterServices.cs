﻿using System.Collections.Generic;
using LeisureInsure.Services.Services.Information;
using LeisureInsure.Services.Services.Information.Interfaces;
using LeisureInsure.Services.Services.Locales;
using LeisureInsure.Services.Services.Payment;
using LeisureInsure.Services.Services.Utilities;
using LeisureInsure.Services.Services.Utilities.Interfaces;
using LeisureInsure.Services.Services.Validation;
using LeisureInsure.Services.Services.Validation.Validators;
using Microsoft.Practices.Unity;

namespace LeisureInsure.Services.Registrations
{
    public class RegisterServices
    {
        public static void WithUnity(IUnityContainer container)
        {
            

            // Locales
            container.RegisterType<IGetLocaleInformation, LocaleService>(new PerResolveLifetimeManager());
           
            // Information
            container.RegisterType<IGetCoverInformation, CoverInformation>(new PerResolveLifetimeManager());
            container.RegisterType<IGetPolicyInformation, PolicyInformation>(new PerResolveLifetimeManager());
            container.RegisterType<IGetTamOccupation, TamOccupationCalculator>(new PerResolveLifetimeManager());
            
            
            // Payments            
            container.RegisterType<IAddGlobalIrisInformation, GlobalIrisFieldAdder>(new PerResolveLifetimeManager());
            container.RegisterType<IGenerateGlobalIrisHashes, GlobalIrisHashGenerator>(new PerResolveLifetimeManager());
            container.RegisterType<IValidateGlobalIrisResponse, GlobalIrisValidator>(new PerResolveLifetimeManager());

            // Discounts
            container.RegisterType<IAmAPaymentOption, Instalments>("Instalments", new PerResolveLifetimeManager());
            container.RegisterType<IAmAPaymentOption, SinglePayment>("SinglePayment", new PerResolveLifetimeManager());
            container.RegisterType<IEnumerable<IAmAPaymentOption>, IAmAPaymentOption[]>();

            // Utilities
            container.RegisterType<IClock, Clock>(new ContainerControlledLifetimeManager());
            container.RegisterType<IMakeNumbersFriendly, NumberAbbreviator>();

            // Validation
            container.RegisterType<IValidateQuotes, ValidationEngine>();
            container.RegisterType<IAmAValidator, EachQuoteMustHaveAPublicLiabilityCover>("eachQuoteMustHaveAPublicLiabilityCover");
            //container.RegisterType<IAmAValidator, StartDateIsPresent>("startDateIsPresent");
            container.RegisterType<IAmAValidator, PostcodeIsPresent>("postcodeIsPresent");

            container.RegisterType<IEnumerable<IAmAValidator>, IAmAValidator[]>();
        }
    }
}