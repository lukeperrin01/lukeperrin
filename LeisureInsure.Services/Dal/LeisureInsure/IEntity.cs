﻿namespace LeisureInsure.Services.Dal.LeisureInsure
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
