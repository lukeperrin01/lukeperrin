﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.Services.Dal.LeisureInsure
{
    public interface IEntityString
    {
        string Id { get; set; }
    }
}
