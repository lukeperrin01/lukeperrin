﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Information
{
    [Table("TrustPilots")]
    public class TrustPilotInformation : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int Stars { get; set; }
        public string Description { get; set; }
    }
}
