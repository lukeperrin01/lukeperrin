﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Information
{
    public class Policy : IEntity
    {
        [Key]
        [Column("PolicyPK")]
        public int Id { get; set; }

        [Column("Bitwise")]
        public int BitwiseId { get; set; }

        [Column("PolicyName")]
        public string Name { get; set; }

        public string TamOccupation { get; set; }
        public decimal BrokerCommission { get; set; }
        public string BusinessDescription { get; set; }
    }
}