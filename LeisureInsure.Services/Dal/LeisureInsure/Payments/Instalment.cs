﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Payments
{
    public class Instalment:IEntity
    {
        [Key]
        [Column("InstallmentPK")]
        public int Id { get; set; }
        [Column("QuoteRef")]
        public int QuoteId { get; set; }
        public int CustomerId { get; set; }
        public string AccountName { get; set; }
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        [Column("Salutation")]
        public string Title { get; set; }
        [Column("LastName")]
        public string Name { get; set; }
        public string BusinessName { get; set; }
        public decimal FullPremium { get; set; }
        public decimal Deposit { get; set; }
        [Column("FlatRate")]
        public decimal MonthlyInstallment { get; set; }
        public int NumberOfPayments { get; set; }
        [Column("InstalmentMonth")]
        public string CollectionDate { get; set; }
    }
}
