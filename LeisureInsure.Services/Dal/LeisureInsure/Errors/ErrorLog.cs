﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.Services.Dal.LeisureInsure.Errors
{
    public class ErrorLog : IEntity
    {
        [Column("ErrorLogPK")]
        public int Id { get; set; }
        [Column("QuoteReference")]
        public string QuoteReference { get; set; }

        [Column("URL")]
        public string URL { get; set; }

        [Column("Msg")]
        public string Msg { get; set; }

        [Column("StackTrace")]
        public string StackTrace { get; set; }

        [Column("Cause")]
        public string Cause { get; set; }

        [Column("DateStamp")]
        public DateTime DateStamp { get; set; }


    }
}
