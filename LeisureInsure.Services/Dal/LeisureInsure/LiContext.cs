﻿using System.Data.Entity;
using LeisureInsure.Services.Dal.LeisureInsure.Information;
using LeisureInsure.Services.Dal.LeisureInsure.LandingPages;
using LeisureInsure.Services.Dal.LeisureInsure.Payments;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes.Locales;
using LeisureInsure.Services.Dal.LeisureInsure.Errors;

namespace LeisureInsure.Services.Dal.LeisureInsure
{
    public class LiContext : DbContext
    {
        public LiContext() : base("DefaultConnection") { }

        // Register aggregate roots here

        private DbSet<LandingPage> LandingPages { get; set; }
        private DbSet<LocaleMultiplier> LocaleMultiplier { get; set; }
        private DbSet<Locales> Locales { get; set; }
        private DbSet<Rate> Rates { get; set; }
        private DbSet<Cover> Covers { get; set; }
        private DbSet<Policy> Policies { get; set; }
        private DbSet<Input> Inputs { get; set; }
        private DbSet<Quote> Quotes { get; set; }
        private DbSet<QuoteDetail> QuoteDetails { get; set; }
        private DbSet<PaymentCard> PaymentCards { get; set; }
        private DbSet <Contact> Contacts { get; set; }
        private DbSet<Instalment> Instalments { get; set; }
        private DbSet<TrustPilotInformation> TrustPilotInformation { get; set; }
        private DbSet<Document> Documents { get; set; }
        private DbSet<Fees> Fees { get; set; }
        private DbSet<ErrorLog> ErrorLogs { get; set; }

        // Views on Rates
        private DbSet<EmployersLiabilityBand> EmployersLiabilityBands { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<QuoteLineBase>()
                .Map<QuoteLine>(m => m.Requires("Discriminator").HasValue(1))
                .Map<Discount>(m => m.Requires("Discriminator").HasValue(2));

            modelBuilder.Entity<Contact>()
                .Map<Underwriter>(x => x.Requires("ContactType").HasValue(3))
                .Map<Agent>(x => x.Requires("ContactType").HasValue(2))
                .Map<Customer>(x => x.Requires("ContactType").HasValue(1));

            modelBuilder.Entity<Quote>().Property(x => x.Total).HasPrecision(18, 3);
            modelBuilder.Entity<Quote>().Property(x => x.LegalFees).HasPrecision(18, 3);
            modelBuilder.Entity<Quote>().Property(x => x.ExcessWaiver).HasPrecision(18, 3);
            modelBuilder.Entity<Quote>().Property(x => x.LeisureInsureFees).HasPrecision(18, 3);
            modelBuilder.Entity<Quote>().Property(x => x.Commission).HasPrecision(18, 3);
            modelBuilder.Entity<Quote>().Property(x => x.Excess).HasPrecision(18, 3);
            modelBuilder.Entity<Quote>().Property(x => x.TaxRate).HasPrecision(18, 3);
            modelBuilder.Entity<Quote>().Property(x => x.CardCharge).HasPrecision(18, 3);

            base.OnModelCreating(modelBuilder);
        }
    }
}