﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.LandingPages
{
    public class LandingPage : IEntity
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(255)]
        public string Url { get; set; }
        [MaxLength(255)]
        public string Title { get; set; }
        public string Keywords { get; set; }
        public string Description { get; set; }
        [MaxLength(255)]
        public string PictureUrl { get; set; }
        public string LandingPageDescription { get; set; }
        public int PolicyId { get; set; }
        public DateTime LastModified { get; set; }

        [ForeignKey("LandingPageId")]
        public ICollection<NavigationData> NavigationData { get; set; }

        public int RatePK { get; set; }

        public bool Enabled { get; set; }

        public string Header1 { get; set; }
        public string Paragraph1 { get; set; }
        public string Buynow1 { get; set; }
        public string ChooseUs1 { get; set; }
        public string ChooseUs2 { get; set; }
        public string ChooseUs3 { get; set; }
        public string ChooseUs4 { get; set; }
        public string Header2 { get; set; }
        public string Paragraph2 { get; set; }
    }
}