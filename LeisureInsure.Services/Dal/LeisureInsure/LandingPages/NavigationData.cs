﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.LandingPages
{
    [Table("NavigationData")]
    public class NavigationData : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int LandingPageId { get; set; }
        public int InputId { get; set; }
        public int RateId { get; set; }
    }
}