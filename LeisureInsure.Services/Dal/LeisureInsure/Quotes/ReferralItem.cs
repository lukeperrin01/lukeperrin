﻿using System.ComponentModel.DataAnnotations;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class ReferralItem : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int RateId { get; set; }
        public bool Accepted { get; set; }
        public int QuoteId { get; set; }
    }
}
