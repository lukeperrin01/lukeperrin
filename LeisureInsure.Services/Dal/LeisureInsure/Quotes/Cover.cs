﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class Cover : IEntity
    {
        [Column("CoverPK")]
        public int Id { get; set; }

        [Column("CoverName")]
        public string Name { get; set; }
        [Column("SectionName")]
        public string Description { get; set; }

        [Column("Bitwise")]
        public long BitwiseId { get; set; }

        [Column("Section")]
        public int SectionNo { get; set; }

        [Column("ReferValue")]
        public decimal? ReferValue { get; set; }



    }
}