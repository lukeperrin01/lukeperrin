﻿namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class vmRates
    {
        public int InputPK { get; set; }
        public int CoverPK { get; set; }
        public int? ParentRatePK { get; set; }
        public string RateValue { get; set; }
        public decimal SumInsured { get; set; }
        public int NoItems { get; set; }
        public int CoverPk { get; set; }
        public int ChildBW { get; set; }
        public int RatePK { get; set; }
        public string RateName { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Threshold { get; set; }
        public decimal? Excess { get; set; }
        public string RateLabel { get; set; }
        public string RateTip { get; set; }
        public int Refer { get; set; }
        public int? DivBy { get; set; }
        public int RateOrdinal { get; set; }
        public int HazardRating { get; set; }
        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountSubsequentRate { get; set; }
        public int RateIsPercentage { get; set; }
        public int ThresholdIsPercentage { get; set; }
        public int RateType { get; set; }
        public int TableFirstColumn { get; set; }
        public int TableChildren { get; set; }
        public int? InputTypeFK { get; set; }
        public string UnitType { get; set; }
        public int ParentInputPK { get; set; }
        public decimal? InputOrdinal { get; set; }
        public int NewInputPK { get; set; }

        public int NewParentBW { get; set; }
        public int NewChildBW { get; set; }
        public int? ListRatePK { get; set; }
        public int ReturnBW { get; set; }
        public int RateTypeFK { get; set; }
        public int RateGroup { get; set; }
        public int CoverCalcFK { get; set; }
        public int PageNo { get; set; }

    }
}
