﻿using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class QuoteNotes:IEntity
    {

        [Column("QuoteNotePK")]
        public int Id { get; set; }


        public int QuoteId { get; set; }
        public string Note { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime DateLogged { get; set; }

    }
}
