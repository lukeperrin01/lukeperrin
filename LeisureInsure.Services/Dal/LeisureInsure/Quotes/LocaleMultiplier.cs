﻿using System.ComponentModel.DataAnnotations;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class LocaleMultiplier : IEntity
    {
        [Key]
        public int Id { get; set; }
        public int PolicyId { get; set; }
        public int CoverId { get; set; }
        public int LocaleId { get; set; }
        public decimal Multiplier { get; set; }
    }
}