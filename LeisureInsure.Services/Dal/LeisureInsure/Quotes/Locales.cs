﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes.Locales
{
    public class Locales : IEntity
    {
        [Column("LocalePK")]
        public int Id { get; set; }
        public int BitWise { get; set; }
        public string Country { get; set; }
        public string strLocale { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal Multiplier { get; set; }
        public int LcID { get; set; }
        public int TamAgency { get; set; }
        public string imgFlag { get; set; }
        public string Territory { get; set; }
        public string CurrencyCode { get; set; }
        public decimal? LocaleRefer { get; set; }

    }
}
