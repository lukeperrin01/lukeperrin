﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    [Table("QuoteInputs")]
    public class Answer : IEntity
    {
        [Key]
        [Column("QuoteInputPk")]
        public int Id { get; set; }

        public int CoverPk { get; set; }
        public string InputString { get; set; }

        public int ListNo { get; set; }
        public int ListOrdinal { get; set; }
        public int RatePk { get; set; }
        public string RateValue { get; set; }

        public int QuoteId { get; set; }

        public int SumInsured { get; set; }
        public int? NoItems { get; set; }

        public int RateTypeFK { get; set; }
        public int Excess { get; set; }
        public int Indemnity { get; set; }
        public int? Refer { get; set; }

        public string TerritoryCover { get; set; }
        public int? CertListItem { get; set; }
    }
    
}