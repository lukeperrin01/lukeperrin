﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class Address : IEntity
    {
        [Column("AddressPK")]
        public int Id { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string Locale { get; set; }

        public int AddressType { get; set; }
        public int QuoteFK { get; set; }

        public int ContactFK { get; set; }


    }
}
