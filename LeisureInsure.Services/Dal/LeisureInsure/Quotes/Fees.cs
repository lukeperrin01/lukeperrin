﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class Fees:IEntity
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
        public int PolicyId { get; set; }
        public int LocaleId { get; set; }
        public decimal FeeRate { get; set; }
        public decimal MinimumFee { get; set; }
    }
}
