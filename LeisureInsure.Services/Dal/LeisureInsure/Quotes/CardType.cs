namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public enum CardType
    {
        Debit = 1,
        Credit = 2,
    }
}