﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class EmployersLiabilityBand : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int CoverBw { get; set; }
        public int PolicyBw { get; set; }
        public int LocaleBw { get; set; }
        public DateTime? DateTill { get; set; }
        public decimal WageRollFrom { get; set; }
        public decimal WageRollTo { get; set; }
        public decimal? Rate { get; set; }
    }
}
