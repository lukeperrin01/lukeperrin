﻿using System.ComponentModel.DataAnnotations;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class PaymentCard : IEntity
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public CardType Type { get; set; }

        public decimal AddChargeUk { get; set; }
        public decimal AddChargeIe { get; set; }
        public decimal AddRate { get; set; }
        public string ImageUrl { get; set; }
    }
}