﻿

using System.ComponentModel;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public enum ContactType
    {
        [Description("The person named below")]
        PersonNamed = 1,
        [Description("Limited Company")]
        LimitedCompany = 2,
        [Description("Trading As")]
        TradingAs = 3,
        [Description("Committee")]
        Committee =4,
        [Description("Charity")]
        Charity = 5,
    }
}


