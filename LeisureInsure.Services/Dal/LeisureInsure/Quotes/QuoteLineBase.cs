﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    /// <summary>
    /// SEE <see cref="LiContext"/> FOR CONFIGURATION
    /// </summary>
    [Table("QuoteLines")]
    public class QuoteLineBase : IEntity
    {
        [Key]
        public int Id { get; set; }

        public string LineDescription { get; set; }
        public decimal Price { get; set; }
        public decimal Commision { get; set; }
        public decimal SumInsured { get; set; }
        public decimal indemnity { get; set; }
        public int coverFK { get; set; }
        public decimal Excess { get; set; }
        public decimal Fees { get; set; }
        public string PeriodOfCover { get; set; }
        [NotMapped]
        public decimal Total { get; set; }

        public string Territory { get; set; }

        public int QuoteId { get; set; }
        [NotMapped]
        public int CoverId { get; set; }

        public string sectiondescription { get; set; }

        public string sectionTypeDescription { get; set; }

        public decimal Tax { get; set; }

        public decimal Net { get; set; }

        public bool? ShowInSummary{ get; set; }

        public string ExtraInfo { get; set; }
    }
}