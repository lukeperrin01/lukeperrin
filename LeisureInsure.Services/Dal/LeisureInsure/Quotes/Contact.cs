﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class Contact:IEntity
    {
        [Column("ContactPK")]
        public int Id { get; set; }



        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(250)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string Telephone { get; set; }
        [StringLength(50)]
        public string Email { get; set; }

        //public int ContactType { get; set; }
        [StringLength(50)]
        public string EntityType { get; set; }
        [StringLength(200)]
        public string EntityName { get; set; }

        [StringLength(50)]
        public string TamClientRef { get; set; }

        public string ContactStatus { get; set; }


        public string FSANumber { get; set; }
        public decimal BrokerCommission { get; set; }

        public int CommissionPolicyBW { get; set; }
        public int? QuotePK { get; set; }

        [StringLength(50)]
        public string ForAttentionOf { get; set; }

        public bool? Locked { get; set; }
    }
}
