﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class ChargeElements
    {
        public int PolicyFK { get; set; }
        public int QuoteInputPK { get; set; }
        public string CoverName { get; set; }
        public int CoverFK { get; set; }
        public long CoverBW { get; set; }
        public int RatePK { get; set; }
        public string RateName { get; set; }
        public decimal Rate { get; set; }
        public decimal Threshold { get; set; }
        public decimal Excess { get; set; }
        public int Refer { get; set; }
        public int DivBy { get; set; }
        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountSubsequentRate { get; set; }
        public int HazardRating { get; set; }
        public int TableFirstColumn { get; set; }
        public int TableChildren { get; set; }
        public int RateTypeFK { get; set; }
        public int PageNo { get; set; }
        public decimal SumInsured { get; set; }
        public int NoItems { get; set; }
        public string RateValue { get; set; }
        public int? TriggerCode { get; set; }
        public string Territory { get; set; }
        public int SectionNo { get; set; }
        public string LongRate { get; set; }
        public string RateLabel { get; set; }
        public int? ListRatePK { get; set; }

        public decimal? FirstRate { get; set; }
        public decimal? MinimumRate { get; set; }
        public decimal? Indemnity { get; set; }

        public bool chargeAdded { get; set; }
    }


    public class ChargeElementsNullable
    {
        public int QuoteInputPK { get; set; }
        public int? PolicyFK { get; set; }
        public string CoverName { get; set; }
        public int? CoverFK { get; set; }
        public long? CoverBW { get; set; }
        public int RatePK { get; set; }
        public string RateName { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Threshold { get; set; }
        public decimal? Excess { get; set; }
        public int Refer { get; set; }
        public int? DivBy { get; set; }
        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountSubsequentRate { get; set; }
        public int? HazardRating { get; set; }
        public int? TableFirstColumn { get; set; }
        public int? TableChildren { get; set; }
        public int? RateTypeFK { get; set; }
        public int? PageNo { get; set; }
        public decimal? SumInsured { get; set; }
        public int? NoItems { get; set; }
        public string RateValue { get; set; }
        public int? TriggerCode { get; set; }
        public string Territory { get; set; }
        public int SectionNo { get; set; }
        public string LongRate { get; set; }
        public string RateLabel { get; set; }
        public int? ListRatePK { get; set; }

        public decimal? FirstRate { get; set; }
        public decimal? MinimumRate { get; set; }
        public decimal? Indemnity { get; set; }
    }
}
