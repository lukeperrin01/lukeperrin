﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    [Table("QuoteEndorsements")]
    public class Endorsement:IEntity
    {
        public int QuoteId { get; set; }

        [Key]
        public int Id { get; set; }

        public string Text { get; set; }
    }
}
