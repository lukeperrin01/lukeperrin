﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LeisureInsure.Services.Models.Quotes;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class Quote : IEntity
    {

        [Key]
        [Column("QuotePK")]
        public int Id { get; set; }

        [Column("NetLegal")]
        public decimal LegalFees { get; set; }
        public decimal? TotalLegal { get; set; }
        public bool LegalFeesAdded { get; set; }
        public decimal ExcessWaiver { get; set; }
        public bool ExcessWaiverAdded { get; set; }
        public bool ExcessWaiverAllowed { get; set; }
        public decimal NetPremium { get; set; }
        public decimal Total { get; set; }
        public decimal Tax { get; set; }
        public int Buy { get; set; }
        public int NoInstructors { get; set; }

        public int? OverrideDatesVal { get; set; } = 0;
        public string ContactEmail { get; set; }
        public string ContactName { get; set; }
        public string BusinessDescription { get; set; }
        public string RenewalTamPolicyNo { get; set; }
        public string RenewalTamPolicyRef { get; set; }
        public int? Renewal { get; set; }
        public int? PolicySlot { get; set; }
        //public string Postcode { get; set; }
        //public decimal BrokerCommission { get; set; }

        [Column("Fee")]
        public decimal LeisureInsureFees { get; set; }
        [Column("BrokerCommission")]
        public decimal Commission { get; set; }
        public decimal Excess { get; set; }
        public bool IsAgent { get; set; }
        [Column("PolicyFK")]
        public int PolicyId { get; set; }
        public string PolicyType { get; set; }


        [Column("QuoteRef")]
        public string QuoteReference { get; set; }
        public int BrokerFK { get; set; }

        [ForeignKey(nameof(Quote.CustomerFK))]
        public virtual Customer Customer { get; set; }
        public int CustomerFK { get; set; }
        public int ContactFK { get; set; }
        public int AddressFK { get; set; }

        [ForeignKey(nameof(CustomerFK))]
        public Customer PrimaryCustomer { get; set; }

        [ForeignKey(nameof(AddressFK))]
        public Address PrimaryAddress { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        [Column("PWord")]
        public string Password { get; set; }

        public int PaymentCardId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column("DateAdded")]
        public DateTime DateCreated { get; set; }

        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        [ForeignKey(nameof(QuoteLine.QuoteId))]
        public virtual ICollection<QuoteLine> ChargeSummaryItems { get; set; }

        [ForeignKey(nameof(Discount.QuoteId))]
        public virtual ICollection<Discount> Discounts { get; set; }
        [ForeignKey(nameof(Answer.QuoteId))]
        public virtual ICollection<Answer> Answers { get; set; }
        [NotMapped]
        public ICollection<PaymentOption> PaymentOptions { get; set; }

        public int LocaleId { get; set; }
        public decimal TaxRate { get; set; }
        [ForeignKey(nameof(Quotes.Customer.QuotePK))]
        public virtual ICollection<Customer> Customers { get; set; }
        [ForeignKey(nameof(Underwriter.QuotePK))]
        public virtual ICollection<Underwriter> Underwriters { get; set; }

        [ForeignKey(nameof(Quote.BrokerFK))]
        public virtual Agent Agent { get; set; }

        [ForeignKey(nameof(Address.QuoteFK))]
        public virtual ICollection<Address> Addresses { get; set; }

        public decimal CardCharge { get; set; }

        public int OrderNumber { get; set; }
        public ReferralType ReferralType { get; set; }

        [ForeignKey(nameof(ReferralItem.QuoteId))]
        public ICollection<ReferralItem> ReferralItems { get; set; }

        [ForeignKey(nameof(QuoteNotes.QuoteId))]
        public ICollection<QuoteNotes> Notes { get; set; }
        public bool LockedForCustomer { get; set; }
        public string TamClientCode { get; set; }
        public string TamLegalPolicyCode { get; set; }
        public string TamPolicyCode { get; set; }
        [Column("Biha")]
        public string BihaNumber { get; set; }
        public string TamBrokerCode { get; set; }
        public decimal CommissionPercentage { get; set; }

        [ForeignKey(nameof(QuoteDocument.QuoteFK))]
        public virtual ICollection<QuoteDocument> Documents { get; set; }

        [StringLength(200)]
        public string BrokerName { get; set; }

        public int LandingPageId { get; set; }
        public int Purchased { get; set; }

        [ForeignKey(nameof(Endorsement.QuoteId))]
        public virtual ICollection<Endorsement> Endorsements { get; set; }
        public string Postcode { get; set; }
        //public string Notes { get; set; }
        public Boolean TermsAndConditions { get; set; }
        public int? PayOption { get; set; }
        [Column("DatePurchased")]
        public DateTime? DatePurchased {get;set;}

        [Column("NumberOfDays")]
        public int? NumberOfDays { get; set; }

        public string CodeName { get; set; }

        [Column("Version")]
        public int? Version { get; set; }
       
    }
}