﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class Document:IEntity
    {
        [Key]
        [Column("DocumentPK")]
        public int Id { get; set; }

        [Column("OriginalFilename")]
        public string Name { get; set; }

        [Column("DocLink")]
        public string Link { get; set; }

        [Column("DocType")]
        public string Type { get; set; }



    }
}
