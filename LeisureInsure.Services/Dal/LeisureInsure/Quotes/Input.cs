﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class Input : IEntity
    {
        [Key]
        [Column("InputPK")]
        public int Id { get; set; }

        [Column("ChildCoverPk")]
        public int? UseCoverId { get; set; }
    }
}