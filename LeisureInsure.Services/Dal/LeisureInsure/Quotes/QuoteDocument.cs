﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class QuoteDocument : IEntity
    {
        [Key]
        [Column("QuoteDocumentPK")]
        public int Id { get; set; }

        public int QuoteFK { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string DocumentType { get; set; }
        public int? Lock { get; set; }
        public int? DocumentFK { get; set; }

    }
}
