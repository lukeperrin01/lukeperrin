﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.Services.Dal.LeisureInsure.Quotes
{
    public class QuoteDetail : IEntity
    {
        [Key]
        [Column("QuoteDetailPK")]
        public int Id { get; set; }

        [Column("InputFK")]
        public int InputId { get; set; }

        [Column("CoverFK")]
        public int CoverId { get; set; }

        [Column("RateFK")]
        public int RateId { get; set; }

        [Column("QuoteFK")]
        public int QuoteId { get; set; }

        public string LineType { get; set; }
        public decimal Base { get; set; }
        public decimal Rate { get; set; }
        public decimal Charge { get; set; }
        public string TamPolicyIndex { get; set; }
        [Column("ListNo")]
        public int ListNumber { get; set; }
        public int ListOrdinal { get; set; }
        public DateTime DateAdded { get; set; }
        public string InputValue { get; set; }
    }
}