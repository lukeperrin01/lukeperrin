﻿namespace ItOps.Endpoint.ViewModels
{
    public class NewBusinessVm : ViewModelBase
    {
        public string CertificateLink { get; set; }
        public string PolicyWording { get; set; }
        public string PolicySummary { get; set; }
        public string QuoteReference { get; set; }

        public string TermsOfBusiness { get; set; }

        public string Name { get; set; }
        public string PolicyType { get; set; }

        public int PolicyId { get; set; }
    }
}
