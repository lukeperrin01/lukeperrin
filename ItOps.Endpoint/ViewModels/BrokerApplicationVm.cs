﻿namespace ItOps.Endpoint.ViewModels
{
    public class BrokerApplicationVm : ViewModelBase
    {
        public string Name { get; set; }
        public string TobaLink { get; set; }
        public string ApplicationFormLink { get; set; }
    }
}
