﻿namespace ItOps.Endpoint.ViewModels
{
    public class ViewModelBase
    {
        public string TrustPilotStarLink { get; set; }
        public string TrustPilotDescription { get; set; }
    }
}
