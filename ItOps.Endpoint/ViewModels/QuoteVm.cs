﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using System.Collections.Generic;
using LeisureInsure.DB;
namespace ItOps.Endpoint.ViewModels
{
    public class QuoteVm : ViewModelBase
    {
        public Quotes Quote { get; set; }
        public string BuyLink { get; set; }
        public string QuoteLink { get; set; }
        public string Currency { get; set; }
        public string CertificateLink { get; set; }

        public IList<RawLinks> RawLinks  { get; set; }
    }
}
