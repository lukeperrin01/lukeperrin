﻿using LeisureInsure.DB;

using System.Collections.Generic;

namespace ItOps.Endpoint.ViewModels
{
    public class QuoteVmNew : ViewModelBase
    {
        public Quotes Quote { get; set; }
        public string BuyLink { get; set; }
        public string Currency { get; set; }
        public string CertificateLink { get; set; }

        public IList<RawLinks> RawLinks  { get; set; }
    }
}
