﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItOps.Endpoint.ViewModels
{
    public class RawLinks : ViewModelBase
    {
        public string LinkType { get; set; }
        public string Url { get; set; }
    }
}
