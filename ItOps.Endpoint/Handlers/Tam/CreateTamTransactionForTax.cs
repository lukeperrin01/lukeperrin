﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    public class CreateTamTransactionForTax : IHandleMessages<CreateTaxTransactionOnTam>
    {
        private IInterfaceWithTam _tam;

        public CreateTamTransactionForTax(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(CreateTaxTransactionOnTam message, IMessageHandlerContext context)
        {
            _tam.AddTaxTransaction(message.QuoteReference);
            return Task.FromResult(0);
        }
    }
}
