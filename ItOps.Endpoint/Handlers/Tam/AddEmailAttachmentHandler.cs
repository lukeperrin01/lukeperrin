﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class AddEmailAttachmentHandler:IHandleMessages<AddEmailAttachmentToTam>
    {
        private IInterfaceWithTam _tam;

        public AddEmailAttachmentHandler(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(AddEmailAttachmentToTam message, IMessageHandlerContext context)
        {
            _tam.AddMailMessage(message.QuoteReference, message.EmailDataBus);
            return Task.FromResult(true);
        }
    }
}
