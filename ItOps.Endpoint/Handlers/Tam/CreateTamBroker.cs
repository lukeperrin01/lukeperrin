﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    public class CreateTamBroker : IHandleMessages<CreateBrokerInTam>
    {
        private IInterfaceWithTam _tam;

        public CreateTamBroker(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(CreateBrokerInTam message, IMessageHandlerContext context)
        {
            _tam.AddTamBroker(message);
            return Task.FromResult(true);
        }
    }
}
