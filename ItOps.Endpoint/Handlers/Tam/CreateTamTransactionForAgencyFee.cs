﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using LeisureInsure.Services.Dal.LeisureInsure;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    public class CreateTamTransactionForAgencyFee :IHandleMessages<CreateAgencyFeeTransactionOnTam>
    {
        private IInterfaceWithTam _tam;
        
        public CreateTamTransactionForAgencyFee(
            IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(CreateAgencyFeeTransactionOnTam message, IMessageHandlerContext context)
        {
            _tam.AddAgencyFeeTransaction(message.QuoteReference);
            return Task.FromResult(0);
        }
    }
}
