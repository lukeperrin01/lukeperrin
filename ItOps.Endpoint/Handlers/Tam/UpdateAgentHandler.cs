﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class UpdateAgentHandler : IHandleMessages<UpdateAgentInTam>
    {
        private IInterfaceWithTam _tam;

        public UpdateAgentHandler(IInterfaceWithTam tam)
        {
            _tam = tam;
        }


        public Task Handle(UpdateAgentInTam message, IMessageHandlerContext context)
        {
            _tam.UpdateAgent(message);
            return Task.FromResult(true);
        }
    }
}
