﻿using System.Threading.Tasks;
using ItOps.Endpoint.Tam;
using ItOps.Messages.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class AddLegalCarePolicyToTam : IHandleMessages<Commands.Tam.AddLegalCarePolicyToTam>
    {
        private IInterfaceWithTam _tam;

        public AddLegalCarePolicyToTam(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public async Task Handle(Commands.Tam.AddLegalCarePolicyToTam message, IMessageHandlerContext context)
        {
            _tam.AddTamLegalPolicy(message.QuoteReference, message.PayByInstallments, message.AgentCode);
            await context.Reply<LegalCarePolicyCreatedOnTam>(m => { }).ConfigureAwait(false);
        }
    }
}
