﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using ItOps.Messages.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class CreateTamPolicy : IHandleMessages<CreatePolicyOnTam>
    {
        private IInterfaceWithTam _tam;

        public CreateTamPolicy(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public async Task Handle(CreatePolicyOnTam message, IMessageHandlerContext context)
        {
            _tam.AddTamPolicy(message.QuoteReference, message.PayByInstallments, message.AgentCode);
            await context.Reply<PolicyCreatedOnTam>(m => { }).ConfigureAwait(false);
        }
    }
}
