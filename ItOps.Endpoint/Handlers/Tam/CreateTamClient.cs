﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using ItOps.Messages.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class CreateTamClient : IHandleMessages<CreateClientOnTam>
    {
        private IInterfaceWithTam _tam;

        public CreateTamClient(IInterfaceWithTam tam)
        {
            _tam = tam;
        }
        
        public async Task Handle(CreateClientOnTam message, IMessageHandlerContext context)
        {
            _tam.AddTamCustomer(message.QuoteReference);
            await context.Reply<ClientCreatedOnTam>(m => {}).ConfigureAwait(false); 
        }
    }
}
