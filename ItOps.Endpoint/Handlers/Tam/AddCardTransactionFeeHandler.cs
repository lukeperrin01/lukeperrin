﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class AddCardTransactionFeeHandler : IHandleMessages<AddCardChargeTransactionToTam>
    {
        private IInterfaceWithTam _tam;

        public AddCardTransactionFeeHandler(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(AddCardChargeTransactionToTam message, IMessageHandlerContext context)
        {
            _tam.AddCardChargeTransaction(message.QuoteReference);
            return Task.FromResult(true);
        }
    }
}
