﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class CreateTamTransactionForPolicy : IHandleMessages<CreatePolicyTransactionOnTam>
    {
        private IInterfaceWithTam _tam;

        public CreateTamTransactionForPolicy(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(CreatePolicyTransactionOnTam message, IMessageHandlerContext context)
        {
            _tam.AddPolicyTransaction(message.QuoteReference);
            return Task.FromResult(0);
        }
    }
}
