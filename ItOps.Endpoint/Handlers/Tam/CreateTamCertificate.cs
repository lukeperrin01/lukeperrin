﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class CreateTamCertificate : IHandleMessages<CreateCertificateOnTam>
    {
        private IInterfaceWithTam _tam;

        public CreateTamCertificate(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(CreateCertificateOnTam message, IMessageHandlerContext context)
        {
            _tam.AddTamAttachment(message.QuoteReference);
            return Task.FromResult(true);
        }
    }
}
