﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class AddLegalFeesTransactionHandler : IHandleMessages<AddLegalFeesTransactionToTam>
    {
        private IInterfaceWithTam _tam;

        public AddLegalFeesTransactionHandler(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(AddLegalFeesTransactionToTam message, IMessageHandlerContext context)
        {
            _tam.AddLegalFeeTransaction(message.QuoteReference);
            return Task.FromResult(true);
        }
    }
}
