﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{

    class AmendTamClient : IHandleMessages<AmendTAMClient>
    {
        private IInterfaceWithTam _tam;

        public AmendTamClient(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(AmendTAMClient message, IMessageHandlerContext context)
        {
            _tam.AddTamCustomer(message.QuoteReference);
            return Task.FromResult(0);
        }
    }
}
