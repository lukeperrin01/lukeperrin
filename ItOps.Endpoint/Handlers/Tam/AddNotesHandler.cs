﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class AddNotesHandler : IHandleMessages<AddNotesToTam>
    {
        private IInterfaceWithTam _tam;

        public AddNotesHandler(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(AddNotesToTam message, IMessageHandlerContext context)
        {
            _tam.AddNotes(message.QuoteReference);
            return Task.FromResult(true);
        }
    }
}
