﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    class AddAccountingContactHandler : IHandleMessages<AddAccountingContactToTam>
    {
        private IInterfaceWithTam _tam;

        public AddAccountingContactHandler(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(AddAccountingContactToTam message, IMessageHandlerContext context)
        {
            _tam.AddAccountingContact(message.QuoteReference);
            return Task.FromResult(true);
        }
    }
}
