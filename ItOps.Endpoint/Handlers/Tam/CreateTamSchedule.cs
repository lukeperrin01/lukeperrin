﻿using System.Threading.Tasks;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Tam;
using NServiceBus;

namespace ItOps.Endpoint.Handlers.Tam
{
    public class CreateTamSchedule : IHandleMessages<CreateScheduleOnTam>
    {
        private IInterfaceWithTam _tam;

        public CreateTamSchedule(IInterfaceWithTam tam)
        {
            _tam = tam;
        }

        public Task Handle(CreateScheduleOnTam message, IMessageHandlerContext context)
        {
            _tam.AddTamCustomDec(message.QuoteReference);
            return Task.FromResult(0);
        }
    }
}
