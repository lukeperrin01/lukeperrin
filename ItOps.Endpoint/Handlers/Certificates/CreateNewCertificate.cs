﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ItOps.Commands.Certificates;
using ItOps.Endpoint.Tam;
using ItOps.Messages.Certificates;
using LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using Microsoft.Azure;
using NServiceBus;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Handlers.Certificates
{
    class CreateNewCertificate : IHandleMessages<CreateCertificate>
    {
        private IInterfaceWithTam _tam;
        private HttpClient _httpClient;
        private IRepository _repository;

        public CreateNewCertificate(
            IInterfaceWithTam tam, 
            IRepository repository, 
            HttpClient httpClient)
        {
            _tam = tam;
            _repository = repository;
            _httpClient = httpClient;
        }

        public IRepository Repository { get; set; }
        public HttpClient HttpClient { get; set; }
        public IInterfaceWithTam Tam { get; set; }

        public async Task Handle(CreateCertificate message, IMessageHandlerContext context)
        {
            Console.WriteLine("Create Cert");
            var quote = Repository.GetQuotebyQuoteRef(message.QuoteReference);
            
            var parameters = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("quoteReference", quote.QuoteRef),
                new KeyValuePair<string, string>("passcode", quote.PWord),
            });

            // Generate the certificate
            var url = $"{CloudConfigurationManager.GetSetting(Constants.Configuration.WebsiteBaseAddress)}/certificates/generate";                      

            var result = await HttpClient.PostAsync(new Uri(url), parameters).ConfigureAwait(false);
           
            if (!result.IsSuccessStatusCode)
            {
                var errorid = AppInsightLog.LogTamError(new ApplicationException($"[ItOps.Endpoint.Handlers.Certificates] Could not generate certificate for quote ref: {message.QuoteReference}"),url+"/"+quote.QuoteRef + "/"+quote.PWord,message.QuoteReference);

                throw new ApplicationException($"<p>QuoteRef: {message.QuoteReference} |</p> <p>Could not generate certificate for quote ref: {message.QuoteReference}</p>");
            }

            await context.Reply<CertificateCreated>(m => { });
        }
    }
}
