﻿using System.Threading.Tasks;
using ItOps.Commands.Email;
using ItOps.Endpoint.Services.Emailer;
using LeisureInsure;
using Microsoft.Azure;
using NServiceBus;
using System;

namespace ItOps.Endpoint.Handlers.Email
{
    class SendEmailHandler : IHandleMessages<SendEmail>
    {
        public ISendEmails Email { get; set; }

        public async Task Handle(SendEmail message, IMessageHandlerContext context)
        {
            
            await Email.Send(CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender), message.To,
                    message.Subject, message.HtmlBody, message.Body, "").ConfigureAwait(false);

        }
    }
}
