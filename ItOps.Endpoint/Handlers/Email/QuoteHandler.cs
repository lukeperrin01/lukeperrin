﻿using System.Linq;
using System.Threading.Tasks;
using ItOps.Commands.Email;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.Services.Emailer;
using ItOps.Endpoint.Services.Interfaces;
using ItOps.Endpoint.ViewModels;
using LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure;
using Microsoft.Azure;
using NServiceBus;
using RazorEngine;
using RazorEngine.Templating;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Handlers.Email
{
    class QuoteHandler : IHandleMessages<Quote>
    {
        public IRepository Repository { get; set; }
        public ISendEmails Email { get; set; }
        public ISupplyTrustPilotInformation TrustPilot { get; set; }

        public async Task Handle(Quote message, IMessageHandlerContext context)
        {
            var quote = Repository.GetQuotebyQuoteRef(message.QuoteReference);

            var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);                        

            var model = new QuoteVm
            {
                Quote = quote,
                BuyLink = message.BaseLink + "/quote/" + message.QuoteReference + "/" + message.Passcode,
                Currency = quote.LocaleId == 1 ? "£" : "€",
            };
            TrustPilot.ProcessViewModel(model);
            var htmlBody = Engine.Razor.Run(Template.Quote, typeof(QuoteVm), model);

            await Email.Send(from, message.Email, "Your LeisureInsure quote", htmlBody, null,"info@leisureInsure.co.uk")
                .ConfigureAwait(false);
        }
    }
}
