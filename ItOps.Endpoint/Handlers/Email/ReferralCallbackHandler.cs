﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItOps.Commands.Email;
using ItOps.Endpoint.Services.Emailer;
using LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using Microsoft.Azure;
using NServiceBus;
using Quote = LeisureInsure.Services.Dal.LeisureInsure.Quotes.Quote;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Handlers.Email
{
    class ReferralCallbackHandler : IHandleMessages<ReferralCallback>
    {
        public ISendEmails Email { get; set; }
        public IRepository Repository { get; set; }

        public async Task Handle(ReferralCallback message, IMessageHandlerContext context)
        {
            var quote = Repository.GetQuotebyQuoteRef(message.QuoteReference);
            var customer = new Customer
            {
                Email = message.Email,
                EntityName = message.Name,
                Telephone = message.Phone,
                FirstName = string.Empty,
                LastName = string.Empty,
            };

            quote.ContactEmail = message.Email;

            //Repository.AddCustomer(message.QuoteReference, message.Email, message.Phone, message.Name);



            var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);
            var to = CloudConfigurationManager.GetSetting("referral.email.to");
            var baseLink = CloudConfigurationManager.GetSetting(Constants.Configuration.WebsiteBaseAddress);

            var body = new StringBuilder();
            body.AppendLine($"New referral request for quote reference: {message.QuoteReference}");
            body.AppendLine($"{baseLink}/admin/underwriter/quotes/edit/{message.QuoteReference}");
            body.AppendLine("");
            body.AppendLine($"Name: {message.Name}");
            if(quote.IsAgent == true)
            {
                body.AppendLine($"Client Name: {message.ClientName}");
            }
            body.AppendLine($"Email: {message.Email}");
            body.AppendLine($"Telephone: {message.Phone}");

            await Email.Send(from, to, "New Referral Callback", body.ToString(), body.ToString(),"")
                .ConfigureAwait(false);
        }
    }
}
