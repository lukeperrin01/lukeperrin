﻿using System.Linq;
using System.Threading.Tasks;
using ItOps.Commands.Email;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.Services.Emailer;
using ItOps.Endpoint.Services.Interfaces;
using ItOps.Endpoint.ViewModels;
using LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure;
using Microsoft.Azure;
using NServiceBus;
using RazorEngine;
using RazorEngine.Templating;
using System.Collections.Generic;
using Quote = LeisureInsure.Services.Dal.LeisureInsure.Quotes.Quote;
using LeisureInsure.DB;
using LeisureInsure.DB.ViewModels;

namespace ItOps.Endpoint.Handlers.Email
{
    class EmailRenewalHandler : IHandleMessages<EmailRenewal>
    {
        public IRepository Repository { get; set; }
        public ISupplyTrustPilotInformation TrustPilot { get; set; }
        public ISendEmails Email { get; set; }

        public async Task Handle(EmailRenewal message, IMessageHandlerContext context)
        {
            var quote = Repository.GetQuotebyQuoteRef(message.QuoteReference);
            var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);
            IList<QuoteDocuments> docs = Repository.getQuoteDocumentsforEmail(quote.QuotePK);
            var emailTo = "";
            IList<RawLinks> docLinks = new List<RawLinks>();
            var webAddress = CloudConfigurationManager.GetSetting("website.address");

            if (!string.IsNullOrEmpty(quote.ContactEmail))
            {
                emailTo = quote.ContactEmail;
            }
            else if (!string.IsNullOrEmpty(quote.BrokerName))
            {
                emailTo = Repository.getBrokerEmailbyTamcode(quote.BrokerName);
            }
            else if (string.IsNullOrEmpty(quote.BrokerName))
            {
                var c = Repository.GetCustomer(quote.CustomerFK);
                if (c != null)
                {
                    emailTo = c.Email;
                }
            }

            var to = emailTo;


            foreach (QuoteDocuments doc in docs.Where(x => x.DocumentType != "Clause"))
            {
                if (doc.DocumentType == "SoF")
                {
                    docLinks.Add(new RawLinks()
                    {
                        LinkType = doc.Description,
                        Url = webAddress + "/" + doc.Url
                    });
                }
                else
                {
                    docLinks.Add(new RawLinks()
                    {
                        LinkType = doc.Description,
                        Url = webAddress + "/Content/CertificateDocuments/" + doc.Url
                    });
                }
            }

            if(quote.LocaleId == 1)
            {
                docLinks.Add(new RawLinks()
                {
                    LinkType = "Legal Policy Key Facts",
                    Url = webAddress + "/Content/CertificateDocuments/Leisureinsure Legal Keyfacts 06-07-2015"
                });
                docLinks.Add(new RawLinks()
                {
                    LinkType = "Legal Policy wording",
                    Url = webAddress + "/Content/CertificateDocuments/Leisureinsure Legal Care Policy 06.07.15"
                });
            }

            var model = new QuoteVm
            {
                BuyLink =
                    CloudConfigurationManager.GetSetting("website.address") + "/checkout/" + quote.QuoteRef + "/" +
                    quote.PWord,
                CertificateLink = CloudConfigurationManager.GetSetting("website.address") + "/certificate/" + quote.QuoteRef + "/" +
                    quote.PWord,
                RawLinks = docLinks
            };
            TrustPilot.ProcessViewModel(model);
            var htmlBody = Engine.Razor.Run(Template.EmailRenewal, typeof(QuoteVm), model);

            await Email.Send(from, to, "Your LeisureInsure quote is due for renewal", htmlBody, null, "")
                    .ConfigureAwait(false);
        }
    }
}
