﻿using System.Linq;
using System.Threading.Tasks;
using ItOps.Commands.Email;
using ItOps.Commands.Tam;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.Services.Emailer;
using ItOps.Endpoint.Services.Interfaces;
using ItOps.Endpoint.ViewModels;
using LeisureInsure;
using Microsoft.Azure;
using NServiceBus;
using RazorEngine;
using RazorEngine.Templating;
using System;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB;
using System.Collections.Generic;
using LeisureInsure.DB.Business;

namespace ItOps.Endpoint.Handlers.Email
{
    class NewBusinessHandler : IHandleMessages<NewPolicyEmail>
    {
        public IRepository Repository { get; set; }
        public ISendEmails Email { get; set; }
        public ISupplyTrustPilotInformation TrustPilot { get; set; }

        public async Task Handle(NewPolicyEmail message, IMessageHandlerContext context)
        {
                       
            var quote = Repository.GetQuotebyQuoteRef(message.QuoteReference);
            string quoteref = "", emailToString = ""; 

            try
            {
                
                Contacts customer = Repository.GetCustomer(quote.CustomerFK);                

                quoteref = quote.QuoteRef;

                var baseLink = CloudConfigurationManager.GetSetting(Constants.Configuration.WebsiteBaseAddress);
                var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);               


                var newdocs = new List<PolicyDocuments>();

                using (var repo = new PoliciesRepository())
                {
                    newdocs = repo.GetDocsForPolicy(quote.QuotePK, quote.LegalFeesAdded);
                }


                var summary = newdocs.FirstOrDefault(x => x.LinkType.ToLower().Equals("keyfacts"));
                var wording = newdocs.FirstOrDefault(x => x.LinkType.ToLower().Equals("wording"));
                var terms = newdocs.FirstOrDefault(x => x.LinkType.ToLower().Equals("terms"));
                var model = new NewBusinessVm();

                model.PolicyType = quote.PolicyType;
                model.PolicyId = (int)quote.PolicyFK;

                model.CertificateLink = $@"{baseLink}/newcertificate/{quote.QuoteRef}/{quote.PWord}";

                if (summary != null)
                    model.PolicySummary = summary.DocLink;

                if (wording != null)
                    model.PolicyWording = wording.DocLink;

                if (terms != null)
                    model.TermsOfBusiness = terms.DocLink;

                model.QuoteReference = quote.QuoteRef;
                if (string.IsNullOrEmpty(quote.ContactName))
                {
                    if(customer != null)
                    model.Name = customer.FirstName ?? customer.ForAttentionOf;
                }
                else
                {
                    model.Name = quote.ContactName;
                }

                TrustPilot.ProcessViewModel(model);

                string htmlBody;

                if (quote.IsAgent)
                {
                    htmlBody = Engine.Razor.Run(Template.NewBusinessBroker, typeof(NewBusinessVm), model);
                }
                else
                {
                    htmlBody = Engine.Razor.Run(Template.NewBusiness, typeof(NewBusinessVm), model);
                }
               

                if (string.IsNullOrEmpty(customer.Email))
                {
                    throw new Exception("Customer Email address is missing or null");
                }

                if (!string.IsNullOrEmpty(quote.ContactEmail))
                {
                    emailToString = quote.ContactEmail;
                }
                else
                {
                    emailToString =customer.Email;
                }
                var bcc = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailBCC);               

                var email = await Email.CreateEml(from, emailToString, quoteref, htmlBody, null, bcc)
                    .ConfigureAwait(false);

                await context.Send<AddEmailAttachmentToTam>(m =>
                {
                    m.QuoteReference = message.QuoteReference;
                    m.EmailDataBus = email;
                }).ConfigureAwait(false);

                await Email.Send(from, emailToString, quoteref, htmlBody, null, bcc)
                        .ConfigureAwait(false);

                Console.WriteLine($"Sent new business email for quote:{message.QuoteReference} to {emailToString}");

            }
            catch (Exception ex)
            {
                
                var errorid = AppInsightLog.LogTamError(ex, "[NewPolicyEmail_Handler" + quote.QuoteRef, message.QuoteReference);
               
                if (ex.Message != "Mailbox unavailable. The server response was: Requested action not taken: mailbox unavailable")
                    throw new Exception("NewPolicyEmail_Handler", ex);
                else
                {
                    var website = CloudConfigurationManager.GetSetting("website.address");

                    //if there is mailbox error allow to continue so we get things in TAM and notify info
                    await Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "New Policy Email Undelivered",
                        $"Policy: {quoteref} was not delivered to {emailToString} from {website}",null,"luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);
                }

            }

        }
    }

}