﻿using System.Linq;
using System.Threading.Tasks;
using ItOps.Commands.Email;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.Services.Emailer;
using ItOps.Endpoint.Services.Interfaces;
using ItOps.Endpoint.ViewModels;
using LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure;
using Microsoft.Azure;
using NServiceBus;
using RazorEngine;
using RazorEngine.Templating;
using Quote = LeisureInsure.Services.Dal.LeisureInsure.Quotes.Quote;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Handlers.Email
{
    class EmailCustomerAfterReferralHandler : IHandleMessages<EmailCustomerAfterReferral>
    {
        public IRepository Repository { get; set; }
        public ISupplyTrustPilotInformation TrustPilot { get; set; }
        public ISendEmails Email { get; set; }

        public async Task Handle(EmailCustomerAfterReferral message, IMessageHandlerContext context)
        {
            var quote = Repository.GetQuotebyQuoteRef(message.QuoteReference);
            var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);
            var emailTo = "";

            if (quote.ContactEmail != null)
            {
                emailTo = quote.ContactEmail;
            }
            else if (quote.BrokerName != null)
            {
                emailTo = Repository.getBrokerEmailbyTamcode(quote.BrokerName);
            }
            else if(quote.BrokerName == null)
            {
                var c = Repository.GetCustomer(quote.CustomerFK);
                if (c != null)
                {
                    emailTo = c.Email;
                }
            }

            var to = emailTo;

            var model = new QuoteVm
            {
                BuyLink =
                    CloudConfigurationManager.GetSetting("website.address") + "/checkout/" + quote.QuoteRef + "/" +
                    quote.PWord,
            };
            TrustPilot.ProcessViewModel(model);
            var htmlBody = Engine.Razor.Run(Template.EmailCustomerAfterReferral, typeof(QuoteVm), model);

            await Email.Send(from, to, "Your LeisureInsure quote has been updated", htmlBody, null,"")
                .ConfigureAwait(false);
        }
    }
}
