﻿using System.Threading.Tasks;
using ItOps.Commands.Email;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.Services.Emailer;
using ItOps.Endpoint.Services.Interfaces;
using ItOps.Endpoint.ViewModels;
using LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure;
using Microsoft.Azure;
using NServiceBus;
using RazorEngine;
using RazorEngine.Templating;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Handlers.Email
{
    class SendBrokerApplicationHandler : IHandleMessages<SendBrokerApplication>
    {
        public IRepository Repository { get; set; }
        public ISendEmails Email { get; set; }
        public ISupplyTrustPilotInformation TrustPilot { get; set; }

        public async Task Handle(SendBrokerApplication message, IMessageHandlerContext context)
        {
            var baseLink = CloudConfigurationManager.GetSetting(Constants.Configuration.WebsiteBaseAddress);
            var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);

            var model = new BrokerApplicationVm
            {
                ApplicationFormLink = $@"{baseLink}/content/pdf/AgencyApplicationForm.pdf",
                TobaLink = $@"{baseLink}/content/pdf/toba.pdf",
            };
            TrustPilot.ProcessViewModel(model);

            var htmlBody = Engine.Razor.Run(Template.NewBrokerApplicaiton, typeof(BrokerApplicationVm), model);

            await Email.Send(from, message.Email, "Leisure Insure - Online Agent Application", htmlBody, null,"")
                .ConfigureAwait(false);
        }
    }
}
