﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ItOps.Commands.TrustPilot;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Information;
using Newtonsoft.Json;
using NServiceBus;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Handlers.TrustPilot
{
    class TrustPilotUpdaterSaga : Saga<TrustPilotUpdaterSagaData>,
        IAmStartedByMessages<StartTrustPilotUpdates>,
        IHandleTimeouts<TrustPilotTimeout>
    {
        public IRepository Repository { get; set; }
        public HttpClient Client { get; set; }

        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<TrustPilotUpdaterSagaData> mapper)
        {
            mapper.ConfigureMapping<StartTrustPilotUpdates>(x => x.Id).ToSaga(x => x.StarterId);
        }

        public async Task Handle(StartTrustPilotUpdates message, IMessageHandlerContext context)
        {
            Data.NextFire = DateTime.Now.AddHours(-6);
            Data.StarterId = message.Id;
            await UpdateTrustPilot(context);
            await SetTimeout(context);
        }


        public async Task Timeout(TrustPilotTimeout state, IMessageHandlerContext context)
        {
            await UpdateTrustPilot(context);
            await SetTimeout(context);
        }

        private async Task UpdateTrustPilot(IMessageHandlerContext context)
        {
            var result = await Client
                .GetAsync(new Uri("https://api.trustpilot.com/v1/business-units/4da9b09f00006400050f8ca6?apikey=i1HQjxqLrv7yi6bFyVq3hv15lsz1NgZn"))
                .ConfigureAwait(false);
            if (!result.IsSuccessStatusCode)
            {
                await RequestTimeout<TrustPilotTimeout>(context, DateTime.Now.AddMinutes(10));
                return;
            }
            var data = await result.Content.ReadAsStringAsync().ConfigureAwait(false);
            var json = JsonConvert.DeserializeObject<dynamic>(data);
            var stars = Convert.ToInt32(json.stars);

            var stored = Repository.GetTrustPilot();
            stored.Stars = stars;
            switch (stored.Stars)
            {
                case 3:
                    stored.Description = "Average";
                    break;
                case 4:
                    stored.Description = "Great";
                    break;
                case 5:
                    stored.Description = "Excellent";
                    break;
            }

            Repository.saveTrustPilot(stored);
            Console.WriteLine("Updated trustpilot score");
        }

        private async Task SetTimeout(IMessageHandlerContext context)
        {
            if (DateTime.Now >= Data.NextFire)
            {
                Data.NextFire = Data.NextFire.AddHours(6);
                var delay = Data.NextFire - DateTime.Now;
                while (delay < TimeSpan.Zero)
                {
                    Data.NextFire = Data.NextFire.AddHours(6);
                    delay += TimeSpan.FromHours(6);
                }
                
                await RequestTimeout<TrustPilotTimeout>(context, delay);
            }
        }
    }

    class TrustPilotTimeout
    {}

    class TrustPilotUpdaterSagaData : IContainSagaData
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public DateTime NextFire { get; set; }
        public int StarterId { get; set; }
    }
}
