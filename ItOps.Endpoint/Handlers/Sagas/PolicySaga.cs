﻿using System;
using System.Threading.Tasks;
using ItOps.Commands.Certificates;
using ItOps.Commands.Email;
using ItOps.Commands.Tam;
using ItOps.Messages.Certificates;
using ItOps.Messages.Tam;
using NServiceBus;
using ItOps.Commands;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB;
using ItOps.Endpoint.Services.Emailer;
using Microsoft.Azure;



namespace ItOps.Endpoint.Handlers.Sagas
{
    class PolicySaga : Saga<PolicySagaData>,
        IAmStartedByMessages<CreatePolicyForAgent>,
        IAmStartedByMessages<CreatePolicyForClient>,

        IHandleMessages<PaymentConfirmed>,
        IHandleMessages<PaymentFailed>,

        IHandleTimeouts<OneWeekSinceStarted>,
        IHandleTimeouts<ElevenMonthsSincePayment>,

        IHandleMessages<ClientCreatedOnTam>,
        IHandleMessages<CertificateCreated>,
        IHandleMessages<PolicyCreatedOnTam>,
        IHandleMessages<LegalCarePolicyCreatedOnTam>
    {

        private IRepository _repository;
        public ISendEmails Email { get; set; }

        public PolicySaga(IRepository repository)
        {
            _repository = repository;            
        }

        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<PolicySagaData> mapper)
        {            

            mapper.ConfigureMapping<CreatePolicyForAgent>(message => message.PolicyReference).ToSaga(saga => saga.PolicyReference);
            mapper.ConfigureMapping<CreatePolicyForClient>(message => message.PolicyReference).ToSaga(saga => saga.PolicyReference);

            mapper.ConfigureMapping<PaymentConfirmed>(message => message.QuoteReference).ToSaga(saga => saga.PolicyReference);
            mapper.ConfigureMapping<PaymentFailed>(message => message.PolicyReference).ToSaga(saga => saga.PolicyReference);
         
        }

        public async Task Handle(CreatePolicyForAgent message, IMessageHandlerContext context)
        {
            try
            {                

                Console.WriteLine($"Agent Saga created for policy ref: {message.PolicyReference}");


                Data.PayByInstallments = message.PayByInstallments;
                Data.AgentCode = message.AgentCode;

                await context.Send<CreateClientOnTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);
                await RequestTimeout<ElevenMonthsSincePayment>(context, DateTime.Now.AddMonths(11)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "[Handler CreatePolicyForAgent]" + message.PolicyReference, message.PolicyReference);
                throw new Exception($"Handle.CreatePolicyForAgent({message.PolicyReference} Message{ex.Message}");
            }
        }

        public Task Handle(CreatePolicyForClient message, IMessageHandlerContext context)
        {            

            Console.WriteLine($"Customer Saga created for policy ref: {message.PolicyReference}");
            Data.PayByInstallments = message.PayByInstallments;
            Data.AgentCode = message.AgentCode;
            return Task.FromResult(0);
        }

        public async Task Handle(PaymentConfirmed message, IMessageHandlerContext context)
        {

            try
            {

                _repository.setQuotePurchased(message.QuoteReference, true);
                await context.Send<CreateClientOnTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);
                await RequestTimeout<ElevenMonthsSincePayment>(context, DateTime.Now.AddSeconds(30)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                AppInsightLog.LogTamError(ex, $"[Handle(PaymentConfirmed)]", message.QuoteReference);

                throw new Exception($"Handle.PaymentConfirmed({message.QuoteReference} Message{ex.Message}");
            }

        }

        public async Task Handle(PaymentFailed message, IMessageHandlerContext context)
        {
            _repository.setQuotePurchased(message.PolicyReference, false);

            await RequestTimeout<OneWeekSinceStarted>(context, DateTime.Now.AddDays(7));
        }

        public Task Timeout(OneWeekSinceStarted state, IMessageHandlerContext context)
        {
            Console.WriteLine("One week since started");
            return Task.FromResult(0);
        }

        public Task Timeout(ElevenMonthsSincePayment state, IMessageHandlerContext context)
        {
            Console.WriteLine("Eleven months since started");
            return Task.FromResult(0);
        }

        public async Task Handle(ClientCreatedOnTam message, IMessageHandlerContext context)
        {
            Console.WriteLine($"Client Created for saga with id: {Data.PolicyReference}");            

            await context.Send<CreatePolicyOnTam>(m =>
            {
                m.QuoteReference = Data.PolicyReference;
                m.PayByInstallments = Data.PayByInstallments;
                m.AgentCode = Data.AgentCode;
            }).ConfigureAwait(false);

            
        }

        public async Task Handle(LegalCarePolicyCreatedOnTam message, IMessageHandlerContext context)
        {
            Console.WriteLine($"LegalCarePolicyCreatedOnTam Created for saga with id: {Data.PolicyReference}");

            await context.Send<AddLegalFeesTransactionToTam>(m =>
            {
                m.QuoteReference = Data.PolicyReference;
            }).ConfigureAwait(false);
        }

        public async Task Handle(PolicyCreatedOnTam message, IMessageHandlerContext context)
        {
            try
            {
                Console.WriteLine($"Policy Created on Tam for saga with id: {Data.PolicyReference}");

                await context.Send<CreateCertificate>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);

                //await context.Send<AddCardChargeTransactionToTam>(m =>
                //{
                //    m.QuoteReference = Data.PolicyReference;
                //}).ConfigureAwait(false);

                await context.Send<CreateAgencyFeeTransactionOnTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);

                await context.Send<CreateTaxTransactionOnTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);

                await context.Send<CreatePolicyTransactionOnTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);

                await context.Send<CreateScheduleOnTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);

                await context.Send<AddLegalCarePolicyToTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);

                await context.Send<AddAccountingContactToTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);

                await context.Send<AddNotesToTam>(m =>
                {
                    m.QuoteReference = Data.PolicyReference;
                }).ConfigureAwait(false);
               
            }
            catch (Exception ex)
            {
                AppInsightLog.LogTamError(ex, $"[Handle(PolicyCreatedOnTam)]", Data.PolicyReference);
                var website = CloudConfigurationManager.GetSetting("website.address");

                await Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Error with policy created on TAM",
                    $"Policy: {Data.PolicyReference} was not created from {website}", null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);
                
                throw new Exception($"Handle.PolicyCreatedOnTam({Data.PolicyReference} Message{ex.Message}");
            }
        }

        public async Task Handle(CertificateCreated message, IMessageHandlerContext context)
        {
            Console.WriteLine($"Certificate Created for saga with id: {Data.PolicyReference}");
            await context.Send<NewPolicyEmail>(m =>
            {
                m.QuoteReference = Data.PolicyReference;
            }).ConfigureAwait(false);
          
            await context.Send<CreateCertificateOnTam>(m =>
            {
                m.QuoteReference = Data.PolicyReference;
            }).ConfigureAwait(false);
        }
    }


    // ReSharper disable once InconsistentNaming
    internal class ElevenMonthsSincePayment
    {
    }

    // ReSharper disable once InconsistentNaming
    internal class OneWeekSinceStarted
    {
    }

    internal class PolicySagaData : IContainSagaData
    {
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }

        public string PolicyReference { get; set; }
        public bool PayByInstallments { get; set; }
        public string AgentCode { get; set; }
        public bool Renew { get; set; }
    }
}

