﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using ItOps.Commands.Tam;
using ItOps.Endpoint.conneXionsWS;
using ItOps.Endpoint.Services.Emailer;
using LeisureInsure.DB.Models;
using ItOps.Endpoint.Services;
using LeisureInsure;
using LeisureInsure.Services.Services.Utilities;
using Microsoft.Azure;
using LeisureInsure.DB;
using Attachment = ItOps.Endpoint.conneXionsWS.Attachment;
using Contact = ItOps.Endpoint.conneXionsWS.Contact;
using Customer = ItOps.Endpoint.conneXionsWS.Customer;
using Schedule = ItOps.Endpoint.conneXionsWS.Schedule;
using LeisureInsure.Services.Utilities;
using LeisureInsure.DB.Logging;
using Newtonsoft.Json;


namespace ItOps.Endpoint.Tam
{
    public class TamClient : IInterfaceWithTam
    {
        private const string UserName = "Steve Cattell";
        private const string Key = "e1fddec4-1311-4d05-9527-916ca43fc016";

        //private readonly ILiRepository _repository;
        private readonly IRepository _rep;
        private readonly IClock _clock;
        private readonly ICreateTamNames _tamName;
        public ISendEmails Email { get; set; }
        public string _website { get; set; }

        public TamClient(
            //ILiRepository repository,
            IRepository rep,
            IClock clock,
            ICreateTamNames tamName)
        {
            //_repository = repository;
            _rep = rep;
            _clock = clock;
            _tamName = tamName;
            _website = CloudConfigurationManager.GetSetting("website.address");
        }

        private conneXionsWS.conneXionsWS CreateInstance()
        {
            var instance = new conneXionsWS.conneXionsWS();
            instance.Url = CloudConfigurationManager.GetSetting("tam.webservice.url");
            return instance;
        }

        public string AddTamCustomer(string quoteReference)
        {
            var quote = _rep.GetQuotebyQuoteRef(quoteReference);
            try
            {
                Trace.WriteLine($"{System.Reflection.MethodBase.GetCurrentMethod().Name}-->");
                var savedContact = _rep.GetCustomer(quote.CustomerFK);
                var savedAddress = _rep.GetAddress(quote.AddressFK ?? 0);
                var occupation = _rep.GetPolicy(quote.PolicyFK ?? 0);

                var tamCustomer = new Customer();
                var customerWebserviceRequest = new CustomerWS
                {
                    customer = tamCustomer,
                };
                var clientRequest = new ClientRequest
                {
                    Username = UserName,
                    Key = Key,
                    customerWS = customerWebserviceRequest,
                };

                if (string.IsNullOrEmpty(quote.TamClientCode))
                {
                    clientRequest.CRUd = "C";
                }
                else
                {
                    clientRequest.CRUd = "U";
                }


                if (!string.IsNullOrEmpty(quote.BrokerName))
                {
                    quote.BrokerName = quote.BrokerName.ToUpper();
                    tamCustomer.BrokerName = quote.BrokerName.ToUpper();
                    tamCustomer.Broker = quote.BrokerName.ToUpper();
                    tamCustomer.MarketingPlan = "AGE";
                }
                else
                {
                    tamCustomer.MarketingPlan = "DIR";
                    tamCustomer.BrokerName = "";
                }
                if (!string.IsNullOrEmpty(savedContact.EntityName))
                {
                    tamCustomer.CustomerType = "C";
                }
                else
                {
                    tamCustomer.CustomerType = "P";
                }
                var name = _tamName.TamNameFor(savedContact);
                if (!string.IsNullOrEmpty(savedContact.EntityName))
                {
                    tamCustomer.Name = name.Replace(" ", "");
                }
                else
                {
                    tamCustomer.Name = name;
                }

                string attn = _tamName.ForAttention(savedContact);
                tamCustomer.Heading = quote.ContactName.Truncate(20);
                tamCustomer.Attention = attn;

                if (string.IsNullOrEmpty(quote.Csr))
                    tamCustomer.CSR = "WU";
                else
                    tamCustomer.CSR = quote.Csr;

                tamCustomer.Agency = quote.LocaleId == 1 ? "1" : "3";

                tamCustomer.Branch = "1"; // TODO: Check
                tamCustomer.Note = "";
                tamCustomer.Statement = "2";
                tamCustomer.InvoicePageBreak = 3;

                tamCustomer.PhoneBusiness = "";
                tamCustomer.PhoneFax = "";
                tamCustomer.CustomerType = savedContact.EntityType;

                tamCustomer.Occupation = occupation.TamOccupation;
                tamCustomer.Street = savedAddress.Address1;
                tamCustomer.Street2 = savedAddress.Address2;
                tamCustomer.City = savedAddress.Town;
                tamCustomer.ZipCode = savedAddress.Postcode;
                tamCustomer.PhoneResidential = savedContact.Telephone;
                tamCustomer.Email = quote.ContactEmail.Truncate(50);

                var json = JsonConvert.SerializeObject(clientRequest);

                using (var webService = CreateInstance())
                {
                    var response = webService.CRUdClient(clientRequest);

                    if (!string.IsNullOrEmpty(response?.CustomerObject?.result) &&
                        response.CustomerObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        quote.TamClientCode = response.CustomerObject.customer.CustomerID;
                        _rep.saveTamClientCode(quote.QuotePK, quote.TamClientCode);

                        return quote.TamClientCode;
                    }
                    throw new ApplicationException($"Could not create tam customer: {response?.CustomerObject?.result}");
                }
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddTamCustomer", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add TAM Customer",
                        $"Policy: {quoteReference} Customer was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quote.QuoteRef} TamClient.AddTamCustomer:{errorDetails}");
            }
        }



        public string AddTamPolicy(string quoteReference, bool payByInstallments, string agentCodeOverride)
        {
            var quote = _rep.GetQuotebyQuoteRef(quoteReference);
            try
            {
                Trace.WriteLine($"{System.Reflection.MethodBase.GetCurrentMethod().Name}-->");
                var payOption = 0;
                var policy = new Policy();
                var policyWebserviceRequest = new PolicyWS
                {
                    policy = policy,
                };

                var request = new ClientRequest()
                {
                    policyWS = policyWebserviceRequest,
                    Username = UserName,
                    Key = Key,
                    CRUd = "C",
                };

                var premium = quote.NetPremium.ToString(CultureInfo.InvariantCulture); // Net before tax and fees

                if (!String.IsNullOrEmpty(quote.CodeName))
                {
                    var code = _rep.GetCode(quote.CodeName);
                    if (code.CodeType == "PROD")
                    {
                        policy.Producer1 = quote.CodeName;
                        policy.CommissionPct1 = code.Commission.ToString();
                        //we dont need to add this on
                        //var divider = (decimal)code.Commission / 100;
                        //var amount = quote.NetPremium * divider;
                        //var premPlusComm = quote.NetPremium + amount;
                        //premium = premPlusComm.ToString(CultureInfo.InvariantCulture);
                    }
                }

                policy.ClientID = quote.TamClientCode;
                policy.Agency = quote.LocaleId == 1 ? "1" : "3";

                //Constants
                //TODO: CHECK POLICY SLOT
                policy.PolicySlot = "1";
                policy.Status = "NEW";

                if (quote.Renewal > 0)
                {
                    policy.PolicySlot = quote.Renewal.ToString();
                    policy.Status = "REN";
                }
                policy.Branch = "1";
                policy.Department = "1";


                if (quote.IsAgent)
                {
                    string actualAgentCode = "";

                    if (!string.IsNullOrEmpty(quote.TamBrokerCode))
                        actualAgentCode = quote.TamBrokerCode.ToUpper();

                    if (!string.IsNullOrEmpty(agentCodeOverride))
                        actualAgentCode = actualAgentCode.ToUpper();

                    policy.BillMode = "B";
                    policy.PayMode = "S";
                    policy.Broker = actualAgentCode.ToUpper();
                    policy.BrokerName = "";
                    policy.OtherInterest = actualAgentCode.ToUpper();
                    policy.BrokerCommissionAmt = quote.BrokerCommission.ToString();
                    policy.BrokerCommissionPct = quote.CommissionPercentage.ToString();
                }
                else
                {
                    policy.BillMode = "A";
                    if (payByInstallments)
                    {
                        policy.PayMode = "F";
                        payOption = 8;
                    }
                    else
                    {
                        payOption = 1;
                        policy.PayMode = "B";
                    }
                }

                if (!string.IsNullOrWhiteSpace(quote.Csr))
                {
                    policy.CSR = quote.Csr;
                }
                else
                {
                    policy.CSR = "WU";
                }
                policy.CommissionType1 = "2";
                policy.AgencyCommissionType = "2";
                policy.DateWritten = $"{quote.DatePurchased:yyyyMMdd}";

                var commisionText = CloudConfigurationManager.GetSetting("leisureinsure.commission");
                var commission = Convert.ToDecimal(commisionText);
                var commissionAmount = quote.NetPremium * commission / 100;
                policy.AgencyCommissionPct = CloudConfigurationManager.GetSetting("leisureinsure.commission");
                policy.AgencyCommissionAmt = commissionAmount.ToString();
                //new
                policy.ICO = quote.LocaleId == 1 ? "CAT" : "DOG";
                policy.BCO = quote.LocaleId == 1 ? "CAT" : "DOG";
                policy.MasterCompanyCode = quote.LocaleId == 1 ? "CAT" : "DOG";
                policy.PolicyType = "WEB3";
                policy.PolicyNumber = quote.QuoteRef;

                policy.Premium = premium;
                policy.EffDate = $"{quote.DateFrom:yyyyMMdd}";
                policy.ExpDate = $"{quote.DateTo:yyyyMMdd}";

                using (var webService = CreateInstance())
                {
                    var response = webService.CRUdPolicy(request);

                    if (!string.IsNullOrEmpty(response?.PolicyObject?.result) && response.PolicyObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        quote.TAMPolicyCode = response.PolicyObject.policy.PolicyIndex;
                        _rep.saveTamPolicyCode(quote.QuotePK, quote.TAMPolicyCode);

                        return quote.TAMPolicyCode;
                    }

                    throw new ApplicationException($"Did not create tam policy - {response?.PolicyObject?.result}");
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddTamPolicy", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add TAM Policy",
                        $"Policy: {quoteReference} was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quote.QuoteRef} TamClient.AddTamPolicy:{errorDetails}");
            }
        }

        public string AddTamLegalPolicy(string quoteReference, bool payByInstallments, string agentCodeOverride)
        {

            try
            {
                Trace.WriteLine($"{System.Reflection.MethodBase.GetCurrentMethod().Name}-->");
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);

                if (!quote.LegalFeesAdded || quote.NetLegal == 0)
                {
                    return "NO LEGAL";
                }

                var policy = new Policy();
                var policyWebserviceRequest = new PolicyWS
                {
                    policy = policy,
                };
                var request = new ClientRequest()
                {
                    policyWS = policyWebserviceRequest,
                    Username = UserName,
                    Key = Key,
                    CRUd = "C",
                };

                policy.ClientID = quote.TamClientCode;
                policy.Agency = quote.LocaleId == 1 ? "1" : "3";

                //Constants
                //TODO: CHECK POLICY SLOT
                policy.PolicySlot = "2";
                policy.Status = "NEW";

                if (quote.PolicySlot > 0)
                {
                    int? polSlot = quote.PolicySlot + 1;
                    policy.PolicySlot = polSlot.ToString();
                    policy.Status = "REN";
                }
                policy.Branch = "1";
                policy.Department = "1";

                var premium = (quote.NetPremium * 6) / 100;
                var comAmount = (decimal)0;

                var actualAgentCode = agentCodeOverride;
                if (string.IsNullOrEmpty(actualAgentCode))
                {
                    if (!string.IsNullOrEmpty(quote.TamBrokerCode))
                    {
                        actualAgentCode = quote.TamBrokerCode.ToUpper();
                    }

                }

                if (quote.IsAgent)
                {

                    actualAgentCode = actualAgentCode?.ToUpper();
                    policy.BillMode = "B";
                    policy.PayMode = "S";
                    policy.Broker = actualAgentCode;
                    policy.BrokerName = "";
                    policy.OtherInterest = actualAgentCode;

                    policy.CommissionType1 = "2";
                    policy.AgencyCommissionType = "2";
                }
                else
                {
                    policy.BillMode = "A";
                    if (payByInstallments)
                    {
                        policy.PayMode = "F";
                    }
                    else
                    {
                        policy.PayMode = "B";
                    }
                    //policy.AgencyCommissionPct = "0";
                    //policy.AgencyCommissionAmt = "0";
                }
                comAmount = (premium * (decimal)51.5) / 100;
                policy.CommissionType1 = "2";
                policy.AgencyCommissionType = "2";
                policy.AgencyCommissionAmt = comAmount.ToString();
                policy.AgencyCommissionPct = "51.5";
                if (!string.IsNullOrWhiteSpace(quote.Csr))
                {
                    policy.CSR = quote.Csr;
                }


                policy.DateWritten = $"{quote.DatePurchased:yyyyMMdd}";

                policy.CSR = "WU";

                //new
                policy.ICO = "CLE";
                policy.BCO = "CLE";
                policy.MasterCompanyCode = "CLE";
                policy.PolicyType = "LE";
                policy.PolicyNumber = quote.QuoteRef + "-LE";

                //var premium = quote.LegalFees - (quote.LegalFees * quote.TaxRate)/100;

                policy.Premium = premium.ToString(CultureInfo.InvariantCulture);
                policy.EffDate = $"{quote.DateFrom:yyyyMMdd}";
                policy.ExpDate = $"{quote.DateTo:yyyyMMdd}";

                using (var webService = CreateInstance())
                {
                    var response = webService.CRUdPolicy(request);

                    if (!string.IsNullOrEmpty(response?.PolicyObject?.result)
                        && response.PolicyObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        quote.TAMLegalPolicyCode = response.PolicyObject.policy.PolicyIndex;
                        _rep.saveTamLegalPolicyCode(quote.QuotePK, quote.TAMLegalPolicyCode);

                        return quote.TAMLegalPolicyCode;
                    }
                    throw new ApplicationException($"Did not create tam Legal policy - {response?.PolicyObject?.result}");
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddTamLegalPolicy", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add TAM Legal Policy",
                        $"Policy: {quoteReference} was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddTamPolicy:{errorDetails}");
            }

        }


        private void AddGeneralTransactionFields(GeneralTrans transaction, Quotes quote)
        {
            try
            {
                transaction.PrintInvoice = true;
                transaction.BrokerAmt = 0;
                transaction.ActgMessageAllowed = false;
                transaction.ActgNoteAllowed = false;
                transaction.DefaultPremAmount = false;
                transaction.ActgMonth = $"{quote.DatePurchased:yyyyMM}"; // yyyy + MM
                transaction.PolicyIndex = quote.TAMPolicyCode;
                transaction.CustomerID = quote.TamClientCode;
                transaction.PolicyPremium = 0;
                transaction.TransCoAmt = 0;
                transaction.RecordNumber = 0;
                transaction.TransDate = String.Format("{0:MM/dd/yyyy}", quote.DatePurchased);
                transaction.TransEffDate = $"{quote.DateFrom:MM/dd/yyyy}";
                if (quote.IsAgent)
                {
                    DateTime d = new DateTime();
                    d = quote.DatePurchased ?? DateTime.Now;

                    transaction.TransDueDate = $"{d.AddDays(30):MM/dd/yyyy}";
                }
                else
                {
                    transaction.TransDueDate = $"{quote.DatePurchased:MM/dd/yyyy}";
                }
            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddGeneralTransactionFields", quote.QuoteRef);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add TAM General Transaction Fields",
                        $"Policy: {quote.QuoteRef} was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quote.QuoteRef} TamClient.AddTamPolicy:{errorDetails}");
            }
        }

        public void AddPolicyTransaction(string quoteReference)
        {
            try
            {
                //var quote = _repository.Q<Quote>().First(x => x.QuoteReference == quoteReference);
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);
                var transaction = new GeneralTrans();
                transaction.ProvincialTaxType = EnumProvincialTaxType.NO_TAX;
                AddGeneralTransactionFields(transaction, quote);

                transaction.TransType = "NEW";
                transaction.Description = "New Business";

                if (quote.LocaleId == 1)
                {
                    if (quote.Postcode != null && (quote.Postcode.StartsWith("JE") ||
                        quote.Postcode.StartsWith("GY") ||
                        quote.Postcode.StartsWith("IM")))
                    {
                        transaction.ProvincialTaxType = EnumProvincialTaxType.NO_TAX;
                    }
                    else
                    {
                        transaction.ProvincialTaxType = EnumProvincialTaxType.TAX_SEPARATE_FROM_PREMIUM;
                    }
                }
                else
                {
                    transaction.ProvincialTaxType = EnumProvincialTaxType.NO_TAX;
                }
                transaction.Amount = quote.NetPremium.ToString(CultureInfo.InvariantCulture);

                transaction.PolicyIndex = quote.TAMPolicyCode;
                transaction.CustomerID = quote.TamClientCode;
                DateTime d = new DateTime();
                d = quote.DateFrom ?? DateTime.Now;
                transaction.TransEffDate = d.ToString("MM/dd/yyyy");
                d = quote.DatePurchased ?? DateTime.Now;
                if (quote.IsAgent)
                {
                    transaction.TransDueDate = d.AddDays(30).ToString("MM/dd/yyyy");

                    //_clock.LocalTime.AddDays(30).ToString("MM/dd/yyyy");
                }
                else
                {
                    transaction.TransDueDate = d.ToString("MM/dd/yyyy");
                    //_clock.LocalTime.ToString("MM/dd/yyyy");
                }

                AddGeneralTamTransaction(transaction, quote.QuotePK);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddPolicyTransaction", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add TAM Crud Transaction",
                        $"Policy crud transaction: {quoteReference} was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddTamPolicy:{errorDetails}");
            }
        }

        public void AddLegalFeeTransaction(string quoteReference)
        {
            try
            {
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);

                if (!quote.LegalFeesAdded)
                {
                    return;
                }
                var taxtype = EnumProvincialTaxType.TAX_SEPARATE_FROM_PREMIUM;
                if (quote.TaxRate == 0)
                {
                    taxtype = EnumProvincialTaxType.NO_TAX;

                }
                var transaction = new GeneralTrans
                {

                    ProvincialTaxType = taxtype

                };
                AddGeneralTransactionFields(transaction, quote);
                transaction.PolicyIndex = quote.TAMLegalPolicyCode;
                transaction.TransType = "NEW";
                transaction.Description = "New business";
                transaction.TransICOCode = "CLE";
                var premium = (quote.NetPremium * 6) / 100;
                //premium -= (quote.LegalFees * quote.TaxRate);
                transaction.Amount = premium.ToString(CultureInfo.InvariantCulture);

                AddGeneralTamTransaction(transaction, quote.QuotePK);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddLegalFeeTransaction", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add Legal Fee Crud Transaction to TAM",
                        $"Policy: {quoteReference} Legal Fee Crud Transaction was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddTamPolicy:{errorDetails}");                
            }
        }

        public void AddCardChargeTransaction(string quoteReference)
        {
            try
            {
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);


                if (quote.IsAgent == true)
                {
                    return;
                }

                if (quote.CardCharge < 1)
                {

                    quote.CardCharge = quote.LocaleId;
                }
                var transaction = new GeneralTrans
                {
                    ProvincialTaxType = EnumProvincialTaxType.NO_TAX
                };
                AddGeneralTransactionFields(transaction, quote);

                transaction.TransType = "CHA";
                transaction.Description = "Card Payment Fee";
                decimal cardCharge = quote.CardCharge ?? 0;

                transaction.Amount = cardCharge.ToString(CultureInfo.InvariantCulture);

                AddGeneralTamTransaction(transaction, quote.QuotePK);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddCardChargeTransaction", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add card charge transaction to TAM",
                        $"Policy: {quoteReference} card charge transaction was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddCardChargeTransaction:{errorDetails}");
            }
        }

        public void AddAgencyFeeTransaction(string quoteReference)
        {
            try
            {
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);

                if (quote.Fee == 0)
                {
                    return;
                }

                var transaction = new GeneralTrans();
                transaction.ProvincialTaxType = EnumProvincialTaxType.NO_TAX;
                AddGeneralTransactionFields(transaction, quote);

                transaction.TransType = "AFE";
                transaction.Description = "Agency Fee";
                transaction.Amount = (quote.Fee).ToString(CultureInfo.InvariantCulture);

                transaction.PolicyIndex = quote.TAMPolicyCode;
                transaction.CustomerID = quote.TamClientCode;
                DateTime d = quote.DateFrom ?? DateTime.Now;
                transaction.TransEffDate = d.ToString("MM/dd/yyyy");
                d = quote.DatePurchased ?? DateTime.Now;


                if (quote.IsAgent)
                {
                    transaction.TransDueDate = d.AddDays(30).ToString("MM/dd/yyyy");
                }
                else
                {
                    transaction.TransDueDate = d.ToString("MM/dd/yyyy");
                }

                AddGeneralTamTransaction(transaction, quote.QuotePK);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddAgencyFeeTransaction", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add agency fee transaction to TAM",
                        $"Policy: {quoteReference} Agency Fee transaction was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddAgencyFeeTransaction:{errorDetails}");

            }
        }

        public void AddTaxTransaction(string quoteReference)
        {
            try
            {
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);

                if (quote.LocaleId == 1)
                {
                    return;
                }

                var transaction = new GeneralTrans();
                transaction.ProvincialTaxType = EnumProvincialTaxType.NO_TAX;
                AddGeneralTransactionFields(transaction, quote);


                transaction.TransType = "IGL";
                transaction.Description = "Irish Government Levy";

                transaction.Amount = (quote.TaxRate * quote.NetPremium).ToString();

                transaction.PolicyIndex = quote.TAMPolicyCode;
                transaction.CustomerID = quote.TamClientCode;
                DateTime d = quote.DateFrom ?? DateTime.Now;
                transaction.TransEffDate = d.ToString("MM/dd/yyyy");
                d = quote.DatePurchased ?? DateTime.Now;



                if (quote.IsAgent)
                {
                    transaction.TransDueDate = d.AddDays(30).ToString("MM/dd/yyyy");
                }
                else
                {
                    transaction.TransDueDate = d.ToString("MM/dd/yyyy");
                }

                AddGeneralTamTransaction(transaction, quote.QuotePK);
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddTamLegalPolicy", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add TAM Legal Policy",
                        $"Policy: {quoteReference} was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddTamPolicy:{errorDetails}");

            }
        }

        private void AddGeneralTamTransaction(GeneralTrans transaction, int quotePK)
        {
            var quote = _rep.GetQuotebyQuotePK(quotePK);
            try
            {
                transaction.PrintInvoice = true;
                transaction.BrokerAmt = 0;
                transaction.ActgMessageAllowed = false;
                transaction.ActgNoteAllowed = false;
                transaction.DefaultPremAmount = false;
                transaction.ActgMonth = String.Format("{0:yyyyMM}", DateTime.Now); // yyyy + MM

                transaction.PolicyPremium = 0;
                transaction.TransCoAmt = 0;
                transaction.RecordNumber = 0;


                var transactionWrapper = new TransactionWS()
                {
                    transaction = transaction,
                };
                var request = new ClientRequest()
                {
                    transactionWS = transactionWrapper,
                    Username = UserName,
                    Key = Key,
                    CRUd = "C",
                };

                using (var webservice = CreateInstance())
                {
                    var response = webservice.CrudTransaction(request);

                    if (string.IsNullOrEmpty(response?.TransactionObject?.result) ||
                        !response.TransactionObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        throw new ApplicationException($"Could not add tam transaction: {response?.TransactionObject?.result}");
                    }
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddGeneralTamTransaction", quote.QuoteRef);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add general tam transaction to TAM",
                        $"Policy: {quote.QuoteRef} General TAM transaction was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quote.QuoteRef} TamClient.AddAgencyFeeTransaction:{errorDetails}");

            }
        }

        public void AddTamCustomDec(string quoteReference)
        {
            try
            {
                var fieldsRaw = new List<Field>();
                var schedules = new List<Schedule>();
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);

                var savedPpolicy = _rep.getPolicies(quote.PolicyFK ?? 0);

                var customdec = new CustomDec();


                var screens = new List<Screen>();

                customdec.PolicyIndex = quote.TAMPolicyCode;
                customdec.TAMUserName = UserName;
                customdec.TAMPassword = Key;
                customdec.TAMCentralAgency = "1";
                var fieldsx = new List<DecField>();
                var scr = new List<Screen>();

                var decFields = CreateCustomDec(quote);

                var fldLoop = 1;
                var rowcount = 0;

                var generatedSchedules = _rep.getTamSchedule(quote.QuotePK);

                int rows = generatedSchedules.Count();
                var des = "";
                foreach (var generatedSchedule in generatedSchedules)
                {
                    if (fldLoop != 10)
                    {
                        des = "Description";
                    }
                    else
                    {
                        des = "Descrition";
                    }
                    var field = new Field
                    {
                        Description = "Key" + fldLoop,
                        Value = generatedSchedule.Key.ToString()
                    };
                    fieldsRaw.Add(field);

                    field = new Field
                    {
                        Description = "Section" + fldLoop,
                        Value = generatedSchedule.Section.ToString()
                    };
                    fieldsRaw.Add(field);

                    field = new Field
                    {
                        Description = des + fldLoop,
                        Value = generatedSchedule.Description
                    };
                    fieldsRaw.Add(field);

                    field = new Field
                    {
                        Description = "Insured" + fldLoop,
                        Value = generatedSchedule.Insured.ToString()
                    };
                    fieldsRaw.Add(field);

                    field = new Field
                    {
                        Description = "Rate" + fldLoop,
                        Value = generatedSchedule.Rate.ToString()
                    };
                    fieldsRaw.Add(field);

                    field = new Field
                    {
                        Description = "Premium" + fldLoop,
                        Value = generatedSchedule.Premium.ToString()
                    };
                    fieldsRaw.Add(field);

                    field = new Field
                    {
                        Description = "Excess" + fldLoop,
                        Value = generatedSchedule.Excess.ToString()
                    };
                    fieldsRaw.Add(field);

                    field = new Field
                    {
                        Description = "Spec" + fldLoop,
                        Value = generatedSchedule.Spec
                    };
                    fieldsRaw.Add(field);

                    fldLoop++;
                    rowcount++;
                    if (fldLoop == 11 || rowcount == rows)
                    {
                        var schedule = new Schedule { Fields = fieldsRaw.ToArray() };
                        schedules.Add(schedule);
                        fldLoop = 1;
                        //rowcount = 1;
                        fieldsRaw.Clear();
                    }
                }

                var sc = new Screen();

                screens.Add(sc);

                screens.Add(sc);
                customdec.Screens = screens.ToArray();
                customdec.Screens[1].Schedules = schedules.ToArray();


                var customDecWrapper = new CustomDecWS()
                {
                    customdec = customdec,

                    decfields = decFields
                };
                var request = new ClientRequest()
                {
                    customDecWS = customDecWrapper,
                    Username = UserName,
                    Key = Key,
                    CRUd = "C",
                    Schedulescreen = 1,
                };

                using (var webservice = CreateInstance())
                {
                    var response = webservice.CRUdCustomDec(request);

                    if (string.IsNullOrEmpty(response?.CustomDecObject?.result) ||
                        !response.CustomDecObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        throw new ApplicationException($"Could not add custom dec - {response?.CustomDecObject?.result}");
                    }
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddTamCustomDec", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add customer declaration to TAM",
                        $"Policy: {quoteReference} customer declaration was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddTamCustomDec:{errorDetails}");
            }
        }

        private DecField[] CreateCustomDec(Quotes quote)
        {
            try
            {

                var fields = new List<DecField>();

                DateTime startDateDate = quote.DateFrom ?? DateTime.Now;
                var startDate = "";
                DateTime DateTo = quote.DateTo ?? DateTime.Now;
                var endDate = DateTo.ToShortDateString();
                //var startTime = quote.DateFrom.ToShortTimeString();
                var endTime = DateTo.ToShortTimeString();
                DateTime today = DateTime.Today;

                //var startTime = "";

                if (startDateDate.Date == today.Date)
                {
                    startDate = $"{quote.DateFrom:dd/MM/yyyy HH:mm}";
                }
                else
                {
                    startDate = $"{quote.DateFrom:dd/MM/yyyy}" + " 00:00";
                }

                var pageOne = _rep.getTamPageOne(quote.QuotePK);

                var customDec = new TamCustomDec
                {
                    QuoteRef = quote.QuotePK.ToString(CultureInfo.InvariantCulture),
                    PolicyPK = quote.PolicyFK,
                    TradingAs = pageOne.TradingAs,
                    StartDate = quote.DateFrom,
                    EndDate = quote.DateTo,
                    BihaNumber = pageOne.BIHA,
                    LiFee = quote.Fee,
                    TotalPremium = quote.NetPremium,
                    POL_IDX = quote.TAMPolicyCode,
                    BrokerFee = quote.BrokerCommission,
                    CustomerFK = quote.CustomerFK,
                    Business1 = pageOne.Occupation,
                    Business2 = "",
                    CustomerRef = quote.TamClientCode,
                    Description = "",
                    Employees = "",
                    ERN = pageOne.ErnNumber,
                    EventEndTime = "",
                    EventName = pageOne.EventName,
                    EventStartTime = "",
                    NightClub = "",
                    NoEvents = pageOne.NoEvents,
                    NoVisitors = pageOne.NoVisitors,
                    PostCode1 = "",
                    PostCode2 = "",
                    RiskAddress1 = pageOne.Address,
                    RiskAddress2 = "",
                    Showman = pageOne.Showman,
                    Territory = pageOne.Territory,
                    TYPE = "Web3",
                    Volunteers = ""


                };

                foreach (var propertyInfo in customDec.GetType().GetProperties())
                {
                    string propertyValue = null;
                    var propertyName = propertyInfo.Name;
                    if (propertyInfo.GetValue(customDec) != null)
                    {
                        propertyValue = propertyInfo.GetValue(customDec).ToString();
                    }

                    var decField = new DecField
                    {
                        Description = propertyName,
                        FieldValue = propertyValue,
                        Screen = 0
                    };

                    fields.Add(decField);
                }

                IList<LeisureInsure.DB.Models.TamTotals> totals = _rep.getTamScheduleTotals(quote.QuotePK).ToList();

                foreach (LeisureInsure.DB.Models.TamTotals s in totals)
                {

                    foreach (var propertyInfo in s.GetType().GetProperties())
                    {
                        string propertyValue = null;
                        var propertyName = s.FieldPrefix + propertyInfo.Name;
                        if (propertyInfo.GetValue(s) != null)
                        {
                            propertyValue = propertyInfo.GetValue(s).ToString();
                        }
                        if (propertyInfo.Name != "FieldPrefix")
                        {
                            var decField = new DecField
                            {
                                Description = propertyName,
                                FieldValue = propertyValue,
                                Screen = 2
                            };

                            fields.Add(decField);
                        }
                    }
                }


                return fields.ToArray();
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.CreateCustomDec", quote.QuoteRef);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to create custom declaration",
                        $"Policy: {quote.QuoteRef} custom declaration was not created on {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quote.QuoteRef} TamClient.CreateCustomDec:{errorDetails}");


            }
        }

        public void AddTamAttachment(string quoteReference)
        {
            try
            {
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);

                var attachment = new Attachment();

                attachment.EntityKey = "C";
                attachment.PolicyIDX = quote.TAMPolicyCode;
                attachment.EntityID = quote.TAMPolicyCode.Substring(0, 7);
                attachment.AttachDate = $"{quote.DatePurchased:yyyyMMdd}";
                if (!string.IsNullOrEmpty(quote.Csr))
                {
                    attachment.AttachedBy = quote.Csr;
                    attachment.CSR = quote.Csr;
                }
                else
                {
                    attachment.AttachedBy = "WU";
                    attachment.CSR = "WU";


                }

                attachment.PolicyType = "Web3";

                attachment.AttachmentType = "5";
                attachment.Description = "Web Attachment";
                attachment.Category = "DOCUMENT";
                attachment.UserDate = $"{quote.DatePurchased:yyyyMMdd}";


                var attachmentWrapper = new AttachmentWS()
                {
                    attachment = attachment,
                    fileinfocategory = "WEB DOCUMENT",
                    fileinfodescription = "Certificate of Insurance",
                    filename = $"{CloudConfigurationManager.GetSetting(Constants.Configuration.WebsiteBaseAddress)}/certificate/{quote.QuoteRef}/{quote.PWord}",
                };
                var request = new ClientRequest()
                {
                    attachmentWS = attachmentWrapper,
                    Username = UserName,
                    Key = Key,
                    CRUd = "C",

                };

                using (var webservice = CreateInstance())
                {
                    var response = webservice.CrudUrlAttachment(request);

                    if (string.IsNullOrEmpty(response?.AttachmentObject?.result) ||
                        !response.AttachmentObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        throw new ApplicationException($"Could not add attachment to TAM - {response?.AttachmentObject?.result}");
                    }
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddTamAttachment", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to create certificate in TAM",
                        $"Policy: {quoteReference} Certificate was not created in TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddTamAttachment:{errorDetails}");
            }
        }

        public void AddMailMessage(string quoteReference, string eml)
        {
            try
            {
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);

                var attachment = new Attachment();

                attachment.EntityKey = "C";
                attachment.PolicyIDX = quote.TAMPolicyCode;
                attachment.EntityID = quote.TAMPolicyCode.Substring(0, 7);
                attachment.AttachDate = $"{quote.DatePurchased:yyyyMMdd}";

                if (string.IsNullOrEmpty(quote.Csr))
                {
                    attachment.AttachedBy = quote.Csr;
                    attachment.CSR = quote.Csr;
                }
                else
                {
                    attachment.AttachedBy = "WU";
                    attachment.CSR = "WU";
                }

                attachment.PolicyType = "Web3";

                attachment.AttachmentType = "5";
                attachment.Description = "Web Attachment";
                attachment.Category = "EMAIL";
                attachment.UserDate = $"{quote.DatePurchased:yyyyMMdd}";

                attachment.FileInfo = new FileInfoClass[1];
                attachment.FileInfo[0] = new FileInfoClass
                {
                    Description = "New Business Email",
                    Category = "EMAIL",
                    FileName = "NewBusiness.eml",
                    Data = eml,
                };

                var attachmentWrapper = new AttachmentWS
                {
                    attachment = attachment,
                };
                var request = new ClientRequest()
                {
                    attachmentWS = attachmentWrapper,
                    Username = UserName,
                    Key = Key,
                    CRUd = "C",
                };

                using (var webservice = CreateInstance())
                {
                    var response = webservice.CrudEmailAttachment(request);

                    if (string.IsNullOrEmpty(response?.AttachmentObject?.result) ||
                        !response.AttachmentObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {                       
                        throw new ApplicationException($"Could not add attachment to TAM - {response?.AttachmentObject?.result}");
                    }
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddMailMessage", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add new business email in TAM",
                        $"Policy: {quoteReference} New business email was not created in TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddMailMessage:{errorDetails}");

            }
        }

        public void UpdateAgent(BrokerInformation message)
        {
            try
            {
                var tamAgent = new Broker();
                var customerWebserviceRequest = new AgentWS
                {
                    agent = tamAgent,
                };
                var clientRequest = new ClientRequest
                {
                    Username = UserName,
                    Key = Key,
                    agentWS = customerWebserviceRequest,
                    CRUd = "U",
                };

                tamAgent.EntityID = message.TamCode;

                tamAgent.Name = message.EntityName;
                tamAgent.Attention = message.FsaNumber;
                tamAgent.Comments = message.Firstname;
                //tamAgent.Heading = message.Password;
                tamAgent.Street = message.Address1;
                tamAgent.Street2 = message.Address2;
                tamAgent.City = message.City;
                tamAgent.County = message.County;
                tamAgent.ZipCode = message.Postcode;
                tamAgent.Phone = message.Telephone;
                tamAgent.Email = message.Email;

                using (var webService = CreateInstance())
                {
                    var response = webService.CRUdAgent(clientRequest);

                    if (!string.IsNullOrEmpty(response?.AgentObject?.result) &&
                        response.AgentObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return;
                    }
                    throw new ApplicationException($"Could not update tam broker: {response?.AgentObject?.result}");
                }
            }
            catch (Exception ex)
            {
                var reference = "";
                if (message.EntityName != null)
                    reference = message.EntityName;
                else
                    reference = message.Firstname + " " + message.Lastname;

                var errorid = AppInsightLog.LogTamError(ex, "TamClient.UpdateAgent", reference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to update agent in TAM",
                        $"Policy: Agent {reference} was not updated in TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{reference} TamClient.UpdateAgent:{errorDetails}");
            }
        }

        public class TamBrokerInformation : BrokerInformation
        {
            public string Email { get; set; }
            public string TamCode { get; set; }
            public string FsaNumber { get; set; }
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string EntityName { get; set; }
            public string Telephone { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string County { get; set; }
            public string Postcode { get; set; }
            public string Location { get; set; }
            public int ContactId { get; set; }
            public bool Locked { get; set; }
        }





        public BrokerInformation GetAgent(string agentCode)
        {
            try
            {
                var tamAgent = new Broker();
                var customerWebserviceRequest = new AgentWS
                {
                    agent = tamAgent,
                };
                var clientRequest = new ClientRequest
                {
                    Username = UserName,
                    Key = Key,
                    agentWS = customerWebserviceRequest,
                    CRUd = "R",
                };

                tamAgent.EntityID = agentCode;

                using (var webService = CreateInstance())
                {
                    var response = webService.CRUdAgent(clientRequest);

                    if (!string.IsNullOrEmpty(response?.AgentObject?.result) &&
                        response.AgentObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        var agent = response.AgentObject.agent;
                        return new TamBrokerInformation
                        {
                            TamCode = agent.EntityID,
                            Email = agent.Email.Trim(),
                            Telephone = agent.Phone,
                            Postcode = agent.ZipCode,
                            City = agent.City,
                            County = agent.County,
                            Address1 = agent.Street,
                            Address2 = agent.Street2,
                            Firstname = agent.Comments,
                            FsaNumber = agent.Attention,
                            EntityName = agent.Name
                        };
                    }
                    throw new ApplicationException($"Could not read tam broker: {response?.AgentObject?.result}");
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.GetAgent", agentCode);
                var errorDetails = $"{ex.Message}";

                //Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to update agent in TAM",
                //        $"Policy: Agent {reference} was not updated in TAM from {_website} See errorID {errorid} in TamErrorLogs",
                //        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{errorDetails} TamClient.GetAgent:{agentCode}");
            }
        }

        private string GetAgentCode(string agentCode)
        {
            try
            {
                var tamAgent = new Broker();
                var codeAvailable = "AgentCodeInUse";
                var customerWebserviceRequest = new AgentWS
                {
                    agent = tamAgent,
                };
                var clientRequest = new ClientRequest
                {
                    Username = UserName,
                    Key = Key,
                    agentWS = customerWebserviceRequest,
                    CRUd = "R",
                };

                tamAgent.EntityID = agentCode;

                using (var webService = CreateInstance())
                {
                    var response = webService.CRUdAgent(clientRequest);

                    if (!string.IsNullOrEmpty(response?.AgentObject?.result) &&
                        response.AgentObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                    }
                    else
                    {
                        codeAvailable = "AgentCodeAvailable";
                    }

                    return codeAvailable;
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.GetAgentCode", agentCode);
                var errorDetails = $"{ex.Message}";

                //Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to update agent in TAM",
                //        $"Policy: Agent {reference} was not updated in TAM from {_website} See errorID {errorid} in TamErrorLogs",
                //        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{errorDetails} TamClient.GetAgentCode:{agentCode}");
            }
        }

        public string AddTamBroker(BrokerInformation message)
        {


            try
            {
                Contacts broker = _rep.getBrokerByEntityName(message.EntityName);

                var tamAgent = new Broker();
                var customerWebserviceRequest = new AgentWS
                {
                    agent = tamAgent,
                };
                var clientRequest = new ClientRequest
                {
                    Username = UserName,
                    Key = Key,
                    agentWS = customerWebserviceRequest,
                    CRUd = "C",
                };

                var entityName = _tamName.TamNameFor(broker);


                entityName = entityName.Replace(" ", "");
                if (!message.Location.Equals("1"))
                {
                    entityName = "RI" + entityName;
                }

                string brokerCode = entityName.Substring(0, 7);


                //Check if AgentCode already exists
                var checkCode = GetAgentCode(brokerCode);
                //add numbers to AgentCode (1-9) and see if those agentcodes exist
                if (checkCode != "AgentCodeAvailable")
                {
                    for (int i = 1; i < 10; i++)
                    {
                        brokerCode = entityName.Substring(0, 6) + i.ToString();
                        checkCode = GetAgentCode(brokerCode);
                        if (checkCode == "AgentCodeAvailable")
                        {
                            break;
                        }
                    }
                }
                //add numbers to AgentCode (10-99) and see if those agentcodes exist
                if (checkCode != "AgentCodeAvailable")
                {
                    for (int i = 10; i < 100; i++)
                    {
                        brokerCode = brokerCode.Substring(0, 5) + i.ToString();
                        checkCode = GetAgentCode(brokerCode);
                        if (checkCode == "AgentCodeAvailable")
                        {
                            break;
                        }
                    }

                }
                // if checkCode still == "AgentCodeAvailable"
                if (checkCode != "AgentCodeAvailable")
                {
                    return "Error";
                }
                else
                {

                }

                //tamAgent.Name = message.TamCode;
                //tamAgent.EntityID = entityName.Substring(0,7);
                tamAgent.EntityID = brokerCode;
                tamAgent.Key = "B";
                tamAgent.Name = message.EntityName;
                tamAgent.Attention = message.FsaNumber;
                //tamAgent.Comments = message.Firstname + " " + message.Lastname;
                tamAgent.Heading = "Application";
                tamAgent.Street = message.Address1;
                tamAgent.Street2 = message.Address2;
                tamAgent.City = message.City;
                tamAgent.Country = "United Kingdom";
                tamAgent.County = "Oxfordshire";
                tamAgent.ZipCode = message.Postcode;
                tamAgent.Phone = message.Telephone;
                tamAgent.Email = message.Email;
                tamAgent.Classification = "";
                tamAgent.Statement = "1";
                tamAgent.FirstItemWithBalance = "0";
                tamAgent.Balance = 0;
                tamAgent.StatementBalanceForward = 0;
                //tamAgent.PrintBudgetPlan = "False"; // ?? this is in the webservice??

                using (var webService = CreateInstance())
                {
                    var response = webService.CRUdAgent(clientRequest);

                    if (!string.IsNullOrEmpty(response?.AgentObject?.result) &&
                        response.AgentObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //var agent = _repository.Find<Agent>(message.ContactId);
                        //agent.TamClientRef = response.AgentObject.agent.EntityID;
                        //_repository.Commit();
                        return brokerCode;
                    }
                    throw new ApplicationException($"Could not create tam broker: {response?.AgentObject?.result}");
                }
            }
            catch (Exception ex)
            {

                var reference = "";
                if (message.EntityName != null)
                    reference = message.EntityName;
                else
                    reference = message.Firstname + " " + message.Lastname;

                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddTamBroker", reference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add agent to TAM",
                        $"Policy: Agent {reference} was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{reference} TamClient.AddTamBroker:{errorDetails}");
            }
        }

        public void AddAccountingContact(string quoteReference)
        {
            try
            {
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);
                Contacts customer = _rep.GetCustomer(quote.CustomerFK);
                Contacts broker = _rep.GetCustomer(quote.BrokerFK ?? 0);

                var contact = new Contact();
                var contactWebserviceRequest = new ContactWS()
                {
                    contact = contact,
                };
                var clientRequest = new ClientRequest
                {
                    Username = UserName,
                    Key = Key,
                    contactWS = contactWebserviceRequest,
                    CRUd = "C",
                };

                contact.EntityKey = "C";
                contact.EntityID = quote.TAMPolicyCode.Substring(0, 7);
                contact.EmailAddress1 = customer.Email;

                if (broker != null)
                {
                    contact.EmailAddress1 = broker.Email;
                }

                contact.Description = "Email via website";

                using (var webService = CreateInstance())
                {
                    var response = webService.CRUdContact(clientRequest);

                    if (!string.IsNullOrEmpty(response?.ContactObject?.result) &&
                        response.ContactObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return;
                    }
                    throw new ApplicationException($"Could not create tam contact: {response?.ContactObject?.result}");
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddAccountingContact", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add accounting contact to TAM",
                        $"Policy: {quoteReference} Accounting contact was not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddAccountingContact:{errorDetails}");
            }
        }

        public void AddNotes(string quoteReference)
        {
            try
            {
                var quote = _rep.GetQuotebyQuoteRef(quoteReference);
                var notes = _rep.GetNotesForTAM(quote.QuotePK);

                foreach (QuoteNotes note in notes)
                {
                    string title = "Referral Override Notes";

                    if (!string.IsNullOrEmpty(note.Note))
                    {
                        var attachment = new Attachment();

                        attachment.EntityKey = "C";
                        attachment.PolicyIDX = quote.TAMPolicyCode;
                        attachment.EntityID = quote.TAMPolicyCode.Substring(0, 7);
                        attachment.AttachDate = $"{note.DateLogged:yyyyMMdd}";

                        if (!string.IsNullOrEmpty(note.Author))
                        {
                            attachment.AttachedBy = note.Author.ToUpper();
                            attachment.CSR = note.Author.ToUpper();

                        }
                        else
                        {
                            attachment.AttachedBy = "WU";
                            attachment.CSR = "WU";
                        }

                        attachment.PolicyType = "Web3";

                        attachment.AttachmentType = "5";
                        attachment.Description = "Web Attachment";
                        attachment.Category = "DOCUMENT";
                        attachment.UserDate = $"{quote.DatePurchased:yyyyMMdd}";

                        attachment.FileInfo = new FileInfoClass[1];

                        if (!string.IsNullOrEmpty(note.Title))
                        {
                            title = note.Title;
                        }
                        attachment.FileInfo[0] = new FileInfoClass
                        {
                            Description = title, //"Referral Override Notes",
                            Category = "DOCUMENT",
                            FileName = "Notes.txt",
                            Data = note.Note,
                        };

                        var attachmentWrapper = new AttachmentWS
                        {
                            attachment = attachment,
                        };
                        var request = new ClientRequest()
                        {
                            attachmentWS = attachmentWrapper,
                            Username = UserName,
                            Key = Key,
                            CRUd = "C",
                        };

                        using (var webservice = CreateInstance())
                        {
                            var response = webservice.CrudEmailAttachment(request);

                            if (string.IsNullOrEmpty(response?.AttachmentObject?.result) ||
                                !response.AttachmentObject.result.Equals("SUCCESS", StringComparison.InvariantCultureIgnoreCase))
                            {
                                throw new ApplicationException($"Could not add attachment to TAM - {response?.AttachmentObject?.result}");
                            }
                            else
                            {

                                _rep.SetNoteTAMInTam(note.QuoteNotePK);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, "TamClient.AddNotes", quoteReference);
                var errorDetails = $"{ex.Message}";

                Email.Send("info@leisureinsure.co.uk", "info@leisureinsure.co.uk", "Failed to add notes to TAM",
                        $"Policy: {quoteReference} Notes were not added to TAM from {_website} See errorID {errorid} in TamErrorLogs",
                        null, "luke.perrin@leisureinsure.co.uk").ConfigureAwait(false);

                throw new Exception($"{quoteReference} TamClient.AddNotes:{errorDetails}");


            }
        }
    }
}