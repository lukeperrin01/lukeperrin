﻿using ItOps.Commands.Tam;

namespace ItOps.Endpoint.Tam
{
    public interface IInterfaceWithTam
    {
        string AddTamCustomer(string quoteReference);
        string AddTamPolicy(string quoteReference, bool payByInstallments, string agentCodeOverride);
        string AddTamLegalPolicy(string quoteReference, bool payByInstallments, string agentCodeOverride);
        void AddPolicyTransaction(string quoteReference);
        void AddLegalFeeTransaction(string quoteReference);
        void AddCardChargeTransaction(string quoteReference);
        void AddAgencyFeeTransaction(string quoteReference);
        void AddTaxTransaction(string quoteReference);
        void AddTamCustomDec(string quoteReference);
        void AddTamAttachment(string quoteReference);
        void AddMailMessage(string quoteReference, string eml);
        string AddTamBroker(BrokerInformation message);
        void UpdateAgent(BrokerInformation message);
        BrokerInformation GetAgent(string agentCode);
        void AddAccountingContact(string quoteReference);
        void AddNotes(string quoteReference);
    }
}
