﻿using System.Linq;
using ItOps.Endpoint.Services.Interfaces;
using ItOps.Endpoint.ViewModels;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Information;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Services
{
    class TrustPilotInformationSupplier : ISupplyTrustPilotInformation
    {
        private readonly IRepository _repository;


        public TrustPilotInformationSupplier(IRepository repository)
        {
            _repository = repository;

        }

        public void ProcessViewModel(ViewModelBase model)
        {
            var info = _repository.GetTrustPilot();
            model.TrustPilotDescription = info.Description;
            model.TrustPilotStarLink = "https://leisureinsure.blob.core.windows.net/images/Email/" + info.Stars + ".png";
        }
    }

    public class TrustPilotInformationSupplierNew : ISupplyTrustPilotInformation
    {
        private readonly IRepository _repository;

        public TrustPilotInformationSupplierNew(IRepository repository)
        {
            _repository = repository;
        }

        public void ProcessViewModel(ViewModelBase model)
        {
            var info = _repository.GetTrustPilot();
            model.TrustPilotDescription = info.Description;
            model.TrustPilotStarLink = "https://leisureinsure.blob.core.windows.net/images/Email/" + info.Stars + ".png";
        }
    }
}
