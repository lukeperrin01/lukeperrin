﻿namespace ItOps.Endpoint.Services
{
    public static class Template
    {
        public const string Quote = "Quote.cshtml";
        public const string QuoteNew = "QuoteNew.cshtml";        
        public const string BespokeQuote = "BespokeQuote.cshtml";
        public const string NewBusiness = "NewBusiness.cshtml";
        public const string NewBusinessBroker = "NewBusinessBroker.cshtml";
        public const string EmailCustomerAfterReferral = "AfterReferral.cshtml";
        public const string EmailRenewal = "Renewal.cshtml";
        public const string Layout = "_Layout.cshtml";
        public const string NewBrokerApplicaiton = "NewBrokerApplication.cshtml";
    }
}
