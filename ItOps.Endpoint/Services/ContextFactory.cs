﻿using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.DB;
namespace ItOps.Endpoint.Services
{
    public class ContextFactory
    {
        public static LiContext LiContext()
        {
            return new LiContext();
        }
        public static LeisureInsureEntities Context()
        {
            return new LeisureInsureEntities();
        }

    }
}
