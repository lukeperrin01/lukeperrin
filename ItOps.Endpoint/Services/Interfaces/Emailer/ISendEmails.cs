﻿using System.Threading.Tasks;

namespace ItOps.Endpoint.Services.Emailer
{
    public interface ISendEmails
    {
        Task Send(string from, string to, string subject, string htmlBody, string textBody,string bcc);
        Task<string> CreateEml(string @from, string to, string subject, string htmlBody, string textBody, string bcc);
        void SendSynch(string @from, string to, string subject, string htmlBody, string textBody, string bcc);
    }
}
