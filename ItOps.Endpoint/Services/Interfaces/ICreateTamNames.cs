﻿using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Services
{
    public interface ICreateTamNames
    {
        string TamNameFor(Contacts contact);
        string ForAttention(Contacts contact);
    }
}