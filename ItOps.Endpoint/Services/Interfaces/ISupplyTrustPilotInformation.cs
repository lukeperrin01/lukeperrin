﻿using ItOps.Endpoint.ViewModels;

// ReSharper disable once CheckNamespace
namespace ItOps.Endpoint.Services.Interfaces
{
    public interface ISupplyTrustPilotInformation
    {
        void ProcessViewModel(ViewModelBase model);
    }
}
