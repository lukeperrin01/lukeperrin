﻿using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Azure;
using LeisureInsure.DB.Logging;
using System;

namespace ItOps.Endpoint.Services.Emailer
{
    public class EmailSender : ISendEmails
    {
        public async Task Send(string @from, string to, string subject, string htmlBody, string textBody, string bcc = "")
        {
            var host = CloudConfigurationManager.GetSetting("email.sender.host");
            var username = CloudConfigurationManager.GetSetting("email.sender.username");
            var password = CloudConfigurationManager.GetSetting("email.sender.password");
            var credentials = new NetworkCredential(username, password);

            try
            {
                using (var mailClient = new SmtpClient(host) { Credentials = credentials })
                {
                    var message = new MailMessage();

                    if (to.Contains(";"))
                    {
                        var recipents = to.Split(';');
                        foreach (var recipent in recipents)
                        {
                            message.To.Add(new MailAddress(recipent));
                        }
                    }
                    else
                        message.To.Add(new MailAddress(to));

                    if (!string.IsNullOrEmpty(bcc))
                    {
                        if (bcc.Contains(";"))
                        {
                            var recipents = bcc.Split(';');
                            foreach (var recipent in recipents)
                            {
                                message.Bcc.Add(new MailAddress(recipent));
                            }
                        }
                        else
                            message.Bcc.Add(new MailAddress(bcc));
                    }

                    message.From = new MailAddress(@from);
                    message.Subject = subject;
                    message.Body = htmlBody;
                    message.IsBodyHtml = true;

                    if (!string.IsNullOrEmpty(textBody))
                    {
                        using (var stream = new MemoryStream())
                        using (var writer = new StreamWriter(stream))
                        {
                            writer.Write(textBody);
                            message.AlternateViews.Add(new AlternateView(stream));
                            await mailClient.SendMailAsync(message);
                            return;
                        }
                    }

                    await mailClient.SendMailAsync(message);
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"EmailSender.Send: Error sending email to: {to}");
                throw new Exception(ex.Message);
            }

        }

        public void SendSynch(string @from, string to, string subject, string htmlBody, string textBody, string bcc = "")
        {
            var host = CloudConfigurationManager.GetSetting("email.sender.host");
            var username = CloudConfigurationManager.GetSetting("email.sender.username");
            var password = CloudConfigurationManager.GetSetting("email.sender.password");
            var credentials = new NetworkCredential(username, password);


            try
            {
                using (var mailClient = new SmtpClient(host) { Credentials = credentials })
                {
                    var message = new MailMessage();

                    if (to.Contains(";"))
                    {
                        var recipents = to.Split(';');
                        foreach (var recipent in recipents)
                        {
                            message.To.Add(new MailAddress(recipent));
                        }
                    }
                    else
                        message.To.Add(new MailAddress(to));

                    if (!string.IsNullOrEmpty(bcc))
                    {
                        if (bcc.Contains(";"))
                        {
                            var recipents = bcc.Split(';');
                            foreach (var recipent in recipents)
                            {
                                message.Bcc.Add(new MailAddress(recipent));
                            }
                        }
                        else
                            message.Bcc.Add(new MailAddress(bcc));
                    }

                    message.From = new MailAddress(@from);
                    message.Subject = subject;
                    message.Body = htmlBody;
                    message.IsBodyHtml = true;

                    if (!string.IsNullOrEmpty(textBody))
                    {
                        using (var stream = new MemoryStream())
                        using (var writer = new StreamWriter(stream))
                        {
                            writer.Write(textBody);
                            message.AlternateViews.Add(new AlternateView(stream));
                            mailClient.Send(message);
                            return;
                        }
                    }

                    mailClient.Send(message);
                }
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"SendSynch Error sending email to: {to}");
                throw new Exception(ex.Message);
            }

        }

        public Task<string> CreateEml(string @from, string to, string subject, string htmlBody, string textBody, string bcc)
        {
            using (var message = new MailMessage())
            {
                message.To.Add(new MailAddress(to));
                message.From = new MailAddress(@from);
                //message.Bcc.Add(new MailAddress(bcc));
                message.Subject = subject;
                message.Body = htmlBody;
                message.IsBodyHtml = true;
                //ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                //message.AlternateViews
                if (!string.IsNullOrEmpty(textBody))
                {
                    using (var stream = new MemoryStream())
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(textBody);
                        message.AlternateViews.Add(new AlternateView(stream));
                        var eml = message.ToEml();
                        return Task.FromResult(eml);
                    }
                }

                return Task.FromResult(message.ToEml());
            }
        }
    }
}

