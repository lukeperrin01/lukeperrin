﻿using System.Text;
using ItOps.Endpoint.Tam;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.Services.Utilities;
using LeisureInsure.DB;
using System;
using System.Diagnostics;
using LeisureInsure.DB.Logging;
namespace ItOps.Endpoint.Services
{
    public class TamNameCreator : ICreateTamNames
    {
        private const string ValidCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789 ";

        public string TamNameFor(Contacts contact)
        {
            try
            {
                var name = "";

                if (!string.IsNullOrWhiteSpace(name = contact.EntityName))
                {
                    name = contact.EntityName;
                }
                else
                {
                    name = contact.FirstName + " " + contact.LastName;
                }
                name = name.Replace("&", "And");
                if(name.ToUpper().EndsWith("LTD"))
                {
                    name = name.Replace("LTD", "");
                    name = name.Replace("ltd", "");
                    name = name.Replace("Ltd", "");
                    
                }

                name = RemoveInvalidCharacters(name);
                name = name.Truncate(30);
                return name;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, $"[ItOps.Endpoint.Services.TamNameFor]");
                throw new ApplicationException($"{errorid}");
            }
        }

        public string ForAttention(Contacts contact)
        {
            try
            {
                var name = contact.EntityName;
                if (string.IsNullOrWhiteSpace(contact.EntityType))
                {
                    contact.EntityType = "Customer";
                }
                if (contact.EntityType == "Customer")
                {
                    name = contact.FirstName + " " + contact.LastName;
                }
                name = name.Replace("&", "And");
                name = RemoveInvalidCharacters(name);
                name = name.Truncate(30);
                return name;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogTamError(ex, $"[ItOps.Endpoint.Services.ForAttention]");
                throw new ApplicationException($"{errorid}");

            }
        }

        private string RemoveInvalidCharacters(string name)
        {
            var processedName = new StringBuilder();
            foreach (var character in name)
            {
                if (ValidCharacters.Contains(character.ToString()))
                {
                    processedName.Append(character);
                }
            }
            return processedName.ToString();
        }
    }
}
