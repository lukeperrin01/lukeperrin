﻿using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using ItOps.Commands.TrustPilot;
using ItOps.Endpoint.Registrations;
using LeisureInsure;
using LeisureInsure.Nsb.Configuration;
using LeisureInsure.Services.Registrations;
using Microsoft.Practices.Unity;
using Nito.AsyncEx;
using NServiceBus;
using LeisureInsure.DB;
using LeisureInsure.DB.Logging;

namespace ItOps.Endpoint
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicePointManager.DefaultConnectionLimit = 100;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.Expect100Continue = false;

            AsyncContext.Run(AsyncMain);
        }

        private static async Task AsyncMain()
        {
            var endpointName = NServiceBusConfig.EndpointName(Constants.Endpoints.ItOps);
            var container = new UnityContainer();
            Unity.RegisterServices(container);
            RegisterServices.WithUnity(container);
            //Templates.Initialise();

            //Database.SetInitializer<LiContext>(null);

            //container.RegisterType<LiContext>(new InjectionFactory(c => ItOps.Endpoint.Services.ContextFactory.LiContext()));
            //container.RegisterType<ILiRepository, DefaultRepository>(new PerResolveLifetimeManager());


            Database.SetInitializer<LeisureInsureEntities>(null);
            container.RegisterType<LeisureInsureEntities>(new InjectionFactory(c => ItOps.Endpoint.Services.ContextFactory.Context()));
            container.RegisterType<IRepository,Repository>(new PerResolveLifetimeManager());

            var endpoint = await NServiceBusConfig.ConfigureBus(container, endpointName);
            // Excess of these will be handled by the saga
            await endpoint.Send<StartTrustPilotUpdates>(m => { m.Id = 1; }).ConfigureAwait(false);
            while (true)
            {
                await Task.Delay(100);
            }
            await endpoint.Stop();
        }
    }
}
