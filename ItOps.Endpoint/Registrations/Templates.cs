﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.ViewModels;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace ItOps.Endpoint.Registrations
{
    public static class Templates
    {
        public static void Initialise()
        {
            //we may need to call this again.. GetEntryAssembly will only run once
            //var templatePath = Path.Combine(new FileInfo(Assembly.GetEntryAssembly().Location).DirectoryName, "Templates");           
            string localPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            var templatePath = Path.Combine(new FileInfo(localPath).DirectoryName, "Templates");                       

            var config = new TemplateServiceConfiguration
            {
#if DEBUG
                Debug = true,
#else
                DisableTempFileLocking = true,
#endif
                CachingProvider = new DefaultCachingProvider(),
                TemplateManager = new ResolvePathTemplateManager(new List<string> {templatePath})
            };

            Engine.Razor = RazorEngineService.Create(config);

            Engine.Razor.Compile(Template.Layout);
            Engine.Razor.Compile(Template.Quote, typeof(QuoteVm));
            Engine.Razor.Compile(Template.BespokeQuote, typeof(QuoteVm));
            //would be nice to call main repo quote and not services quote.. but this gives namespace error for LeisureInsure.DB
            //Engine.Razor.Compile(Template.QuoteNew, typeof(QuoteVmNew));
            Engine.Razor.Compile(Template.NewBusiness, typeof(NewBusinessVm));
            Engine.Razor.Compile(Template.NewBusinessBroker, typeof(NewBusinessVm));
            Engine.Razor.Compile(Template.EmailCustomerAfterReferral, typeof(QuoteVm));
            Engine.Razor.Compile(Template.EmailRenewal, typeof(QuoteVm));
            Engine.Razor.Compile(Template.NewBrokerApplicaiton, typeof(BrokerApplicationVm));
            
            Console.WriteLine("Finished compiling templates");
        }
    }
}
