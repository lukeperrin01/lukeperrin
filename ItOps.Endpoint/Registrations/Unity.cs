﻿using System;
using System.Net.Http;
using ItOps.Endpoint.Services.Emailer;
using Microsoft.Practices.Unity;
using ItOps.Endpoint.Tam;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.Services.Interfaces;
using LeisureInsure.DB;

namespace ItOps.Endpoint.Registrations
{
    static class Unity 
    {
        public static void RegisterServices(IUnityContainer container)
        {
            container.RegisterType<ISendEmails, EmailSender>();

            var client = new HttpClient();
            client.Timeout = TimeSpan.FromMinutes(5);
            container.RegisterInstance(client);
            container.RegisterType<IInterfaceWithTam, TamClient>();
            container.RegisterType<ICreateTamNames, TamNameCreator>();
            container.RegisterType<ISupplyTrustPilotInformation, TrustPilotInformationSupplier>();
        }
    }
}
