﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.ViewModels;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;


namespace ItOps.Endpoint.Email
{
    static class InitialiseQuote
    {
        public static void Initialise()
        {                     
            string localPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            var templatePath = Path.Combine(new FileInfo(localPath).DirectoryName, "Templates");

            var config = new TemplateServiceConfiguration
            {
#if DEBUG
                Debug = true,
#else
                DisableTempFileLocking = true,
#endif
                CachingProvider = new DefaultCachingProvider(),
                TemplateManager = new ResolvePathTemplateManager(new List<string> { templatePath })
            };

            Engine.Razor = RazorEngineService.Create(config);          
            //Engine.Razor.Compile(Template.Quote, typeof(QuoteVm));
            Engine.Razor.Compile(Template.BespokeQuote, typeof(QuoteVm));

        }
    }
}
