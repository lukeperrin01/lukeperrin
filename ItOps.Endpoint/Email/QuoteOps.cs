﻿using System;
using ItOps.Commands.Email;
using ItOps.Endpoint.Services;
using ItOps.Endpoint.Services.Emailer;
using ItOps.Endpoint.Services.Interfaces;
using ItOps.Endpoint.ViewModels;
using RazorEngine;
using RazorEngine.Templating;
using Microsoft.Azure;
using LeisureInsure;
using LeisureInsure.DB.Logging;
using LeisureInsure.DB;
using System.Linq;
using System.IO;
using System.Web;
using LeisureInsure.Services.Utilities;
using System.Collections.Generic;
using LeisureInsure.DB.Business;

namespace ItOps.Endpoint.Email
{
    public class QuoteOps
    {

        public ISupplyTrustPilotInformation TrustPilot { get; set; }
        public ISendEmails Email { get; set; }        

        public QuoteOps()
        {            

        }

        public void EmailQuote(EmailQuote messagequote)
        {
            InitialiseQuote.Initialise();

            try
            {


                Quotes quote;
                QuoteVm quotevm;
                var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);
                if (string.IsNullOrEmpty(from))
                {
                    from = "website@leisureinsure.co.uk";
                }

                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == messagequote.QuoteReference);
                }                

                //this Repo is a mess and needs to be split up or use LeisureInsureEntities
                using (var repo = new Repository())
                {
                    TrustPilot = new TrustPilotInformationSupplier(repo);

                    if (quote == null)
                        throw new Exception($"Cant email null quote {messagequote.QuoteReference}");

                    quotevm = new QuoteVm
                    {
                        Quote = quote,
                        BuyLink = messagequote.BaseLink + "/quote/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        Currency = quote.LocaleId == 1 ? "£" : "€",
                    };

                    TrustPilot.ProcessViewModel(quotevm);
                }
                

                var htmlBody = Engine.Razor.Run(Template.Quote, typeof(QuoteVm), quotevm);

                Email = new EmailSender();

                Email.SendSynch(from, messagequote.Email, "Your LeisureInsure quote", htmlBody, null, "info@leisureInsure.co.uk");

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[QuoteOP.EmailQuote] Error emailing quote{messagequote.QuoteReference}]", messagequote.QuoteReference);
                throw new Exception(ex.Message);
            }
        }

        public void EmailQuoteClient(EmailQuote messagequote)
        {            

            try
            {


                Quotes quote;
                QuoteVm quotevm;
                var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);
                if (string.IsNullOrEmpty(from))
                {
                    from = "website@leisureinsure.co.uk";
                }
                // ----------------------- consilidate these repos... -------------------------------
                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == messagequote.QuoteReference);
                }

                var docLinks = new List<PolicyDocuments>();              

                using (var docRepo = new PoliciesRepository())
                {
                    docLinks = docRepo.GetDocsForPolicy(quote.QuotePK,false);                    
                }

                //this Repo is a mess and needs to be split up or use LeisureInsureEntities
                using (var repo = new Repository())
                {
                    TrustPilot = new TrustPilotInformationSupplier(repo);

                    if (quote == null)
                        throw new Exception($"Cant email null quote {messagequote.QuoteReference}");

                    quotevm = new QuoteVm
                    {
                        Quote = quote,
                        BuyLink = messagequote.BaseLink + "/purchase/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        QuoteLink = messagequote.BaseLink + "/newcertificate/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        Currency = quote.LocaleId == 1 ? "£" : "€",
                    };

                    TrustPilot.ProcessViewModel(quotevm);
                }

                //if there are issues with razor we just use flat html with placeholders
                var rootpath = AppDomain.CurrentDomain.BaseDirectory;
                var headerHtml = File.ReadAllText($"{rootpath}//Templates//headerFooter.html");                
                var bodyHtml = File.ReadAllText($"{rootpath}//Templates//ClientQuote.html");

                var currency = LocaleHelper.GetCurrency(quote.LocaleId);
                var total = $"{currency}{string.Format("{0:n0}",quote.Total)}";
                var name = quote.ContactName;

                bodyHtml = bodyHtml.Replace("{{Name}}", name);
                bodyHtml = bodyHtml.Replace("{{QuoteRef}}", quote.QuoteRef);
                bodyHtml = bodyHtml.Replace("{{Total}}", total);
                bodyHtml = bodyHtml.Replace("{{QuoteLink}}", quotevm.QuoteLink);
                bodyHtml = bodyHtml.Replace("{{BuyLink}}", quotevm.BuyLink);
                headerHtml = headerHtml.Replace("{{TrustStarLink}}", quotevm.TrustPilotStarLink);
                headerHtml = headerHtml.Replace("{{TrustDescription}}", quotevm.TrustPilotDescription);
                

                

                string docHtml = "<br>";
                foreach (var doc in docLinks)
                {
                    docHtml += $"<a href='{doc.DocLink}'>{doc.DocType}</a><br>";
                }
                docHtml += "<br>";

                bodyHtml = bodyHtml.Replace("{{DocLinks}}", docHtml);

                headerHtml = headerHtml.Replace("{{Body}}", bodyHtml);                

                var Email = new EmailSender();

                Email.SendSynch(from, messagequote.Email, "Insurance Quotation from Leisureinsure LLP", headerHtml, null, "info@leisureInsure.co.uk");

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[QuoteOP.EmailQuote] Error emailing quote{messagequote.QuoteReference}]", messagequote.QuoteReference);
                throw new Exception(ex.Message);
            }
        }

        public void EmailPolicyClient(EmailQuote messagequote)
        {

            try
            {


                Quotes quote;
                QuoteVm quotevm;
                var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);
                if (string.IsNullOrEmpty(from))
                {
                    from = "website@leisureinsure.co.uk";
                }
                // ----------------------- consilidate these repos... -------------------------------
                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == messagequote.QuoteReference);
                }

                var docLinks = new List<PolicyDocuments>();

                using (var docRepo = new PoliciesRepository())
                {
                    docLinks = docRepo.GetDocsForPolicy(quote.QuotePK, false);
                }

                //this Repo is a mess and needs to be split up or use LeisureInsureEntities
                using (var repo = new Repository())
                {
                    TrustPilot = new TrustPilotInformationSupplier(repo);

                    if (quote == null)
                        throw new Exception($"Cant email null quote {messagequote.QuoteReference}");

                    quotevm = new QuoteVm
                    {
                        Quote = quote,
                        BuyLink = messagequote.BaseLink + "/purchase/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        QuoteLink = messagequote.BaseLink + "/newcertificate/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        Currency = quote.LocaleId == 1 ? "£" : "€",
                    };

                    TrustPilot.ProcessViewModel(quotevm);
                }

                //if there are issues with razor we just use flat html with placeholders
                var rootpath = AppDomain.CurrentDomain.BaseDirectory;
                var headerHtml = File.ReadAllText($"{rootpath}//Templates//headerFooter.html");
                var bodyHtml = File.ReadAllText($"{rootpath}//Templates//ClientPolicy.html");

                var currency = LocaleHelper.GetCurrency(quote.LocaleId);
                var total = $"{currency}{string.Format("{0:n0}", quote.Total)}";
                var name = quote.ContactName;

                bodyHtml = bodyHtml.Replace("{{Name}}", name);
                bodyHtml = bodyHtml.Replace("{{QuoteRef}}", quote.QuoteRef);                
                bodyHtml = bodyHtml.Replace("{{BriefLink}}", quotevm.BuyLink);

                headerHtml = headerHtml.Replace("{{TrustStarLink}}", quotevm.TrustPilotStarLink);
                headerHtml = headerHtml.Replace("{{TrustDescription}}", quotevm.TrustPilotDescription);




                string docHtml = "<br>";
                foreach (var doc in docLinks)
                {
                    docHtml += $"<a href='{doc.DocLink}'>{doc.DocType}</a><br>";
                }
                docHtml += "<br>";

                bodyHtml = bodyHtml.Replace("{{DocLinks}}", docHtml);

                headerHtml = headerHtml.Replace("{{Body}}", bodyHtml);

                var Email = new EmailSender();

                Email.SendSynch(from, messagequote.Email, "Your insurance documents from Leisureinsure LLP", headerHtml, null, "info@leisureInsure.co.uk");

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[QuoteOP.EmailQuote] Error emailing quote{messagequote.QuoteReference}]", messagequote.QuoteReference);
                throw new Exception(ex.Message);
            }
        }

        public void EmailPolicyBroker(EmailQuote messagequote)
        {

            try
            {


                Quotes quote;
                QuoteVm quotevm;
                var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);
                if (string.IsNullOrEmpty(from))
                {
                    from = "website@leisureinsure.co.uk";
                }
                // ----------------------- consilidate these repos... -------------------------------
                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == messagequote.QuoteReference);
                }

                var docLinks = new List<PolicyDocuments>();

                using (var docRepo = new PoliciesRepository())
                {
                    docLinks = docRepo.GetDocsForPolicy(quote.QuotePK, false);
                }

                //this Repo is a mess and needs to be split up or use LeisureInsureEntities
                using (var repo = new Repository())
                {
                    TrustPilot = new TrustPilotInformationSupplier(repo);

                    if (quote == null)
                        throw new Exception($"Cant email null quote {messagequote.QuoteReference}");

                    quotevm = new QuoteVm
                    {
                        Quote = quote,
                        BuyLink = messagequote.BaseLink + "/purchase/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        QuoteLink = messagequote.BaseLink + "/newcertificate/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        Currency = quote.LocaleId == 1 ? "£" : "€",
                    };

                    TrustPilot.ProcessViewModel(quotevm);
                }

                //if there are issues with razor we just use flat html with placeholders
                var rootpath = AppDomain.CurrentDomain.BaseDirectory;
                var headerHtml = File.ReadAllText($"{rootpath}//Templates//headerFooter.html");
                var bodyHtml = File.ReadAllText($"{rootpath}//Templates//ClientQuote.html");

                var currency = LocaleHelper.GetCurrency(quote.LocaleId);
                var total = $"{currency}{string.Format("{0:n0}", quote.Total)}";
                var name = quote.ContactName;

                bodyHtml = bodyHtml.Replace("{{Name}}", name);
                bodyHtml = bodyHtml.Replace("{{QuoteRef}}", quote.QuoteRef);
                bodyHtml = bodyHtml.Replace("{{Total}}", total);
                bodyHtml = bodyHtml.Replace("{{QuoteLink}}", quotevm.QuoteLink);
                bodyHtml = bodyHtml.Replace("{{BuyLink}}", quotevm.BuyLink);
                headerHtml = headerHtml.Replace("{{TrustStarLink}}", quotevm.TrustPilotStarLink);
                headerHtml = headerHtml.Replace("{{TrustDescription}}", quotevm.TrustPilotDescription);




                string docHtml = "<br>";
                foreach (var doc in docLinks)
                {
                    docHtml += $"<a href='{doc.DocLink}'>{doc.DocType}</a><br>";
                }
                docHtml += "<br>";

                bodyHtml = bodyHtml.Replace("{{DocLinks}}", docHtml);

                headerHtml = headerHtml.Replace("{{Body}}", bodyHtml);

                var Email = new EmailSender();

                Email.SendSynch(from, messagequote.Email, "Insurance Quotation from Leisureinsure LLP", headerHtml, null, "info@leisureInsure.co.uk");

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[QuoteOP.EmailQuote] Error emailing quote{messagequote.QuoteReference}]", messagequote.QuoteReference);
                throw new Exception(ex.Message);
            }
        }

        public void EmailQuoteBroker(EmailQuote messagequote)
        {           

            try
            {


                Quotes quote;
                QuoteVm quotevm;
                var from = CloudConfigurationManager.GetSetting(Constants.Configuration.EmailSender);
                var brokername = "";
                var clientname = "";

                if (string.IsNullOrEmpty(from))
                {
                    from = "website@leisureinsure.co.uk";
                }
                // ----------------------- consilidate these repos... -------------------------------
                using (var repo = new LeisureInsureEntities())
                {
                    quote = repo.Quotes.FirstOrDefault(x => x.QuoteRef == messagequote.QuoteReference);
                    brokername = quote.ContactName;                   

                    var quoteengine = repo.QuoteEngine.FirstOrDefault(x => x.QuoteId == quote.QuotePK);
                    if (quoteengine != null)
                    {
                        brokername = quoteengine.BrokerContact;                        
                    }

                    clientname = repo.Contacts.FirstOrDefault(x => x.ContactPK == quote.CustomerFK).EntityName;
                }

                var docLinks = new List<PolicyDocuments>();

                using (var docRepo = new PoliciesRepository())
                {
                    docLinks = docRepo.GetDocsForPolicy(quote.QuotePK, false);
                }

                //this Repo is a mess and needs to be split up or use LeisureInsureEntities
                using (var repo = new Repository())
                {
                    TrustPilot = new TrustPilotInformationSupplier(repo);

                    if (quote == null)
                        throw new Exception($"Cant email null quote {messagequote.QuoteReference}");

                    quotevm = new QuoteVm
                    {
                        Quote = quote,
                        BuyLink = messagequote.BaseLink + "/purchase/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        QuoteLink = messagequote.BaseLink + "/newcertificate/" + quote.QuoteRef + "/" + messagequote.Passcode,
                        Currency = quote.LocaleId == 1 ? "£" : "€",
                    };

                    TrustPilot.ProcessViewModel(quotevm);
                }

                //if there are issues with razor we just use flat html with placeholders
                var rootpath = AppDomain.CurrentDomain.BaseDirectory;
                var headerHtml = File.ReadAllText($"{rootpath}//Templates//headerFooter.html");              
                var bodyHtml = File.ReadAllText($"{rootpath}//Templates//BrokerQuote.html");

                var currency = LocaleHelper.GetCurrency(quote.LocaleId);
                var total = $"{currency}{string.Format("{0:n0}", quote.Total)}";                                

                bodyHtml = bodyHtml.Replace("{{Name}}", brokername);
                bodyHtml = bodyHtml.Replace("{{QuoteRef}}", quote.QuoteRef);
                bodyHtml = bodyHtml.Replace("{{Total}}", total);
                bodyHtml = bodyHtml.Replace("{{QuoteLink}}", quotevm.QuoteLink);
                bodyHtml = bodyHtml.Replace("{{BuyLink}}", quotevm.BuyLink);
                headerHtml = headerHtml.Replace("{{TrustStarLink}}", quotevm.TrustPilotStarLink);
                headerHtml = headerHtml.Replace("{{TrustDescription}}", quotevm.TrustPilotDescription);


                string docHtml = "<br>";
                foreach (var doc in docLinks)
                {
                    docHtml += $"<a href='{doc.DocLink}'>{doc.DocType}</a><br>";
                }
                docHtml += "<br>";

                bodyHtml = bodyHtml.Replace("{{DocLinks}}", docHtml);

                headerHtml = headerHtml.Replace("{{Body}}", bodyHtml);

                var Email = new EmailSender();

                var subject = $"Insurance Quotation for {clientname}";

                Email.SendSynch(from, messagequote.Email,subject, headerHtml, null, "info@leisureInsure.co.uk");

            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, $"[QuoteOP.EmailQuote] Error emailing quote{messagequote.QuoteReference}]", messagequote.QuoteReference);
                throw new Exception(ex.Message);
            }
        }
    }
}
