﻿namespace ItOps.Commands
{
    // ReSharper disable once InconsistentNaming
    public interface PaymentConfirmed
    {
        string QuoteReference { get; set; }
    }
}
