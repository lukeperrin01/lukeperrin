﻿namespace ItOps.Commands
{
    public interface Test
    {
        int Number { get; set; }
        string Word { get; set; }

    }
}
