﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface AddNotesToTam
    {
        string QuoteReference { get; set; }
    }
}
