﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface AddEmailAttachmentToTam
    {
        string QuoteReference { get; set; }
        string EmailDataBus { get; set; }
    }
}
