﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItOps.Commands.Tam
{
    public interface CreateScheduleOnTam
    {
        string QuoteReference { get; set; }
    }
}
