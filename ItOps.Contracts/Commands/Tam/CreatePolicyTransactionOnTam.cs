﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface CreatePolicyTransactionOnTam
    {
        string QuoteReference { get; set; }
    }
}
