﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface CreatePolicyOnTam
    {
        string QuoteReference { get; set; }
        bool PayByInstallments { get; set; }
        string AgentCode { get; set; }
    }
}
