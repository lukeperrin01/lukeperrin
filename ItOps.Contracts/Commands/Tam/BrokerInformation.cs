﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface BrokerInformation
    {
        string Email { get; set; }
        string TamCode { get; set; }
        string FsaNumber { get; set; }
        string Firstname { get; set; }
        string Lastname { get; set; }
        string EntityName { get; set; }
        string Telephone { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string Postcode { get; set; }
        string Location { get; set; }
        int ContactId { get; set; }
        bool Locked { get; set; }
        string County { get; set; }
    }
}
