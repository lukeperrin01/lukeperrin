﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface AddLegalFeesTransactionToTam
    {
        string QuoteReference { get; set; }
    }
}
