﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface AddCardChargeTransactionToTam
    {
        string QuoteReference { get; set; }
    }
}
