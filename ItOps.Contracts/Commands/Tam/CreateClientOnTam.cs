﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface CreateClientOnTam
    {
        string QuoteReference { get; set; }
    }
}
