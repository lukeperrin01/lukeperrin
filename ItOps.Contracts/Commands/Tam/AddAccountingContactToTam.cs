﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface AddAccountingContactToTam
    {
        string QuoteReference { get; set; }
    }
}
