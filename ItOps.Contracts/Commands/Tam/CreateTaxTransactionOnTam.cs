﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface CreateTaxTransactionOnTam
    {
        string QuoteReference { get; set; }
    }
}
