﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface CreateAgencyFeeTransactionOnTam
    {
        string QuoteReference { get; set; }
    }
}
