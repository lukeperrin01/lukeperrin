﻿namespace ItOps.Commands.Tam
{
    // ReSharper disable once InconsistentNaming
    public interface CreateCertificateOnTam
    {
        string QuoteReference { get; set; }
    }
}
