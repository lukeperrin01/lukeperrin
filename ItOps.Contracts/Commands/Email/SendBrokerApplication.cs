﻿namespace ItOps.Commands.Email
{
    // ReSharper disable once InconsistentNaming
    public interface SendBrokerApplication
    {
        string Brokerage { get; set; }
        string Name { get; set; }
        string Email { get; set; }
    }
}
