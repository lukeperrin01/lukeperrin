﻿namespace ItOps.Commands.Email
{
    // ReSharper disable once InconsistentNaming
    public interface SendEmail
    {
        string To { get; set; }
        string Subject { get; set; }
        string Body { get; set; }
        string HtmlBody { get; set; }
    }
}
