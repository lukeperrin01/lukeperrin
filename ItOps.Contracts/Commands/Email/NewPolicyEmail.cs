﻿namespace ItOps.Commands.Email
{
    // ReSharper disable once InconsistentNaming
    public interface NewPolicyEmail
    {
        string QuoteReference { get; set; }
    }
}
