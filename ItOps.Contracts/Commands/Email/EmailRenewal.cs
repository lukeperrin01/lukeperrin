﻿namespace ItOps.Commands.Email
{
    // ReSharper disable once InconsistentNaming
    public interface EmailRenewal
    {
        string QuoteReference { get; set; }
    }
}
