﻿namespace ItOps.Commands.Email
{
    // ReSharper disable once InconsistentNaming
    public interface Quote
    {
        string Name { get; set; }
        string Email { get; set; }
        string QuoteReference { get; set; }
        string Passcode { get; set; }
        string BaseLink { get; set; }
    }

    public class EmailQuote
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string QuoteReference { get; set; }
        public string Passcode { get; set; }
        public string BaseLink { get; set; }
    }
}
