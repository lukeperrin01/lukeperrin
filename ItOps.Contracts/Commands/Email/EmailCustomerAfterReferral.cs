﻿namespace ItOps.Commands.Email
{
    // ReSharper disable once InconsistentNaming
    public interface EmailCustomerAfterReferral
    {
        string QuoteReference { get; set; }
    }
}
