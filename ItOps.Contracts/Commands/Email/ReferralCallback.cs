﻿namespace ItOps.Commands.Email
{
    // ReSharper disable once InconsistentNaming
    public interface ReferralCallback
    {
        string QuoteReference { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string ClientName { get; set; }
    }
}
