﻿namespace ItOps.Commands.TrustPilot
{
    // ReSharper disable once InconsistentNaming
    public interface StartTrustPilotUpdates
    {
        int Id { get; set; }
    }
}
