﻿namespace ItOps.Commands.Certificates
{
    // ReSharper disable once InconsistentNaming
    public interface CreateCertificate
    {
        string QuoteReference { get; set; }
    }
}
