﻿namespace ItOps.Commands
{
    // ReSharper disable once InconsistentNaming
    public interface PaymentFailed
    {
        string PolicyReference { get; set; }
    }
}
