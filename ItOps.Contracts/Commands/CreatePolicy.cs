﻿namespace ItOps.Commands
{
    // ReSharper disable once InconsistentNaming
    public interface CreatePolicy
    {
        string PolicyReference { get; set; }
        bool PayByInstallments { get; set; }
        string AgentCode { get; set; }
    }
}
