﻿using System;
using NServiceBus;

namespace LeisureInsure.Nsb.Configuration
{
    public static class DelayedRetryConfigurationExtensions
    {
        public static void DelayedRetryConfiguration(this EndpointConfiguration config)
        {
            config.Recoverability().Delayed(cfg =>
            {
                cfg.NumberOfRetries(0);
                cfg.TimeIncrease(TimeSpan.FromSeconds(0));
            });
        }
    }
}

