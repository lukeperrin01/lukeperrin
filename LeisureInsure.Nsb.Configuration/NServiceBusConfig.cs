﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Practices.Unity;
using NServiceBus;
using NServiceBus.Faults;
using NServiceBus.Logging;
using System.Net;
using LeisureInsure.DB;
using LeisureInsure.DB.Logging;
using System.Web;
using ItOps.Commands.Certificates;
using ItOps.Messages.Certificates;
using ItOps.Messages.Tam;
using ItOps.Commands.TrustPilot;

namespace LeisureInsure.Nsb.Configuration
{
    public static class NServiceBusConfig
    {
        static void CommonConfiguration(EndpointConfiguration configuration)
        {
            //AppInsightLog.LogInfo($"CommonConfiguration started for {configuration.EndpointName.configuration}");

            configuration.UseSerialization<JsonSerializer>();
            configuration.EnableInstallers();

            configuration.ConfigureAzureServiceBus();
            configuration.ConfigureAzureStorage();

            configuration.ImmediateRetryConfiguration();
            configuration.DelayedRetryConfiguration();

            //configuration.ExcludeAssemblies("System.Data.SqlServerCE.dll");
            AssemblyScannerConfiguration ASC = new AssemblyScannerConfiguration();
            ASC.ExcludeAssemblies("System.Data.SqlServerCE.dll");
            LogManager.Use<DefaultFactory>().Level(LogLevel.Warn);
        }

        public static string EndpointName(string configurationString)
        {
            if (string.IsNullOrWhiteSpace(configurationString))
            {
                throw new ArgumentNullException(nameof(configurationString));
            }
            var endpointName = CloudConfigurationManager.GetSetting($"nservicebus.endpoints.{configurationString}");
            if (endpointName == null) // Not in the cloud...
            {

                endpointName = "LeisureInsure." + configurationString + ".Endpoint-" + Environment.UserName.ToLowerInvariant();

            }
            return endpointName;
        }

        private static void ErrorsOnMessageSentToErrorQueue(object sender, FailedMessage failedMessage)
        {
            var host = CloudConfigurationManager.GetSetting("email.sender.host");
            var username = CloudConfigurationManager.GetSetting("email.sender.username");
            var password = CloudConfigurationManager.GetSetting("email.sender.password");
            var credentials = new NetworkCredential(username, password);
            var url = CloudConfigurationManager.GetSetting("website.address");

            var quoteRef = "";
            string subject = "";
            string errorString = "";
            int startString = -1;
            int endString = -1;

            using (var mailClient = new SmtpClient(host) { Credentials = credentials })
            {
                errorString = System.Text.Encoding.Default.GetString(failedMessage.Body);
                startString = errorString.ToUpper().IndexOf("LEI0");

                if (startString > -1)
                {
                    quoteRef = errorString.Substring(startString, 11);
                    var stack = failedMessage.Exception.ToString();
                    var repo = new Repository();
                    repo.SaveToErrorLog(quoteRef, errorString, stack, url, "NSB Logged");
                }



                if (quoteRef == "")
                {
                    errorString = failedMessage.Exception.ToString();
                    startString = errorString.ToLower().IndexOf("quoteref: ");
                    endString = errorString.IndexOf("|");
                    if (startString > 0 && endString > 0)
                    {
                        quoteRef = errorString.Substring(startString + 10, endString - (startString + 10));
                    }
                }

                if (quoteRef != "")
                {
                    subject = "Error with NSB - " + quoteRef + " (" + url + ")";
                }
                else
                {
                    subject = "Error with NSB" + " (" + url + ")";
                }

                var message = new MailMessage();
                message.To.Add(new MailAddress(CloudConfigurationManager.GetSetting("nsb.error.email")));
                message.From = new MailAddress("(ERRORS) LeisureInsure <info@leisureinsure.co.uk>");
                message.Subject = subject;
                message.Body = $"Error with: {failedMessage.Body} <br/> {failedMessage.Exception}";

                message.IsBodyHtml = true;

                mailClient.Send(message);
            }
        }


        /// <summary>
        /// Configures the NServiceBus send only bus and registers it with the 
        ///     container
        /// </summary>
        public static async Task<IEndpointInstance> ConfigureSendOnlyBus(IUnityContainer container, string endpointName)
        {
            Console.WriteLine($"Configuring nservicebus endpoint: {endpointName}");
            var endpointConfiguration = new EndpointConfiguration(endpointName);
            CommonConfiguration(endpointConfiguration);

            endpointConfiguration.UseContainer<UnityBuilder>(c => c.UseExistingContainer(container));
            endpointConfiguration.Notifications.Errors.MessageSentToErrorQueue += ErrorsOnMessageSentToErrorQueue;
            //endpointConfiguration.SendOnly();
            // replaces obsolete code -----------------------------------------------------------
            endpointConfiguration.SendFailedMessagesTo("error");
            endpointConfiguration.AuditProcessedMessagesTo("audit");
            var defaultFactory = LogManager.Use<DefaultFactory>();
            defaultFactory.Level(LogLevel.Debug);
            //defaultFactory.Directory("directory here");
            var transport = endpointConfiguration.UseTransport<AzureServiceBusTransport>();
            //var routing = transport.Routing();

            //routing.RouteToEndpoint(
            //    assembly: typeof(CertificateCreated).Assembly,
            //    destination: Constants.Endpoints.ItOps);

            //routing.RouteToEndpoint(
            //    assembly: typeof(CertificateCreatedOnTam).Assembly,
            //    destination: Constants.Endpoints.ItOps);

            //routing.RouteToEndpoint(
            //    assembly: typeof(ClientCreatedOnTam).Assembly,
            //    destination: Constants.Endpoints.ItOps);

            //routing.RouteToEndpoint(
            //   assembly: typeof(LegalCarePolicyCreatedOnTam).Assembly,
            //   destination: Constants.Endpoints.ItOps);

            //routing.RouteToEndpoint(
            //   assembly: typeof(PolicyCreatedOnTam).Assembly,
            //   destination: Constants.Endpoints.ItOps);

            //routing.RouteToEndpoint(
            //   assembly: typeof(StartTrustPilotUpdates).Assembly,
            //   destination: Constants.Endpoints.ItOps);
            // ----------------------------------------------------------------------------------

            var bus = await Endpoint.Start(endpointConfiguration);
            container.RegisterInstance<IEndpointInstance>(bus);
            return bus;
        }

        /// <summary>
        /// Configure a new bus and register it with the container
        /// </summary>
        public static async Task<IEndpointInstance> ConfigureBus(IUnityContainer container, string endpointName, Action<EndpointConfiguration> additionalConfiguration = null)
        {
            Console.WriteLine($"Configuring nservicebus endpoint: {endpointName}");
            var endpointConfiguration = new EndpointConfiguration(endpointName);

            AppInsightLog.LogInfo($"About to start CommonConfiguration for  {endpointName}");

            CommonConfiguration(endpointConfiguration);

            endpointConfiguration.UseContainer<UnityBuilder>(c => c.UseExistingContainer(container));
            endpointConfiguration.Notifications.Errors.MessageSentToErrorQueue += ErrorsOnMessageSentToErrorQueue;
            additionalConfiguration?.Invoke(endpointConfiguration);

            var bus = await Endpoint.Start(endpointConfiguration);
            container.RegisterInstance<IEndpointInstance>(bus);

            Console.WriteLine("Bus successfully started");
            return bus;
        }
    }
}
