﻿using System.Configuration;
using Microsoft.Azure;
using NServiceBus;

namespace LeisureInsure.Nsb.Configuration
{
    public class ConfigureDatabus : INeedInitialization
    {
        public void Customize(EndpointConfiguration configuration)
        {
            var databusConnectionString = CloudConfigurationManager.GetSetting(Constants.Configuration.AzureStorageConnectionString);

            if (string.IsNullOrWhiteSpace(databusConnectionString))
            {
                throw new ConfigurationErrorsException(string.Format("The data bus connection string app setting is missing, please include an app setting with the name: {0}",
                    Constants.Configuration.AzureStorageConnectionString));
                
            }

            //Console.WriteLine("Using Data Bus connection string for NSB: {0}", databusConnectionString);
            var dataBus = configuration.UseDataBus<AzureDataBus>();
            
            dataBus.ConnectionString(databusConnectionString);
            dataBus.Container("databus");
            //BasePath(): the blobs base path under the container, defaults to empty string
            //DefaultTTL: time in seconds to keep blob in storage before it is removed, defaults to Int64.MaxValue seconds
            //MaxRetries: number of upload / download retries, defaults to 5 retries
            //NumberOfIOThreads: number of blocks that will be simultaneously uploaded, defaults to 5 threads
            //BackOffInterval: the back-off time between retries, defaults to 30 seconds
            //BlockSize: the size of a single block for upload when the number of IO threads is more than 1, defaults to 4MB
        }
    }
}
