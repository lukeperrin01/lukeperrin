﻿
using NServiceBus.Config;
using NServiceBus.Config.ConfigurationSource;

namespace LeisureInsure.Nsb.Configuration
{
    public class UnicastBusConfiguration : IProvideConfiguration<UnicastBusConfig>
    {
        public UnicastBusConfig GetConfiguration()
        {

            var itopsEndpointName = NServiceBusConfig.EndpointName(Constants.Endpoints.ItOps);

            var config = new UnicastBusConfig
            {
                MessageEndpointMappings = new MessageEndpointMappingCollection()
            };

            config.MessageEndpointMappings.Add(new MessageEndpointMapping { AssemblyName = "ItOps.Contracts", Endpoint = itopsEndpointName });

            return config;
        }
    }
}
