﻿using NServiceBus;

namespace LeisureInsure.Nsb.Configuration
{
    public class ConfigureConventions : INeedInitialization
    {
        public void Customize(EndpointConfiguration configuration)
        {
            configuration.Conventions()
                .DefiningCommandsAs(c => 
                    c.Namespace != null && (                   
                    c.Namespace.StartsWith("ItOps.Commands") 
                ));
            
            configuration.Conventions().DefiningDataBusPropertiesAs(c => c.Name.EndsWith("DataBus"));

            configuration.Conventions().DefiningEventsAs(c => 
                c.Namespace != null && (               
                c.Namespace.StartsWith("ItOps.Events") ||
                c.Namespace.StartsWith("Website.Events")
            ));

            configuration.Conventions().DefiningMessagesAs(c =>
                c.Namespace != null && (                
                c.Namespace.StartsWith("ItOps.Messages")
            ));
        }
    }
}
