﻿using System;
using System.Configuration;
using Microsoft.Azure;
using NServiceBus;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Nsb.Configuration
{
    public static class ConfigureAzureServiceBusExtensions
    {
        public static void ConfigureAzureServiceBus(this EndpointConfiguration config)
        {
            var azureServiceBusConnectionString = CloudConfigurationManager.GetSetting(Constants.Configuration.AzureServiceBusConnectionString);           

            if (string.IsNullOrWhiteSpace(azureServiceBusConnectionString))
            {
                //azureServiceBusConnectionString = "Endpoint=sb://leisureinsure.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=GdLK3NGWFhA987dNqEq4H6++RHdg4Qib8dz4MhOdcrU=";

                throw new ConfigurationErrorsException(string.Format("The azure service bus connection string app setting is missing, please include an app setting with the name: {0}",
                    Constants.Configuration.AzureServiceBusConnectionString));                
            }

            //Console.WriteLine("Using Azure Service Bus connection string for NSB: {0}", azureServiceBusConnectionString);

            var asbConfig = config.UseTransport<AzureServiceBusTransport>().ConnectionString(azureServiceBusConnectionString);
            asbConfig.UseForwardingTopology();            
            
            var queues = asbConfig.Queues();
            queues.LockDuration(TimeSpan.FromMinutes(3));
            queues.MaxDeliveryCount(3);

            var subscriptions = asbConfig.Subscriptions();
            subscriptions.MaxDeliveryCount(3);

            var sanitization = asbConfig.Sanitization();
            sanitization.UseStrategy<ValidateAndHashIfNeeded>();

            asbConfig.UseNamespaceAliasesInsteadOfConnectionStrings();
        }
    }
}
