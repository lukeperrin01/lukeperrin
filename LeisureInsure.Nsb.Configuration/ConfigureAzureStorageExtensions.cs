﻿using System.Configuration;
using Microsoft.Azure;
using NServiceBus;
using NServiceBus.Persistence;
using LeisureInsure.DB.Logging;

namespace LeisureInsure.Nsb.Configuration
{
    public static class ConfigureAzureStorageExtensions
    {
        public static void ConfigureAzureStorage(this EndpointConfiguration config)
        {
            var azureSagaStoragePersistenceConnectionString = CloudConfigurationManager.GetSetting(Constants.Configuration.AzureStorageConnectionString);
            var azureSubscriptionStoragePersistenceConnectionString = CloudConfigurationManager.GetSetting(Constants.Configuration.AzureStorageConnectionString);
            var azureTimeoutStoragePersistenceConnectionString = CloudConfigurationManager.GetSetting(Constants.Configuration.AzureStorageConnectionString);           

            if (string.IsNullOrWhiteSpace(azureSagaStoragePersistenceConnectionString))
            {
                //azureSagaStoragePersistenceConnectionString = "DefaultEndpointsProtocol=https;AccountName=leisureinsure;AccountKey=174IRp8lqgEM2uXujj+K5a4/wj+N+W6st/ozls9/NACze3CfrK9rRFjMLKamZUEFjDK15rLkg4vap9jJdQ6IPw==";

                throw new ConfigurationErrorsException(
                    $"The azure storage connection string app setting for NSB Sagas is missing, please include an app setting with the name: {Constants.Configuration.AzureStorageConnectionString}");
            }

            if (string.IsNullOrWhiteSpace(azureTimeoutStoragePersistenceConnectionString))
            {
                //azureTimeoutStoragePersistenceConnectionString = "DefaultEndpointsProtocol=https;AccountName=leisureinsure;AccountKey=174IRp8lqgEM2uXujj+K5a4/wj+N+W6st/ozls9/NACze3CfrK9rRFjMLKamZUEFjDK15rLkg4vap9jJdQ6IPw==";
                throw new ConfigurationErrorsException(
                    $"The azure storage connection string app setting for NSB Timeouts is missing, please include an app setting with the name: {Constants.Configuration.AzureStorageConnectionString}");
            }

            if (string.IsNullOrWhiteSpace(azureSubscriptionStoragePersistenceConnectionString))
            {
                //azureSubscriptionStoragePersistenceConnectionString = "DefaultEndpointsProtocol=https;AccountName=leisureinsure;AccountKey=174IRp8lqgEM2uXujj+K5a4/wj+N+W6st/ozls9/NACze3CfrK9rRFjMLKamZUEFjDK15rLkg4vap9jJdQ6IPw==";
                throw new ConfigurationErrorsException(
                    $"The azure storage connection string app setting for NSB Subscriptions is missing, please include an app setting with the name: {Constants.Configuration.AzureStorageConnectionString}");
            }

            //Console.WriteLine("Using Saga Storage connection string: {0}", azureSagaStoragePersistenceConnectionString);
            //Console.WriteLine("Using Timeout Storage connection string: {0}", azureTimeoutStoragePersistenceConnectionString);
            //Console.WriteLine("Using Subscription Storage connection string: {0}", azureSubscriptionStoragePersistenceConnectionString);
            
            config.UsePersistence<AzureStoragePersistence, StorageType.Sagas>()
                .ConnectionString(azureSagaStoragePersistenceConnectionString)
                .CreateSchema(true);
            config.UsePersistence<AzureStoragePersistence, StorageType.Subscriptions>()
                .ConnectionString(azureSubscriptionStoragePersistenceConnectionString)
                .CreateSchema(true);          
        }
    }
}
