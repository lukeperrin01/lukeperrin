﻿using NServiceBus;

namespace LeisureInsure.Nsb.Configuration
{
    public static class ImmediateRetryConfigurationExtensions 
    {
        public static void ImmediateRetryConfiguration(this EndpointConfiguration config)
        {
            config.Recoverability().Immediate(cfg =>
            {
                cfg.NumberOfRetries(0);
            });
        }
    }
}
