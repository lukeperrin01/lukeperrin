﻿using FluentAssertions;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Information;
using LeisureInsure.Services.Services.Information;
using Moq;
using Ploeh.AutoFixture.Xunit2;
using Xunit;

namespace LeisureInsure_Unit_Tests.ServiceTests.InformationTests.PolicyInformationTests
{
    public class BitwiseIdTests
    {
        [Theory, AutoMoqData]
        public void CacheWorksAfterFirstRequest(
            [Frozen] Mock<ILiRepository> repository,
            PolicyInformation sut,
            Policy policy,
            int policyId,
            int bitwiseId)
        {
            policy.BitwiseId = bitwiseId;
            repository.Setup(x => x.Find<Policy>(policyId)).Returns(policy);

            sut.BitwiseId(policyId);
            sut.BitwiseId(policyId);

            repository.Verify(x => x.Find<Policy>(policyId), Times.Once);
        }

        [Theory, AutoMoqData]
        public void BitwiseIdIsCorrect([Frozen] Mock<ILiRepository> repository,
            PolicyInformation sut,
            Policy policy,
            int policyId,
            int bitwiseId)
        {
            policy.BitwiseId = bitwiseId;
            repository.Setup(x => x.Find<Policy>(policyId)).Returns(policy);

            var result = sut.BitwiseId(policyId);
            
            result.Should().Be(bitwiseId);
        }
    }
}
