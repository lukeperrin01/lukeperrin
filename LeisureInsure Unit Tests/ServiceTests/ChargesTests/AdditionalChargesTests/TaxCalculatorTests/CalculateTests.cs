﻿//using System.Collections.Generic;
//using System.Linq;
//using FluentAssertions;
//using LeisureInsure.Services.Dal.LeisureInsure;
//using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
//using LeisureInsure.Services.Services;
//using LeisureInsure.Services.Services.Charges.AdditionalCharges;
//using Moq;
//using Ploeh.AutoFixture.Xunit2;
//using Xunit;

//namespace LeisureInsure_Unit_Tests.ServiceTests.ChargesTests.AdditionalChargesTests.TaxCalculatorTests
//{
//    // Slightly difficult to test as based a lot on the database

//    public class CalculateTests
//    {
//        [AutoMoqData, Theory]
//        public void CalculationUsesLocale(
//            [Frozen] Mock<ILiRepository> repository,
//            TaxCalculator sut, 
//            Rate testRate1,
//            Rate testRate2)
//        {
//            testRate1.LocaleBw = 1;
//            testRate1.PolicyBw = 1;
//            //testRate1.InputId = Inputs.Tax;
//            testRate1.Value = 10;
//            testRate1.ValidUntil = null;

//            testRate2.LocaleBw = 2;
//            testRate2.PolicyBw = 1;
//            //testRate2.InputId = Inputs.Tax;
//            testRate2.Value = 20;
//            testRate2.ValidUntil = null;

//            repository.Setup(x => x.Q<Rate>()).Returns(new List<Rate> {testRate1, testRate2}.AsQueryable());

//            // Try first rate
//            var result = sut.Calculate(100, 1);
//            result.Should().Be(10); // 100 X 10%

//            // Try second rate
//            result = sut.Calculate(100, 2);
//            result.Should().Be(20); // 100 X 20%
//        }

//        [InlineAutoMoq(100)]
//        [InlineAutoMoq(200)]
//        [InlineAutoMoq(500)]
//        [Theory]
//        public void CalculationUsesPremium(
//            decimal premium,
//            [Frozen] Mock<ILiRepository> repository,
//            TaxCalculator sut,
//            Rate testRate1)
//        {
//            testRate1.LocaleBw = 1;
//            testRate1.PolicyBw = 1;
//            //testRate1.InputId = Inputs.Tax;
//            testRate1.Value = 10;
//            testRate1.ValidUntil = null;
            
//            repository.Setup(x => x.Q<Rate>()).Returns(new List<Rate> { testRate1 }.AsQueryable());
//            var expectedTax = premium * 0.10m;


//            var result = sut.Calculate(premium, 1);


//            result.Should().Be(expectedTax); 
//        }
//    }
//}
