﻿//using System.Collections.Generic;
//using System.Linq;
//using FluentAssertions;
//using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
//using LeisureInsure.Services.Models.Quotes;
//using LeisureInsure.Services.Services;
//using LeisureInsure.Services.Services.Charges.Discounts;
//using Xunit;

//namespace LeisureInsure_Unit_Tests.ServiceTests.ChargesTests.DiscountsTests.MultiPartyDiscountTests
//{
//    public class CalculateTests
//    {
//        [InlineAutoMoq(1, 0)]
//        [InlineAutoMoq(2, 0)]
//        [InlineAutoMoq(3, 0)]
//        [InlineAutoMoq(4, 10)]
//        [InlineAutoMoq(5, 10)]
//        [InlineAutoMoq(6, 10)]
//        [InlineAutoMoq(7, 20)]
//        [InlineAutoMoq(8, 20)]
//        [Theory]
//        void DiscountIsCorrect(int numberOfInstructors, decimal expectedDiscount, 
//            MultiPartyDiscount sut)
//        {
//            var summary = new Quote
//            {
//                ChargeSummaryItems = new List<QuoteLine>
//                {
//                    new QuoteLine
//                    {
//                        CoverId = 2,
//                        Price = 10000,
//                    }
//                },
//                Discounts = new List<Discount>(),
//            };

//            var answers = new List<ChargeCalculationAnswer>
//            {
//                new ChargeCalculationAnswer
//                {
//                    RateTypeFK = 24,
//                    RateValue = numberOfInstructors.ToString()
//                }
//            };

//            sut.Calculate(summary, answers, 15);

//            var discount = summary.Discounts.FirstOrDefault()?.Price??0;
//            discount.Should().Be(expectedDiscount/100*10000);
//        }
//    }
//}
