﻿//using System.Linq;
//using FluentAssertions;
//using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
//using LeisureInsure.Services.Services.Charges.Discounts;
//using Xunit;

//namespace LeisureInsure_Unit_Tests.ServiceTests.ChargesTests.DiscountsTests.OnlineDiscountTests
//{
//    public class CalculateTests
//    {
//        [Theory, AutoMoqData]
//        public void TotalIsTheSameAfterApplyingDiscount(OnlineDiscount sut, Quote testSummary)
//        {
//            var exptectedTotal = testSummary.ChargeSummaryItems.Sum(x => x.Price) - testSummary.Discounts.Sum(x => x.Price);

//            sut.Calculate(testSummary, null, 0);

//            var result = testSummary.ChargeSummaryItems.Sum(x => x.Price) - testSummary.Discounts.Sum(x => x.Price);
//            result.Should().Be(exptectedTotal);
//        }
//    }
//}
