﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace LeisureInsure_Unit_Tests
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class InlineAutoMoqAttribute : AutoMoqDataAttribute
    {
        private readonly object[] _values;

        public InlineAutoMoqAttribute(params object[] values)
        {
            _values = values;
        }

        public override IEnumerable<object[]> GetData(MethodInfo method)
        {
            var testCaseData = base.GetData(method);

            foreach (object[] caseValues in testCaseData)
            {
                if (_values.Length > caseValues.Length)
                {
                    throw new InvalidOperationException("Number of parameters provided is more than expected parameter count");
                }
                Array.Copy(_values, caseValues, _values.Length);

                yield return caseValues;
            }
        }
    }
}
