﻿using FluentAssertions;
using Xunit;

namespace LeisureInsure_Unit_Tests.GeneralTests
{
    public class EnumNameTests
    {
        [Fact]
        public void EnumToStringIsTheSameAsTheProperty()
        {
            TestEnum.Test1.ToString().Should().Be("Test1");
        }

        private enum TestEnum
        {
            Test1
        }
    }
}
