﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LeisureInsure.Helpers;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.Services.Utilities;

namespace LeisureInsure_Unit_Tests.GeneralTests
{
    /// <summary>
    /// Summary description for DateTests, test start in the past, test start today, test start in the future
    /// </summary>
    [TestClass]
    public class DateTests
    {                             

        /// <summary>
        /// There is Date From and Date To, times default from 00:00 to 23:59
        /// </summary>
        [TestMethod]
        public void TestFieldSportsEvent()
        {
            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
            var datehelper = new DateTimeHelper();

            var startdate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, dateNow.Hour - 2, dateNow.Minute, 0);
            var startTime = startdate.ToString("HH:mm");

            //Fieldsports (no times) start today end tomorrow , time is a little backdated   
                                    
            var date1 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day + 1),
                startTime = "00:00",
                endTime = "23:59",
                PolicyType = "E",
                PolicyPK = PolicyType.FieldSportsEvent
            };

            //Assert time is brought forward 
            var result = datehelper.DateChecker(date1);
            Assert.AreEqual(result.startDate.Hour, dateNow.Hour);
            Assert.AreEqual(result.startDate.Minute, dateNow.Minute);
            Assert.AreEqual(result.startDate.Date, dateNow.Date);

            //Fieldsports (no times) start 2 days ago, finish tomorrow
            //Assert start date/time is brought forward
            var date3 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day - 2),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day + 1),
                startTime = "00:00",
                endTime = "23:59",
                PolicyType = "E",
                PolicyPK = PolicyType.FieldSportsEvent
            };

            var result3 = datehelper.DateChecker(date3);
            Assert.AreEqual(result3.startDate.Hour, dateNow.Hour);
            Assert.AreEqual(result3.startDate.Minute, dateNow.Minute);
            Assert.AreEqual(result3.startDate.Date, dateNow.Date);

            //Fieldsports (no times) start tomorrow last 3 days            
            var date2 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day + 1),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day + 4),
                startTime = "00:00",
                endTime = "23:59",
                PolicyType = "E",
                PolicyPK = PolicyType.FieldSportsEvent
            };
            //Assert start time is midnight, endtime is 23:59
            var result2 = datehelper.DateChecker(date2);
            Assert.AreEqual(result2.startTime, "00:00");
            Assert.AreEqual(result2.endTime, "23:59");           
        }

        /// <summary>
        /// Only one event or One off event shows all dates/time options
        /// </summary>
        [TestMethod]
        public void TestEvent()
        {
            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
            var datehelper = new DateTimeHelper();            

            var startdate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, dateNow.Hour - 2, dateNow.Minute, 0);
            var startTime = startdate.ToString("HH:mm");

            //start today end tomorrow , time is backdated by 2 hours  
            //Assert time is brought forward                         
            var date1 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day + 1),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "E",
                PolicyPK = PolicyType.Events
            };


            var result = datehelper.DateChecker(date1);
            Assert.AreEqual(result.startDate.Hour, dateNow.Hour);
            Assert.AreEqual(result.startDate.Minute, dateNow.Minute);
            Assert.AreEqual(result.startDate.Date, dateNow.Date);

            //start 2 days ago, finish tomorrow
            //Assert start date/time is brought forward
            var date3 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day - 2),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day + 1),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "E",
                PolicyPK = PolicyType.Events
            };

            var result3 = datehelper.DateChecker(date3);
            Assert.AreEqual(result3.startDate.Hour, dateNow.Hour);
            Assert.AreEqual(result3.startDate.Minute, dateNow.Minute);
            Assert.AreEqual(result3.startDate.Date, dateNow.Date);

            //start tomorrow last 3 days
            //Assert start time is midnight, endtime is 23:59
            var date2 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day + 1),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day + 4),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "E",
                PolicyPK = PolicyType.Events
            };

            var result2 = datehelper.DateChecker(date2);
            Assert.AreEqual(result2.startTime, "00:00");
            Assert.AreEqual(result2.endTime, "23:59");
        }

        /// <summary>
        /// There is only a Date From on website
        /// </summary>
        [TestMethod]
        public void TestMobileCateringAnnual()
        {
            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
            var datehelper = new DateTimeHelper();

            var startdate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, dateNow.Hour - 2, dateNow.Minute, 0);
            var startTime = startdate.ToString("HH:mm");

            //paymentController.ts defaults start time to now, end time to 23:59          

            //MobileCatering (no times) was set to start yesterday, client fell asleep for a day
            //unlikely to happen as dates/times are entered on payment page                      
            var date1 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day - 1),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day - 1),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "A",
                PolicyPK = PolicyType.MobileCatering
            };

            var result = datehelper.DateChecker(date1);
            //Assert start time/date is brought forward
            Assert.AreEqual(result.startDate.Hour, dateNow.Hour);
            Assert.AreEqual(result.startDate.Minute, dateNow.Minute);
            Assert.AreEqual(result.startDate.Date, dateNow.Date);

            //Assert end date/time is set properly
            Assert.AreEqual(result.endDate.Day, dateNow.Day);
            Assert.AreEqual(result.endDate.Month, dateNow.Month);
            Assert.AreEqual(result.endDate.Year, dateNow.Year + 1);                        
            Assert.AreEqual(result.endTime, "00:00");

            //MobileCatering (no times) was set to start today                            
            var date2 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "A",
                PolicyPK = PolicyType.MobileCatering
            };

            var result2 = datehelper.DateChecker(date2);
            //Assert start time/date is today
            Assert.AreEqual(result2.startDate.Hour, dateNow.Hour);
            Assert.AreEqual(result2.startDate.Minute, dateNow.Minute);
            Assert.AreEqual(result2.startDate.Date, dateNow.Date);

            //Assert end date/time is set properly
            Assert.AreEqual(result2.endDate.Day, dateNow.Day);
            Assert.AreEqual(result2.endDate.Month, dateNow.Month);
            Assert.AreEqual(result2.endDate.Year, dateNow.Year + 1);
            Assert.AreEqual(result2.endTime, "00:00");

            //MobileCatering (no times) was set to start next month on the 5th                              
            var date3 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month + 1, 5),
                endDate = new DateTime(dateNow.Year, dateNow.Month + 1, 5),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "A",
                PolicyPK = PolicyType.MobileCatering
            };

            var result3 = datehelper.DateChecker(date3);
            //Assert start time/date is the 5th of next month
            Assert.AreEqual(result3.startDate.Hour, 0);
            Assert.AreEqual(result3.startDate.Minute, 0);
            Assert.AreEqual(result3.startDate.Date, new DateTime(dateNow.Year, dateNow.Month + 1, 5));

            //Assert end date/time is set properly
            Assert.AreEqual(result3.endDate.Day, 5);
            Assert.AreEqual(result3.endDate.Month, dateNow.Month + 1);
            Assert.AreEqual(result3.endDate.Year, dateNow.Year + 1);
            Assert.AreEqual(result3.endTime, "00:00");

        }

        /// <summary>
        /// There is only a Date From
        /// </summary>
        [TestMethod]
        public void TestEquipmentHirerAnnual()
        {
            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
            var datehelper = new DateTimeHelper();

            var startdate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, dateNow.Hour - 2, dateNow.Minute, 0);
            var startTime = startdate.ToString("HH:mm");

            //Equipment Hirer (no times) 
            //start yesterday, client fell asleep for a day            
            var date1 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day - 1),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day - 1),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "A",
                PolicyPK = PolicyType.EquipmentHirer
            };

            var result = datehelper.DateChecker(date1);
            //Assert start time/date is brought forward
            Assert.AreEqual(result.startDate.Hour, dateNow.Hour);
            Assert.AreEqual(result.startDate.Minute, dateNow.Minute);
            Assert.AreEqual(result.startDate.Date, dateNow.Date);

            //Assert end date/time is set properly
            Assert.AreEqual(result.endDate.Day, dateNow.Day);
            Assert.AreEqual(result.endDate.Month, dateNow.Month);
            Assert.AreEqual(result.endDate.Year, dateNow.Year + 1);
            Assert.AreEqual(result.endTime, "00:00");

            //Set to start today                            
            var date2 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day),
                endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "A",
                PolicyPK = PolicyType.EquipmentHirer
            };

            var result2 = datehelper.DateChecker(date2);
            //Assert start time/date is today
            Assert.AreEqual(result2.startDate.Hour, dateNow.Hour);
            Assert.AreEqual(result2.startDate.Minute, dateNow.Minute);
            Assert.AreEqual(result2.startDate.Date, dateNow.Date);

            //Assert end date/time is set properly
            Assert.AreEqual(result2.endDate.Day, dateNow.Day);
            Assert.AreEqual(result2.endDate.Month, dateNow.Month);
            Assert.AreEqual(result2.endDate.Year, dateNow.Year + 1);
            Assert.AreEqual(result2.endTime, "00:00");

            //Set to start next month on the 5th                              
            var date3 = new vmDateCheck
            {
                startDate = new DateTime(dateNow.Year, dateNow.Month + 1, 5),
                endDate = new DateTime(dateNow.Year, dateNow.Month + 1, 5),
                startTime = startTime,
                endTime = "23:59",
                PolicyType = "A",
                PolicyPK = PolicyType.EquipmentHirer
            };

            var result3 = datehelper.DateChecker(date3);
            //Assert start time/date is the 5th of next month
            Assert.AreEqual(result3.startDate.Hour, 0);
            Assert.AreEqual(result3.startDate.Minute, 0);
            Assert.AreEqual(result3.startDate.Date, new DateTime(dateNow.Year, dateNow.Month + 1, 5));

            //Assert end date/time is set properly
            Assert.AreEqual(result3.endDate.Day, 5);
            Assert.AreEqual(result3.endDate.Month, dateNow.Month + 1);
            Assert.AreEqual(result3.endDate.Year, dateNow.Year + 1);
            Assert.AreEqual(result3.endTime, "00:00");

        }
    }
}
