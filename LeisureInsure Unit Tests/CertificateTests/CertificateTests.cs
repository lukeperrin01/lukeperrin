﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LeisureInsure.DB;
using LeisureInsure.Services.Services.Utilities;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace LeisureInsure_Unit_Tests.CertificateTests
{
    [TestClass]
    public class CertificateTests
    {

        [TestMethod]
        public void RegexTest()
        {
            string test = "£50,000";
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");

            var allPrem = rgx.Replace(test, "");
        }

        [TestMethod]
        public void TestMethod1()
        {
            //309525 paintball only PL with free covers
            //309520 food contamination - lots of free covers
            //309526 street parties only PL added no free covers
            //309527 equipment hirer
            //309529 event supplier one off event
            //309534 annual event 15 a year
            //309548 exhibitors (bundled MD 2K, cancellation 2k)
            string test = null;
            var util = new NumberAbbreviator();

            string spanUK1 = "£{value} Each Insured Event and in the aggregate";
            decimal suminsured = 2000000;
            var sum = Math.Round(suminsured, 0); //util.Transform(suminsured);

            var format = String.Format("{0:n0}", suminsured);
            spanUK1 = spanUK1.Replace("{value}", sum.ToString());

            var repo = new Repository();
            var headers = repo.GetCertHeaders(309520);
            var sections = repo.GetCertSections(headers, 309520);


            //repo._getCertSectionsNEW(309520);

        }

        [TestMethod]
        public void TestStability()
        {
            var policy = new policy { Name = "Showmen",PolicyInfo = "Some useful stuff" };
            var cover = new cover { Name = "ShowmenPL" };
            var rate1 = new rate { Name = "Rate1", RateValue = "100000", SumInsured = 100000 };
            var rate2 = new rate { Name = "Rate2", RateValue = "200000", SumInsured = 200000 };
            var rate3 = new rate { Name = "Rate3", RateValue = "300000", SumInsured = 300000 };
            cover.InputsForCover.Add(rate1);
            cover.InputsForCover.Add(rate2);
            cover.InputsForCover.Add(rate3);
            policy.Covers.Add(cover);

        }

    }

    public class policy
    {
        public string Name { get; set; }
        public string PolicyInfo { get; set; }
        public List<cover> Covers { get; set; }

        public policy()
        {
            Covers = new List<cover>();
        }
    }

    public class cover
    {
        public string Name { get; set; }
        public List<rate> InputsForCover { get; set; }

        public cover()
        {
            InputsForCover = new List<rate>();
        }
    }

    public class rate
    {
        public string Name { get; set; }
        public string RateValue { get; set; }
        public decimal SumInsured { get; set; }
    }
}


