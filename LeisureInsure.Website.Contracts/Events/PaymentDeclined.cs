﻿namespace Website.Events
{
    // ReSharper disable once InconsistentNaming
    public interface PaymentDeclined
    {
        string QuoteReference { get; set; }
    }
}
