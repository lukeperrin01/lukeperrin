﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.Business
{
    public class PolicyDocuments
    {
        public string DocType { get; set; }
        public string LinkType { get; set; }
        public string DocLink { get; set; }
        public bool MustRead { get; set; }
    }
}
