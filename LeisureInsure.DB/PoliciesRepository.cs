﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;
using LeisureInsure.DB.ViewModels;
using System.Data;
using LeisureInsure.DB.Utilities;
using LeisureInsure.Services.Utilities;
using LeisureInsure.Services.Services.Utilities;
using LeisureInsure.DB.Business;
using Microsoft.Azure;
using System.Text.RegularExpressions;


namespace LeisureInsure.DB
{
    public class PoliciesRepository : IDisposable
    {
        private readonly LeisureInsureEntities _dbContext;
        private bool _disposed;
        public PoliciesRepository()
        {
            _dbContext = new LeisureInsureEntities();
        }

        private static Regex digitsOnly = new Regex(@"[^\d]");


        #region Locales
        public vmLocales GetLocales(string strLocale, int policyid)
        {

            IList<vmLocale> loc = new List<vmLocale>();

            var locList = _dbContext.Locales.ToList();
            var selectedId = 0;
            foreach (var locale in locList)
            {
                if (locale.strLocale == strLocale)
                    selectedId = locale.LocalePK;
            }

            foreach (var l in locList)
            {
                if (l.LocalePK == 2)
                {
                    l.Multiplier = _dbContext.Products.FirstOrDefault(x => x.Id == policyid).EireMultiplier;
                }

                loc.Add(new vmLocale()
                {
                    localePK = l.LocalePK,
                    localeBW = l.BitWise,
                    flagUrl = l.imgFlag,
                    strLocale = l.strLocale,
                    currencySymbol = l.CurrencySymbol,
                    territory = l.Territory,
                    country = l.Country,
                    localePKselected = selectedId,
                    localeRefer = l.LocaleRefer,
                    multiplier = l.Multiplier,
                    exchangeRate = l.ExchangeRate,
                    showFlag = true
                });
            }

            var locSelected = new vmLocales()
            {
                localeList = loc,
                localeSelected = loc.FirstOrDefault(x => x.localePK == selectedId)
            };

            return locSelected;
        }

        #endregion

        #region Brokers
        public vmBrokerAddress GetBrokerAddress(int quotePK)
        {

            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            var agent = _dbContext.Contacts.FirstOrDefault(x => x.ContactPK == quote.BrokerFK);

            //this is the address of the customer
            //var addy = _dbContext.Addresses.FirstOrDefault(x => x.AddressPK == quote.AddressFK);
            var addy = _dbContext.Addresses.FirstOrDefault(x => x.ContactFK == agent.ContactPK);

            var brokerAddy = new vmBrokerAddress();

            brokerAddy.Address1 = addy.Address1;
            brokerAddy.Address2 = addy.Address2;
            brokerAddy.DateEffective = (DateTime)quote.DateFrom;
            brokerAddy.Address3 = addy.Address3;
            brokerAddy.ContactPK = (int)quote.BrokerFK;
            brokerAddy.Country = addy.Country;
            brokerAddy.County = addy.County;
            brokerAddy.EntityName = agent.EntityName;
            brokerAddy.Postcode = addy.Postcode;
            brokerAddy.Telephone = agent.Telephone;
            brokerAddy.Town = addy.Town;

            return brokerAddy;
        }

        #endregion

        #region Quotes
        /// <summary>
        /// Generic method we want on most repositories
        /// </summary>
        /// <param name="quoteref"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Quotes GetQuote(string quoteref, string password)
        {
            var quote = _dbContext.Quotes.Where(x => x.QuoteRef == quoteref && x.PWord == password).FirstOrDefault();

            return quote;
        }

        #endregion

        #region Policies

        public Products GetPolicy(int policyid)
        {
            var policy = _dbContext.Products.FirstOrDefault(x => x.Id == policyid);

            return policy;
        }


        /// <summary>
        /// Get Policy with rates/items for the frontend
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PolicyView GetPolicyData(int id, vmLocale locale, bool isAgent)
        {

            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

            var coverViews = new List<CoverView>();
            var policy = _dbContext.Products.Where(x => x.Id == id).FirstOrDefault();
            var landingdata = _dbContext.LandingPages.FirstOrDefault(x => x.NewPolicyId == policy.Id);

            var covers = _dbContext.CoversNew.Where(x => x.ProductId == policy.Id).ToList();

            //MD and BI covers are only assigned to paintball, but if another product requires these as 
            //well then grab the same ones. Otherwise it is very time consuming if products have their own copies
            if(policy.BiMdProduct == 1)
            {
                var bimdCovers = _dbContext.CoversNew.Where(x => x.CoverTypeId == 3 || x.CoverTypeId == 22).ToList();
                covers.AddRange(bimdCovers);
            }


            var allItemRates = _dbContext.ItemRates.ToList();

            //get min start date, and max start date
            var minStartDate = new DateTime();
            var maxStartDate = new DateTime();
            int minDateDays = (int)policy.MinDateDays;
            int maxDateDays = (int)policy.MaxDateMonths;
            minStartDate = dateNow.AddDays(minDateDays);
            maxStartDate = dateNow.AddDays(maxDateDays);

            var minStart = JavaScriptDateConverter.Convert(minStartDate);
            var maxStart = JavaScriptDateConverter.Convert(maxStartDate);

            var publishedKey = CloudConfigurationManager.GetSetting("stripePublishedKey");

            var mdHeaderAdded = false;
            var biHeaderAdded = false;

            covers = covers.OrderBy(x => x.CoverOrdinal).ToList();

            foreach (var cover in covers)
            {
                var coverview = new CoverView();
                var rateViews = new List<RateView>();

                coverview = ViewMapper.MapCoverView(cover);

                if ((cover.CoverTypeId == 3 || cover.CoverTypeId == 22) && cover.CoverType == "Optional")
                {
                    coverview.ShowCover = false;
                    coverview.SectionType = true;
                }
                else
                {
                    coverview.ShowCover = true;
                    coverview.SectionType = false;
                }

                if ((cover.CoverTypeId == 3) && cover.CoverType == "Optional")
                {
                    if (!mdHeaderAdded)
                        coverview.sectionHeader = true;
                    mdHeaderAdded = true;
                }

                if ((cover.CoverTypeId == 22) && cover.CoverType == "Optional")
                {
                    if (!biHeaderAdded)
                        coverview.sectionHeader = true;
                    biHeaderAdded = true;
                }


                var ratesList = cover.RatesNew.Where(x => (x.LocaleBW & locale.localePK) > 0).ToList();

                foreach (var rate in ratesList)
                {                                      

                    //tables thank link a category rating to an item need to get the rate for that category
                    if(rate.RateTypeId == 50)
                    {
                        var localeIndemnity = (decimal)rate.Indemnity * locale.exchangeRate;
                        rate.Indemnity = (int)localeIndemnity;
                    }
                    
                    
                    //indemnity (table)
                    if (rate.RateTypeId == 5)
                    {
                        var localeIndemnity = (decimal)rate.Indemnity * locale.exchangeRate;
                        rate.Indemnity = (int)localeIndemnity;
                        //apply multiplier on minimum charges for PL table
                        if (rate.MinimumRate > 0)
                            rate.MinimumRate = rate.MinimumRate * locale.multiplier;
                    }

                    //turnover (column table)                 
                    if (rate.RateTypeId == 48)
                    {
                        if (rate.DivBy == 1)
                            rate.Rate = rate.Rate * locale.multiplier;
                    }

                    //premiums - eg items like equipment
                    if (rate.RateTypeId == 10)
                    {
                        //apply exchange rate
                        if (rate.DivBy == 1)
                        {
                            rate.Rate = rate.Rate * locale.multiplier;
                            rate.FirstRate = rate.FirstRate * locale.multiplier;
                            rate.DiscountFirstRate = rate.DiscountFirstRate * locale.multiplier;
                            rate.DiscountSubsequentRate = rate.DiscountSubsequentRate * locale.multiplier;
                        }
                        //apply multiplier on minimum charges for keyed value (turnover) 
                        if (rate.MinimumRate > 0)
                            rate.MinimumRate = rate.MinimumRate * locale.multiplier;
                    }

                    //minimum charges - note there are 2 for EL, 
                    if (rate.RateTypeId == 13)
                    {
                        rate.Rate = rate.Rate * locale.multiplier;
                    }

                    var rateview = ViewMapper.MapRateView(rate, locale.localePK);

                    //get list of all default options for a select
                    //do we have a select?
                    if (rateview.InputTypeId == 7)
                    {
                        //get rates based on our item type
                        var rateIdsForSelect = allItemRates.Where(x => x.ItemTypeId == rateview.ItemTypeId).ToList();
                        var optionIds = rateIdsForSelect.Select(x => x.RateId).ToList();
                        var options = ratesList.Where(x => optionIds.Contains(x.Id)).ToList();

                        //SumInsured Dropdowns - we are not applying exchange rate to this
                        foreach (var option in options)
                        {
                            if (option.RateTypeId == 6 && option.InputTypeId == 20)
                            {
                                decimal rateDec = option.RateAsNum ?? 0;

                                option.RateName = $"{locale.currencySymbol}{string.Format("{0:n}",rateDec)}";
                            } 
                        }
                        
                        options = options.OrderBy(x => x.Ordinal).ToList();
                        var optionViews = ViewMapper.MapRateViews(options);
                        rateview.Options = optionViews;
                    }

                    rateViews.Add(rateview);

                }
                var itemTypes = cover.ItemTypes.ToList();

                var itemRateViews = ViewMapper.MapItemRateViews(itemTypes);

                if (coverview.CoverType == "Mandatory")
                    coverview.CoverRequired = true;
                if (coverview.CoverType == "Optional")
                    coverview.CoverRequired = false;
                if (coverview.CoverType == "Bundled")
                    coverview.CoverRequired = true;

                coverview.RateItems = itemRateViews;
                var ordereredRates = rateViews.OrderBy(x => x.Ordinal).ToList();
                coverview.Rates = ordereredRates;
                coverViews.Add(coverview);
            }

            coverViews = coverViews.OrderBy(x => x.CoverOrdinal).ToList();

            var policyView = new PolicyView
            {
                Id = policy.Id,
                PolicyName = policy.ProductName,
                TamType = policy.TamType,
                Ordinal = policy.Ordinal,
                LeisureInsureFee = policy.LeisureInsureFee,
                PeriodYear = policy.PeriodDay,
                PeriodDay = policy.PeriodDay,
                PolicyType = policy.PolicyType,
                PolicyGroupFK = policy.PolicyGroupFK,
                PremiseRequired = policy.PremiseRequired,
                TimeRequired = policy.TimeRequired,
                TamOccupation = policy.TamOccupation,
                ClauseCode = policy.ClauseCode,
                Excess = policy.Excess,
                BusinessDescription = policy.BusinessDescription,
                CertificateBody = policy.CertificateBody,
                Covers = coverViews,
                //start dates
                MinStartDate = minStart,
                MaxStartDate = maxStart,
                StripePublishableKey = publishedKey,
                LandingPage = landingdata
            };

            if (isAgent)
                policyView.CommissionPercentage = policy.BrokerCommission;

            return policyView;
        }


        /// <summary>
        /// Get all policies with covers and rates that affect pricing (used in admin section)
        /// </summary>
        /// <returns></returns> 
        public List<PolicyView> GetPolicesForUpdating()
        {

            var policies = _dbContext.Products.ToList();
            var allItemRates = _dbContext.ItemRates.ToList();


            var policyViews = new List<PolicyView>();

            foreach (var policyDB in policies)
            {
                var covers = _dbContext.CoversNew.Where(x => x.ProductId == policyDB.Id).ToList();
                var coverViews = new List<CoverView>();

                foreach (var cover in covers)
                {
                    var coverview = new CoverView();
                    var rateViews = new List<RateView>();

                    coverview.Id = cover.Id;
                    coverview.PolicyId = cover.ProductId;
                    coverview.LocaleBW = cover.LocaleBW;
                    coverview.Section = cover.Section;
                    coverview.CoverName = cover.CoverName;
                    coverview.AngularForm = cover.AngularForm;
                    coverview.SectionName = cover.SectionName;
                    coverview.Specification = cover.Specification;
                    coverview.CoverTip = cover.CoverTip;
                    coverview.ReferValue = cover.ReferValue;
                    coverview.CoverOrdinal = cover.CoverOrdinal;
                    coverview.CoverHeader = cover.CoverHeader;
                    coverview.CoverType = cover.CoverType;


                    var ratesList = cover.RatesNew.Where(x => x.RateTypeId == RateType.Indemnity || x.RateTypeId == RateType.Premium ||
                    x.RateTypeId == RateType.SumInsured || x.RateTypeId == RateType.Excess || x.RateTypeId == RateType.Fee || x.RateTypeId == RateType.TurnoverTable).ToList();

                    foreach (var rate in ratesList)
                    {
                        string locale = "";

                        //indemnity show value as well name of rate
                        if (rate.RateTypeId == RateType.Indemnity)
                        {
                            rate.RateName = $"{rate.RateName} {rate.Indemnity}";
                        }

                        if (rate.RateTypeId == RateType.Excess)
                        {
                            rate.RateName = $"{rate.RateName} Excess";
                        }

                        if (rate.RateTypeId == RateType.TurnoverTable)
                        {
                            rate.RateName = $"{rate.RateName} {rate.Indemnity}";
                        }

                        if (rate.LocaleBW == 1)
                            locale = "UK Only";
                        else if (rate.LocaleBW == 2)
                            locale = "Ireland Only";
                        else if (rate.LocaleBW == 3)
                            locale = "Ireland and UK";

                        rate.RateName = $"{rate.RateName} {"     "} {locale}";


                        var rateview = ViewMapper.MapRateView(rate);
                        rateViews.Add(rateview);
                    }

                    var ordereredRates = rateViews.OrderBy(x => x.Ordinal).ToList();
                    coverview.Rates = ordereredRates;
                    coverViews.Add(coverview);
                }

                var policyView = new PolicyView
                {
                    Id = policyDB.Id,
                    PolicyName = policyDB.ProductName,
                    TamType = policyDB.TamType,
                    Ordinal = policyDB.Ordinal,
                    LeisureInsureFee = policyDB.LeisureInsureFee,
                    BrokerCommission = policyDB.BrokerCommission,
                    PeriodYear = policyDB.PeriodDay,
                    PeriodDay = policyDB.PeriodDay,
                    PolicyType = policyDB.PolicyType,
                    PolicyGroupFK = policyDB.PolicyGroupFK,
                    PremiseRequired = policyDB.PremiseRequired,
                    TimeRequired = policyDB.TimeRequired,
                    TamOccupation = policyDB.TamOccupation,
                    ClauseCode = policyDB.ClauseCode,
                    Excess = policyDB.Excess,
                    BusinessDescription = policyDB.BusinessDescription,
                    CertificateBody = policyDB.CertificateBody,
                    Covers = coverViews
                };

                policyViews.Add(policyView);
            }

            return policyViews;
        }

        #endregion

        #region Documents


        /// <summary>
        /// Used to display links on payment page
        /// </summary>
        /// <param name="quotepk"></param>
        /// <returns></returns>  
        public List<PolicyDocuments> GetDocsForPolicy(int quotepk, bool legalAdded)
        {
            var details = new List<PolicyDocuments>();
            try
            {
                var quote = _dbContext.Quotes.Where(x => x.QuotePK == quotepk).FirstOrDefault();
                var policy = _dbContext.Products.FirstOrDefault(x => x.Id == quote.PolicyFK);
                var mainCover = policy.CoversNew.Where(x => x.CoverType == "Mandatory").FirstOrDefault();
                var policyDocs = _dbContext.PolicyDocs.Where(x => x.ProductId == quote.PolicyFK);
                //get any items added to the main mandatory cover 
                var quoteline = _dbContext.QuoteLineNew.FirstOrDefault(x => x.QuoteId == quote.QuotePK && x.CoverId == mainCover.Id);
                var allDocs = _dbContext.Documents.ToList();
                //get actual documents 
                var mandatoryDocs = policyDocs.Select(x => x.Documents).ToList();
                //select docs for this locale
                var docs = mandatoryDocs.Where(x => (x.LocaleBW & quote.LocaleId) > 0).ToList();

                var quoteInputs = _dbContext.QuoteInputNew.Where(x => x.QuoteId == quotepk);

                var quotelines = _dbContext.QuoteLineNew.Where(x => x.QuoteId == quote.QuotePK);
                var mdCovers = _dbContext.CoversNew.Where(x => x.CoverTypeId == 3);

                if (policy.ClauseCode != null)
                {
                    docs.Add(allDocs.Where(x => x.ClauseCode == policy.ClauseCode).FirstOrDefault());
                }

                //get clauses from MD Covers
                var mdClauses = (from ql in quotelines
                                 join md in mdCovers on ql.CoverId equals md.Id
                                 where (ql.Net > 0)
                                 select new
                                 {
                                     ClauseCode = md.ClauseCode,
                                     Desc = ql.SectionDescription
                                 }).ToList();

                mdClauses = mdClauses.DistinctBy(x => x.ClauseCode).ToList();

                foreach (var item in mdClauses)
                {
                    docs.Add(allDocs.Where(x => x.ClauseCode == item.ClauseCode).FirstOrDefault());
                }

                //get clauses from hazard questions (yes or no) 18  / premium types 10 / turnover table 48 / indemnity table 5
                var rateClauses = (from qi in quoteInputs
                                   where qi.RateValue == "yes" && (qi.RatesNew.RateTypeId == 18 || qi.RatesNew.RateTypeId == 23)
                                   || (!string.IsNullOrEmpty(qi.RateValue) && qi.RatesNew.RateTypeId == 10) 
                                         && qi.RatesNew.ClauseCode != null
                                   select new
                                   {
                                       ClauseCode = qi.RatesNew.ClauseCode,
                                       Answer = qi.RateValue,
                                       Desc = ""
                                   }).ToList();

                //terms of business, these are not mandatory but variable for cliens and agents
                if (quote.IsAgent)
                    docs.Add(allDocs.FirstOrDefault(x => x.DocumentPK == 83));
                else
                    docs.Add(allDocs.FirstOrDefault(x => x.DocumentPK == 84));


                if (quoteline.QuoteListItem.Count() > 0)
                {
                    var itemclauses = (from li in quoteline.QuoteListItem
                                       join r in _dbContext.RatesNew on li.RateId equals r.Id
                                       where r.ClauseCode != null
                                       select new
                                       {
                                           ClauseCode = r.ClauseCode,
                                           Desc = li.ItemDescription
                                       }).ToList();

                    itemclauses = itemclauses.DistinctBy(x => x.ClauseCode).ToList();

                    foreach (var item in itemclauses)
                    {
                        docs.Add(allDocs.Where(x => x.ClauseCode == item.ClauseCode).FirstOrDefault());
                    }
                }


                if (rateClauses.Count > 0)
                {
                    foreach (var clause in rateClauses)
                    {
                        docs.Add(allDocs.Where(x => x.ClauseCode == clause.ClauseCode).FirstOrDefault());
                    }
                }

                if (legalAdded)
                {
                    var legalDocs = allDocs.Where(x => x.DocType.Contains("Legal")).ToList();

                    docs.AddRange(legalDocs);
                }

                var website = CloudConfigurationManager.GetSetting("website.address");

                foreach (var doc in docs)
                {
                    var detail = new PolicyDocuments();
                    detail.LinkType = doc.DocType;

                    switch (doc.DocType)
                    {
                        case "Keyfacts":
                            detail.DocType = "Policy Summary";
                            detail.DocLink = $"{website}/Content/CertificateDocuments/{doc.DocLink}.pdf";
                            detail.MustRead = false;
                            break;
                        case "Wording":
                            detail.DocType = "Policy Wording";
                            detail.DocLink = $"{website}/Content/CertificateDocuments/{doc.DocLink}.pdf";
                            detail.MustRead = false;
                            break;
                        case "Clause":
                            detail.DocType = doc.DocName;
                            detail.DocLink = $"{website}/Content/CertificateDocuments/{doc.DocLink}.pdf";
                            detail.MustRead = true;
                            break;
                        case "Terms":
                            detail.DocType = "Terms Of Business";
                            detail.DocLink = $"{website}/Content/CertificateDocuments/{doc.DocLink}.pdf";
                            detail.MustRead = true;
                            break;
                        case "Legal Keyfacts":
                            detail.DocType = "Legal Keyfacts";
                            detail.DocLink = $"{website}/Content/CertificateDocuments/{doc.DocLink}.pdf";
                            detail.MustRead = false;
                            break;
                        case "Legal Wording":
                            detail.DocType = "Legal Wording";
                            detail.DocLink = $"{website}/Content/CertificateDocuments/{doc.DocLink}.pdf";
                            detail.MustRead = false;
                            break;
                        case "SoF":
                            detail.DocType = "Statement Of Fact";
                            detail.DocLink = $"GetNew{doc.DocLink}/LEI00{quotepk.ToString()}";
                            detail.MustRead = true;
                            break;
                        default:
                            break;
                    }

                    details.Add(detail);
                }

                return details;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Get relevant data for our schedule
        /// </summary>
        /// <param name="quoteId"></param>
        /// <returns></returns>
        public vmCertSchedule GetCertSchedule(int quoteId)
        {
            var quote = _dbContext.Quotes.FirstOrDefault(x => x.QuotePK == quoteId);
            var policy = _dbContext.Products.FirstOrDefault(x => x.Id == quote.PolicyFK);
            var contact = _dbContext.Contacts.FirstOrDefault(x => x.ContactPK == quote.CustomerFK);
            var address = _dbContext.Addresses.FirstOrDefault(x => x.ContactFK == contact.ContactPK);
            var territory = "United Kingdom of Great Britain & Northern Ireland, the Isle of Man & the Channel Isles";
            var currency = LocaleHelper.GetCurrency(quote.LocaleId);

            var quoteEvent = new QuoteEngine();

            if (policy.PolicyType == "E")
            {
                quoteEvent = quote.QuoteEngine.FirstOrDefault();
            }

            try
            {
                //get docs for this policy
                var policyDocs = _dbContext.PolicyDocs.Where(x => x.ProductId == quote.PolicyFK);
                //get actual documents 
                var mandatoryDocs = policyDocs.Select(x => x.Documents).ToList();
                //select docs for this locale
                var docs = mandatoryDocs.Where(x => (x.LocaleBW & quote.LocaleId) > 0).ToList();
                var wording = docs.FirstOrDefault(x => x.DocType == "Wording");
                string premises = "";


                var correspondanceAddy = address.Address1;
                if (address.Address2 != null)
                    correspondanceAddy += $" {address.Address2}";
                if (address.Address3 != null)
                    correspondanceAddy += $" {address.Address3}";
                correspondanceAddy += $" {address.County}";
                correspondanceAddy += $" {address.Country}";

                if (quote.LocaleId == 1)
                    correspondanceAddy += $" {address.Postcode.ToUpper()}";

                //getting dates and times from quote, not the stored proc
                var vmCert = new vmCertSchedule();
                //need to be done when we get events               
                var venue = "";
                var contingency = 0;
                var taxRate = ((decimal)quote.TaxRate * 100);
                taxRate = Math.Round(taxRate, 2);

                var taxType = $"Insurance Premium Tax {taxRate}%";

                decimal legalGross = quote.TotalLegal ?? 0;
                string legalAsStr = $"{currency}{string.Format("{0:n}", legalGross)}";

                var additional = new List<additionalCovered>();

                vmCert.QuoteReference = quote.QuoteRef;
                vmCert.Business = quote.BusinessDescription;
                vmCert.CorrespondenceAddress = correspondanceAddy;
                vmCert.EndDate = quote.DateTo;
                vmCert.StartDate = quote.DateFrom;
                vmCert.TimeFrom = quote.TimeFrom;
                vmCert.TimeTo = quote.TimeTo;
                vmCert.GrossPremium = $"{currency}{string.Format("{0:n}", quote.Total)}";
                vmCert.InsuredName = contact.EntityName;
                vmCert.NetPremium = $"{currency}{string.Format("{0:n}", quote.NetPremium)}";
                vmCert.GrossLegal = legalAsStr;
                vmCert.PolicyNumber = quote.QuotePK.ToString();
                vmCert.PolicyWording = wording.OriginalFilename;
                vmCert.Premises = premises;
                vmCert.Tax = $"{currency}{string.Format("{0:n}", quote.Tax)}";
                vmCert.TerritorialLimits = territory;
                vmCert.TaxType = taxType;
                vmCert.Fee = $"{currency}{string.Format("{0:n}", quote.Fee)}";
                vmCert.Purchased = quote.Purchased;
                vmCert.Contingency = contingency;
                vmCert.PolicyType = quote.PolicyType;
                vmCert.PurchasedDate = quote.DatePurchased;
                vmCert.EventName = quoteEvent.EventName;
                vmCert.Attendance = quoteEvent.NoVisitors.ToString();
                vmCert.additionalInsured = additional;
                vmCert.PolicyPK = quote.PolicyFK ?? 0;
                vmCert.Venue = venue;
                vmCert.LocalePK = quote.LocaleId;

                return vmCert;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        /// <summary>
        /// Used to get clauses for certificate
        /// </summary>
        /// <param name="quotepk"></param>
        /// <returns></returns>
        public List<PolicyDocuments> GetClausesForCert(int quotepk)
        {
            var details = new List<PolicyDocuments>();
            var quote = _dbContext.Quotes.FirstOrDefault(x => x.QuotePK == quotepk);
            var policy = _dbContext.Products.FirstOrDefault(x => x.Id == quote.PolicyFK);
            var allDocs = _dbContext.Documents.Where(x => (x.LocaleBW & quote.LocaleId) > 0).ToList();
            var quoteInputs = _dbContext.QuoteInputNew.Where(x => x.QuoteId == quotepk);
            var quotelines = _dbContext.QuoteLineNew.Where(x => x.QuoteId == quote.QuotePK);
            var mdCovers = _dbContext.CoversNew.Where(x => x.CoverTypeId == 3);

            var manualClauses = _dbContext.QuoteClause.Where(x => x.QuoteId == quote.QuotePK && x.CustomClauseName == null).ToList();

            try
            {

                //get clauses from MD Covers
                var mdClauses = (from ql in quotelines
                                 join md in mdCovers on ql.CoverId equals md.Id
                                 where (ql.Net > 0)
                                 select new
                                 {
                                     ClauseCode = md.ClauseCode,
                                     Answer = "",
                                     Desc = ql.SectionDescription
                                 }).ToList();

                mdClauses = mdClauses.DistinctBy(x => x.ClauseCode).ToList();

                //get clauses from hazard questions (yes or no) 18
                //premium types 10
                var rateClauses = (from qi in quoteInputs
                                   where qi.RateValue == "yes" && (qi.RatesNew.RateTypeId == 18 || qi.RatesNew.RateTypeId == 23)
                                   || (!string.IsNullOrEmpty(qi.RateValue) && qi.RatesNew.RateTypeId == 10)
                                   select new
                                   {
                                       ClauseCode = qi.RatesNew.ClauseCode,
                                       Answer = qi.RateValue,
                                       Desc = ""
                                   }).ToList();

                if (policy.ClauseCode != null)
                {
                    rateClauses.Add(new { ClauseCode = policy.ClauseCode, Answer = "", Desc = "" });
                }

                rateClauses.AddRange(mdClauses);

                //have we added a clause on quote engine
                foreach (var manClause in manualClauses)
                {
                    var quoteClause = new { ClauseCode = manClause.ClauseCode, Answer = "", Desc = "" };
                    rateClauses.Add(quoteClause);
                }


                //get clauses from any items added to mandatory cover such as inflatables           
                var mainCover = policy.CoversNew.FirstOrDefault(x => x.CoverType == "Mandatory");
                var quoteline = _dbContext.QuoteLineNew.FirstOrDefault(x => x.QuoteId == quote.QuotePK && x.CoverId == mainCover.Id);

                if (quoteline != null)
                {
                    if (quoteline.QuoteListItem.Any())
                    {
                        //join our items back to rates to get our clause codes
                        var itemclauses = (from li in quoteline.QuoteListItem
                                           join r in _dbContext.RatesNew on li.RateId equals r.Id
                                           where r.ClauseCode != null
                                           select new
                                           {
                                               ClauseCode = r.ClauseCode,
                                               Answer = "",
                                               Desc = li.ItemDescription
                                           }).ToList();

                        itemclauses = itemclauses.DistinctBy(x => x.ClauseCode).ToList();

                        rateClauses.AddRange(itemclauses);

                    }
                }

                if (rateClauses.Count > 0)
                {
                    foreach (var clause in rateClauses)
                    {
                        var policyDoc = new PolicyDocuments();
                        var doc = allDocs.FirstOrDefault(x => x.ClauseCode == clause.ClauseCode);
                        if (doc != null)
                        {
                            policyDoc.DocLink = $"{doc.DocLink}.pdf"; ;
                            details.Add(policyDoc);
                        }
                    }
                }

                return details;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Get hazards for certificate eg nightclub question
        /// </summary>
        /// <param name="quotepk"></param>
        /// <returns></returns>
        public vmCertHazardList GetHazardsForCert(int quotepk)
        {
            try
            {
                var hazardQuestions = new vmCertHazardList();

                var quote = _dbContext.Quotes.Where(x => x.QuotePK == quotepk).FirstOrDefault();

                var quoteInputs = _dbContext.QuoteInputNew.Where(x => x.QuoteId == quotepk);

                var rateHazards = (from qi in quoteInputs
                                   join r in _dbContext.RatesNew on qi.RateId equals r.Id
                                   where r.RateTypeId == 18
                                   select new vmCertHazards
                                   {
                                       Question = r.RateName,
                                       Answer = qi.RateValue
                                   }).ToList();

                foreach (var haz in rateHazards)
                {
                    if (haz.Answer == "no")
                        haz.Answer = "No";
                    else if (haz.Answer == "yes")
                        haz.Answer = "Yes";
                }

                hazardQuestions.Hazards = rateHazards;

                return hazardQuestions;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Combine sections and headers for cert body
        /// </summary>
        /// <param name="quotePk"></param>
        /// <returns></returns>
        public IEnumerable<vmSectionHeaders> GetCertSectionsAndHeaders(int quotePk)
        {

            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quotePk).FirstOrDefault();

            var sectionHeaders = new List<vmSectionHeaders>();

            try
            {
                var headers = GetCertHeaders(quotePk);
                var sectionLists = GetCertLists(quote.QuotePK);
                var certificateSections = GetCertSections(headers, quote.QuotePK);

                var sections = new List<vmSections>();

                foreach (var s in certificateSections)
                {
                    //professional cover for irish should be 1.3M - replace this hack
                    //if (s.Section == 8 && s.LineGroup == 2 && quote.LocaleId == 2)
                    //{
                    //    s.LineValue1 = "€1,300,000 any one <b>Occurrence</b> and in the aggregate";
                    //}

                    sections.Add(new vmSections
                    {
                        HeaderLevel = s.HeaderLevel,
                        Section = s.Section ?? 0,
                        Insured = s.Insured,
                        Label1 = s.Label1,
                        Label2 = s.Label2,
                        LineValue1 = s.LineValue1,
                        LineValue2 = s.LineValue2,
                        LineGroup = s.LineGroup,
                        ordinal = s.ordinal,
                        Para = s.Para ?? 0,
                        ParagraphType = s.ParagraphType ?? 0
                    });
                }

                foreach (var h in headers)
                {
                    var sectionHeader = new vmSectionHeaders();
                    sectionHeader.Header = h.Header;
                    sectionHeader.HeaderLevel = h.HeaderLevel;
                    sectionHeader.Insured = h.Insured;
                    sectionHeader.Section = h.Section ?? 0;
                    sectionHeader.SectionDetail = sections.Where(x => x.Section == h.Section && h.Insured > 0);
                    sectionHeader.SectionLists = sectionLists.Where(x => x.Section == h.Section && h.Insured > 0);

                    sectionHeaders.Add(sectionHeader);

                }

                return sectionHeaders;
            }
            catch (Exception ex)
            {

                throw;
            }


        }

        /// <summary>
        /// Get all cover headers for body and check if insured
        /// </summary>
        /// <param name="quoteid"></param>
        /// <returns></returns>
        public List<CertHeader> GetCertHeaders(int quoteid)
        {
            var certheaders = new List<CertHeader>();

            try
            {
                var quote = _dbContext.Quotes.Where(x => x.QuotePK == quoteid).FirstOrDefault();
                var policy = _dbContext.Products.Where(x => x.Id == quote.PolicyFK).FirstOrDefault();

                var territory = LocaleHelper.GetLocale(quote.LocaleId);

                //get headers 
                var headers = _dbContext.SectionHeadersNew.Where(x => x.ProductId == policy.Id).ToList();

                //get selected covers
                var quoteLines = _dbContext.QuoteLineNew.Where(x => x.QuoteId == quoteid).ToList();
                var quoteInputs = _dbContext.QuoteInputNew.Where(x => x.QuoteId == quoteid).ToList();
                //get covers for this policy
                var covers = policy.CoversNew.ToList();


                //go through all potential covers and show what we have added
                foreach (var header in headers)
                {

                    string coverdesc = "";
                    var certheader = new CertHeader();
                    var coversForHeader = covers.Where(x => x.CoverTypeId == header.CoverTypeId).ToList();
                    coverdesc = header.Description;

                    //some headers (MD on paintball) need to show multiple covers                    
                    foreach (var cover in coversForHeader)
                    {
                        var addedcover = quoteLines.Where(x => x.CoverId == cover.Id).FirstOrDefault();

                        if (addedcover != null)
                        {
                            if (addedcover.Required)
                            {
                                certheader.Header = $"{coverdesc}: Insured";
                                certheader.Insured = cover.Id;
                                break;
                            }
                            else
                            {
                                certheader.Header = $"{coverdesc}: Not Insured";
                                certheader.Insured = 0;
                            }
                        }
                        else
                        {
                            certheader.Header = $"{coverdesc}: Not Insured";
                            certheader.Insured = 0;
                        }
                    }

                    if (coversForHeader.Count() == 0)
                    {
                        certheader.Header = $"{coverdesc}: Not Insured";
                        certheader.Insured = 0;
                    }

                    certheader.Ordinal = header.Ordinal;
                    certheader.HeaderLevel = 1;
                    certheader.Section = header.SectionNumber;
                    certheader.CoverTypeId = header.CoverTypeId ?? 0;
                    //if (coverdesc == "Cancellation")
                    //    coverdesc = "Contingency";                               
                    certheaders.Add(certheader);
                }


                certheaders = certheaders.OrderBy(x => x.Ordinal).ToList();

                return certheaders;
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw new Exception(ex.Message);
            }


        }

        /// <summary>
        /// Get paragraphs (sections) for each header 
        /// </summary>
        /// <param name="quoteid"></param>
        /// <returns></returns>
        public List<CertSection> GetCertSections(List<CertHeader> headers, int quoteid)
        {
            var allCertsections = new List<CertSection>();

            try
            {
                var quote = _dbContext.Quotes.Where(x => x.QuotePK == quoteid).FirstOrDefault();
                var locale = quote.LocaleId;
                var policy = _dbContext.Products.Where(x => x.Id == quote.PolicyFK).FirstOrDefault();
                var covers = _dbContext.CoversNew.Where(x => x.ProductId == policy.Id).ToList();
                var territory = LocaleHelper.GetLocale(quote.LocaleId);
                var currency = LocaleHelper.GetCurrency(quote.LocaleId);
                var sectionHeaders = _dbContext.SectionHeaderLines.ToList();

                var quoteInputs = _dbContext.QuoteInputNew.Where(x => x.QuoteId == quoteid).ToList();
                var quoteLines = _dbContext.QuoteLineNew.Where(x => x.QuoteId == quoteid).ToList();
                var numutil = new NumberAbbreviator();

                foreach (var header in headers)
                {
                    //get all sections for each header
                    var sections = sectionHeaders.Where(x => x.SectionNumber == header.Section).ToList();
                    var cover = covers.Where(x => x.CoverTypeId == header.CoverTypeId).FirstOrDefault();

                    if (header.Header.ToLower().Contains("not insured"))
                        continue;
                    //var line = quoteLines.Where(x => x.CoverId == cover.Id).FirstOrDefault();

                    var line = quoteLines.Where(x => x.CoversNew.CoverTypeId == header.CoverTypeId).FirstOrDefault();

                    var certsections = new List<CertSection>();

                    foreach (var section in sections)
                    {

                        string span1 = "", span2 = "";
                        if (locale == 1)
                        {
                            span1 = section.SpanUK1;
                            span2 = section.SpanUK2;
                        }
                        else if (locale == 2)
                        {
                            span1 = section.SpanEire1;
                            span2 = section.SpanEire2;
                        }

                        //excess
                        if (section.RateTypeFK == 7)
                        {
                            var dec = (decimal)line.Excess;
                            var excessRounded = Math.Round(dec, 0);
                            var format = String.Format("{0:n0}", excessRounded);
                            span1 = span1.Replace("{value}", format);

                            decimal decSub;
                            if (line.Subsidence != null)
                                decSub = (decimal)line.Subsidence;
                            else
                                decSub = 2500;
                            var subRounded = Math.Round(decSub, 0);
                            var subformat = String.Format("{0:n0}", subRounded);
                            span1 = span1.Replace("{subsidence}", subformat);

                        }

                        //if this is a policy aggregate section show main limit of cover
                        if (section.SectionNumber == 22)
                        {
                            line = quoteLines.Where(x => x.CoverId == 42).FirstOrDefault();
                        }
                        //number of locations
                        if (section.SectionNumber == 23)
                        {
                            var location = quoteInputs.Where(x => x.RateId == 4235).FirstOrDefault();
                            span1 = span1.Replace("{value}", location.RateValue);
                        }
                        //indemnity period
                        if (section.SectionNumber == 24)
                        {
                            var indemperiod = quoteInputs.Where(x => x.RateTypeId == 40).FirstOrDefault();
                            span1 = span1.Replace("{value}", indemperiod.RateValue);
                        }

                        //we have a coverPK to get indemnity  
                        if (section.QuoteLineField == "Indemnity")
                        {
                            var dec = (decimal)line.Indemnity;
                            var indem = Math.Round(dec, 0);
                            var format = String.Format("{0:n0}", indem);
                            if (!String.IsNullOrEmpty(span1))
                                span1 = span1.Replace("{value}", format);
                            if (!String.IsNullOrEmpty(span2))
                                span2 = span2.Replace("{value}", format);
                            section.Label1 = section.Label1.Replace("{value}", format);
                        }

                        if (section.QuoteLineField == "Turnover")
                        {
                            var turnoverInput = line.QuoteInputNew.FirstOrDefault(x => x.RateTypeId == 10);
                            string turnover = "";
                            if (turnoverInput != null)
                                turnover = turnoverInput.RateValue;
                            else
                                turnover = $"{currency}{String.Format("{0:n0}", line.Turnover)}";

                            if (!String.IsNullOrEmpty(span1))
                                span1 = span1.Replace("{value}", turnover);
                            if (!String.IsNullOrEmpty(span2))
                                span2 = span2.Replace("{value}", turnover);
                            section.Label1 = section.Label1.Replace("{value}", turnover);


                        }


                        var certsection = new CertSection();

                        certsection.Header = header.Header ?? "";
                        certsection.HeaderLevel = header.HeaderLevel;
                        certsection.Section = section.SectionNumber;
                        certsection.Label1 = section.Label1 ?? "";
                        certsection.Label2 = section.Label2 ?? "";
                        certsection.LineValue1 = span1 ?? "";
                        certsection.LineGroup = section.LineGroup;
                        certsection.ordinal = section.Ordinal;
                        certsection.Para = section.ParaAfter;
                        certsection.LineValue2 = span2 ?? "";
                        certsection.ParagraphType = section.ParagraphType;

                        certsections.Add(certsection);
                    }

                    certsections = certsections.OrderBy(x => x.ordinal).ToList();

                    allCertsections.AddRange(certsections);
                }

                return allCertsections;
            }
            catch (Exception ex)
            {
                var error = ex.Message;

                throw new Exception(ex.Message);
            }


        }

        /// <summary>
        /// Get added items such as equipment/employee for our cert body
        /// </summary>
        /// <param name="quoteid"></param>
        /// <returns></returns>
        public List<vmCertLists> GetCertLists(int quoteid)
        {
            var certLists = new List<vmCertLists>();

            try
            {
                var quote = _dbContext.Quotes.Where(x => x.QuotePK == quoteid).FirstOrDefault();
                var policy = _dbContext.Products.Where(x => x.Id == quote.PolicyFK).FirstOrDefault();
                var territory = LocaleHelper.GetLocale(quote.LocaleId);
                var currency = LocaleHelper.GetCurrency(quote.LocaleId);
                var headers = _dbContext.SectionHeadersNew.Where(x => x.ProductId == policy.Id).ToList();

                var mainCover = policy.CoversNew.Where(x => x.CoverType == "Mandatory").FirstOrDefault();
                var mainQuoteline = _dbContext.QuoteLineNew.FirstOrDefault(x => x.QuoteId == quote.QuotePK && x.CoverId == mainCover.Id);

                var quotelines = _dbContext.QuoteLineNew.Where(x => x.QuoteId == quote.QuotePK).ToList();

                int mdRow = 0;
                int biRow = 0;
                int elRow = 0;
                int plRow = 0;

                foreach (var quoteline in quotelines)
                {


                    var headerCover = headers.FirstOrDefault(x => x.CoverTypeId == quoteline.CoversNew.CoverTypeId);

                    //does this cover have list items? eg equipment,employee types
                    if (quoteline.QuoteListItem.Any())
                    {

                        foreach (var item in quoteline.QuoteListItem)
                        {
                            plRow++;
                            bool adding = true;

                            var certList = new vmCertLists();
                            //PL
                            if (quoteline.CoversNew.CoverTypeId == 1)
                            {
                                if (item.Width > 0 && item.Length > 0)
                                {
                                    var width = (decimal)item.Width;
                                    var length = (decimal)item.Width;
                                    certList.LineType = $"{item.Quantity} x {item.ItemDescription} {width.ToString("#")}' x {length.ToString("#")}'";
                                }
                                else
                                {
                                    if (item.Quantity != null)
                                        certList.LineType = $"{item.Quantity} x {item.ItemDescription}";
                                    else
                                        certList.LineType = $"{item.ItemDescription}";
                                }
                                certList.RowNo = plRow;
                            }
                            //EL
                            if (quoteline.CoversNew.CoverTypeId == 2)
                            {
                                elRow++;
                                if (item.ItemDescription != null)
                                    certList.LineType = $"{item.ItemDescription}";
                                else
                                    certList.LineType = $"{item.Activity}";
                                if (item.RateValue != null)
                                    certList.WageRoll = item.RateValue;
                                else
                                {
                                    decimal wageroll = item.WageRoll ?? 0;
                                    certList.WageRoll = $"{currency}{wageroll.ToString("#,0")}";
                                }

                                certList.RowNo = elRow;
                            }

                            //MD
                            if (quoteline.CoversNew.CoverTypeId == 3)
                            {
                                mdRow++;
                                if (item.SumInsured > 0)
                                {
                                    if (item.Width > 0 && item.Length > 0)
                                    {
                                        var width = (decimal)item.Width;
                                        var length = (decimal)item.Width;
                                        certList.LineType = $"{item.Quantity} x {item.ItemDescription} {width.ToString("#")}' x {length.ToString("#")}'";
                                    }
                                    else
                                    {
                                        if (item.Quantity != null)
                                            certList.LineType = $"{item.Quantity} x {item.ItemDescription}";
                                        else
                                            certList.LineType = $"{item.ItemDescription}";
                                    }

                                    var sumIns = (decimal)item.SumInsured;
                                    certList.SumInsured = $"{currency}{sumIns.ToString("#,0")}";
                                    certList.Specification = quoteline.CoversNew.Specification;
                                    certList.RowNo = mdRow;
                                }
                                else
                                    adding = false;
                            }

                            certList.QuoteInputPK = item.Id;
                            certList.Section = headerCover.SectionNumber ?? 0;
                            certList.Territory = territory;

                            if (adding)
                                certLists.Add(certList);
                        }

                    }
                    else if (quoteline.QuoteListItem.Count() == 0)
                    {
                        //no list items but have added cover
                        if (quoteline.Net > 0 || quoteline.Total > 0)
                        {
                            //MD
                            if (quoteline.CoversNew.CoverTypeId == 3)
                            {
                                var inputs = quoteline.QuoteInputNew.Where(x => x.RateTypeId == 10).ToList();

                                var certList = new vmCertLists();
                                var spec = quoteline.CoversNew.Specification;
                                decimal totalSumInsured = 0;
                                string description = "";

                                foreach (var item in inputs)
                                {
                                    //so far personal assault (10,000 cover) is the only checkbox with an MD cover
                                    if (item.RateValue == "true" || item.RateValue == "false")
                                    {
                                        if (item.RateValue == "false")
                                            continue;
                                        else
                                        {
                                            totalSumInsured = 10000;
                                            item.RateName = "Personal Assault Cover";
                                            var pacover = _dbContext.CoversNew.FirstOrDefault(x =>
                                                x.Id == item.RatesNew.ExtraCoverId);
                                            spec = pacover.Specification;
                                        }
                                    }
                                    else
                                        totalSumInsured = StringToDec(item.RateValue);

                                    description = item.RateName;
                                }

                                mdRow++;
                                certList.Section = headerCover.SectionNumber ?? 0;
                                certList.Territory = territory;
                                certList.RowNo = mdRow;
                                certList.SumInsured = $"{currency}{totalSumInsured.ToString("#,0")}";
                                certList.Specification = spec;
                                certList.LineType = description;
                                certLists.Add(certList);

                            }

                            //BI
                            if (quoteline.CoversNew.CoverTypeId == 22)
                            {
                                biRow++;
                                var certList = new vmCertLists();
                                certList.Section = headerCover.SectionNumber ?? 0;
                                certList.Territory = territory;
                                certList.RowNo = biRow;
                                //get quote inputs (premiums) 
                                decimal totalBi = 0;
                                var inputs = quoteline.QuoteInputNew.Where(x => x.RateTypeId == 10).ToList();
                                var indemnityPeriod = quoteline.QuoteInputNew.FirstOrDefault(x => x.RateTypeId == 40);
                                //sometime we have more than one input
                                foreach (var item in inputs)
                                {
                                    if (item.RateValue != null)
                                        totalBi += StringToDec(item.RateValue);
                                    else
                                        totalBi += item.RateValueNum ?? 0;
                                }
                                certList.IndemnityPeriod = indemnityPeriod.RateValue;
                                certList.SumInsured = $"{currency}{totalBi.ToString("#,0")}";
                                certList.LineType = quoteline?.CustomDescription ?? quoteline.CoversNew.CoverHeader;

                                certList.Specification = quoteline.CoversNew.Specification;
                                certLists.Add(certList);

                            }
                        }
                    }
                }

                return certLists;
            }
            catch (Exception ex)
            {
                var error = ex.Message;

                throw new Exception(ex.Message);
            }


        }


        #endregion

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _dbContext.Dispose();
            }

            // Free any unmanaged objects here.
            //
            _disposed = true;
        }

        public static decimal StringToDec(string stringwithnum)
        {
            var digits = digitsOnly.Replace(stringwithnum, "");
            return Convert.ToDecimal(digits);
        }
    }
}