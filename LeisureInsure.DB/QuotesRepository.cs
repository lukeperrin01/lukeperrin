﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.DB.Models;
using System.Data;
using System.Data.SqlClient;
using LeisureInsure.DB.Business;
using LeisureInsure.DB.Utilities;
using LeisureInsure.DB.LinqClasses;
using LeisureInsure.Services.Utilities;
using System.Text.RegularExpressions;
using System.Diagnostics;
using LeisureInsure.DB.Logging;
using LeisureInsure.Services.Services.Utilities;

namespace LeisureInsure.DB
{
    public class QuotesRepository
    {
        private readonly LeisureInsureEntities _dbContext;
        private bool _disposed;

        public QuotesRepository()
        {
            _dbContext = new LeisureInsureEntities();
        }

        public Quotes UpdateQuote(Quotes quote)
        {
            var updatedQuote = _dbContext.Quotes.FirstOrDefault(x => x.QuotePK == quote.QuotePK);
            //updatedQuote = quote;
            updatedQuote.NetPremium = 50;
            updatedQuote.LandingPageId = 10;

            _dbContext.SaveChanges();

            return quote;
        }

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _dbContext.Dispose();
            }

            // Free any unmanaged objects here.
            //
            _disposed = true;
        }
    }
}