﻿using System.Collections.Generic;
using LeisureInsure.DB.ViewModels;
using System.Linq;
using LeisureInsure.DB;
using System;

namespace LeisureInsure.DB.Utilities
{
    public static class ViewMapper
    {

        #region Map from DB to view models
        public static List<RateView> MapRateViews(List<RatesNew> rates)
        {
            var rateViews = new List<RateView>();

            foreach (var rate in rates)
            {
                var rateview = new RateView();
                rateview.Id = rate.Id;
                rateview.CoverId = rate.CoverId;
                rateview.InputTypeId = rate.InputTypeId;
                rateview.RateTypeId = rate.RateTypeId;
                rateview.Ordinal = rate.Ordinal;
                rateview.Display = rate.Display;
                rateview.LocaleBW = rate.LocaleBW;
                rateview.VisibleChild = rate.VisibleChild;
                rateview.Indemnity = rate.Indemnity;
                rateview.RateName = rate.RateName;
                rateview.Rate = rate.Rate;
                rateview.NilExcessRate = rate.NilExcessRate;
                rateview.DivBy = rate.DivBy;
                rateview.Threshold = rate.Threshold;
                rateview.Excess = rate.Excess;                
                rateview.RateTip = rate.RateTip;
                rateview.Refer = rate.Refer;
                rateview.CatRating = rate.CatRating;
                rateview.DiscountFirstRate = rate.DiscountFirstRate;
                rateview.DiscountSubsequentRate = rate.DiscountSubsequentRate;
                rateview.TableParentRatePK = rate.TableParentRatePK;
                rateview.TableFirstColumn = rate.TableFirstColumn;
                rateview.TableChildren = rate.TableChildren;
                rateview.InvertReferral = rate.InvertReferral;                              
                rateview.RateExclude = rate.RateExclude;
                rateview.VisibleParent = rate.VisibleParent;
                rateview.FirstRate = rate.FirstRate;
                rateview.PageNo = rate.PageNo;                
                rateview.ValidUntil = rate.ValidUntil;
                rateview.ClauseCode = rate.ClauseCode;
                rateview.ngDisabled = rate.ngDisabled;               
                rateview.LongRate = rate.LongRate;
                rateview.SumInsured = rate.SumInsured;
                rateview.MinimumRate = rate.MinimumRate;
                rateview.ngRequired = rate.ngRequired;
                rateview.ItemTypeId = rate.ItemTypeId;
                rateview.inputName = $"nme{rate.Id.ToString()}";
                rateview.SelectChild = rate.SelectChild;
                rateview.SelectParent = rate.SelectParent;
                rateview.ItemListId = rate.ItemListId;
                rateview.ExtraCoverId = rate.ExtraCoverId;
                rateview.Heightwidth = rate.heightwidth;
                rateview.ExtraInfo = rate.extraInfo;
                rateview.RateValueNum = rate.RateAsNum ?? 0;

                rateViews.Add(rateview);
            }

            return rateViews;
        }

        public static RateView MapRateView(RatesNew rate,int localeid = 0)
        {
            var rateview = new RateView();            

            if (localeid != 0)
            {                
                if (rate.RateTypeId == 5)
                {
                    var dec = (decimal)rate.Indemnity;
                    var excessRounded = Math.Round(dec, 0);
                    var format = String.Format("{0:n0}", excessRounded);
                    if (localeid == 1)
                        rateview.RateValue = $"£{format}";
                    if (localeid == 2)
                        rateview.RateValue = $"€{format}";
                    
                }

                if (rate.RateTypeId == 48)
                {
                    var dec = (decimal)rate.Turnover;
                    var excessRounded = Math.Round(dec, 0);
                    var format = String.Format("{0:n0}", excessRounded);
                    if (localeid == 1)
                        rateview.RateValue = $"£{format}";
                    if (localeid == 2)
                        rateview.RateValue = $"€{format}";
                }
            }            
            
            rateview.Id = rate.Id;
            rateview.CoverId = rate.CoverId;
            rateview.InputTypeId = rate.InputTypeId;
            rateview.RateTypeId = rate.RateTypeId;
            rateview.Ordinal = rate.Ordinal;
            rateview.Display = rate.Display;
            rateview.LocaleBW = rate.LocaleBW;
            rateview.VisibleChild = rate.VisibleChild;
            rateview.Indemnity = rate.Indemnity;
            rateview.Turnover = rate.Turnover;
            rateview.RateName = rate.RateName;
            rateview.Rate = rate.Rate;
            rateview.NilExcessRate = rate.NilExcessRate;
            rateview.DivBy = rate.DivBy;
            rateview.Threshold = rate.Threshold;
            rateview.Excess = rate.Excess;           
            rateview.RateTip = rate.RateTip;
            rateview.Refer = rate.Refer;
            rateview.CatRating = rate.CatRating;
            rateview.DiscountFirstRate = rate.DiscountFirstRate;
            rateview.DiscountSubsequentRate = rate.DiscountSubsequentRate;
            rateview.TableParentRatePK = rate.TableParentRatePK;
            rateview.TableFirstColumn = rate.TableFirstColumn;
            rateview.TableChildren = rate.TableChildren;
            rateview.InvertReferral = rate.InvertReferral;            
            rateview.RateExclude = rate.RateExclude;
            rateview.VisibleParent = rate.VisibleParent;
            rateview.FirstRate = rate.FirstRate;
            rateview.PageNo = rate.PageNo;            
            rateview.ValidUntil = rate.ValidUntil;
            rateview.ClauseCode = rate.ClauseCode;
            rateview.ngDisabled = rate.ngDisabled;            
            rateview.LongRate = rate.LongRate;
            rateview.SumInsured = rate.SumInsured;
            rateview.MinimumRate = rate.MinimumRate;
            rateview.ngRequired = rate.ngRequired;
            rateview.ItemTypeId = rate.ItemTypeId;
            rateview.inputName = $"nme{rate.Id.ToString()}";
            rateview.SelectChild = rate.SelectChild;
            rateview.SelectParent = rate.SelectParent;
            rateview.ItemListId = rate.ItemListId;
            rateview.ExtraCoverId = rate.ExtraCoverId;
            rateview.Heightwidth = rate.heightwidth;
            rateview.ExtraInfo = rate.extraInfo;
            rateview.RateValueNum = rate.RateAsNum ?? 0;


            return rateview;
        }

        public static CoverView MapCoverView(CoversNew cover)
        {
            var coverview = new CoverView();

            coverview.Id = cover.Id;
            coverview.PolicyId = cover.ProductId;
            coverview.LocaleBW = cover.LocaleBW;
            coverview.Section = cover.Section;
            coverview.CoverName = cover.CoverName;
            coverview.AngularForm = cover.AngularForm;
            coverview.SectionName = cover.SectionName;
            coverview.Specification = cover.Specification;
            coverview.CoverTip = cover.CoverTip;
            coverview.ReferValue = cover.ReferValue;
            coverview.CoverOrdinal = cover.CoverOrdinal;
            coverview.CoverHeader = cover.CoverHeader;
            coverview.CoverType = cover.CoverType;
            coverview.CoverTypeDesc = cover.CoverType1.Description;
            coverview.RequiredParent = cover.RequiredParent;
            coverview.RequiredChild = cover.RequiredChild;
            coverview.CoverTypeId = cover.CoverTypeId;
            coverview.OtherCoverAdded = cover.OtherCoverAdded;

            return coverview;
        }

        public static CoverView MapQuoteLineToView(CoverView coverview, QuoteLineNew quoteline)
        {
            coverview.LocaleBW = quoteline.CoversNew.LocaleBW;
            coverview.Section = quoteline.CoversNew.Section;
            coverview.CoverName = quoteline.CoversNew.CoverName;
            coverview.AngularForm = quoteline.CoversNew.AngularForm;
            coverview.SectionName = quoteline.CoversNew.SectionName;
            coverview.Specification = quoteline.CoversNew.Specification;
            coverview.CoverTip = quoteline.CoversNew.CoverTip;
            coverview.ReferValue = quoteline.CoversNew.ReferValue;
            coverview.CoverOrdinal = quoteline.CoversNew.CoverOrdinal;
            coverview.CoverHeader = quoteline.CoversNew.CoverHeader;
            coverview.CustomDescription = quoteline.CustomDescription;
            coverview.CoverType = quoteline.CoversNew.CoverType;
            //specific info for quote
            coverview.SumInsured = quoteline.SumInsured;
            coverview.Tax = quoteline.Tax;
            coverview.Net = quoteline.Net;
            coverview.Total = quoteline.Total;
            coverview.LeisureInsureFee = quoteline.Fee;
            coverview.Indemnity = quoteline.Indemnity;
            coverview.SumInsured = quoteline.SumInsured;
            coverview.Excess = quoteline.Excess;
            coverview.ShowInSummary = quoteline.ShowInSummary ?? true;

            coverview.ErnExempt = quoteline.ErnExempt ?? true;
            //note, we want id of the actual cover, not quoteline
            coverview.Id = quoteline.CoversNew.Id;
            coverview.CoverTypeDesc = quoteline.CoversNew.CoverType1.Description;
            coverview.RequiredParent = quoteline.CoversNew.RequiredParent;
            coverview.RequiredChild = quoteline.CoversNew.RequiredChild;
            coverview.CoverTypeId = quoteline.CoversNew.CoverTypeId;
            coverview.OtherCoverAdded = quoteline.CoversNew.OtherCoverAdded;
            coverview.CoverRequired = quoteline.Required;
            coverview.Turnover = quoteline.Turnover;
            coverview.Rate = quoteline.Rate ?? 0;

            return coverview;

        }

        public static List<RateItemView> MapItemRateViews(List<ItemTypes> itemTypes)
        {
            var itemRateViews = new List<RateItemView>();
            var itemRates = new List<ItemRates>();

            foreach (var itemtype in itemTypes)
            {
                var itemRatesForType = itemtype.ItemRates;
                itemRates.AddRange(itemRatesForType);
            }

            foreach (var itemrate in itemRates)
            {
                var rateItemView = new RateItemView();
                rateItemView.Id = itemrate.Id;
                rateItemView.Description = itemrate.Description;
                rateItemView.ItemTypeId = itemrate.ItemTypeId;
                rateItemView.RateId = itemrate.RateId;
                itemRateViews.Add(rateItemView);
            }

            return itemRateViews;
        }

        public static List<ListItemView> MapListItemViews(List<QuoteListItem> listItems)
        {
            
            var itemviews = new List<ListItemView>();

            foreach (var listitem in listItems)
            {
                var item = new ListItemView();
                item.RateId = listitem.RateId;
                item.ExtraCoverId = listitem.ExtraCoverId ?? 0;
                item.ItemDescription = listitem.ItemDescription;
                item.Length = listitem.Length ?? 0;
                item.Width = listitem.Width ?? 0;
                item.Quantity = listitem.Quantity ?? 0;
                item.FirstRate = listitem.FirstRate ?? 0;
                item.DiscountFirstRate = listitem.DiscountFirstRate ?? 0;
                item.DiscountsubsequentRate = listitem.DiscountSubsequentRate ?? 0;
                item.Indemnity = listitem.Indemnity ?? 0;
                item.InitialCharge = listitem.InitialCharge ?? 0;
                item.SubsequentCharge = listitem.SubsequentCharge ?? 0;
                item.Rate = listitem.Rate ?? 0;
                item.SumInsured = listitem.SumInsured ?? 0;
                item.Premium = listitem.Premium ?? 0;
                item.RateValue = listitem.RateValue;
                item.ExtraCover = listitem.ExtraCover;
                item.RateValueNum = listitem.RateValueNum ?? 0;

                itemviews.Add(item);
            }

            return itemviews;
        }

        public static List<AddressView>MapAddressViews(List<Addresses> addresses, Contacts contact)
        {
            var addressviews = new List<AddressView>();

            foreach (var address in addresses)
            {
                var addyview = new AddressView();
                //contact details first
                addyview.EntityName = contact.EntityName;
                addyview.EntityType = contact.EntityType;
                addyview.FirstName = contact.FirstName;
                addyview.LastName = contact.LastName;
                addyview.Telephone = contact.Telephone;
                addyview.Email = contact.Email;

                addyview.Id = address.AddressPK;
                addyview.Address1 = address.Address1;
                addyview.Address2 = address.Address2;
                addyview.Address3 = address.Address3;
                addyview.Town = address.Town;
                addyview.County = address.County;
                addyview.Postcode = address.Postcode;
                addyview.Country = address.Country;
                addyview.Locale = address.Locale;                

                addressviews.Add(addyview);
            }

            return addressviews;

        }

        //public static List<RateView> MapOptionsForSelect(List<ItemRates> itemRates)
        //{
            
        //    var optionIds = itemRates.Select(x => x.RateId).ToList();
        //    var options = ratesForCover.Where(x => optionIds.Contains(x.Id)).ToList();
        //    var optionViews = ViewMapper.MapRateViews(options);
        //}

        //public static PolicyView MapPolicyView(Products policy)
        //{


        //    return policyView;
        //}

        #endregion

        #region Map from view model to DB
        public static Quotes MapQuote(PolicyView policyData,ref Quotes quote )
        {
            quote.PremiseFK = policyData.PremiseRequired;
            //price related
            quote.Fee = policyData.LeisureInsureFee ?? 0;
            quote.NetPremium = policyData.Net;
            quote.Total = policyData.Total;
            quote.Tax = policyData.Tax;
            quote.TaxRate = policyData.TaxRate;
            quote.Excess = policyData.Excess ?? 0;
            quote.LegalFeesAdded = policyData.LegalFeesAdded;
            quote.NetLegal = policyData.LegalNet;
            quote.TotalLegal = policyData.LegalFees;
            //policy
            quote.BusinessDescription = policyData.BusinessDescription;
            quote.PolicyFK = policyData.Id;
            quote.PolicyType = policyData.PolicyType;           
            //broker?
            quote.BrokerCommission = policyData.BrokerCommission ?? 0;
            quote.BrokerName = policyData.BrokerName;
            quote.IsAgent = policyData.BrokerName != null ? true : false;
            quote.CommissionPercentage = policyData.CommissionPercentage ?? 0;
            quote.TamBrokerCode = policyData.TamBrokerCode;

            return quote;
        }

        public static QuoteLineNew MapQuoteLine(CoverView cover, ref QuoteLineNew quoteline, int quoteid, int version)
        {
            quoteline.CoverId = cover.Id;
            quoteline.SectionDescription = cover.CoverHeader;
            quoteline.SectionTypeDescription = "";
            quoteline.Commision = 0;
            quoteline.QuoteId = quoteid;
            quoteline.Territory = "";
            quoteline.PeriodOfCover = "";
            quoteline.Tax = cover.Tax;
            quoteline.Net = cover.Net;
            quoteline.Total = cover.Total ?? 0;
            quoteline.Fee = cover.LeisureInsureFee ?? 0;
            quoteline.Indemnity = cover.Indemnity;
            quoteline.SumInsured = cover.SumInsured;
            quoteline.Excess = cover.Excess;
            quoteline.ShowInSummary = true;
            quoteline.ErnExempt = null;
            quoteline.ExtraInfo = "";
            quoteline.Version = version;
            quoteline.Required = cover.CoverRequired;
            quoteline.Turnover = cover.Turnover;
            quoteline.Rate = cover.Rate;

            return quoteline;
        }

        public static QuoteListItem MapQuoteListItem(ListItemView item, ref QuoteListItem listitem, int version,int lineid)
        {
            listitem.RateId = item.RateId;
            listitem.QuoteLineId = lineid;
            listitem.ExtraCoverId = item.ExtraCoverId;
            listitem.ItemDescription = item.ItemDescription;
            listitem.Length = item.Length;
            listitem.Width = item.Width;
            listitem.Quantity = item.Quantity;
            listitem.FirstRate = item.FirstRate;
            listitem.DiscountFirstRate = item.DiscountFirstRate;
            listitem.DiscountSubsequentRate = item.DiscountsubsequentRate;
            listitem.Indemnity = item.Indemnity;
            listitem.InitialCharge = item.InitialCharge;
            listitem.SubsequentCharge = item.SubsequentCharge;
            listitem.Rate = item.Rate;
            listitem.SumInsured = item.SumInsured;
            listitem.Premium = item.Premium;
            listitem.RateValue = item.RateValue;
            listitem.ExtraCover = item.ExtraCover;
            listitem.RateValueNum = item.RateValueNum;
            listitem.Version = version;

            return listitem;
        }


        #endregion 
    }
}
