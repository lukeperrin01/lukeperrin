﻿using System.Collections.Generic;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.DB.LinqClasses;

namespace LeisureInsure.DB.Utilities
{
    public static class ChargeElementMapper
    {
        /// <summary>
        /// Map a list of nullable charges to 'price engine friendly' charges
        /// </summary>
        /// <param name="charges"></param>
        /// <returns></returns>
        public static List<ChargeElements> MapChargeElements(List<ChargeElementsNullable> charges)
        {
            var newcharges = new List<ChargeElements>();

            foreach (var item in charges)
            {
                newcharges.Add(new ChargeElements
                {
                    PolicyFK = item.PolicyFK.GetValueOrDefault(),
                    QuoteInputPK = item.QuoteInputPK,
                    LongRate = item.LongRate,
                    RateLabel = item.RateLabel,
                    CoverName = item.CoverName,
                    CoverFK = item.CoverFK.GetValueOrDefault(),
                    CoverBW = item.CoverBW.GetValueOrDefault(),
                    ListRatePK = item.ListRatePK,
                    RatePK = item.RatePK,
                    RateName = item.RateName,
                    Rate = item.Rate.GetValueOrDefault(),
                    Threshold = item.Threshold.GetValueOrDefault(),
                    Excess = item.Excess.GetValueOrDefault(),
                    DivBy = item.DivBy.GetValueOrDefault(),
                    DiscountFirstRate = item.DiscountFirstRate,
                    DiscountSubsequentRate = item.DiscountSubsequentRate,
                    HazardRating = item.HazardRating.GetValueOrDefault(),
                    TableFirstColumn = item.TableFirstColumn.GetValueOrDefault(),
                    TableChildren = item.TableChildren.GetValueOrDefault(),
                    RateTypeFK = item.RateTypeFK.GetValueOrDefault(),
                    PageNo = item.PageNo.GetValueOrDefault(),
                    SumInsured = item.SumInsured.GetValueOrDefault(),
                    NoItems = item.NoItems.GetValueOrDefault(),
                    RateValue = item.RateValue,
                    TriggerCode = item.TriggerCode,
                    Refer = item.Refer,
                    Territory = item.Territory,
                    Indemnity = item.Indemnity,
                    MinimumRate = item.MinimumRate,
                    FirstRate = item.FirstRate
                });

            }

            return newcharges;
        }

        /// <summary>
        /// Map a single rate for pricing engine
        /// </summary>
        /// <param name="charges"></param>
        /// <returns></returns>
        public static ChargeElementsNullable MapRate(CoverRates cr, string territory)
        {
            var newcharge = new ChargeElementsNullable();
            long? coverbitwise = 0;
            string coverName = "All";
            if (cr.covers != null)
            {
                if(cr.covers.CoverName != "")
                    coverName = cr.covers.CoverName;
                coverbitwise = cr.covers.BitWise;
            }
            newcharge.Indemnity = cr.rates.Indemnity;
            newcharge.PolicyFK = cr.policies.PolicyPK;
            //newcharge.QuoteInputPK = cr.quoteinputs.QuoteInputPK;
            newcharge.LongRate = cr.rates.LongRate;
            newcharge.RateLabel = cr.rates.RateLabel;
            newcharge.CoverName = coverName;
            newcharge.CoverFK = cr.rates.CoverCalcFK;
            newcharge.CoverBW = cr.rates.CoverBW;
            newcharge.ListRatePK = cr.rates.ListRatePK;
            newcharge.RatePK = cr.rates.RatePK;
            newcharge.RateName = cr.rates.RateName;
            newcharge.Rate = cr.rates.Rate;
            newcharge.Threshold = cr.rates.Threshold;
            newcharge.Excess = cr.rates.Excess;
            newcharge.DivBy = cr.rates.DivBy;
            newcharge.DiscountFirstRate = cr.rates.DiscountFirstRate;
            newcharge.DiscountSubsequentRate = cr.rates.DiscountSubsequentRate;
            newcharge.HazardRating = cr.rates.HazardRating;
            newcharge.TableFirstColumn = cr.rates.TableFirstColumn;
            newcharge.TableChildren = cr.rates.TableChildren;
            newcharge.RateTypeFK = cr.rates.RateTypeFK;
            newcharge.PageNo = cr.rates.PageNo;
            newcharge.SumInsured = 0;
            newcharge.NoItems = 0;
            newcharge.RateValue = "";
            newcharge.TriggerCode = 0;
            newcharge.Territory = territory;
            newcharge.Indemnity = cr.rates.Indemnity;
            newcharge.MinimumRate = cr.rates.MinimumRate;
            newcharge.FirstRate = cr.rates.FirstRate;
            return newcharge;
        }

        /// <summary>
        /// Map a list of general rates for pricing engine
        /// </summary>
        /// <param name="charges"></param>
        /// <returns></returns>
        public static List<ChargeElementsNullable> MapRates(List<CoverRates> coverRates, string territory)
        {
            var newCharges = new List<ChargeElementsNullable>();

            foreach (var r in coverRates)
            {
                var newcharge = new ChargeElementsNullable();
                //newcharge.QuoteInputPK = r.quoteinputs.QuoteInputPK;
                newcharge.Indemnity = r.rates.Indemnity;
                newcharge.PolicyFK = r.policies.PolicyPK;
                newcharge.LongRate = r.rates.LongRate;
                newcharge.RateLabel = r.rates.RateLabel;
                newcharge.CoverName = "All";
                newcharge.CoverFK = r.rates.CoverCalcFK;
                newcharge.CoverBW = r.rates.CoverBW;
                newcharge.ListRatePK = r.rates.ListRatePK;
                newcharge.RatePK = r.rates.RatePK;
                newcharge.RateName = r.rates.RateName;
                newcharge.Rate = r.rates.Rate;
                newcharge.Threshold = r.rates.Threshold;
                newcharge.Excess = r.rates.Excess;
                newcharge.DivBy = r.rates.DivBy;
                newcharge.DiscountFirstRate = r.rates.DiscountFirstRate;
                newcharge.DiscountSubsequentRate = r.rates.DiscountSubsequentRate;
                newcharge.HazardRating = r.rates.HazardRating;
                newcharge.TableFirstColumn = r.rates.TableFirstColumn;
                newcharge.TableChildren = r.rates.TableChildren;
                newcharge.RateTypeFK = r.rates.RateTypeFK;
                newcharge.PageNo = r.rates.PageNo;
                newcharge.SumInsured = 0;
                newcharge.NoItems = 0;
                newcharge.RateValue = "";
                newcharge.TriggerCode = 0;
                newcharge.Territory = territory;
                newcharge.MinimumRate = r.rates.MinimumRate;
                newcharge.FirstRate = r.rates.FirstRate;
                newCharges.Add(newcharge);
            }            

            return newCharges;
        }

        /// <summary>
        /// Map a list of chargeable rates (selects etc) for pricing engine
        /// </summary>
        /// <param name="charges"></param>
        /// <returns></returns>
        public static List<ChargeElementsNullable> MapChargeableRates(List<CoverRates> coverRates, string territory)
        {
            var newCharges = new List<ChargeElementsNullable>();                     

            foreach (var coverRate in coverRates)
            {
                var newcharge = new ChargeElementsNullable();
                string inputstring = coverRate.rates.RateName;

                string covername = coverRate?.covers?.CoverName ?? "";
                long coverbitwise = coverRate?.covers?.BitWise ?? 0;                
                inputstring = coverRate?.quoteinputs?.InputString ?? inputstring;

                newcharge.QuoteInputPK = coverRate.quoteinputs.QuoteInputPK;
                newcharge.Indemnity = coverRate.rates.Indemnity;
                newcharge.PolicyFK = coverRate.policies.PolicyPK;
                newcharge.LongRate = coverRate.rates.LongRate;
                newcharge.RateLabel = coverRate.rates.RateLabel;
                newcharge.CoverName = covername;
                newcharge.CoverFK = coverRate.rates.CoverCalcFK;
                newcharge.CoverBW = coverbitwise;
                newcharge.ListRatePK = coverRate.rates.ListRatePK;
                newcharge.RatePK = coverRate.rates.RatePK;
                newcharge.RateName = inputstring;
                newcharge.Rate = coverRate.rates.Rate;
                newcharge.Threshold = coverRate.rates.Threshold;
                newcharge.Excess = coverRate.rates.Excess;
                newcharge.DivBy = coverRate.rates.DivBy;
                newcharge.DiscountFirstRate = coverRate.rates.DiscountFirstRate;
                newcharge.DiscountSubsequentRate = coverRate.rates.DiscountSubsequentRate;
                newcharge.HazardRating = coverRate.rates.HazardRating;
                newcharge.TableFirstColumn = coverRate.rates.TableFirstColumn;
                newcharge.TableChildren = coverRate.rates.TableChildren;
                newcharge.RateTypeFK = coverRate.rates.RateTypeFK;
                newcharge.PageNo = coverRate.rates.PageNo;
                newcharge.SumInsured = coverRate.quoteinputs.SumInsured;
                newcharge.NoItems = coverRate.quoteinputs.NoItems;
                newcharge.RateValue = coverRate.quoteinputs.RateValue;
                newcharge.TriggerCode = coverRate.ratetypes.TriggerCode;
                newcharge.Refer = coverRate.rates.Refer;
                newcharge.Territory = territory;
                newcharge.MinimumRate = coverRate.rates.MinimumRate;
                newcharge.FirstRate = coverRate.rates.FirstRate;
                newCharges.Add(newcharge);
            }

            return newCharges;
        }

        /// <summary>
        /// Map list of covers to be sent to pricing engine
        /// </summary>
        /// <param name="charges"></param>
        /// <returns></returns>
        public static List<ChargeElementsNullable> MapCovers(List<CoverRates> coverRates, string territory)
        {
            var newcharges = new List<ChargeElementsNullable>();

            foreach (var rc in coverRates)
            {
                var newcharge = new ChargeElementsNullable();

                
                
                newcharge.PolicyFK = rc.policies.PolicyPK;                
                newcharge.QuoteInputPK = rc.quoteinputs?.QuoteInputPK ?? 0;
                newcharge.Indemnity = rc.rates.Indemnity;
                newcharge.LongRate = rc.rates.LongRate;
                newcharge.RateLabel = rc.rates.RateLabel;
                newcharge.CoverName = rc.covers.CoverName;
                newcharge.CoverFK = rc.rates.CoverCalcFK;
                newcharge.CoverBW = rc.rates.CoverBW;
                newcharge.ListRatePK = rc.rates.ListRatePK;
                newcharge.RatePK = rc.rates.RatePK;
                newcharge.RateName = rc.rates.RateName;
                newcharge.Rate = rc.rates.Rate;
                newcharge.Threshold = rc.rates.Threshold;
                newcharge.Excess = rc.rates.Excess;
                newcharge.DivBy = rc.rates.DivBy;
                newcharge.DiscountFirstRate = rc.rates.DiscountFirstRate;
                newcharge.DiscountSubsequentRate = rc.rates.DiscountSubsequentRate;
                newcharge.HazardRating = rc.rates.HazardRating;
                newcharge.TableFirstColumn = rc.rates.TableFirstColumn;
                newcharge.TableChildren = rc.rates.TableChildren;
                newcharge.RateTypeFK = rc.rates.RateTypeFK;
                newcharge.PageNo = rc.rates.PageNo;
                newcharge.SumInsured = 0;
                newcharge.NoItems = 0;
                newcharge.RateValue = "";
                newcharge.TriggerCode = 0;
                newcharge.Territory = territory;
                newcharge.MinimumRate = rc.rates.MinimumRate;
                newcharge.FirstRate = rc.rates.FirstRate;

                newcharges.Add(newcharge);
                
            }

            return newcharges;
        }
    }
}
