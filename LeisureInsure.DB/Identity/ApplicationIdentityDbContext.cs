﻿using LeisureInsure.DB.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LeisureInsure.DB.Identity
{
    public class ApplicationIdentityDbContext : IdentityDbContext<LiApplicationUser>
    {
        public ApplicationIdentityDbContext()
            : base("DefaultConnection")
        {
        }

        public static ApplicationIdentityDbContext Create()
        {
            return new ApplicationIdentityDbContext();
        }
    }
}