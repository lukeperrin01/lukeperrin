//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeisureInsure.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Certs
    {
        public int CertificatePK { get; set; }
        public string Url { get; set; }
        public byte[] Certificate { get; set; }
        public string QuoteRef { get; set; }
        public string FileName { get; set; }
        public Nullable<int> Lock { get; set; }
        public Nullable<System.DateTime> DateStamp { get; set; }
    }
}
