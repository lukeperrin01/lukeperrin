//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeisureInsure.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class CoversNew
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CoversNew()
        {
            this.ItemTypes = new HashSet<ItemTypes>();
            this.QuoteLineNew = new HashSet<QuoteLineNew>();
            this.SectionHeadersNew = new HashSet<SectionHeadersNew>();
            this.RatesNew = new HashSet<RatesNew>();
            this.RatesNew1 = new HashSet<RatesNew>();
        }
    
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int LocaleBW { get; set; }
        public int Section { get; set; }
        public string CoverName { get; set; }
        public string AngularForm { get; set; }
        public string SectionName { get; set; }
        public string Specification { get; set; }
        public string CoverTip { get; set; }
        public Nullable<decimal> ReferValue { get; set; }
        public Nullable<decimal> CoverOrdinal { get; set; }
        public string CoverHeader { get; set; }
        public string CoverType { get; set; }
        public Nullable<int> CoverTypeId { get; set; }
        public Nullable<int> RequiredParent { get; set; }
        public Nullable<int> RequiredChild { get; set; }
        public string ClauseCode { get; set; }
        public Nullable<int> OtherCoverAdded { get; set; }
    
        public virtual CoverType CoverType1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ItemTypes> ItemTypes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QuoteLineNew> QuoteLineNew { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SectionHeadersNew> SectionHeadersNew { get; set; }
        public virtual Products Products { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RatesNew> RatesNew { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RatesNew> RatesNew1 { get; set; }
    }
}
