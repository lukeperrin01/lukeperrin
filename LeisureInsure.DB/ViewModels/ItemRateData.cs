﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.ViewModels
{
    public class ItemRateData
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Nullable<int> RateId { get; set; }
        public Nullable<int> ItemTypeId { get; set; }
    }
}
