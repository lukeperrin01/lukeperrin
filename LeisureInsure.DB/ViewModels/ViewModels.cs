﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.ComponentModel.DataAnnotations.Schema;

namespace LeisureInsure.DB.ViewModels
{
    public class vmOldRepositoryRate
    {


        public int Id { get; set; }

        public decimal Value { get; set; }

        public decimal? FirstRate { get; set; }

        public Nullable<int> Indemnity { get; set; }

        public decimal? MinimumRate { get; set; }

        public decimal? SumInsured { get; set; }
        public decimal? Threshold { get; set; }

        public int? Excess { get; set; }

        public int MustRefer { get; set; }

        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountSubsequentRate { get; set; }

        public int LocaleBw { get; set; }
        public int? DivBy { get; set; }
        public int PolicyBw { get; set; }

        public int? HazardRating { get; set; }

        public long CoverBw { get; set; }
        //public RateTypes RateType { get; set; }

        public int RateTypeFK { get; set; }

        public int RateExclude { get; set; }

        public string TamOccupation { get; set; }

        public DateTime? ValidUntil { get; set; }

        public int? TableParentRatePK { get; set; }
        public int? ListRatePK { get; set; }
    }
    //public class TamTotals
    //{
    //    public string FieldPrefix { get; set; }
    //    public string insured { get; set; }
    //    public string indemnity { get; set; }
    //    public string excess { get; set; }
    //    public string net { get; set; }
    //    public string fee { get; set; }
    //    public string gross { get; set; }
    //    public string tax { get; set; }

    //}
    public class TamCustomDecPageOne
    {
        public string TradingAs { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        public string EventName { get; set; }
        public string Territory { get; set; }
        public string BIHA { get; set; }
        public string ErnNumber { get; set; }
        public string Showman { get; set; }

        public string NoVisitors { get; set; }
        public string NoEvents { get; set; }

    }
    public class TamCustomDecSchedule
    {
        public int Key { get; set; }
        public int Section { get; set; }
        public string Description { get; set; }
        public string Insured { get; set; }
        public string Rate { get; set; }
        public string Premium { get; set; }
        public string Excess { get; set; }
        public string Spec { get; set; }

    }

    public class vmBrokerAddress
    {
        public int ContactPK { get; set; }
        public string EntityName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string Country { get; set; }

        public string Postcode { get; set; }

        public string Telephone { get; set; }

        public DateTime DateEffective { get; set; }

    }
    public class vmDateCheck
    {
        public int PolicyPK { get; set; }
        public string PolicyType { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

        public bool dateToRequired { get; set; }
        public bool endTimeRequired { get; set; }
        public bool startTimeRequired { get; set; }
        public int numberOfDays { get; set; }

    }

    public class vmDocument
    {
        public string DocumentType { get; set; }
        public string DocumentLink { get; set; }

        public string LinkName { get; set; }

        public int DocumentFK { get; set; }
    }

    public class vmDocumentsForQuote
    {
        public int Ord { get; set; }
        public int? DocTypeFK { get; set; }
        public string DocType { get; set; }
        public string DocName { get; set; }
        public string DocLink { get; set; }
        public int DocumentFK { get; set; }
    }

    public class vmEndorsement
    {
        public string html { get; set; }

    }
    public class vmEndorsements
    {
        public IEnumerable<vmEndorsement> Endorsements { get; set; }

    }
    public class vmQuote
    {
        public int QuotePK { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Progress { get; set; }
        public int? BrokerFK { get; set; }
        public int LocaleID { get; set; }
        public string strLocale { get; set; }
    }

    public class vmXmlPayment
    {
        public string renewalpolicynumber { get; set; }
        public bool instalments { get; set; }
        public string culture { get; set; }
        public string quoteRef { get; set; }
        public string securityNumber { get; set; }
        public string expiryMonth { get; set; }
        public string expiryYear { get; set; }
        public string issueNumber { get; set; }
        public string cardNumber { get; set; }
        public string startMonth { get; set; }
        public string startYear { get; set; }
        public string amount { get; set; }
        public string paymentMode { get; set; }
    }

    public class vmNavigation
    {
        public string NavMessage { get; set; }
        public int? quotePK { get; set; }
        public int? progress { get; set; }
        public int? CurrentPage { get; set; }
        public int? PolicyPK { get; set; }
        public int? LocaleId { get; set; }
        public DateTime? dateFrom { get; set; }
        public DateTime? dateTo { get; set; }
        public vmLocales locales { get; set; }
        public vmPolicies policies { get; set; }
        public vmCovers covers { get; set; }
        public IEnumerable<vmInputHeader> inputs { get; set; }
        public IEnumerable<vmInputHeader> SaveInputs { get; set; }

        public IEnumerable<vmInputsFlat> inputsFlat { get; set; }
    }

    public class vmLocale
    {
        public int localePK { get; set; }
        public string strLocale { get; set; }
        public string flagUrl { get; set; }
        public string currencySymbol { get; set; }
        public string country { get; set; }
        public string territory { get; set; }
        public int localeBW { get; set; }
        public decimal exchangeRate { get; set; }
        public decimal multiplier { get; set; }
        public int localePKselected { get; set; }
        public decimal? localeRefer { get; set; }
        public Boolean showFlag { get; set; }

    }

    public class vmLocales
    {
        public vmLocale localeSelected { get; set; }
        public IEnumerable<vmLocale> localeList { get; set; }
    }

    public class vmPolicy
    {
        public int PolicyPK { get; set; }
        public int? PolicyBW { get; set; }
        public string PolicyName { get; set; }
        public int? PeriodYear { get; set; }
        public int? PeriodDay { get; set; }
        public int? PremiseRequired { get; set; }
        public int? TimeRequired { get; set; }
        public long? CoverBW { get; set; }
        public int? policyPKSelected { get; set; }

        public string MinDate { get; set; }

        public string MaxDate { get; set; }
    }
    public class vmPolicies
    {
        public vmPolicy policySelected { get; set; }
        public IEnumerable<vmPolicy> policyList { get; set; }
    }

    public class vmCovers
    {
        public IEnumerable<vmCover> coverList { get; set; }
        public IEnumerable<vmCover> coverSelected { get; set; }
    }

    public class vmCover
    {
        public int CoverPK { get; set; }
        public string SectionName { get; set; }
        public int SectionNumber { get; set; }
        public string CoverName { get; set; }
        public string CoverTip { get; set; }
        public int coverPKSelected { get; set; }

        public int? Pages { get; set; }
        public int Optional { get; set; }
        public long? BitWise { get; set; }
        public int CoverDetailsCompleted { get; set; }

        public decimal? ReferValue { get; set; }
    }

    public class vmSaveInputs
    {
        public int CoverPK { get; set; }
        public int? ListNo { get; set; }
        public int? ListOrdinal { get; set; }
        public string InputName { get; set; }
        public int? ParentPK { get; set; }
        public int? InputTypeFK { get; set; }
        public int InputPK { get; set; }
        public int RatePK { get; set; }
        public string RateValue { get; set; }
        public string RateString { get; set; }
        public string InputString { get; set; }
        public int InputPKLock { get; set; }
        public string UnitTypex { get; set; }
    }
    public class vmInputHeader
    {
        public int CoverPK { get; set; }
        public int InputPK { get; set; }
        public string InputName { get; set; }
        public decimal? InputOrdinal { get; set; }
        public int? InputType { get; set; }
        public int? InputSelectedPK { get; set; }
        public string InputString { get; set; }
        public int? InputTriggerFK { get; set; }
        public int? PageNo { get; set; }
        public int? ParentPK { get; set; }
        public int? InputVis { get; set; }
        public string ngRequired { get; set; }
        public string ngClass { get; set; }
        public string ngPattern { get; set; }
        public int? ListNo { get; set; }
        public int? ListOrdinal { get; set; }
        public IEnumerable<vmInputDetails> InputDetails { get; set; }
        public vmInputDetails InputSelected { get; set; }
        public int? InputStatus { get; set; }
        public string UnitType { get; set; }
    }

    public class vmInputsFlat
    {
        public int? CoverPK { get; set; }
        public int? ParentBW { get; set; }
        public int? ChildBW { get; set; }

        public int RatePK { get; set; }
        public string RateName { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Threshold { get; set; }
        public int? Excess { get; set; }
        public string RateLabel { get; set; }
        public string RateTip { get; set; }
        public int Refer { get; set; }
        public int? DivBy { get; set; }
        public decimal? RateOrdinal { get; set; }
        public int? ParentRatePK { get; set; }
        public int? TableChildren { get; set; }
        public int? TableFirstColumn { get; set; }
        public decimal? InputOrdinal { get; set; }
        public int? InputTypeFK { get; set; }
        public string UnitType { get; set; }
        public string RateValue { get; set; }
        public int? RateVis { get; set; }
        public int? InputSelectedPK { get; set; }

        public vmInputsFlat InputSelected { get; set; }
        public IEnumerable<vmInputsFlat> InputDetails { get; set; }
        public int? ListNo { get; set; }
        public int? ListOrdinal { get; set; }

        public int? ListRatePK { get; set; }
        public string ngClass { get; set; }
        public string ngPattern { get; set; }

        public decimal? SumInsured { get; set; }
        public int? NoItems { get; set; }
        public int? DisplayBW { get; set; }
        public int? ReturnBW { get; set; }
        public string lblSumInsured { get; set; }
        public string lblNumber { get; set; }

        public int? PageNo { get; set; }

        public string SubRateHeading { get; set; }
        public int? TrigSubHeadings { get; set; }


        public int HazardRating { get; set; }
        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountSubsequentRate { get; set; }
        public int RateIsPercentage { get; set; }
        public int ThresholdIsPercentage { get; set; }




        public int? RateTypeFK { get; set; }
        public int RateGroup { get; set; }
        public int CoverCalcFK { get; set; }
        public int InputTrigger { get; set; }
        public int ngDisabled { get; set; }
        public int TradeFilter { get; set; }
        public bool ngRequired { get; set; }
        public int SectionNo { get; set; }

        public bool? OneLine { get; set; }

        public int? TableParentRatePK { get; set; }

        public Nullable<int> Indemnity { get; set; }



    }
    public class vmInputDetails
    {
        public int? CoverPK { get; set; }
        public int? ChildBW { get; set; }
        public int InputPK { get; set; }
        public int RatePK { get; set; }
        public string RateName { get; set; }
        public string RateValue { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Threshold { get; set; }
        public int? Excess { get; set; }
        public string RateLabel { get; set; }
        public string RateTip { get; set; }
        public bool Refer { get; set; }
        public int? DivBy { get; set; }
        public decimal? RateOrdinal { get; set; }
        public int? ParentPK { get; set; }
        public int? RateBW { get; set; }
        public int? RateVis { get; set; }
        public int? InputSelectedPK { get; set; }

        public int? ListNo { get; set; }

        public int? ListOrdinal { get; set; }

        public string UnitTypex { get; set; }
        public int? TableChildren { get; set; }
        public int? TableFirstColumn { get; set; }
    }

    public class vmLogin
    {
        public int ContactPK { get; set; }
        public int ContactType { get; set; }
        public string Email { get; set; }
    }

    public class vmInputs
    {
        public int CoverPK { get; set; }
        public int RatePK { get; set; }
        public int ListNo { get; set; }
        public int ListOrdinal { get; set; }
        public int InputPK { get; set; }

        public string RateValue { get; set; }
        public string InputString { get; set; }
    }
    public class vmCharges
    {
        //public int QuotePK { get; set; }
        public int CoverPK { get; set; }
        public int InputPK { get; set; }
        public int RatePK { get; set; }
        public int InvLevel { get; set; }
        public string LineType { get; set; }
        public decimal Base { get; set; }
        public decimal Rate { get; set; }
        public decimal Charge { get; set; }
    }

    public class vmChargeSummary
    {
        public int CoverPK { get; set; }
        public int InvLevel { get; set; }
        public string LineType { get; set; }
        public decimal Charge { get; set; }
        public decimal Rate { get; set; }
        public int Section { get; set; }
    }

    #region payment
    public class vmPayment
    {
        public string QuoteRef { get; set; }
        public decimal Premium { get; set; }
        public decimal CardCharge { get; set; }
        public decimal TotalPayable { get; set; }
        public IEnumerable<vmCardType> _cardType { get; set; }
        public int cardTypeSelected { get; set; }
        public string CardNumber { get; set; }
        public int IssueNumber { get; set; }
        public string ValidMM { get; set; }
        public string ValidYY { get; set; }
        public string ExpiryMM { get; set; }
        public string ExpiryYY { get; set; }
        public string SecurityNumber { get; set; }
        public bool Instalments { get; set; }
        public string Culture { get; set; }
        public string PaymentMode { get; set; }
    };
    public class vmCardType
    {
        public int CardTypePK { get; set; }
        public string CardType { get; set; }
        public double Charge { get; set; }
        public int Credit { get; set; }

    };

    public class vmPaymentOptions
    {
        public int groupOrd { get; set; }
        public int Ord { get; set; }
        public string LineType { get; set; }
        public decimal? summed { get; set; }
    };
    public class vmGlobalIris
    {
        public string actionLink { get; set; }
        public string merchant { get; set; }
        public string order { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public string stamp { get; set; }
        public string hash { get; set; }
    };
    public class vmInstalments
    {
        public int InstallmentPK { get; set; }
        public Nullable<int> QuoteRef { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string SortCode { get; set; }
        public string BusinessName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<decimal> FullPremium { get; set; }
        public Nullable<decimal> Deposit { get; set; }
        public Nullable<decimal> FlatRate { get; set; }
        public Nullable<int> NumberOfPayments { get; set; }
        public string InstalmentMonth { get; set; }
        public Nullable<System.DateTime> Timestamp { get; set; }
        public Nullable<System.DateTime> DownLoaded { get; set; }
    };

    #endregion

    #region Certificate

    public class CertificatesModel
    {
        [Key]
        public int CertificatePK { get; set; }
        public string Url { get; set; }
        public byte[] Certificate { get; set; }
        public string QuoteRef { get; set; }
        public int Lock { get; set; }
        public string FileName { get; set; }
    };

    public class OldCertificatesModel
    {
        [Key]
        public int QuoteRef { get; set; }
        public string ID { get; set; }
        public byte[] Data { get; set; }

    };

    public class vmCert
    {
        public string CertificatePK { get; set; }
        public vmCertSchedule Schedule { get; set; }

        public IEnumerable<vmSectionHeaders> Sections { get; set; }
        //public IEnumerable<vmCertSectionPKs> SectionPKs { get; set; }
        //public vmCertHeader Header { get; set; }

    };
    public class vmCertHazards
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    };
    public class vmCertHazardList
    {
        public IEnumerable<vmCertHazards> Hazards { get; set; }

    }
    public class vmCertSectionPKs
    {
        public int? QuotePK { get; set; }
        public int? SectionPK { get; set; }
        public string InsuredFlag { get; set; }
        public string CoverName { get; set; }

    };


    public class vmCertSchedule
    {
        public string QuoteReference { get; set; }
        public string PolicyWording { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string InsuredName { get; set; }
        public string CorrespondenceAddress { get; set; }
        public string Premises { get; set; }
        public string Business { get; set; }
        public string TerritorialLimits { get; set; }
        public string NetPremium { get; set; }
        public string TaxType { get; set; }
        public string Tax { get; set; }
        public string Fee { get; set; }
        public string GrossPremium { get; set; }
        public string GrossLegal { get; set; }
        public int? Purchased { get; set; }

        public string PolicyType { get; set; }
        public int? Contingency { get; set; }
        public DateTime? PurchasedDate { get; set; }
        public string EventName { get; set; }
        public string Attendance { get; set; }
        public int PolicyPK { get; set; }
        public string Venue { get; set; }
        public int LocalePK { get; set; }
        public IEnumerable<additionalCovered> additionalInsured { get; set; }

    };

    public class additionalCovered
    {
        public int ContactPK { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }

    public class vmCertSections
    {
        public int QuotePK { get; set; }
        public int CoverPK { get; set; }
        public int SectionPK { get; set; }
        public long? ItemNo { get; set; }
        public string Description { get; set; }
        public string Specification { get; set; }
        public string SumInsured { get; set; }
        public string Excess { get; set; }
        public string LimitofLiability { get; set; }

    };

    public class vmSections
    {
        public int HeaderLevel { get; set; }
        public int Section { get; set; }
        public int Insured { get; set; }

        public int ParagraphType { get; set; }

        public string Label1 { get; set; }
        public string Label2 { get; set; }

        public string LineValue1 { get; set; }
        public string LineValue2 { get; set; }
        public int? LineGroup { get; set; }
        public decimal? ordinal { get; set; }
        public int Para { get; set; }
    }
    public class vmSectionHeaders
    {
        public int HeaderLevel { get; set; }
        public int Section { get; set; }
        public int Insured { get; set; }
        public string Header { get; set; }
        public IEnumerable<vmSections> SectionDetail { get; set; }

        public IEnumerable<vmCertLists> SectionLists { get; set; }

    }

    public class vmCertLists
    {
        public int QuoteInputPK { get; set; }
        public long? RowNo { get; set; }
        public int CoverFK { get; set; }
        public int Section { get; set; }
        public string LineType { get; set; }
        public string Specification { get; set; }
        public string Territory { get; set; }
        public string SumInsured { get; set; }
        public string WageRoll { get; set; }        
        public string IndemnityPeriod { get; set; }
    };

    #endregion

    public class vmContactDetail
    {
        public List<vmAddress> Address { get; set; }
        public List<vmContact> Contact { get; set; }

        public string QuoteReference { get; set; }

        public string PassCode { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }



        //public string QuotePK { get; set; }
    }
    public class vmContact
    {
        public int ContactType { get; set; }
        public int ContactPK { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string PWord { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string EntityType { get; set; }
        public string EntityName { get; set; }
        public string TamClientCode { get; set; }
        public int Lock { get; set; }
        public string ForAttentionOf { get; set; }
    }

    public class vmContactx
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class vmAddress
    {
        public int ContactFK { get; set; }
        public int AddressPK { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string Locale { get; set; }
        public DateTime Timestamp { get; set; }
        public int Lock { get; set; }
        public string QuoteFK { get; set; }
        public int AddressType { get; set; }
    }

    #region renewals
    public class vmPolList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BitwiseId { get; set; }
    }

    #endregion

    #region TAM

    public class vmTamPolicy
    {
        //public IEnumerable<vmTamPolicy> tamPol { get; set; }
        public int QuotePK { get; set; }
        public int TamAgency { get; set; }
        public string ICO { get; set; }
        public string TamType { get; set; }
        public string PolicyNumber { get; set; }
        public decimal? Premium { get; set; }
        public DateTime? EffDate { get; set; }
        public DateTime? ExpDate { get; set; }
        public int CoverGroup { get; set; }
    }

    public class vmTamTransaction
    {
        //public IEnumerable<vmTamTransaction> tamTrans { get; set; }
        public string TamClientCode { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string LineType { get; set; }
        public int? Trans { get; set; }
        public string TransType { get; set; }
        public decimal? Charge { get; set; }
        public string PolicyIndex { get; set; }
        public string TransDesc { get; set; }
        public int Tax { get; set; }
    }



    #endregion

    #region Renewals
    public class vmRenewalListItem
    {
        public string TamPolicyRef { get; set; }
        public string TamQuoteNo { get; set; }
        public string TamBroker { get; set; }

        public string ClientName { get; set; }
        public DateTime ExpiryDate { get; set; }

        public DateTime? EmailedDate { get; set; }

        public string QuoteRef { get; set; }

    }

    public class vmRenewalList
    {
        public IEnumerable<vmRenewalListItem> RenewalList { get; set; }
    }

    #endregion

}