﻿using System;

namespace LeisureInsure.DB.ViewModels
{
    public class RateItemView
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Nullable<int> RateId { get; set; }
        public Nullable<int> ItemTypeId { get; set; }
    }
}
