﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.ViewModels
{
    public class AddressView
    {

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string ContactName { get; set; }
        public string Country { get; set; }
        public string County { get; set; }        
        public string Email { get; set; }
        public string EntityName { get; set; }
        public string EntityType { get; set; }
        public string FirstName { get; set; }
        public int? Id { get; set; }
        public string LastName { get; set; }
        public string Locale { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Town { get; set; }

       
                                                            
    }
}
