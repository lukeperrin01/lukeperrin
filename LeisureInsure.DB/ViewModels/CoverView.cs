﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeisureInsure.DB;

namespace LeisureInsure.DB.ViewModels
{
    public class CoverView
    {
        public int Id { get; set; }
        public int PolicyId { get; set; }
        public int? CoverTypeId { get; set; }
        public int LocaleBW { get; set; }
        public int Section { get; set; }
        public string CoverName { get; set; }
        public string AngularForm { get; set; }
        public string SectionName { get; set; }
        public string CoverHeader { get; set; }
        public string Specification { get; set; }
        public string CoverTip { get; set; }
        public int? Excess { get; set; }
        public Nullable<decimal> ReferValue { get; set; }
        public Nullable<decimal> CoverOrdinal { get; set; }
        public List<RateView> Rates { get; set; }
        public List<RateItemView> RateItems { get; set; }
        public List<ListItemView> ItemsAdded { get; set; }
        public string CoverType { get; set; }
        public bool CoverRequired { get; set; }
        public bool ErnExempt { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Net { get; set; }
        public decimal? Total { get; set; }
        public decimal? LeisureInsureFee { get; set; }       
        public decimal? Indemnity { get; set; }
        public decimal? SumInsured { get; set; }
        public bool ShowInSummary { get; set; }
        public string CoverTypeDesc { get; set; }
        public int? RequiredParent { get; set; }
        public int? RequiredChild { get; set; }
        //has this been added to a section? ie MB/BI
        public bool ShowCover { get; set; }
        //does this cover belong in a section? ie MD/BI
        public bool SectionType { get; set; }
        //cover cant be added if the other cover is added first
        public int? OtherCoverAdded { get; set; }
        public bool sectionHeader { get; set; }
        public decimal? Turnover { get; set; }
        public string Ern { get; set; }
        public int EmployeeTotal { get; set; }
        public string MDAddress { get; set; }
        public string MDPostCode { get; set; }
        public decimal Subsidence { get; set; }
        public decimal Rate { get; set; }
        public string CustomDescription { get; set; }

    }
}
