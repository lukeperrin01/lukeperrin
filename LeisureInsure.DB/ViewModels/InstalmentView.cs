﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.ViewModels
{
    public class InstalmentView
    {
        public decimal TotalToPay { get; set; }
        public decimal Deposit { get; set; }
        public int NumberOfPayments { get; set; }
        public decimal EachPayment { get; set; }
        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string Title { get; set; }
        public string BusinessName { get; set; }
        public string Name { get; set; }
        public bool AccountHolder { get; set; }
        public bool AccountConfirm { get; set; }              
    }
}
