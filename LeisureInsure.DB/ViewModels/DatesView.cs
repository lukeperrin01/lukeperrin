﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.ViewModels
{
    public class DatesView
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string MaxDate { get; set; }
        public string MinDate { get; set; }
        public string StartingHour { get; set; }
        public string StartingMinute { get; set; }
        public string EndingHour { get; set; }
        public string EndingMinute { get; set; }
        public bool DateToRequired { get; set; }
        public bool StartTimeRequired { get; set; }
        public bool EndTimeRequired { get; set; }
        public int MinToMaxDays { get; set; }
        public int CoverDays { get; set; }
        public string MaxExpDate { get; set; }
        public string MinExpDate { get; set; }       
    }
}
