﻿using System;
using System.Collections.Generic;

namespace LeisureInsure.DB.ViewModels
{
    public class RateView
    {
        public int Id { get; set; }
        public int CoverId { get; set; }
        public int? InputTypeId { get; set; }
        public int? RateTypeId { get; set; }
        public decimal? Ordinal { get; set; }
        public int? Display { get; set; }
        public int LocaleBW { get; set; }
        public int? VisibleChild { get; set; }
        public int? Indemnity { get; set; }

        public int? Turnover { get; set; }
        public string RateName { get; set; }
        public decimal? Rate { get; set; }
        public int? DivBy { get; set; }
        public decimal? Threshold { get; set; }
        public int? Excess { get; set; }
        public string RateLabel { get; set; }
        public string RateTip { get; set; }
        public int Refer { get; set; }
        public string CatRating { get; set; }
        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountSubsequentRate { get; set; }
        public int? TableParentRatePK { get; set; }
        public int? TableFirstColumn { get; set; }
        public int? TableChildren { get; set; }
        public string UnitType { get; set; }
        public int? NoItems { get; set; }
        public string lblSumInsured { get; set; }
        public string lblNumber { get; set; }
        public int? RateExclude { get; set; }
        public int? VisibleParent { get; set; }
        public decimal? FirstRate { get; set; }
        public int? PageNo { get; set; }
        public int? InputTrigger { get; set; }
        public DateTime? ValidUntil { get; set; }
        public string ClauseCode { get; set; }
        public int? ngDisabled { get; set; }
        public int? TradeFilter { get; set; }
        public string LongRate { get; set; }
        public decimal? SumInsured { get; set; }
        public decimal? MinimumRate { get; set; }
        public int? ngRequired { get; set; }
        public int? ItemTypeId { get; set; }
        public string inputName { get; set; }
        public int? SelectChild { get; set; }
        public int? SelectParent { get; set; }
        public List<RateView> Options { get; set; }
        public int? ItemListId { get; set; }
        public string RateValue { get; set; }
        public decimal RateValueNum { get; set; }
        public int? ExtraCoverId { get; set; }
        public string ExtraCover { get; set; }
        public RateView InputSelected { get; set; }
        public bool? Heightwidth { get; set; }
        public bool? ExtraInfo { get; set; }
        public decimal? NilExcessRate { get; set; }
        public bool InvertReferral { get; set; }
    }
}
