﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeisureInsure.DB.Business;

namespace LeisureInsure.DB.ViewModels
{
    public class PolicyView
    {
        public int Id { get; set; }
        public string PolicyName { get; set; }        
        public string TamType { get; set; }
        public int Ordinal { get; set; }
        public decimal? LeisureInsureFee { get; set; }
        //brokers
        public decimal? BrokerCommission { get; set; }
        public decimal? CommissionPercentage { get; set; }
        public string TamBrokerCode { get; set; }
        public string BrokerName { get; set; }
        public int? PeriodYear { get; set; }
        public int? PeriodDay { get; set; }
        public string PolicyType { get; set; }
        public int? PolicyGroupFK { get; set; }
        public int? PremiseRequired { get; set; }
        public int? TimeRequired { get; set; }
        public string TamOccupation { get; set; }
        public string ClauseCode { get; set; }
        public decimal? Excess { get; set; }
        public string BusinessDescription { get; set; }
        public string CertificateBody { get; set; }
        public List<CoverView> Covers { get; set; }
        public List<AddressView> Addresses { get; set; }
        public List<PolicyDocuments> Documents { get; set; }
        public vmLocale Locale { get; set; }
        public string Browser { get; set; }
        public string QuoteReference { get; set; }
        public string Password { get; set; }
        public int LocaleId { get; set; }
        //used for creating a quote
        public decimal Net { get; set; }
        public decimal Tax { get; set; }
        public decimal LegalFees { get; set; }
        public decimal LegalNet { get; set; }
        public decimal TaxRate { get; set; }
        public decimal LegalRate { get; set; }
        public decimal Total { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public long MinStartDate { get; set; }
        public long MaxStartDate { get; set; }
        public int NumberOfDays { get; set; }
        public DatesView Dates { get; set; }
        public bool LegalFeesAdded { get; set; }
        public string StripePublishableKey { get; set; }
        public InstalmentView InstalmentData { get; set; }
        public string CSR { get; set; }
        public bool MdSectionAdded { get; set; }
        public bool BiSectionAdded { get; set; }
        public LandingPages LandingPage { get; set; }



    }
}
