﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.ViewModels
{
    public class vmOldRepositoryRate
    {

        public int Id { get; set; }

        public decimal Value { get; set; }

        public decimal? FirstRate { get; set; }

        public Nullable<int> Indemnity { get; set; }

        public decimal? MinimumRate { get; set; }

        public decimal? SumInsured { get; set; }
        public decimal? Threshold { get; set; }

        public int? Excess { get; set; }

        public int MustRefer { get; set; }

        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountSubsequentRate { get; set; }

        public int LocaleBw { get; set; }
        public int? DivBy { get; set; }
        public int PolicyBw { get; set; }

        public int? HazardRating { get; set; }

        public long CoverBw { get; set; }
        //public RateTypes RateType { get; set; }

        public int RateTypeFK { get; set; }

        public int RateExclude { get; set; }

        public string TamOccupation { get; set; }

        public DateTime? ValidUntil { get; set; }

        public int? TableParentRatePK { get; set; }
        public int? ListRatePK { get; set; }
    }

}
