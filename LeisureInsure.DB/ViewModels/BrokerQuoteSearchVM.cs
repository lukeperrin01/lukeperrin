﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeisureInsure.ViewModels
{
    public class BrokerQuoteSearchVM
    {
        public string QuoteRef { get; set; }
        public string BrokerContact { get; set; }
        public int QuotePK { get; set; }

        public string PostCode { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DatePurchased { get; set; }
        public int? CustomerFK { get; set; }
        public string CustomerName { get; set; }
        public string Policy { get; set; }
    }
}