﻿namespace LeisureInsure.DB.ViewModels
{
    public class CertHeader
    {
        public int HeaderLevel { get; set; }
        public int? Section { get; set; }
        public int Insured { get; set; }
        public decimal? Ordinal { get; set; }
        public string Header { get; set; }
        public int CoverId { get; set; }
        public int CoverTypeId { get; set; }
    }
}
