﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.ViewModels
{
    public class ListItemView
    {
        public int RateId { get; set; }
        public string ExtraInfo { get; set; }
        public string ItemDescription { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public int Quantity { get; set; }
        public decimal FirstRate { get; set; }
        public decimal DiscountFirstRate { get; set; }
        public decimal DiscountsubsequentRate { get; set; }
        public decimal Indemnity { get; set; }
        public int ExtraCoverId { get; set; }
        public decimal InitialCharge { get; set; }
        public decimal SubsequentCharge { get; set; }
        public bool FirstCharge { get; set; }
        public decimal SumInsured { get; set; }
        public decimal Rate { get; set; }
        public decimal Premium { get; set; }
        public string RateValue { get; set; }
        public string ExtraCover { get; set; }
        public decimal RateValueNum { get; set; }
        //used on quote engine
        public decimal Turnover { get; set; }
        public int Excess { get; set; }
        public bool Selected { get; set; }
        public string Activity { get; set; }
        public decimal WageRoll { get; set; }
        public string IndemnityPeriod { get; set; }

    }
}
