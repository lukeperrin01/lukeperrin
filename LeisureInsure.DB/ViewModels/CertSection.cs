﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.ViewModels
{
    public class CertSection
    {
        public int HeaderLevel { get; set; }
        public int? Section { get; set; }
        public int? ParagraphType { get; set; }
        public int Insured { get; set; }
        public string Header { get; set; }
        public Nullable<int> LocalePK { get; set; }
        public string Label1 { get; set; }
        public string Label2 { get; set; }
        public string LineValue1 { get; set; }
        public string LineValue2 { get; set; }
        public Nullable<int> LineGroup { get; set; }
        public Nullable<decimal> ordinal { get; set; }
        public int? Para { get; set; }
    }
}
