//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeisureInsure.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class TamErrorLogs
    {
        public int Id { get; set; }
        public string QuoteReference { get; set; }
        public string URL { get; set; }
        public string Msg { get; set; }
        public string StackTrace { get; set; }
        public string Cause { get; set; }
        public Nullable<System.DateTime> DateStamp { get; set; }
    }
}
