//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeisureInsure.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuoteListItem
    {
        public int Id { get; set; }
        public int RateId { get; set; }
        public int QuoteLineId { get; set; }
        public Nullable<int> ExtraCoverId { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<decimal> Length { get; set; }
        public Nullable<decimal> Width { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> FirstRate { get; set; }
        public Nullable<decimal> DiscountFirstRate { get; set; }
        public Nullable<decimal> DiscountSubsequentRate { get; set; }
        public Nullable<decimal> Indemnity { get; set; }
        public Nullable<decimal> InitialCharge { get; set; }
        public Nullable<decimal> SubsequentCharge { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<decimal> SumInsured { get; set; }
        public string RateValue { get; set; }
        public string ExtraCover { get; set; }
        public Nullable<decimal> RateValueNum { get; set; }
        public Nullable<int> Version { get; set; }
        public Nullable<decimal> Turnover { get; set; }
        public Nullable<int> Excess { get; set; }
        public string Activity { get; set; }
        public Nullable<decimal> WageRoll { get; set; }
        public string IndemnityPeriod { get; set; }
        public Nullable<decimal> Premium { get; set; }
    
        public virtual QuoteLineNew QuoteLineNew { get; set; }
        public virtual RatesNew RatesNew { get; set; }
    }
}
