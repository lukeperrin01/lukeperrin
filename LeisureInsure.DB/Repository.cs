﻿using System;
using System.Collections.Generic;
using System.Linq;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.DB.Models;
using System.Data;
using System.Data.SqlClient;
using LeisureInsure.DB.Business;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using LeisureInsure.DB.Utilities;
using LeisureInsure.DB.LinqClasses;
using LeisureInsure.Services.Utilities;
using System.Text.RegularExpressions;
using DalQuote = LeisureInsure.DB.Quotes;
using System.Diagnostics;
using LeisureInsure.DB.Logging;
using LeisureInsure.Services.Services.Utilities;
using LeisureInsure.ViewModels;
using CoverTypeHelper = LeisureInsure.Services.Utilities.CoverType;


namespace LeisureInsure.DB
{
    public class Repository : IRepository
    {
        private readonly LeisureInsureEntities _dbContext;
        private bool _disposed;
        //to be used by GetChargeInputs/GetCertHeaders
        private List<ChargeElementsNullable> policyCovers;
        private List<ChargeElementsNullable> policyRates;

        public Repository()
        {
            _dbContext = new LeisureInsureEntities();
            policyCovers = new List<ChargeElementsNullable>();
            policyRates = new List<ChargeElementsNullable>();
        }


        public void AddYear(string quoteRef)
        {
            Quotes q = _dbContext.Quotes.Where(x => x.QuoteRef == quoteRef).FirstOrDefault();
            DateTime dt = new DateTime();
            dt = (DateTime)q.DateTo;
            dt = dt.AddYears(1);

            q.DateTo = dt;
            _dbContext.SaveChanges();
        }
        public IList<S_GetInstallmentsSpreadSheet_Result> GetInstallments()
        {
            var i = _dbContext.S_GetInstallmentsSpreadSheet().ToList();

            return i;

        }

        public void SetInstalmentsDownloaded()
        {
            string strSql = "update Instalments set downloaded = getdate() where downloaded is null";

            _dbContext.Database.ExecuteSqlCommand(strSql);

        }

        public IList<Quotes> GetQuotesFailedTAM()
        {
            var qs = _dbContext.Quotes.Where(x => x.Purchased == 1 && string.IsNullOrEmpty(x.TamClientCode)).ToList();
            return qs;
        }


        #region BrokerPortal

        public void UpdateAspNetEmail(string user, string email)
        {
            var u = _dbContext.AspNetUsers.Where(x => x.UserName == user).FirstOrDefault();

            u.Email = email;
            _dbContext.SaveChanges();

        }
        public IList<BrokerQuoteSearchVM> getBrokerQuotes(string brokerName)
        {
            var dt = DateTime.Now;
            dt = dt.AddDays(-30);
            var quote = _dbContext.Quotes.Where(x => x.BrokerName == brokerName && x.TamClientCode == null && x.DateAdded > dt).ToList();
            IList<BrokerQuoteSearchVM> b = new List<BrokerQuoteSearchVM>();
            foreach (var q in quote)
            {
                var customerName = "";
                var p = _dbContext.Policies.Where(x => x.PolicyPK == q.PolicyFK).FirstOrDefault();
                if (q.CustomerFK > 0)
                {
                    var c = _dbContext.Contacts.Where(x => x.ContactPK == q.CustomerFK).FirstOrDefault();

                    if (!string.IsNullOrEmpty(c.EntityName))
                    {
                        customerName = c.EntityName;
                    }
                    else
                    {
                        customerName = c.FirstName + " " + c.LastName;
                    }
                }
                b.Add(new BrokerQuoteSearchVM()
                {
                    BrokerContact = q.ContactName,
                    CustomerFK = q.CustomerFK,
                    CustomerName = customerName,
                    DateCreated = q.DateAdded,
                    DatePurchased = q.DatePurchased,
                    PostCode = q.Postcode,
                    QuoteRef = q.QuoteRef,
                    Policy = p.PolicyName,
                    QuotePK = q.QuotePK
                });

            }

            return b;
        }

        public Contacts GetContactByPK(int contactPK)
        {
            var c = _dbContext.Contacts.Where(x => x.ContactPK == contactPK).FirstOrDefault();

            return c;
        }
        public Contacts GetContactByBrokerCode(string brokerCode)
        {
            var c = _dbContext.Contacts.Where(x => x.TamClientRef == brokerCode).FirstOrDefault();

            return c;
        }

        public int updateContact(Contacts contact)
        {
            if (contact.ContactPK == 0)
            {
                _dbContext.Contacts.Add(contact);
            }
            else
            {
                var c = _dbContext.Contacts.Where(x => x.ContactPK == contact.ContactPK).FirstOrDefault();
                c = contact;

            }
            _dbContext.SaveChanges();
            return contact.ContactPK;
        }

        public int updateAddress(Addresses address)
        {
            if (address.AddressPK == 0)
            {
                _dbContext.Addresses.Add(address);
            }
            else
            {
                var a = _dbContext.Addresses.Where(x => x.AddressPK == address.AddressPK).FirstOrDefault();
                a = address;

            }
            _dbContext.SaveChanges();
            return address.AddressPK;
        }

        public Addresses GetAddressByContactPK(int contactPK)
        {
            var c = _dbContext.Addresses.Where(x => x.ContactFK == contactPK).FirstOrDefault();

            return c;
        }


        #endregion

        #region weberrors

        public IList<F_GetWebErrors_Result> getWebErrors()
        {
            IList<F_GetWebErrors_Result> e = _dbContext.F_GetWebErrors().ToList();

            return e;
        }
        #endregion


        #region TamNew

        public void SetNoteTAMInTam(int QuoteNotePK)
        {
            QuoteNotes qn = _dbContext.QuoteNotes.Where(x => x.QuoteNotePK == QuoteNotePK).FirstOrDefault();

            qn.DateTAM = DateTime.Now;
            _dbContext.SaveChanges();

        }
        public IList<QuoteNotes> GetNotesForTAM(int quotePK)
        {

            IList<QuoteNotes> notes = _dbContext.QuoteNotes.Where(x => x.DateTAM == null && x.QuoteId == quotePK).ToList();

            return notes;



        }
        public void SaveToErrorLog(string quoteRef, string cause, string stack, string url, string msg)
        {

            try
            {
                //var d = DateTime.Now.AddHours(-1);
                var currentEL = _dbContext.ErrorLogs.Where(x => x.QuoteReference == quoteRef);

                if (currentEL.Count() == 0)
                {
                    var el = new ErrorLogs()
                    {
                        Cause = cause,
                        QuoteReference = quoteRef,
                        StackTrace = stack,
                        Msg = "",
                        URL = url,
                        DateStamp = DateTime.Now
                    };
                    _dbContext.ErrorLogs.Add(el);
                    _dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void AddInputCharge(int quoteInputPK, decimal charge)
        {
            QuoteInputs q = _dbContext.QuoteInputs.Where(x => x.QuoteInputPK == quoteInputPK).FirstOrDefault();
            q.Charge = charge;
            _dbContext.SaveChanges();

        }
        public Quotes GetQuotebyQuoteRef(string quoteRef)
        {
            Quotes q = _dbContext.Quotes.Where(x => x.QuoteRef == quoteRef).FirstOrDefault();
            return q;
        }
        public Quotes GetQuotebyQuotePK(int quotePK)
        {
            Quotes q = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            return q;
        }
        public Contacts GetCustomer(int contactPK)
        {
            Contacts c = _dbContext.Contacts.Where(x => x.ContactPK == contactPK).FirstOrDefault();
            return c;
        }
        public Addresses GetAddress(int addressPK)
        {
            Addresses a = _dbContext.Addresses.Where(x => x.AddressPK == addressPK).FirstOrDefault();
            return a;
        }
        public Policies GetPolicy(int policyPK)
        {
            Policies p = _dbContext.Policies.Where(x => x.PolicyPK == policyPK).FirstOrDefault();
            return p;
        }
        public void saveTamClientCode(int quotePK, string tamClientCode)
        {
            Quotes q = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            q.TamClientCode = tamClientCode;
            _dbContext.SaveChanges();

        }
        public void saveTamPolicyCode(int quotePK, string tamPolicyCode)
        {
            Quotes q = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            q.TAMPolicyCode = tamPolicyCode;
            _dbContext.SaveChanges();

        }
        public void saveTamLegalPolicyCode(int quotePK, string tamLegalPolicyCode)
        {
            Quotes q = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            q.TAMLegalPolicyCode = tamLegalPolicyCode;
            _dbContext.SaveChanges();

        }       

        public IList<TamCustomDecSchedule> getTamSchedule(int quotePK)
        {
            IList<TamCustomDecSchedule> schedules = new List<TamCustomDecSchedule>();
            var _s = _dbContext.F_TamSchedule(quotePK).ToList();

            foreach (var s in _s)
            {
                schedules.Add(new TamCustomDecSchedule()
                {
                    Description = s.Description,
                    Excess = s.Excess,
                    Insured = s.Insured,
                    Key = s.KEY,
                    Premium = s.Premium,
                    Rate = s.Rate,
                    Section = s.Section ?? 0,
                    Spec = s.Spec
                });
            }
            return schedules;
        }
        public TamCustomDecPageOne getTamPageOne(int quotePK)
        {
            //var p1  = _dbContext.F_TamSchedulePageOne(quotePK);

            var p = _dbContext.F_TamSchedulePageOne(quotePK).FirstOrDefault();

            TamCustomDecPageOne pageOne = new TamCustomDecPageOne();

            pageOne.Address = p.Address;
            pageOne.BIHA = p.BIHA;
            pageOne.ErnNumber = p.ErnNumber;
            pageOne.EventName = p.EventName;
            pageOne.NoEvents = p.NoEvents;
            pageOne.NoVisitors = p.NoVisitors;
            pageOne.Occupation = p.Occupation;
            pageOne.Showman = p.Showman;
            pageOne.Territory = p.Territory;
            pageOne.TradingAs = p.TradingAs;



            return pageOne;

        }
        public IList<TamTotals> getTamScheduleTotals(int quotePK)
        {
            var _totals = _dbContext.F_TamScheduleTotals(quotePK);
            IList<LeisureInsure.DB.Models.TamTotals> totals = new List<LeisureInsure.DB.Models.TamTotals>();

            foreach (var t in _totals)
            {
                totals.Add(new LeisureInsure.DB.Models.TamTotals()
                {
                    excess = t.Excess,
                    fee = t.Fee,
                    FieldPrefix = t.FieldPrefix,
                    gross = t.Gross,
                    indemnity = t.Indemnity,
                    insured = t.Indemnity,
                    net = t.Net,
                    tax = t.Tax
                });
            }
            return totals;
        }

        public Contacts getBrokerByEntityName(string entityName)
        {
            Contacts c = _dbContext.Contacts.Where(x => x.EntityName == entityName).FirstOrDefault();

            return c;
        }

        public string getBrokerEmailbyTamcode(string brokerName)
        {
            string brokerEmail = _dbContext.Contacts.Where(x => x.TamClientRef == brokerName && x.ContactType == 2).FirstOrDefault().Email;
            return brokerEmail;
        }

        public IList<QuoteDocuments> getQuoteDocumentsforEmail(int quotePK)
        {
            IList<QuoteDocuments> docs = _dbContext.QuoteDocuments.Where(x => x.QuoteFK == quotePK).ToList();

            return docs;
        }
        public IList<QuoteDocuments> addQuoteDocuments(int quotePK)
        {
            _dbContext.Database.ExecuteSqlCommand("S_AddQuoteDocuments {0}", quotePK);

            IList<QuoteDocuments> docs = getQuoteDocumentsforEmail(quotePK);
            return docs;

        }        


        public void setQuotePurchased(string quoteRef, bool purchased)
        {
            Quotes q = GetQuotebyQuoteRef(quoteRef);

            if (purchased == true)
            {
                q.Purchased = 1;
                q.LockedForCustomer = true;
            }
            else
            {
                q.Purchased = 0;
                q.LockedForCustomer = false;
            }
            _dbContext.SaveChanges();

        }

        public PaymentCards getPaymentCardById(int paymentCardID)
        {
            PaymentCards p = _dbContext.PaymentCards.Where(x => x.Id == paymentCardID).FirstOrDefault();

            return p;
        }

        public IList<QuoteInputs> getQuoteInputsbyQuotePK(int quotePK)
        {
            IList<QuoteInputs> q = _dbContext.QuoteInputs.Where(x => x.QuoteId == quotePK).ToList();
            return q;

        }
        public Rates getRatebyPK(int ratePK)
        {
            Rates r = _dbContext.Rates.Where(x => x.RatePK == ratePK).FirstOrDefault();
            return r;

        }
        public IList<vmOldRepositoryRate> getPolicyRatesbyRateTypeFK(int policyBW, int localeBW, int rateTypeFK)
        {
            IList<Rates> rates = _dbContext.Rates.Where(x => (x.PolicyBW & policyBW) > 0 && (x.LocaleBW & localeBW) > 0 && x.RateTypeFK == rateTypeFK && x.RateExclude == 0).ToList();
            IList<vmOldRepositoryRate> rateList = new List<vmOldRepositoryRate>();

            foreach (var r in rates)
            {
                rateList.Add(new vmOldRepositoryRate
                {
                    CoverBw = r.CoverBW,
                    DiscountFirstRate = r.DiscountFirstRate,
                    DiscountSubsequentRate = r.DiscountSubsequentRate,
                    DivBy = r.DivBy,
                    Excess = r.Excess,
                    FirstRate = r.FirstRate,
                    HazardRating = r.HazardRating,
                    Id = r.RatePK,
                    Indemnity = r.Indemnity,
                    ListRatePK = r.ListRatePK,
                    LocaleBw = r.LocaleBW,
                    MinimumRate = r.MinimumRate,
                    MustRefer = r.Refer,
                    PolicyBw = r.PolicyBW,
                    RateExclude = r.RateExclude ?? 0,
                    RateTypeFK = r.RateTypeFK ?? 0,
                    SumInsured = r.SumInsured,
                    TableParentRatePK = r.TableParentRatePK,
                    TamOccupation = r.TamOccupation,
                    Threshold = r.Threshold,
                    ValidUntil = r.ValidUntil,
                    Value = r.Rate ?? 0

                });


            }


            return rateList;

        }

        public IList<vmOldRepositoryRate> getTaxRates(int localeBW)
        {
            IList<Rates> rates = _dbContext.Rates.Where(x => (x.LocaleBW & localeBW) > 0 && x.RateTypeFK == 12 && x.RateExclude == 0).ToList();
            IList<vmOldRepositoryRate> rateList = new List<vmOldRepositoryRate>();

            foreach (var r in rates)
            {
                rateList.Add(new vmOldRepositoryRate
                {
                    CoverBw = r.CoverBW,
                    DiscountFirstRate = r.DiscountFirstRate,
                    DiscountSubsequentRate = r.DiscountSubsequentRate,
                    DivBy = r.DivBy,
                    Excess = r.Excess,
                    FirstRate = r.FirstRate,
                    HazardRating = r.HazardRating,
                    Id = r.RatePK,
                    Indemnity = r.Indemnity,
                    ListRatePK = r.ListRatePK,
                    LocaleBw = r.LocaleBW,
                    MinimumRate = r.MinimumRate,
                    MustRefer = r.Refer,
                    PolicyBw = r.PolicyBW,
                    RateExclude = r.RateExclude ?? 0,
                    RateTypeFK = r.RateTypeFK ?? 0,
                    SumInsured = r.SumInsured,
                    TableParentRatePK = r.TableParentRatePK,
                    TamOccupation = r.TamOccupation,
                    Threshold = r.Threshold,
                    ValidUntil = r.ValidUntil,
                    Value = r.Rate ?? 0

                });
            }

            return rateList;




        }

        public IList<Covers> getCoverList()
        {
            IList<Covers> c = _dbContext.Covers.ToList();


            return c;
        }

        public IQueryable<Rates> GetRateByBitWise(long? coverbitwise, int? policyBitWise, int ratetype, int localeid)
        {
            var result = _dbContext.Rates.Where(x => (x.PolicyBW & policyBitWise) > 0
                    && (x.CoverBW & coverbitwise) > 0
                    && (x.LocaleBW & localeid) > 0
                    && x.RateTypeFK == ratetype);

            return result;
        }

        public Covers GetCover(int id)
        {
            var cover = _dbContext.Covers.Where(x => x.CoverPK == id).FirstOrDefault();

            return cover;
        }


        public IList<LandingPages> getLandingPages()
        {
            IList<LandingPages> lp = _dbContext.LandingPages.Where(x => x.Enabled == true).ToList();
            return lp;
        }

        #endregion



        public TrustPilots GetTrustPilot()
        {
            var trustPilot = _dbContext.TrustPilots.FirstOrDefault();

            return trustPilot;

        }

        public void saveTrustPilot(TrustPilots t)
        {
            TrustPilots tp = _dbContext.TrustPilots.Where(x => x.Id == t.Id).FirstOrDefault();
            tp.Description = t.Description;
            tp.LastUpdated = DateTime.Now;
            tp.Stars = t.Stars;
            _dbContext.SaveChanges();


        }

        public DalQuote GetQuote(string quoteref)
        {
            var quote = _dbContext.Quotes.Where(x => x.QuoteRef == quoteref).FirstOrDefault();

            return quote;

        }

        public void RenewalEmailSent(int quotePK)
        {
            var ren = _dbContext.RenewalQuotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            if (ren != null)
            {
                ren.EmailedClientDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
                _dbContext.SaveChanges();
            }
        }

        public void deleteCerts(string quoteRef)
        {
            _dbContext.Database.ExecuteSqlCommand("S_DeleteUnlockedCerts {0}", quoteRef);

        }
        public IEnumerable<vmRenewalListItem> GetRenewalsByCsr(string csr)
        {

            vmRenewalList RList = new vmRenewalList();
            IList<vmRenewalListItem> RL = new List<vmRenewalListItem>();

            var rl = (from i in _dbContext.RenewalQuotes.Where(x => x.Csr == csr || csr == "All")
                      select i).Where(x => x.Purchased == 0).ToList();

            foreach (var i in rl)
            {
                DateTime expDate = (DateTime)i.DateFrom;
                var clientRecord = (from ii in _dbContext.RenewalContacts.Where(x => x.TamPolicyRef == i.RenewalTamPolicyRef)
                                    select ii).FirstOrDefault();
                var ClientName = "";
                if (clientRecord != null)
                {
                    if (!string.IsNullOrEmpty(clientRecord.FirstName))
                    {
                        ClientName = clientRecord.FirstName + ' ' + clientRecord.LastName;
                    }
                    else
                    {
                        ClientName = clientRecord.EntityName;
                    }
                    ClientName = ClientName.Replace("  ", " ").Replace("  ", " ");
                    RL.Add(new vmRenewalListItem()
                    {
                        TamPolicyRef = i.RenewalTamPolicyRef,
                        TamQuoteNo = i.RenewalTamPolicyNo,
                        ClientName = ClientName,
                        ExpiryDate = expDate,
                        QuoteRef = i.QuoteRef,
                        EmailedDate = i.EmailedClientDate,
                        TamBroker = i.BrokerName

                    });
                }
            };




            //RList.RenewalListItems =  RL.ToList();
            return RL.ToList();
        }


        public string GetAspNetUser(string userName)
        {
            string _user = "Not Found";
            var user = _dbContext.AspNetUsers.Where(x => x.UserName == userName).FirstOrDefault();

            if (user != null)
            {
                _user = user.UserName;
            }

            return _user;
        }

        public int setQuoteCustomerPK(int quotePK)
        {
            _dbContext.Database.ExecuteSqlCommand("exec S_SetQuoteCustomerPK {0}", quotePK);
            int? CustomerPK = _dbContext.Quotes.Where(y => y.QuotePK == quotePK).Select(x => x.CustomerFK).FirstOrDefault();
            return CustomerPK ?? 0;
        }

        public void GetBundledCoversAndRates(Policies policy, int localeid, string territory, List<QuoteInputs> quoteInputs)
        {
            //get policy specific rates and covers
            switch (policy.PolicyPK)
            {
                case PolicyType.FoodContamination:
                    var foodCovers = (from r in _dbContext.Rates
                                      join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK
                                      join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                                      where (r.LocaleBW & localeid) > 0 && (r.PolicyBW & policy.BitWise) > 0 && r.RateTypeFK == RateType.FoodCovers
                                      select new CoverRates
                                      {
                                          rates = r,
                                          covers = c,
                                          policies = p
                                      }).ToList();
                    policyCovers = ChargeElementMapper.MapCovers(foodCovers, territory);
                    //get rates 
                    var foodRates = (from r in _dbContext.Rates
                                     join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK
                                     join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                                     where (r.LocaleBW & localeid) > 0 && (r.PolicyBW & policy.BitWise) > 0 && r.RateTypeFK == RateType.MinCharge
                                     select new CoverRates
                                     {
                                         rates = r,
                                         covers = c,
                                         policies = p
                                     }).ToList();
                    policyRates = ChargeElementMapper.MapRates(foodRates, territory);
                    break;
                case PolicyType.FreelanceActivity:
                case PolicyType.FreelanceSports:
                    //get our hazards(categories) and indemnity
                    var cats = quoteInputs.Where(x => x.RateTypeFK == RateType.HazardRating).ToList();
                    var indemnity = quoteInputs.Where(x => x.RateTypeFK == RateType.Indemnity).FirstOrDefault();
                    var indemRate = _dbContext.Rates.Where(x => x.RatePK == indemnity.RatePK).FirstOrDefault();
                    //get rates based on our categories
                    var activityIds = cats.Select(x => x.RatePK).ToList();
                    var activities = _dbContext.Rates
                               .Where(t => activityIds.Contains(t.RatePK)).ToList();
                    //get our highest category, add it to our list
                    var worstHazard = activities.OrderByDescending(x => x.HazardRating).FirstOrDefault();
                    var hazardRate = _dbContext.Rates.Where(x => x.RateName == indemRate.RateName && x.HazardRating == worstHazard.HazardRating
                        && (x.PolicyBW & policy.BitWise) > 0).FirstOrDefault();
                    var hazardInfo = (from r in _dbContext.Rates
                                      join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                                      join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK
                                      where r.RatePK == hazardRate.RatePK
                                      select new CoverRates
                                      {
                                          rates = r,
                                          policies = p,
                                          covers = c
                                      }).FirstOrDefault();

                    var activityCat = ChargeElementMapper.MapRate(hazardInfo, territory);
                    policyRates.Add(activityCat);

                    //add professional cover
                    var professionalCover = (from r in _dbContext.Rates
                                             join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK
                                             join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                                             where (r.LocaleBW & localeid) > 0 && (r.PolicyBW & policy.BitWise) > 0
                                             && (r.CoverCalcFK == CoverTypeHelper.Professional)
                                             select new CoverRates
                                             {
                                                 rates = r,
                                                 covers = c,
                                                 policies = p
                                             }).ToList();
                    policyCovers = ChargeElementMapper.MapCovers(professionalCover, territory);



                    break;
                case PolicyType.Exhibitors:
                    var plCover = quoteInputs.Where(x => x.RateTypeFK == 10).FirstOrDefault();
                    var exhibitplRate = _dbContext.Rates.Where(x => x.RatePK == plCover.RatePK).FirstOrDefault();

                    var exhibitorCovers = (from r in _dbContext.Rates
                                           join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK
                                           join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                                           where (r.LocaleBW & localeid) > 0 && (r.PolicyBW & policy.BitWise) > 0
                                           && (r.CoverCalcFK == CoverTypeHelper.MDBusinessEquipment || r.CoverCalcFK == CoverTypeHelper.Cancellation)
                                           && r.TableChildren == exhibitplRate.TableFirstColumn
                                           select new CoverRates
                                           {
                                               rates = r,
                                               covers = c,
                                               policies = p
                                           }).ToList();
                    policyCovers = ChargeElementMapper.MapCovers(exhibitorCovers, territory);


                    break;
                case PolicyType.ConfandMeetings:
                    var confpl = quoteInputs.Where(x => x.RateTypeFK == 10).FirstOrDefault();
                    var confplRate = _dbContext.Rates.Where(x => x.RatePK == confpl.RatePK).FirstOrDefault();

                    var meetingCovers = (from r in _dbContext.Rates
                                         join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK
                                         join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                                         where (r.LocaleBW & localeid) > 0 && (r.PolicyBW & policy.BitWise) > 0
                                         && (r.CoverCalcFK == CoverTypeHelper.MDBusinessEquipment || r.CoverCalcFK == CoverTypeHelper.Cancellation)
                                         && r.TableChildren == confplRate.TableFirstColumn
                                         select new CoverRates
                                         {
                                             rates = r,
                                             covers = c,
                                             policies = p
                                         }).ToList();
                    policyCovers = ChargeElementMapper.MapCovers(meetingCovers, territory);

                    break;
                default:

                    //var pl = quoteInputs.Where(x => x.RateTypeFK == 10).FirstOrDefault();
                    //var plRate = _dbContext.Rates.Where(x => x.RatePK == pl.RatePK).FirstOrDefault();                    
                    //need a much better way of bundling on free covers, we get nothing back from the form to tell us if 
                    //the table includes product/employer cover? example Mobile Catering
                    var freecovers = (from r in _dbContext.Rates
                                      join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK
                                      join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                                      where (r.RateName == "Public Liability" || r.RateName == "Product Liability" || r.RateName == "Employer Liability")
                                      && (r.LocaleBW & localeid) > 0 && (r.PolicyBW & policy.BitWise) > 0 && r.TableChildren == 1
                                      select new CoverRates
                                      {
                                          rates = r,
                                          covers = c,
                                          policies = p
                                      }).ToList();

                    policyRates = ChargeElementMapper.MapRates(freecovers, territory);
                    break;
            }

        }


        /// <summary>
        /// Get chargeable rates to pass on to the pricing engine
        /// </summary>
        /// <param name="quoteid"></param>
        /// <param name="localeid"></param>
        /// <returns></returns>
        public List<ChargeElements> GetChargeInputs(int quoteid, int localeid)
        {
            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quoteid).FirstOrDefault();
            try
            {
                var policy = _dbContext.Policies.Where(x => x.PolicyPK == quote.PolicyFK).FirstOrDefault();
                var territory = LocaleHelper.GetLocale(localeid);
                var quoteInputs = _dbContext.QuoteInputs.Where(x => x.QuoteId == quoteid).ToList();

                var elCovers = new List<ChargeElementsNullable>();

                GetBundledCoversAndRates(policy, localeid, territory, quoteInputs);

                //have we added Employers liability?
                var ELInputs = quoteInputs.Where(x => x.CoverPK == CoverTypeHelper.EmployersLiability).ToList();
                if (ELInputs.Count > 0)
                {
                    //add minimum charge
                    var elRates = (from r in _dbContext.Rates
                                   join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK
                                   from p in _dbContext.Policies
                                   where r.RateTypeFK == RateType.MinCharge && r.CoverCalcFK == CoverTypeHelper.EmployersLiability
                                   && (r.PolicyBW & policy.BitWise) > 0 && p.PolicyPK == policy.PolicyPK
                                   select new CoverRates
                                   {
                                       rates = r,
                                       covers = c,
                                       policies = p
                                   }).ToList();
                    elCovers = ChargeElementMapper.MapCovers(elRates, territory);
                }

                var fee = (from r in _dbContext.Rates
                           join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                           where r.RateTypeFK == RateType.Fee && (r.LocaleBW & localeid) > 0 && (r.PolicyBW & policy.BitWise) > 0 && r.RateExclude == 0
                           select new CoverRates
                           {
                               rates = r,
                               policies = p
                           }).FirstOrDefault();

                var rates = (from qi in quoteInputs
                             join p in _dbContext.Policies on policy.PolicyPK equals p.PolicyPK
                             join r in _dbContext.Rates on qi.RatePK equals r.RatePK
                             join c in _dbContext.Covers on r.CoverCalcFK equals c.CoverPK into coverscontainer
                             from c in coverscontainer.DefaultIfEmpty()
                             join rt in _dbContext.RateTypes on qi.RateTypeFK equals rt.RateTypePK
                             where rt.Ratable == 1 ||
                             rt.RateTypePK == RateType.PostCode
                             select new CoverRates
                             {
                                 quoteinputs = qi,
                                 ratetypes = rt,
                                 rates = r,
                                 covers = c,
                                 policies = p
                             }).ToList();

                var mainRates = ChargeElementMapper.MapChargeableRates(rates, territory);

                if (fee != null)
                {
                    var feecharge = ChargeElementMapper.MapRate(fee, territory);
                    mainRates.Add(feecharge);
                }

                foreach (var cover in elCovers)
                {
                    mainRates.Add(cover);
                }

                //add policy specific covers
                foreach (var cover in policyCovers)
                {
                    mainRates.Add(cover);
                }

                //add policy specific rates
                foreach (var rate in policyRates)
                {
                    mainRates.Add(rate);
                }

                //map nullable version back to our proper version for the pricing engine            
                var chargeElements = ChargeElementMapper.MapChargeElements(mainRates);
                return chargeElements;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[LeisureInsure.DB.Repository.GetChargeInputs]", quoteid.ToString());
                throw new ApplicationException($"{errorid}");
            }

        }



        /// <summary>
        /// Get documents for a quote
        /// </summary>
        /// <param name="quotePK"></param>
        /// <returns></returns>
        public IList<vmDocument> getDocuments(int quotePK)
        {

            // new method superceding the use of F_GetDocumentsForQuote


            // delete documents held against the quote in quoteDocuments if they have not been locked in
            _dbContext.Database.ExecuteSqlCommand("DELETE FROM QuoteDocuments WHERE QuoteFK = {0} AND ISNULL(LOCK,0) =0", quotePK);


            // using table valued function to ensure the records are refreshed (entity framework doesn't refresh if records altered via carls' repository
            var quote = _dbContext.F_GetQuoteByPK(quotePK).FirstOrDefault();
            // get documents table
            var docTable = _dbContext.Documents.ToList();

            // QuoteLines to check if MDSecurity or PortableEquipmentSecurity endorsements are required
            var coverClauses = _dbContext.QuoteLines.Where(x => x.QuoteId == quotePK).ToList();

            // Specific rates hold additional and alternative endorsements
            var rateClauses = _dbContext.QuoteInputs.Where(x => x.QuoteId == quotePK).ToList();

            // Most policies have endorsements associated with them
            var pol = _dbContext.Policies.Where(x => x.PolicyPK == quote.PolicyFK).FirstOrDefault();
            List<vmDocument> docs = new List<vmDocument>();

            // table to hold endorsements
            var docList = new List<Documents>();

            // Predefined documents required against any policy
            Documents PolicyWording = null;
            Documents KeyFacts = null;
            Documents LegalPolicyWording = null;
            Documents LegalKeyFacts = null;
            Documents Terms = null;
            Documents SoF = null;

            // Policy endorsement is defined because it may need to be replaced by irish or other endorsement depending on what equipment/activities are selected by the user
            Documents PolicyClause = null;

            // MDSecurity can be document 65 if any material damage cover is selected but must be document 65 if business equipment is selected by user
            Documents MDSecurity = null;


            // POlicyWording & keyfacts
            if(quote.PolicyFK == 10 || quote.PolicyFK == 11)
            {
                PolicyWording = docTable.Where(x => x.DocumentPK == 86).FirstOrDefault();
            }
            else if (quote.PolicyFK == 27)
            {
                PolicyWording = docTable.Where(x => x.DocumentPK == 80).FirstOrDefault();
            }
            else if (quote.PolicyType == "E")
            {
                if (quote.LocaleId == 1)
                {
                    PolicyWording = docTable.Where(x => x.DocumentPK == 7).FirstOrDefault();
                }
                else if (quote.LocaleId == 2)
                {
                    PolicyWording = docTable.Where(x => x.DocumentPK == 4).FirstOrDefault();
                    KeyFacts = docTable.Where(x => x.DocumentPK == 3).FirstOrDefault();
                }
            }
            else if (!string.IsNullOrEmpty(quote.BIHA) && quote.BIHA.ToUpper() != "NO")
            {
                if (quote.LocaleId == 1)
                {
                    PolicyWording = docTable.Where(x => x.DocumentPK == 6).FirstOrDefault();
                    KeyFacts = docTable.Where(x => x.DocumentPK == 5).FirstOrDefault();
                }
                else if (quote.LocaleId == 2)
                {
                    PolicyWording = docTable.Where(x => x.DocumentPK == 9).FirstOrDefault();
                    KeyFacts = docTable.Where(x => x.DocumentPK == 8).FirstOrDefault();
                }
            }
            else
            {
                if (quote.LocaleId == 1)
                {
                    PolicyWording = docTable.Where(x => x.DocumentPK == 2).FirstOrDefault();
                    KeyFacts = docTable.Where(x => x.DocumentPK == 1).FirstOrDefault();
                }
                else if (quote.LocaleId == 2)
                {
                    PolicyWording = docTable.Where(x => x.DocumentPK == 4).FirstOrDefault();
                    KeyFacts = docTable.Where(x => x.DocumentPK == 3).FirstOrDefault();
                }
            }

            //Terms of business
            if (quote.IsAgent == true)
            {
                Terms = docTable.Where(x => x.DocumentPK == 83).FirstOrDefault();
            }
            else
            {
                Terms = docTable.Where(x => x.DocumentPK == 84).FirstOrDefault();
            }

            SoF = docTable.Where(x => x.DocumentPK == 64).FirstOrDefault();

            // Legal
            if (quote.LegalFeesAdded == true)
            {
                LegalPolicyWording = docTable.Where(x => x.DocumentPK == 40).FirstOrDefault();
                LegalKeyFacts = docTable.Where(x => x.DocumentPK == 39).FirstOrDefault();
            }


            // if other activities then poldoc will be replaced with clause 
            // if showmen then no md clause

            //Add policydocs to doclist
            docList.Add(PolicyWording);
            if (KeyFacts != null)
            {
                docList.Add(KeyFacts);
            }
            if (LegalKeyFacts != null)
            {
                docList.Add(LegalKeyFacts);
                docList.Add(LegalPolicyWording);
            }
            docList.Add(Terms);
            docList.Add(SoF);



            // add Security endorsement if md is included, if business equip is included then portable equip endorsement must be added (only)
            foreach (var c in coverClauses)
            {
                var cover = _dbContext.Covers.Where(x => x.CoverPK == c.CoverFK).FirstOrDefault();
                if (cover != null)
                {
                    if (quote.PolicyFK == 4 && cover.Section == 4)
                    {
                        MDSecurity = docTable.Where(x => x.DocumentPK == 65).FirstOrDefault();
                        break;
                    }
                    if (cover.CoverPK == 8 || cover.CoverPK == 13)
                    {
                        MDSecurity = docTable.Where(x => x.DocumentPK == 38).FirstOrDefault();
                        break;
                    }
                    if (cover.Section == 4)
                    {
                        MDSecurity = docTable.Where(x => x.DocumentPK == 65).FirstOrDefault();
                    }
                }
            }

            if (MDSecurity != null && quote.PolicyFK != 26)
            {
                // if other activities then normal mdsecurity 
                docList.Add(MDSecurity);
            }

            // Endorsements included depending on policy selected - Policy table
            var polClause = _dbContext.Policies.Where(x => x.PolicyPK == quote.PolicyFK).FirstOrDefault();

            if (!string.IsNullOrEmpty(polClause.ClauseCode))
            {
                PolicyClause = docTable.Where(x => x.ClauseCode == polClause.ClauseCode).FirstOrDefault();
                docList.Add(PolicyClause);

            }

            // Endorsements based on policyBW in documents table
            foreach (var d in docTable.Where(x => x.PolicyBW > 0 && x.DocType.ToUpper() == "CLAUSE"))
            {
                var rem = docList.Where(x => x.ClauseCode == d.ClauseCode).FirstOrDefault();

                if ((pol.BitWise & d.PolicyBW) > 0 && (quote.LocaleId & d.LocaleBW) > 0)
                {
                    if (rem != null)
                    {
                        docList.Remove(rem);
                    }
                    PolicyClause = d;
                    docList.Add(d);
                }
            }


            // endorsements based on ratefk in documents table or clausecode held in rates table
            foreach (var c in rateClauses)
            {
                var rate = _dbContext.Rates.Where(x => x.RatePK == c.RatePK).FirstOrDefault();
                var doc = docTable.Where(x => x.RateFK == c.RatePK).FirstOrDefault();

                if (rate != null && !string.IsNullOrEmpty(rate.ClauseCode))
                {
                    var clause = docTable.Where(x => x.ClauseCode == rate.ClauseCode && (x.LocaleBW & quote.LocaleId) > 0).FirstOrDefault();
                    if (clause != null)
                    {
                        if (docList.FindIndex(x => x.ClauseCode == rate.ClauseCode) == -1)
                        {

                            if (clause.ClauseCode == "D10" || quote.PolicyFK == 4)
                            {
                                docList.Remove(PolicyClause);
                            }
                            docList.Add(clause);
                        }
                    }
                }
                if (doc != null)
                {
                    if (docList.FindIndex(x => x.ClauseCode == doc.ClauseCode) == -1)
                    {
                        docList.Add(doc);
                    }
                }


            }
            docList = docList.Distinct().ToList();

            // Hacks
            if (quote.PolicyFK == 1 && (rateClauses.FindIndex(x => x.RatePK == 151) > -1 || (rateClauses.FindIndex(x => x.RatePK == 5267) > -1)))
            {
                // if either of the nightclub/pub questions are answered positively then the pub endorsement replaces the policy endorsement
                var pubClause = docTable.Where(x => x.DocumentPK == 85).FirstOrDefault();
                docList.Remove(PolicyClause);
                docList.Add(pubClause);
            }

            if (quote.PolicyFK == 26 && quote.LocaleId == 2)
            {
                var cateringTrailor = docTable.Where(x => x.DocumentPK == 73).FirstOrDefault();

                if (docList.FindIndex(x => x.ClauseCode == "D2") > -1)
                {
                    docList.Remove(docList.Where(x => x.ClauseCode == "D2").FirstOrDefault());

                }
                docList.Add(cateringTrailor);
            }
            //if (quote.PolicyFK == 25)
            //{
            //    var dayCover = docTable.Where(x => x.DocumentPK == 13).FirstOrDefault();
            //    docList.Add(dayCover);
            //}

            docList = docList.Distinct().ToList();


            foreach (var doc in docList)
            {
                var link = doc.DocLink + ".pdf";
                if (doc.DocType.ToUpper() == "SOF")
                {
                    link = "GetStatementofFact/" + quote.QuoteRef;
                }

                _dbContext.QuoteDocuments.Add(new QuoteDocuments()
                {
                    Description = doc.DocName,
                    QuoteFK = quotePK,
                    Url = link,
                    DocumentType = doc.DocType,
                    DocumentFK = doc.DocumentPK,
                    DateStamp = DateTime.Now,
                    Lock = 0
                });


            }
            _dbContext.SaveChanges();

            IList<QuoteDocuments> quoteDocs = _dbContext.QuoteDocuments.Where(x => x.QuoteFK == quotePK).ToList();
            foreach (QuoteDocuments doc in quoteDocs)
            {
                docs.Add(new vmDocument()
                {
                    DocumentLink = doc.Url,
                    DocumentType = doc.DocumentType,
                    LinkName = doc.Url,
                    DocumentFK = doc.DocumentFK ?? 0,
                });

            }

            return docs;
        }
        public string getPolicyType(int policyPK)
        {
            string PolicyType = (from i in _dbContext.Policies
                                 where i.PolicyPK == policyPK
                                 select i.PolicyType).FirstOrDefault();

            return PolicyType;

        }
        public IList<vmDocument> getQuoteDocuments(int quotePK)
        {

            List<vmDocument> docs = new List<vmDocument>();
            var dbdocs = (from i in _dbContext.QuoteDocuments
                          where i.QuoteFK == quotePK
                          select i).ToList();
            foreach (var d in dbdocs)
            {

                docs.Add(new vmDocument()
                {
                    DocumentLink = d.Url,
                    DocumentType = d.DocumentType,
                    LinkName = d.DocumentType

                });
            }

            return docs;
        }
        public vmEndorsements getQuoteEndorsements(int quotePK)
        {
            IList<vmEndorsement> htmlx = new List<vmEndorsement>();
            vmEndorsements htmlList = new vmEndorsements();
            var dbdocs = (from i in _dbContext.QuoteEndorsements
                          where i.QuoteId == quotePK
                          select i).ToList();

            foreach (var i in dbdocs)
            {
                htmlx.Add(new vmEndorsement()
                {
                    html = i.Text
                });

            }


            htmlList.Endorsements = htmlx.ToList();
            return htmlList;

        }
        public void deleteContactss(int quotePK)
        {
            _dbContext.Database.ExecuteSqlCommand("DELETE FROM Contacts WHERE quotePK = {0}", quotePK);

            _dbContext.SaveChanges();

        }
        public string getBrokerEmail(string tamCode)
        {
            var email = "Info@Leisureinsure.co.uk";
            var broker = (from i in _dbContext.Contacts
                          where i.TamClientRef == tamCode
                          select i).FirstOrDefault();

            if (broker != null)
            {
                email = broker.Email;

            }
            return email;
        }        

        public void deleteQuoteInputs(int quotePK)
        {

            Quotes q = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            _dbContext.Database.ExecuteSqlCommand("S_DeleteQuoteChildren {0}", q.QuoteRef);
            _dbContext.SaveChanges();

        }
        public vmBrokerAddress getBrokerAddress(int quotePK)
        {
            var b = (from i in _dbContext.F_GetBrokerAddress(quotePK)
                     select i).FirstOrDefault();
            vmBrokerAddress br = new vmBrokerAddress()
            {
                Address1 = b.Address1,
                Address2 = b.Address2,
                DateEffective = (DateTime)b.DateFrom,
                Address3 = b.Address3,
                ContactPK = (int)b.ContactPK,
                Country = b.Country,
                County = b.County,
                EntityName = b.EntityName,
                Postcode = b.Postcode,
                Telephone = b.Telephone,
                Town = b.Town

            };

            return br;

        }

        public vmQuote getQuote(int quotePK)
        {
            var dbQuote = (from i in _dbContext.Quotes
                           where i.QuotePK == quotePK
                           select i).FirstOrDefault();

            vmQuote quote = new vmQuote()
            {
                BrokerFK = dbQuote.BrokerFK,
                QuotePK = dbQuote.QuotePK,
                LocaleID = dbQuote.LocaleId

            };
            if (quote.LocaleID == 1)
            {
                quote.strLocale = "en-GB";
            }
            else if (quote.LocaleID == 2)
            {
                quote.strLocale = "ga-IE";
            }


            return quote;

        }
        public int AddQuote(Quotes quote)
        {

            _dbContext.Quotes.Add(quote);
            _dbContext.SaveChanges();
            quote.QuoteRef = "LEI" + quote.QuotePK.ToString("D8");
            _dbContext.SaveChanges();
            return quote.QuotePK;
        }
        public void AddQuoteInputs(IList<QuoteInputs> quoteInputs)
        {
            int quotePK = (from x in quoteInputs
                           select x.QuoteId).FirstOrDefault();

            var quote = (from i in _dbContext.Quotes
                         where i.QuotePK == quotePK
                         select i).FirstOrDefault();

            try
            {
                //hack to remove md if not selected
                if (quote.PolicyFK == 1)
                {
                    var y = (from i in quoteInputs
                             where i.RatePK == 156
                             select i).FirstOrDefault();
                    if (y == null || y.SumInsured == 0)
                    {
                        quoteInputs.Remove(quoteInputs.Where(x => x.RatePK == 1784).FirstOrDefault());
                        quoteInputs.Remove(quoteInputs.Where(x => x.RatePK == 1793).FirstOrDefault());
                        quoteInputs.Remove(quoteInputs.Where(x => x.RatePK == 1788).FirstOrDefault());


                    }
                }
                foreach (QuoteInputs qi in quoteInputs)
                {
                    _dbContext.QuoteInputs.Add(qi);
                }
                _dbContext.SaveChanges();

            }
            catch (Exception ex)
            {

                var errorid = AppInsightLog.LogError(ex, "[LeisureInsure.DB.Repository.AddQuoteInputs]", quotePK.ToString());
                throw new ApplicationException($"{errorid}");
            }

        }
        public void ReferQuote(int quotePK = 0, string quoteRef = "")
        {
            if (quotePK > 0)
            {
                _dbContext.Database.ExecuteSqlCommand("Update quotes set referraltype = 1 where quotepk =  {0}", quotePK);
            }
            else if (quoteRef != "")
            {
                _dbContext.Database.ExecuteSqlCommand("Update quotes set referraltype = 1 where quoteref =  {0}", quoteRef);
            }
        }
        public IList<vmInputsFlat> getFlatInputs(int localeBw, int policyPk, int quotePk)
        {
            IList<vmInputsFlat> inputs = new List<vmInputsFlat>();
            var srcinputs = _dbContext.F_Inputs(localeBw, policyPk, quotePk);

            foreach (var i in srcinputs.OrderBy(x => x.CoverPK).OrderBy(x => x.InputOrdinal).OrderBy(x => x.RateOrdinal).OrderBy(x => x.RateName))
            {
                var req = true;
                if (i.ngRequired == 0)
                {
                    req = false;
                }
                inputs.Add(new vmInputsFlat()
                {
                    ChildBW = i.ChildBW,
                    CoverPK = i.CoverPK,
                    DivBy = i.DivBy,
                    Excess = i.Excess,
                    InputOrdinal = i.InputOrdinal,
                    InputSelectedPK = i.InputSelectedPK,
                    InputTypeFK = i.InputTypeFK,
                    ListNo = i.ListNo,
                    ListOrdinal = i.ListOrdinal,
                    ListRatePK = i.ListRatePK,
                    ParentBW = i.ParentBW,
                    ParentRatePK = i.ParentRatePK,
                    Rate = i.Rate,
                    RateLabel = i.RateLabel,
                    RateName = i.RateName,
                    RateOrdinal = i.RateOrdinal,
                    RatePK = i.RatePK,
                    RateTip = i.RateTip,
                    RateValue = i.RateValue,
                    RateVis = i.RateVis,
                    Refer = i.Refer,
                    TableChildren = i.TableChildren,
                    TableFirstColumn = i.TableFirstColumn,
                    Threshold = i.Threshold,
                    UnitType = i.UnitType,
                    DisplayBW = i.DisplayBW,
                    lblNumber = i.lblNumber,
                    lblSumInsured = i.lblSumInsured,
                    SumInsured = i.SumInsured,
                    NoItems = i.NoItems,
                    ReturnBW = i.ReturnBW,
                    PageNo = i.PageNo,
                    SubRateHeading = i.SubRateHeading,
                    TrigSubHeadings = i.TrigSubHeadings,
                    RateTypeFK = i.RateTypeFK,
                    DiscountFirstRate = i.DiscountFirstRate,
                    DiscountSubsequentRate = i.DiscountSubsequentRate,
                    CoverCalcFK = (int)i.CoverCalcFK,
                    RateGroup = (int)i.RateGroup,
                    InputTrigger = i.InputTriggerFK,
                    ngDisabled = 0,
                    TradeFilter = (int)i.tradeFilter,
                    ngRequired = req,
                    SectionNo = i.SectionNo,
                    TableParentRatePK = i.TableParentRatePK,
                    Indemnity = (int)i.Indemnity
                });
            }
            return inputs;
        }

        public vmNavigation _getHome(vmLocales locales, int quotePk, int policyPk)
        {
            vmNavigation nav;
            nav = new vmNavigation();
            var locale = 1;
            if (locales.localeSelected != null)
            {
                locale = locales.localeSelected.localePK;
            }
            else
            {
                locales.localeSelected = locales.localeList.FirstOrDefault();

            }

            var dateNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
            var dateStart = dateNow;
            var dateEnd = dateNow;
            IList<vmInputsFlat> inputsFlatx = new List<vmInputsFlat>();

            if (quotePk > 0)
            {

                locales.localeSelected.showFlag = false;
                var dbQuote = (from i in _dbContext.Quotes
                               where i.QuotePK == quotePk
                               select i).FirstOrDefault() ?? new Quotes();

                if (dbQuote.QuotePK != quotePk)
                {
                    quotePk = -1;
                }

                var progress = dbQuote.Progress ?? 1;
                dateStart = dbQuote.DateFrom ?? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
                dateEnd = dbQuote.DateTo ?? TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

                locale = dbQuote.LocaleId;

                policyPk = dbQuote.PolicyFK ?? -1;

                inputsFlatx = _getQuoteInputs(locales.localeSelected.localeBW, policyPk, quotePk);

                nav = new vmNavigation()
                {
                    quotePK = quotePk,
                    LocaleId = locale,
                    progress = progress,
                    PolicyPK = policyPk,
                    CurrentPage = 1,
                    dateFrom = dateStart,
                    dateTo = dateEnd,
                    locales = locales,
                    policies = _getPolicies(locale, policyPk),
                    covers = _getCovers(locale, policyPk, quotePk),
                    inputsFlat = inputsFlatx
                };
            }
            else if (policyPk > 0)
            {
                foreach (vmLocale loc in locales.localeList)
                {
                    loc.showFlag = false;

                }
                locales.localeSelected.showFlag = false;
                //inputs = _getInputs(locales.localeSelected.localeBW, policyPk, quotePk);
                inputsFlatx = getFlatInputs(locales.localeSelected.localeBW, policyPk, quotePk);
                nav = new vmNavigation()
                {
                    quotePK = 0,
                    LocaleId = locales.localeSelected.localePK,
                    progress = 1,
                    PolicyPK = policyPk,
                    CurrentPage = 1,
                    dateFrom = dateStart,
                    dateTo = dateEnd,
                    locales = locales,
                    policies = _getPolicies(locales.localeSelected.localeBW, policyPk),
                    covers = _getCovers(locale, policyPk, 0),
                    //inputs = inputs,
                    //SaveInputs = saveInputs,
                    inputsFlat = inputsFlatx
                };
            }
            else
            {
                foreach (vmLocale loc in locales.localeList)
                {
                    loc.showFlag = true;

                }
                locales.localeSelected.showFlag = true;
                //inputsFlat = getFlatInputs(locales.localeSelected.localeBW, policyPk, quotePk);
                nav = new vmNavigation()
                {
                    quotePK = 0,
                    LocaleId = locales.localeSelected.localePK,
                    progress = 0,
                    PolicyPK = policyPk,
                    CurrentPage = 1,
                    dateFrom = dateStart,
                    dateTo = dateEnd,
                    locales = locales,
                    policies = _getPolicies(locales.localeSelected.localeBW, policyPk),
                    covers = _getCovers(locale, policyPk, 0),
                    //inputs = inputs,
                    //SaveInputs = saveInputs,
                    inputsFlat = null
                };
            }

            return nav;
        }

        public Locales getLocale(int localePK)
        {
            Locales loc = (from i in _dbContext.Locales
                           where i.LocalePK == localePK
                           select i).FirstOrDefault();

            return loc;
        }       

        public vmPolicies _getPolicies(int localeBw, int policyPk)
        {
            IList<vmPolicy> policyList = new List<vmPolicy>();
            var _pl = _dbContext.F_Policies(localeBw, policyPk).ToList();
            foreach (var pl in _pl)
            {
                policyList.Add(new vmPolicy()
                {
                    PolicyPK = pl.PolicyPK,
                    PolicyName = pl.PolicyName,
                    PolicyBW = pl.PolicyBW,
                    PremiseRequired = pl.PremiseRequired,
                    TimeRequired = pl.TimeRequired,
                    PeriodDay = pl.PeriodDay,
                    PeriodYear = pl.PeriodYear,
                    CoverBW = pl.CoverBW,
                    policyPKSelected = pl.policyPKSelected,
                    MinDate = pl.MinDate,
                    MaxDate = pl.MaxDate
                });
            }

            var policies = new vmPolicies()
            {
                policyList = policyList,
                policySelected = policyList.FirstOrDefault(x => x.policyPKSelected > 0)
            };
            return policies;
        }

        public vmCovers _getCovers(int localeBw, int policyPk, int quotePk)
        {
            if (policyPk == 0)
            {
                policyPk = -1;

            }
            IList<vmCover> coverList = new List<vmCover>();
            var coverResultList = _dbContext.F_Covers(localeBw, policyPk, quotePk).ToList();

            foreach (F_Covers_Result cl in coverResultList)
            {
                //_pages = _inputs.Where(x => x.CoverPK == cl.CoverPK).Max(x => x.PageNo);
                int optional;
                if (cl.CoverPK < 3)
                {
                    optional = 0;
                }
                else
                {
                    optional = 1;
                }
                coverList.Add(new vmCover()
                {
                    CoverPK = cl.CoverPK,
                    SectionName = cl.SectionName,
                    SectionNumber = cl.Section,
                    CoverName = cl.CoverName,
                    CoverTip = cl.CoverTip,
                    coverPKSelected = cl.coverPKSelected,
                    Pages = cl.Pages,
                    BitWise = cl.bitwise,
                    Optional = optional,
                    CoverDetailsCompleted = cl.CoverCompleted,
                    ReferValue = cl.ReferValue
                    //,
                    //SaveInputs = _inputs.Where(y => y.CoverPK == cl.CoverPK && (y.ListNo != 0 || y.InputSelectedPK !=0))
                });
            }
            var covers = new vmCovers()
            {
                coverSelected = coverList.Where(x => x.CoverPK == 1),
                coverList = coverList,
            };
            return covers;
        }


        public List<vmInputHeader> _getInputs(int localeBw, int policyPk, int quotePk)
        {
            //the list of inputs that we are returning
            List<vmInputHeader> inputs = new List<vmInputHeader>();

            IList<vmInputDetails> inputDetail = new List<vmInputDetails>();
            IList<vmInputDetails> inputDetailList = new List<vmInputDetails>();

            var inputHeaders = _dbContext.F_Inputs(localeBw, policyPk, quotePk);

            foreach (var id in inputHeaders.Where(x => x.ListOrdinal == 0))
            {
                inputDetail.Add(new vmInputDetails
                {
                    CoverPK = id.CoverPK,
                    RateOrdinal = id.RateOrdinal,
                    Rate = id.Rate,
                    RatePK = id.RatePK,
                    RateName = id.RateName,
                    ChildBW = id.ChildBW,
                    RateVis = id.RateVis,
                    RateValue = id.RateValue,
                    InputSelectedPK = id.InputSelectedPK,
                    ListNo = id.ListNo,
                    ListOrdinal = id.ListOrdinal,
                    UnitTypex = id.UnitType,
                    Threshold = id.Threshold,
                    TableChildren = id.TableChildren,
                    TableFirstColumn = id.TableFirstColumn
                });
            }
            foreach (var id in inputHeaders.Where(x => x.ListOrdinal != 0))
            {
                inputDetailList.Add(new vmInputDetails
                {
                    CoverPK = id.CoverPK,
                    RateOrdinal = id.RateOrdinal,
                    Rate = id.Rate,
                    RatePK = id.RatePK,
                    RateName = id.RateName,
                    ChildBW = id.ChildBW,
                    RateVis = id.RateVis,
                    RateValue = id.RateValue,
                    InputSelectedPK = id.InputSelectedPK,
                    ListNo = id.ListNo,
                    ListOrdinal = id.ListOrdinal,
                    UnitTypex = id.UnitType,
                    Threshold = id.Threshold,
                    TableChildren = id.TableChildren,
                    TableFirstColumn = id.TableFirstColumn
                });
            }

            var inputheaderPk = 0;

            foreach (var ih in inputHeaders.Where(x => x.ListNo < 1))
            {
                if (ih.RatePK != inputheaderPk || ih.ListOrdinal != 0)
                {
                    IList<vmInputDetails> inputDetailListAdd;
                    if (ih.ListOrdinal == 0)
                    {
                        inputDetailListAdd = inputDetail.Where(x => x.RatePK == ih.RatePK).ToList();
                    }
                    else
                    {
                        inputDetailListAdd = inputDetailList.Where(x => x.ListOrdinal == ih.ListNo).ToList();
                    }

                    if (ih.ListNo < 1)
                    {
                        inputs.Add(new vmInputHeader()
                        {

                            InputOrdinal = ih.InputOrdinal,
                            CoverPK = ih.CoverPK,
                            InputType = ih.InputTypeFK,
                            InputSelectedPK = ih.InputSelectedPK,
                            InputSelected = inputDetail.Where(x => x.RatePK == ih.InputSelectedPK).FirstOrDefault(),
                            InputString = ih.InputString,
                            PageNo = ih.PageNo,

                            InputTriggerFK = ih.InputTriggerFK,
                            InputVis = ih.InputVis,
                            ngRequired = "true",
                            InputDetails = inputDetailListAdd.Where(x => x.CoverPK == ih.CoverPK),
                            ListNo = ih.ListNo,
                            ListOrdinal = ih.ListOrdinal,
                            InputStatus = ih.RateStatus,
                            UnitType = ih.UnitType

                        });
                    }
                }
                inputheaderPk = ih.RatePK;
            }

            return inputs;
        }


        public IList<vmInputsFlat> _getQuoteInputs(int localeBw, int policyPk, int quotePk)
        {
            // the list of inputs that we are returning
            IList<vmInputsFlat> inputs = new List<vmInputsFlat>();

            // get the initial list of inputs that we would display for a new cover
            inputs = getFlatInputs(localeBw, policyPk, quotePk);

            // list of answers saved in the QuoteInputs table for this quote
            IList<QuoteInputs> quoteInputs = (from i in _dbContext.QuoteInputs
                                              where i.QuoteId == quotePk
                                              select i).ToList();

            // rates from rate table for each answer in quoteInputs table for this quote
            var rates = from qi in quoteInputs
                        join r in _dbContext.Rates on qi.RatePK equals r.RatePK
                        select r;


            // Lists to be added to ListFlatInput items
            IList<vmInputsFlat> detail = new List<vmInputsFlat>();
            vmInputsFlat _selected = new vmInputsFlat();

            // loop through rates
            foreach (Rates r in rates)
            {
                // get associated quoteInput line
                QuoteInputs q = quoteInputs.Where(x => x.RatePK == r.RatePK).FirstOrDefault();

                // only set ratevalue etc where this is NOT a ListFlatInput item 
                if (q.ListNo == 0)
                {
                    if (r.ListRatePK < 0 && q.ListNo == 0)
                    {
                        int? rateSelect = r.ListRatePK * -1;
                        var iSelect = inputs.Where(x => x.RatePK == rateSelect).FirstOrDefault();
                        if (iSelect != null)
                        {
                            inputs.Where(x => x.RatePK == rateSelect).FirstOrDefault().InputSelectedPK = r.RatePK;
                            inputs.Where(x => x.RatePK == rateSelect).FirstOrDefault().InputSelected = inputs.Where(x => x.RatePK == r.RatePK).FirstOrDefault();
                        }
                    }
                    else if (r.TableFirstColumn > 0)
                    {
                        var iTable = inputs.Where(x => x.RatePK == r.TableParentRatePK).FirstOrDefault();
                        if (iTable != null)
                        {
                            iTable.InputSelectedPK = r.RatePK;
                            iTable.InputSelected = inputs.Where(x => x.RatePK == r.RatePK).FirstOrDefault();
                            //Prevent self referencing loop
                            iTable.InputSelected.InputSelected = null;
                        }

                    }

                    // update input fields where input is not 0
                    var i = inputs.Where(x => x.RatePK == r.RatePK).FirstOrDefault();
                    if (i != null && q != null)
                    {
                        if (!string.IsNullOrEmpty(q.RateValue))
                        {
                            i.RateValue = q.RateValue;
                        }
                        if (q.NoItems != 0)
                        {
                            i.NoItems = q.NoItems;
                        }
                        if (q.SumInsured != 0)
                        {
                            i.SumInsured = q.SumInsured;
                        }
                    }
                }
            }


            // get quoteInputs that are ListFlatInput items
            foreach (QuoteInputs q in quoteInputs.Where(x => x.ListNo < 0))
            {
                Rates r = rates.Where(x => x.RatePK == q.RatePK).FirstOrDefault();
                int? listDetailNo = q.ListNo * -1;
                // get quoteInputs that are to be added to ListFlatInput.inputselected and ListFlatInput.inputDetails
                IList<QuoteInputs> det = quoteInputs.Where(x => x.ListOrdinal == q.ListNo && x.CoverPK == q.CoverPK).ToList();

                foreach (QuoteInputs d in det)
                {
                    detail.Add(new vmInputsFlat()
                    {
                        ChildBW = r.ChildBW,
                        CoverPK = d.CoverPK,
                        DivBy = r.DivBy,
                        Excess = r.Excess,
                        InputOrdinal = r.InputOrdinal,
                        InputSelectedPK = 0,
                        InputTypeFK = r.InputTypeFK,
                        ListNo = d.ListNo,
                        ListOrdinal = d.ListOrdinal,
                        ListRatePK = r.ListRatePK,
                        ParentBW = r.ParentBW,
                        ParentRatePK = r.ParentRatePK,
                        Rate = r.Rate,
                        RateLabel = r.RateLabel,
                        RateName = d.InputString,
                        RateOrdinal = r.RateOrdinal,
                        RatePK = d.RatePK ?? 0,
                        RateTip = r.RateTip,
                        RateValue = d.RateValue,
                        RateVis = 1,
                        Refer = d.Refer ?? 0,
                        TableChildren = r.TableChildren,
                        TableFirstColumn = r.TableFirstColumn,
                        Threshold = r.Threshold,
                        UnitType = r.UnitType,
                        DisplayBW = r.DisplayBW,
                        lblNumber = r.lblNumber,
                        lblSumInsured = r.lblSumInsured,
                        SumInsured = d.SumInsured,
                        NoItems = d.NoItems,
                        ReturnBW = r.ReturnBW,
                        PageNo = r.PageNo,
                        RateTypeFK = r.RateTypeFK,
                        InputSelected = null,
                        DiscountFirstRate = r.DiscountFirstRate,
                        DiscountSubsequentRate = r.DiscountSubsequentRate,
                        CoverCalcFK = r.CoverCalcFK ?? 0,
                        InputTrigger = r.InputTrigger ?? 0,
                        TradeFilter = r.TradeFilter ?? 0,
                        RateGroup = r.RateGroup ?? 0,
                        ngRequired = true,
                        SectionNo = 0,
                        TableParentRatePK = r.TableParentRatePK,
                        Indemnity = r.Indemnity
                    });
                }
                inputs.Add(new vmInputsFlat()
                {
                    ChildBW = r.ChildBW,
                    CoverPK = q.CoverPK,
                    DivBy = r.DivBy,
                    Excess = r.Excess,
                    InputOrdinal = r.InputOrdinal,
                    InputSelectedPK = 0,
                    InputTypeFK = r.InputTypeFK,
                    ListNo = q.ListNo,
                    ListOrdinal = q.ListOrdinal,
                    ListRatePK = r.ListRatePK,
                    ParentBW = r.ParentBW,
                    ParentRatePK = r.ParentRatePK,
                    Rate = r.Rate,
                    RateLabel = r.RateLabel,
                    RateName = q.InputString,
                    RateOrdinal = r.RateOrdinal,
                    RatePK = r.RatePK,
                    RateTip = r.RateTip,
                    RateValue = q.RateValue,
                    RateVis = 1,
                    Refer = q.Refer ?? 0,
                    TableChildren = r.TableChildren,
                    TableFirstColumn = r.TableFirstColumn,
                    Threshold = r.Threshold,
                    UnitType = r.UnitType,
                    DisplayBW = r.DisplayBW,
                    lblNumber = r.lblNumber,
                    lblSumInsured = r.lblSumInsured,
                    SumInsured = q.SumInsured,
                    NoItems = q.NoItems,
                    ReturnBW = r.ReturnBW,
                    PageNo = r.PageNo,
                    InputDetails = detail.Where(x => x.ListOrdinal == q.ListNo && x.CoverPK == q.CoverPK).ToList(),
                    RateTypeFK = r.RateTypeFK,
                    InputSelected = detail.Where(x => x.RatePK == r.RatePK && q.ListNo == x.ListNo).FirstOrDefault(),
                    DiscountFirstRate = r.DiscountFirstRate,
                    DiscountSubsequentRate = r.DiscountSubsequentRate,
                    CoverCalcFK = (int)r.CoverCalcFK,
                    InputTrigger = r.InputTrigger ?? 0,
                    TradeFilter = r.TradeFilter ?? 0,
                    RateGroup = r.RateGroup ?? 0,
                    ngRequired = true,
                    SectionNo = 0,
                    TableParentRatePK = r.TableParentRatePK,
                    Indemnity = r.Indemnity
                });


            }

            // ensure that children of selected items are visible
            var childVis = inputs.Where(x => x.InputSelected != null);

            foreach (vmInputsFlat parent in childVis)
            {
                parent.RateVis = 1;
                foreach (vmInputsFlat child in inputs.Where(x => x.ParentRatePK == parent.RatePK && x.RateVis == 0 && (parent.InputSelected.ParentBW & x.ChildBW) > 0))
                {

                    inputs.Where(x => x.RatePK == child.RatePK).FirstOrDefault().RateVis = 1;
                }
            }
            return inputs;
        }


        public IList<vmChargeSummary> GetCharges(List<vmInputs> inputsx)
        {
            var qti = new DataTable();
            qti.Columns.Add("CoverPK", typeof(int));
            qti.Columns.Add("ListNo", typeof(int));
            qti.Columns.Add("ListOrdinal", typeof(int));
            //qti.Columns.Add("InputName", typeof(string));
            //qti.Columns.Add("ParentPK", typeof(int));
            //qti.Columns.Add("InputTypeFK", typeof(int));
            qti.Columns.Add("InputPK", typeof(int));
            qti.Columns.Add("RatePK", typeof(int));
            qti.Columns.Add("RateValue", typeof(string));
            //qti.Columns.Add("RateString", typeof(string));
            qti.Columns.Add("InputString", typeof(string));
            //qti.Columns.Add("InputPKLock", typeof(int));

            foreach (var inputs in inputsx)
            {
                qti.Rows.Add(
                    inputs.CoverPK,
                    inputs.ListNo,
                    inputs.ListOrdinal,
                    //_inputs.InputName,
                    //_inputs.ParentPK,
                    //_inputs.InputTypeFK,
                    inputs.InputPK,
                    inputs.RatePK,
                    inputs.RateValue,
                    //_inputs.RateString,
                    inputs.InputString
                    //_inputs.InputPKLock
                    );
            }
            var inputP = new SqlParameter("@Inputs", SqlDbType.Structured);
            inputP.TypeName = "dbo.vmInputs";
            inputP.Value = qti;

            //IList<vmChargeSummary> charges = new List<vmChargeSummary>();

            //var result = _dbContext.Database.SqlQuery<string>("Exec dbo.S_Calc @Inputs", _inputP).ToList();
            var charges = _dbContext.Database.SqlQuery<vmChargeSummary>("Exec dbo.S_Calc @Inputs", inputP)
                .Select(x => new vmChargeSummary
                {
                    Section = x.Section,
                    CoverPK = x.CoverPK,
                    InvLevel = x.InvLevel,
                    LineType = x.LineType,
                    Charge = x.Charge,
                    Rate = x.Rate
                }).ToList();
            return charges;
        }



        public int saveInstallment(vmInstalments instalmentVm)
        {
            var i = new Instalments()
            {
                AccountName = instalmentVm.AccountName,
                AccountNumber = instalmentVm.AccountNumber,
                BusinessName = instalmentVm.BusinessName,
                CustomerID = instalmentVm.CustomerID,
                Deposit = (decimal)instalmentVm.Deposit,
                DownLoaded = instalmentVm.DownLoaded,
                Salutation = instalmentVm.Salutation,
                FirstName = instalmentVm.FirstName,
                LastName = instalmentVm.LastName,
                QuoteRef = (int)instalmentVm.QuoteRef,
                SortCode = instalmentVm.SortCode,
                FlatRate = (decimal)instalmentVm.FlatRate,
                FullPremium = (decimal)instalmentVm.FullPremium,
                InstalmentMonth = instalmentVm.InstalmentMonth,
                NumberOfPayments = (int)instalmentVm.NumberOfPayments,
                InstallmentPK = instalmentVm.InstallmentPK,
                Timestamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time")
            };
            var inst = _dbContext.Instalments.Find(instalmentVm.InstallmentPK);
            if (instalmentVm.InstallmentPK == 0)
            {

                _dbContext.Instalments.Add(i);
            }
            else
            {
                _dbContext.Entry(inst).CurrentValues.SetValues(i);
            }
            _dbContext.SaveChanges();
            return i.InstallmentPK;
        }

        public vmGlobalIris getIris(int quotePk, decimal charge)
        {
            var sql = string.Format("Exec dbo.S_SubmitPayment {0}, {1}", quotePk, charge);

            var gi = _dbContext.Database.SqlQuery<vmGlobalIris>(sql);

            var globalIris = gi.FirstOrDefault();

            return globalIris;
        }

        public IEnumerable<vmCardType> cardTypes()
        {
            IList<vmCardType> vmListct = new List<vmCardType>();

            vmListct.Add(new vmCardType()
            {
                CardTypePK = 1,
                CardType = "Visa Debit",
                Charge = 1.00,
                Credit = 0
            });
            vmListct.Add(new vmCardType()
            {
                CardTypePK = 2,
                CardType = "Visa Credit",
                Charge = 3.00,
                Credit = 1
            });
            vmListct.Add(new vmCardType()
            {
                CardTypePK = 3,
                CardType = "MasterCard",
                Charge = 3.00,
                Credit = 1
            });
            vmListct.Add(new vmCardType()
            {
                CardTypePK = 4,
                CardType = "Solo",
                Charge = 1.00,
                Credit = 0
            });
            vmListct.Add(new vmCardType()
            {
                CardTypePK = 5,
                CardType = "UK Maestro",
                Charge = 1.00,
                Credit = 0
            });
            vmListct.Add(new vmCardType()
            {
                CardTypePK = 6,
                CardType = "Electron",
                Charge = 1.00,
                Credit = 0
            });
            vmListct.Add(new vmCardType()
            {
                CardTypePK = 7,
                CardType = "Maestro",
                Charge = 1.00,
                Credit = 0
            });
            return vmListct;
        }

        #region renewals
        public IEnumerable<vmPolList> getPolicies(int localeid = 0)
        {
            IList<vmPolList> pList = new List<vmPolList>();
            var pols = _dbContext.F_Policies(localeid, 0).ToList();

            foreach (var p in pols)
            {
                pList.Add(new vmPolList()
                {
                    Id = p.PolicyPK,
                    Name = p.PolicyName,
                    BitwiseId = 1

                });
            }
            return pList;
        }

        public IEnumerable<Covers> getCovers()
        {
            IList<Covers> lines = new List<Covers>();

            lines = _dbContext.Covers.ToList();
            return lines;
        }

        public int checkRateReffered(int ratePK)
        {
            int refer = 0;
            var referRate = _dbContext.Rates.Where(x => x.RatePK == ratePK).FirstOrDefault();
            if (referRate != null)
            {
                refer = referRate.Refer;
            }
            return refer;

        }

        public IList<vmCertLists> GetCertificateLists(int quotePK)
        {
            IList<vmCertLists> list = new List<vmCertLists>();

            //get quote and quoteinputs
            var q = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            var qi = _dbContext.QuoteInputs.Where(x => x.QuoteId == quotePK && x.RateTypeFK != 13).ToList();
            var covers = _dbContext.Covers.ToList();
            var territory = "United Kingdom";
            var cur = "£";
            if (q.LocaleId == 2)
            {
                territory = "Republic of Ireland";
                cur = "€";
            }

            // get listItems
            var listItems = qi.Where(x => (x.CoverPK == 2 && x.RatePK > 0 && x.ListNo < 1 && (x.RateTypeFK == 10)) || x.CoverPK > 2 || x.RateTypeFK == 29).OrderBy(x => x.CoverPK).ToList();


            foreach (var l in listItems)
            {
                //get the rate and the appropriate cover
                var rate = _dbContext.Rates.Where(x => x.RatePK == l.RatePK).FirstOrDefault();
                var cover = covers.Where(x => x.CoverPK == rate.CoverCalcFK).FirstOrDefault();
                var sumInsured = 0;
                int n = 1;
                string line = "";
                if (rate != null && cover != null && rate.InputTypeFK != 12 && rate.ListRatePK != -199) // exclude inputs such as 'add material damage' and 'childrens party'
                {
                    if (l.CoverPK == 2)
                    {
                        // only some policies have lists under PL or MD paased in via PL
                        if (q.PolicyFK == 1 || q.PolicyFK == 2 || q.PolicyFK == 3 || q.PolicyFK == 5 || q.PolicyFK == 7 || q.PolicyFK == 18)
                        {
                            if (l.NoItems != null && l.NoItems > n)
                            {
                                n = (int)l.NoItems;
                            }
                            line = n.ToString() + " x " + rate.RateName;

                            // get related inputs (such as 'detailed description, length etc)
                            var des = qi.Where(x => x.ListOrdinal == l.ListOrdinal && x.ListNo > 0).ToList();
                            if (des.Count() > 0)
                            {
                                line = line + " | ";
                                // specific inputs to create linetext in lists
                                foreach (var ii in des.Where(x => x.RatePK == 1769 || x.RatePK == 1770 || x.RatePK == 123 || x.RatePK == 124 || x.RatePK == 641))
                                {
                                    if (ii.RatePK == 1769 || ii.RatePK == 1770 || ii.RatePK == 123 || ii.RatePK == 124)
                                    {
                                        line = line + ii.RateValue + " x ";
                                    }
                                    if (ii.RatePK == 641)
                                    {
                                        line = line + ii.RateValue;
                                    }
                                }
                                if (line.EndsWith(" x ") == true)
                                {
                                    line = line.Substring(0, line.Length - 3);
                                }
                                if (line.EndsWith(" | ") == true)
                                {
                                    line = line.Substring(0, line.Length - 3);
                                }
                            }
                            //exclude tables (dealt with below) and turnover rate inputs
                            //insert PL listitems
                            if ((rate.lblSumInsured == null || rate.lblSumInsured.ToUpper() != "TURNOVER:") && (rate.TableParentRatePK == null || rate.TableParentRatePK == 0))
                            {
                                list.Add(new vmCertLists()
                                {
                                    CoverFK = cover.CoverPK,
                                    QuoteInputPK = l.QuoteInputPK,
                                    RowNo = l.ListNo * -1,
                                    Section = cover.Section,
                                    Specification = cover.Specification,
                                    SumInsured = sumInsured.ToString(),
                                    Territory = territory,
                                    LineType = line
                                });
                            }

                            // check if equip hirers has added md
                            var si = qi.Where(x => x.ListOrdinal == l.ListOrdinal && x.RatePK == 156).FirstOrDefault();
                            if (si != null)
                            {
                                sumInsured = (int)si.SumInsured;
                            }
                            //check if showmen has added md
                            if (si == null)
                            {
                                si = qi.Where(x => x.ListOrdinal == l.ListOrdinal && x.RatePK == 834).FirstOrDefault();
                                if (si != null)
                                {
                                    sumInsured = (int)si.SumInsured;
                                }
                            }
                            //check if DCFI has added md
                            if (si == null)
                            {
                                si = qi.Where(x => x.ListOrdinal == l.ListOrdinal && x.RatePK == 859).FirstOrDefault();
                                if (si != null)
                                {
                                    sumInsured = (int)si.SumInsured;
                                }
                            }
                            // check if md has been added via a table
                            if (si == null)
                            {
                                if (rate.TableParentRatePK == 1071 || rate.TableParentRatePK == 1182)
                                {
                                    sumInsured = (int)_dbContext.Rates.Where(x => x.TableParentRatePK == rate.TableParentRatePK && x.TableChildren == rate.TableFirstColumn && x.CoverCalcFK == 8).FirstOrDefault().Threshold;
                                }
                            }

                            // add md inputs
                            string s = cur + sumInsured.ToString("#,##0");
                            foreach (var ii in des.Where(x => x.SumInsured > 0 && (x.RatePK == 156 || x.RatePK == 834 || x.RatePK == 859)))
                            {
                                cover = covers.Where(x => x.CoverPK == 8).FirstOrDefault();
                                list.Add(new vmCertLists
                                {
                                    CoverFK = 8,
                                    QuoteInputPK = l.QuoteInputPK,
                                    RowNo = l.ListNo * -1,
                                    Section = cover.Section,
                                    Specification = cover.Specification,
                                    SumInsured = s,
                                    Territory = territory,
                                    LineType = line
                                });

                            }
                            //add md inputs from tables
                            if (rate.TableParentRatePK == 1071 || rate.TableParentRatePK == 1182)
                            {
                                cover = covers.Where(x => x.CoverPK == 8).FirstOrDefault();
                                list.Add(new vmCertLists
                                {
                                    CoverFK = 8,
                                    QuoteInputPK = l.QuoteInputPK,
                                    RowNo = l.ListNo * -1,
                                    Section = cover.Section,
                                    Specification = cover.Specification,
                                    SumInsured = s,
                                    Territory = territory,
                                    LineType = cover.SectionName
                                });
                            }

                        }
                    }
                    // EL - RateTypeFK 30 = ERN number
                    else if (l.CoverPK == 3 && l.RateTypeFK != 30)
                    {
                        n = 1;
                        sumInsured = l.SumInsured ?? 0;
                        string s = cur + sumInsured.ToString("#,##0");
                        line = rate.RateName;
                        if (l.SumInsured > 0)
                        {
                            list.Add(new vmCertLists()
                            {
                                CoverFK = cover.CoverPK,
                                QuoteInputPK = l.QuoteInputPK,
                                RowNo = l.ListNo * -1,
                                Section = cover.Section,
                                Specification = cover.Specification,
                                SumInsured = s,
                                Territory = territory,
                                LineType = line
                            });
                        }
                        else if(rate.TableParentRatePK == 5288)
                        {
                            list.Add(new vmCertLists()
                            {
                                CoverFK = cover.CoverPK,
                                QuoteInputPK = l.QuoteInputPK,
                                RowNo = l.ListNo * -1,
                                Section = cover.Section,
                                Specification = cover.Specification,
                                SumInsured = "0",
                                Territory = territory,
                                LineType = line
                            });
                        }
                    }
                    else if (cover.Section == 4)
                    {
                        var ter = territory;
                        n = 1;
                        // set suminsured to input.suminsured
                        sumInsured = l.SumInsured ?? 0;
                        // if suminsured still zero then check if the input ratevalue is numeric
                        if (sumInsured == 0)
                        {
                            if (Regex.IsMatch(l.RateValue, @"^\d+$") == true)
                            {
                                sumInsured = int.Parse(l.RateValue);
                            }
                        }
                        // Territory List
                        if (rate.ListRatePK == -916)
                        {
                            ter = l.RateValue;
                        }
                        // suminsured passed in via table
                        if (rate.TableParentRatePK == 1380 || rate.TableParentRatePK == 1167 || rate.TableParentRatePK == 1204)
                        {
                            sumInsured = (int)rate.Threshold;
                        }

                        string s = cur + sumInsured.ToString("#,##0");
                        // Set line description to cover.sectionName or ratename 
                        if (rate.ListRatePK == 0 || Regex.IsMatch(l.RateValue, @"\d") == true || rate.ListRatePK == -916 || l.RatePK == 949)
                        {
                            line = cover.SectionName;
                        }
                        else
                        {
                            line = rate.RateName;
                        }
                        if (cover.CoverPK == 17)
                        {
                            line = rate.RateName;
                        }
                        // rate 949 is personal assault - should only be added if it has a ratevalue
                        if ((sumInsured > 0 && l.RatePK != 949) || (l.RatePK == 949 && !string.IsNullOrEmpty(l.RateValue)))
                        {
                            if (l.RatePK == 949)
                            {
                                var sum = _dbContext.QuoteLines.Where(x => x.QuoteId == quotePK && x.CoverFK == 17).FirstOrDefault();
                                if (sum != null)
                                {
                                    sumInsured = (int)sum.SumInsured;
                                    s = cur + sumInsured.ToString("#,##0");
                                }
                            }
                            list.Add(new vmCertLists()
                            {
                                CoverFK = cover.CoverPK,
                                QuoteInputPK = l.QuoteInputPK,
                                RowNo = l.ListNo * -1,
                                Section = cover.Section,
                                Specification = cover.Specification,
                                SumInsured = s,
                                Territory = ter,
                                LineType = line
                            });
                        }
                    }
                    else if (cover.Section == 5)
                    {
                        n = 1;
                        sumInsured = l.SumInsured ?? 0;
                        if (sumInsured == 0)
                        {
                            if (Regex.IsMatch(l.RateValue, @"^\d+$") == true)
                            {
                                sumInsured = int.Parse(l.RateValue);
                            }
                        }
                        string s = cur + sumInsured.ToString("#,##0");
                        if (rate.ListRatePK == 0 || Regex.IsMatch(l.RateValue, @"\d") == true)
                        {
                            line = cover.SectionName;
                        }

                        // indemnity period for business interruption is selected for all but one of the covers
                        var indemnperiod = l.RateValue;
                        if (l.CoverPK == 26)
                        {
                            indemnperiod = "12 Months";
                        }
                        if (sumInsured > 0)
                        {
                            list.Add(new vmCertLists()
                            {
                                CoverFK = cover.CoverPK,
                                QuoteInputPK = l.QuoteInputPK,
                                RowNo = l.ListNo * -1,
                                Section = cover.Section,
                                Specification = cover.Specification,
                                SumInsured = s,
                                Territory = indemnperiod,
                                LineType = line
                            });
                        }
                    }
                }
            }
            return list;
        }


        public IEnumerable<vmCertLists> GetCertList(int quotePK)
        {
            var sectionLists = _dbContext.S_CertLists(quotePK).Select(x => new vmCertLists
            {
                QuoteInputPK = x.quoteInputPK,
                CoverFK = x.CoverFK ?? 0,
                LineType = x.LineType,
                RowNo = x.RowNo,
                Section = x.Section,
                Specification = x.Specification,
                SumInsured = x.SumInsured,
                Territory = x.Territory,
            }).ToList();

            return sectionLists;

        }
        public string setBusinessDescription(int quotePK, string businessDescription = "")
        {
            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();

            try
            {
                var policy = _dbContext.Policies.Where(x => x.PolicyPK == quote.PolicyFK).FirstOrDefault();
                int n;
                bool isNumeric = false;
                var polDescription = "";
                string BusDescription = "";
                if (!string.IsNullOrEmpty(businessDescription))
                {
                    BusDescription = businessDescription;
                }

                else
                {
                    if (policy != null)
                    {
                        polDescription = policy.BusinessDescription;
                        isNumeric = int.TryParse(polDescription, out n);


                        if (isNumeric == false)
                        {
                            BusDescription = policy.BusinessDescription;
                        }

                        else
                        {
                            BusDescription = getBusinessDescription(quotePK, int.Parse(polDescription));

                        }
                    }
                }
                if (BusDescription.Length > 300)
                {
                    BusDescription = BusDescription.Substring(0, 300);
                }

                quote.BusinessDescription = BusDescription;
                _dbContext.SaveChanges();
                return quote.BusinessDescription;
            }
            catch (Exception ex)
            {
                var errorid = AppInsightLog.LogError(ex, "[LeisureInsure.DB.Repository.setBusinessDescription]", quotePK.ToString());
                throw new ApplicationException($"{errorid}");
            }

        }

        //public IList<vmDocumentsForQuote> getPolicyDocuments(int quotePK)
        //{

        //    List<vmDocumentsForQuote> docs = new List<vmDocumentsForQuote>();
        //    var dbdocs = _dbContext.F_GetDocumentsForQuote(quotePK).ToList();
        //    foreach (var d in dbdocs)
        //    {
        //        docs.Add(new vmDocumentsForQuote()
        //        {
        //            Ord = d.Ord,
        //            DocTypeFK = d.DocTypeFK,
        //            DocumentFK = d.DocumentPK,
        //            DocLink = d.DocLink,
        //            DocName = d.DocName,
        //            DocType = d.DocType

        //        });
        //    }


        //    return docs.OrderBy(x => x.Ord).ToList();

        //}
        #endregion 


        #region cert

        public void addCertificate(CertificatesModel document)
        {
            var certificatePk = (from i in _dbContext.Certs
                                 where
                                    i.QuoteRef == document.QuoteRef
                                 select i.CertificatePK).FirstOrDefault();

            var certificateInfo = new Certs()
            {
                Certificate = document.Certificate,
                QuoteRef = document.QuoteRef,
                FileName = document.FileName,
                Lock = document.Lock,
                Url = "",
                CertificatePK = certificatePk,
                DateStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time")
            };
            var c = _dbContext.Certs.Find(certificatePk);
            if (certificatePk == 0)
            {

                _dbContext.Certs.Add(certificateInfo);
            }
            else
            {
                _dbContext.Entry(c).CurrentValues.SetValues(certificateInfo);
            }
            _dbContext.SaveChanges();
        }

        public CertificatesModel Getcert(int quotepk)
        {
            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quotepk).FirstOrDefault();
            var certificate = new CertificatesModel();
            var cert = (from i in _dbContext.Certs
                        where
                           i.QuoteRef == quote.QuoteRef
                        select i).FirstOrDefault();

            if (cert != null)
            {
                certificate = new CertificatesModel()

                {

                    CertificatePK = cert.CertificatePK,
                    Url = cert.Url,
                    Certificate = cert.Certificate,
                    QuoteRef = cert.QuoteRef
                };
            }

            return certificate;
        }

        public OldCertificatesModel GetOldcert(int quotepk)
        {
            var certificate = new OldCertificatesModel();
            var cert = (from i in _dbContext.OldWebCerts
                        where
                           i.QuoteRef == quotepk
                        select i).FirstOrDefault();

            if (cert != null)
            {
                certificate = new OldCertificatesModel()

                {
                    Data = cert.Data,
                    ID = cert.ID,
                    QuoteRef = cert.QuoteRef ?? 0
                };
            }

            return certificate;
        }

        public string getBusinessDescription(int quotePK, int listRatePK)
        {
            var q = _dbContext.Quotes.Where(x => x.QuotePK == quotePK).FirstOrDefault();
            var activityList = _dbContext.F_BusinessDescriptionList(quotePK, listRatePK).ToList();
            int activities = 0;
            var seperator = "";
            string businessDescription = "";
            foreach (var a in activityList)
            {
                activities += 1;
                if (activities > 1)
                {
                    seperator = ", ";
                }
                if (activities == activityList.Count() && activityList.Count() > 1)
                {
                    seperator = " and ";
                }
                businessDescription += seperator + a.RateName;

            }
            if (q.PolicyFK == 6)
            {

                if (businessDescription == "Event Supplier - One Off Event (maximum of 5 consecutive days)")
                {
                    businessDescription = "Event Supplier - One Off Event";
                    var days = _dbContext.QuoteInputs.Where(x => x.QuoteId == quotePK && x.RateTypeFK == 5).FirstOrDefault();
                    var eventType = _dbContext.QuoteInputs.Where(x => x.QuoteId == quotePK && x.RateTypeFK == 29).FirstOrDefault();

                    if(days != null)
                    {
                        businessDescription += " (" +days.NoItems +" Day/s)";
                    }

                    if (eventType != null)
                    {
                        businessDescription += " - " + eventType.RateValue;
                    }

                }
                else if (businessDescription == "Event Organiser - One Event (maximum of 5 consecutive days)")
                {
                    businessDescription = "Event Organiser - One Event";
                    var attendees = _dbContext.QuoteInputs.Where(x => x.QuoteId == quotePK && x.RateTypeFK == 15).FirstOrDefault();
                    var eventType = _dbContext.QuoteInputs.Where(x => x.QuoteId == quotePK && x.RateTypeFK == 29).FirstOrDefault();


                    if (attendees != null && attendees.RateValue != "0")
                    {
                        businessDescription += " (" + attendees.RateValue + " attendees) ";
                    }

                    if (eventType != null)
                    {
                        businessDescription += "- " + eventType.RateValue;
                    }
                }
            }


            if (q.PolicyFK == 7)
            {
                List<QuoteInputs> covertypes = _dbContext.QuoteInputs.Where(x => x.QuoteId == quotePK && (x.RateTypeFK == 14 || x.RateTypeFK == 46 || x.RateTypeFK == 15)).ToList();
                foreach (var i in covertypes)
                {
                    var r = _dbContext.Rates.Where(x => x.RatePK == i.RatePK).FirstOrDefault();
                    if (r.RateTypeFK == 15 || r.RateTypeFK == 46)
                    {
                        businessDescription = businessDescription + " (" + r.RateName + ")";
                    }
                }
            }


            return businessDescription;

        }        

        public vmCertSchedule _getCertScheduleByQuotePK(int quotePk)
        {
            var c = _dbContext.S_CreateSchedule(quotePk).FirstOrDefault();
            var additionalCovered = _dbContext.F_AdditionalCovered(quotePk).ToList();

            //formatting issue with the stored proc so lets get the original values instead :D
            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quotePk).FirstOrDefault();
            string businessDescription = quote.BusinessDescription;

            IList<additionalCovered> ac = new List<additionalCovered>();

            if (c.PolicyPK == 15 || c.PolicyPK == 16)
            {
                if (!string.IsNullOrEmpty(businessDescription))
                {
                    if(c.PolicyPK == 15)
                    {

                        businessDescription = "Freelance Activity Provider(s): " + businessDescription;
                    }
                    if(c.PolicyPK == 16)
                    {
                        businessDescription = "Freelance Sports instructor(s): " + businessDescription;
                    }
                }
            }

            foreach (var i in additionalCovered)
            {
                ac.Add(new additionalCovered()
                {
                    ContactPK = i.ContactPK,
                    FirstName = i.FirstName,
                    LastName = i.LastName

                });

            }

            //getting dates and times from quote, not the stored proc
            var cs = new vmCertSchedule()
            {
                QuoteReference = c.QuoteReference,
                Business = businessDescription,
                CorrespondenceAddress = c.CorrespondenceAddress,
                EndDate = quote.DateTo,
                StartDate = quote.DateFrom,
                TimeFrom = quote.TimeFrom,
                TimeTo = quote.TimeTo,
                GrossPremium = c.GrossPremium,
                InsuredName = c.InsuredName,
                NetPremium = c.NetPremium,
                GrossLegal = c.GrossLegal,
                PolicyNumber = c.PolicyNumber,
                PolicyWording = c.PolicyWording,
                Premises = c.Premises,
                Tax = c.Tax,
                TerritorialLimits = c.Territory,
                TaxType = c.TaxType,
                Fee = c.Fee,
                Purchased = c.Purchased,
                Contingency = c.Contingency,
                PolicyType = c.PolicyType,
                PurchasedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time"),
                EventName = c.EventName,
                Attendance = c.Attendance,
                additionalInsured = ac,
                PolicyPK = c.PolicyPK,
                Venue = c.Venue,
                LocalePK = quote.LocaleId
            };

            //clean up address
            string caddress = cs.CorrespondenceAddress;
            while (caddress.Contains("  ") || caddress.Contains(",,") || caddress.Contains(", ,"))
            {
                caddress = Regex.Replace(caddress, @"  ", " ");
                caddress = Regex.Replace(caddress, @",,", ",");
                caddress = Regex.Replace(caddress, @", ,", ",");
            }

            cs.CorrespondenceAddress = caddress;

            return cs;
        }

        /// <summary>
        /// This is tested for foodborne illness 
        /// </summary>
        /// <param name="quoteid"></param>
        /// <returns></returns>
        public List<CertSection> GetCertSections(List<CertHeader> headers, int quoteid)
        {
            var certsections = new List<CertSection>();

            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quoteid).FirstOrDefault();
            var policy = _dbContext.Policies.Where(x => x.PolicyPK == quote.PolicyFK).FirstOrDefault();
            var covers = _dbContext.Covers.ToList();
            var territory = LocaleHelper.GetLocale(quote.LocaleId);
            var sectionHeaders = _dbContext.SectionHeaderLines.ToList();

            var quoteInputs = _dbContext.QuoteInputs.Where(x => x.QuoteId == quoteid).ToList();
            var quoteLines = _dbContext.QuoteLines.Where(x => x.QuoteId == quoteid).ToList();
            var numutil = new NumberAbbreviator();

            foreach (var header in headers)
            {
                //get all sections for each header
                var sections = sectionHeaders.Where(x => x.SectionNumber == header.Section).ToList();
                //get cover for this header based on coverPK
                var line = quoteLines.Where(x => x.CoverFK == header.Insured).FirstOrDefault();

                foreach (var section in sections)
                {
                    string spanUK1 = section.SpanUK1;

                    //excess
                    if (section.RateTypeFK == 7)
                    {
                        var excess = quoteLines.Where(x => x.Excess > 0).FirstOrDefault();
                        var dec = (decimal)excess.Excess;
                        var excessRounded = Math.Round(dec, 0);
                        var format = String.Format("{0:n0}", excessRounded);
                        spanUK1 = spanUK1.Replace("{value}", format);
                    }
                    else
                    {
                        //if this is a policy aggregate section show main limit of cover
                        if (section.SectionNumber == 22)
                        {
                            line = quoteLines.Where(x => x.CoverFK == 42).FirstOrDefault();
                        }
                        //number of locations
                        if (section.SectionNumber == 23)
                        {
                            var location = quoteInputs.Where(x => x.RatePK == 4235).FirstOrDefault();
                            spanUK1 = spanUK1.Replace("{value}", location.RateValue);
                        }
                        //indemnity period
                        if (section.SectionNumber == 24)
                        {
                            var indemperiod = quoteInputs.Where(x => x.RateTypeFK == 40).FirstOrDefault();
                            spanUK1 = spanUK1.Replace("{value}", indemperiod.RateValue);
                        }

                        //we have a coverPK to get indemnity  
                        if (line != null)
                        {
                            var dec = (decimal)line.Indemnity;
                            var indem = Math.Round(dec, 0);
                            var format = String.Format("{0:n0}", indem);
                            if (!String.IsNullOrEmpty(spanUK1))
                                spanUK1 = spanUK1.Replace("{value}", format);
                            section.Label1 = section.Label1.Replace("{value}", format);
                        }
                    }

                    var certsection = new CertSection();

                    certsection.Header = header.Header;
                    certsection.HeaderLevel = header.HeaderLevel;
                    certsection.Section = section.SectionNumber;
                    certsection.Label1 = section.Label1;
                    certsection.LineValue1 = spanUK1 ?? "";
                    certsection.LineGroup = section.LineGroup;
                    certsection.ordinal = section.Ordinal;
                    certsection.Para = section.ParaAfter;
                    certsection.LineValue2 = section?.SpanUK2 ?? "";

                    certsections.Add(certsection);
                }
            }

            return certsections;
        }

        /// <summary>
        /// This is tested for foodborne illness and has been tested for other policies but needs more testing
        /// </summary>
        /// <param name="quoteid"></param>
        /// <returns></returns>
        public List<CertHeader> GetCertHeaders(int quoteid)
        {
            var certheaders = new List<CertHeader>();

            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quoteid).FirstOrDefault();
            var policy = _dbContext.Policies.Where(x => x.PolicyPK == quote.PolicyFK).FirstOrDefault();
            var covers = _dbContext.Covers.ToList();
            var territory = LocaleHelper.GetLocale(quote.LocaleId);

            //get headers 
            var headers = _dbContext.SectionHeaders.Where(x => x.PolicyId == policy.PolicyPK).ToList();

            //get selected covers
            var quoteLines = _dbContext.QuoteLines.Where(x => x.QuoteId == quoteid).ToList();
            var quoteInputs = _dbContext.QuoteInputs.Where(x => x.QuoteId == quoteid).ToList();
            //get covers for this policy
            //var rates = _dbContext.Rates.Where(x => (x.PolicyBW & policy.BitWise) > 0 && x.CoverCalcFK > 0).ToList();
            //List<IGrouping<int?, Rates>> coverpolicyIDs = rates.GroupBy(x => x.CoverCalcFK).ToList();


            //need to show these because of generic policy wording, need to be added to rates table
            //equipment hirer
            if (policy.PolicyPK == 1)
            {
                var certheader = new CertHeader();
                certheader.Header = "Trustees Liability: Not Insured";
                certheader.Insured = 0; certheader.Section = 7; certheader.HeaderLevel = 1;

                var certheader2 = new CertHeader();
                certheader2.Header = "Trustees Personal Accident: Not Insured";
                certheader2.Insured = 0; certheader2.Section = 9; certheader2.HeaderLevel = 1;

                var certheader3 = new CertHeader();
                certheader3.Header = "Business Interruption: Not Insured";
                certheader3.Insured = 0; certheader3.Section = 5; certheader3.HeaderLevel = 1;

                certheaders.Add(certheader); certheaders.Add(certheader2); certheaders.Add(certheader3);
            }

            //go through all potential covers and show what we have added
            foreach (var header in headers)
            {

                string coverdesc = "";
                var certheader = new CertHeader();
                var cover = covers.Where(x => x.CoverPK == header.CoverId).FirstOrDefault();
                coverdesc = header.Description;

                if (cover != null)
                {
                    var addedcover = quoteLines.Where(x => x.CoverFK == cover.CoverPK).FirstOrDefault();

                    if (addedcover != null)
                    {
                        certheader.Header = $"{coverdesc}: Insured";
                        certheader.Insured = cover.CoverPK;
                    }
                    else
                    {
                        certheader.Header = $"{coverdesc}: Not Insured";
                        certheader.Insured = 0;
                    }
                }
                else
                {
                    //if this is not a cover then show default description and mark as insured
                    //this will be a rate or answer to some question
                    certheader.Header = coverdesc;
                    certheader.Insured = 1;
                }

                certheader.Ordinal = header.Ordinal;
                certheader.HeaderLevel = 1;
                certheader.Section = header.SectionNumber;

                //if (coverdesc == "Cancellation")
                //    coverdesc = "Contingency";                               
                certheaders.Add(certheader);
            }

            //GetBundledCoversAndRates(policy, quote.LocaleId, territory, quoteInputs);

            //include bundled covers
            //foreach (var cover in this.policyCovers)
            //{

            //}

            certheaders = certheaders.OrderBy(x => x.Ordinal).ToList();

            return certheaders;
        }        

        public IEnumerable<vmSectionHeaders> _getCertSectionsNEW(int quotePk)
        {

            var quote = _dbContext.Quotes.Where(x => x.QuotePK == quotePk).FirstOrDefault();

            IList<vmSectionHeaders> sectionHeaders = new List<vmSectionHeaders>();
            dynamic header, certificateSections;

            if (quote.PolicyFK == 27)
                header = GetCertHeaders(quotePk);
            else
                header = _dbContext.S_CertHeaders(quotePk).ToList();

            /*
            var sectionLists = _dbContext.S_CertLists(quotePk).Select(x => new vmCertLists
            {
                CoverFK = x.CoverFK ?? 0,
                LineType = x.LineType,
                RowNo = x.RowNo,
                Section = x.Section,
                Specification = x.Specification,
                SumInsured = x.SumInsured,
                Territory = x.Territory,
            }).ToList();
            */
            var sectionLists = GetCertificateLists(quotePk);


            if (quote.PolicyFK == 27)
                certificateSections = GetCertSections(header, quote.QuotePK);
            else
                certificateSections = _dbContext.S_GetCertSections(quotePk).ToList();

            var sections = new List<vmSections>();

            foreach (var s in certificateSections)
            {
                //professional cover for irish should be 1.3M
                if (s.Section == 8 && s.LineGroup == 2 && quote.LocaleId == 2)
                {
                    s.LineValue1 = "€1,300,000 any one <b>Occurrence</b> and in the aggregate";
                }
                if (quote.PolicyFK != 10 && quote.PolicyFK != 11)
                {
                    sections.Add(new vmSections
                    {
                        HeaderLevel = s.HeaderLevel,
                        Section = s.Section,
                        Insured = s.Insured,
                        Label1 = s.Label1,
                        LineValue1 = s.LineValue1,
                        LineValue2 = s.LineValue2,
                        LineGroup = s.LineGroup,
                        ordinal = s.ordinal,
                        Para = s.Para
                    });
                }
                else
                {
                    if (s.Section != 2 && s.Section != 3)
                    {
                        sections.Add(new vmSections
                        {
                            HeaderLevel = s.HeaderLevel,
                            Section = s.Section,
                            Insured = s.Insured,
                            Label1 = s.Label1,
                            LineValue1 = s.LineValue1,
                            LineValue2 = s.LineValue2,
                            LineGroup = s.LineGroup,
                            ordinal = s.ordinal,
                            Para = s.Para
                        });
                    }
                    else
                    {
                        if (s.Label1 == "Limit of Liability:")
                        {
                            sections.Add(new vmSections
                            {
                                HeaderLevel = s.HeaderLevel,
                                Section = s.Section,
                                Insured = s.Insured,
                                Label1 = s.Label1,
                                LineValue1 = s.LineValue1,
                                LineValue2 = s.LineValue2,
                                LineGroup = s.LineGroup,
                                ordinal = s.ordinal,
                                Para = s.Para
                            });
                        }
                    }
                }
            }

            foreach (var h in header)
            {

                sectionHeaders.Add(new vmSectionHeaders()
                {
                    Header = h.Header,
                    HeaderLevel = h.HeaderLevel,
                    Insured = h.Insured,
                    Section = h.Section,
                    SectionDetail = sections.Where(x => x.Section == h.Section && h.Insured > 0),
                    SectionLists = sectionLists.Where(x => x.Section == h.Section && h.Insured > 0)
                });
            }

            return sectionHeaders;
        }

        public IEnumerable<vmSectionHeaders> _getCertSections(int quotePk)
        {

            IList<vmSectionHeaders> sectionHeaders = new List<vmSectionHeaders>();

            var sectionLists = _dbContext.S_CertLists(quotePk).Select(x => new vmCertLists
            {
                CoverFK = x.CoverFK ?? 0,
                LineType = x.LineType,
                RowNo = x.RowNo,
                Section = x.Section,
                Specification = x.Specification,
                SumInsured = x.SumInsured,
                Territory = x.Territory,
            }).ToList().OrderBy(x => x.RowNo);

            var certificateSections = _dbContext.S_GetCertSections(quotePk).ToList();

            var sections = certificateSections.Select(s => new vmSections
            {
                HeaderLevel = s.HeaderLevel,
                Section = s.Section,
                Insured = s.Insured,
                Label1 = s.Label1,
                LineValue1 = s.LineValue1,
                LineValue2 = s.LineValue2,
                LineGroup = s.LineGroup,
                ordinal = s.ordinal,
                Para = s.Para
            });
            var sect = 0;
            foreach (var s in certificateSections.OrderBy(x => x.Section))
            {
                if (s.Section != sect)
                {
                    sectionHeaders.Add(new vmSectionHeaders()
                    {
                        Header = s.Header,
                        HeaderLevel = s.HeaderLevel,
                        Insured = s.Insured,
                        Section = s.Section,
                        SectionDetail = sections.Where(y => y.Section == s.Section),
                        //SectionLists = _sectionListsAdd
                        SectionLists = sectionLists.Where(z => z.Section == s.Section)

                    });
                }
                sect = s.Section;
            }


            //var sectionHeaders = certificateSections.Select(s => new vmSectionHeaders()
            //{
            //    Header = s.Header,
            //    HeaderLevel = s.HeaderLevel,
            //    Insured = s.Insured,
            //    Section = s.Section,
            //    SectionDetail = sections.Where(y => y.Section == s.Section),
            //    //SectionLists = _sectionListsAdd
            //    SectionLists = sectionLists.Where(z => z.Section == s.Section)
            //}).ToList();

            return sectionHeaders;
        }

        public vmCertHazardList getCertHazards(int quotePK)
        {
            IList<vmCertHazards> hazards = new List<vmCertHazards>();

            var hh = _dbContext.F_CertHazards(quotePK);

            foreach (var h in hh)
            {
                hazards.Add(new vmCertHazards()
                {
                    Question = h.Question,
                    Answer = h.Answer

                });
            }
            vmCertHazardList hazardList = new vmCertHazardList() { Hazards = hazards };

            return hazardList;
        }

        #endregion

        #region contact
        //public int AddContact(IEnumerable<vmContact> contacts)
        //{
        //    //Contact table param
        //    var c = new DataTable();
        //    c.Columns.Add("QuoteFK", typeof(int));
        //    c.Columns.Add("FirstName", typeof(string));
        //    c.Columns.Add("LastName", typeof(string));
        //    c.Columns.Add("Telephone", typeof(string));
        //    c.Columns.Add("Email", typeof(string));
        //    c.Columns.Add("ContactType", typeof(int));
        //    c.Columns.Add("EntityType", typeof(string));
        //    c.Columns.Add("EntityName", typeof(string));
        //    foreach (var contact in contacts)
        //    {
        //        c.Rows.Add(
        //                    contact.QuotePK,
        //                    contact.FirstName,
        //                    contact.LastName,
        //                    contact.Telephone,
        //                    contact.Email,
        //                    contact.ContactType,
        //                    contact.EntityType,
        //                    contact.EntityName
        //                    );
        //    }
        //    var parameter = new SqlParameter("@Contact", SqlDbType.Structured);
        //    parameter.Value = c;
        //    parameter.TypeName = "dbo.Contacts";

        //    var customerPk = _dbContext.Database.SqlQuery<int>("Exec dbo.S_AddContact @Contact",
        //                                            parameter
        //                                         );

        //    var cpk = customerPk.FirstOrDefault();

        //    return cpk;
        //}

        //public void AddAddress(IEnumerable<vmAddress> addresses)
        //{
        //    var a = new DataTable();
        //    a.Columns.Add("QuotePK", typeof(int));
        //    a.Columns.Add("Address1", typeof(string));
        //    a.Columns.Add("Address2", typeof(string));
        //    a.Columns.Add("Address3", typeof(string));
        //    a.Columns.Add("Town", typeof(string));
        //    a.Columns.Add("County", typeof(string));
        //    a.Columns.Add("Postcode", typeof(string));
        //    a.Columns.Add("Locale", typeof(string));

        //    foreach (var address in addresses)
        //    {
        //        a.Rows.Add(
        //                    //_contact.QuotePK,
        //                    address.QuotePK,
        //                    address.Address1,
        //                    address.Address2,
        //                    address.Address3,
        //                    address.Town,
        //                    address.County,
        //                    address.Postcode,
        //                    address.Locale
        //                    );
        //    }
        //    var paramAddress = new SqlParameter("@Address", SqlDbType.Structured);
        //    paramAddress.Value = a;
        //    paramAddress.TypeName = "dbo.Address";

        //    _dbContext.Database.ExecuteSqlCommand("Exec dbo.S_AddAddress @Address",
        //                                            paramAddress
        //                                         );
        //}
        #endregion

        #region TAM


        #endregion

        #region CODES
        public Codes GetCode(string codename)
        {
            var code = _dbContext.Codes.Where(x => x.CodeName == codename).FirstOrDefault();

            return code;
        }

        #endregion

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public void deleteQuoteChildren(string quoteRef)
        {
            _dbContext.Database.ExecuteSqlCommand("S_DeleteQuoteChildren {0}", quoteRef);

        }



        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _dbContext.Dispose();
            }

            // Free any unmanaged objects here.
            //
            _disposed = true;
        }
    }
}