﻿using System;

namespace LeisureInsure.DB.Models
{
    public class TamTotals
    {
        public string FieldPrefix { get; set; }
        public string insured { get; set; }
        public string indemnity { get; set; }
        public string excess { get; set; }
        public string net { get; set; }
        public string fee { get; set; }
        public string gross { get; set; }
        public string tax { get; set; }
    }
}