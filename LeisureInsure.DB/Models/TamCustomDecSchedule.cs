﻿namespace LeisureInsure.DB.Models
{
    public class TamCustomDecSchedule
    {
        public int Key { get; set; }
        public int Section { get; set; }
        public string Description { get; set; }
        public string Insured { get; set; }
        public string Rate { get; set; }
        public string Premium { get; set; }
        public string Excess { get; set; }
        public string Spec { get; set; }

    }
}
