﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeisureInsure.DB.Models
{
    public class TamCustomDecPageOne
    {
        public string TradingAs { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        public string EventName { get; set; }
        public string Territory { get; set; }
        public string BIHA { get; set; }
        public string ErnNumber { get; set; }
        public string Showman { get; set; }

        public string NoVisitors { get; set; }
        public string NoEvents { get; set; }

    }
}
