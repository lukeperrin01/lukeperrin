﻿using System;

namespace LeisureInsure.DB.Models
{
    public class TamCustomDec
    {
        public int? PolicyPK { get; set; }
        public int? CustomerFK { get; set; }
        public string CustomerRef { get; set; }
        public string POL_IDX { get; set; }
        public string TYPE { get; set; }
        public string QuoteRef { get; set; }
        public string Description { get; set; }
        public string Business1 { get; set; }
        public string Business2 { get; set; }
        public string TradingAs { get; set; }
        public string RiskAddress1 { get; set; }
        public string RiskAddress2 { get; set; }
        public string PostCode1 { get; set; }
        public string PostCode2 { get; set; }
        public string EventName { get; set; }
        public string NoVisitors { get; set; }
        public string NoEvents { get; set; }
        public string Employees { get; set; }
        public string Volunteers { get; set; }
        public string Territory { get; set; }
        public string BihaNumber { get; set; }
        public string ERN { get; set; }
        public string Showman { get; set; }
        public string NightClub { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EventStartTime { get; set; }
        public string EventEndTime { get; set; }
        public decimal? LiFee { get; set; }
        public decimal? TotalPremium { get; set; }
        public decimal? BrokerFee { get; set; }
    }
}