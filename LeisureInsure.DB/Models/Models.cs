﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace LeisureInsure.DB
{

    public class ContactsDBContext : System.Data.Entity.DbContext
    {
        public DbSet<ContactsModel> Covers { get; set; }
    }
    public class ContactsModel
    {
        [Key]
        public int ContactPK { get; set; }
        public int QuoteFK { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Telephone { get; set; }
        public string Cover { get; set; }
        public string Email { get; set; }
        public int ContactType { get; set; }
        public string UserName { get; set; }
        public string PWord { get; set; }
        public string EntityType { get; set; }
        public string EntityName { get; set; }
        public string EntityTypeName { get; set; }
    }
    public class AddressesDBContext : System.Data.Entity.DbContext
    {
        public DbSet<AddressesModel> Covers { get; set; }
    }
    public class AddressesModel
    {
        [Key]
        public int AddressPK { get; set; }
        public int ContactFK { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public int Locale { get; set; }

    }


}
