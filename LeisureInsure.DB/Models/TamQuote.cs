﻿using System;

namespace LeisureInsure.DB.Models
{
    class TamQuote
    {
        public int QuoteId { get; set; }
        public string QuotePk { get; set; }
        public string Csr { get; set; }
        public string Passcode { get; set; }
        public string TamAgency { get; set; }
        public string TamclientIndex { get; set; }
        public string TamPolicyIndex { get; set; }
        public string PolicyType { get; set; }
        public string Locale { get; set; }
        public string DocumentId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
