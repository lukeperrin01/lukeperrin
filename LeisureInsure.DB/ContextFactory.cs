﻿using LeisureInsure.Services.Dal.LeisureInsure;

namespace LeisureInsure.DB
{
    public class ContextFactory 
    {
        public static LeisureInsureEntities Context()
        {
            return new LeisureInsureEntities();
        }

        public static LiContext LiContext()
        {
            return new LiContext();
        }
    }
}