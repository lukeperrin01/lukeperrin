﻿
using System;
using Microsoft.Azure;


namespace LeisureInsure.DB.Logging
{
    public static class AppInsightLog
    {

        public static string LogTamError(Exception ex, string reference = "", string quoteRef = "")
        {


            var errorLog = new TamErrorLogs();
            string id = "";
            string message = "";

            errorLog.QuoteReference = quoteRef;

            var stacktrace = ex.StackTrace;
            errorLog.DateStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

            if (ex.InnerException != null)
            {
                stacktrace += ex.InnerException.Message;
            }
            errorLog.StackTrace = stacktrace;
            errorLog.Cause = reference;
            message = ex.Message;
            if (ex.InnerException != null)
                message += " " + ex.InnerException.Message;
            errorLog.URL = CloudConfigurationManager.GetSetting("website.address");

            if (message.Length > 1000)
            {
                message = message.Substring(0, 1000);
            }
            errorLog.Msg = message;

            try
            {
                using (var rep = new LeisureInsureEntities())
                {
                    rep.TamErrorLogs.Add(errorLog);
                    rep.SaveChanges();
                    id = "ErrorID: " + errorLog.Id.ToString();
                }

                return id;
            }
            catch (Exception dbEx)
            {
                //LogErrorAppInsights(dbEx, message);
                return "1001";
            }
        }


        public static string LogError(Exception ex, string reference = "", string quoteRef = "")
        {


            var errorLog = new ErrorLogs();
            string id = "";
            string message = "";

            errorLog.QuoteReference = quoteRef;

            var stacktrace = ex.StackTrace;
            errorLog.DateStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");

            if (ex.InnerException != null)
            {
                stacktrace += ex.InnerException.Message;
            }
            errorLog.StackTrace = stacktrace;
            errorLog.Cause = reference;
            message = ex.Message;
            if (ex.InnerException != null)
                message += " " + ex.InnerException.Message;
            errorLog.URL = CloudConfigurationManager.GetSetting("website.address");

            if (message.Length > 1000)
            {
                message = message.Substring(0, 1000);
            }
            errorLog.Msg = message;

            try
            {
                using (var rep = new LeisureInsureEntities())
                {
                    rep.ErrorLogs.Add(errorLog);
                    rep.SaveChanges();
                    id = "ErrorID: " + errorLog.ErrorLogPK.ToString();
                }

                return id;
            }
            catch (Exception dbEx)
            {
                //LogErrorAppInsights(dbEx, message);
                return "1001";
            }
        }



        public static string LogInfo(string message, string reference = "")
        {
            string id = "";

            var errorLog = new ErrorLogs();

            errorLog.DateStamp = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time");
            errorLog.Cause = reference;
            errorLog.URL = CloudConfigurationManager.GetSetting("website.address");
            errorLog.Msg = message;

            try
            {
                using (var rep = new LeisureInsureEntities())
                {
                    rep.ErrorLogs.Add(errorLog);
                    rep.SaveChanges();
                    id = "ErrorID: " + errorLog.ErrorLogPK.ToString();
                }

                return id;
            }
            catch (Exception dbEx)
            {
                //LogErrorAppInsights(dbEx, message);
                return "1001";
            }
        }

        //public static void LogErrorAppInsights(Exception ex, string message)
        //{
        //    var fullMessage =
        //                 $"Message:{ex.Message} Stack Trace:{ex.StackTrace}";

        //    //this is to log events to azure app insights, keeping code just in case
        //    var properties = new Dictionary<string, string>
        //               {{"Message", fullMessage+message}, {"Time", TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, "GMT Standard Time").ToString()}};

        //    // Note: A single instance of telemetry client is sufficient to track multiple telemetry items.
        //    var ai = new TelemetryClient();
        //    ai.TrackException(ex);
        //    ai.TrackEvent("ExceptionMessage", properties);

        //}
    }
}
