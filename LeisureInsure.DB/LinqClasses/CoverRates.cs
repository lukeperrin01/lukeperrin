﻿
using PoliciesModel = LeisureInsure.DB.Policies;

namespace LeisureInsure.DB.LinqClasses
{
    public class CoverRates
    {
        public Rates rates { get; set; }
        public Covers covers { get; set; }
        public PoliciesModel policies { get; set; }
        public QuoteInputs quoteinputs { get; set; }
        public RateTypes ratetypes { get; set; }
    }
}
