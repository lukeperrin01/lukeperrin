//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeisureInsure.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Instalments
    {
        public int InstallmentPK { get; set; }
        public int QuoteRef { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string SortCode { get; set; }
        public string BusinessName { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal FullPremium { get; set; }
        public decimal Deposit { get; set; }
        public decimal FlatRate { get; set; }
        public int NumberOfPayments { get; set; }
        public string InstalmentMonth { get; set; }
        public System.DateTime Timestamp { get; set; }
        public Nullable<System.DateTime> DownLoaded { get; set; }
        public int Lock { get; set; }
    }
}
