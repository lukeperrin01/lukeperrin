//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeisureInsure.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuoteLineNew
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public QuoteLineNew()
        {
            this.QuoteListItem = new HashSet<QuoteListItem>();
            this.QuoteInputNew = new HashSet<QuoteInputNew>();
        }
    
        public int Id { get; set; }
        public int CoverId { get; set; }
        public string SectionDescription { get; set; }
        public string SectionTypeDescription { get; set; }
        public decimal Commision { get; set; }
        public int QuoteId { get; set; }
        public string Territory { get; set; }
        public string PeriodOfCover { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> Net { get; set; }
        public decimal Total { get; set; }
        public decimal Fee { get; set; }
        public Nullable<decimal> Indemnity { get; set; }
        public Nullable<decimal> SumInsured { get; set; }
        public Nullable<bool> ShowInSummary { get; set; }
        public Nullable<bool> ErnExempt { get; set; }
        public string ExtraInfo { get; set; }
        public Nullable<int> Version { get; set; }
        public bool Required { get; set; }
        public Nullable<decimal> Turnover { get; set; }
        public string Ern { get; set; }
        public Nullable<int> NoEmployees { get; set; }
        public string MDAddress { get; set; }
        public string MDPostCode { get; set; }
        public Nullable<decimal> Subsidence { get; set; }
        public Nullable<int> Excess { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public string CustomDescription { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QuoteListItem> QuoteListItem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QuoteInputNew> QuoteInputNew { get; set; }
        public virtual CoversNew CoversNew { get; set; }
    }
}
