//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeisureInsure.DB
{
    using System;
    
    public partial class F_TamScheduleTotals_Result
    {
        public string FieldPrefix { get; set; }
        public int Key { get; set; }
        public Nullable<int> Section { get; set; }
        public string Description { get; set; }
        public string Insured { get; set; }
        public string Indemnity { get; set; }
        public string Rate { get; set; }
        public string Net { get; set; }
        public string Excess { get; set; }
        public string Fee { get; set; }
        public string Gross { get; set; }
        public string Tax { get; set; }
        public string Specification { get; set; }
    }
}
