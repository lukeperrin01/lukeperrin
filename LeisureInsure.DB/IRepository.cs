﻿using System;
using System.Collections.Generic;
using LeisureInsure.DB.ViewModels;
using LeisureInsure.DB.Models;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using DalQuote = LeisureInsure.DB.Quotes;
using System.Linq;
using LeisureInsure.ViewModels;

namespace LeisureInsure.DB
{
    public interface IRepository : IDisposable
    {
        void AddYear(string quoteRef);
        void SetInstalmentsDownloaded();
        IList<S_GetInstallmentsSpreadSheet_Result> GetInstallments();
        IList<vmCertLists> GetCertificateLists(int quotePK);
        IEnumerable<vmCertLists> GetCertList(int quotePK);
        IList<Quotes> GetQuotesFailedTAM();
        void UpdateAspNetEmail(string user, string email);
        Contacts GetContactByPK(int contactPK);
        Contacts GetContactByBrokerCode(string brokerCode);
        int updateContact(Contacts contact);
        Addresses GetAddressByContactPK(int contactPK);
        int updateAddress(Addresses address);
        void ReferQuote(int quotePK = 0, string quoteRef ="");

        #region brokerPortal
        IList<BrokerQuoteSearchVM> getBrokerQuotes(string brokerName);
        #endregion

        #region weberrors
        IList<F_GetWebErrors_Result> getWebErrors();
        #endregion

        #region TAM
        void SetNoteTAMInTam(int QuoteNotePK);
        IList<QuoteNotes> GetNotesForTAM(int quotePK);
        void SaveToErrorLog(string quoteRef, string cause, string stack, string url, string msg);
        void AddInputCharge(int quoteInputPK, decimal charge);
        Contacts GetCustomer(int contactPK);
        Addresses GetAddress(int addressPK);
        Policies GetPolicy(int policyPK);
        Quotes GetQuotebyQuoteRef(string quoteRef);
        Quotes GetQuotebyQuotePK(int quotePK);
        void saveTamClientCode(int quotePK, string tamClientCode);
        void saveTamPolicyCode(int quotePK, string tamPolicyCode);
        void saveTamLegalPolicyCode(int quotePK, string tamLegalPolicyCode);        
        IList<TamCustomDecSchedule> getTamSchedule(int quotePK);
        TamCustomDecPageOne getTamPageOne(int quotePK);
        IList<TamTotals> getTamScheduleTotals(int quotePK);
        Contacts getBrokerByEntityName(string entityName);
        string getBrokerEmailbyTamcode(string brokerName);
        IList<QuoteDocuments> getQuoteDocumentsforEmail(int quotePK);
        IList<QuoteDocuments> addQuoteDocuments(int quotePK);        
        void saveTrustPilot(TrustPilots t);
        void setQuotePurchased(string quoteRef, bool purchased);

        PaymentCards getPaymentCardById(int paymentCardID);
        IList<QuoteInputs> getQuoteInputsbyQuotePK(int quotePK);
        Rates getRatebyPK(int ratePK);
        IList<vmOldRepositoryRate> getPolicyRatesbyRateTypeFK(int policyBW, int localeBW, int rateTypeFK);
        IList<vmOldRepositoryRate> getTaxRates(int localeBW);
        IList<Covers> getCoverList();
        IQueryable<Rates> GetRateByBitWise(long? coverbitwise, int? policyBitWise, int ratetype, int localeid);
        Covers GetCover(int id);

        IList<LandingPages> getLandingPages();

        #endregion

        #region renewals
        void deleteCerts(string quoteRef);
        void RenewalEmailSent(int quotePK);
        IEnumerable<vmPolList> getPolicies(int localeid = 0);
        //IList<vmDocumentsForQuote> getPolicyDocuments(int quotePK);
        IEnumerable<Covers> getCovers();
        //IEnumerable<vmCertLists> GetCertList(int quotePK);
        string setBusinessDescription(int quotePK, string businessDescription = "");
        #endregion
        //vmLogin login(string user, string pword);
        TrustPilots GetTrustPilot();
        DalQuote GetQuote(string quoteref);
        int checkRateReffered(int ratePK);
        IEnumerable<vmRenewalListItem> GetRenewalsByCsr(string csr);
        OldCertificatesModel GetOldcert(int quotepk);
        string getBusinessDescription(int quotePK, int listRatePK);
        string getBrokerEmail(string tamCode);
        int setQuoteCustomerPK(int quotePK);
        void deleteQuoteChildren(string quoteRef);
        string getPolicyType(int policyPK);        
        void deleteContactss(int quotePK);
        IEnumerable<vmSectionHeaders> _getCertSectionsNEW(int quotePk);
        vmEndorsements getQuoteEndorsements(int quotePK);
        IList<vmDocument> getQuoteDocuments(int quotePK);
        Locales getLocale(int localePK);
        int AddQuote(Quotes quote);
        void AddQuoteInputs(IList<QuoteInputs> quoteInputs);
        vmCertHazardList getCertHazards(int quotePK);
        void deleteQuoteInputs(int quotePK);
        IList<vmInputsFlat> getFlatInputs(int localeBw, int policyPk, int quotePk);
        vmBrokerAddress getBrokerAddress(int brokerPK);
        vmQuote getQuote(int quotePK);
        IList<vmDocument> getDocuments(int quotePK);
        vmNavigation _getHome(vmLocales locales, int quotePk, int policyPk);
       
        vmPolicies _getPolicies(int _LocaleBW, int _policyPK);
        vmCovers _getCovers(int _quotePK, int _localeBW, int _policyPK);
        List<vmInputHeader> _getInputs(int _localeBW, int _policyPK, int _quotePK);
        IList<vmInputsFlat> _getQuoteInputs(int localeBw, int policyPk, int quotePk);
        IList<vmChargeSummary> GetCharges(List<vmInputs> _Inputsx);
        IEnumerable<vmCardType> cardTypes();
        int saveInstallment(vmInstalments instalmentVm);
        vmGlobalIris getIris(int _quotePK, decimal _charge);
        string GetAspNetUser(string userName);
        List<ChargeElements> GetChargeInputs(int quoteid, int localeid);
        #region Certificate
        void addCertificate(CertificatesModel document);
        CertificatesModel Getcert(int _quotepk);
        vmCertSchedule _getCertScheduleByQuotePK(int _quotePK);
        IEnumerable<vmSectionHeaders> _getCertSections(int _quotePK);
        List<CertHeader> GetCertHeaders(int quoteid);

        #endregion
        #region Contacts info
        //void AddAddress(IEnumerable<vmAddress> _addresses);
        //int AddContact(IEnumerable<vmContact> _contacts);

        #endregion

        #region Renewals
        //vmRenewalList GetRenewalsByCsr(string csr);
        #endregion

        #region CODES
        Codes GetCode(string codename);
        #endregion

    }
}