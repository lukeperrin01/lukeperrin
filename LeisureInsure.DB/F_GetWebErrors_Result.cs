//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LeisureInsure.DB
{
    using System;
    
    public partial class F_GetWebErrors_Result
    {
        public Nullable<int> ErrorLogPK { get; set; }
        public string QuoteReference { get; set; }
        public string Mantis { get; set; }
        public string Notes { get; set; }
        public string url { get; set; }
        public string Msg { get; set; }
        public string StackTrace { get; set; }
        public string Cause { get; set; }
        public string Reported { get; set; }
        public string Resolved { get; set; }
    }
}
