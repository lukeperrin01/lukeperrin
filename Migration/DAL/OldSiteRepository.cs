﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using LeisureInsure.Services.Dal.LeisureInsure;

namespace Migration.DAL
{
    public class OldSiteRepository : ILiRepository
    {
        private readonly OldSiteContext _context;
        private bool _disposed;

        public OldSiteRepository(OldSiteContext context)
        {
            _context = context;
        }
        public IQueryable<T> StoredProcedure<T>(string functionName, params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public IQueryable<T> Function<T>(string functionName, params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public void Add<T>(T entity) where T : class, IEntity
        {
            _context.Set<T>().Add(entity);
        }

        public void Delete<T>(T entity) where T : class, IEntity
        {
            _context.Set<T>().Remove(entity);
        }

        public IQueryable<T> Q<T>(params Expression<Func<T, object>>[] eagerLoadProperties) where T : class, IEntity
        {
            var result = _context.Set<T>().AsQueryable();
            foreach (var includeRelationship in eagerLoadProperties)
            {
                result = result.Include(includeRelationship);
            }
            return result;
        }

        public IQueryable<T> QS<T>(params Expression<Func<T, object>>[] eagerLoadProperties) where T : class, IEntityString
        {
            var result = _context.Set<T>().AsQueryable();
            foreach (var includeRelationship in eagerLoadProperties)
            {
                result = result.Include(includeRelationship);
            }
            return result;
        }

        public T Find<T>(long id) where T : class, IEntity
        {
            return _context.Set<T>().Find(id);
        }

        public int Commit()
        {
            int recordsAffected;
            try
            {
                recordsAffected = _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                foreach (var dbEntityEntry in e.Entries)
                {
                    Debug.WriteLine("Type: {0} \n\nState: {1}\nEntity: {2}",
                        dbEntityEntry.Entity.GetType(), dbEntityEntry.State, dbEntityEntry.Entity);
                }
                throw;
            }

            return recordsAffected;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _context.Dispose();
            }
            _disposed = true;
        }
    }
}