﻿using System.Data.Entity;

namespace Migration.DAL
{
    public class OldSiteContext : DbContext
    {
        public OldSiteContext() : base("OldSite"){}

        private DbSet<MigrationBrokers> Brokers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           
        }
    }
}
