﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LeisureInsure.Services.Dal.LeisureInsure;

namespace Migration.DAL
{
    public class MigrationBrokers : IEntity
    {
        [Column("Code")]
        public string Username { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }
        public decimal RegulatoryNumber { get; set; }
        public int ContactType { get; set; }

        [Key]
        public int Id { get; set; }
    }
}
