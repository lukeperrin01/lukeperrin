﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using ClosedXML.Excel;
using LeisureInsure.Services.Dal.LeisureInsure;
using LeisureInsure.Services.Dal.LeisureInsure.Quotes;
using Migration.DAL;

namespace Migration
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer<LiContext>(null);
            Database.SetInitializer<OldSiteContext>(null);

            var liRepository = new DefaultRepository(new LiContext());
            var oldSiteRepository = new OldSiteRepository(new OldSiteContext());

            var brokers = oldSiteRepository.Q<MigrationBrokers>();

            var workbook = new XLWorkbook();
            var emailWorksheet = workbook.Worksheets.Add("Email Brokers");
            var nonEmailWorksheet = workbook.Worksheets.Add("Non-email Brokers");
            var failedWorksheet = workbook.Worksheets.Add("Failed");

            var emailRow = 1;
            var nonEmailRow = 1;
            var failedRow = 1;
            var client = new HttpClient();

            var uri = new Uri(ConfigurationManager.AppSettings["create.account"]);
            var digitRegex = new Regex(@".*\d.*", RegexOptions.Compiled | RegexOptions.CultureInvariant);

            foreach (var broker in brokers)
            {
                // Check that the password is valid (at least 6 chars, at least 1 digit)
                var match = digitRegex.Match(broker.Password);
                if (broker.Password.Length < 6 || !match.Success)
                {
                    broker.Password += new Random().Next(999999).ToString("000000");
                }
                
                var data = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Email", broker.Email),
                    new KeyValuePair<string, string>("Password", broker.Password),
                    new KeyValuePair<string, string>("Username", broker.Username),
                });

                var result = client.PostAsync(uri, data)
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult();
                if (!result.IsSuccessStatusCode)
                {
                    failedWorksheet.Cell($"A{failedRow}").Value = broker.Username;
                    failedWorksheet.Cell($"B{failedRow}").Value = broker.Password;
                    failedWorksheet.Cell($"C{failedRow}").Value = broker.Email;
                    failedWorksheet.Cell($"D{failedRow}").Value = broker.Phone;
                    failedWorksheet.Cell($"D{failedRow}").Value = result.ReasonPhrase;

                    failedRow++;
                }

                // Insert into the contacts table too
                var firstName = broker.Name.Split(' ').DefaultIfEmpty(string.Empty).FirstOrDefault();
                var surname = broker.Name.Substring(firstName.Length).Trim(' ');
                var contact = new Agent
                {
                    Email = broker.Email,
                    FSANumber = broker.RegulatoryNumber.ToString(CultureInfo.InvariantCulture),
                    Telephone = broker.Phone,
                    TamClientRef = broker.Username,
                    FirstName = firstName,
                    LastName = surname,
                    ForAttentionOf = string.Empty,
                    //TamName = string.Empty,
                };
                liRepository.Add(contact);

                liRepository.Commit();
                
                if (string.IsNullOrEmpty(broker.Email))
                {
                    nonEmailWorksheet.Cell($"A{nonEmailRow}").Value = broker.Username;
                    nonEmailWorksheet.Cell($"B{nonEmailRow}").Value = broker.Password;
                    nonEmailWorksheet.Cell($"C{nonEmailRow}").Value = broker.Email;
                    nonEmailWorksheet.Cell($"D{nonEmailRow}").Value = broker.Phone;
                    nonEmailRow++;
                }
                else
                {
                    emailWorksheet.Cell($"A{emailRow}").Value = broker.Username;
                    emailWorksheet.Cell($"B{emailRow}").Value = broker.Password;
                    emailWorksheet.Cell($"C{emailRow}").Value = broker.Email;
                    emailWorksheet.Cell($"D{emailRow}").Value = broker.Phone;
                    emailRow++;
                }
            }

            workbook.SaveAs("Broker Passwords.xlsx");

            Console.WriteLine("Finished migration");
            Console.WriteLine("Press enter to exit");
            Console.ReadLine();
        }
    }
}
