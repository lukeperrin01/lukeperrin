﻿using Microsoft.Azure;

namespace LeisureInsure
{
    public static class Config
    {
        public static string AzureStorageConnectionString => CloudConfigurationManager.GetSetting(Constants.Configuration.AzureStorageConnectionString);

        public static string AzureServiceBusConnectionString => CloudConfigurationManager.GetSetting(Constants.Configuration.AzureServiceBusConnectionString);
    }
}
