﻿namespace LeisureInsure
{
    public static class Constants
    {
        public static class Configuration
        {
            public const string AzureStorageConnectionString = "nsb.azure-storage.connection-string";
            public const string AzureServiceBusConnectionString = "nsb.azure-service-bus.connection-string";

            public const string WebsiteBaseAddress = "website.address";
            public const string EmailSender = "email.sender";
            public const string EmailBCC = "email.bcc";
        }

        public static class Endpoints
        {
            public const string Website = "website";           
            public const string ItOps = "itops";
        }
    }
}
